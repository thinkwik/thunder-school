var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Department = db.define('departments', {
    school_id: {
        type: 'integer'
    },
    department_name: String,
    department_description: String,
    status: ['0', '1']
}, {
    timestamp: true,
    methods: {

    }
});
