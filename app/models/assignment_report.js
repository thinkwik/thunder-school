var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var AssignmentReports = db.define('assignment_reports', {
    assignment_id: {
        type: 'integer'
    },
    student_id: {
        type: 'integer'
    },
    achieved_grade: String,
    remarks: String,
    file_path: String,
    status: ['0', '1']
}, {
    timestamp: true,
    methods: {

    }
});
