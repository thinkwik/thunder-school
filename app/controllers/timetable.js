var db = require('orm').db,
    Timetables = db.models.timetables;

exports.register = function(req, res) {
    if (typeof req.body.school_id != 'undefined') {
        newTimetable = new Timetables();
        newTimetable.school_id = req.body.school_id;
        newTimetable.grade_id = req.body.grade_id;
        newTimetable.subject_id = req.body.subject_id;
        newTimetable.day = req.body.day;
        newTimetable.start_time = req.body.start_time;
        newTimetable.end_time = req.body.end_time;
        newTimetable.save(function(err, created_timetable) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, "Bad Request check your parameters");
            }
            return res.send(created_timetable);
        });
    } else {
        return sendError(req, res, 422, 'school_id required for register new designation');
    }
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Designation id required!!');
    }
    Timetables.find({
        id: req.body.id
    }, 1, function(err, timetables) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        timetables[0].school_id = req.body.school_id;
        timetables[0].grade_id = req.body.grade_id;
        timetables[0].subject_id = req.body.subject_id;
        timetables[0].day = req.body.day;
        timetables[0].start_time = req.body.start_time;
        timetables[0].end_time = req.body.end_time;
        timetables[0].save(function(err, updated_timetable) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, "Bad Request check your parameters");
            }
            return res.send(updated_timetable);
        });
    });
};

exports.active = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 400, 'Designation id required!!');
    }
    Timetables.find({
        id: req.body.id
    }, 1, function(err, timetables) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        timetables[0].status = "1";
        timetables[0].save(function(err, activated_timetable) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(activated_timetable);
        });
    });
};

exports.inactive = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Designation id required!!');
    }
    Timetables.find({
        id: req.body.id
    }, 1, function(err, timetables) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        timetables[0].status = "0";
        timetables[0].save(function(err, inactivated_timetable) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(inactivated_timetable);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Designation id required!!');
    }
    Timetables.find({
        id: req.body.id
    }, 1, function(err, timetables) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        timetables[0].remove(function(err) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(timetables[0]);
        });
    });
};

exports.getTimeTable = function(req, res) {
    if (typeof req.query.id == 'undefined') return sendError(req, res, 401, "ID required field");
    Timetables.find({
        id: req.query.id
    }, 1, function(err, timetables) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        if (!timetables.length) return sendError(req, res, 404, "Timetable not found");
        return res.send(timetables[0]);
    });
};


exports.getAllSchoolTimeTable = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = "SELECT t1.*,t2.grade,t3.subject_name,t4.school_name FROM `timetables` as t1 LEFT JOIN grades as t2 on t1.grade_id = t2.id  LEFT JOIN subjects as t3 on t1.subject_id = t3.id  LEFT JOIN schools as t4 on t1.school_id = t4.id WHERE  t4.id = ?";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};



// exports.getAllTimeTable = function(req, res) {
//     if (typeof req.body.school_id == 'undefined' || req.body.school_id === null) {
//         return sendError(req, res, 422, "refer parameters");
//     }
//     var query_str = "SELECT t1.*,t2.school_name FROM `designations` as t1 LEFT JOIN schools as t2 on t1.school_id = t2.id WHERE  t2.parent_id = ?";
//     db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
//         if (err) {
//             console.log(err);
//             return sendError(req, res, 422, "Bad Request check your parameters");
//         }
//         return res.send(rows);
//     });
// };
