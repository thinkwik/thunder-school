var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var AttendanceReports = db.define('attendance_reports', {
    attendance_id: {
        type: 'integer'
    },
    student_id: {
        type: 'integer'
    },
    comment: String,
    status: ['0', '1']
}, {
    timestamp: true,
    methods: {

    }
});
