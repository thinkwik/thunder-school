app.controller('staffSigninFormController', ['$scope', '$http', '$state', '$cookieStore', 'toaster',
    function($scope, $http, $state, $cookieStore, toaster) {
        $scope.staffLogin = function() {
            var data = $scope.staff;
            $http({
                method: 'POST',
                url: '/staff/staffLogin',
                data: data
            }).then(function successCallback(response) {
                $cookieStore.put('loggedInStaff', response.data);
                $state.go('schoolstaff.dashboard');
                toaster.pop('success', 'Staff', 'Logged-in successfully.');
            }, function errorCallback(error) {
                console.log(error);
                $scope.authError = "Please enter correct email and password!";
            });
        };
    }
]);

app.controller('staffLogoutController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.logout = function(staff_id) {
            $http({
                method: 'POST',
                url: '/logout'
            }).then(function successCallback(response) {
                angular.forEach($cookieStore, function(v, k) {
                    $cookieStore.remove(k);
                });
                $cookieStore.remove('loggedInParent');
                $cookieStore.remove('loggedInStudent');
                $cookieStore.remove('loggedInStaff');
                $cookieStore.remove('loggedInUser');
                $cookieStore.remove('loggedInDistrict');
                $cookieStore.remove('loggedInSchool');
            }, function errorCallback(error) {
                toaster.pop('error', 'Logout', 'Something went wrong!');
            });
        };
    }
]);

app.controller('editStaffProfileController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', '$window','Upload',
    function($scope, $http, $state, toaster, $cookieStore, $window,Upload) {

        $scope.getEditStaff = function() {
            $http({
                method: 'GET',
                url: '/staff/edit?id=' + $cookieStore.get('loggedInStaff').id
            }).then(function successCallback(response) {
                $scope.staff = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Staff', 'cannot get all the Staff! try reloading page.');
            });
        };

        $scope.saveEditStaff = function() {
            data = $scope.staff;
            if ($scope.imagename) {
                data.photo = $scope.imagename;
            }
            $http({
                method: 'POST',
                url: '/staff/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Staff', 'Saved Successfully.');
                $state.go('schoolstaff.dashboard');
            }, function errorCallback(error) {
                toaster.pop('error', 'Staff', error.data.message);
            });
        };

        $scope.uploadFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected !');
            Loading(true);
            Upload.upload({
                url: '/staff/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.imagename = success.data;
                    $scope.saveDisable = true;
                },
                function(error) {
                    toaster.pop('error', "Error", error.data.message);
                    $scope.saveDisable = false;
                });
        };

        $scope.cancelStaffForm = function() {
            $state.go('schoolstaff.dashboard');
        };
    }
]);

app.controller('changeStaffPasswordController', ['$scope', '$http', '$translate', '$localStorage', '$cookieStore', 'toaster',
    function($scope, $http, $translate, $localStorage, $cookieStore, toaster) {
        $scope.changePassword = function() {
            if ($scope.newPassword != $scope.confirmNewPassword) {
                toaster.pop('error', 'Password mis match', 'New Password and Confirm New password Must be same!');
            } else {
                var data = {
                    id:$cookieStore.get('loggedInStaff').id,
                    oldPassword: $scope.oldPassword,
                    newPassword: $scope.newPassword,
                    confirmNewPassword: $scope.confirmNewPassword
                };
                $http({
                    method: 'POST',
                    url: '/staff/change_password',
                    data: data
                }).then(function successCallback(response) {
                    toaster.pop('success', 'Password', 'Your Password has been changed.');
                }, function errorCallback(error) {
                    toaster.pop('error', 'Password', error.data.message);
                });
            }
        };
    }
]);
