var bCrypt = require('bcrypt-nodejs');
var db = require('orm').db,
    ParentPages = db.models.parentPages;

exports.register = function(req, res) {
    new_parent_pages = new ParentPages();
    new_parent_pages.title = req.body.title;
    new_parent_pages.status = (typeof req.body.status != 'undefined') ? req.body.status : '1';
    new_parent_pages.save(function(err, saved_parent_page) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(saved_parent_page);
    });
};

exports.getParentPage = function(req, res) {
    if (typeof req.query.id == 'undefined') {
        return sendError(req, res, 422, 'page id required');
    }
    ParentPages.find({
        id: req.query.id
    }, 1, function(err, parentPages) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!parentPages.length) {
            return sendError(req, res, 404, "Page not found");
        }
        return res.send(parentPages[0]);
    });
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'page id required');
    }
    ParentPages.find({
        id: req.body.id
    }, 1, function(err, parent_pages) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!parent_pages.length) {
            return sendError(req, res, 404, "page not found");
        }
        parent_pages[0].title = req.body.title;
        parent_pages[0].status = (typeof req.body.status != 'undefined') ? req.body.status : parent_pages[0].status;
        parent_pages[0].save(function(err, saved_parent_page) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "something went wrong in saving page");
            }
            return res.send(saved_parent_page);
        });
    });
};

exports.getAllParentPages = function(req, res) {
    ParentPages.find({}, function(err, parent_pages) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(parent_pages);
    });
};

exports.active = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'page id required');
    }
    ParentPages.find({
        id: req.body.id
    }, 1, function(err, parent_pages) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        parent_pages[0].status = "1";
        parent_pages[0].save(function(err, activated_parent_page) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "something went wrong in activating page");
            }
            return res.send(activated_parent_page);
        });
    });
};

exports.inactive = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'page id required');
    }
    ParentPages.find({
        id: req.body.id
    }, 1, function(err, parent_pages) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        parent_pages[0].status = "0";
        parent_pages[0].save(function(err, inactivated_parent_page) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "something went wrong in inactivating page");
            }
            return res.send(inactivated_parent_page);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'page id required');
    }
    ParentPages.find({
        id: req.body.id
    }, 1, function(err, parent_pages) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!parent_pages.length) {
            return sendError(req, res, 404, "Page not found");
        }
        parent_pages[0].remove(function(err) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "Something went wrong in deleting page!Please try again");
            }
            return res.send(parent_pages[0]);
        });
    });
};
