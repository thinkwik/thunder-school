app.controller('createStudentAppointmentController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        var date = new Date();
        date.setDate(date.getDate() - 1);
        $scope.minDate = date.toString();
        
        $scope.cancelAppointmentForm = function() {
            $state.go('students.appointment_list');
        };

        $scope.hours = "00";
        $scope.mins = "00";

        $scope.createAppointment = function() {
            $scope.appointment.datetime = $scope.appointment.datetime + " "+ $scope.hours + ":"+ $scope.mins + ":00";
            data = $scope.appointment;
            data.student_id = $cookieStore.get('loggedInStudent').id;
            $http({
                method: "POST",
                url: "/appointment/register",
                data: data
            }).then(function successCallback(response) {
                $state.go('students.appointment_list');
                toaster.pop('success', 'Appointment', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Appointment', error.data.message);
            });
        };

        $scope.getSchoolStaffs = function() {
            var data = {
                school_id: $cookieStore.get('loggedInStudent').school_id
            };
            $http({
                method: 'POST',
                url: '/staff/getAllStaffStudent',
                data: data
            }).then(function successCallback(response) {
                $scope.staffs = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'District', 'Something went wrong!');
            });
        };
    }
]);

app.controller('listStudentAppointmentController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {

        $scope.addNewAppointment = function() {
            $state.go('students.appointment_create');
        };

        $scope.inactiveAppointment = function(appointment_id) {
            var data = {
                id: appointment_id
            };
            $http({
                method: 'POST',
                url: '/appointment/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllAppointments();
            }, function errorCallback(error) {
                toaster.pop('error', 'Appointment', error.data.message);
            });
        };

        $scope.activeAppointment = function(appointment_id) {
            var data = {
                id: appointment_id
            };
            $http({
                method: 'POST',
                url: '/appointment/active',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllAppointments();
            }, function errorCallback(error) {
                toaster.pop('error', 'Appointment', error.data.message);
            });
        };

        $scope.deleteAppointment = function(appointment_id) {
            var data = {
                id: appointment_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/appointment/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllAppointments();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Appointment', error.data.message);
                });
            }
        };

        $scope.getAllAppointments = function() {
            var data = {
                student_id: $cookieStore.get('loggedInStudent').id
            };
            $http({
                method: 'POST',
                url: '/appointment/getAllStudentsAppointment',
                data: data
            }).then(function successCallback(response) {
                var appointments = response.data;
                $scope.appointments = appointments;
                // var newAppointmentArr = [];
                // var appointments = response.data;
                // for (var i = 0; i < appointments.length; i++) {
                //     if (new Date() > new Date(appointments[i].created_at) && new Date() < new Date(appointments[i].datetime)) {
                //         newAppointmentArr.push(appointments[i]);
                //     } else {
                //         console.log();
                //     }
                // }
                // $scope.appointments = newAppointmentArr;
            }, function errorCallback(error) {
                toaster.pop('error', 'Appointment', 'cannot get all the Appointments! try reloading page.');
            });
        };

        $scope.editAppointment = function(appointment_id) {
            $state.go('students.appointment_edit', {
                appointment_id: appointment_id
            });
        };
    }
]);

app.controller('editStudentAppointmentController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        $scope.getEditAppointtment = function() {
            $http({
                method: 'GET',
                url: '/appointment/edit?id=' + $state.params.appointment_id,
            }).then(function successCallback(response) {
                $scope.appointment = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Appointment', 'cannot get all the appointment! try reloading page.');
            });
        };

        $scope.getSchoolStaffs = function() {
            var data = {
                school_id: $cookieStore.get('loggedInStudent').school_id
            };
            $http({
                method: 'POST',
                url: '/staff/getAllStaffStudent',
                data: data
            }).then(function successCallback(response) {
                $scope.staffs = response.data;
                $scope.getEditAppointtment();
            }, function errorCallback(error) {
                toaster.pop('error', 'District', 'Something went wrong!');
            });
        };

        $scope.saveEditAppointment = function() {
            data = $scope.appointment;
            $http({
                method: 'POST',
                url: '/appointment/edit',
                data: data
            }).then(function successCallback(response) {
                $state.go('students.appointment_list');
                toaster.pop('success', 'Appointment', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Appointment', error.data.message);
            });
        };

        $scope.cancelAppointmentForm = function() {
            $state.go('students.appointment_list');
        };
    }
]);
