app.controller('createSuperAdminController', ['$scope', '$http', '$state', 'toaster', 'Upload',
    function($scope, $http, $state, toaster, Upload) {
        $scope.createSuperAdmin = function() {
            var data = $scope.superadmin;
            data.profile_image = $scope.profile_image;
            data.school_id = 0;
            $http({
                method: 'POST',
                url: '/superadmin/register',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Super Admin', 'Created Successfully!');
                $state.go('users.listSuperAdmin');
            }, function errorCallback(error) {
                toaster.pop('error', 'Super Admin', error.data.message);
            });
        };

        $scope.cancelCreateSuperAdminForm = function() {
            $state.go('users.listSuperAdmin');
        };

        $scope.uploadFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected !');
            Loading(true);
            Upload.upload({
                url: '/user/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.profile_image = success.data;
                    $scope.saveDisable = true;
                },
                function(error) {
                    toaster.pop('error', "Error", error.data.message);
                    $scope.saveDisable = false;
                });
        };
    }
]);

app.controller('listSuperAdminController', ['$scope', '$http', '$state', 'toaster', 'Upload',
    function($scope, $http, $state, toaster, Upload) {
        $scope.ChengeOffest = function(increment) {
            $scope.offset = $scope.offset + increment * 10;
            $scope.superadminusers();
        };

        $scope.listSuperAdmin = function() {
            $scope.users = [];
            $scope.offset = 0;
            var data = {
                offset: $scope.offset
            };
            $http({
                method: 'POST',
                url: '/superAdmin/getAllSuperAdmins',
                data: data
            }).then(function successCallback(response) {
                var superAdmins = response.data;
                $scope.superAdmins = superAdmins;
            }, function errorCallback(error) {
                if (error.data.error_code == 444) {
                    $state.go('signin');
                }
                console.log(error.data.message);
            });
        };

        $scope.addNewSuperAdmin = function() {
            $state.go('users.createSuperAdmin');
        };

        $scope.editSuperAdmin = function(superAdmin_id) {
            $state.go('users.editSuperAdmin', {
                superAdmin_id: superAdmin_id
            });
        };

        $scope.superadminusers = function() {
            var data = {
                offset: $scope.offset
            };
            $http({
                method: 'POST',
                url: '/superAdmin/getAllSuperAdmins',
                data: data
            }).then(function successCallback(response) {
                var users = response.data;
                $scope.users = users;
            }, function errorCallback(error) {
                if (error.data.error_code == 444) {
                    $state.go('signin');
                }
                console.log(error.data.message);
            });
        };

        $scope.inactiveSuperAdmin = function(superAdmin_id) {
            var data = {
                id: superAdmin_id
            };
            $http({
                method: 'POST',
                url: '/user/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.listSuperAdmin();
            }, function errorCallback(error) {
                console.log(error.data.message);
            });
        };

        $scope.activeSuperAdmin = function(superAdmin_id) {
            var data = {
                id: superAdmin_id
            };
            $http({
                method: 'POST',
                url: '/user/active',
                data: data
            }).then(function successCallback(response) {
                $scope.listSuperAdmin();
            }, function errorCallback(error) {
                console.log(error.data.message);
            });
        };

        $scope.deleteSuperAdmin = function(superAdmin_id) {
            var data = {
                id: superAdmin_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/user/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.listSuperAdmin();
                }, function errorCallback(error) {
                    console.log(error.data.message);
                });
            }
        };

    }
]);

app.controller('editSuperAdminController', ['$scope', '$http', '$state', 'toaster', 'Upload',
    function($scope, $http, $state, toaster, Upload) {

        $scope.cancelEditSuperAdminForm = function() {
            $state.go('users.listSuperAdmin');
        };

        $scope.uploadFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected !');
            Loading(true);
            Upload.upload({
                url: '/user/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.profile_image = success.data;
                    $scope.saveDisable = true;
                },
                function(error) {
                    toaster.pop('error', "Error", error.data.message);
                    $scope.saveDisable = false;
                });
        };

        $scope.getSuperAdmin = function() {
            $http({
                method: 'GET',
                url: '/user/edit?id=' + $state.params.superAdmin_id,
            }).then(function successCallback(response) {
                $scope.superadmin = response.data;
            }, function errorCallback(error) {
                console.log(error.data.message);
            });
        };
        $scope.saveSuperAdmin = function() {
            var data = $scope.superadmin;
            data.profile_image = $scope.profile_image;
            $http({
                method: 'POST',
                url: '/user/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Success', 'User updated.');
                $state.go('users.listSuperAdmin');
            }, function errorCallback(error) {
                toaster.pop('error', 'Success', error.data.message);
            });
        };
    }
]);
