app.controller('createParentIssuesController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        $scope.cancelIssueForm = function() {
            $state.go('parents.issues_list');
        };

        $scope.createIssue = function() {
            data = $scope.issue;
            data.parent_id = $cookieStore.get('loggedInParent').id;
            $http({
                method: "POST",
                url: "/issues/register",
                data: data
            }).then(function successCallback(response) {
                var issue_message = {
                    issue_id: response.data.id,
                    message_from: $cookieStore.get('loggedInParent').id,
                    message_to: response.data.staff_id,
                    message: $scope.issue.description
                };
                $http({
                    method: "POST",
                    url: "/issue_messages/saveIssuesMessges",
                    data: issue_message
                }).then(function successCallback(response) {
                    $state.go('parents.issues_list');
                    toaster.pop('success', 'Issues', 'Saved Successfully.');
                }, function errorCallback(error) {
                    toaster.pop('error', 'Issues', error.data.message);
                });
            }, function errorCallback(error) {
                toaster.pop('error', 'Issues', error.data.message);
            });
        };

        $scope.getSchoolStaffs = function() {
            var data = {
                school_id: $cookieStore.get('loggedInParent').school_id
            };
            $http({
                method: 'POST',
                url: '/staff/getAllStaffStudent',
                data: data
            }).then(function successCallback(response) {
                $scope.staffs = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'District', 'Something went wrong!');
            });
        };
    }
]);

app.controller('listParentIssuesController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {

        $scope.addNewIssue = function() {
            $state.go('parents.issues_create');
        };

        $scope.inactiveIssue = function(issue_id) {
            var data = {
                id: issue_id
            };
            $http({
                method: 'POST',
                url: '/issues/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllIssues();
            }, function errorCallback(error) {
                toaster.pop('error', 'Issue', error.data.message);
            });
        };

        $scope.activeIssue = function(issue_id) {
            var data = {
                id: issue_id
            };
            $http({
                method: 'POST',
                url: '/issues/active',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllIssues();
            }, function errorCallback(error) {
                toaster.pop('error', 'Issue', error.data.message);
            });
        };

        $scope.deleteIssue = function(issue_id) {
            var data = {
                id: issue_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/issues/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllIssues();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Issue', error.data.message);
                });
            }
        };

        $scope.getAllIssues = function() {
            var data = {
                parent_id: $cookieStore.get('loggedInParent').id
            };
            $http({
                method: 'POST',
                url: '/issues/getAllParentsIssues',
                data: data
            }).then(function successCallback(response) {
                var issues = response.data;
                $scope.issues = issues;
            }, function errorCallback(error) {
                toaster.pop('error', 'Issue', 'cannot get all the Issues! try reloading page.');
            });
        };

        $scope.editIssue = function(issue_id) {
            $state.go('parents.issues_edit', {
                issue_id: issue_id
            });
        };

        $scope.viewResponse = function(issue_id) {
            window.open('/parent/issues/view/' + issue_id, '_blank');
        };

    }
]);

app.controller('editParentIssuesController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        $scope.getEditIssue = function() {
            $http({
                method: 'GET',
                url: '/issues/edit?id=' + $state.params.issue_id,
            }).then(function successCallback(response) {
                $scope.issue = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Issue', 'cannot get all the issue! try reloading page.');
            });
        };

        $scope.getSchoolStaffs = function() {
            var data = {
                school_id: $cookieStore.get('loggedInParent').school_id
            };
            $http({
                method: 'POST',
                url: '/staff/getAllStaffStudent',
                data: data
            }).then(function successCallback(response) {
                $scope.staffs = response.data;
                $scope.getEditIssue();
            }, function errorCallback(error) {
                toaster.pop('error', 'District', 'Something went wrong!');
            });
        };

        $scope.saveEditIssue = function() {
            data = $scope.issue;
            $http({
                method: 'POST',
                url: '/issues/edit',
                data: data
            }).then(function successCallback(response) {
                $state.go('parents.issues_list');
                toaster.pop('success', 'Issue', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Issue', error.data.message);
            });
        };

        $scope.cancelIssueForm = function() {
            $state.go('parents.issues_list');
        };
    }
]);

app.controller('viewParentIssuesController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        $scope.viewMessage = function() {
            var data = {
                id: $state.params.issue_id
            };
            $http({
                method: 'POST',
                url: '/issues/viewStudentIssue',
                data: data
            }).then(function successCallback(response) {
                var issuesData = response.data;
                $scope.issuesData = issuesData;
                toaster.pop('success', 'Issue', 'Status updated successfully!');
                $scope.getAllIssues();
            }, function errorCallback(error) {
                toaster.pop('error', 'Issue', 'cannot get all the Issues! try reloading page.');
                console.log(error.data.message);
            });
        };

        $scope.btnBack = function() {
            $state.go('parents.issues_list');
        };

        $scope.btnRefresh = function() {
            $scope.viewMessage();
        };

        $scope.getAllIssues = function() {
            var data = {
                student_id: $cookieStore.get('loggedInParent').id
            };
            $http({
                method: 'POST',
                url: '/issues/getAllStudentsIssues',
                data: data
            }).then(function successCallback(response) {
                var issues = response.data;
                $scope.issues = issues;
            }, function errorCallback(error) {
                toaster.pop('error', 'Issue', 'cannot get all the Issues! try reloading page.');
            });
        };
    }
]);


app.controller('responseIssuesFromParentController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
          $scope.responseMessageFromParents = function() {
            var data = {
                issue_id: $state.params.issue_id,
                message_from: $cookieStore.get('loggedInParent').id,
                message_to: $scope.$parent.issuesData[0].staff_id,
                message: $scope.response.message
            };
            $http({
                method: 'POST',
                url: '/issue_messages/saveIssuesMessges',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Issue', 'response sent successfully!');
                $scope.response = null;
            }, function errorCallback(error) {
                toaster.pop('error', 'Issue', 'cannot get all the Issues! try reloading page.');
                console.log(error.data.message);
            });
        };
    }
]);
