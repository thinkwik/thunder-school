var bCrypt = require('bcrypt-nodejs');
var db = require('orm').db,
    Todo = db.models.todos;

exports.register = function(req, res) {
    if (typeof req.body.user_id != 'undefined' && typeof req.body.type != 'undefined') {
        var arr_type = ['admin', 'student', 'staff', 'parent'];
        if (arr_type.indexOf(req.body.type) != -1) {
            var new_todo = new Todo();
            new_todo.user_id = req.body.user_id;
            new_todo.type = req.body.type;
            new_todo.description = req.body.description;
            new_todo.date = req.body.date;
            new_todo.status = "0";
            new_todo.save(function(err, created_todo) {
                if (err) {
                    console.log(err);
                    return apiSendError(req, res, 500, "Something went wrong while creating");
                }
                var response = {};
                response['data'] = [];
                response['code'] = 200;
                return res.send(response);
            });
        } else {
            return apiSendError(req, res, 422, 'please check type');
        }
    } else {
        return apiSendError(req, res, 422, 'please check your parameters');
    }
};

exports.getMyTodos = function(req, res) {
    if (typeof req.body.user_id != 'undefined' && typeof req.body.type != 'undefined') {
        var arr_type = ['admin', 'student', 'staff', 'parent'];
        if (arr_type.indexOf(req.body.type) != -1) {
            var query_str = "SELECT * FROM `todos` WHERE user_id = ? AND type = ? ORDER BY id DESC";
            db.driver.execQuery(query_str, [req.body.user_id, req.body.type], function(err, rows) {
                if (err) {
                    console.log(err);
                    return apiSendError(req, res, 400, "Bad Request check your parameters");
                }
                var response = {};
                response['data'] = rows;
                response['code'] = 200;
                return res.send(response);
            });
        } else {
            return apiSendError(req, res, 422, 'please check type');
        }
    } else {
        return apiSendError(req, res, 422, 'please check your parameters');
    }
};

exports.delete = function(req, res) {
    if (typeof req.body.id != 'undefined') {
        Todo.find({
            id: req.body.id
        }, 1, function(err, todos) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 500, 'Something went wrong!');
            }
            if (todos.length <= 0) {
                return apiSendError(req, res, 404, 'Todo not found');
            }
            todos[0].remove(function(err, removed_todo) {
                if (err) {
                    console.log(err);
                    return apiSendError(req, res, 500, 'Something went wrong!');
                }
                var response = {};
                response['data'] = [];
                response['code'] = 200;
                return res.send(response);
            });
        });
    } else {
        return apiSendError(req, res, 422, 'please check your parameters');
    }
};
exports.done = function(req, res) {
    if (typeof req.body.id != 'undefined') {
        Todo.find({
            id: req.body.id
        }, 1, function(err, todos) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 500, 'Something went wrong!');
            }
            if (todos.length <= 0) {
                return apiSendError(req, res, 404, 'Todo not found');
            }
            todos[0].status = (todos[0].status == "1") ? "0" : "1";
            todos[0].save(function(err, saved_todo) {
                if (err) {
                    console.log(err);
                    return apiSendError(req, res, 500, 'Something went wrong!');
                }
                var response = {};
                response['data'] = [];
                response['code'] = 200;
                return res.send(response);
            });
        });
    } else {
        return apiSendError(req, res, 422, 'please check your parameters');
    }
};
