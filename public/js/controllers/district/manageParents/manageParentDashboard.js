app.controller('parentDashboardController', ['$scope', '$http', '$state', '$cookieStore', 'toaster','$compile',
    function($scope, $http, $state, $cookieStore, toaster, $compile) {
        $scope.getParentDashboardData = function() {
           //$scope.student_total_staffs();
            $scope.student_events_lists();
            $scope.student_total_assignments();
            $scope.student_events_lists_calendar();
            $scope.parent_video_conferance_calendar();
            $scope.parent_appointments();
        };
        $scope.parentAssignmentRedirect = function(){
            $state.go('parents.download_assignment');
        };
        $scope.viewStaffClassesRedirect = function(){
            $state.go('parents.view_staff');
        };
        $scope.parent_appointments = function() {
            var data ={
                parent_id:$cookieStore.get('loggedInParent').id
            };
            $http({
                method: 'POST',
                url: '/parent/appointment/listAllAppointments',
                data:data
            }).then(function successCallback(response) {
                $scope.showAppointments = [];
                if(response.data.length) {
                    $scope.calAppointments = response.data;
                    Notification.requestPermission(function (permission) {
                      // If the user accepts, let's create a notification
                      if (permission === "granted") {
                        response.data.forEach(function(value) {
                            var mail_datetime = new Date(value.datetime);
                            tost_datetime = new Date(mail_datetime.getTime() - 15*60000);
                            var next_three_days = new Date();
                            next_three_days.setDate(next_three_days.getDate()+3);
                            if(mail_datetime >= new Date() && mail_datetime <= next_three_days) {
                                $scope.$apply(function() {
                                    $scope.showAppointments.push(value);
                                });
                                setToHappen(function () {
                                    var body = "At Time : " + value.datetime + "\n";
                                    body+= "Description : "+ value.description;
                                    var options = {
                                        icon:"/img/1472530977_tmp_Socrates_Logo.jpg",
                                        body: body,
                                        requireInteraction:true
                                    };
                                    var with_str = value.staff_name;
                                    var n = new Notification('You have an appointment with ' + with_str ,options);
                                }, tost_datetime);
                            }
                        });
                      }
                    });
                }
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong! getting Dashobard data');
            });
        };
        $scope.appointArrived = function(appointment_id) {
            var data ={
                id:appointment_id
            };
            $http({
                method: 'POST',
                url: '/appointment/active',
                data:data
            }).then(function successCallback(response) {
                $scope.parent_appointments();
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong!');
            });
        };
        $scope.student_total_staffs = function(){
            var data ={
                grade_id:$cookieStore.get('loggedInParent').grade_id
            };
            $http({
                method: 'POST',
                url: '/dashboard/student/total_staffs',
                data:data
            }).then(function successCallback(response) {
              $scope.total_staffs = (response.data[0].total_staffs !== undefined) ? response.data[0].total_staffs : "0";
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong! getting Dashobard data');
            });
        };
        $scope.student_events_lists = function(){
            var data ={
                school_id:$cookieStore.get('loggedInParent').school_id
            };
            $http({
                method: 'POST',
                url: '/dashboard/student/events_lists',
                data:data
            }).then(function successCallback(response) {
               $scope.events_lists = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong! getting Dashobard data');
            });
        };
        
        $scope.student_total_assignments = function(){
            var data ={
                grade_id:$cookieStore.get('loggedInParent').grade_id
            };
            $http({
                method: 'POST',
                url: '/dashboard/student/total_assignments',
                data:data
            }).then(function successCallback(response) {
                $scope.total_assignments = (response.data[0].total_assignments !== undefined) ? response.data[0].total_assignments : "0";
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong! getting Dashobard data');
            });
        };
        $scope.student_student_total_exams = function(){
            var data ={
                grade_id:$cookieStore.get('loggedInParent').grade_id
            };
            $http({
                method: 'POST',
                url: '/dashboard/student/total_exams',
                data:data
            }).then(function successCallback(response) {
                $scope.total_exams = (response.data[0].total_exams !== undefined) ? response.data[0].total_exams : "0";
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong! getting Dashobard data');
            });
        };
        
        $scope.student_total_parents = function(){
            var data ={
                student_id:$cookieStore.get('loggedInParent').id
            };
            $http({
                method: 'POST',
                url: '/dashboard/student/total_parents',
                data:data
            }).then(function successCallback(response) {
                $scope.total_parents = (response.data[0].total_parents !== undefined) ? response.data[0].total_parents : "0";
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong! getting Dashobard data');
            });
        };
        

        $scope.student_pending_assignments = function(){
            var data ={
                grade_id:$cookieStore.get('loggedInParent').grade_id
            };
            $http({
                method: 'POST',
                url: '/dashboard/student/pending_assignments',
                data:data
            }).then(function successCallback(response) {
               $scope.pending_assignments = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong! getting Dashobard data');
            });
        };
        $scope.studentCollapsed = [{name: 'one',isCollapsed: true}];
        $scope.staffCollapsed = [{name: 'two',isCollapsed: true}];

        $scope.eventRender = function( event, element, view ) { 
            element.attr({
                'data-toggle' : 'tooltip',
                'title' : event.description
            });
            element.tooltip();
            $compile(element)($scope);
        };

        //calendar options and config
        $scope.uiConfig = {
          calendar:{
            height: 450,
            editable: false,
            header:{
              //left: 'month basicWeek basicDay agendaWeek agendaDay',
              left: 'month basicWeek',
              center: 'title',
              right: 'today prev,next'
            },
            eventClick: $scope.alertEventOnClick,
            eventDrop: $scope.alertOnDrop,
            eventResize: $scope.alertOnResize,
            eventRender:$scope.eventRender
          }
        };

        $scope.events = [];
        $scope.eventSources = [$scope.events];
        
        $scope.student_events_lists_calendar = function(){
            var data ={
                school_id:$cookieStore.get('loggedInParent').school_id
            };
            $http({
                method: 'POST',
                url: '/dashboard/student/events_lists/calendar',
                data:data
            }).then(function successCallback(response) {
               angular.forEach(response.data,function(value, key){
                $scope.events.push({
                    title: value.title,
                    description: (typeof value.content != 'undefined' ) ? value.content.substring(17, 125).replace("</div>", "")+"..." : "No Description",
                    start: value.start_date,
                    end: value.end_date,
                    stick:true
                });
               });
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong! getting Dashobard data');
            });
        };
        $scope.parent_video_conferance_calendar = function(){
            var data = {
                unique_id : $cookieStore.get('loggedInParent').unique_id
            };
            $http({
                method: 'POST',
                url: '/video/myConferanceList',
                data:data
            }).then(function successCallback(response) {
               angular.forEach(response.data,function(value, key){
                $scope.events.push({
                    title: value.room_name,
                    description: 'Video Conferance Invitation',
                    start: value.scheduled_date,
                    allDay: true,
                    color: "#5b6f60",
                    stick:true
                });
               });
               $scope.add_appointments();
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong! getting Dashobard data');
            });
        };

        $scope.add_appointments = function() {
            setTimeout(function() {
                if($scope.calAppointments.length){
                    angular.forEach($scope.calAppointments,function(value, key){
                        var status = "Pending";
                        var color = "#ee3939";
                        if(value.status == "1") {
                            status = "Accepted";
                            color = "#27c24c";
                        } else if (value.status == "2") {
                            status = "Declined";
                            color = "#ee3939";
                        } else if (value.status == "3") {
                            status = "Rescheduled";
                            color = "#23b7e5";
                        } else if (value.status == "4") {
                            status = "No Show";
                            color = "#ee3939";
                        } else if (value.status == "5") {
                            status = "Completed";
                            color = "#27c24c";
                        }
                        var with_str = (value.parent_id == null)  ? value.student_name : value.parent_name;
                        $scope.$apply(function() {
                            $scope.events.push({
                                title: "Appointment - " + status,
                                description: "Appointment with " + with_str+ ' : ' +value.description,
                                start: new Date(value.datetime),
                                allDay: true,
                                color: color,
                                stick:true
                            });
                        });                        
                    });
                }                
            }, 1500);            
        };
    }
]);



app.controller('parentStaffListController', ['$scope', '$http', '$state', '$cookieStore', 'toaster',
    function($scope, $http, $state, $cookieStore, toaster) {
        $scope.getAllStudentClasses = function(){
            data = {
                student_id : $cookieStore.get('loggedInParent').student_id,
                grade_id : $cookieStore.get('loggedInParent').grade_id,
            };
            $http({
                method: 'POST',
                url: '/staff/getAllStudentClasses',
                data:data
            }).then(function successCallback(response) {
               $scope.staffs = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Staff', 'Something went wrong! getting data');
            });
        };

}
]);