var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Appointments = db.define('appointments', {
    staff_id: {
        type: 'integer'
    },
    student_id: {
        type: 'integer'
    },
    parent_id: {
        type: 'integer'
    },
    appointment_type: String,
    datetime: String,
    description: String,
    phone_number: String,
    student_attend: ['0', '1'],
    status: ['0', '1', '2']
}, {
    timestamp: true,
    methods: {

    }
});
