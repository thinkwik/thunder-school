var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var StudentQAResult = db.define('exam_qa_results', {
    exam_id: {
        type: 'integer'
    },
    student_id: {
        type: 'integer'
    },
    qa_id: {
        type: 'integer'
    },
    type: String,
    given_answer: String,
    is_correct: ['0','1'],
    is_attend: ['0','1'],    
    achived_marks : Number
}, {
    timestamp: true,
    methods: {}
});
