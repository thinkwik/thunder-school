var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var InquiryFollowUps = db.define('inquiryfollowups', {
    student_id: {
        type: 'integer'
    },
    followup: String,
    remarks: String,
    next_follow_date: String,
    normal_status: String,
    inquiry_status: String,
    status: ['0', '1']
}, {
    timestamp: true,
    methods: {

    }
});
