var bCrypt = require('bcrypt-nodejs');
var moment = require('moment');
var db = require('orm').db,
    Exam = db.models.exams,
    ExamResult = db.models.exam_results;
StudentQAResult = db.models.exam_qa_results;


exports.register = function(req, res) {
    if (typeof req.body.school_id == 'undefined') {
        return sendError(req, res, 422, 'required parameter not fullfiled for register Exam');
    }
    var new_exam = new Exam();
    new_exam.school_id = req.body.school_id;
    new_exam.staff_id = req.body.staff_id;
    new_exam.session_id = req.body.session_id;
    new_exam.grade_id = req.body.grade_id;
    new_exam.subject_id = req.body.subject_id;
    new_exam.class_id = req.body.class_id;
    new_exam.exam_head = req.body.exam_head;
    new_exam.start_date = req.body.start_date;
    new_exam.end_date = req.body.end_date;
    new_exam.start_time = req.body.start_time;
    new_exam.end_time = req.body.end_time;
    new_exam.max_marks = req.body.max_marks;
    new_exam.save(function(err, saved_exam) {
        if (err) {
            console.log(err);
            return sendError(req, res, 500, 'Something went wrong!');
        }
        var query_str = 'SELECT t1.student_id as id FROM student_classes as t1  WHERE t1.grade_id = ? AND t1.class_id = ?';
        db.driver.execQuery(query_str, [req.body.grade_id, req.body.class_id], function(err, all_students) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            if (all_students.length > 0) {
                var counter = 0;
                all_students.forEach(function(value, key) {
                    var data = {
                        exam_id: saved_exam.id,
                        student_id: value.id,
                        result_date: null,
                        achived_marks: 0,
                        total_marks: 0,
                        status: "0",
                        achived_grade:0
                    };
                    ExamResult.create(data, function(err) {
                        counter++;
                        if (err) {
                            console.log(err);
                            return sendError(req, res, 500, "Something went wrong!");
                        }
                        if(counter == all_students.length){
                            return res.send(saved_exam);            
                        }
                    });
                });
                
            } else {
                return res.send(saved_exam);
            }
        });
    });
};

exports.getExam = function(req, res) {
    if (typeof req.query.id == 'undefined') return res.send("id required for getting Exam");
    Exam.find({
        id: req.query.id
    }, 1, function(err, exams) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!exams.length) {
            return sendError(req, res, 404, "exam not found with perticular id");
        }
        return res.send(exams[0]);
    });
};
exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'exam id required');
    }
    Exam.find({
        id: req.body.id
    }, 1, function(err, exams) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!exams.length) {
            return sendError(req, res, 404, "Exam not found");
        }
        exams[0].session_id = req.body.session_id;
        exams[0].grade_id = req.body.grade_id;
        exams[0].class_id = req.body.class_id;
        exams[0].subject_id = req.body.subject_id;
        exams[0].exam_head = req.body.exam_head;
        exams[0].start_date = req.body.start_date;
        exams[0].end_date = req.body.end_date;
        exams[0].max_marks = req.body.max_marks;
        exams[0].start_time = req.body.start_time;
        exams[0].end_time = req.body.end_time;
        exams[0].save(function(err, updated_exam) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            return res.send(updated_exam);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'exam id required');
    }
    Exam.find({
        id: req.body.id
    }, 1, function(err, exams) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!exams.length) {
            return sendError(req, res, 404, "Exam not found");
        }
        exams[0].remove(function(err) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "Something went wrong in deleting Exam");
            }
            return res.send(exams[0]);
        });
    });
};

exports.getAllExams = function(req, res) {
    if (typeof req.body.school_id == 'undefined') {
        return sendError(req, res, 422, 'school id required');
    }
    var query_str = `SELECT t1.* , t2.school_name, t3.session_name, t4.grade, t5.subject_name, COALESCE(SUM(t6.max_mark),0) as total_marks 
                    FROM exams as t1 
                    LEFT JOIN schools as t2 ON t1.school_id = t2.id 
                    LEFT JOIN sessions as t3 ON t1.session_id = t3.id 
                    LEFT JOIN grades as t4 ON t1.grade_id = t4.id 
                    LEFT JOIN subjects as t5 ON t1.subject_id = t5.id 
                    LEFT JOIN exam_qa as t6 ON t1.id = t6.exam_id 
                    WHERE t1.school_id = ? 
                    GROUP BY t1.id`;

    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getStudentsExam = function(req, res) {
    if (typeof req.body.student_id == 'undefined' || typeof req.body.student_id == 'undefined') {
        return sendError(req, res, 422, 'Grade ID required');
    }
    var query_str = `SELECT t1.* , COALESCE(SUM(t6.max_mark),0) as total_marks ,t2.exam_head,t2.start_date,t2.end_date,t2.start_time,t2.end_time, t3.session_name, t4.grade, t5.subject_name, t7.class_name
        FROM exam_results as t1
        LEFT JOIN exams as t2 ON t1.exam_id = t2.id
        LEFT JOIN sessions as t3 ON t2.session_id = t3.id
        LEFT JOIN grades as t4 ON t2.grade_id = t4.id
        LEFT JOIN subjects as t5 ON t2.subject_id = t5.id
        LEFT JOIN classes as t7 ON t2.class_id = t7.id
        LEFT JOIN exam_qa as t6 ON t1.exam_id = t6.exam_id
        WHERE t1.student_id = ? GROUP BY t1.id
        ORDER BY t1.id DESC`;
    // var query_str = `SELECT t1.* , COALESCE(SUM(t6.max_mark),0) as total_marks ,t2.exam_head,t2.start_date,t2.end_date,t2.start_time,t2.end_time, t3.session_name, t4.grade, t5.subject_name, t7.class_name FROM exam_results as t1 LEFT JOIN exams as t2 ON t1.exam_id = t2.id LEFT JOIN sessions as t3 ON t2.session_id = t3.id  LEFT JOIN grades as t4 ON t2.grade_id = t4.id LEFT JOIN subjects as t5 ON t2.subject_id = t5.id LEFT JOIN classes as t7 ON t2.class_id = t7.id LEFT JOIN exam_qa as t6 ON t1.exam_id = t6.exam_id WHERE t1.student_id = ? GROUP BY t1.id HAVING total_marks <> 0 ORDER BY t1.id DESC`;
    db.driver.execQuery(query_str, [req.body.student_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.verifyStudent = function(req, res) {
    if (typeof req.body.student_id == 'undefined' || typeof req.body.exam_id == 'undefined') {
        return sendError(req, res, 422, 'Please check your parameters');
    }
    var query_str = "SELECT * FROM exam_results WHERE student_id = ?  AND exam_id = ?";
    db.driver.execQuery(query_str, [req.body.student_id, req.body.exam_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (rows.length == 0) {
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.startExamInit = function(req, res) {
    if (typeof req.body.student_id == 'undefined' || typeof req.body.exam_id == 'undefined') {
        return sendError(req, res, 422, 'Please check your parameters');
    }
    //delete exam and student data first if has already
    var query_str = "DELETE FROM `exam_qa_results` WHERE `exam_id` = ? AND  `student_id` = ?";
    db.driver.execQuery(query_str, [req.body.exam_id, req.body.student_id], function(err, rows) {});
    var query_str = "SELECT * FROM exam_qa WHERE exam_id = ?";
    db.driver.execQuery(query_str, [req.body.exam_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Something Went wrong,Please try after sometime.");
        }
        if (rows.length == 0) {
            return sendError(req, res, 400, "No Question added by Teacher.");
        }
        rows.forEach(function(exam_qa) {
            var data = {
                exam_id: req.body.exam_id,
                student_id: req.body.student_id,
                qa_id: exam_qa.id,
                type: exam_qa.type,
                given_answer: null,
                is_correct: "0",
                is_attend: "0",
                achived_marks: 0
            }
            StudentQAResult.create(data, function(err) {
                if (err) {
                    console.log(err);
                    return sendError(req, res, 500, "Something went wrong!");
                }
            });
        })

        //final Update total marks of student in exam result table
        var query_str = `SELECT COALESCE(SUM(max_mark),0) as max_mark FROM exam_qa WHERE exam_id = ? `;
        db.driver.execQuery(query_str, [req.body.exam_id], function(err, final_marks) {
            console.log(final_marks);
            if (err) {
                console.log(err);
            }
            var update_qur = `UPDATE exam_results SET total_marks = ?  WHERE exam_id = ? AND student_id = ?`;
            db.driver.execQuery(update_qur, [final_marks[0].max_mark, req.body.exam_id, req.body.student_id], function(err, final) {
                if (err) {
                    console.log(err);
                }
                res.send("success", 200);
            });
        });
    });
};

exports.getStudentQuestion = function(req, res) {
    if (typeof req.body.student_id == 'undefined' || typeof req.body.exam_id == 'undefined') {
        return sendError(req, res, 422, 'Please check your parameters');
    }
    var query_str = `SELECT t1.id as qa_id, t1.is_attend, t1.type , t2.question, t2.option_a,t2.option_b,t2.option_c,t2.option_d, t2.max_mark  FROM exam_qa_results as t1
                LEFT JOIN exam_qa as t2 ON t1.qa_id = t2.id
                WHERE t1.student_id = ? AND t1.exam_id = ? AND t1.is_attend = "0"
                ORDER BY RAND() LIMIT 1`;
    db.driver.execQuery(query_str, [req.body.student_id, req.body.exam_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (rows.length == 0) {
            return res.send("exam_completed");
        }
        return res.send(rows[0]);
    });
};

exports.addStudentAnswer = function(req, res) {
    if (typeof req.body.qa_id == 'undefined' || typeof req.body.answer == 'undefined') {
        return sendError(req, res, 422, 'Please check your parameters');
    }
    var query_str = "UPDATE exam_qa_results SET given_answer =  ? , is_attend = '1' WHERE id = ?";
    db.driver.execQuery(query_str, [req.body.answer, req.body.qa_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (rows.length == 0) {
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows[0]);
    });
};

exports.calculateExamResult = function(req, res) {
    if (typeof req.body.student_id == 'undefined' || typeof req.body.exam_id == 'undefined') {
        return sendError(req, res, 422, 'Please check your parameters');
    }
    var query_str = `SELECT * FROM exam_qa_results WHERE exam_id = ? AND student_id = ? AND type = ?`;
    db.driver.execQuery(query_str, [req.body.exam_id, req.body.student_id, "muliple_choice"], function(err, answers) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (answers.length == 0) {
            var update_qur = `UPDATE exam_results SET achived_marks = ?, status = '1' WHERE exam_id = ? AND student_id = ?`;
            db.driver.execQuery(update_qur, ["0", req.body.exam_id, req.body.student_id], function(err, essay_exam) {
                if (err) {
                    console.log(err);
                    return sendError(req, res, 400, "Bad Request check your parameters");
                }
                return res.send(200);
            });
        }
        check_answers(req, res, answers, function(err, result) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }

            update_result(req.body.exam_id, req.body.student_id, function(err, result) {
                if (err) {
                    console.log(err);
                    return sendError(req, res, 400, "Bad Request check your parameters");
                }
                return res.send(200);
            })
        })
    });
};

check_answers = function(req, res, answers, cb) {
    var itemsProcessed = 0;
    var answers = answers;
    answers.forEach(function(ans) {
        var sub_query_str = `SELECT answer, max_mark FROM exam_qa WHERE id = ?`;
        db.driver.execQuery(sub_query_str, [ans.qa_id], function(err, main_que) {
            if (err) {
                cb("Bad Request check your parameters", null);
            }
            var is_correct = (main_que[0].answer == ans.given_answer) ? '1' : '0';
            var achived_marks = (main_que[0].answer == ans.given_answer) ? main_que[0].max_mark : 0;
            var update_qur = "UPDATE exam_qa_results SET is_correct = ?, achived_marks = ? WHERE id = ?";
            db.driver.execQuery(update_qur, [is_correct, achived_marks, ans.id], function(err, final) {
                if (err) {
                    console.log(err);
                    cb("Bad Request check your parameters", null);
                }
                itemsProcessed++;
                if (itemsProcessed === answers.length) {
                    cb(null, true);
                }
            });
        });
    })
};

update_result = function(exam_id, student_id, cb) {
    //final Update marks of student in exam result table
    var query_str = `SELECT COALESCE(SUM(achived_marks),0) as achived_marks FROM exam_qa_results WHERE exam_id = ? AND student_id = ?`;
    db.driver.execQuery(query_str, [exam_id, student_id], function(err, final_marks) {
        if (err) {
            cb(err, null);
        }
        var update_qur = `UPDATE exam_results SET achived_marks = ?, status = '1' WHERE exam_id = ? AND student_id = ?`;
        db.driver.execQuery(update_qur, [final_marks[0].achived_marks, exam_id, student_id], function(err, final) {
            if (err) {
                cb(err, null);
            }
            cb(null, true);
        });
    });
};

exports.getExamTime = function(req, res) {
    if (typeof req.body.student_id == 'undefined' || typeof req.body.exam_id == 'undefined') {
        return sendError(req, res, 422, 'Please check your parameters');
    }
    var query_str = "SELECT `start_time`,`end_time` FROM `exams` WHERE id= ?";
    db.driver.execQuery(query_str, [req.body.exam_id], function(err, rows) {
        if (err) {
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (rows.length == 0) {
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        var startDate = moment(rows[0].start_time, 'HH:mma')
        var endDate = moment(rows[0].end_time, 'HH:mma')
        var seconds = moment(endDate).diff(startDate, 'seconds');
        var response = {
            seconds: seconds
        };
        res.send(response);
    });
};

exports.forceCompleteExam = function(req, res) {
    if (typeof req.body.student_id == 'undefined' || typeof req.body.exam_id == 'undefined') {
        return sendError(req, res, 422, 'Please check your parameters');
    }
    var query_str = "UPDATE `exam_qa_results` SET `is_attend`= '1' WHERE `exam_id` = ? AND `student_id` = ?";
    db.driver.execQuery(query_str, [req.body.exam_id, req.body.student_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        res.send(200);
    });
};

exports.questionAnswerReport = function(req, res) {
    if (typeof req.body.student_id == 'undefined' || typeof req.body.exam_id == 'undefined') {
        return sendError(req, res, 422, 'Please check your parameters');
    }
    var query_str = `SELECT t1.*, t2.option_a,t2.option_b,t2.option_c,t2.option_d, t2.answer as main_question_ans, t2.question, t2.max_mark as question_max_mark, CONCAT(t3.first_name, " ", t3.last_name) as student_name, t3.image as student_image, t4.achived_marks as total_achived_marks, t4.total_marks as total_marks FROM exam_qa_results as t1
                    LEFT JOIN exam_qa as t2 ON t1.qa_id = t2.id
                    LEFT JOIN admissions as t3 ON t1.student_id = t3.id
                    LEFT JOIN exam_results as t4 ON t1.exam_id = t4.exam_id AND t1.student_id = t4.student_id
                    WHERE t1.exam_id = ? AND t1.student_id = ? GROUP BY t1.id`;
    db.driver.execQuery(query_str, [req.body.exam_id, req.body.student_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        res.send(rows);
    });
};
exports.updateQuestionAnswerReport = function(req, res) {
    if (typeof req.body.student_id == 'undefined' || typeof req.body.exam_id == 'undefined' || typeof req.body.achived_marks == 'undefined') {
        return sendError(req, res, 422, 'Please check your parameters');
    }
    var is_correct = (req.body.achived_marks > 0) ? "1" : "0";
    var query_str = `UPDATE exam_qa_results SET is_correct= ? ,achived_marks= ? WHERE id = ?`;
    db.driver.execQuery(query_str, [is_correct, req.body.achived_marks, req.body.id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        update_result(req.body.exam_id, req.body.student_id, function(err, success) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            res.send(200);
        })
    });
};

exports.gradeCalculation = function (req, res){
    if (typeof req.body.exam_id == 'undefined') {
        return sendError(req, res, 422, 'Please check your parameters');
    }
    var query_str = `SELECT t1.* , t2.school_id FROM exam_results as t1 
                     LEFT JOIN exams as t2 on t1.exam_id = t2.id
                     WHERE exam_id = ?`;
    db.driver.execQuery(query_str, [req.body.exam_id], function(err, results) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if(results.length == 0){
            return sendError(req, res, 404, "No results found for perticular exam");
        }
        result_grade_calculation(results,function(err,success){
            if(err){
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            res.send(200);
        });

    });
};

result_grade_calculation = function(results, cb){
    var itemsProcessed = 0;
    results.forEach(function(single_result){
        var percentage= 0;
        if(single_result.total_marks > 0) {
            percentage = parseInt(single_result.achived_marks/single_result.total_marks * 100);
        }
        /*
        var scale = 0;
        if(97 <= percentage && percentage <= 100){
            scale = 4;
        } else if(93 <= percentage && percentage <= 96){
            scale = 4;
        } else if(90 <= percentage && percentage <= 92){
            scale = 3.7;
        } else if(87 <= percentage && percentage <= 89){
            scale = 3.3;
        } else if(83 <= percentage && percentage <= 86){
            scale = 3.0;
        } else if(80 <= percentage && percentage <= 82){
            scale = 2.7;
        } else if(77 <= percentage && percentage <= 79){
            scale = 2.3;
        } else if(73 <= percentage && percentage <= 76){
            scale = 2.0;
        } else if(70 <= percentage && percentage <= 72){
            scale = 1.7;
        } else if(67 <= percentage && percentage <= 69){
            scale = 1.3;
        } else if(65 <= percentage && percentage <= 66){
            scale = 1.0;
        }
*/
        var query_str_1 = `SELECT * FROM school_grade_scale WHERE ( '`+percentage+`' >= from_per AND '`+percentage+`' <= to_per) HAVING school_id = '`+single_result.school_id+`' ORDER BY id ASC`;

        db.driver.execQuery(query_str_1, [], function(error, scale_data) {
            if(error){
                console.log(err);
                cb(true,null);
            }
            var scale = 0;
            if(scale_data.length > 0){
                scale = scale_data[0].scale;
            }
            var query_str_2 = 'UPDATE `exam_results` SET achived_grade = ?  WHERE id = ?';
            db.driver.execQuery(query_str_2, [scale, single_result.id], function(err, temp) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            itemsProcessed++;
            if (itemsProcessed === results.length) {
            console.log("success");
                cb(null, true);
            }
            });            
        });
    });
};

exports.sendEmailExamGrade = function(req, res){
    if (typeof req.body.exam_id == 'undefined') {
        return sendError(req, res, 422, 'Please check your parameters');
    }
    var query_str = `SELECT * FROM exam_results WHERE exam_id = '`+ req.body.exam_id +`' `;
    //IF student_id is set then send to perticular player
    if (typeof req.body.student_id != 'undefined' && req.body.student_id != "") {
        query_str += ` AND student_id = '`+ req.body.student_id +`' `;
    }
    db.driver.execQuery(query_str, [], function(err, results) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if(results.length == 0) {
            return sendError(req, res, 404, "No results found for perticular exam");
        }
        var itemsProcessed = 0;
        results.forEach(function(single_result){
            send_grade_mail(req,res,single_result,function(err,success){
                if(err){
                    return sendError(req, res, 500, "something went wrong while sending mail");
                }
                if(success){
                    itemsProcessed++;
                    if(itemsProcessed == results.length){
                        res.send(200);
                    }    
                }                
            });
        });

    });
};

send_grade_mail = function(req,res,single_result,cb){
    var query_str_1 = ` SELECT 
                        t1.*,
                        t2.email as parent_email, t2.full_name as parent_name, t2.mobile as parent_mobile ,
                        t3.school_name, t3.id as school_id
                        FROM admissions as t1 
                        LEFT JOIN parents as t2 ON t1.id = t2.student_id AND t2.email <> null
                        LEFT JOIN schools as t3 ON t1.school_id = t3.id
                        WHERE t1.id = ?
                        GROUP BY t1.id`;
    db.driver.execQuery(query_str_1, [single_result.student_id], function(err, student_data) {
        if (err) {
            console.log(err);
            return cb(true,null);
        }
        if(student_data.length == 0) {
            console.log("email templete not found");
            return cb(true,null);
        }
        var query_str_2 = 'SELECT * FROM `school_email_templates` WHERE `school_id` = ? AND `ui_key` = ? LIMIT 1';
        db.driver.execQuery(query_str_2, [student_data[0].school_id,single_result.achived_grade], function(err, email_templete) {
            if (err) {
                console.log(err);
                return cb(true,null);
            }
            
            var query_str_3 = `SELECT t1.* ,t2.class_name,t3.subject_name
                                FROM exams as t1
                                LEFT JOIN classes as t2 ON t1.class_id = t2.id
                                LEFT JOIN subjects as t3 ON t1.subject_id = t3.id
                                WHERE t1.id = ?
                                GROUP BY t1.id
                                LIMIT 1
                            `;
            db.driver.execQuery(query_str_3, [single_result.exam_id], function(err, exam_data) {
            if (err) {
                console.log(err);
                return cb(true,null);
            }
            var email_html = email_templete[0].ui_data;
            email_html = email_html.replace("<<STUDENT_FIRST_NAME>>", student_data[0].first_name);
            email_html = email_html.replace("<<STUDENT_LAST_NAME>>", student_data[0].last_name);
            email_html = email_html.replace("<<STUDENT_EMAIL>>", student_data[0].email);
            email_html = email_html.replace("<<STUDENT_CLASS>>", exam_data[0].class_name);
            email_html = email_html.replace("<<STUDENT_SUBJECT>>", exam_data[0].subject_name);
            email_html = email_html.replace("<<STUDENT_MOBILE>>", student_data[0].mobile);
            email_html = email_html.replace("<<EXAM_NAME>>", exam_data[0].exam_head);
            email_html = email_html.replace("<<EXAM_TOTAL_MARKS>>", exam_data[0].total_marks);
            email_html = email_html.replace("<<EXAM_ACHIVED_MARKS>>", exam_data[0].achived_marks);
            email_html = email_html.replace("<<EXAM_ACHIVED_SCALE>>", exam_data[0].achived_grade);
            email_html = email_html.replace("<<EXAM_START_DATE>>", exam_data[0].start_date);
            email_html = email_html.replace("<<EXAM_END_DATE>>", exam_data[0].end_date);
            email_html = email_html.replace("<<EXAM_START_TIME>>", exam_data[0].start_time);
            email_html = email_html.replace("<<EXAM_END_TIME>>", exam_data[0].end_time);
            email_html = email_html.replace("<<PARENTS_NAME>>", student_data[0].parent_name);
            email_html = email_html.replace("<<PARENTS_EMAIL>>", student_data[0].parent_email);
            email_html = email_html.replace("<<PARENTS_MOBILE>>", student_data[0].parent_mobile);
            
            var from = 'binit@thinkwik.com';
            var to = [student_data[0].email,student_data[0].parent_email];
            var subject = "Thunder-School Exam Report Notification";
            sendMail(req, res, from, to, subject, email_html, function(err, success) {
                if (err) {
                    console.log(err);
                }
            });
            cb(null,true);
            });
        });
    });
};
