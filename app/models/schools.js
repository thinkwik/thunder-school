var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var School = db.define('schools', {
    school_name: String,
    school_type: ['district', 'school'],
    address: String,
    country: String,
    state: String,
    city: String,
    phone_number: String,
    office_number: String,
    fax_number: String,
    max_students:{
        type: 'integer'
    },
    authorized_user_id: {
        type: 'integer'
    },
    parent_id: {
        type: 'integer'
    },
    status: ['0', '1']
}, {
    timestamp: true,
    methods: {

    }
});