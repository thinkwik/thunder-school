app.controller('listLectureController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', '$window',
    function($scope, $http, $state, toaster, $cookieStore, $window) {
        $scope.getAllLectures = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/lecture/getAllLecturesSchoolWise',
                data: data
            }).then(function successCallback(response) {
                var lectures = response.data;
                $scope.lectures = lectures;
            }, function errorCallback(error) {
                toaster.pop('error', 'Lectures', 'cannot get all the Lectures! try reloading page.');
                console.log(error.data.message);
            });
        };

        $scope.addNewLecture = function() {
            $state.go('schools.create_lecture');
        };

        $scope.deleteLectures = function(lecture_id) {
            var data = {
                id: lecture_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/lecture/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllLectures();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Lectures', error.data.message);
                });
            }
        };

        $scope.editLectures = function(lecture_id) {
            $state.go('schools.edit_lecture', {
                lecture_id: lecture_id
            });
        };
    }
]);


app.controller('createLectureController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        $scope.getSessions = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/session/getAllSchoolSessions',
                data: data
            }).then(function successCallback(response) {
                $scope.sessions = response.data;
                $scope.getStaffs();
                $scope.getGrades();
                $scope.getDepartments();
                $scope.getLocations();
            }, function errorCallback(error) {
                toaster.pop('error', 'Sessions', 'Something went wrong!');
            });
        };
        $scope.getLocations = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/location/getAllLocations',
                data: data
            }).then(function successCallback(response) {
                $scope.locations = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Location', 'Something went wrong!');
            });
        };

        $scope.getStaffs = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/staff/getSchoolsAllStaff',
                data: data
            }).then(function successCallback(response) {
                $scope.staffs = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Staff', 'Something went wrong!');
            });
        };

        $scope.getDepartments = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/school/department/getAllSchoolDepartment',
                data: data
            }).then(function successCallback(response) {
                $scope.departments = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Departments', 'Something went wrong!');
            });
        };

        $scope.getGrades = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/grade/getAllSchoolGrades',
                data: data
            }).then(function successCallback(response) {
                $scope.grades = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Grades', 'Something went wrong!');
            });
        };

        $scope.getSubject = function(grade_id) {
            var data = {
                grade_id: grade_id,
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/subject/getSchoolGradesSubjectWise',
                data: data
            }).then(function successCallback(response) {
                $scope.subjects = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Subjects', 'Something went wrong!');
            });
        };

        $scope.listLectures = function() {
            $state.go('schools.list_lecture');
        };

        $scope.createLecture = function() {
            data = $scope.lecture;
            data.school_id = $cookieStore.get('loggedInSchool').id;
            $http({
                method: "POST",
                url: "/lecture/register",
                data: data
            }).then(function successCallback(response) {
                $state.go('schools.list_lecture');
                toaster.pop('success', 'Lectures', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Lectures', error.data.message);
            });
        };

        $scope.getSchoolGrades = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/grade/getAllSchoolGrades',
                data: data
            }).then(function successCallback(response) {
                $scope.grades = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Grades', 'Something went wrong!');
            });
        };

        $scope.getClassSubjectWise = function(subject_id) {
            var data = {
                subject_id: subject_id
            };
            $http({
                method: 'POST',
                url: '/class/getClassSubjectWise',
                data: data
            }).then(function successCallback(response) {
                $scope.classes = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Student Classes', error.data.message);
            });
        };

        $scope.getSubjectGradeWise = function(grade_id) {
          $scope.classes = [];
          $scope.subjects = [];
          $scope.lecture.class_id = "";
          $scope.lecture.subject_id = "";
            var data = {
                grade_id: grade_id,
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/subject/getSchoolGradesSubjectWise',
                data: data
            }).then(function successCallback(response) {
                $scope.subjects = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Subjects', 'Something went wrong!');
            });
        };

    }
]);

app.controller('editLectureController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.getSessions = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/session/getAllSchoolSessions',
                data: data
            }).then(function successCallback(response) {
                $scope.sessions = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Sessions', 'Something went wrong!');
            });
        };

        $scope.getLocations = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/location/getAllLocations',
                data: data
            }).then(function successCallback(response) {
                $scope.locations = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Location', 'Something went wrong!');
            });
        };

        $scope.getStaffs = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/staff/getSchoolsAllStaff',
                data: data
            }).then(function successCallback(response) {
                $scope.staffs = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Staff', 'Something went wrong!');
            });
        };

        $scope.getGrades = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/grade/getAllSchoolGrades',
                data: data
            }).then(function successCallback(response) {
                $scope.grades = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Grades', 'Something went wrong!');
            });
        };

        $scope.getSubjectGradeWise = function(grade_id) {
            var data = {
                grade_id: grade_id,
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/subject/getSchoolGradesSubjectWise',
                data: data
            }).then(function successCallback(response) {
                $scope.subjects = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Subjects', 'Something went wrong!');
            });
        };

        $scope.getSubject = function(grade_id) {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/subject/getAllSchoolSubjects',
                data: data
            }).then(function successCallback(response) {
                $scope.subjects = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Subjects', 'Something went wrong!');
            });
        };

        $scope.getDepartments = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/school/department/getAllSchoolDepartment',
                data: data
            }).then(function successCallback(response) {
                $scope.departments = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Departments', 'Something went wrong!');
            });
        };

        $scope.getEditLectures = function() {
            $http({
                method: 'GET',
                url: '/lecture/edit?id=' + $state.params.lecture_id
            }).then(function successCallback(response) {
                $scope.lecture = response.data;
                $scope.getLocations();
                $scope.getSessions();
                $scope.getStaffs();
                $scope.getGrades();
                $scope.getDepartments();
                $scope.getSubjectGradeWise(response.data.grade_id);
            }, function errorCallback(error) {
                toaster.pop('error', 'Lecture', 'cannot get all the Lectures! try reloading page.');
            });
        };

        $scope.saveEditLectures = function() {
            data = $scope.lecture;
            $http({
                method: 'POST',
                url: '/lecture/edit',
                data: data
            }).then(function successCallback(response) {
                $state.go('schools.list_lecture');
                toaster.pop('success', 'Lecture', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Lecture', error.data.message);
            });
        };

        $scope.listLectures = function() {
            $state.go('schools.list_lecture');
        };
    }
]);
