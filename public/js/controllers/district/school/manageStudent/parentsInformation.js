app.controller('studentParentsInformationController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', '$window',
    function($scope, $http, $state, toaster, $cookieStore, $window) {
        $scope.logged_in_parent = $cookieStore.get('loggedInStudent').id;
        $scope.getParentsList = function() {
            var data = {
                student_id: $cookieStore.get('loggedInStudent').id
            };
            $http({
                method: 'POST',
                url: '/parents/getStudentsParents',
                data: data
            }).then(function successCallback(response) {
                $scope.all_parents = response.data;
            }, function errorCallback(error) {
                console.log(error);
                toaster.pop('error', 'Parents information', 'cannot get details of Parents ! try reloading page.');
          });
        };

        // $scope.changeAnotherChild = function(student_id) {
        //     var data = {
        //         id: $cookieStore.get('loggedInParent').id,
        //         student_id:student_id
        //     };
        //     $http({
        //         method: 'POST',
        //         url: '/parents/changeChild',
        //         data: data
        //     }).then(function successCallback(response) {
        //         $cookieStore.remove('loggedInParent');
        //         $cookieStore.put('loggedInParent', response.data);
        //         toaster.pop('success', 'Children', 'Changed Child Details');
        //         $state.go('parents.dashboard');
        //     }, function errorCallback(error) {
        //         toaster.pop('error', 'Children', 'cannot get details of Children ! try reloading page.');
        //         console.log(error.data.message);
        //     });
        // };
    }
]);
