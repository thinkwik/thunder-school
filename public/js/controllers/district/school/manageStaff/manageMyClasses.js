app.controller('myScheduleListStaffController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
	function($scope, $http, $state, toaster, $cookieStore) {
		$scope.getAllMyScheduleList = function() {
			var data = {
				staff_id: $cookieStore.get('loggedInStaff').id
			};
			$http({
				method: 'POST',
				url: '/staff/getAllMySchedules',
				data: data
			}).then(function successCallback(response) {
				$scope.mySchedules = response.data;
			}, function errorCallback(error) {
				toaster.pop('error', 'Event Resource', 'Something went wrong! getting all resources');
			});
		};
	}
]);
