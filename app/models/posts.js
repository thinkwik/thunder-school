var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Post = db.define('posts', {
    photo: String,
    description: String,
    created_by: String
}, {
    timestamp: true,
    methods: {

    }
});