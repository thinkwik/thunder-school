  var db = require('orm').db,
      User = db.models.users,
      School = db.models.schools,
      Admissions = db.models.admissions,
      Staff = db.models.staff,
      Emails = db.models.emails;

  exports.sendEmails = function(req, res) {
      newEmail = new Emails();
      newEmail.email_from = req.body.email_from;
      newEmail.email_to = req.body.email_to;
      newEmail.subject = req.body.subject;
      newEmail.content = req.body.content;
      newEmail.save(function(err, created_email) {
          if (err) {
              console.log(err);
              return res.send(err);
          }
          return res.send(created_email);
      });
  };

  exports.deleteMail = function(req, res) {
      if (typeof req.body.id == 'undefined') {
          return sendError(req, res, 404, 'Mail id required!!');
      }
      Emails.find({
          id: req.body.id
      }, 1, function(err, emails) {
          if (err) {
              console.log(err);
              return sendError(req, res, 400, "Bad Request check your parameters");
          }
          if (!emails.length) {
              return sendError(req, res, 404, "Mail not found");
          }
          emails[0].remove(function(err) {
              if (err) {
                  console.log(err);
                  return sendError(req, res, 500, "Something went wrong in deleting mail!Please try again");
              }
              return res.send(emails[0]);
          });
      });
  };

  exports.getTheMail = function(req, res) {
      if (typeof req.query.id == 'undefined') return sendError(req, res, 401, "ID required field");
      Emails.find({
          id: req.query.id
      }, 1, function(err, mails) {
          if (err) {
              console.log(err);
              return res.send(err);
          }
          if (!mails.length) return sendError(req, res, 404, "Mails Inquiry not found");
          return res.send(mails[0]);
      });
  };

  exports.listMails = function(req, res) {
      Emails.find({
          email_to: req.body.email
      }, function(err, mails) {
          if (err) {
              console.log(err);
              return res.send(err);
          }
          // if (!mails.length) return sendError(req, res, 404, "Mails Inquiry not found");
          return res.send(mails);
      });
  };

  exports.sendEmailsToStaaf = function(req, res) {
      newEmail = new Emails();
      newEmail.email_from = req.body.email_from;
      newEmail.email_to = req.body.email_to;
      newEmail.subject = req.body.subject;
      newEmail.content = req.body.content;
      newEmail.save(function(err, created_email) {
          if (err) {
              console.log(err);
              return res.send(err);
          }
          return res.send(created_email);
      });
  };


  exports.getStaffEmail = function(req, res) {
      if (typeof req.query.id == 'undefined') return sendError(req, res, 401, "ID required field");
      Emails.find({
          id: req.query.id
      }, 1, function(err, mails) {
          if (err) {
              console.log(err);
              return res.send(err);
          }
          if (!mails.length) return sendError(req, res, 404, "Mails Inquiry not found");
          return res.send(mails[0]);
      });
  };

  exports.allSentEmailList = function(req, res) {
      if (typeof req.body.email_id == 'undefined' || req.body.email_id === null) {
          return sendError(req, res, 422, "email ID required");
      }
      var query_str = "SELECT * FROM emails WHERE email_from = ? ORDER BY id DESC LIMIT 20";
      db.driver.execQuery(query_str, [req.body.email_id], function(err, rows) {
          if (err) {
              console.log(err);
              return sendError(req, res, 400, "Bad Request check your parameters");
          }
          return res.send(rows);
      });
  };

  exports.allReceivedEmailList = function(req, res) {
      if (typeof req.body.email_id == 'undefined' || req.body.email_id === null) {
          return sendError(req, res, 422, "email ID required");
      }
      var query_str = "SELECT * FROM emails WHERE email_to LIKE ? ORDER BY id DESC LIMIT 20";
      db.driver.execQuery(query_str, ["%" + req.body.email_id + "%"], function(err, rows) {
          if (err) {
              console.log(err);
              return sendError(req, res, 400, "Bad Request check your parameters");
          }
          return res.send(rows);
      });
  };

  // exports.listMails = function(req, res) {
  //     Emails.find({}, function(err, allEmails) {
  //         if (err) {
  //             console.log(err);
  //             return sendError(req, res, 422, "Bad Request check your parameters");
  //         }
  //         return res.send(allEmails);
  //     });
  // };
