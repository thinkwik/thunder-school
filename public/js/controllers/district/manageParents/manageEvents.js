app.controller('listParentsEventsController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        $scope.listEvents = function() {
            var data = {
                school_id: $cookieStore.get('loggedInParent').school_id
            };
            $http({
                method: 'POST',
                url: '/gallery/listAllParentevents',
                data: data
            }).then(function successCallback(response) {
                var events = response.data;
                $scope.events = events;
            }, function errorCallback(error) {
                toaster.pop('error', 'Events', 'cannot get all the Events! try reloading page.');
            });
        };

        $scope.ViewGallery = function(gallery_id) {
            $state.go('parents.event_gallery_view', {
                gallery_id: gallery_id
            });
        };

        $scope.parentRegistration = function(event) {
            var result = confirm("Are you sure you want to go to this event?");
            if (result) {
                var data = {};
                data.parent_id = $cookieStore.get("loggedInParent").id;
                data.school_id = $cookieStore.get("loggedInParent").school_id;
                data.event_id = event.event_id;
                $http({
                    url: '/event/registration/parentRegistration',
                    method: 'POST',
                    data: data
                }).then(function successCallback(response) {
                    toaster.pop('success', 'Events', 'You are registered Successfully!');
                    $scope.listEvents();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Events', 'Something went wrong!');
                })
            }
        };

    }
]);

app.controller('viewEventsGalleryAlbumController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {

        $scope.viewAlbum = function() {
            var data = {
                gallery_id: $state.params.gallery_id
            };
            $http({
                method: 'POST',
                url: '/event/viewAlbum',
                data: data
            }).then(function successCallback(response) {
                var galleryImages = response.data;
                $scope.galleryImages = galleryImages;
            }, function errorCallback(error) {
                toaster.pop('error', 'Gallery', error.data.message);
            });
        };
    }
]);
