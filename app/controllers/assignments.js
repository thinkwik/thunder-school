var db = require('orm').db,
    Assignments = db.models.assignments;
AssignmentReports = db.models.assignment_reports;

exports.register = function(req, res) {
    if (typeof req.body.class_id === 'undefined' || typeof req.body.grade_id === 'undefined') {
        return res.sendError(req, res, 500, 'Something went wrong!');
    }
    newAssignments = new Assignments();
    newAssignments.school_id = req.body.school_id;
    newAssignments.staff_id = req.body.staff_id;
    newAssignments.subject_id = parseInt(req.body.subject_id);
    newAssignments.grade_id = req.body.grade_id;
    newAssignments.class_id = req.body.class_id;
    newAssignments.title = req.body.title;
    newAssignments.description = req.body.description;
    newAssignments.due_date = req.body.due_date;
    newAssignments.file_name = req.body.file_name;
    newAssignments.save(function(err, created_Assignment) {
        if (err) {
            console.log(err);
            return sendError(req, res, 500, 'Something went wrong!');
        }
        var query_str = 'SELECT t1.student_id as id FROM student_classes as t1  WHERE t1.grade_id = ? AND t1.class_id = ?';
        db.driver.execQuery(query_str, [req.body.grade_id, req.body.class_id], function(err, all_students) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            if (all_students.length > 0) {
                all_students.forEach(function(value, key) {
                    var data = {
                        assignment_id: created_Assignment.id,
                        student_id: value.id,
                        achieved_grade: null,
                        remarks: null,
                        file_path: null,
                        status: "0"
                    };
                    AssignmentReports.create(data, function(err) {
                        if (err) {
                            console.log(err);
                            return sendError(req, res, 500, "Something went wrong!");
                        }
                    });
                });
                return res.send(created_Assignment);
            } else {
                return res.send(created_Assignment);
            }
        });
    });
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Assignment id required!!');
    }
    Assignments.find({
        id: req.body.id
    }, 1, function(err, assignments) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        assignments[0].school_id = req.body.school_id;
        assignments[0].staff_id = req.body.staff_id;
        assignments[0].grade_id = req.body.grade_id;
        assignments[0].subject_id = req.body.subject_id;
        assignments[0].title = req.body.title;
        assignments[0].description = req.body.description;
        assignments[0].due_date = req.body.due_date;
        assignments[0].file_name = req.body.file_name;
        assignments[0].save(function(err, updated_assignments) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(updated_assignments);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Assignment id required!!');
    }
    Assignments.find({
        id: req.body.id
    }, 1, function(err, assignments) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        if (assignments.length == 0) {
            return sendError(req, res, 404, "Assignment not found.");
        }
        if (assignments[0].file_name) {
            var assignmentFile = __dirname + "/../../public/upload/assignments/" + assignments[0].file_name;
            var fs = require('fs');
            fs.exists(assignmentFile, function(exists) {
                if (exists) {
                    fs.unlinkSync(assignmentFile);
                }
            });
        }
        assignments[0].remove(function(err) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(assignments[0]);
        });
    });
};

exports.getAssignment = function(req, res) {
    if (typeof req.query.id == 'undefined') {
        return sendError(req, res, 401, "ID required field");
    }
    Assignments.find({
        id: req.query.id
    }, 1, function(err, assignments) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        if (!assignments.length) return sendError(req, res, 404, "Assignment not found");
        return res.send(assignments[0]);
    });
};

exports.getAllAssignments = function(req, res) {
    if (typeof req.body.staff_id == 'undefined') {
        return sendError(req, res, 422, "Please check your parameters");
    }
    var query_str = `SELECT t1.*,t2.grade,CONCAT(t3.first_name," ",t3.last_name) as staff_name,t4.school_name, t5.subject_name,t6.class_name
                    FROM assignments as t1
                    LEFT JOIN grades as t2 ON t1.grade_id = t2.id
                    LEFT JOIN staff as t3 ON t1.staff_id = t3.id
                    LEFT JOIN schools as t4 ON t1.school_id = t4.id
                    LEFT JOIN subjects as t5 ON t1.subject_id = t5.id
                    LEFT JOIN classes as t6 ON t6.id = t1.class_id
                    WHERE t1.staff_id = ?`;
    db.driver.execQuery(query_str, [req.body.staff_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getAllStudentAssignments = function(req, res) {
    var current_date = new Date().toISOString().split("T")[0].toString();
    if (typeof req.body.student_id == 'undefined' || req.body.student_id === null) {
        return sendError(req, res, 422, "student_id ID required");
    }
    var query_str = `SELECT t1.*,t2.title,t2.due_date,t2.file_name,t3.grade,CONCAT(t4.first_name," ",t4.last_name) as staff_name,t5.school_name, t6.subject_name, t7.class_name
                      FROM assignment_reports as t1
                      LEFT JOIN assignments as t2 ON t1.assignment_id = t2.id
                      LEFT JOIN grades as t3 ON t2.grade_id = t3.id
                      LEFT JOIN staff as t4 ON t2.staff_id = t4.id
                      LEFT JOIN schools as t5 ON t2.school_id = t5.id
                      LEFT JOIN subjects as t6 ON t2.subject_id = t6.id
                      LEFT JOIN classes as t7 ON t2.class_id = t7.id
                      WHERE t2.due_date >= \'` + current_date + `\' AND t1.student_id = ? ORDER BY t1.id DESC`;
    db.driver.execQuery(query_str, [req.body.student_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.uploadFile = function(req, res) {
    var fs = require('fs');
    var assignments = __dirname + "/../../public/upload/assignments/";
    if (!fs.existsSync(assignments)) {
        fs.mkdirSync(assignments);
    }
    var file = req.files.file;
    var fileExtension = file.name.split(".")[1];
    if (fileExtension) {
      fileName = file.path.split('/');
      fs.rename(file.path, assignments + fileName[fileName.length - 1] +"."+ fileExtension);
      return res.send(fileName[fileName.length - 1] +"."+ fileExtension);
    }
    else {
      return sendError(req, res, 400, "Bad Request check your parameters");
    }

};

exports.getStudentsAssignment = function(req, res) {
    if (typeof req.body.id == 'undefined' || req.body.id === null) {
        return sendError(req, res, 422, "Assignment ID required");
    }
    var query_str = "SELECT t1.* FROM `assignment_reports` as t1 WHERE t1.id = ?";
    db.driver.execQuery(query_str, [req.body.id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
