app.controller('districtAllEventRegistrationsListController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.addNewEventRegistrationRedirect = function() {
            $state.go('districts.event_registration_create');
        };
        $scope.getAllEventDistrictRegistrations = function() {
            var data = {
                district_id: $cookieStore.get('loggedInDistrict').id
            };
            $http({
                method: 'POST',
                url: '/event/registration/getAllEventDistrictRegistrations',
                data: data
            }).then(function successCallback(response) {
                $scope.registrations = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Event Resources', 'Something went wrong! getting all registrations');
            });
        };
        $scope.activeRegistration = function(registration_id) {
            var data = {
                id: registration_id
            };
            $http({
                method: 'POST',
                url: '/event/registration/active',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllEventDistrictRegistrations();
            }, function errorCallback(error) {
                toaster.pop('error', 'Events', error.data.message);
            });
        };
        $scope.inactiveRegistration = function(registration_id) {
            var data = {
                id: registration_id
            };
            $http({
                method: 'POST',
                url: '/event/registration/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllEventDistrictRegistrations();
            }, function errorCallback(error) {
                toaster.pop('error', 'Events', error.data.message);
            });
        };
        $scope.deleteRegistration = function(registration_id) {
            var data = {
                id: registration_id
            };
            $http({
                method: 'POST',
                url: '/event/registration/delete',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllEventDistrictRegistrations();
            }, function errorCallback(error) {
                toaster.pop('error', 'Events', error.data.message);
            });
        };
        $scope.editRegistration = function(registration_id) {
            $state.go('districts.event_registration_edit', {
                registration_id: registration_id
            });
        };
    }
]);

app.controller('districtNewEventRegistrationController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.events = [];
        $scope.getAllEventDistrictForRegistration = function() {
            var data = {
                district_id: $cookieStore.get('loggedInDistrict').id
            };
            $http({
                method: 'POST',
                url: '/event/getAllDistrictEvents',
                data: data
            }).then(function successCallback(response) {
                $scope.events = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Events', error.message);
            });
        };
        $scope.createRegistration = function() {
            var data = $scope.registration;
            $http({
                method: 'POST',
                url: '/event/registration/register',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Registration', "successfull.");
                $state.go('districts.event_registration_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'Events', error.message);
            });
        };
        $scope.cancelRegistration = function() {
            $state.go('districts.event_registration_list');
        };
    }
]);

app.controller('districtEditEventRegistrationController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.cancelRegistration = function() {
            $state.go('districts.event_registration_list');
        };
        $scope.getEventRegistration = function() {
            var data = {
                district_id: $cookieStore.get('loggedInDistrict').id
            };
            $http({
                method: 'POST',
                url: '/event/getAllDistrictEvents',
                data: data
            }).then(function successCallback(response) {
                $scope.events = response.data;
                $http({
                    method: 'GET',
                    url: '/event/registration/edit?id=' + $state.params.registration_id
                }).then(function successCallback(response) {
                    $scope.registration = response.data;
                }, function errorCallback(error) {
                    toaster.pop('error', 'Event Registration', error.data.message);
                });
            }, function errorCallback(error) {
                toaster.pop('error', 'Events', error.message);
            });
        };
        $scope.editSaveRegistration = function() {
            var data = $scope.registration;
            $http({
                method: 'POST',
                url: '/event/registration/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Event Registration', "Updated successfully.");
            }, function errorCallback(error) {
                toaster.pop('error', 'Event Registration', error.data.message);
            });
        };
    }
]);
