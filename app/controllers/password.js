var bCrypt = require('bcrypt-nodejs');
var db = require('orm').db,
    User = db.models.users,
    Staff = db.models.staff,
    Admission = db.models.admissions,
    Parent = db.models.parents;
var isValidPassword = function(user, password) {
    return bCrypt.compareSync(password, user.password);
};

var createHash = function(password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
};


exports.userChangePassword = function(req, res) {
    if (typeof req.body.id != 'undefined' && req.body.id !== "") {
        User.find({
            id: req.body.id
        }, 1, function(err, users) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            if (!users.length) {
                return sendError(req, res, 404, "User not found");
            } else {
                user = users[0];
                if (!isValidPassword(user, req.body.oldPassword)) {
                    return sendError(req, res, 422, "You have entered wrong password!");
                } else if (req.body.newPassword != req.body.confirmNewPassword) {
                    return sendError(req, res, 422, "new password and confirm new password must be same");
                }
                user.password = createHash(req.body.newPassword);
                user.save(function(err, saved_user) {
                    if (err) {
                        return sendError(req, res, 500, "Something went wrong!Please try again");
                    }
                    return res.send(saved_user);
                });
            }
        });
    } else {
        return sendError(req, res, 404, "ID is required");
    }
};

exports.staffChangePassword = function(req, res) {
    if (typeof req.body.id != 'undefined' && req.body.id !== "") {
        Staff.find({
            id: req.body.id
        }, 1, function(err, staffs) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            if (!staffs.length) {
                return sendError(req, res, 404, "Staff not found");
            } else {
                staff = staffs[0];
                if (!isValidPassword(staff, req.body.oldPassword)) {
                    return sendError(req, res, 422, "You have entered wrong password!");
                } else if (req.body.newPassword != req.body.confirmNewPassword) {
                    return sendError(req, res, 422, "new password and confirm new password must be same");
                }
                staff.password = createHash(req.body.newPassword);
                staff.save(function(err, saved_staff) {
                    if (err) {
                        return sendError(req, res, 500, "Something went wrong!Please try again");
                    }
                    return res.send(saved_staff);
                });
            }
        });
    } else {
        return sendError(req, res, 404, "ID is required");
    }
};

exports.studentChangePassword = function(req, res) {
    if (typeof req.body.id != 'undefined' && req.body.id !== "") {
        Admission.find({
            id: req.body.id
        }, 1, function(err, students) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            if (!students.length) {
                return sendError(req, res, 404, "Student not found");
            } else {
                student = students[0];
                if (!isValidPassword(student, req.body.oldPassword)) {
                    return sendError(req, res, 422, "You have entered wrong password!");
                } else if (req.body.newPassword != req.body.confirmNewPassword) {
                    return sendError(req, res, 422, "new password and confirm new password must be same");
                }
                student.password = createHash(req.body.newPassword);
                student.save(function(err, saved_student) {
                    if (err) {
                        return sendError(req, res, 500, "Something went wrong!Please try again");
                    }
                    return res.send(saved_student);
                });
            }
        });
    } else {
        return sendError(req, res, 404, "ID is required");
    }
};

exports.parentChangePassword = function(req, res) {
    if (typeof req.body.id != 'undefined' && req.body.id !== "") {
        Parent.find({
            id: req.body.id
        }, 1, function(err, parents) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            if (!parents.length) {
                return sendError(req, res, 404, "Student not found");
            } else {
                parent = parents[0];
                if (!isValidPassword(parent, req.body.oldPassword)) {
                    return sendError(req, res, 422, "You have entered wrong password!");
                } else if (req.body.newPassword != req.body.confirmNewPassword) {
                    return sendError(req, res, 422, "new password and confirm new password must be same");
                }
                parent.password = createHash(req.body.newPassword);
                parent.save(function(err, saved_parent) {
                    if (err) {
                        return sendError(req, res, 500, "Something went wrong!Please try again");
                    }
                    return res.send(saved_parent);
                });
            }
        });
    } else {
        return sendError(req, res, 404, "ID is required");
    }
};
