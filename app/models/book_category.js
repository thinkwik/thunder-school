var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var BookCategory = db.define('bookcategory', {
  category_name: String,
}, {
    timestamp: true,
    methods: {
    }
});
