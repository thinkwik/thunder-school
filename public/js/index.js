'use strict';
var app = angular.module('app', ['ui.router', 'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngStorage',
    'ui.router',
    'ui.bootstrap',
    'ui.load',
    'ui.validate',
    'pascalprecht.translate',
    'toaster',
    'angularTrix',
    'ngCookies',
    '720kb.datepicker',
    'ngFileUpload',
    'ngTagsInput',
    'ui.calendar',
    'chart.js',
    'pdfjsViewer',
    // 'ui.jq',
    // 'ui.bootstrap.datetimepicker',
    'timer',
    'datatables'
]);

var app = angular.module('app').config(
    ['$controllerProvider', '$provide',
        function($controllerProvider) {
            app.controller = $controllerProvider.register;
        }
    ]);

// app.run(function(DTDefaultOptions,DTOptionsBuilder) {
//     // Display 25 items per page by default
//     DTDefaultOptions.setDisplayLength(25);
// });

app.run(function($rootScope, $http, $state) {
    // Page change - specific settings
    $rootScope.$on("$stateChangeSuccess", function(event, currentRoute, previousRoute) {
        //setting for background image if current state is "signin" then it will add image else not
        if ($state.current.name == 'signin' || $state.current.name == 'signout' || $state.current.name == 'forgotpwd' || $state.current.name == 'staffSignin' || $state.current.name == 'studentSignin' || $state.current.name == 'studentSignin' || $state.current.name == 'parentSignin' || $state.current.name == 'staffSignout' || $state.current.name == 'studentSignout' || $state.current.name == 'parentSignout' || $state.current.name == 'resetPassword') {
            $('body').css({
                "background-image": "url(img/Socrates_Logo.png)",
                "background-attachment": "fixed",
                "background-size": "100% 100%"
            });
        } else {
            $('body').css({
                "background-image": "",
                "background-attachment": "fixed",
                "background-size": "100% 100%"
            });
        }
        Loading(true);
        setTimeout(function(){
            Loading(false);
        },1000);
    });
});

app.directive('convertToNumber', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function(val) {
                return val ? parseInt(val, 10) : null;
            });
            ngModel.$formatters.push(function(val) {
                return val ? '' + val : null;
            });
        }
    };
});

app.directive('onErrorSrc', function() {
    return {
        link: function(scope, element, attrs) {
            element.bind('error', function() {
                if (attrs.src != attrs.onErrorSrc) {
                    attrs.$set('src', attrs.onErrorSrc);
                }
            });
        }
    };
});

app.filter('capitalize', function() {
    return function(input) {
        return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    };
});

app.factory('DownloadReport', function ($http, toaster, Upload, $window) {
        var root = {};
        root.download = function(file_name) {
            if (file_name) {
                var url = $window.location.origin + '/upload/generatedReports/' + file_name;
                var request = new XMLHttpRequest();
                request.open('HEAD', url, false);
                request.send();
                if (request.status == 200) {
                    var element = angular.element('<a/>');
                    element.attr({
                        href: url,
                        target: '_blank',
                        download: file_name
                    })[0].click();
                } else {
                    toaster.pop('error', 'Report', 'Report not found on server!');
                }
            } else {
                toaster.pop('error', 'Report', 'No Report Generated!');
            }
        };
        return root;
});

app.factory('videoFactory', [function() {
    var username, token, chatroomname;
    return ({
        setDetails: function(username_, token_, chatroomname_) {
            if (username_) username = username_;
            if (token_) token = token_;
            if (chatroomname_) chatroomname = chatroomname_;
        },
        getDetails: function() {
            return ({
                username: username,
                token: token,
                chatroomname: chatroomname
            });
        }
    });
}]);

app.filter('trusted', ['$sce', function($sce) {
    return function(url) {
        return $sce.trustAsResourceUrl(url);
    };
}]);

app.directive('prettyp', function() {
    return function(scope, element, attrs) {
        $("[rel^='prettyPhoto']").prettyPhoto({
            deeplinking: false,
            social_tools: false
        });
    }
})

app.factory('socket', ['$rootScope', function($rootScope) {
    var socket;
    return {
        on: function(eventName, callback) {
            socket.on(eventName, function() {
                var args = arguments;
                $rootScope.$apply(function() {
                    callback.apply(socket, args);
                });
            });
        },
        emit: function(eventName, data, callback) {
            socket.emit(eventName, data, function() {
                var args = arguments;
                $rootScope.$apply(function() {
                    if (callback) {
                        callback.apply(socket, args);
                    }
                });
            });
        },
        connect: function(username, token, roomname) {
            if (socket) {
                socket.disconnect();
            } //connection exists, disconnect previous!
            socket = io.connect('https://' + location.hostname + ':3001', {
                query: "username=" + username + "&token=" + token + "&roomname=" + roomname,
                'reconnection': false
            });
        },
        stunservers: function() {
            var configuration = {
                'iceServers': [{
                    url: 'stun:stun01.sipphone.com'
                }, {
                    url: 'stun:stun.ekiga.net'
                }, {
                    url: 'stun:stun.fwdnet.net'
                }, {
                    url: 'stun:stun.ideasip.com'
                }, {
                    url: 'stun:stun.iptel.org'
                }, {
                    url: 'stun:stun.rixtelecom.se'
                }, {
                    url: 'stun:stun.schlund.de'
                }, {
                    url: 'stun:stun.l.google.com:19302'
                }, {
                    url: 'stun:stun1.l.google.com:19302'
                }, {
                    url: 'stun:stun2.l.google.com:19302'
                }, {
                    url: 'stun:stun3.l.google.com:19302'
                }, {
                    url: 'stun:stun4.l.google.com:19302'
                }, {
                    url: 'stun:stunserver.org'
                }, {
                    url: 'stun:stun.softjoys.com'
                }, {
                    url: 'stun:stun.voiparound.com'
                }, {
                    url: 'stun:stun.voipbuster.com'
                }, {
                    url: 'stun:stun.voipstunt.com'
                }, {
                    url: 'stun:stun.voxgratia.org'
                }, {
                    url: 'stun:stun.xten.com'
                }]
            };
            return configuration;
        }
    };
}]);



/*
Error Codes
200 OK
401 email already exists
403 Forbidden (not authorized)
404 Not found
422 Required parameters not full-filled
444 user Not loggedin
500 server error
*/
