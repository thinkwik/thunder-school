var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Materials = db.define('materials', {
    school_id: {
        type: 'integer'
    },
    staff_id: {
        type: 'integer'
    },
    grade_id: {
        type: 'integer'
    },
    subject_id: {
        type: 'integer'
    },
    title: String,
    description: String,
    due_date: String,
    file_name: String,
    cover_pic: String,
    publisher: String,
    isbn_number: String,
    author: String
}, {
    timestamp: true,
    methods: {

    }
});
