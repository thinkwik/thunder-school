var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Child_Menus = db.define('child_menus', {
    parent_menu_id: {
        type: 'integer'
    },
    title: String,
    position: String,
    status: ['0', '1']
}, {
    timestamp: true,
    methods: {

    }
});
