var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var StudentClasses = db.define('student_classes', {
    class_id: {
        type: 'integer'
    },
    student_id: {
        type: 'integer'
    },
    grade_id: {
        type: 'integer'
    }
}, {
    timestamp: true,
    methods: {

    }
});
