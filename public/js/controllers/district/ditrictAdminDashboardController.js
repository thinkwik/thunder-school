app.controller('districtAdminDashboardController', ['$scope', '$http', '$state','toaster','$cookieStore','$compile',
    function($scope, $http, $state, toaster, $cookieStore, $compile) {
        $scope.todo_lists = [];
        $scope.redirectToSchoolCreate = function(){
            $state.go('districts.district_school_create');
        };
        $scope.schoolsEnrolledRedirect = function(){
            $state.go('districts.district_school_list');  
        };
        $scope.departmentEnrolledRedirect = function(){
            $state.go('districts.schooldepartment_list');
        }
        $scope.getAllDistrictAdminDashboardData = function(){
            $scope.district_total_registered_schools();
            $scope.district_total_students();
            $scope.district_schools_lists();
            $scope.district_events_lists();
            $scope.district_total_departments();
            $scope.district_events_lists_calendar();
            $scope.rander_chart();
        };
        $scope.district_total_registered_schools = function(){
        	var data ={
        		district_id:$cookieStore.get('loggedInDistrict').id
        	};
            $http({
                method: 'POST',
                url: '/dashboard/district/total_registered_schools',
                data:data
            }).then(function successCallback(response) {
                $scope.total_registered_schools = (response.data[0].total_registered_schools !== undefined) ? response.data[0].total_registered_schools : "0";
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashboard', 'Something went wrong! getting Dashobard data');
            });
        };
        $scope.district_total_students = function(){
        	var data ={
        		district_id:$cookieStore.get('loggedInDistrict').id
        	};
            $http({
                method: 'POST',
                url: '/dashboard/district/total_students',
                data:data
            }).then(function successCallback(response) {
              $scope.total_students = (response.data[0].total_students !== undefined) ? response.data[0].total_students : "0";
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong! getting Dashobard data');
            });
        };
        $scope.district_total_departments = function(){
        	var data ={
        		district_id:$cookieStore.get('loggedInDistrict').id
        	};
            $http({
                method: 'POST',
                url: '/dashboard/district/total_departments',
                data:data
            }).then(function successCallback(response) {
              $scope.total_departments = (response.data[0].total_departments !== undefined) ? response.data[0].total_departments : "0";
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong! getting Dashobard data');
            });
        };
        $scope.district_schools_lists = function(){
        	var data ={
        		district_id:$cookieStore.get('loggedInDistrict').id
        	};
            $http({
                method: 'POST',
                url: '/dashboard/district/schools_lists',
                data:data
            }).then(function successCallback(response) {
               $scope.schools_lists = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong! getting Dashobard data');
            });
        };
        $scope.district_events_lists = function(){
        	var data ={
        		district_id:$cookieStore.get('loggedInDistrict').id
        	};
            $http({
                method: 'POST',
                url: '/dashboard/district/events_lists',
                data:data
            }).then(function successCallback(response) {
               $scope.events_lists = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong! getting Dashobard data');
            });
        };
        $scope.payment_received = function(){

        };
        $scope.dashboardEditEvent = function(event_id){
            $state.go('districts.event_edit',{
                event_id:event_id
            });
        };


        //calendar options and config

        $scope.uiConfig = {
          calendar:{
            height: 450,
            editable: false,
            header:{
              //left: 'month basicWeek basicDay agendaWeek agendaDay',
              left: 'month basicWeek',
              center: 'title',
              right: 'today prev,next'
            },
            eventClick: $scope.alertEventOnClick,
            eventDrop: $scope.alertOnDrop,
            eventResize: $scope.alertOnResize,
            eventRender: $scope.eventRender
          }
        };

        $scope.events = [];
        $scope.eventSources = [$scope.events];
        
        $scope.district_events_lists_calendar = function(){
            var data ={
                district_id:$cookieStore.get('loggedInDistrict').id
            };
            $http({
                method: 'POST',
                url: '/dashboard/district/events_lists/calendar',
                data:data
            }).then(function successCallback(response) {
               angular.forEach(response.data,function(value, key){
                $scope.events.push({
                    title: value.title,
                    description: (typeof value.content != 'undefined' ) ? value.content.substring(17, 125).replace("</div>", "")+"..." : "No Description",
                    start: value.start_date,
                    end: value.end_date,
                    stick:true
                });
               });
               $scope.add_my_todos();
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong! getting Dashobard data');
            });
        };

        $scope.eventRender = function( event, element, view ) { 
            element.attr({
                'data-toggle' : 'tooltip',
                'title' : event.description
            });
            element.tooltip();
            $compile(element)($scope);
        };

        $scope.add_my_todos = function(){
            var data = {};
            data.user_id = $cookieStore.get('loggedInDistrict').authorized_user_id;
            data.type = "admin";
            $http({
                method: 'POST',
                url: '/todo/getMyTodos',
                data:data
            }).then(function successCallback(response) {
                $scope.todo_lists = response.data;
               angular.forEach(response.data,function(value, key){
                $scope.events.push({
                    title: value.description,
                    description: 'To-Do Task',
                    start: new Date(value.date),
                    allDay: true,
                    color: (value.status == "1") ? "#27c24c" : "#ee3939",
                    stick:true
                });
               });
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong! getting Dashobard data');
            });
        };

        //charts option and config
        $scope.chart_datasetOverride = [{
            label: "Total Enrolled Student",
            borderWidth: 3,
            hoverBackgroundColor: "rgba(255,99,132,0.4)",
            hoverBorderColor: "rgba(255,99,132,1)",
            type: 'bar'
        }];
        var today = new Date();
        $scope.chart_labels = [];
        for (var i = 0; i < 7; i++) {
            $scope.chart_labels.push(today.toISOString().slice(0, 10));
            today.setDate(today.getDate() - 1);
        }
        $scope.chart_data = [];
        $scope.rander_chart = function() {
            var data ={
                district_id:$cookieStore.get('loggedInDistrict').id
            };
            $http({
                method: 'POST',
                url: '/dashboard/district/chart_data',
                data:data
            }).then(function successCallback(response) {
                var row = [];

                angular.forEach($scope.chart_labels, function(value, key) {
                    var keepGoing = true;
                    var last_val = 0;
                    angular.forEach(response.data, function(res_value, res_key) {
                        if (keepGoing && value == res_value.date.slice(0, 10)) {
                            last_val = parseInt(res_value.count);
                            keepGoing = false;
                        }
                    });
                    row.push(last_val);
                });
                $scope.chart_data.push(row);
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong! getting Dashobard data');
            });
        };

    }

]);