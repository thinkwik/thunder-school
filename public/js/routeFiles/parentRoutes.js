app.config(function($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider.otherwise("/parent/login");
    //Manage Parents Login
    $stateProvider.state('parentSignin', {
            url: "/parent/login",
            templateUrl: "/partials/auth/parentSignin.html",
            controller: 'parentSigninFormController'
        })
        //Manage Profile
        .state('parents', {
            url: '/parent',
            templateUrl: '/partials/profile/parents.html',
            abstract: true,
            controller: "protectParentRoutesController"
        })
        .state('parents.changeParentPassword', {
            url: '/parent/changepassword',
            templateUrl: '/partials/parents/changePassword.html'
        })
        .state('parents.edit_profile', {
            url: '/edit',
            templateUrl: '/partials/parents/editProfile.html',
            controller: 'editParentsProfileController'
        })
        //Manage Dashboard
        .state('parents.dashboard', {
            url: '/dashboard',
            templateUrl: '/partials/parents/manageDashboard.html'
        })
        //Manage Video Conference
        .state('parents.video_conference', {
            url: '/video_conference/create',
            templateUrl: '/partials/superAdmin/district/manageParents/manageVideoConference/createConference.html',
            controller: 'parentCreateVideoConferenceController'
        })
        .state('parents.video_conference_live', {
            url: '/video_conference/live',
            templateUrl: '/partials/superAdmin/district/manageParents/manageVideoConference/groupCall.html'
                //controller: 'videoConferenceLiveController'
        })
        //Manage TO-DO
        .state('parents.todo_create', {
            url: '/todo/create',
            templateUrl: '/partials/superAdmin/district/manageParents/manageTodo/createTodo.html',
            controller: 'studentTodoCreateController'
        })
        .state('parents.todo_list', {
            url: '/todo/list',
            templateUrl: '/partials/superAdmin/district/manageParents/manageTodo/listTodo.html',
            controller: 'studentTodoListController'
        })
        // Manage Attendance
        .state('parents.attendance_list', {
            url: '/attendance/list',
            templateUrl: '/partials/superAdmin/district/manageParents/manageAttendance/listAttendance.html',
            controller: 'studentAttendenceListController'
        })
        .state('parents.view_staff', {
            url: '/staff/list',
            templateUrl: '/partials/superAdmin/district/manageParents/viewStaffList/parentViewStaffList.html',
            controller: 'parentStaffListController'
        })

    //Manage Student Homework
    .state('parents.list_homework', {
            url: '/list/homework',
            templateUrl: '/partials/superAdmin/district/manageParents/manageHomework/listHomework.html',
            controller: 'showStudentHomeworkToParentsController'
        })
        // Manage Issues
        .state('parents.issues_create', {
            url: '/issues/create',
            templateUrl: '/partials/superAdmin/district/manageParents/manageIssues/createIssues.html',
            controller: 'createParentIssuesController'
        })
        .state('parents.issues_list', {
            url: '/issues/list',
            templateUrl: '/partials/superAdmin/district/manageParents/manageIssues/listIssues.html',
            controller: 'listParentIssuesController'
        })
        .state('parents.issues_edit', {
            url: '/issues/:issue_id',
            templateUrl: '/partials/superAdmin/district/manageParents/manageIssues/editIssues.html',
            controller: 'editParentIssuesController'
        })
        .state('parents.issues_view', {
            url: '/issues/view/:issue_id',
            templateUrl: '/partials/superAdmin/district/manageParents/manageIssues/viewIssue.html',
            controller: 'viewParentIssuesController'
        })
        // Manage Materials
        .state('parents.download_material', {
            url: '/material/download',
            templateUrl: '/partials/superAdmin/district/manageParents/managematerial/download_material.html',
            controller: 'downloadParentMaterialController'
        })
        // Manage Assignments
        .state('parents.download_assignment', {
            url: '/assignment/download',
            templateUrl: '/partials/superAdmin/district/manageParents/manageAssignments/downloadAssignment.html',
            controller: 'downloadParentAssignmentController'
        })
        // Manage Student Details
        .state('parents.view_studentDetails', {
            url: '/view/studentdetails',
            templateUrl: '/partials/superAdmin/district/manageParents/viewStudentDeatil/viewStudentDeatil.html',
            controller: 'viewStudentDetailsController'
        })
        //Manage Emails
        .state('parents.email_list', {
            url: '/email/list',
            templateUrl: '/partials/superAdmin/district/manageParents/manageEmails/manageEmails.html',
            controller: 'listParentEmailController'
        })
        .state('parents.email_view', {
            url: '/email/view/:mail_id',
            templateUrl: '/partials/superAdmin/district/manageParents/manageEmails/viewEmails.html',
            controller: 'viewParentEmailController'
        })
        // Manage Events
        .state('parents.event_list', {
            url: '/event/list',
            templateUrl: '/partials/superAdmin/district/manageParents/manageEvents/listEvent.html',
            controller: 'listParentsEventsController'
        })
        .state('parents.event_gallery_view', {
            url: '/gallery/view/:gallery_id',
            templateUrl: '/partials/superAdmin/district/manageParents/manageEvents/eventGallery.html',
            controller: 'viewEventsGalleryAlbumController'
        })
        // Manage Appointment
        .state('parents.create_appointment', {
            url: '/create/appointment',
            templateUrl: '/partials/superAdmin/district/manageParents/manageAppointments/createAppointment.html',
            controller: 'createParentAppointmentController'
        })
        .state('parents.edit_appointment', {
            url: '/edit/appointment/:appointment_id',
            templateUrl: '/partials/superAdmin/district/manageParents/manageAppointments/editAppointment.html',
            controller: 'editParentAppointmentController'
        })
        .state('parents.list_appointment', {
            url: '/list/appointment',
            templateUrl: '/partials/superAdmin/district/manageParents/manageAppointments/listAppointment.html',
            controller: 'listParentAppointmentController'
        })
        // Manage Exams
        .state('parents.exam_list', {
            url: '/exam/list',
            templateUrl: '/partials/superAdmin/district/manageParents/manageExams/listExams.html',
            controller: 'parentsExamListController'
        })
        .state('parents.view_exam_report', {
            url: '/exam/report_view/:exam_id/:student_id',
            templateUrl: '/partials/superAdmin/district/manageParents/manageExams/reportExam.html',
            controller: 'parentExamReportController'
        })
        //Manage Exam Results
        .state('parents.exam_result_list', {
            url: '/exam_result/list',
            templateUrl: '/partials/superAdmin/district/manageParents/manageExams/listExamResults.html',
            controller: 'parentsResultListController'
        })
        // Manage Notices
        .state('parents.notices_list', {
            url: '/notices/list',
            templateUrl: '/partials/superAdmin/district/manageParents/manageNotices/listNotices.html',
            controller: 'listParentsNoticesController'
        })

        // Manage E-Books
        .state('parents.list_books', {
            url: '/books/list',
            templateUrl: '/partials/superAdmin/district/manageParents/manageBooks/listBooks.html',
            controller: 'listParentsBooksController'
        })
        .state('parents.view_books', {
            url: '/books/view/:book_id',
            templateUrl: '/partials/superAdmin/district/manageParents/manageBooks/viewBooks.html',
            controller: 'viewParentsBooksController'
        })
        .state('parents.view_epub_book', {
           url: '/bookstore/epub/view/:book_id',
           templateUrl: '/partials/superAdmin/district/manageParents/manageBooks/viewEpubBook.html',
           controller: 'viewParentEpubBookStoreController'
       })
        //Manage Parents Logout
        .state('parentSignout', {
            url: "/parent/signout",
            templateUrl: "/partials/auth/signout/parentSignout.html",
            controller: 'parentLogOutController'
        });

    $locationProvider.html5Mode(true);
});
