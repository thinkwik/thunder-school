app.controller('schoolCreateMedicalInfoController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        $scope.cancelMedicalInfoCreate = function() {
            $state.go('schools.medical_info_list');
        };

        $scope.createMedicalInfo = function() {
            data = $scope.info;
            data.school_id = $cookieStore.get('loggedInSchool').id;
            $http({
                method: "POST",
                url: "/medical_info/register",
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Medical Info', 'Saved Successfully.');
                $scope.cancelMedicalInfoCreate();
            }, function errorCallback(error) {
                toaster.pop('error', 'Medical Info', error.data.message);
            });
        };
    }
]);

app.controller('schoolListMedicalInfoController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.addNewMedicalInfo = function() {
            $state.go('schools.medical_info_create');
        };

        $scope.deleteInfo = function(id) {
            var data = {
                id: id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/medical_info/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllInfo();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Medical Info', error.data.message);
                });
            }
        };

        $scope.getAllInfo = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/medical_info/getAllMedicalInfos',
                data: data
            }).then(function successCallback(response) {
                $scope.infos = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Medical Info', 'cannot get all the Staff! try reloading page.');
            });
        };
        $scope.getAllInfo();
        $scope.editInfo = function(id) {
            $state.go('schools.medical_info_edit', {
                info_id: id
            });
        };
    }
]);

app.controller('schoolEditMedicalInfoController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        $scope.getEditMedicalInfo = function() {
            $http({
                method: 'GET',
                url: '/medical_info/edit?id=' + $state.params.info_id
            }).then(function successCallback(response) {
                $scope.info = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Medical Info', 'cannot get Info! try reloading page.');
            });
        };
        $scope.getEditMedicalInfo();

        $scope.saveEditMedicalInfo = function() {
            data = $scope.info;
            $http({
                method: 'POST',
                url: '/medical_info/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Medical Info', 'Saved Successfully.');
                $state.go('schools.medical_info_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'Medical Info', error.data.message);
            });
        };
        $scope.cancelInfoForm = function() {
            $state.go('schools.medical_info_list');
        };
    }
]);


//
