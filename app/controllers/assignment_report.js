var db = require('orm').db,
    Assignments = db.models.assignments;
AssignmentReports = db.models.assignment_reports;

exports.completed = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'AssignmentReports id required!!');
    }
    AssignmentReports.find({
        id: req.body.id
    }, 1, function(err, reports) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        reports[0].status = (reports[0].status == "0") ? "1" : "0";
        reports[0].save(function(err, saved_report) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, 'AssignmentReports id required!');
            }
            return res.send(saved_report);
        });
    });
};

exports.notCompleted = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'AssignmentReports id required!!');
    }
    AssignmentReports.find({
        id: req.body.id
    }, 1, function(err, reports) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        reports[0].status = (reports[0].status == "1") ? "0" : "1";
        reports[0].save(function(err, saved_report) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, 'AssignmentReports id required!');
            }
            return res.send(saved_report);
        });
    });
};

exports.getAllStudentAssignmentList = function(req, res) {
    if (req.body.assignment_id == 'undefined') {
        return sendError(req, res, 422, "AssignmentReports ID required");
    }
    var query_str = 'SELECT t1.*, CONCAT(t2.first_name, " ", t2.last_name) as student_name FROM `assignment_reports` as t1 LEFT JOIN admissions as t2 ON t1.student_id = t2.id WHERE t1.assignment_id = ?';
    db.driver.execQuery(query_str, [req.body.assignment_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

var fs = require('fs');
exports.uploadStudentFile = function(req, res) {
    var fs = require('fs');
    var studentassignments = __dirname + "/../../public/upload/studentassignments/";
    if (!fs.existsSync(studentassignments)) {
        fs.mkdirSync(studentassignments);
    }
    var file = req.files.file;
    var fileExtension = file.name.split(".")[1];
    if (fileExtension) {
      fileName = file.path.split('/');
      fs.rename(file.path, studentassignments + fileName[fileName.length - 1] +"."+ fileExtension);
      return res.send(fileName[fileName.length - 1] +"."+ fileExtension);
    }
    else {
      return sendError(req, res, 400, "Bad Request check your parameters");
    }

};

exports.addStudentAssignment = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'AssignmentReports id required!!');
    }
    AssignmentReports.find({
        id: req.body.id
    }, function(err, submitted_assignment) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, 'AssignmentReports id required!');
        }
        if (submitted_assignment.length === 0) {
            return sendError(req, res, 422, 'Assignment not found!');
        }
        submitted_assignment[0].file_path = req.body.student_submitted;
        submitted_assignment[0].save(function(err, saved_assignment) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, 'AssignmentReports id required!');
            }
            return res.send(saved_assignment);
        });
    });
};

exports.updateGrades = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'AssignmentReports id required!!');
    }
    AssignmentReports.find({
        id: req.body.id
    }, function(err, grades_achieved) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, 'AssignmentReports id required!');
        }
        if (grades_achieved.length === 0) {
            return sendError(req, res, 422, 'Assignment not found!');
        }
        grades_achieved[0].achieved_grade = req.body.achieved_grade;
        grades_achieved[0].save(function(err, saved_grades) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, 'AssignmentReports id required!');
            }
            return res.send(saved_grades);
        });
    });
};
