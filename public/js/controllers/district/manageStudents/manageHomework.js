app.controller('listStudentHomeworkController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', '$window', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, $window, Upload) {
      $scope.todaysDate = new Date();

        $scope.getMyHomework = function() {
            var data = {
                student_id: $cookieStore.get("loggedInStudent").id
            };
            $http({
                method: 'POST',
                url: '/homework/getHomeworkStudentWise',
                data: data
            }).then(function successCallback(response,status,headers,config,statusText) {
              $scope.homeworks = response.data;
            }, function errorCallback(error,status,headers,config,statusText) {
              toaster.pop('error', 'HomeWork', 'Something went wrong!');
            });
        };

        $scope.DownloadHomework = function(homework) {
            if(homework.file_path) {
              var homewrokFileName = homework.file_path;
              homewrokFileName = homewrokFileName.split(".");
                var url =  $window.location.origin + '/upload/studenthomeworks/' + homework.file_path;
                var request = new XMLHttpRequest();
                request.open('HEAD', url, false);
                request.send();
                if(request.status == 200) {
                  var element = angular.element('<a/>');
                  element.attr({
                      href: url,
                      target: '_blank',
                      download: homework.title + '.' + homewrokFileName[1]
                  })[0].click();
                } else {
                  toaster.pop('error', 'HomeWork', 'HomeWork not found!');
                  return false;
                }
            } else {
              toaster.pop('error', 'HomeWork', 'HomeWork not found!');
              return false;
            }
        };

        $scope.submitHomework = function(homework) {
            homework_id = homework.id;
            $state.go('students.homework_reports', {
                homework_id: homework_id
            });
        };

    }
]);


app.controller('studentHomeworkReportsController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', '$window', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, $window, Upload) {
        $scope.todaysDate = new Date();
        $scope.submitHomework = function(homework) {
            var data = {
                id: $state.params.homework_id,
                student_submitted: $scope.filename
            };
            $http({
                method: 'POST',
                url: '/homework/submission',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Homework', 'Submitted successfully!');
                $state.go('students.list_homework');
            }, function errorCallback(error) {
                toaster.pop('error', 'Homework', 'Something went wrong!');
            });
        };

        $scope.listHomework = function() {
            $state.go('students.list_homework');
        };

        $scope.getHomework = function() {
            var data = {
                id: $state.params.homework_id,
                student_id: $cookieStore.get("loggedInStudent").id
            };
            $http({
                method: 'POST',
                url: '/homework/getStudentsHomework ',
                data: data
            }).then(function successCallback(response) {
                $scope.homework = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Homework Report', 'Something went wrong!');
            });
        };

        $scope.uploadFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected !');
            Loading(true);
            Upload.upload({
                url: '/homework/upload/studentHomework',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.filename = success.data;
                    $scope.saveDisable = true;
                },
                function(error) {
                    toaster.pop('error', "Error", error.data.message);
                    $scope.saveDisable = false;
                });
        };
    }
]);
