var bCrypt = require('bcrypt-nodejs');
var db = require('orm').db,
    StaffRole = db.models.staff_roles;


exports.register = function(req, res) {
    if (typeof req.body.school_id == 'undefined') {
        return sendError(req, res, 422, 'required parameter not fullfiled for register new resource');
    }
    var new_school_role = new StaffRole();
    new_school_role.role_name = req.body.role_name;
    new_school_role.school_id = req.body.school_id;
    new_school_role.description = req.body.description;
    new_school_role.modules = req.body.modules;
    new_school_role.status = (typeof req.body.status != 'undefined') ? req.body.status : "1";
    new_school_role.save(function(err, saved_role) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        res.send(saved_role);
    });
};

exports.getStaffRole = function(req, res) {
    if (typeof req.query.id == 'undefined') {
        return sendError(req, res, 422, 'role id required');
    }
    StaffRole.find({
        id: req.query.id
    }, 1, function(err, roles) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!roles.length) {
            return sendError(req, res, 404, "role not found");
        }
        return res.send(roles[0]);
    });
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'role id required');
    }
    console.log(req.body);
    StaffRole.find({
        id: req.body.id
    }, 1, function(err, roles) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!roles.length) {
            return sendError(req, res, 404, "role not found");
        }
        roles[0].role_name = req.body.role_name;
        roles[0].school_id = (typeof req.body.school_id != 'undefined') ? req.body.school_id : roles[0].school_id;
        roles[0].description = req.body.description;
        roles[0].modules = req.body.modules;
        roles[0].status = (typeof req.body.status != 'undefined') ? req.body.status : roles[0].status;
        roles[0].save(function(err, updated_role) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            return res.send(updated_role);
        });
    });
};

exports.active = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'role id required');
    }
    StaffRole.find({
        id: req.body.id
    }, 1, function(err, roles) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        roles[0].status = "1";
        roles[0].save(function(err, activated_role) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "something went wrong in activating role");
            }
            return res.send(activated_role);
        });
    });
};

exports.inactive = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'role id required');
    }
    StaffRole.find({
        id: req.body.id
    }, 1, function(err, roles) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        roles[0].status = "0";
        roles[0].save(function(err, activated_role) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "something went wrong in inactivating role");
            }
            return res.send(activated_role);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'role id required');
    }
    StaffRole.find({
        id: req.body.id
    }, 1, function(err, roles) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!roles.length) {
            return sendError(req, res, 404, "role not found");
        }
        roles[0].remove(function(err) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "Something went wrong in deleting role");
            }
            return res.send(roles[0]);
        });
    });
};

exports.getAllStaffRoleDistrict = function(req, res) {
    if (typeof req.body.district_id == 'undefined' || req.body.district_id === null) {
        return sendError(req, res, 422, "district id required");
    }
    var query_str = "SELECT t1.*, t2.school_name FROM `staff_roles` as t1 LEFT JOIN schools as t2 ON t1.school_id = t2.id WHERE t2.parent_id = ?";
    db.driver.execQuery(query_str, [req.body.district_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getAllStaffRoleSchool = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id === null) {
        return sendError(req, res, 422, "school id required");
    }
    var query_str = "SELECT t1.*, t2.school_name FROM `staff_roles` as t1 LEFT JOIN schools as t2 ON t1.school_id = t2.id WHERE t2.id = ?";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
