var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Exam = db.define('exams', {
    school_id: {
        type: 'integer'
    },
    session_id: {
        type: 'integer'
    },
    grade_id: {
        type: 'integer'
    },
    class_id: {
        type: 'integer'
    },
    subject_id: {
        type: 'integer'
    },
    staff_id: {
        type: 'integer'
    },
    exam_head: String,
    max_marks: String,
    start_date: String,
    end_date: String,
    start_time: String,
    end_time: String
}, {
    timestamp: true,
    methods: {
    }
});
