var db = require('orm').db,
    Attendance = db.models.attendance,
    AttendanceReports = db.models.attendance_reports;

exports.register = function(req, res) {
    if (typeof req.body.staff_id == 'undefined' || typeof req.body.grade_id == 'undefined') {
        return sendError(req, res, 422, 'Id required!!');
    }
    newAttendance = new Attendance();
    newAttendance.school_id = req.body.school_id;
    newAttendance.staff_id = req.body.staff_id;
    newAttendance.grade_id = req.body.grade_id;
    newAttendance.subject_id = req.body.subject_id;
    newAttendance.class_id = req.body.class_id;
    newAttendance.date = req.body.date;
    newAttendance.description = req.body.description;
    newAttendance.save(function(err, created_Attendance) {
        if (err) {
            console.log(err);
            return sendError(req, res, 500, 'Something went wrong!');
        }
        var query_str = 'SELECT t1.id FROM admissions as t1  WHERE t1.grade_id = ?';
        db.driver.execQuery(query_str, [req.body.grade_id], function(err, all_students) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            if (all_students.length > 0) {
                all_students.forEach(function(value, key) {
                    var data = {
                        attendance_id: created_Attendance.id,
                        student_id: value.id,
                        status: "0"
                    };
                    AttendanceReports.create(data, function(err) {
                        if (err) {
                            console.log(err);
                            return sendError(req, res, 500, "Something went wrong!");
                        }
                    });
                });
                return res.send(created_Attendance);
            } else {
                return res.send(created_Attendance);
            }
        });
    });
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Attendance id required!!');
    }
    Attendance.find({
        id: req.body.id
    }, 1, function(err, attendance) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        attendance[0].school_id = req.body.school_id;
        attendance[0].staff_id = req.body.staff_id;
        attendance[0].grade_id = req.body.grade_id;
        attendance[0].subject_id = req.body.subject_id;
        attendance[0].class_id = req.body.class_id;
        attendance[0].date = req.body.date;
        attendance[0].description = req.body.description;
        attendance[0].save(function(err, updated_attendance) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(updated_attendance);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Attendance id required!!');
    }
    Attendance.find({
        id: req.body.id
    }, 1, function(err, attendance) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        attendance[0].remove(function(err) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(attendance[0]);
        });
    });
};

exports.getAttendance = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 401, 'Attendance id required!!');
    }
    Attendance.find({
        id: req.body.id
    }, 1, function(err, attendance) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, 'Something went wrong!');
        }
        if (!attendance.length) return sendError(req, res, 404, "Attendance not found");
        return res.send(attendance[0]);
    });
};

exports.getAttendanceStaffwise = function(req, res) {
    if (typeof req.body.staff_id == 'undefined' || req.body.staff_id === null) {
        return sendError(req, res, 422, "District ID required");
    }
    var query_str = 'SELECT t1.*,t2.grade,CONCAT(t3.first_name,t3.last_name) as staff_name,t6.subject_name FROM attendance as t1 LEFT JOIN grades as t2 ON t1.grade_id = t2.id LEFT JOIN subjects as t6 ON t1.subject_id = t6.id LEFT JOIN staff as t3 ON t1.staff_id = t3.id WHERE t3.id = ?';
    db.driver.execQuery(query_str, [req.body.staff_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getEditedAttendance = function(req, res) {
    if (typeof req.query.id == 'undefined') {
        return sendError(req, res, 401, 'Attendance id required!!');
    }
    Attendance.find({
        id: req.query.id
    }, 1, function(err, attendance) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, 'Something went wrong!');
        }
        if (!attendance.length) return sendError(req, res, 404, "Attendance not found");
        return res.send(attendance[0]);
    });
};
