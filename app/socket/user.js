
  var addUser, deleteUser, returnUser, userList;

  userList = {};

  addUser = function(user, socketid, io) {
    if(userList[user]){
      io.sockets.connected[userList[user].socket].disconnect();
      delete userList[user];
    }
    userList[user] = {
      socket: socketid
    };
    return true;
  };

  returnUser = function(user) {
    if (userList.user) {
      return userList.user;
    }
    return false;
  };

  deleteUser = function(socketid) {
    var key, value;
    for (key in userList) {
      value = userList[key];
      if (value.socket === socketid) {
        delete userList[key];
        console.log("Found existing user with id " + key);
        return key;
      }
    }
    console.log("Non existant user tried to disconnect!?");
    return false;
  };

  module.exports = {
    addUser: addUser,
    returnUser: returnUser,
    deleteUser: deleteUser
  };
