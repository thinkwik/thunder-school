app.controller('allPackageController', ['$scope', '$http', '$state', 'toaster',
    function($scope, $http, $state, toaster) {
        $scope.addNewPackage = function() {
            $state.go('users.package_create');
        };

        $scope.superAdminPackages = function() {
            var data = {
                offset: 'fdfdsfdfs'
            };
            $http({
                method: 'POST',
                url: '/package/getAllPackages',
                data: data
            }).then(function successCallback(response) {
                // console.log(response);
                var packages = response.data;
                $scope.packages = packages;
            }, function errorCallback(error) {
                toaster.pop('error', 'Packages', 'cannot get all the packages! try reloading page.');
                console.log(error.data.message);
            });
        };
        $scope.editPackage = function(package_id) {
            $state.go('users.packages_edit', {
                package_id: package_id
            });
        };
        $scope.inactivePackage = function(package_id) {
            var data = {
                id: package_id
            };
            $http({
                method: 'POST',
                url: '/package/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.superAdminPackages();
            }, function errorCallback(error) {
                console.log(error.data.message);
            });
        };
        $scope.activePackage = function(package_id) {
            var data = {
                id: package_id
            };
            $http({
                method: 'POST',
                url: '/package/active',
                data: data
            }).then(function successCallback(response) {
                $scope.superAdminPackages();
            }, function errorCallback(error) {
                console.log(error.data.message);
            });
        };
        $scope.deletePackage = function(package_id) {
            var data = {
                id: package_id
            };
            $http({
                method: 'POST',
                url: '/package/delete',
                data: data
            }).then(function successCallback(response) {
                $scope.superAdminPackages();
            }, function errorCallback(error) {
                console.log(error.data.message);
            });
        };

    }
]);

app.controller('addNewPackageController', ['$scope', '$http', '$state', 'toaster',
    function($scope, $http, $state, toaster) {
        $scope.cancelPackage = function() {
            $state.go('users.package_list');
        };
        $scope.addNewPackage = function() {
            var data = $scope.package;
            $http({
                method: 'POST',
                url: '/package/register',
                data: data
            }).then(function successCallback(response) {
                console.log(response);
                $state.go('users.package_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'Packages', 'cannot get all the packages! try reloading page.');
                console.log(error.data.message);
            });
        };
    }
]);

app.controller('editPackageController', ['$scope', '$http', '$state', 'toaster',
    function($scope, $http, $state, toaster) {
        $scope.cancelPackage = function() {
            $state.go('users.package_list');
        };
        $scope.getEditPackage = function() {
            console.log($state);
            console.log("dsfgdsfsfd" + $state.params.package_id);
            var data = $scope.package;
            $http({
                method: 'GET',
                url: '/package/edit?id=' + $state.params.package_id,
                data: data
            }).then(function successCallback(response) {
                console.log(response.data);
                $scope.package = response.data;
                //  $state.go('users.listpackage');
            }, function errorCallback(error) {
                toaster.pop('error', 'Packages', 'cannot get all the packages! try reloading page.');
                console.log(error.data.message);
            });
        };
        $scope.saveUpdatedPackage = function() {
            var data = $scope.package;
            $http({
                method: 'POST',
                url: '/package/edit',
                data: data
            }).then(function successCallback(response) {
                console.log(response.data);
                $scope.package = response.data;
                toaster.pop('success', 'Packages', 'Updated successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Packages', 'cannot get all the packages! try reloading page.');
                console.log(error.data.message);
            });
        };
    }
]);
