app.controller('viewStudentDetailsController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', '$window',
    function($scope, $http, $state, toaster, $cookieStore, $window) {
        $scope.logged_in_child = $cookieStore.get('loggedInParent').student_id;
        $scope.getChildList = function() {
            var data = {
                email: $cookieStore.get('loggedInParent').email
            };
            $http({
                method: 'POST',
                url: '/get/getAllChildren',
                data: data
            }).then(function successCallback(response) {
                $scope.all_childrens = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Children', 'cannot get details of Children ! try reloading page.');
                console.log(error.data.message);
            });
        };

        $scope.changeAnotherChild = function(student_id) {
            var data = {
                id: $cookieStore.get('loggedInParent').id,
                student_id:student_id
            };
            $http({
                method: 'POST',
                url: '/parents/changeChild',
                data: data
            }).then(function successCallback(response) {
                $cookieStore.remove('loggedInParent');
                $cookieStore.put('loggedInParent', response.data);
                toaster.pop('success', 'Children', 'Changed Child Details');
                $state.go('parents.dashboard');
            }, function errorCallback(error) {
                toaster.pop('error', 'Children', 'cannot get details of Children ! try reloading page.');
                console.log(error.data.message);
            });
        };
    }
]);
