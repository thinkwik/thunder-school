app.controller('listSchoolDeptController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.inactiveSchoolDept = function(schooldept_id) {
            var data = {
                id: schooldept_id
            };
            $http({
                method: 'POST',
                url: '/school/department/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllSchoolDepartment();
            }, function errorCallback(error) {
                toaster.pop('error', 'School Department', error.data.message);
            });
        };

        $scope.activeSchoolDept = function(schooldept_id) {
            var data = {
                id: schooldept_id
            };
            $http({
                method: 'POST',
                url: '/school/department/active',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllSchoolDepartment();
            }, function errorCallback(error) {
                toaster.pop('error', 'School Department', error.data.message);
            });
        };

        $scope.deleteSchoolDept = function(schooldept_id) {
            var data = {
                id: schooldept_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/school/department/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllSchoolDepartment();
                }, function errorCallback(error) {
                    toaster.pop('error', 'School Department', error.data.message);
                });
            }
        };

        $scope.getAllSchoolDepartment = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/school/department/getAllSchoolDepartment',
                data: data
            }).then(function successCallback(response) {
                $scope.schooldepts = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Department', 'cannot get all the Department! try reloading page.');
            });
        };
        $scope.editSchoolDept = function(department_id) {
            $state.go('schools.schooldepartment_edit', {
                department_id: department_id
            });
        };
        $scope.addNewDeptRedirect = function() {
            $state.go('schools.schooldepartment_create');
        };

    }
]);

app.controller('editSchoolDeptController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.getEditSchoolDept = function() {
            $http({
                method: 'GET',
                url: '/school/department/edit?id=' + $state.params.department_id
            }).then(function successCallback(response) {
                $scope.schooldept = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Category', 'cannot get all the Category! try reloading page.');
            });
        };


        $scope.saveEditSchoolDept = function() {
            data = $scope.schooldept;
            $http({
                method: 'POST',
                url: '/school/department/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Department', 'Saved Successfully.');
                $state.go('schools.schooldepartment_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'Department', error.data.message);
            });
        };

        $scope.cancelSchoolDeptEdit = function() {
            $state.go('schools.schooldepartment_list');
        };
    }
]);

app.controller('createSchoolDeptController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.createSchoolDept = function() {
            data = $scope.schooldept;
            data.school_id = $cookieStore.get('loggedInSchool').id;
            $http({
                method: "POST",
                url: "/school/department/register",
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'School Department', 'Saved Successfully.');
                $state.go('schools.schooldepartment_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'School Department', error.data.message);
            });
        };
        $scope.cancelSchoolDept = function() {
            $state.go('schools.schooldepartment_list');
        };
    }
]);
