var fs = require('fs');
var db = require('orm').db,
    Assignments = db.models.assignments,
    AssignmentReports = db.models.assignment_reports;

exports.register = function(req, res) {
    if (typeof req.body.class_id == 'undefined' || typeof req.body.school_id == 'undefined' || typeof req.body.subject_id == 'undefined' || typeof req.body.description == 'undefined' || typeof req.body.due_date == 'undefined') {
        return apiSendError(req, res, 422, 'Please check your parameters.');
    }
    if (typeof req.files.assignment_file == 'undefined') {
        return apiSendError(req, res, 422, 'Please add assignment file.');
    }
    newAssignments = new Assignments();
    newAssignments.school_id = req.body.school_id;
    newAssignments.staff_id = req.body.staff_id;
    newAssignments.subject_id = parseInt(req.body.subject_id);
    newAssignments.grade_id = req.body.grade_id;
    newAssignments.class_id = req.body.class_id;
    newAssignments.title = req.body.title;
    newAssignments.description = req.body.description;
    newAssignments.due_date = req.body.due_date;
    var assignment123_file = null;
    if (req.files.assignment_file != 'undefined') {
        var file = req.files.assignment_file;
        fileName = file.path.split('/');
        type = (typeof file.name != 'undefined' && file.name != "") ? "." + file.name.split('.')[1] : "";
        fs.rename(file.path, __dirname + "/../../../public/upload/assignments/" + fileName[fileName.length - 1] + type, function(err) {
            if (err) {}
        });
        assignment123_file = fileName[fileName.length - 1] + type;
    } else {
      return apiSendError(req, res, 400, "Bad Request check your parameters");
    }
    newAssignments.file_name = assignment123_file;
    newAssignments.save(function(err, created_Assignment) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 500, 'Something went wrong!');
        }
        var query_str = 'SELECT t1.student_id as id FROM student_classes as t1  WHERE t1.grade_id = ? AND t1.class_id = ?';
        db.driver.execQuery(query_str, [req.body.grade_id, req.body.class_id], function(err, all_students) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 400, "Bad Request check your parameters");
            }
            if (all_students.length > 0) {
                all_students.forEach(function(value, key) {
                    var data = {
                        assignment_id: created_Assignment.id,
                        student_id: value.id,
                        achieved_grade: null,
                        remarks: null,
                        file_path: null,
                        status: "0"
                    };
                    AssignmentReports.create(data, function(err) {
                        if (err) {
                            console.log(err);
                            return apiSendError(req, res, 500, "Something went wrong!");
                        }
                    });
                });
                var response = {};
                response['data'] = [];
                response['code'] = 200;
                return res.send(response);
            } else {
                var response = {};
                response['data'] = [];
                response['code'] = 200;
                return res.send(response);
            }
        });
    });
};

exports.apiGetAllStaffAssignments = function(req, res) {
    if (typeof req.body.staff_id == 'undefined') {
        return apiSendError(req, res, 422, "staff id is required.");
    }
    var query_str = `SELECT t1.*,t2.grade,CONCAT(t3.first_name," ",t3.last_name) as staff_name,t4.school_name, t5.subject_name,t6.class_name
                    FROM assignments as t1 LEFT JOIN grades as t2 ON t1.grade_id = t2.id
                    LEFT JOIN staff as t3 ON t1.staff_id = t3.id LEFT JOIN schools as t4 ON t1.school_id = t4.id
                    LEFT JOIN subjects as t5 ON t1.subject_id = t5.id LEFT JOIN classes as t6 ON t6.id = t1.class_id
                    WHERE t1.staff_id = ? ORDER BY t1.due_date DESC,t6.class_name DESC`;
    db.driver.execQuery(query_str, [req.body.staff_id], function(err, rows) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 400, "Bad Request check your parameters");
        }
        var response = {};
        response['data'] = rows;
        response['code'] = 200;
        return res.send(response);
    });
};

exports.apiGetAllStudentAssignments = function(req, res) {
    var current_date = new Date().toISOString().split("T")[0].toString();
    if (typeof req.body.student_id == 'undefined') {
        return apiSendError(req, res, 422, "student_id ID required");
    }
    var query_str = `SELECT t1.achieved_grade, t1.remarks,t1.file_path as studentUploadedAssignment,t1.status,
                     t2.title,t2.due_date, t2.file_name AS staffUploadedAssignment,
                     t3.grade,CONCAT(t4.first_name," ",t4.last_name) as staff_name,t5.school_name,t6.subject_name
                     FROM assignment_reports as t1
                     LEFT JOIN assignments as t2 ON t1.assignment_id = t2.id
                     LEFT JOIN grades as t3 ON t2.grade_id = t3.id
                     LEFT JOIN staff as t4 ON t2.staff_id = t4.id
                     LEFT JOIN schools as t5 ON t2.school_id = t5.id
                     LEFT JOIN subjects as t6 ON t2.subject_id = t6.id
                     WHERE t2.due_date >= \'` + current_date + `\' AND t1.student_id = ? ORDER BY t2.due_date ASC`;
    db.driver.execQuery(query_str, [req.body.student_id], function(err, rows) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!rows.length) {
            return apiSendError(req, res, 404, "No Assignment found");
        }
        var response = {};
        response['data'] = rows;
        response['code'] = 200;
        return res.send(response);
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return apiSendError(req, res, 404, 'Assignment id required!!');
    }
    Assignments.find({
        id: req.body.id
    }, 1, function(err, assignments) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 400, "Bad Request check your parameters");
        }
        if (assignments.length == 0) {
            return apiSendError(req, res, 404, "Assignment not found.");
        }
        if (assignments[0].file_name) {
            var assignmentFile = __dirname + "/../../public/upload/assignments/" + assignments[0].file_name;
            var fs = require('fs');
            fs.exists(assignmentFile, function(exists) {
                if (exists) {
                    fs.unlinkSync(assignmentFile);
                }
            });
        }
        assignments[0].remove(function(err) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 400, "Bad Request check your parameters");
            }
            var response = {};
            response['data'] = [];
            response['code'] = 200;
            return res.send(response);
        });
    });
};

exports.completed = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return apiSendError(req, res, 404, 'AssignmentReports id required!!');
    }
    AssignmentReports.find({
        id: req.body.id
    }, 1, function(err, reports) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 400, 'Something want wrong.');
        }
        reports[0].status = (reports[0].status == "0") ? "1" : "0";
        reports[0].save(function(err, saved_report) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 404, 'AssignmentReports not found!!');
            }
            var response = {};
            response['data'] = [];
            response['code'] = 200;
            return res.send(response);
        });
    });
};

exports.apiGetAllAssignmentStudentList = function(req, res) {
    if (req.body.assignment_id == 'undefined') {
        return apiSendError(req, res, 422, "AssignmentReports ID required");
    }
    var query_str = `SELECT t1.*, CONCAT(t2.first_name, " ", t2.last_name) as student_name,t2.image as student_image
    				FROM assignment_reports as t1
    				LEFT JOIN admissions as t2 ON t1.student_id = t2.id
    				WHERE t1.assignment_id = ?`;
    db.driver.execQuery(query_str, [req.body.assignment_id], function(err, rows) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 400, "Bad Request check your parameters");
        }
        var response = {};
        response['data'] = rows;
        response['code'] = 200;
        return res.send(response);
    });
};

exports.apiUpdateGrades = function(req, res) {
    if (typeof req.body.id == 'undefined' || typeof req.body.achieved_grade == 'undefined') {
        return apiSendError(req, res, 422, 'Please check your paramenters');
    }
    AssignmentReports.find({
        id: req.body.id
    }, function(err, grades_achieved) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 400, 'Something went wrong');
        }
        if (grades_achieved.length === 0) {
            return apiSendError(req, res, 404, 'Assignment not found!');
        }
        grades_achieved[0].achieved_grade = req.body.achieved_grade;
        grades_achieved[0].save(function(err, saved_grades) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 422, 'AssignmentReports id required!');
            }
            var response = {};
            response['data'] = [];
            response['code'] = 200;
            return res.send(response);
        });
    });
};

exports.getAssignment = function(req, res) {
    if (typeof req.query.id == 'undefined') {
        return apiSendError(req, res, 404, 'Assignment id required!!');
    }
    Assignments.find({
        id: req.query.id
    }, 1, function(err, assignments) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 404, 'Assignment not found.');
        }
        if (!assignments.length) {
            return apiSendError(req, res, 404, "Assignment not found.");
        }
        var response = {};
        response['data'] = assignments[0];
        response['code'] = 200;
        return res.send(response);
    });
};

exports.edit = function(req, res) {
    if (req.body.id !== 'undefined' && req.body.id) {
        Assignments.find({
            id: req.body.id
        }, 1, function(err, assignments) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 404, 'Assignment not found.');
            }
            if (req.files) {
                if (!req.files.file_name) {
                    return apiSendError(req, res, 400, "Bad Request check your parameters");
                } else {
                    var fs = require('fs');
                    var assignmentss = __dirname + "/../../../public/upload/assignments/";
                    if (!fs.existsSync(assignmentss)) {
                        fs.mkdirSync(assignmentss);
                    }
                    var file = req.files.file_name;
                    var fileExtension = file.name.split(".")[1];
                    if (fileExtension) {
                        fileName = file.path.split('/');
                        fs.rename(file.path, assignmentss + fileName[fileName.length - 1] + "." + fileExtension);
                        editedFileName = fileName[fileName.length - 1] + "." + fileExtension;
                    }
                }
            }
            if (req.body.title && req.body.description && req.body.due_date) {
            assignments[0].title = req.body.title;
            assignments[0].description = req.body.description;
            assignments[0].due_date = req.body.due_date;
            assignments[0].file_name = editedFileName;
            assignments[0].save(function(err, updated_assignments) {
                if (err) {
                    console.log(err);
                    return apiSendError(req, res, 400, "Bad Request check your parameters");
                }
                var response = {};
                response['data'] = [];
                response['code'] = 200;
                return res.send(response);
            });
          }
        });
    } else {
        return apiSendError(req, res, 404, 'Assignment id required!!');
    }
};

exports.uploadFile = function(req, res) {
    var assignment_file = null;
    if (typeof req.body.student_list_assignment_id != 'undefined' && typeof req.files.assignment_file != 'undefined') {
        var file = req.files.assignment_file;
        fileName = file.path.split('/');
        type = (typeof file.name != 'undefined' && file.name != "") ? "." + file.name.split('.')[1] : "";
        fs.rename(file.path, __dirname + "/../../../public/upload/assignments/" + fileName[fileName.length - 1] + type, function(err) {
            if (err) {
                console.log(err);
                var error_message = (err.message.split(':')[1]) ? err.message.split(':')[1] : "Somthing went wrong!";
                return apiSendError(req, res, 422, error_message);
            }
        });
        assignment_file = fileName[fileName.length - 1] + type;
        var query_str = "UPDATE `assignment_reports` SET `status`= ? ,`file_path`= ? WHERE `id` = ?";
        db.driver.execQuery(query_str, ['1', assignment_file, req.body.student_list_assignment_id], function(err, rows) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 500, 'Something went wrong!');
            }
            var response = {};
            response['data'] = [];
            response['code'] = 200;
            return res.send(response);
        });
    } else {
        return apiSendError(req, res, 422, 'student_list_assignment_id and assignment_file required');
    }
};
