app.controller('createVideoConferenceController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        $scope.selectedParticipate = [];

        $scope.getAllParticipate = function() {
            var data = {
                school_id: $cookieStore.get("loggedInStaff").school_id,
                unique_id: $cookieStore.get("loggedInStaff").unique_id
            }
            $http({
                method: "POST",
                url: "/video/getAllParticipate",
                data: data
            }).then(function successCallback(response) {
                $scope.participates = response.data;
                $scope.myConferanceList();
            }, function errorCallback(error) {
                toaster.pop('error', 'Video Conference', error.data.message);
            });

        };
        $scope.joinConferance = function(room_id){
            window.location.href = "/school/staff/video_conference/live?room_id="+room_id;
        };

        $scope.myConferanceList = function() {
            var data = {
                unique_id: $cookieStore.get("loggedInStaff").unique_id
            }
            $http({
                method: "POST",
                url: "/video/myConferanceList",
                data: data
            }).then(function successCallback(response) {
                $scope.my_conferances_list = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Video Conference', error.data.message);
            });
        };

        $scope.submitConference = function() {
            Loading(true);
            var data = {
                "room_name": $scope.chat_room.room_name,
                "scheduled_date": $scope.chat_room.meeting_date + ' ' + $scope.chat_room.meeting_time,
                "school_id": $cookieStore.get("loggedInStaff").school_id,
                "creater_id": $cookieStore.get("loggedInStaff").unique_id + "**staff**"+$cookieStore.get("loggedInStaff").email_id,
                "participants": $scope.selectedParticipate
            };
            $http({
                method: "POST",
                url: "/chat_room/create",
                data: data
            }).then(function successCallback(response) {
                Loading(false);
                toaster.pop('success', 'Video Conference', 'Created Successfully.');
                $scope.chat_room.room_name = "";
                $scope.chat_room.meeting_date = "";
                $scope.chat_room.meeting_time = "";
                $scope.myConferanceList();
            }, function errorCallback(error) {
                Loading(false);
                toaster.pop('error', 'Video Conference', error.data.message);
            });
        };


        // toggle selection for a given fruit by name
        $scope.toggleSelection = function toggleSelection(unique_id) {
            var idx = $scope.selectedParticipate.indexOf(unique_id);

            // is currently selected
            if (idx > -1) {
                $scope.selectedParticipate.splice(idx, 1);
            }

            // is newly selected
            else {
                $scope.selectedParticipate.push(unique_id);
            }
        };
    }
]);

app.controller('staffVideoConferenceLiveController', ['$scope', '$http', '$stateParams', 'socket', '$window', '$cookieStore', 'toaster', function($scope, $http, $stateParams, socket, $window, $cookieStore, toaster) {
    // if(!$stateParams.chatroomId){
    //   return console.log("Wrong chatroom name!");
    // }
    var iceCandidateResponses={};
     var configuration =socket.stunservers();
    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };
    $scope.getMessagingHistory = function() {
        var data = {
            "room_id": getUrlParameter("room_id")
        };
        $http({
            method: "POST",
            url: "/chat_room/getMessagingHistory",
            data: data
        }).then(function successCallback(response) {
            $scope.group_messages = response.data;
        }, function errorCallback(error) {
            console.log('connot get older message');
        });
    };
    $scope.getRoomData = function() {
        var data = {
            "room_id": getUrlParameter("room_id"),
            "unique_id": $cookieStore.get('loggedInStaff').unique_id
        };
        $http({
            method: "POST",
            url: "/chat_room/getUserConfigCheck",
            data: data
        }).then(function successCallback(response) {
            $scope.room_title = response.data[0].room_name;
            $scope.userName = $cookieStore.get('loggedInStaff').first_name + " " + $cookieStore.get('loggedInStaff').last_name;
            $scope.roomName = response.data[0].room_id;
            $scope.joinRoom();
            $scope.getMessagingHistory();
        }, function errorCallback(error) {
            alert(error.data.message);
            //toaster.pop('error', 'Video Conference', error.data.message);
        });
    };
    $scope.userName = "";
    $scope.roomName = "";
    $scope.remoteVideo = [];
    var RTCOffers = {},
        RTCAccept = {},
        globalStream=window.globalStream;
    var localVideo = document.getElementById('localVideo');
    $scope.group_messages = [];
    $scope.joinRoom = function() {
        socket.connect($scope.userName, null, $scope.roomName);
        socket.on('connect', function() {
            console.log("Connected to socket server");
        });
        socket.on('error', function(err) {
            console.log("Failed to connect to socket server :" + JSON.stringify(err));
        });
        socket.on('user-list', function(data) {
            console.log("Got list of users to be connected to :" + JSON.stringify(data));
            //start logic to add these participants to your window!
            navigator.mediaDevices.getUserMedia({
                audio: true,
                video: true
            }).then(function(stream) {
                localVideo.srcObject = stream;
                globalStream = stream;
                for (var i in data.client) {
                    initiateRtc(data.client[i], stream);
                }
            });

            function initiateRtc(client, stream) {
                RTCOffers[client] = new RTCPeerConnection(configuration);
                RTCOffers[client].addStream(stream);
                RTCOffers[client].onaddstream = function(e) {
                    addRemoteStream(e.stream, client);
                    console.log("Got stream for user " + client);
                };
                RTCOffers[client].onicecandidate = function(ice) {
                    if (!ice.candidate) return;
                    socket.emit('video-signal', {
                        signal: 'ice-emit',
                        roomname: $scope.roomName,
                        ice: JSON.stringify(ice.candidate),
                        from: $scope.userName,
                        for: client
                    });
                };
                RTCOffers[client].createOffer({
                    offerToReceiveAudio: 1,
                    offerToReceiveVideo: 1
                }).then(function(success) {
                    RTCOffers[client].setLocalDescription(new RTCSessionDescription(success)).then(function() {
                        //success is the offer to send to other party(client)
                        socket.emit('video-signal', {
                            signal: 'offer-broadcast',
                            from: $scope.userName,
                            for: client,
                            roomname: $scope.roomName,
                            offer: success
                        });
                    });
                });
            }
        });
        socket.on('video-signal', function(data) {
            if (data.for === $scope.userName) {
                switch (data.signal) {
                    case 'offer-accept':
                        {
                            RTCOffers[data.from].setRemoteDescription(new RTCSessionDescription(data.offer)).then(function() {
                                    console.log("Set remote description success from :" + data.from);
                                },
                                function() {
                                    console.log("Set remote description failed from :" + data.from);
                                });
                            break;
                        }
                    case 'offer-broadcast':
                        {
                            if (globalStream) { //this is not the first client
                                acceptRemoteOffer(globalStream, data);
                            } else {
                                navigator.mediaDevices.getUserMedia({
                                    audio: true,
                                    video: true
                                }).then(function(stream) {
                                    acceptRemoteOffer(stream, data);
                                });
                            }
                            break;
                        }
                    case 'ice-emit':
                        {
                            //check if pending responses from this user are to be added !
                            var duesCleared=false;
                            if(iceCandidateResponses[data.from]){
                                for(var i=0;i<iceCandidateResponses[data.from].length;i++){
                                    console.log("Adding stale ice candidate");
                                    if (RTCAccept[data.from]) {
                                        addIceCandidate(RTCAccept[data.from], iceCandidateResponses[data.from][i], data.from);
                                        duesCleared=true;
                                    }
                                    else if (RTCOffers[data.from]) {
                                        addIceCandidate(RTCOffers[data.from], iceCandidateResponses[data.from][i], data.from);
                                        duesCleared=true;
                                    }
                                }
                                if(duesCleared) delete iceCandidateResponses[data.from];
                            }
                            if (RTCAccept[data.from]) {
                                addIceCandidate(RTCAccept[data.from], data.ice, data.from);
                            }
                            else if (RTCOffers[data.from]) {
                                addIceCandidate(RTCOffers[data.from], data.ice, data.from);
                            }
                            else {
                                console.log("Storing stale ice candidate");
                                if(iceCandidateResponses[data.from]) {
                                    iceCandidateResponses[data.from].push(data.ice);
                                }
                                else{
                                    iceCandidateResponses[data.from]=[data.ice];
                                }
                            }
                            break;
                        }
                    default:
                        {
                            console.log("Wrong video signal recieved");
                        }
                }
            }
            if (data.signal === 'teardown') {
                delete RTCAccept[data.deserter];
                delete RTCOffers[data.deserter];
                console.log("User with id " + data.deserter + " left the call");
                for (var i in $scope.remoteVideo) {
                    if ($scope.remoteVideo[i].id === data.deserter) {
                        $scope.remoteVideo.splice(i, 1);
                        break;
                    }
                }
            }

            function acceptRemoteOffer(stream, data) {
                RTCAccept[data.from] = new RTCPeerConnection(configuration);
                RTCAccept[data.from].addStream(stream);
                RTCAccept[data.from].onaddstream = function(e) {
                    addRemoteStream(e.stream, data.from);
                    console.log("Got remote stream of user " + data.from);
                };
                RTCAccept[data.from].onicecandidate = function(ice) {
                    if (!ice.candidate) return;
                    socket.emit('video-signal', {
                        signal: 'ice-emit',
                        roomname: $scope.roomName,
                        from: $scope.userName,
                        ice: JSON.stringify(ice.candidate),
                        for: data.from
                    });
                };
                RTCAccept[data.from].setRemoteDescription(new RTCSessionDescription(data.offer)).then(function(success) {
                        console.log("Set remote description successful");
                        RTCAccept[data.from].createAnswer().then(function(success) {
                            RTCAccept[data.from].setLocalDescription(new RTCSessionDescription(success)).then(function() {
                                //Now pass this to the orignal initiator, for completing signalling process
                                socket.emit('video-signal', {
                                    roomname: $scope.roomName,
                                    signal: 'offer-accept',
                                    from: $scope.userName,
                                    for: data.from,
                                    offer: success
                                });
                            }, function(failure) {
                                console.log("setLocalDescription failed due to : " + failure);
                            });
                        }, function(failure) {
                            console.log("Create answer failed due to : " + failure);
                        });
                    },
                    function(error) {
                        console.log("Set remote description failed due to error :" + error);
                    });
            }

            function addIceCandidate(rtc, ice, candidate) {
                rtc.addIceCandidate(new RTCIceCandidate(JSON.parse(ice))).then(function() {
                    console.log("successfully added ICE candidate for :" + candidate);
                }, function(err) {
                    console.log("Error while adding ICE candidate for :" + candidate + " with error " + err);
                });
            }
        });
        socket.on('group-message', function(data) {
            console.log(data);
            $scope.group_messages.push(data);
            console.log("Got group message ->" + JSON.stringify(data));
        });
    };

    function addRemoteStream(stream, candidate) {
          $scope.remoteVideo.push({
              source: $window.URL.createObjectURL(stream),
              id: candidate
          });
    }
    $scope.abandonCall = function() {
        socket.emit('video-signal', {
            signal: 'teardown',
            roomname: $scope.roomName,
            deserter: $scope.userName
        });
        RTCAccept = [];
        RTCOffers = [];
        $scope.remoteVideo = [];
    };

    $scope.sendGroupMessage = function() {
        socket.emit('group-message', {
            message: $scope.groupMessage,
            from: $scope.userName,
            roomname: $scope.roomName,
            unique_id: $cookieStore.get('loggedInStaff').unique_id,
            datetime: new Date()
        });
        $scope.groupMessage = "";
    };
}]);
