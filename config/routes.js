module.exports = function(app, passport, multipartyMiddleware) {

    //home routes
    var homeController = require('../app/controllers/home');
    app.get('/', homeController.index);

    //user routes
    var userController = require('../app/controllers/user');
    app.get('/logout', userController.logout);
    app.post('/logout', userController.logout);

    app.post('/login', passport.authenticate('login', {
        successRedirect: '/home',
        failureRedirect: '/failure_login',
        failureFlash: false
    }));
    app.post('/register', passport.authenticate('signup', {
        successRedirect: '/home',
        failureRedirect: '/failure_register',
        failureFlash: false
    }));

    app.post('/forgot_password', userController.fargot_password_all);
    app.post('/reset_password', userController.reset_password_all);
    app.post('/superAdmin/getAllSuperAdmins', isLoggedIn, isLoggedSuperAdmin, userController.getAllSuperAdmins);
    app.get('/user/edit', isLoggedIn, userController.getUser);
    app.post('/user/edit', isLoggedIn, userController.edit);
    app.post('/user/active', isLoggedIn, userController.active);
    app.post('/user/inactive', isLoggedIn, userController.inactive);
    app.post('/user/delete', isLoggedIn, userController.delete);
    app.post('/user/upload', multipartyMiddleware, userController.uploadFile);
    app.post('/user/changepassword', isLoggedIn, userController.changePassword);
    app.post('/user/getSchoolByAuthorizedPerson', userController.getSchoolByAuthorizedPerson);
    app.get('/failure_login', function(req, res) {
        return sendError(req, res, 444, req.flash('message').join());
    });
    app.get('/home', function(req, res) {
        user = req.flash('message')[0];
        delete user.password;
        //console.log(user.email + " logged in");
        return res.send({
            user: user
        });
    });
    app.get('/failure_register', function(req, res) {
        var error_code = 444;
        var message = "User must be login to perform this task";
        if (req.flash('message').join() == "User already exists with same email") {
            error_code = 401;
            message = "User already exists with same email";
        }
        return sendError(req, res, error_code, message);
    });

    //school routes
    var schoolController = require('../app/controllers/school');
    app.post('/school/register', isLoggedIn, schoolController.register);
    app.get('/school/edit', isLoggedIn, schoolController.getSchool);
    app.post('/school/edit', isLoggedIn, schoolController.edit);
    app.post('/school/active', isLoggedIn, schoolController.active);
    app.post('/school/inactive', isLoggedIn, schoolController.inactive);
    app.post('/school/delete', isLoggedIn, schoolController.delete);
    app.post('/school/getAllDestricts', isLoggedIn, schoolController.getAllDestricts);
    app.post('/school/getAllSchools', isLoggedIn, schoolController.getAllSchools);
    app.post('/school/getUniversalSchools', isLoggedIn, schoolController.getUniversalSchools);
    app.post('/school/getDistrictSchools', isLoggedIn, schoolController.getDistrictSchools);
    app.post('/school/tryLogin', isLoggedIn, schoolController.trySchoolLogin);
    app.post('/sendmail', isLoggedIn, schoolController.sendMail);
    app.post('/school/getAllSuperAdminSchools', isLoggedIn, schoolController.getAllSuperAdminSchools);

    //district routes
    var districtController = require('../app/controllers/district');
    app.post('/district/school/register', districtController.register);
    app.post('/district/tryLogin', districtController.getDistrict);
    app.post('/district/getAllSchools', districtController.getAllDistrictSchools);
    app.post('/district/school/active', districtController.active);
    app.post('/district/school/inactive', districtController.inactive);
    app.post('/district/school/delete', districtController.delete);

    //Manage Department routes
    var departmentController = require('../app/controllers/department');
    app.post('/school/department/register', isLoggedIn, departmentController.register);
    app.get('/school/department/edit', isLoggedIn, departmentController.getDepartment);
    app.post('/school/department/edit', isLoggedIn, departmentController.edit);
    app.post('/school/department/active', isLoggedIn, departmentController.active);
    app.post('/school/department/inactive', isLoggedIn, departmentController.inactive);
    app.post('/school/department/delete', isLoggedIn, departmentController.delete);
    app.post('/school/department/getAllDistDepartment', isLoggedIn, departmentController.getAllDistDepartment);
    app.post('/school/department/getAllSchoolDepartment', isLoggedIn, departmentController.getAllSchoolDepartment);

    //packages routes
    var packageController = require('../app/controllers/package');
    app.post('/package/register', isLoggedIn, packageController.register);
    app.get('/package/edit', isLoggedIn, packageController.getPackage);
    app.post('/package/edit', isLoggedIn, packageController.edit);
    app.post('/package/active', isLoggedIn, packageController.active);
    app.post('/package/inactive', isLoggedIn, packageController.inactive);
    app.post('/package/delete', isLoggedIn, packageController.delete);
    app.post('/package/getAllPackages', isLoggedIn, packageController.getAllPackages);

    //Dashobard routes
    var dashboardController = require('../app/controllers/dashboard');
    //super Admin
    app.post('/dashboard/total_registered_districts', isLoggedIn, dashboardController.total_registered_districts);
    app.post('/dashboard/total_registered_schools', isLoggedIn, dashboardController.total_registered_schools);
    app.post('/dashboard/districts_lists', isLoggedIn, dashboardController.districts_lists);
    app.post('/dashboard/schools_lists', isLoggedIn, dashboardController.schools_lists);
    app.post('/dashboard/payment_received', isLoggedIn, dashboardController.payment_received);
    app.post('/dashboard/total_students', isLoggedIn, dashboardController.total_students);
    app.post('/dashboard/events_lists', isLoggedIn, dashboardController.events_lists);
    app.post('/dashboard/events_lists/calendar', isLoggedIn, dashboardController.events_lists_calendar);
    app.post('/dashboard/chart_data', isLoggedIn, dashboardController.chart_data);

    //district Admin
    app.post('/dashboard/district/total_registered_schools', isLoggedIn, dashboardController.district_total_registered_schools);
    app.post('/dashboard/district/total_departments', isLoggedIn, dashboardController.district_total_departments);
    app.post('/dashboard/district/total_students', isLoggedIn, dashboardController.district_total_students);
    app.post('/dashboard/district/schools_lists', isLoggedIn, dashboardController.district_schools_lists);
    app.post('/dashboard/district/payment_received', isLoggedIn, dashboardController.district_payment_received);
    app.post('/dashboard/district/events_lists', isLoggedIn, dashboardController.district_events_lists);
    app.post('/dashboard/district/events_lists/calendar', isLoggedIn, dashboardController.district_events_lists_calendar);
    app.post('/dashboard/district/chart_data', isLoggedIn, dashboardController.district_chart_data);

    //school Admin
    app.post('/dashboard/school/total_departments', isLoggedIn, dashboardController.school_total_departments);
    app.post('/dashboard/school/total_students', isLoggedIn, dashboardController.school_total_students);
    app.post('/dashboard/school/total_staffs', isLoggedIn, dashboardController.school_total_staffs);
    app.post('/dashboard/school/payment_received', isLoggedIn, dashboardController.school_payment_received);
    app.post('/dashboard/school/events_lists', isLoggedIn, dashboardController.school_events_lists);
    app.post('/dashboard/school/students_lists', isLoggedIn, dashboardController.school_students_lists);
    app.post('/dashboard/school/staffs_lists', isLoggedIn, dashboardController.school_staffs_lists);
    app.post('/dashboard/school/events_lists/calendar', isLoggedIn, dashboardController.school_events_lists_calendar);
    app.post('/dashboard/school/chart_data', isLoggedIn, dashboardController.school_chart_data);

    //staff
    app.post('/dashboard/staff/total_students', isLoggedIn, dashboardController.staff_total_students);
    app.post('/dashboard/staff/total_schedules', isLoggedIn, dashboardController.staff_total_schedules);
    app.post('/dashboard/staff/total_assignments', isLoggedIn, dashboardController.staff_total_assignments);
    app.post('/dashboard/staff/total_pending_appointments', isLoggedIn, dashboardController.staff_total_pending_appointments);
    app.post('/dashboard/staff/events_lists', isLoggedIn, dashboardController.staff_events_lists);
    app.post('/dashboard/staff/students_lists', isLoggedIn, dashboardController.staff_students_lists);
    app.post('/dashboard/staff/events_lists/calendar', isLoggedIn, dashboardController.staff_events_lists_calendar);
    //student

    app.post('/dashboard/student/total_staffs', isLoggedIn, dashboardController.student_total_staffs);
    app.post('/dashboard/student/events_lists', isLoggedIn, dashboardController.student_events_lists);
    app.post('/dashboard/student/total_assignments', isLoggedIn, dashboardController.student_total_assignments);
    app.post('/dashboard/student/total_exams', isLoggedIn, dashboardController.student_total_exams);
    app.post('/dashboard/student/total_parents', isLoggedIn, dashboardController.student_total_parents);
    app.post('/dashboard/student/pending_assignments', isLoggedIn, dashboardController.student_pending_assignments);
    app.post('/dashboard/student/events_lists/calendar', isLoggedIn, dashboardController.student_events_lists_calendar);

    // parent pages routes
    var parentPagesController = require('../app/controllers/managePages/parent_page');
    app.post('/parent/pages/register', isLoggedIn, parentPagesController.register);
    app.get('/parent/pages/edit', isLoggedIn, parentPagesController.getParentPage);
    app.post('/parent/pages/edit', isLoggedIn, parentPagesController.edit);
    app.post('/parent/pages/active', isLoggedIn, parentPagesController.active);
    app.post('/parent/pages/inactive', isLoggedIn, parentPagesController.inactive);
    app.post('/parent/pages/delete', isLoggedIn, parentPagesController.delete);
    app.post('/parent/pages/getAllParentPages', isLoggedIn, parentPagesController.getAllParentPages);

    // parent menus routes
    var parentMenusController = require('../app/controllers/managePages/parent_menu.js');
    app.post('/parent/menus/register', isLoggedIn, parentMenusController.register);
    app.get('/parent/menus/edit', isLoggedIn, parentMenusController.getParentMenu);
    app.post('/parent/menus/edit', isLoggedIn, parentMenusController.edit);
    app.post('/parent/menus/active', isLoggedIn, parentMenusController.active);
    app.post('/parent/menus/inactive', isLoggedIn, parentMenusController.inactive);
    app.post('/parent/menus/delete', isLoggedIn, parentMenusController.delete);
    app.post('/parent/menus/getAllParentMenus', isLoggedIn, parentMenusController.getAllParentMenus);

    //child menus routes
    var childMenusController = require('../app/controllers/managePages/child_menu.js');
    app.post('/menus/register', isLoggedIn, childMenusController.register);
    app.get('/menus/edit', isLoggedIn, childMenusController.getChildMenu);
    app.post('/menus/edit', isLoggedIn, childMenusController.edit);
    app.post('/menus/active', isLoggedIn, childMenusController.active);
    app.post('/menus/inactive', isLoggedIn, childMenusController.inactive);
    app.post('/menus/delete', isLoggedIn, childMenusController.delete);
    app.post('/menus/getAllChildMenus', isLoggedIn, childMenusController.getAllChildMenus);

    //Manage pages routes
    var pageController = require('../app/controllers/page');
    app.post('/page/register', isLoggedIn, pageController.register);
    app.get('/page/edit', isLoggedIn, pageController.getPage);
    app.post('/page/edit', isLoggedIn, pageController.edit);
    app.post('/page/active', isLoggedIn, pageController.active);
    app.post('/page/inactive', isLoggedIn, pageController.inactive);
    app.post('/page/delete', isLoggedIn, pageController.delete);
    app.post('/page/getAllPages', isLoggedIn, pageController.getAllPages);
    app.post('/page/upload', multipartyMiddleware, pageController.uploadFile);

    //events routes
    var eventController = require('../app/controllers/event');
    app.post('/event/register', isLoggedIn, eventController.register);
    app.get('/event/edit', isLoggedIn, eventController.getEvent);
    app.post('/event/edit', isLoggedIn, eventController.edit);
    app.post('/event/active', isLoggedIn, eventController.active);
    app.post('/event/inactive', isLoggedIn, eventController.inactive);
    app.post('/event/delete', isLoggedIn, eventController.delete);
    app.post('/event/getAllEvents', isLoggedIn, eventController.getAllEvents);
    app.post('/event/getAllDistrictEvents', isLoggedIn, eventController.getAllDistrictEvents);
    app.post('/event/getAllSchoolEvents', isLoggedIn, eventController.getAllSchoolEvents);

    //event resource routes
    var eventResourceController = require('../app/controllers/resource');
    app.post('/event/resource/register', isLoggedIn, eventResourceController.register);
    app.get('/event/resource/edit', isLoggedIn, eventResourceController.getEventResource);
    app.post('/event/resource/edit', isLoggedIn, eventResourceController.edit);
    app.post('/event/resource/active', isLoggedIn, eventResourceController.active);
    app.post('/event/resource/inactive', isLoggedIn, eventResourceController.inactive);
    app.post('/event/resource/delete', isLoggedIn, eventResourceController.delete);
    app.post('/event/resource/getAllEventResources', isLoggedIn, eventResourceController.getAllEventResources);

    //event registration routes
    var eventRegistrationController = require('../app/controllers/event_registration');
    app.post('/event/registration/register', eventRegistrationController.register);
    app.post('/event/registration/studentregister', eventRegistrationController.studentRegistration);
    app.post('/event/registration/parentRegistration', eventRegistrationController.parentRegistration);

    app.get('/event/registration/edit', isLoggedIn, eventRegistrationController.getEventRegistration);
    app.post('/event/registration/edit', isLoggedIn, eventRegistrationController.edit);
    app.post('/event/registration/active', isLoggedIn, eventRegistrationController.active);
    app.post('/event/registration/inactive', isLoggedIn, eventRegistrationController.inactive);
    app.post('/event/registration/delete', isLoggedIn, eventRegistrationController.delete);
    app.post('/event/registration/getAllEventRegistrations', isLoggedIn, eventRegistrationController.getAllEventRegistrations);
    app.post('/event/registration/getAllEventDistrictRegistrations', isLoggedIn, eventRegistrationController.getAllEventDistrictRegistrations);
    app.post('/event/registration/getAllEventSchoolRegistrations', isLoggedIn, eventRegistrationController.getAllEventSchoolRegistrations);

    app.post('/event/registration/listAllEventSchoolRegistrations', eventRegistrationController.listAllEventSchoolRegistrations);

    //school role registration routes
    var schoolRoleRegistrationController = require('../app/controllers/school_role');
    app.post('/school/role/register', schoolRoleRegistrationController.register);
    app.get('/school/role/edit', isLoggedIn, schoolRoleRegistrationController.getRole);
    app.post('/school/role/edit', isLoggedIn, schoolRoleRegistrationController.edit);
    app.post('/school/role/active', isLoggedIn, schoolRoleRegistrationController.active);
    app.post('/school/role/inactive', isLoggedIn, schoolRoleRegistrationController.inactive);
    app.post('/school/role/delete', isLoggedIn, schoolRoleRegistrationController.delete);
    app.post('/school/role/getAllRoleDistrict', isLoggedIn, schoolRoleRegistrationController.getAllRoleDistrict);
    app.post('/school/role/getAllRoleSchool', isLoggedIn, schoolRoleRegistrationController.getAllRoleSchool);

    //staff role registration routes
    var staffRoleRegistrationController = require('../app/controllers/staff_role');
    app.post('/staff/role/register', staffRoleRegistrationController.register);
    app.get('/staff/role/edit', isLoggedIn, staffRoleRegistrationController.getStaffRole);
    app.post('/staff/role/edit', isLoggedIn, staffRoleRegistrationController.edit);
    app.post('/staff/role/active', isLoggedIn, staffRoleRegistrationController.active);
    app.post('/staff/role/inactive', isLoggedIn, staffRoleRegistrationController.inactive);
    app.post('/staff/role/delete', isLoggedIn, staffRoleRegistrationController.delete);
    app.post('/staff/role/getAllStaffRoleDistrict', isLoggedIn, staffRoleRegistrationController.getAllStaffRoleDistrict);
    app.post('/staff/role/getAllStaffRoleSchool', isLoggedIn, staffRoleRegistrationController.getAllStaffRoleSchool);

    //district school admin routes
    app.post('/user/getAllSchoolAdminsByDistrict', userController.getAllSchoolAdminsByDistrict);
    app.post('/user/getAllSchoolAdminsBySchool', userController.getAllSchoolAdminsBySchool);
    app.post('/user/schoolAdmin/register', userController.schoolAdminRegister);

    // Manage Super Admin
    app.post('/superadmin/register', isLoggedIn, userController.createSuperAdmin);

    //newsLetter routes
    var newsLetterController = require('../app/controllers/news_letter');
    app.post('/letter/register', isLoggedIn, newsLetterController.register);
    app.get('/letter/edit', isLoggedIn, newsLetterController.getNewsLetter);
    app.post('/letter/edit', isLoggedIn, newsLetterController.edit);
    app.post('/letter/active', isLoggedIn, newsLetterController.active);
    app.post('/letter/inactive', isLoggedIn, newsLetterController.inactive);
    app.post('/letter/delete', isLoggedIn, newsLetterController.delete);
    app.post('/letter/getAllNewsLetters', isLoggedIn, newsLetterController.getAllNewsLetters);

    //Manage Blog routes
    var blogController = require('../app/controllers/blog');
    app.post('/blog/register', blogController.register);
    app.get('/blog/edit', blogController.getBlog);
    app.post('/blog/edit', blogController.edit);
    app.post('/blog/active', blogController.active);
    app.post('/blog/inactive', blogController.inactive);
    app.post('/blog/delete', blogController.delete);
    app.post('/blog/getAllBlogs', blogController.getAllBlogs);
    app.post('/blog/upload', multipartyMiddleware, blogController.uploadFile);

    //Manage Gallery routes
    var galleryController = require('../app/controllers/gallery');
    app.post('/gallery/register', galleryController.register);
    app.get('/gallery/edit', galleryController.getGallery);
    app.post('/gallery/edit', galleryController.edit);
    app.post('/gallery/active', galleryController.active);
    app.post('/gallery/inactive', galleryController.inactive);
    app.post('/gallery/delete', galleryController.delete);
    app.post('/gallery/getAllDistrictEventsGalleries', galleryController.getAllDistrictEventsGalleries);
    app.post('/gallery/getAllSchoolEventsGalleries', galleryController.getAllSchoolEventsGalleries);
    app.post('/gallery/upload', multipartyMiddleware, galleryController.uploadFile);

    // Manage Gallery > Image_gallery routes
    var imageGalleryController = require('../app/controllers/image_gallery');
    app.post('/gallery/addImage', isLoggedIn, imageGalleryController.register);
    app.post('/gallery/viewAlbum', isLoggedIn, imageGalleryController.viewAlbum);
    app.post('/gallery/Album/deleteImage', isLoggedIn, imageGalleryController.delete);

    // Gallery Routes for students
    app.post('/gallery/listAllStudentevents', imageGalleryController.listAllStudentevents);
    app.post('/gallery/listAllParentevents', imageGalleryController.listAllParentevents);
    app.post('/event/viewAlbum', imageGalleryController.viewAlbum);

    // Gallery Routes for Staff
    app.post('/gallery/listAllStaffevents', imageGalleryController.listAllStaffevents);
    app.post('/event/viewAlbum', imageGalleryController.viewAlbum);

    //Manage Banner routes
    var bannerController = require('../app/controllers/banner');
    app.post('/banner/register', bannerController.register);
    app.get('/banner/edit', bannerController.getBanner);
    app.post('/banner/edit', bannerController.edit);
    app.post('/banner/active', bannerController.active);
    app.post('/banner/inactive', bannerController.inactive);
    app.post('/banner/delete', bannerController.delete);
    app.post('/banner/getAllBanners', bannerController.getAllBanners);
    app.post('/banner/upload', multipartyMiddleware, bannerController.uploadFile);

    //Manage District E-CommercePortal > ProductCategory routes
    var productCategoryController = require('../app/controllers/productCategory');
    app.post('/category/register', isLoggedIn, productCategoryController.register);
    app.get('/category/edit', isLoggedIn, productCategoryController.getCategory);
    app.post('/category/edit', isLoggedIn, productCategoryController.edit);
    app.post('/category/active', isLoggedIn, productCategoryController.active);
    app.post('/category/inactive', isLoggedIn, productCategoryController.inactive);
    app.post('/category/delete', isLoggedIn, productCategoryController.delete);
    app.post('/category/getAllDistCategories', isLoggedIn, productCategoryController.getAllDistCategories);
    app.post('/category/getAllSchoolCategories', isLoggedIn, productCategoryController.getAllSchoolCategories);

    // fee routes
    var feeController = require('../app/controllers/school_fee.js');
    app.post('/fee/register', isLoggedIn, feeController.register);
    app.get('/fee/edit', isLoggedIn, feeController.getFee);
    app.post('/fee/edit', isLoggedIn, feeController.edit);
    app.post('/fee/delete', isLoggedIn, feeController.delete);
    app.post('/fee/getAllDistrictFees', isLoggedIn, feeController.getAllDistrictFees);
    app.post('/fee/getAllSchoolFees', isLoggedIn, feeController.getAllSchoolFees);

    //Manage District E-CommercePortal > Product routes
    var productController = require('../app/controllers/product');
    app.post('/product/register', isLoggedIn, productController.register);
    app.get('/product/edit', isLoggedIn, productController.getProduct);
    app.post('/product/edit', isLoggedIn, productController.edit);
    app.post('/product/active', isLoggedIn, productController.active);
    app.post('/product/inactive', isLoggedIn, productController.inactive);
    app.post('/product/delete', isLoggedIn, productController.delete);
    app.post('/product/upload', multipartyMiddleware, productController.uploadFile);
    app.post('/product/getAllDistProducts', isLoggedIn, productController.getAllDistProducts);
    app.post('/product/getAllSchoolProducts', isLoggedIn, productController.getAllSchoolProducts);

    // Manage District Session routes
    var districtSessionController = require('../app/controllers/session.js');
    app.post('/session/register', isLoggedIn, districtSessionController.register);
    app.get('/session/edit', isLoggedIn, districtSessionController.getSession);
    app.post('/session/edit', isLoggedIn, districtSessionController.edit);
    app.post('/session/active', isLoggedIn, districtSessionController.active);
    app.post('/session/inactive', isLoggedIn, districtSessionController.inactive);
    app.post('/session/delete', isLoggedIn, districtSessionController.delete);
    app.post('/session/getAllDistSessions', districtSessionController.getAllDistSessions);
    app.post('/session/getAllSchoolSessions', districtSessionController.getAllSchoolSessions);

    // Manage District School Staff routes
    var districtStaffController = require('../app/controllers/staff');
    app.post('/staff/register', districtStaffController.register);
    app.get('/staff/edit', districtStaffController.getStaff);
    app.post('/staff/edit', districtStaffController.edit);
    app.post('/staff/active', districtStaffController.active);
    app.post('/staff/inactive', districtStaffController.inactive);
    app.post('/staff/delete', districtStaffController.delete);
    app.post('/staff/getAllStaff', districtStaffController.getAllStaff);
    app.post('/staff/getAllStaffStudent', districtStaffController.getSchoolsAllStaff);
    app.post('/staff/getSchoolsAllStaff', districtStaffController.getSchoolsAllStaff);
    app.post('/staff/getAllMySchedules', districtStaffController.getAllMySchedules);
    app.post('/staff/getAllStudentClasses', districtStaffController.getAllStudentClasses);
    app.post('/staff/upload', multipartyMiddleware, districtStaffController.uploadFile);
    //Internal Login
    app.post('/staff/tryLogin', districtStaffController.tryStaffLogin);
    //External Login
    app.post('/staff/staffLogin', districtStaffController.staffLogin);

    app.post('/staff/appointment/listAllAppointments', districtStaffController.listAllAppointments);
    app.post('/staff/appointment/listAllSchoolAppointments', districtStaffController.listAllSchoolAppointments);
    app.post('/parent/appointment/listAllAppointments', districtStaffController.parentListAllAppointments);
    app.post('/student/appointment/listAllAppointments', districtStaffController.studentListAllAppointments);
    app.post('/mail/staff', districtStaffController.getStaffEmailList);
    app.post('/mail/student', districtStaffController.getStudentEmailList);
    app.post('/mail/parents', districtStaffController.getParentEmailList);

    // Manage School Notifications routes
    var NotificationController = require('../app/controllers/notice');
    app.post('/notice/create_sms', NotificationController.register_sms);
    app.post('/notice/create_email', NotificationController.register_email);
    app.post('/notice/getAllNotices', NotificationController.getAllNotices);
    app.post('/notice/delete', NotificationController.delete);
    app.get('/notice/edit', NotificationController.getNotice);
    app.post('/notice/edit', NotificationController.edit);

    //change passwords
    var passwordController = require('../app/controllers/password');
    app.post('/user/change_password', isLoggedIn, passwordController.userChangePassword);
    app.post('/staff/change_password', isLoggedIn, passwordController.staffChangePassword);
    app.post('/student/change_password', isLoggedIn, passwordController.studentChangePassword);
    app.post('/parent/change_password', isLoggedIn, passwordController.parentChangePassword);

    //Manage District Acagemics > Designation routes
    var designationController = require('../app/controllers/designation.js');
    app.post('/designation/register', isLoggedIn, designationController.register);
    app.get('/designation/edit', isLoggedIn, designationController.getDesignation);
    app.post('/designation/edit', isLoggedIn, designationController.edit);
    app.post('/designation/active', isLoggedIn, designationController.active);
    app.post('/designation/inactive', isLoggedIn, designationController.inactive);
    app.post('/designation/delete', isLoggedIn, designationController.delete);
    app.post('/designation/getAllDesignations', isLoggedIn, designationController.getAllDesignations);
    app.post('/designation/getAllSchoolDesignations', isLoggedIn, designationController.getAllSchoolDesignations);

    //Manage District Acagemics > Grades routes
    var gradeController = require('../app/controllers/grade.js');
    app.post('/grade/register', isLoggedIn, gradeController.register);
    app.get('/grade/edit', isLoggedIn, gradeController.getGrade);
    app.post('/grade/edit', isLoggedIn, gradeController.edit);
    app.post('/grade/active', isLoggedIn, gradeController.active);
    app.post('/grade/inactive', isLoggedIn, gradeController.inactive);
    app.post('/grade/delete', isLoggedIn, gradeController.delete);
    app.post('/grade/getAllDistrictGrades', isLoggedIn, gradeController.getAllDistrictGrades);
    app.post('/grade/getAllSchoolGrades', isLoggedIn, gradeController.getAllSchoolGrades);

    //Manage Exams
    var examController = require('../app/controllers/exam');
    app.post('/exam/register', isLoggedIn, examController.register);
    app.get('/exam/edit', isLoggedIn, examController.getExam);
    app.post('/exam/edit', isLoggedIn, examController.edit);
    app.post('/exam/delete', isLoggedIn, examController.delete);
    app.post('/exam/getAllExams', isLoggedIn, examController.getAllExams);
    app.post('/exam/getStudentsExam', isLoggedIn, examController.getStudentsExam);

    /*Student Take Exam routes*/
    app.post('/exam/verifyStudent', examController.verifyStudent);
    app.post('/exam/startExamInit', examController.startExamInit);
    app.post('/exam/getStudentQuestion', examController.getStudentQuestion);
    app.post('/exam/addStudentAnswer', examController.addStudentAnswer);
    app.post('/exam/calculateExamResult', examController.calculateExamResult);
    app.post('/exam/getExamTime', examController.getExamTime);
    app.post('/exam/forceCompleteExam', examController.forceCompleteExam);
    app.post('/exam/questionAnswerReport', examController.questionAnswerReport);
    app.post('/exam/updateQuestionAnswerReport', examController.updateQuestionAnswerReport);
    app.post('/exam/gradeCalculation', examController.gradeCalculation);
    app.post('/exam/sendEmailExamGrade', examController.sendEmailExamGrade);

    // Manage Exam Question Answer
    var examQuestionAnswerController = require('../app/controllers/exam_qa');
    app.post('/exam/qa/create', isLoggedIn, examQuestionAnswerController.register);
    app.post('/exam/qa/delete', isLoggedIn, examQuestionAnswerController.delete);
    app.post('/exam/qa/getAllQuestionAnswer', isLoggedIn, examQuestionAnswerController.getAllQuestionAnswer);

    //Manage Exam Results
    var examResultController = require('../app/controllers/exam_result');
    app.post('/exam_result/register', isLoggedIn, examResultController.register);
    app.get('/exam_result/edit', isLoggedIn, examResultController.getExamResult);
    app.post('/exam_result/edit', isLoggedIn, examResultController.edit);
    app.post('/exam_result/delete', isLoggedIn, examResultController.delete);
    app.post('/exam_result/getAllExamResults', isLoggedIn, examResultController.getAllExamResults);
    app.post('/exam_result/getStudentExamResults', isLoggedIn, examResultController.getStudentExamResults);

    //Manage District School Class routes
    var classController = require('../app/controllers/class');
    app.post('/class/register', isLoggedIn, classController.register);
    app.get('/class/edit', isLoggedIn, classController.getDistrictClass);
    app.post('/class/edit', isLoggedIn, classController.edit);
    app.post('/class/active', isLoggedIn, classController.active);
    app.post('/class/inactive', isLoggedIn, classController.inactive);
    app.post('/class/delete', isLoggedIn, classController.delete);
    app.post('/class/getAllDistrictClass', isLoggedIn, classController.getAllDistrictClass);
    app.post('/class/getAllSchoolClass', isLoggedIn, classController.getAllSchoolClass);
    app.post('/class/getSchoolClassesGradeWise', isLoggedIn, classController.getSchoolClassesGradeWise);
    app.post('/class/getAllSubjectClasses', isLoggedIn, classController.getAllSubjectClasses);
    app.post('/class/addMultipleStudentInOneClass', isLoggedIn, classController.addMultipleStudentInOneClass)
        // Student_Classes
    app.post('/create/student_class', isLoggedIn, classController.createStudentClass);
    app.post('/class/getAllStudentClasses', isLoggedIn, classController.getAllStudentClasses);
    app.post('/student_class/deleteStudentClass', isLoggedIn, classController.deleteStudentClass);
    app.post('/class/getClassGradeWise', isLoggedIn, classController.getClassGradeWise);
    app.post('/class/getClassSubjectWise', isLoggedIn, classController.getClassSubjectWise);

    //Manage District School Subject routes
    var subjectController = require('../app/controllers/subject');
    app.post('/subject/register', isLoggedIn, subjectController.register);
    app.get('/subject/edit', isLoggedIn, subjectController.getDistrictSubject);
    app.post('/subject/edit', isLoggedIn, subjectController.edit);
    app.post('/subject/active', isLoggedIn, subjectController.active);
    app.post('/subject/inactive', isLoggedIn, subjectController.inactive);
    app.post('/subject/delete', isLoggedIn, subjectController.delete);
    app.post('/subject/getAllDistrictSubjects', isLoggedIn, subjectController.getAllDistrictSubjects);
    app.post('/subject/getAllSchoolSubjects', isLoggedIn, subjectController.getAllSchoolSubjects);
    app.post('/subject/getSchoolGradesSubjectWise', isLoggedIn, subjectController.getAllSchoolSubjectsGradeWise);

    //Manage Student profile
    var studentController = require('../app/controllers/admission');
    app.post('/student/register', isLoggedIn, studentController.register);
    app.get('/student/edit', isLoggedIn, studentController.getAdmission);
    app.post('/student/edit', isLoggedIn, studentController.edit);
    app.post('/student/active', isLoggedIn, studentController.active);
    app.post('/student/inactive', isLoggedIn, studentController.inactive);
    app.post('/student/delete', isLoggedIn, studentController.delete);
    app.post('/student/getAllAdmissionInquiry', isLoggedIn, studentController.getAllAdmissionInquiry);
    app.post('/student/getStudent', isLoggedIn, studentController.getStudent);
    app.post('/student/upload', multipartyMiddleware, studentController.uploadFile);
    app.post('/student/getStudentsList', isLoggedIn, studentController.getStudentsList);
    app.post('/student/getStudentsListGradeWise', isLoggedIn, studentController.getStudentsListGradeWise);
    app.post('/student/studentPromotion', isLoggedIn, studentController.studentPromotion);
    app.post('/student/getAllStudentGradeWise', isLoggedIn, studentController.getAllStudentGradeWise);

    //Manage Location Controller
    var locationController = require('../app/controllers/location');
    app.post('/location/register', isLoggedIn, locationController.register);
    app.post('/location/edit', isLoggedIn, locationController.edit);
    app.post('/location/active', isLoggedIn, locationController.active);
    app.post('/location/inactive', isLoggedIn, locationController.inactive);
    app.get('/location/edit', isLoggedIn, locationController.getLocation);
    app.post('/location/delete', isLoggedIn, locationController.delete);
    app.post('/location/getAllLocations', locationController.getAllLocations);


    //Manage Source Controller
    var sourceController = require('../app/controllers/source');
    app.post('/source/register', isLoggedIn, sourceController.register);
    app.get('/source/edit', isLoggedIn, sourceController.getSource);
    app.post('/source/edit', isLoggedIn, sourceController.edit);
    app.post('/source/delete', isLoggedIn, sourceController.delete);
    app.post('/source/getAllSources', sourceController.getAllSources);
    app.post('/sources/getAllSchoolSources', sourceController.getAllSources);
    // Manage Emails
    app.post('/send/staff', isLoggedIn, studentController.getStaffEmailList);
    app.post('/student/mails', isLoggedIn, studentController.getStudentEmailList);
    // Manage Student Login
    studentLoginController = require('../app/controllers/student');
    app.post('/student/studentLogin', studentLoginController.studentLogin);

    // Manage Emails
    emailController = require('../app/controllers/email');
    // Staff Mail
    app.post('/student/sendMail', isLoggedIn, emailController.sendEmails);
    app.post('/students/emails/list', isLoggedIn, emailController.listMails);
    app.post('/students/mail/delete', isLoggedIn, emailController.deleteMail);
    app.post('/student/mail/edit', isLoggedIn, emailController.getTheMail);
    app.post('/staff/mail/list', isLoggedIn, emailController.listMails);
    app.post('/staff/sendmail', isLoggedIn, emailController.sendEmailsToStaaf);
    // Staff Mail
    app.post('/send/mail/staff', isLoggedIn, emailController.sendEmails);
    app.post('/staff/mail/edit', isLoggedIn, emailController.getStaffEmail);
    app.post('/staff/mail/delete', isLoggedIn, emailController.deleteMail);
    //Sent Email List
    app.post('/mail/sentList', emailController.allSentEmailList);
    app.post('/mail/receivedList', emailController.allReceivedEmailList);

    //Manage District Student Search
    app.post('/student/getSelectedGradesRecords', isLoggedIn, studentController.getSelectedGradesRecords);
    app.post('/student/getSchoolStudentsList', isLoggedIn, studentController.getSchoolStudentsList);

    //Manage Student followUp
    var followUpController = require('../app/controllers/inquiryFollowUp');
    app.post('/followUp/register', isLoggedIn, followUpController.register);
    app.post('/followUp/active', isLoggedIn, followUpController.active);
    app.post('/followUp/inactive', isLoggedIn, followUpController.inactive);
    app.post('/followUp/delete', isLoggedIn, followUpController.delete);
    app.post('/followUp/list', isLoggedIn, followUpController.getInquiryFollowUp);

    //Manage Students parent profile
    var parentsController = require('../app/controllers/parents.js');
    app.post('/parents/register', isLoggedIn, parentsController.register);
    app.post('/parents/getStudentsParents', isLoggedIn, parentsController.getStudentsParents);
    app.post('/parents/getStudentsParentsEdit', isLoggedIn, parentsController.getStudentsParentsEdit);
    app.post('/parents/edit', isLoggedIn, parentsController.edit);
    app.get('/parents/edit', isLoggedIn, parentsController.getStudentsParentsEdit);
    app.post('/parents/delete', isLoggedIn, parentsController.delete);
    app.post('/parents/parentsLogin', parentsController.parentsLogin);
    app.post('/student/getStudents', isParentLoggedIn, studentController.getStudent);
    app.post('/parents/upload', multipartyMiddleware, parentsController.uploadFile);
    app.post('/parents/changeChild', parentsController.changeChild);

    // Manage Materials
    var materialController = require('../app/controllers/material');
    app.post('/material/register', isLoggedIn, materialController.register);
    app.get('/material/edit', isLoggedIn, materialController.getMaterial);
    app.post('/material/edit', isLoggedIn, materialController.edit);
    app.post('/material/delete', isLoggedIn, materialController.delete);
    app.post('/material/getAllMaterials', isLoggedIn, materialController.getAllMaterials);
    app.post('/material/getAllDistrictMaterials', isLoggedIn, materialController.getAllDistrictMaterials);
    app.post('/material/upload', multipartyMiddleware, materialController.uploadFile);

    // Manage Materials for Students
    app.post('/material/getStudentMaterials', materialController.getStudentMaterials);

    // Manage Assignments
    var assignmentController = require('../app/controllers/assignments.js');
    app.post('/assignment/register', isLoggedIn, assignmentController.register);
    app.get('/assignment/edit', isLoggedIn, assignmentController.getAssignment);
    app.post('/assignment/edit', isLoggedIn, assignmentController.edit);
    app.post('/assignment/delete', isLoggedIn, assignmentController.delete);
    app.post('/assignment/getAllAssignments', assignmentController.getAllAssignments);
    app.post('/assignment/getAllStudentAssignments', assignmentController.getAllStudentAssignments);
    app.post('/assignment/getStudentsAssignment', assignmentController.getStudentsAssignment);
    app.post('/assignment/upload', multipartyMiddleware, assignmentController.uploadFile);

    // Manage Medical Info
    var medicalController = require('../app/controllers/medical_info');
    app.post('/medical_info/register', isLoggedIn, medicalController.register);
    app.get('/medical_info/edit', isLoggedIn, medicalController.getMedicalInfo);
    app.post('/medical_info/edit', isLoggedIn, medicalController.edit);
    app.post('/medical_info/delete', isLoggedIn, medicalController.delete);
    app.post('/medical_info/getAllMedicalInfos', isLoggedIn, medicalController.getAllMedicalInfos);


    //Manage Appointment routes
    var appointmentController = require('../app/controllers/appointment');
    app.post('/appointment/register', appointmentController.register);
    app.get('/appointment/edit', appointmentController.getAppointment);
    app.post('/appointment/edit', appointmentController.edit);
    app.post('/appointment/active', appointmentController.active);
    app.post('/appointment/inactive', appointmentController.inactive);
    app.post('/appointment/delete', appointmentController.delete);
    app.post('/appointment/getAllStudentsAppointment', appointmentController.getAllStudentsAppointments);
    app.post('/appointment/getAllParentsAppointments', appointmentController.getAllParentsAppointments);

    app.post('/staff/appointment/changeStatus', isLoggedIn, appointmentController.changeStatus);
    app.post('/staff/appointment/changeStudentAttend', isLoggedIn, appointmentController.changeStudentAttend);
    // app.post('/students/appointment/getAllDistAppointment', isStudentLoggedIn, appointmentController.getAllDistAppointment);
    // app.post('/students/appointment/getAllSchoolAppointment', isStudentLoggedIn, appointmentController.getAllSchoolAppointment);

    // Manage Timetables
    var timeTableController = require('../app/controllers/timetable.js');
    app.post('/timetable/register', isLoggedIn, timeTableController.register);
    app.get('/timetable/edit', isLoggedIn, timeTableController.getTimeTable);
    app.post('/timetable/edit', isLoggedIn, timeTableController.edit);
    app.post('/timetable/delete', isLoggedIn, timeTableController.delete);
    app.post('/timetable/active', isLoggedIn, timeTableController.active);
    app.post('/timetable/inactive', isLoggedIn, timeTableController.inactive);
    app.post('/timetable/getAllSchoolTimeTable', isLoggedIn, timeTableController.getAllSchoolTimeTable);

    //Manage Lectures
    var lecturesController = require('../app/controllers/lecture.js');
    app.post('/lecture/register', isLoggedIn, lecturesController.register);
    app.post('/lecture/edit', isLoggedIn, lecturesController.edit);
    app.post('/lecture/delete', isLoggedIn, lecturesController.delete);
    app.post('/lecture/getLecture', isLoggedIn, lecturesController.getLecture);
    app.get('/lecture/edit', isLoggedIn, lecturesController.getLecture);
    app.post('/lecture/getAllLecturesSchoolWise', lecturesController.getAllLecturesSchoolWise);
    app.post('/lecture/getAllLecturesStaffWise', lecturesController.getAllLecturesStaffWise);

    //Manage Homework
    var homeworkController = require('../app/controllers/homework.js');
    app.post('/homework/register', isLoggedIn, homeworkController.register);
    app.get('/homework/edit', isLoggedIn, homeworkController.getHomework);
    app.post('/homework/edit', isLoggedIn, homeworkController.edit);
    app.post('/homework/delete', isLoggedIn, homeworkController.delete);
    app.post('/homework/getHomework', isLoggedIn, homeworkController.getHomework_POST);
    app.post('/homewrok/upload', isLoggedIn, homeworkController.uploadFile);
    app.post('/homework/getHomeworkStaffwise', isLoggedIn, homeworkController.getHomeworkStaffwise);
    app.post('/homework/getHomeworkStudentWise', isLoggedIn, homeworkController.getHomeworkStudentWise);
    app.post('/homework/getStudentsHomework', homeworkController.getStudentsHomework);
    // app.post('/homework/getAllHomeworkStaffWise', isLoggedIn, homeworkController.getAllHomeworkStaffWise);
    // app.post('/homework/getAllHomeworksSchoolWise', isLoggedIn, homeworkController.getAllHomeworksSchoolWise);


    //Assignment Report routes
    var assignment_reportController = require('../app/controllers/assignment_report.js');
    app.post('/assignment/upload/studentAssignment', multipartyMiddleware, assignment_reportController.uploadStudentFile);
    app.post('/assignment/getAllStudentList', assignment_reportController.getAllStudentAssignmentList);
    app.post('/assignment/submission', assignment_reportController.addStudentAssignment);
    app.post('/assignment_report/completed', assignment_reportController.completed);
    app.post('/assignment_report/notCompleted', assignment_reportController.notCompleted);
    app.post('/update/grades', assignment_reportController.updateGrades);
    // app.post('/student/getStudents', isParentLoggedIn, studentController.getStudent);

    //Homework Report routes
    var homeworkReportController = require('../app/controllers/homework_report.js');
    app.post('/homework_report/complete', homeworkReportController.complete);
    app.post('/homework_report/notCompleted', homeworkReportController.notCompleted);
    app.post('/update/homework/grades', homeworkReportController.updateGrades);
    app.post('/homework/submission', homeworkReportController.addStudentHomework);
    app.post('/homework_report/getAllStudentList', homeworkReportController.getAllStudentList);
    app.post('/homework_report/makeCompleteAll', homeworkReportController.makeCompleteAll);
    app.post('/homework_report/makeInCompleteAll', homeworkReportController.makeInCompleteAll);
    app.post('/homework/upload/studentHomework', homeworkReportController.uploadStudentHomeworkFile);

    //Manage Attendance
    var attendanceController = require('../app/controllers/attendance.js');
    app.post('/attendance/register', isLoggedIn, attendanceController.register);
    app.get('/attendance/edit', isLoggedIn, attendanceController.getEditedAttendance);
    app.post('/attendance/edit', isLoggedIn, attendanceController.edit);
    app.post('/attendance/delete', isLoggedIn, attendanceController.delete);
    app.post('/attendance/getAttendance', isLoggedIn, attendanceController.getAttendance);
    app.post('/attendance/getAttendanceStaffwise', isLoggedIn, attendanceController.getAttendanceStaffwise);

    //Manage Attendance Report routes
    var attendanceReportController = require('../app/controllers/attendance_report.js');
    app.post('/attendance_report/attend', attendanceReportController.attend);
    app.post('/attendance_report/notAttend', attendanceReportController.notAttend);
    app.post('/attendance_report/getAllStudentList', attendanceReportController.getAllStudentList);
    app.post('/attendance_report/makeNotAttendAll', attendanceReportController.makeNotAttendAll);
    app.post('/attendance_report/makeAttendAll', attendanceReportController.makeAttendAll);
    app.post('/attendance_report/getAllStudentAttendence', attendanceReportController.getAllStudentAttendence);
    //Manage Issues routes
    var studentIssueController = require('../app/controllers/issue');
    app.post('/issues/register', isLoggedIn, studentIssueController.register);
    app.get('/issues/edit', isLoggedIn, studentIssueController.getIssue);
    app.post('/issues/edit', isLoggedIn, studentIssueController.edit);
    app.post('/issues/active', isLoggedIn, studentIssueController.active);
    app.post('/issues/inactive', isLoggedIn, studentIssueController.inactive);
    app.post('/issues/delete', isLoggedIn, studentIssueController.delete);
    app.post('/issues/getAllStudentsIssues', isLoggedIn, studentIssueController.getAllStudentsIssues);
    app.post('/issues/getAllParentsIssues', studentIssueController.getAllParentsIssues);
    app.post('/issues/getAllIssues', isLoggedIn, studentIssueController.getAllIssues);
    // app.post('/issues/saveResponse', isLoggedIn, studentIssueController.saveResponse);
    app.post('/issues/updateStatus', isLoggedIn, studentIssueController.updateStatus);
    app.post('/issues/viewIssue', isLoggedIn, studentIssueController.viewIssue);
    app.post('/issues/viewStudentIssue', isLoggedIn, studentIssueController.viewIssue);
    // app.post('/issues/getAllDistIssues', isParentLoggedIn, issueController.getAllDistIssues);
    // app.post('/issues/getAllSchoolIssues', isParentLoggedIn, issueController.getAllSchoolIssues);

    ////Manage Todo's
    var todoController = require('../app/controllers/todo');
    app.post('/todo/register', todoController.register);
    app.post('/todo/delete', todoController.delete);
    app.post('/todo/getMyTodos', todoController.getMyTodos);
    app.post('/todo/done', todoController.done);

    //Issue Messages
    app.post('/issue_messages/saveIssuesMessges', studentIssueController.saveIssuesMessges);
    app.post('/issue_messages/showIssueMessagesToStaff', isLoggedIn, studentIssueController.showIssueMessagesToStaff);
    app.post('/issue_messages/saveStudentsIssuesMessges', studentIssueController.saveIssuesMessges);
    app.post('/get/children', parentsController.children);
    app.post('/get/getAllChildren', parentsController.getAllChildren);
    app.post('/parents/getSchoolParents', isLoggedIn, parentsController.getSchoolParents);

    //Manage Relation Controller
    var relationController = require('../app/controllers/relation');
    app.post('/relation/register', isLoggedIn, relationController.register);
    app.get('/relation/edit', isLoggedIn, relationController.getRelation);
    app.post('/relation/edit', isLoggedIn, relationController.edit);
    app.post('/relation/delete', isLoggedIn, relationController.delete);
    app.post('/relation/getAllRelations', relationController.getAllRelations);

    //Manage Email Notification

    var emailNotificationController = require('../app/controllers/email_notification');
    app.post('/email_notification/create', emailNotificationController.register);
    app.post('/email_notification/getAllNotifications', emailNotificationController.getAllNotifications);
    app.post('/email_notification/delete', emailNotificationController.delete);


    // Manage Book Store
    var bookStoreController = require('../app/controllers/book_store.js');
    app.post('/bookstore/register', isLoggedIn, bookStoreController.register);
    app.get('/bookstore/edit', isLoggedIn, bookStoreController.getBook);
    app.post('/bookstore/edit', isLoggedIn, bookStoreController.edit);
    app.post('/bookstore/available', isLoggedIn, bookStoreController.available);
    app.post('/bookstore/notAvailable', isLoggedIn, bookStoreController.notAvailable);
    app.post('/bookstore/delete', isLoggedIn, bookStoreController.delete);
    app.post('/bookstore/upload', multipartyMiddleware, bookStoreController.uploadFile);
    app.post('/bookstore/uploadBook', multipartyMiddleware, bookStoreController.uploadBook);
    app.post('/bookstore/getAllBooks', bookStoreController.getAllBooks);
    app.post('/bookstore/getAllAvailBooks', bookStoreController.getAllAvailBooks);

    // Manage Book Store
    var bookCategoryController = require('../app/controllers/book_category.js');
    app.post('/book_category/register', bookCategoryController.register);
    app.post('/book_category/getAllSchoolBookCategory', bookCategoryController.getAllSchoolBookCategory);
    app.post('/book_category/delete', bookCategoryController.delete);

    // Manage Templates
    var templateController = require('../app/controllers/template');
    app.post('/template/register', isLoggedIn, templateController.register);
    app.get('/template/edit', isLoggedIn, templateController.getTemplate);
    app.post('/template/edit', isLoggedIn, templateController.edit);
    app.post('/template/delete', isLoggedIn, templateController.delete);
    app.post('/template/getAllTemplate', templateController.getAllTemplate);
    // app.post('/template/get/keys', templateController.getAllKeys);


    //Manage School Admin Templates
    var schoolAdminTemplateController = require('../app/controllers/school_email_templates');
    app.post('/school/template/register', isLoggedIn, schoolAdminTemplateController.register);
    app.get('/school/template/edit', isLoggedIn, schoolAdminTemplateController.getTemplate);
    app.post('/school/template/edit', isLoggedIn, schoolAdminTemplateController.edit);
    app.post('/school/template/delete', isLoggedIn, schoolAdminTemplateController.delete);
    app.post('/school/template/getAllTemplate', schoolAdminTemplateController.getAllTemplate);
    app.post('/school/template/getAllSchoolTemplate', schoolAdminTemplateController.getAllSchoolTemplate);
    // app.post('/school/template/get/keys', schoolAdminTemplateController.getAllKeys);

    //school grade scale
    app.get('/school/grade_scale/edit', schoolAdminTemplateController.getEditGradeScale);
    app.post('/school/grade_scale/edit', schoolAdminTemplateController.EditGradeScale);    
    app.post('/school/grade_scale/getAllSchoolGradeScale', schoolAdminTemplateController.getAllSchoolGradeScale);

    //reporting module
    var reportController = require('../app/controllers/report');
    app.post('/generate/student_report', reportController.student_report);
    app.post('/generate/staff_report', reportController.staff_report);
    app.post('/generate/parent_report', reportController.parent_report);
    app.post('/generate/student_grade_report', reportController.student_grade_report);
    app.post('/generate/student_homework_report', reportController.student_homework_report);
    
    
    
    
    
};

function isLoggedIn(req, res, next) {
    if (req.cookies.loggedInUser || req.cookies.loggedInSchool || req.cookies.loggedInDistrict || req.cookies.loggedInStudent || req.cookies.loggedInStaff || req.cookies.loggedInParent) {
        return next();
    } else {
        return sendError(req, res, 444, "User must be login to perform this task");
    }
}

function isStudentLoggedIn(req, res, next) {
    if (req.cookies.loggedInStudent) {
        return next();
    } else {
        return sendError(req, res, 444, "Student must be login to perform this task");
    }
}

function isStaffLoggedIn(req, res, next) {
    if (req.cookies.loggedInStaff) {
        return next();
    } else {
        return sendError(req, res, 444, "Staff must be login to perform this task");
    }
}

function isParentLoggedIn(req, res, next) {
    if (req.cookies.loggedInParent) {
        return next();
    } else {
        return sendError(req, res, 444, "Student must be login to perform this task");
    }
}

function isLoggedSuperAdmin(req, res, next) {
    if (typeof req.user != 'undefined') {
        if (req.user.user_type == 'admin') {
            return next();
        } else {
            return sendError(req, res, 403, "You Don't have access to perform this action.");
        }
    } else {
        return sendError(req, res, 444, "User must be login to perform this task");
    }
}
