app.controller('createDistrictProductCategoryController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.getSchools = null;
        $scope.SchoolList = [];
        var district = $cookieStore.get('loggedInDistrict');
        if (typeof district != 'undefined') {
            var data = {
                district_id: district.id
            };
            $http({
                method: 'POST',
                url: '/school/getDistrictSchools',
                data: data
            }).then(function successCallback(response) {

                $scope.SchoolList = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'District', 'Something went wrong!');
            });
        } else {
            toaster.pop('error', 'Login', 'something went wrong! Please try re-login');
        }

        $scope.addCategory = function() {
            $state.go('districts.productCategory_create');
        };

        $scope.cancelCategoryForm = function() {
            $state.go('districts.productCategory_list');
        };

        $scope.createCategory = function() {
            data = $scope.category;
            $http({
                method: "POST",
                url: "/category/register",
                data: data
            }).then(function successCallback(response) {
                $state.go('districts.productCategory_list');
                toaster.pop('success', 'Category', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Category', error.data.message);
            });
        };
    }
]);

app.controller('listDistrictProductCategoryController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.addNewCategory = function() {
            $state.go('districts.productCategory_create');
        };

        $scope.inactiveCategory = function(category_id) {
            var data = {
                id: category_id
            };
            $http({
                method: 'POST',
                url: '/category/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllCategories();
            }, function errorCallback(error) {
                toaster.pop('error', 'Category', error.data.message);
            });
        };

        $scope.activeCategory = function(category_id) {
            var data = {
                id: category_id
            };
            $http({
                method: 'POST',
                url: '/category/active',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllCategories();
            }, function errorCallback(error) {
                toaster.pop('error', 'Category', error.data.message);
            });
        };

        $scope.deletecategory = function(category_id) {
            var data = {
                id: category_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/category/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllCategories();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Category', error.data.message);
                });
            }
        };

        $scope.getAllCategories = function() {
            var data = {
                district_id: $cookieStore.get('loggedInDistrict').id
            };
            $http({
                method: 'POST',
                url: '/category/getAllDistCategories',
                data: data
            }).then(function successCallback(response) {
                var categories = response.data;
                $scope.categories = categories;
            }, function errorCallback(error) {
                toaster.pop('error', 'Category', 'cannot get all the Categories! try reloading page.');
            });
        };

        $scope.editCategory = function(category_id) {
            $state.go('districts.productCategory_edit', {
                category_id: category_id
            });
        };
    }
]);

app.controller('editDistrictProductCategoryController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {

        $scope.getSchools = null;
        $scope.SchoolList = [];
        var district = $cookieStore.get('loggedInDistrict');
        if (typeof district != 'undefined') {
            var data = {
                district_id: district.id
            };
            $http({
                method: 'POST',
                url: '/school/getDistrictSchools',
                data: data
            }).then(function successCallback(response) {

                $scope.SchoolList = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'District', 'Something went wrong!');
            });
        } else {
            toaster.pop('error', 'Login', 'something went wrong! Please try re-login');
        }

        $scope.getEditCategory = function() {
            $http({
                method: 'GET',
                url: '/category/edit?id=' + $state.params.category_id
            }).then(function successCallback(response) {
                $scope.category = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Category', 'cannot get all the Category! try reloading page.');
            });
        };

        $scope.saveEditCategory = function() {
            data = $scope.category;
            $http({
                method: 'POST',
                url: '/category/edit',
                data: data
            }).then(function successCallback(response) {
                $state.go('districts.productCategory_list');
                toaster.pop('success', 'Category', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Category', error.data.message);
            });
        };

        $scope.cancelCategoryForm = function() {
            $state.go('districts.productCategory_list');
        };
    }
]);
