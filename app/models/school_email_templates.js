var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var School_Email_Templates = db.define('school_email_templates', {
    ui_key: String,
    school_id: {
        type: 'integer'
    },
    ui_data: String
}, {
    timestamp: true,
    methods: {

    }
});
