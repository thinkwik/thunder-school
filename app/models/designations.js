var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Designation = db.define('designations', {
    school_id: {
        type: 'integer'
    },
    designation: String,
    description: String,
    status: ['0', '1']
}, {
    timestamp: true,
    methods: {

    }
});
