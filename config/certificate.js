var certificate, fs, privateKey;

  fs = require('fs');
  privateKey = fs.readFileSync(__dirname+"/cert/server.key");

  certificate = fs.readFileSync(__dirname+"/cert/server.crt");

  module.exports = {
    privateKey: privateKey,
    certificate: certificate
  };