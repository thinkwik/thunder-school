var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Page = db.define('pages', {
    parent_page_id: {
        type: 'integer'
    },
    parents_menu_id: {
        type: 'integer'
    },
    title: String,
    content: String,
    image: String,
    status: ['0', '1']
}, {
    timestamp: true,
    methods: {

    }
});
