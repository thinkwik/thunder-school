var accountSid = 'AC40e4cf734e7161b50b30d364b7537287'; 
var authToken = '542cd9d044050855047cbd75245d299c'; 
 
//require the Twilio module and create a REST client 
var client = require('twilio')(accountSid, authToken);
 
exports.sendMessage = function(from,to,message){
	if(to.length == 10){
		if(to.charAt(0) == "9" || to.charAt(0) == "8" || to.charAt(0) == "7"){
			to = "+91"+to;
		} else {
			to = "+1"+to;
		}
	}
	if(from === true) {
		from = "+15108172421";
	}
	client.messages.create({
	    to: to, 
	    from: from, 
	    body: message
	}, function(err, message) { 
	    if(err){
	        console.log(err);
	        return false;
	    }
	    return true;
	});
};