app.controller('createSchoolTemplateController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore',
    function($scope, $http, $state, toaster, Upload, $cookieStore) {
        $scope.template = {};
        $scope.createTemplate = function() {
            if($scope.template.ui_key === undefined || $scope.template.ui_data === undefined){
                toaster.pop('error', "Templates", "All fields are required");
                return false;
            }
            if(isNaN($scope.template.ui_key)){
                toaster.pop('error', "Scale Key", "Key must be numeric field");
                return false;
            }
            
            data = $scope.template;
            data.school_id = $cookieStore.get('loggedInSchool').id;
            $http({
                url: '/school/template/register',
                method: "POST",
                data: data
            }).then(function(response) {
                $state.go('schools.templates_list');
                toaster.pop('success', "Templates", "Template create successfully!!!");
            }, function(response) {
                toaster.pop('error', "Templates", "Something went wrong!!!");
            });
        };

        $scope.listTemplates = function() {
            $state.go('schools.templates_list');
        };
    }
]);

app.controller('listSchoolTemplateController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore',
    function($scope, $http, $state, toaster, Upload, $cookieStore) {
        $scope.listTemplates = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/school/template/getAllSchoolTemplate',
                data : data
            }).then(function successCallback(response) {
                var templates = response.data;
                $scope.templates = templates;
            }, function errorCallback(error) {
                toaster.pop('error', 'School Template', 'cannot get all the templates! try reloading page.');
            });
        };

        $scope.deleteTemplate = function(template_id) {
            var data = {
                id: template_id
            };
            if (confirm("Please confirm, Do you really want to delete ?")) {
                $http({
                    method: 'POST',
                    url: '/school/template/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.listTemplates();
                    toaster.pop('success', 'Template', 'Template removed successfully.');
                }, function errorCallback(error) {
                    toaster.pop('error', 'Template', 'something went wrong!');
                });
            }
        };

        $scope.editTemplate = function(template_id) {
            $state.go('schools.templates_edit', {
                template_id: template_id
            });
        };

        $scope.addNewTemplate = function() {
            $state.go('schools.templates_create');
        };

    }
]);

app.controller('editSchoolTemplateController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore',
    function($scope, $http, $state, toaster, Upload, $cookieStore) {
        $scope.listTemplates = function() {
            $state.go('schools.templates_list');
        };

        $scope.saveUpdateTemplate = function() {
            var data = $scope.template;
            $http({
                method: 'POST',
                url: '/school/template/edit',
                data: data
            }).then(function successCallback(response) {
                $state.go('schools.templates_list');
                toaster.pop('success', 'Template', 'Template updated successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Source', error.data.message);
                toaster.pop('error', 'Template', 'Something went wrong!');
            });
        };

        $scope.getKeyList = function() {
            $http({
                method: 'POST',
                url: '/school/template/getAllTemplate'
            }).then(function successCallback(response) {
                var keys = response.data;
                $scope.keys = keys;
            }, function errorCallback(error) {
                toaster.pop('error', 'Template', 'cannot get all the templates! try reloading page.');
            });
        };

        $scope.editTemplate = function() {
            $http({
                method: 'GET',
                url: '/school/template/edit?id=' + $state.params.template_id
            }).then(function successCallback(response) {
                var template = response.data;
                $scope.template = template;
                $scope.getKeyList($scope.template.ui_key);
            }, function errorCallback(error) {
                toaster.pop('error', 'Template', error.data.message);
            });
        };
    }
]);
