var https = require('https');
cert = require('./config/certificate');
var express = require('express'),
    orm = require('orm'),
    fs = require('fs'),
    config = require('./config/config');
var fd = fs.readFileSync('multipart_parser.js');
fs.writeFileSync('./node_modules/formidable/lib/multipart_parser.js', fd);
//db connestions
orm.db = orm.connect(config.db, function(err, db) {
    if (err) {
        console.log("Something is wrong with the connection it seems ", err);
        return;
    } else {
        console.log("Connected to Database");
    }
});

//File Upload Multi party
var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty();

//Mail Connections
var nodemailer = require("nodemailer");
var smtpTransport = require("nodemailer-smtp-transport");
var transporter = nodemailer.createTransport(
    smtpTransport('smtps://binit@thinkwik.com:B!n!t.571994@smtp.gmail.com')
);

var modelsPath = __dirname + '/app/models';
fs.readdirSync(modelsPath).forEach(function(file) {
    if (file.indexOf('.js') >= 0) {
        require(modelsPath + '/' + file);
    }
});

var app = express();

//body parse to parse incoming request object
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({
    extended: true
}));

//cookie-session parser for client and express-session
var cookieParser = require('cookie-parser');
var session = require('express-session');
app.use(cookieParser());
app.use(session({
    secret: "JKZZFJBtLCxVCOMeHOnf",
    saveUninitialized: true,
    resave: false
}));

//passport for login-register authencation
var passport = require('passport');
//to get-set session flash messages
var flash = require('connect-flash');

app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

/* global functions area start*/

// function for sanding errors
// @params
// req , res object
// error_code: error code you want to throw
// error_message can be string or array of string
sendError = function(req, res, error_code, error_message) {
    if (typeof error_message != 'string') {
        error_message = error_message.join();
    }
    var return_json = {
        error_code: error_code,
        message: error_message
    }
    return res.send(error_code, return_json);
};

apiSendError = function(req, res, error_code, error_message) {
    if (typeof error_message != 'string') {
        error_message = error_message.join();
    }
    var return_json = {
        "error": {
            "message": error_message
        },
        "code": error_code
    };
    return res.send(error_code, return_json);
};
// function for sanding mails
// @params
// req , res object
// from: string you want to pass
// to : to can be array or string imploaded by emails with ","
// subject : subject you want to send
// html: html or data
sendMail = function(req, res, from, to, subject, html, cb) {
    if (typeof to != 'string') {
        to = to.join(',');
    }
    transporter.sendMail({
        from: from,
        to: to,
        subject: subject,
        html: html
    }, function(error, response) {
        if (error) {
            return cb(error, null);
        } else {
            return cb(null, response);
        }
    });
};


/* global functions area ends*/

require('./config/passport')(app, passport);
require('./config/express')(app, config);
require('./config/routes')(app, passport, multipartyMiddleware);
require('./config/routes_api')(app, passport, multipartyMiddleware);
require('./app/socket/socket')(app);
https.createServer({  
    key: cert.privateKey,
    cert: cert.certificate
  }, app).listen(config.port, function(err, success) {
    if (!err) {
        console.log("App started at localhost:" + config.port);
    } else {
        console.log("Something went wrong server cannot start");
    }
});

/* copy formidable */
