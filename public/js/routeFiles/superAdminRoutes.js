app.config(function($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider.otherwise("/");
    //Manage Super-Admin Login
    $stateProvider.state('signin', {
            url: "/",
            templateUrl: "/partials/auth/signin.html",
            controller: 'SigninFormController'
        })
        // Manage Profile
        .state('users.edit_profile', {
            url: '/edit',
            templateUrl: '/partials/superAdmin/useredit.html',
            controller: 'editUserController'
        })
        .state('forgotpwd', {
            url: '/forgot_password',
            templateUrl: '/partials/auth/forgotPassword.html',
            controller: 'forgotPasswordController'
        })
        .state('blogs', {
            url: '/blogs',
            templateUrl: '/partials/auth/blogs.html',
            controller: ''
        })
        .state('users.changepassword', {
            url: '/changepassword',
            templateUrl: '/partials/superAdmin/changePassword.html'
        })
        .state('resetPassword', {
            url: '/reset_password',
            templateUrl: '/partials/auth/resetPassword.html',
            controller:'resetPasswordController'
        })
        .state('404', {
            url: '/404',
            templateUrl: '/partials/auth/404.html'
        })
        .state('users', {
            url: '/users',
            templateUrl: '/partials/profile/users.html',
            abstract: true,
            controller: 'protectUserRoutesController'
        })
        // Manage Super Admin
        .state('users.createSuperAdmin', {
            url: '/superadmin/create',
            templateUrl: '/partials/superAdmin/manageSuperAdmin/createSuperAdmin.html',
            controller: 'createSuperAdminController'
        })
        .state('users.editSuperAdmin', {
            url: '/superadmin/edit/:superAdmin_id',
            templateUrl: '/partials/superAdmin/manageSuperAdmin/editSuperAdmin.html',
            controller: 'editSuperAdminController'
        })
        .state('users.listSuperAdmin', {
            url: '/superadmin/list',
            templateUrl: '/partials/superAdmin/manageSuperAdmin/listSuperAdmin.html',
            controller: 'listSuperAdminController'
        })
        // Manage Dashboard
        .state('users.dashboard', {
            url: '/dashboard',
            templateUrl: '/partials/superAdmin/dashboard.html',
            controller: 'superAdminDashboardController'
        })
        //Manage To-Do
        .state('users.todo_create', {
            url: '/todo/create',
            templateUrl: '/partials/superAdmin/manageTodo/createTodo.html',
            controller: 'superAdminTodoCreateController'
        })
        .state('users.todo_list', {
            url: '/todo/list',
            templateUrl: '/partials/superAdmin/manageTodo/listTodo.html',
            controller: 'superAdminTodoListController'
        })
        // .state('users.manageadmin', {
        //     url: '/admin',
        //     templateUrl: '/partials/superAdmin/manageadmin.html',
        //     controller: 'superAdminListController'
        // })
        // Manage Schools
        .state('users.school_create', {
            url: '/school/create',
            templateUrl: '/partials/superAdmin/manageSchool/createschool.html',
            controller: "schoolController"
        })
        .state('users.school_list', {
            url: '/school/list',
            templateUrl: '/partials/superAdmin/manageSchool/listschool.html',
            controller: "schoolController"
        })
        .state('users.student_list', {
            url: '/student/list',
            templateUrl: '/partials/superAdmin/manageDashboard/listschool.html',
            controller: "schoolController"
        })
        .state('users.school_edit', {
            url: '/school/edit/:school_id',
            templateUrl: '/partials/superAdmin/manageSchool/editschool.html',
            controller: 'schoolController'
        })
        // Manage Districts
        .state('users.district_list', {
            url: '/district/list',
            templateUrl: '/partials/superAdmin/manageSchool/manageDistrict/listdistrict.html',
            controller: "schoolController"
        })
        .state('users.district_edit', {
            url: '/district/edit/:school_id',
            templateUrl: '/partials/superAdmin/manageSchool/manageDistrict/editdistrict.html',
            controller: 'schoolController'
        })
        // Manage Payments
        .state('users.payment_create', {
            url: '/payment/create',
            templateUrl: '/partials/superAdmin/manageSchool/managePayment/createpayment.html',
            controller: "schoolController"
        })
        .state('users.payment_list', {
            url: '/payment/list',
            templateUrl: '/partials/superAdmin/manageSchool/managePayment/listpayment.html',
            controller: "schoolController"
        })
        // Manage Gallery
        .state('users.gallery_list', {
            url: '/gallery/list',
            templateUrl: '/partials/superAdmin/manageGallery/listGallery.html',
            controller: "listAllSchoolGalleryController"
        })
        .state('users.gallery_create', {
            url: '/gallery/create',
            templateUrl: '/partials/superAdmin/manageGallery/createGallery.html',
            controller: "createAllSchoolGalleryController"
        })
        .state('users.gallery_edit', {
            url: '/gallery/edit/:gallery_id',
            templateUrl: '/partials/superAdmin/manageGallery/editGallery.html',
            controller: 'editAllSchoolGalleryController'
        })
        .state('users.gallery_view', {
            url: '/gallery/view/:gallery_id',
            templateUrl: '/partials/superAdmin/manageGallery/viewAlbum.html',
            controller: 'viewAllSchoolGalleryAlbumController'
        })
        .state('users.image_add', {
            url: '/gallery/addimage/:gallery_id',
            templateUrl: '/partials/superAdmin/manageGallery/addImage.html',
            controller: 'addAllSchoolImageGalleryController'
        })
        // Parent Pages routes
        .state('users.parent_pages_list', {
            url: '/parent/pages/list',
            templateUrl: '/partials/superAdmin/managePages/manageParentPages/listParentPage.html',
            controller: "listParentPagesController"
        })
        .state('users.parent_pages_edit', {
            url: '/parent/pages/edit/:parent_page_id',
            templateUrl: '/partials/superAdmin/managePages/manageParentPages/editParentPage.html',
            controller: "editParentPagesController"
        })
        .state('users.parent_pages_create', {
            url: '/parent/pages/create',
            templateUrl: '/partials/superAdmin/managePages/manageParentPages/createParentPage.html',
            controller: "createParentPagesController"
        })
        // Manage Parent Menus
        .state('users.parent_menus_list', {
            url: '/parent/menus/list',
            templateUrl: '/partials/superAdmin/managePages/manageParentMenus/listParentMenu.html',
            controller: "listParentMenusController"
        })
        .state('users.parent_menus_edit', {
            url: '/parent/menus/edit/:parent_menu_id',
            templateUrl: '/partials/superAdmin/managePages/manageParentMenus/editParentMenu.html',
            controller: "editParentMenusController"
        })
        .state('users.parent_menus_create', {
            url: '/parent/menus/create',
            templateUrl: '/partials/superAdmin/managePages/manageParentMenus/createParentMenu.html',
            controller: "createParentMenusController"
        })
        // Manage Child Pages
        .state('users.child_pages_list', {
            url: '/pages/list',
            templateUrl: '/partials/superAdmin/managePages/manageChildPages/listChildPage.html',
            controller: "listChildPagesController"
        })
        .state('users.child_pages_edit', {
            url: '/pages/edit/:page_id',
            templateUrl: '/partials/superAdmin/managePages/manageChildPages/editChildPage.html',
            controller: "editChildPagesController"
        })
        .state('users.child_pages_create', {
            url: '/pages/create',
            templateUrl: '/partials/superAdmin/managePages/manageChildPages/createChildPage.html',
            controller: "createChildPagesController"
        })
        // Manage Menus
        .state('users.child_menus_list', {
            url: '/menus/list',
            templateUrl: '/partials/superAdmin/managePages/manageChildMenus/listChildMenu.html',
            controller: "listChildMenusController"
        })
        .state('users.child_menus_edit', {
            url: '/menus/edit/:child_menu_id',
            templateUrl: '/partials/superAdmin/managePages/manageChildMenus/editChildMenu.html',
            controller: "editChildMenusController"
        })
        .state('users.child_menus_create', {
            url: '/menus/create',
            templateUrl: '/partials/superAdmin/managePages/manageChildMenus/createChildMenu.html',
            controller: "createChildMenusController"
        })
        //Manage Banners
        .state('users.banner_list', {
            url: '/banner/list',
            templateUrl: '/partials/superAdmin/manageBanner/listbanner.html',
            controller: "bannerController"
        })
        .state('users.banner_create', {
            url: '/banner/create',
            templateUrl: '/partials/superAdmin/manageBanner/createbanner.html',
            controller: "bannerController"
        })
        .state('users.banner_edit', {
            url: '/banners/edit/:banner_id',
            templateUrl: '/partials/superAdmin/manageBanner/editBanner.html',
            controller: 'bannerController'
        })
        //Manage News-letters
        .state('users.newsleter_list', {
            url: '/newsleter/list',
            templateUrl: '/partials/superAdmin/manageNewsMaterial/listnewsletter.html',
            controller: "newsletterController"
        })
        .state('users.newsleter_create', {
            url: '/newsleter/create',
            templateUrl: '/partials/superAdmin/manageNewsMaterial/createnewsletter.html',
            controller: "newsletterController"
        })
        .state('users.newsleter_edit', {
            url: '/newsleter/edit/:newsletter_id',
            templateUrl: '/partials/superAdmin/manageNewsMaterial/editnewsletter.html',
            controller: "newsletterController"
        })
        //Manage Packages
        .state('users.package_list', {
            url: '/package/list',
            templateUrl: '/partials/superAdmin/managePackages/listpackage.html',
            controller: "allPackageController"
        })
        .state('users.package_create', {
            url: '/package/create',
            templateUrl: '/partials/superAdmin/managePackages/createpackage.html',
            controller: "addNewPackageController"
        })
        .state('users.packages_edit', {
            url: '/packages/edit/:package_id',
            templateUrl: '/partials/superAdmin/managePackages/editpackage.html',
            controller: "editPackageController",
        })
        //Manage Blogs
        .state('users.blogs_list', {
            url: '/blogs/list',
            templateUrl: '/partials/superAdmin/manageBlogs/listblogs.html',
            controller: "schoolController"
        })
        .state('users.blogs_create', {
            url: '/blogs/create',
            templateUrl: '/partials/superAdmin/manageBlogs/createblogs.html',
            controller: "schoolController"
        })
        .state('users.blog_edit', {
            url: '/blogs/edit/:blog_id',
            templateUrl: '/partials/superAdmin/manageBlogs/editblogs.html',
            controller: 'blogController'
        })
        //Manage Events
        .state('users.events_list', {
            url: '/events/list',
            templateUrl: '/partials/superAdmin/manageEvents/listEvent.html',
            controller: "allEventsListController"
        })
        .state('users.event_create', {
            url: '/events/create',
            templateUrl: '/partials/superAdmin/manageEvents/createEvent.html',
            controller: "newEventController"
        })
        .state('users.event_edit', {
            url: '/events/edit/:event_id',
            templateUrl: '/partials/superAdmin/manageEvents/editEvent.html',
            controller: 'editEventController'
        })
        .state('users.event_resource_list', {
            url: '/events/:event_id/resource/list',
            templateUrl: '/partials/superAdmin/manageEvents/listEventResource.html',
            controller: "allEventResourceListController"
        })
        .state('users.event_resource_create', {
            url: '/events/:event_id/resource/create',
            templateUrl: '/partials/superAdmin/manageEvents/addEventResource.html',
            controller: "newEventResourceController"
        })
        .state('users.event_resource_edit', {
            url: '/events/:event_id/resource/edit/:resource_id',
            templateUrl: '/partials/superAdmin/manageEvents/editEventResource.html',
            controller: 'editEventResourceController'
        })
        .state('users.event_registration_list', {
            url: '/events/registration/list',
            templateUrl: '/partials/superAdmin/manageEvents/listEventRegistration.html',
            controller: "allEventRegistrationsListController"
        })
        .state('users.event_registration_create', {
            url: '/events/registration/create',
            templateUrl: '/partials/superAdmin/manageEvents/addEventRegistration.html',
            controller: "newEventRegistrationController"
        })
        .state('users.event_registration_edit', {
            url: '/events/registration/edit/:registration_id',
            templateUrl: '/partials/superAdmin/manageEvents/editEventRegistration.html',
            controller: 'editEventRegistrationController'
        })
        //Manage News-letters
        .state('users.templates_list', {
            url: '/template/list',
            templateUrl: '/partials/superAdmin/manageTemplates/listTemplate.html',
            controller: "listTemplateController"
        })
        .state('users.templates_create', {
            url: '/template/create',
            templateUrl: '/partials/superAdmin/manageTemplates/createTemplate.html',
            controller: "createTemplateController"
        })
        .state('users.templates_edit', {
            url: '/template/edit/:template_id',
            templateUrl: '/partials/superAdmin/manageTemplates/editTemplate.html',
            controller: "editTemplateController"
        })
        //Manage Super-Admin Logout
        .state('signout', {
            url: "/signout",
            templateUrl: "/partials/auth/signout/signout.html",
            controller: 'logOutController'
        })
    $locationProvider.html5Mode(true);
});
