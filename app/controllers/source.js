var bCrypt = require('bcrypt-nodejs');
var db = require('orm').db,
    Source = db.models.sources;

exports.register = function(req, res) {
    new_source = new Source();
    new_source.school_id = req.body.school_id;
    new_source.source_title = req.body.source_title;
    new_source.save(function(err, saved_source) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(saved_source);
    });
};

exports.getSource = function(req, res) {
    if (typeof req.query.id == 'undefined') {
        return sendError(req, res, 422, 'Source id required');
    }
    Source.find({
        id: req.query.id
    }, 1, function(err, sources) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!sources.length) {
            return sendError(req, res, 404, "Source not found");
        }
        return res.send(sources[0]);
    });
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'Source id required');
    }
    Source.find({
        id: req.body.id
    }, 1, function(err, sources) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!sources.length) {
            return sendError(req, res, 404, "Source not found");
        }
        sources[0].school_id = req.body.school_id;
        sources[0].source_title = req.body.source_title;
        sources[0].save(function(err, sources) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "something went wrong in saving source");
            }
            return res.send(sources);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'Source id required');
    }
    Source.find({
        id: req.body.id
    }, 1, function(err, source) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (source.length === 0) {
            return sendError(req, res, 404, "Source not found");
        }
        source[0].remove(function(err) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "Something went wrong in deleting source!Please try again");
            }
            return res.send(source[0]);
        });
    });
};

exports.getAllSources = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id == "null") {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = "SELECT t1.*,t2.school_name FROM `sources` as t1 LEFT JOIN schools as t2 on t1.school_id = t2.id WHERE  t2.id = ?";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
