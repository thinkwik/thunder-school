app.controller('listGradeScaleController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore',
    function($scope, $http, $state, toaster, Upload, $cookieStore) {
        $scope.listGradeScale = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/school/grade_scale/getAllSchoolGradeScale',
                data : data
            }).then(function successCallback(response) {
                var templates = response.data;
                $scope.templates = templates;
            }, function errorCallback(error) {
                toaster.pop('error', 'School Template', 'cannot get all the templates! try reloading page.');
            });
        };

        $scope.editGradeScale = function(template_id) {
            $state.go('schools.grade_scale_edit', {
                grade_scale_id: template_id
            });
        };

    }
]);

app.controller('editGradeScaleController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore',
    function($scope, $http, $state, toaster, Upload, $cookieStore) {
        $scope.listGradeScale = function() {
            $state.go('schools.grade_scale_list');
        };

        $scope.saveUpdateGradeScale = function() {
            var data = $scope.template;
            $http({
                method: 'POST',
                url: '/school/grade_scale/edit',
                data: data
            }).then(function successCallback(response) {
                $state.go('schools.grade_scale_list');
                toaster.pop('success', 'Template', 'Template updated successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Source', error.data.message);
                toaster.pop('error', 'Template', 'Something went wrong!');
            });
        };

        $scope.getKeyList = function() {
            $http({
                method: 'POST',
                url: '/school/template/getAllTemplate'
            }).then(function successCallback(response) {
                var keys = response.data;
                $scope.keys = keys;
            }, function errorCallback(error) {
                toaster.pop('error', 'Template', 'cannot get all the templates! try reloading page.');
            });
        };

        $scope.editGradeScale = function() {
            $http({
                method: 'GET',
                url: '/school/grade_scale/edit?id=' + $state.params.grade_scale_id
            }).then(function successCallback(response) {
                var template = response.data;
                $scope.template = template;
                $scope.getKeyList($scope.template.ui_key);
            }, function errorCallback(error) {
                toaster.pop('error', 'Template', error.data.message);
            });
        };
    }
]);
