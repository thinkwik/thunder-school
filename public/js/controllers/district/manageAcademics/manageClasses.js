// app.controller('createDistrictClassController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
//     function($scope, $http, $state, toaster, $cookieStore) {
//         var district = $cookieStore.get('loggedInDistrict');
//         if (typeof district != 'undefined') {
//             var data = {
//                 district_id: district.id
//             };
//             $http({
//                 method: 'POST',
//                 url: '/school/getDistrictSchools',
//                 data: data
//             }).then(function successCallback(response) {
//                 $scope.schools = response.data;
//             }, function errorCallback(error) {
//                 toaster.pop('error', 'District', 'Something went wrong!');
//             });
//         } else {
//             toaster.pop('error', 'Login', 'something went wrong! Please try re-login');
//         }
//
//         $scope.addClass = function() {
//             $state.go('districts.class_create');
//         };
//
//         $scope.cancelClassForm = function() {
//             $state.go('districts.class_list');
//         };
//
//         $scope.getSchoolDepartments = function() {
//             var data = {
//                 school_id: $scope.class.school_id
//             };
//             $http({
//                 method: 'POST',
//                 url: '/school/department/getAllSchoolDepartment',
//                 data: data
//             }).then(function successCallback(response) {
//                 $scope.departments = response.data;
//                 $scope.getSchoolStaffs();
//                 $scope.getSchoolGrades();
//             }, function errorCallback(error) {
//                 toaster.pop('error', 'District', 'Something went wrong!');
//             });
//         };
//
//         $scope.getSchoolStaffs = function() {
//             var data = {
//                 school_id: $scope.class.school_id
//             };
//             $http({
//                 method: 'POST',
//                 url: '/staff/getSchoolsAllStaff',
//                 data: data
//             }).then(function successCallback(response) {
//                 $scope.staffs = response.data;
//             }, function errorCallback(error) {
//                 toaster.pop('error', 'District', 'Something went wrong!');
//             });
//         };
//         $scope.getSchoolGrades = function() {
//             var data = {
//                 school_id: $cookieStore.get('loggedInSchool').id
//             };
//             $http({
//                 method: 'POST',
//                 url: '/grade/getAllSchoolGrades',
//                 data: data
//             }).then(function successCallback(response) {
//                 $scope.grades = response.data;
//             }, function errorCallback(error) {
//                 toaster.pop('error', 'School', 'Something went wrong!');
//             });
//         };
//
//         $scope.createClass = function() {
//             data = $scope.class;
//             $http({
//                 method: "POST",
//                 url: "/class/register",
//                 data: data
//             }).then(function successCallback(response) {
//                 $scope.cancelClassForm();
//                 toaster.pop('success', 'Class', 'Saved Successfully.');
//             }, function errorCallback(error) {
//                 toaster.pop('error', 'Class', error.data.message);
//             });
//         };
//     }
// ]);
//
// app.controller('listDistrictClassController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
//     function($scope, $http, $state, toaster, $cookieStore) {
//         $scope.addNewClass = function() {
//             $state.go('districts.class_create');
//         };
//
//         $scope.inactiveClass = function(class_id) {
//             var data = {
//                 id: class_id
//             };
//             $http({
//                 method: 'POST',
//                 url: '/class/inactive',
//                 data: data
//             }).then(function successCallback(response) {
//                 $scope.getAllDistrictClass();
//             }, function errorCallback(error) {
//                 toaster.pop('error', 'Class', error.data.message);
//             });
//         };
//
//         $scope.activeClass = function(class_id) {
//             var data = {
//                 id: class_id
//             };
//             $http({
//                 method: 'POST',
//                 url: '/class/active',
//                 data: data
//             }).then(function successCallback(response) {
//                 $scope.getAllDistrictClass();
//             }, function errorCallback(error) {
//                 toaster.pop('error', 'Class', error.data.message);
//             });
//         };
//
//         $scope.deleteClass = function(class_id) {
//             var data = {
//                 id: class_id
//             };
//             var result = confirm("Please confirm, Do you really want to delete ?");
//             if (result) {
//                 $http({
//                     method: 'POST',
//                     url: '/class/delete',
//                     data: data
//                 }).then(function successCallback(response) {
//                     $scope.getAllDistrictClass();
//                 }, function errorCallback(error) {
//                     toaster.pop('error', 'Class', error.data.message);
//                 });
//             }
//         };
//
//         $scope.getAllDistrictClass = function() {
//             var data = {
//                 district_id: $cookieStore.get('loggedInDistrict').id
//             };
//             $http({
//                 method: 'POST',
//                 url: '/class/getAllDistrictClass',
//                 data: data
//             }).then(function successCallback(response) {
//                 var classes = response.data;
//                 $scope.classes = classes;
//             }, function errorCallback(error) {
//                 toaster.pop('error', 'Class', 'cannot get all the Class! try reloading page.');
//             });
//         };
//
//         $scope.editClass = function(class_id) {
//             $state.go('districts.class_edit', {
//                 class_id: class_id
//             });
//         };
//     }
// ]);
//
// app.controller('editDistrictClassController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
//     function($scope, $http, $state, toaster, $cookieStore) {
//         $scope.getSchools = function() {
//             var district = $cookieStore.get('loggedInDistrict');
//             if (typeof district != 'undefined') {
//                 var data = {
//                     district_id: district.id
//                 };
//                 $http({
//                     method: 'POST',
//                     url: '/school/getDistrictSchools',
//                     data: data
//                 }).then(function successCallback(response) {
//                     $scope.schools = response.data;
//                     $scope.getSchoolDepartments();
//                 }, function errorCallback(error) {
//                     toaster.pop('error', 'District', 'Something went wrong!');
//                 });
//             } else {
//                 toaster.pop('error', 'Login', 'something went wrong! Please try re-login');
//             }
//
//         };
//
//         $scope.getSchoolDepartments = function() {
//             var data = {
//                 district_id: $cookieStore.get('loggedInDistrict').id
//             };
//             $http({
//                 method: 'POST',
//                 url: '/school/department/getAllDistDepartment',
//                 data: data
//             }).then(function successCallback(response) {
//                 $scope.departments = response.data;
//                 $scope.getSchoolStaffs();
//             }, function errorCallback(error) {
//                 toaster.pop('error', 'District', 'Something went wrong!');
//             });
//         };
//
//         $scope.getSchoolStaffs = function() {
//             var data = {
//                 school_id: $scope.class.school_id
//             };
//             $http({
//                 method: 'POST',
//                 url: '/staff/getSchoolsAllStaff',
//                 data: data
//             }).then(function successCallback(response) {
//                 $scope.staffs = response.data;
//             }, function errorCallback(error) {
//                 toaster.pop('error', 'District', 'Something went wrong!');
//             });
//         };
//
//         $scope.getEditClass = function() {
//             $http({
//                 method: 'GET',
//                 url: '/class/edit?id=' + $state.params.class_id
//             }).then(function successCallback(response) {
//                 $scope.class = response.data;
//                 $scope.getSchoolGrades();
//             }, function errorCallback(error) {
//                 toaster.pop('error', 'Class', 'cannot get all the Class! try reloading page.');
//             });
//         };
//
//         $scope.getSchoolGrades = function() {
//             var data = {
//                 school_id: $cookieStore.get('loggedInSchool').id
//             };
//             $http({
//                 method: 'POST',
//                 url: '/grade/getAllSchoolGrades',
//                 data: data
//             }).then(function successCallback(response) {
//                 $scope.grades = response.data;
//             }, function errorCallback(error) {
//                 toaster.pop('error', 'School', 'Something went wrong!');
//             });
//         };
//
//         $scope.saveEditClass = function() {
//             data = $scope.class;
//             $http({
//                 method: 'POST',
//                 url: '/class/edit',
//                 data: data
//             }).then(function successCallback(response) {
//                 toaster.pop('success', 'Class', 'Saved Successfully.');
//                 $state.go('districts.class_list');
//             }, function errorCallback(error) {
//                 toaster.pop('error', 'Class', error.data.message);
//             });
//         };
//
//         $scope.cancelClassForm = function() {
//             $state.go('districts.class_list');
//         };
//     }
// ]);
