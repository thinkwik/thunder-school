var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Resource = db.define('resources', {
    event_id: {
        type: 'integer'
    },
    resource_name: String,
    resource_value: String,
    cost:String,
    total_cost:String,
    arrival_date:String,
    vendor_phone:String,
    status: String
}, {
    timestamp: true,
    methods: {

    }
});