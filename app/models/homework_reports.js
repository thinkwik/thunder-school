var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var HomeworkReport = db.define('homework_reports', {
    homework_id: {
        type: 'integer'
    },
    student_id: {
        type: 'integer'
    },
    achieved_grade: Number,
    file_path: String,
    comment: String,
    status: ['0', '1']
}, {
    timestamp: true,
    methods: {

    }
});
