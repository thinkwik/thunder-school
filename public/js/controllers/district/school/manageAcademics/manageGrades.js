app.controller('createSchoolGradesController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.addGrade = function() {
            $state.go('schools.grades_create');
        };

        $scope.cancelGradeCreate = function() {
            $state.go('schools.grades_list');
        };

        $scope.createGrade = function() {
            data = $scope.grade;
            data.school_id = $cookieStore.get('loggedInSchool').id;
            $http({
                method: "POST",
                url: "/grade/register",
                data: data
            }).then(function successCallback(response) {
                $scope.cancelGradeCreate();
                toaster.pop('success', 'Grade', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Grade', error.data.message);
            });
        };

        $scope.cancelGradeForm = function() {
            $state.go('schools.grades_list');
        };
    }
]);

app.controller('listSchoolGradesController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.addNewGrade = function() {
            $state.go('schools.grades_create');
        };

        $scope.inactiveGrades = function(grade_id) {
            var data = {
                id: grade_id
            };
            $http({
                method: 'POST',
                url: '/grade/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllSchoolGrades();
            }, function errorCallback(error) {
                toaster.pop('error', 'Grade', error.data.message);
            });
        };

        $scope.activeGrades = function(grade_id) {
            var data = {
                id: grade_id
            };
            $http({
                method: 'POST',
                url: '/grade/active',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllSchoolGrades();
            }, function errorCallback(error) {
                toaster.pop('error', 'Grade', error.data.message);
            });
        };

        $scope.deleteGrades = function(grade_id) {
            var data = {
                id: grade_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/grade/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllSchoolGrades();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Grade', error.data.message);
                });
            }
        };

        $scope.getAllSchoolGrades = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/grade/getAllSchoolGrades',
                data: data
            }).then(function successCallback(response) {
                var grades = response.data;
                $scope.grades = grades;
            }, function errorCallback(error) {
                toaster.pop('error', 'Grades', 'cannot get all the Grades! try reloading page.');
                console.log(error.data.message);
            });
        };


        $scope.editGrades = function(grade_id) {
            $state.go('schools.grades_edit', {
                grade_id: grade_id
            });
        };
    }
]);

app.controller('editSchoolGradesController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.getEditGrade = function() {
            $http({
                method: 'GET',
                url: '/grade/edit?id=' + $state.params.grade_id
            }).then(function successCallback(response) {
                console.log(response);
                $scope.grade = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Grade', 'cannot get all the Grades! try reloading page.');
            });
        };

        $scope.saveEditGrade = function() {
            data = $scope.grade;
            $http({
                method: 'POST',
                url: '/grade/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Grade', 'Saved Successfully.');
                $state.go('schools.grades_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'Grade', error.data.message);
            });
        };

        $scope.cancelGradesList = function() {
            $state.go('schools.grades_list');
        };
    }
]);
