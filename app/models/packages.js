var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Package = db.define('packages', {
    school_type: ['district', 'school'],
    package_name: String,
    price: {
        type: 'integer'
    },
    period: String,
    status: ['0', '1']
}, {
    timestamp: true,
    methods: {

    }
});