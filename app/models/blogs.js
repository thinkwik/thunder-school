var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Blogs = db.define('blogs', {
    blog_name: String,
    blog_content: String,
    image_url: String,
    status: ['0', '1']
}, {
    timestamp: true,
    methods: {

    }
});
