var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Attendance = db.define('attendance', {
    school_id: {
        type: 'integer'
    },
    staff_id: {
        type: 'integer'
    },
    grade_id: {
        type: 'integer'
    },
    subject_id: {
        type: 'integer'
    },
    class_id: {
        type: 'integer'
    },
    date: String,
    description: String
}, {
    timestamp: true,
    methods: {

    }
});
