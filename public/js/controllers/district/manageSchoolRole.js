app.controller('districtRoleListController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
	function($scope, $http, $state, toaster, $cookieStore) {
		$scope.getAllRoleList = function() {
			data = {
				district_id: $cookieStore.get('loggedInDistrict').id
			};
			$http({
				method: 'POST',
				url: '/school/role/getAllRoleDistrict',
				data: data
			}).then(function successCallback(response) {
				$scope.schoolroles = response.data;
			}, function errorCallback(error) {
				toaster.pop('error', 'Schools', error.data.message);
			});
		};
		$scope.addNewRoleRedirect = function() {
			$state.go('districts.school_roles_create');
		};
		$scope.activeRole = function(role_id) {
			var data = {
				id: role_id
			};
			$http({
				method: 'POST',
				url: '/school/role/active',
				data: data
			}).then(function successCallback(response) {
				$scope.getAllRoleList();
			}, function errorCallback(error) {
				toaster.pop('error', 'Role', error.data.message);
			});
		};
		$scope.inactiveRole = function(role_id) {
			var data = {
				id: role_id
			};
			$http({
				method: 'POST',
				url: '/school/role/inactive',
				data: data
			}).then(function successCallback(response) {
				$scope.getAllRoleList();
			}, function errorCallback(error) {
				toaster.pop('error', 'Role', error.data.message);
			});
		};
		$scope.deleteRole = function(role_id) {
			var data = {
				id: role_id
			};
			$http({
				method: 'POST',
				url: '/school/role/delete',
				data: data
			}).then(function successCallback(response) {
				$scope.getAllRoleList();
			}, function errorCallback(error) {
				toaster.pop('error', 'Role', error.data.message);
			});
		};
		$scope.editRole = function(role_id) {
			$state.go('districts.school_roles_edit', {
				role_id: role_id
			});
		};
	}
]);

app.controller('districtRoleCreateController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
	function($scope, $http, $state, toaster, $cookieStore) {
		$scope.getDistrictSchools = function() {
			var data = {
				district_id: $cookieStore.get('loggedInDistrict').id
			};
			$http({
				method: 'POST',
				url: '/school/getDistrictSchools',
				data: data
			}).then(function successCallback(response) {
				$scope.schools = response.data;
			}, function errorCallback(error) {
				toaster.pop('error', 'Schools', error.data.message);
			});
		};
		$scope.createRole = function() {
			var data = $scope.role;
			var modules = data.modules;
			modules = $.map(modules, function(value, index) {
				return [index];
			});
			data.modules = modules.join(',');
			$http({
				method: 'POST',
				url: '/school/role/register',
				data: data
			}).then(function successCallback(response) {
				toaster.pop('success','Role','Added Successfully.');
				$state.go('districts.school_roles_list');
			}, function errorCallback(error) {
				toaster.pop('error', 'Schools', error.data.message);
			});
		};
		$scope.cancelRole = function() {
			$state.go('districts.school_roles_list');
		};
	}
]);

app.controller('districtRoleEditController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
	function($scope, $http, $state, toaster, $cookieStore) {
		$scope.getEditRole = function() {
			var data = {
				district_id: $cookieStore.get('loggedInDistrict').id
			};
			$http({
				method: 'POST',
				url: '/school/getDistrictSchools',
				data: data
			}).then(function successCallback(response) {
				$scope.schools = response.data;
				$http({
					method: 'GET',
					url: '/school/role/edit?id=' + $state.params.role_id
				}).then(function successCallback(response) {
					$scope.role = response.data;
					var arr = response.data.modules;
					var array1 = arr.split(',');
					var data = {};
					for (var i = 0; i < array1.length; i++) {
						data[array1[i]] = true;
					}
					$scope.role.modules = data;
				}, function errorCallback(error) {
					toaster.pop('error', 'Schools', error.data.message);
				});
			}, function errorCallback(error) {
				toaster.pop('error', 'Schools', error.data.message);
			});
		};
		$scope.editSaveRole = function() {
			var data = $scope.role;
			var modules = data.modules;
			modules = $.map(modules, function(value, index) {
				if(value){
					return [index];
				}
			});
			data.modules = modules.join(',');
			$http({
				method: 'POST',
				url: '/school/role/edit',
				data: data
			}).then(function successCallback(response) {
				toaster.pop('success', 'Role', 'Updated successfully.');
				$state.go('districts.school_roles_list');
			}, function errorCallback(error) {
				toaster.pop('error', 'Role', error.data.message);
			});
		};
		$scope.cancelRole = function() {
			$state.go('districts.school_roles_list');
		};
	}
]);
