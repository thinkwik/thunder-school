app.controller('createDistrictSubjectController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        var district = $cookieStore.get('loggedInDistrict');
        if (typeof district != 'undefined') {
            var data = {
                district_id: district.id
            };
            $http({
                method: 'POST',
                url: '/school/getDistrictSchools',
                data: data
            }).then(function successCallback(response) {
                $scope.schools = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'District', 'Something went wrong!');
            });
        } else {
            toaster.pop('error', 'Login', 'something went wrong! Please try re-login');
        }

        $scope.addSubject = function() {
            $state.go('districts.subject_create');
        };

        $scope.cancelSubjectForm = function() {
            $state.go('districts.subject_list');
        };

        $scope.getSchoolDepartments = function() {
            var data = {
                school_id: $scope.subject.school_id
            };
            $http({
                method: 'POST',
                url: '/school/department/getAllSchoolDepartment',
                data: data
            }).then(function successCallback(response) {
                $scope.departments = response.data;
                $scope.getSchoolGrades();
                $scope.getSchoolSessions();
            }, function errorCallback(error) {
                toaster.pop('error', 'District', 'Something went wrong!');
            });
        };

        $scope.getSchoolSessions = function() {
            var data = {
                school_id: $scope.subject.school_id
            };
            $http({
                method: 'POST',
                url: '/session/getAllSchoolSessions',
                data: data
            }).then(function successCallback(response) {
                $scope.Sessions = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'District', 'Something went wrong!');
            });
        };

        $scope.getSchoolGrades = function() {
            var data = {
                school_id: $scope.subject.school_id
            };
            $http({
                method: 'POST',
                url: '/grade/getAllSchoolGrades',
                data: data
            }).then(function successCallback(response) {
                $scope.grades = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'District', 'Something went wrong!');
            });
        };

        $scope.createSubject = function() {
            data = $scope.subject;
            $http({
                method: "POST",
                url: "/subject/register",
                data: data
            }).then(function successCallback(response) {
                $scope.cancelSubjectForm();
                toaster.pop('success', 'Subject', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Subject', error.data.message);
            });
        };
    }
]);

app.controller('listDistrictSubjectController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.addNewSubject = function() {
            $state.go('districts.subject_create');
        };

        $scope.inactiveSubject = function(subject_id) {
            var data = {
                id: subject_id
            };
            $http({
                method: 'POST',
                url: '/subject/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllDistrictSubject();
            }, function errorCallback(error) {
                toaster.pop('error', 'Subject', error.data.message);
            });
        };

        $scope.activeSubject = function(subject_id) {
            var data = {
                id: subject_id
            };
            $http({
                method: 'POST',
                url: '/subject/active',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllDistrictSubject();
            }, function errorCallback(error) {
                toaster.pop('error', 'Subject', error.data.message);
            });
        };

        $scope.deleteSubject = function(subject_id) {
            var data = {
                id: subject_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/subject/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllDistrictSubject();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Subject', error.data.message);
                });
            }
        };

        $scope.getAllDistrictSubject = function() {
            var data = {
                district_id: $cookieStore.get('loggedInDistrict').id
            };
            $http({
                method: 'POST',
                url: '/subject/getAllDistrictSubjects',
                data: data
            }).then(function successCallback(response) {
                var subjects = response.data;
                $scope.subjects = subjects;
            }, function errorCallback(error) {
                toaster.pop('error', 'Subject', 'cannot get all the Subjects! try reloading page.');
            });
        };

        $scope.editSubject = function(subject_id) {
            $state.go('districts.subject_edit', {
                subject_id: subject_id
            });
        };
    }
]);

app.controller('editDistrictSubjectController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.getSchools = function() {
            var district = $cookieStore.get('loggedInDistrict');
            if (typeof district != 'undefined') {
                var data = {
                    district_id: district.id
                };
                $http({
                    method: 'POST',
                    url: '/school/getDistrictSchools',
                    data: data
                }).then(function successCallback(response) {
                    $scope.schools = response.data;
                    $scope.getSchoolDepartments();
                    $scope.getSchoolGrades();

                }, function errorCallback(error) {
                    toaster.pop('error', 'District', 'Something went wrong!');
                });
            } else {
                toaster.pop('error', 'Login', 'something went wrong! Please try re-login');
            }
        };

        $scope.getSchoolDepartments = function() {
            var data = {
                district_id: $cookieStore.get('loggedInDistrict').id
            };
            $http({
                method: 'POST',
                url: '/school/department/getAllDistDepartment',
                data: data
            }).then(function successCallback(response) {
                $scope.departments = response.data;
                $scope.getSchoolSessions();
            }, function errorCallback(error) {
                toaster.pop('error', 'District', 'Something went wrong!');
            });
        };


        $scope.getSchoolSessions = function() {
            var data = {
                district_id: $cookieStore.get('loggedInDistrict').id
            };
            $http({
                method: 'POST',
                url: '/session/getAllDistSessions',
                data: data
            }).then(function successCallback(response) {
                $scope.Sessions = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'District', 'Something went wrong!');
            });
        };

        $scope.getSchoolGrades = function() {
            var data = {
                district_id: $cookieStore.get('loggedInDistrict').id
            };
            $http({
                method: 'POST',
                url: '/grade/getAllDistrictGrades',
                data: data
            }).then(function successCallback(response) {
                $scope.grades = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'District', 'Something went wrong!');
            });
        };

        $scope.getEditSubject = function() {
            $http({
                method: 'GET',
                url: '/subject/edit?id=' + $state.params.subject_id
            }).then(function successCallback(response) {
                $scope.subject = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Subject', 'cannot get all the Subjects! try reloading page.');
            });
        };

        $scope.saveEditSubject = function() {
            data = $scope.subject;
            $http({
                method: 'POST',
                url: '/subject/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Subject', 'Saved Successfully.');
                $state.go('districts.subject_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'Subject', error.data.message);
            });
        };

        $scope.cancelSubjectForm = function() {
            $state.go('districts.subject_list');
        };
    }
]);
