var bCrypt = require('bcrypt-nodejs');
var db = require('orm').db,
    Exam_QA = db.models.exam_qa;

exports.register = function(req, res) {
    if (typeof req.body.exam_id == 'undefined') {
        return sendError(req, res, 422, 'required parameter not fullfiled for register Exam');
    }
    var new_exam_qa = new Exam_QA();
    new_exam_qa.exam_id = req.body.exam_id;
    new_exam_qa.question = req.body.question;
    new_exam_qa.type = req.body.type;
    new_exam_qa.option_a = req.body.option_a;
    new_exam_qa.option_b = req.body.option_b;
    new_exam_qa.option_c = req.body.option_c;
    new_exam_qa.option_d = req.body.option_d;
    new_exam_qa.answer = req.body.answer;
    new_exam_qa.max_mark = req.body.max_mark;
    new_exam_qa.save(function(err, saved_exam_qa) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        res.send(saved_exam_qa);
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'Appointment id required!!');
    }
    Exam_QA.find({
        id: req.body.id
    }, 1, function(err, exam_qas) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        exam_qas[0].remove(function(err) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(exam_qas[0]);
        });
    });
};

exports.getAllQuestionAnswer = function(req, res) {
    if (req.body.exam_id == 'undefined' || req.body.exam_id === null) {
        return sendError(req, res, 422, "Exam Id required");
    }
    var query_str = "SELECT t1.* FROM `exam_qa` as t1  WHERE t1.exam_id = ?";
    db.driver.execQuery(query_str, [req.body.exam_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
