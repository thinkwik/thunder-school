var db = require('orm').db;

exports.getAllParticipate = function(req, res) {
    if (typeof req.body.school_id != 'undefined' && typeof req.body.unique_id != 'undefined') {
        var query_str = `
    		SELECT *
			FROM (
			    SELECT
			        CONCAT(id) as id,
			        CONCAT(unique_id) as unique_id,
			        CONCAT(first_name," ",last_name, " - (Staff)") as name,
			        email_id as email,"staff" as type,
			        school_id
			      FROM staff
			      UNION
			      SELECT
			        CONCAT(id) as id,
			        CONCAT(unique_id) as unique_id,
			        CONCAT(first_name," ",last_name, " - (Student)") as name,
			        email,
			        "student" as type,
			        school_id
			      FROM admissions
			      UNION
			      SELECT
			        CONCAT(t1.id) as id,
			        CONCAT(t1.unique_id) as unique_id,
			        CONCAT(t1.full_name, " - (Parent)") as name,
			        t1.email,
			        "parent" as type,
			        t2.school_id as school_id
			      FROM parents as t1
			      LEFT JOIN admissions as t2 ON t1.student_id = t2.id
			      ) as final
			WHERE final.school_id = ? AND final.unique_id <> ?
    	`;
        db.driver.execQuery(query_str, [req.body.school_id, req.body.unique_id], function(err, rows) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            return res.send(rows);
        });
    } else {
        return sendError(req, res, 422, 'please check your parameters');
    }
};

exports.myConferanceList = function(req, res) {
    if (typeof req.body.unique_id != 'undefined') {
        var query_str = `SELECT t2.* FROM chat_room_participants as t1
						LEFT JOIN chat_rooms as t2 ON t1.chat_room_id = t2.id
						WHERE unique_id = ? GROUP BY t2.id ORDER BY t2.id DESC LIMIT 5`;
        db.driver.execQuery(query_str, [req.body.unique_id], function(err, rows) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            return res.send(rows);
        });
    } else {
        return sendError(req, res, 422, 'please check your parameters');
    }


};
