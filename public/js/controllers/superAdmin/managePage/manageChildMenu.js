app.controller('listChildMenusController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.addNewMenu = function() {
            $state.go('users.child_menus_create');
        };
        $scope.getAllChildMenus = function(){
            $http({
                method: 'POST',
                url: '/menus/getAllChildMenus'
            }).then(function successCallback(response) {
                $scope.childMenus = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Menus', error.data.message);
            });
        };
        $scope.inactiveChildMenu = function(child_menu_id) {
            var data = {
                id: child_menu_id
            };
            $http({
                method: 'POST',
                url: '/menus/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllChildMenus();
            }, function errorCallback(error) {
                toaster.pop('error', 'Menus', error.data.message);
            });
        };
        $scope.activeChildMenu = function(child_menu_id) {
            var data = {
                id: child_menu_id
            };
            $http({
                method: 'POST',
                url: '/menus/active',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllChildMenus();
            }, function errorCallback(error) {
                toaster.pop('error', 'Menus', error.data.message);
            });
        };
        $scope.deleteChildMenu = function(child_menu_id) {
            var data = {
                id: child_menu_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/menus/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllChildMenus();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Menus', error.data.message);
                });
            }
        };
        $scope.editChildMenu = function(child_menu_id) {
            $state.go('users.child_menus_edit', {
                child_menu_id: child_menu_id
            });
        };
    }
]);

app.controller('editChildMenusController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
      $http({
          method: "POST",
          url: "/parent/menus/getAllParentMenus"
      }).then(function successCallback(response) {
        $scope.ParentMenus = response.data;
      }, function errorCallback(error) {
          toaster.pop('error', 'Product', error.data.message);
      });

        $scope.SaveChildMenu = function() {
            var data = $scope.menu;
            $http({
                method: 'POST',
                url: '/menus/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Menus', 'updated successfully.');
                $state.go('users.child_menus_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'Menus', error.data.message);
            });
        };
        $scope.getEditChildMenu = function() {
            $http({
                method: 'GET',
                url: '/menus/edit?id=' + $state.params.child_menu_id
            }).then(function successCallback(response) {
                $scope.menu = response.data;
                }, function errorCallback(error) {
                toaster.pop('error', 'Menu', error.data.message);
            });
        };
        $scope.cancelMenuPage = function() {
            $state.go('users.child_menus_list');
        };
    }

]);

app.controller('createChildMenusController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
      $http({
          method: "POST",
          url: "/parent/menus/getAllParentMenus"
      }).then(function successCallback(response) {
        $scope.ParentMenus = response.data;
      }, function errorCallback(error) {
          toaster.pop('error', 'Product', error.data.message);
      });

      $scope.cancelMenuPage = function() {
          $state.go('users.child_menus_list');
      };
        $scope.createMenu = function() {
            var data = $scope.menu;
            $http({
                method: 'POST',
                url: '/menus/register',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Menus', 'added successfully.');
                $state.go('users.child_menus_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'Menus', error.data.message);
            });
        };
    }
]);
