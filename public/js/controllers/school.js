app.controller('schoolController', ['$scope', '$http', '$state',
    function($scope, $http, $state) {
        $scope.cancel = function() {
            $state.go('users.school_list');
        };
        $scope.addNewPayment = function() {
            $state.go('users.payment_create');
        };
        $scope.cancelSchoolList = function() {
            $state.go('users.payment_list');
        };
        $scope.addNewPage = function() {
            $state.go('users.pages_create');
        };
        $scope.cancelPageList = function() {
            $state.go('users.pages_list');
        };
        $scope.addNewMenu = function() {
            $state.go('users.menu_create');
        };
        $scope.cancelMenuList = function() {
            $state.go('users.menu_list');
        };
        $scope.addNewBanner = function() {
            $state.go('users.banner_create');
        };
        $scope.cancelBannerList = function() {
            $state.go('users.banner_list');
        };
        $scope.addNewPackage = function() {
            $state.go('users.package_create');
        };
        $scope.cancelPackage = function() {
            $state.go('users.package_list');
        };
        $scope.addNewBlog = function() {
            $state.go('users.blogs_create');
        };
        $scope.cancelBlog = function() {
            $state.go('users.blogs_list');
        };
        $scope.editSchool = function() {
            $state.go('users.district_edit');
        };
    }
]);


app.controller('newSchoolAddController', ['$scope', '$http', '$state', 'toaster',
    function($scope, $http, $state, toaster) {
        $scope.cancel = function() {
            $state.go('users.district_list');
        };

        $scope.districts = [];
        $scope.districtSelect = false;
        $scope.createSchool = function() {
            $scope.school.parent_id = ($scope.school.school_type == 'school') ? $scope.school.parent_id : null;
            var data = $scope.school;
            $http({
                method: 'POST',
                url: '/school/register',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'District', 'Created Successfully.');
                $state.go('users.district_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'District', 'cannot get all the districts! try reloading page.');
            });
        };
        $scope.schoolTypeChange = function(school_type) {
            if (school_type == 'school') {
                $scope.districtSelect = true;
                var data = {
                    offset: 'fdfdsfdfs'
                };
                $http({
                    method: 'POST',
                    url: '/school/getAllDestricts',
                    data: data
                }).then(function successCallback(response) {
                    $scope.districts = response.data;
                }, function errorCallback(error) {
                    toaster.pop('error', 'District', 'cannot get all the districts! try reloading page.');
                });
            } else {
                $scope.districts = [];
                $scope.districtSelect = false;
            }
        };
    }
]);



app.controller('schoolManageController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.schools = [];
        $scope.cancelSchool = function() {
            $state.go("users.school_list");
        };
        $scope.goToDashboard = function() {
            $state.go("users.dashboard");
        };

        $scope.getAllSchoolList = function() {
            $http({
                method: 'POST',
                url: '/school/getAllSuperAdminSchools'
            }).then(function successCallback(response) {
                $scope.schools = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Super Admin', 'cannot get all the schools! try reloading page.');
            });
        };
        $scope.loginSchool = function(school_id) {
            var data = {
                id: school_id
            };
            $http({
                method: 'POST',
                url: '/school/tryLogin',
                data: data
            }).then(function successCallback(response) {
                $cookieStore.put('loggedInSchool', response.data);
                // $state.go('schools.dashboard');
                window.open('/schools/school/dashboard', '_blank');
            }, function errorCallback(error) {
                toaster.pop('error', 'School', error.data.message);
            });
        };
        $scope.inactiveSchool = function(school_id) {
            var data = {
                id: school_id
            };
            $http({
                method: 'POST',
                url: '/school/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllSchoolList();
            }, function errorCallback(error) {
                toaster.pop('error', 'School', error.data.message);
            });
        };

        $scope.activeSchool = function(school_id) {
            var data = {
                id: school_id
            };
            $http({
                method: 'POST',
                url: '/school/active',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllSchoolList();
            }, function errorCallback(error) {
                toaster.pop('error', 'School', error.data.message);
            });
        };

        $scope.getEditSchool = function() {
            $http({
                method: 'GET',
                url: '/school/edit?id=' + $state.params.school_id
            }).then(function successCallback(response) {
                $scope.school = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'School', error.data.message);
            });
        };

        $scope.deleteSchool = function(school_id) {
            var data = {
                id: school_id
            };
            $http({
                method: 'POST',
                url: '/school/delete',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllSchoolList();
            }, function errorCallback(error) {
                toaster.pop('error', 'School', error.data.message);
            });
        };

        $scope.editSchool = function(school_id) {
            $state.go('users.school_edit', {
                school_id: school_id
            });
        };

        $scope.saveEditSchool = function(school) {
            var data = $scope.school;
            $http({
                method: 'POST',
                url: '/school/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'School', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'School', error.data.message);
            });
        };

    }
]);


app.controller('districtManageController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.districts = [];

        $scope.cancelDistrict = function() {
            $state.go("users.district_list");
        };

        $scope.getAllDistrictList = function() {
            var data = {
                temp: '123123'
            };
            $http({
                method: 'POST',
                url: '/school/getAllDestricts',
                data: data
            }).then(function successCallback(response) {
                $scope.districts = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'District', 'cannot get all the district! try reloading page.');
            });
        };

        $scope.inactiveSchool = function(school_id) {
            var data = {
                id: school_id
            };
            $http({
                method: 'POST',
                url: '/school/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllDistrictList();
            }, function errorCallback(error) {
                toaster.pop('error', 'District', error.data.message);
            });
        };

        $scope.activeSchool = function(school_id) {
            var data = {
                id: school_id
            };
            $http({
                method: 'POST',
                url: '/school/active',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllDistrictList();
            }, function errorCallback(error) {
                toaster.pop('error', 'District', error.data.message);
            });
        };

        $scope.deleteSchool = function(school_id) {
            var data = {
                id: school_id
            };
            $http({
                method: 'POST',
                url: '/school/delete',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllDistrictList();
            }, function errorCallback(error) {
                toaster.pop('error', 'District', error.data.message);
            });
        };

        $scope.editDestict = function(school_id) {
            $state.go('users.district_edit', {
                school_id: school_id
            });
        };

        $scope.getEditDistrict = function() {
            $http({
                method: 'GET',
                url: '/school/edit?id=' + $state.params.school_id
            }).then(function successCallback(response) {
                $scope.school = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'District', error.data.message);
            });
        };

        $scope.saveEditDistrict = function(school) {
            var data = $scope.school;
            $http({
                method: 'POST',
                url: '/school/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'District', 'Saved Successfully.');
                $state.go("users.district_list");
            }, function errorCallback(error) {
                toaster.pop('error', 'District', error.data.message);
            });
        };

        $scope.loginDistrict = function(school_id) {
            var data = {
                id: school_id
            };
            $http({
                method: 'POST',
                url: '/district/tryLogin',
                data: data
            }).then(function successCallback(response) {
                $cookieStore.put('loggedInDistrict', response.data);
                window.open('/districts/school/dashboard', '_blank');
            }, function errorCallback(error) {
                toaster.pop('error', 'District', error.data.message);
            });
        };
    }
]);

app.controller('logOutController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.logout = function(school_id) {
            var data = {
                temp: 'dssadas'
            };
            $http({
                method: 'POST',
                url: '/logout',
                data: data
            }).then(function successCallback(response) {
                angular.forEach($cookieStore, function(v, k) {
                    $cookieStore.remove(k);
                });
                $cookieStore.remove('loggedInParent');
                $cookieStore.remove('loggedInStudent');
                $cookieStore.remove('loggedInStaff');
                $cookieStore.remove('loggedInUser');
                $cookieStore.remove('loggedInDistrict');
                $cookieStore.remove('loggedInSchool');
            }, function errorCallback(error) {
                toaster.pop('error', 'Logout', 'Something went wrong!');
            });
        };
    }
]);
