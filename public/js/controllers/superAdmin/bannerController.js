app.controller('bannerController', ['$scope', '$http', '$state', 'toaster', 'Upload',
    function($scope, $http, $state, toaster, Upload) {
        $scope.saveDisable = false;
        $scope.createBanner = function(event_id) {
            var data = {
                banner_title: $scope.banner_title,
                banner_order: $scope.banner_order,
                banner_image: $scope.image_path
            };
            $http({
                method: 'POST',
                url: '/banner/register',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', "Banner", "Banner created successfully!");
                $state.go('users.banner_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'Banner', error.data.message);
            });
        };

        $scope.uploadFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected !');
            Loading(true);
            Upload.upload({
                url: '/banner/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.image_path = success.data;
                },
                function(error) {
                    toaster.pop('error', "Error", error.data.message);
                    Loading(false);
                });
        };

        $scope.cancelBanner = function() {
            $state.go('users.banner_list');
        };
    }
]);

app.controller('editBannerController', ['$scope', '$http', '$state', 'toaster','Upload',
    function($scope, $http, $state, toaster, Upload) {
        $scope.saveEditBanner = function(banner_id) {
            var data = {
                'id': $scope.banner.id,
                'banner_title': $scope.banner.title,
                'blog_order': $scope.banner.order
            };
            if ($scope.image_path) {
                data.image_path = $scope.image_path;
            }
            $http({
                method: 'POST',
                url: '/banner/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Banner', 'Saved Successfully.');
                $state.go('users.banner_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'Banner', error.data.message);
            });
        };

        $scope.uploadFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected !');
            Loading(true);
            Upload.upload({
                url: '/banner/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.image_path = success.data;
                },
                function(error) {
                    toaster.pop('error', "Error", error.data.message);
                    Loading(false);
                });
        };


        $scope.getEditBanner = function() {
            var data = $scope.banner;
            $http({
                method: 'GET',
                url: '/banner/edit?id=' + $state.params.banner_id,
                data: data
            }).then(function successCallback(response) {
                $scope.banner = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Banner', error.data.message);
            });
        };
    }
]);

app.controller('listBannerController', ['$scope', '$http', '$state', 'toaster',
    function($scope, $http, $state, toaster) {

        $scope.addNewBanner = function() {
            $state.go('users.banner_create');
        };

        $scope.editBanner = function(banner_id) {
            $state.go('users.banner_edit', {
                banner_id: banner_id
            });
        };

        $scope.listBanner = function() {
            var data = {
                offset: 'fdfdsfdfs'
            };
            $http({
                method: 'POST',
                url: '/banner/getAllBanners',
                data: data
            }).then(function successCallback(response) {
                var banners = response.data;
                $scope.banners = banners;
            }, function errorCallback(error) {
                toaster.pop('error', 'Banner', 'cannot get all the banners! try reloading page.');
                console.log(error.data.message);
            });
        };


        $scope.activeBanner = function(banner_id) {
            var data = {
                id: banner_id
            };
            $http({
                method: 'POST',
                url: '/banner/active',
                data: data
            }).then(function successCallback(response) {
                $scope.listBanner();
            }, function errorCallback(error) {
                console.log(error.data.message);
            });
        };

        $scope.deleteBanner = function(banner_id) {
            var data = {
                id: banner_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/banner/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.listBanner();
                }, function errorCallback(error) {
                    console.log(error.data.message);
                });
            }
        };

        $scope.inactiveBanner = function(banner_id) {
            var data = {
                id: banner_id
            };
            $http({
                method: 'POST',
                url: '/banner/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.listBanner();
            }, function errorCallback(error) {
                console.log(error.data.message);
            });
        };

    }
]);
