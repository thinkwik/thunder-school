var bCrypt = require('bcrypt-nodejs');
var fs = require('fs');
var db = require('orm').db,
    Homework = db.models.homeworks,
    HomeworkReports = db.models.homework_reports,
    Follow = db.models.followers,
    Post = db.models.posts;

exports.apiGetList = function(req, res) {
    var type_str = 'student,staff,parent';
    if (typeof req.body.logged_uid == 'undefined' || typeof req.body.type == 'undefined') {
        return apiSendError(req, res, 422, 'Please check your parameters.');
    }
    if (type_str.indexOf(req.body.type) == -1) {
        return apiSendError(req, res, 422, 'list type required.');
    }
    var query_str = `
    		SELECT *
			FROM (
			     SELECT
			        id as id,
              photo as profile_image,
              CONCAT(unique_id) as unique_id,
			        CONCAT(first_name," ",last_name) as name,
			        email_id as email,
              'staff' as type,
			        school_id,profile_status
			      FROM staff
			      UNION
			      SELECT
			        id as id,
              image as profile_image,
			        CONCAT(unique_id) as unique_id,
              CONCAT(first_name," ",last_name) as name,
			        email,
			        "student" as type,
              school_id,profile_status
			      FROM admissions
			      UNION
			      SELECT
			        CONCAT(t1.id) as id,
              parentImage as profile_image,
			        CONCAT(t1.unique_id) as unique_id,
			        CONCAT(t1.full_name) as name,
			        t1.email,
			        "parent" as type,
			        t2.school_id as school_id,t1.profile_status
			      FROM parents as t1
			      LEFT JOIN admissions as t2 ON t1.student_id = t2.id
			      ) as final
			WHERE final.unique_id <> ? AND final.type = ?`;
    var final_arr = [];
    db.driver.execQuery(query_str, [req.body.logged_uid, req.body.type], function(err, rows) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 400, "Bad Request check your parameters");
        }
        if (rows.length == 0) {
            return apiSendError(req, res, 404, "No data found.");
        }
        var row_count = 0;
        rows.forEach(function(value) {
            var sql = "select * from followers WHERE follower_id = ? AND following_id = ?";
            db.driver.execQuery(sql, [req.body.logged_uid, value.unique_id], function(error, done) {
                if (error) {
                    return apiSendError(req, res, 404, "No data found.");
                }
                value.is_following = (done.length > 0) ? 1 : 0;
                final_arr.push(value);
                row_count++;
                if (row_count == rows.length) {
                    var response = {};
                    response['data'] = final_arr;
                    response['code'] = 200;
                    return res.send(response);
                }
            });
        });
    });
};

exports.followPerson = function(req, res) {
    if (typeof req.body.follower_id == 'undefined' || typeof req.body.following_id == 'undefined') {
        return apiSendError(req, res, 422, 'Please check your parameters');
    }
    if (req.body.follower_id == req.body.following_id) {
        return apiSendError(req, res, 422, 'You cannot follow your self.');
    }
    Follow.find({
        follower_id: req.body.follower_id,
        following_id: req.body.following_id
    }, 1, function(err, rows) {
        if (err) {
            return apiSendError(req, res, 500, 'Something went wrong!');
        }
        if (rows.length > 0) {
            rows[0].remove(function(err) {
                if (err) {
                    console.log(err);
                    return apiSendError(req, res, 500, 'Something went wrong!');
                }
                var response = {};
                response['data'] = [];
                response['code'] = 200;
                return res.send(response);
            });
        } else {
            var new_follow = new Follow();
            new_follow.follower_id = req.body.follower_id;
            new_follow.following_id = req.body.following_id;
            Follow.create(new_follow, function(err) {
                if (err) {
                    console.log(err);
                    return apiSendError(req, res, 500, "Something went wrong!");
                }
                var response = {};
                response['data'] = [];
                response['code'] = 200;
                return res.send(response);
            });
        }
    });
};

exports.getAllPosts = function(req, res) {
    if (typeof req.body.unique_id == 'undefined') {
        return apiSendError(req, res, 422, "unique_id required!");
    }
    var query_str = `
			SELECT   t1.*,  r.name, r.profile_image, r.type
			FROM posts AS t1
			LEFT JOIN ( SELECT * FROM (
			          SELECT
			            id AS id,
			            CONCAT(unique_id) AS unique_id,
                        photo as profile_image,
			            CONCAT(first_name, " ", last_name) AS NAME,
			            email_id AS email,
			            "staff" AS type,
			            school_id
			          FROM staff
			      UNION
			        SELECT
			          id AS id,
			          CONCAT(unique_id) AS unique_id,
                      image as profile_image,
			          CONCAT(first_name, " ", last_name) AS NAME,
			          email,
			          "student" AS type,
			          school_id
			        FROM admissions
			      UNION
			        SELECT
			          CONCAT(t1.id) AS id,
			          CONCAT(t1.unique_id) AS unique_id,
                      parentImage as profile_image,
			          CONCAT(t1.full_name) AS NAME,
			          t1.email,
			          "parent" AS type,
			          t2.school_id AS school_id
			        FROM parents AS t1
			        LEFT JOIN admissions AS t2 ON  t1.student_id = t2.id
			      ) AS final
			  WHERE 1 ) AS r ON  t1.created_by = r.unique_id
			WHERE t1.created_by IN (SELECT following_id FROM followers WHERE follower_id =?) ORDER BY t1.created_at DESC
	`;
    var final_arr = [];
    db.driver.execQuery(query_str, [req.body.unique_id], function(err, rows) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 500, "Something went wrong!");
        }
        if (rows.length == 0) {
            return apiSendError(req, res, 404, "No data found.");
        }
        var row_count = 0;
        rows.forEach(function(value) {
            var sql = `SELECT
				  (SELECT COUNT(*) FROM likes WHERE post_id = ? ) as total_likes,
				  (SELECT COUNT(*) FROM comments WHERE post_id = ? ) as total_comments,
				  (SELECT COUNT(*) FROM likes WHERE post_id = ? AND like_by = ? ) as is_liked_by_me
				  `;
            db.driver.execQuery(sql, [value.id, value.id, value.id, req.body.unique_id], function(error, done) {
                if (error) {
                    console.log(error);
                    return apiSendError(req, res, 404, "No data found.");
                }
                value.total_likes = done[0].total_likes;
                value.total_comments = done[0].total_comments;
                value.is_liked_by_me = (done[0].is_liked_by_me > 0) ? 1 : 0;
                final_arr.push(value);
                row_count++;
                if (row_count == rows.length) {
                    var response = {};
                    response['data'] = final_arr;
                    response['code'] = 200;
                    return res.send(response);
                }
            });
        });
    });
};

exports.apiAddPost = function(req, res) {
    if (typeof req.body.created_by == 'undefined' || !req.body.created_by) {
        return apiSendError(req, res, 422, "created_by required!");
    }
    if (typeof req.files.post_image == 'undefined' && typeof req.body.description == 'undefined') {
        return apiSendError(req, res, 422, "Please check your parameter.");
    }
    if (req.body.created_by) {
        var new_post = new Post();
        new_post.created_by = req.body.created_by;
        new_post.description = req.body.description;
        var post_image = null;

        if (req.files) {
          if (!req.files.post_image) {
            return apiSendError(req, res, 400, "Bad Request check your parameters");
          }else {
            var file = req.files.post_image;
            fileName = file.path.split('/');
            type = (typeof file.type != 'undefined' && file.type != "") ? "." + file.type.split('/')[1] : "";
            fs.rename(file.path, __dirname + "/../../../public/upload/posts/" + fileName[fileName.length - 1] + type, function(err) {
                if (err) {
                    console.log(err);
                }
            });
            post_image = fileName[fileName.length - 1] + type;
          }
        }


        new_post.photo = post_image;
        new_post.save(function(err, created_post) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 500, "Something went wrong!");
            }
            var query_str = "SELECT t1.* FROM `followers` as t1 WHERE t1.following_id = ? AND t1.follower_id = ?";
            db.driver.execQuery(query_str, [req.body.created_by, req.body.created_by], function(err, rows) {
                if (err) {
                    console.log(err);
                    return sendError(req, res, 422, "Bad Request check your parameters");
                }
                if (!rows.length) {
                    newFollower = new Follow();
                    newFollower.follower_id = req.body.created_by;
                    newFollower.following_id = req.body.created_by;
                    newFollower.save(function(err, created_followers) {
                        if (err) {
                            console.log(err);
                            return sendError(req, res, 422, "Bad Request check your parameters");
                        }
                        console.log("Success!!!");
                    });
                }
            });
            var response = {};
            response['data'] = [];
            response['code'] = 200;
            return res.send(response);
        });
    }
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return apiSendError(req, res, 422, 'Class id required');
    }
    Post.find({
        id: req.body.id
    }, 1, function(err, posts) {
        if (err) {
            return apiSendError(req, res, 400, "Bad Request check your parameters");
        }
        if (posts.length == 0) {
            return apiSendError(req, res, 404, "post not found.");
        }
        posts[0].remove(function(err) {
            if (err) {
                return apiSendError(req, res, 400, "Bad Request check your parameters");
            }
            var response = {};
            response['data'] = [];
            response['code'] = 200;
            return res.send(response);
        });
    });
};

exports.like = function(req, res) {
    if (typeof req.body.post_id == 'undefined' || typeof req.body.like_by == 'undefined') {
        return apiSendError(req, res, 422, 'Please check your parameters');
    }
    var query_str = `SELECT * FROM likes WHERE post_id = ? AND like_by = ?`;
    db.driver.execQuery(query_str, [req.body.post_id, req.body.like_by], function(err, likes) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 404, "No data found.");
        }
        if (likes.length > 0) {
            var delete_str = `DELETE FROM likes WHERE post_id = ? AND like_by = ?`;
            db.driver.execQuery(delete_str, [req.body.post_id, req.body.like_by], function(error, done) {
                if (error) {
                    console.log(error);
                    return apiSendError(req, res, 500, "Something wnt wrong!");
                }
                var response = {};
                response['data'] = [];
                response['code'] = 200;
                return res.send(response);
            });
        } else {
            var inster_str = `INSERT INTO likes(post_id, like_by) VALUES (?,?)`;
            db.driver.execQuery(inster_str, [req.body.post_id, req.body.like_by], function(error, done) {
                if (error) {
                    console.log(error);
                    return apiSendError(req, res, 500, "Something wnt wrong!");
                }
                var response = {};
                response['data'] = [];
                response['code'] = 200;
                return res.send(response);
            });
        }
    });
};

exports.comment = function(req, res) {
    if (typeof req.body.post_id == 'undefined' || typeof req.body.comment_by == 'undefined' || typeof req.body.comment == 'undefined') {
        return apiSendError(req, res, 422, 'Please check your parameters');
    }
    var inster_str = `INSERT INTO comments(post_id, comment, comment_by) VALUES (?,?,?)`;
    db.driver.execQuery(inster_str, [req.body.post_id, req.body.comment, req.body.comment_by], function(error, done) {
        if (error) {
            console.log(error);
            return apiSendError(req, res, 500, "Something wnt wrong!");
        }
        var response = {};
        response['data'] = [];
        response['code'] = 200;
        return res.send(response);
    });
};

exports.apiListComments = function(req, res) {
    if (typeof req.body.post_id == 'undefined') {
        return apiSendError(req, res, 422, 'post id required!');
    }
    var query_str = `
    		SELECT   t1.*,  r.name, r.profile_image,r.type
			FROM comments AS t1
			LEFT JOIN ( SELECT * FROM (
			          SELECT
			            id AS id,
			            CONCAT(unique_id) AS unique_id,
                        photo as profile_image,
			            CONCAT(first_name, " ", last_name) AS NAME,
			            email_id AS email,
			            "staff" AS type,
			            school_id
			          FROM staff
			      UNION
			        SELECT
			          id AS id,
			          CONCAT(unique_id) AS unique_id,
                      image as profile_image,
			          CONCAT(first_name, " ", last_name) AS NAME,
			          email,
			          "student" AS type,
			          school_id
			        FROM admissions
			      UNION
			        SELECT
			          CONCAT(t1.id) AS id,
			          CONCAT(t1.unique_id) AS unique_id,
                      parentImage as profile_image,
			          CONCAT(t1.full_name) AS NAME,
			          t1.email,
			          "parent" AS type,
			          t2.school_id AS school_id
			        FROM parents AS t1
			        LEFT JOIN admissions AS t2 ON  t1.student_id = t2.id
			      ) AS final
			  WHERE 1 ) AS r ON  t1.comment_by = r.unique_id
			WHERE t1.post_id = ? ORDER BY t1.id DESC
    `;
    db.driver.execQuery(query_str, [req.body.post_id], function(error, comments) {
        if (error) {
            console.log(error);
            return apiSendError(req, res, 500, "Something wnt wrong!");
        }
        var response = {};
        response['data'] = comments;
        response['code'] = 200;
        return res.send(response);
    });
};

exports.apiListLikes = function(req, res) {
    if (typeof req.body.post_id == 'undefined') {
        return apiSendError(req, res, 422, 'post id required!');
    }
    var query_str = `
    		SELECT   t1.*,  r.name, r.profile_image,r.type
			FROM likes AS t1
			LEFT JOIN ( SELECT * FROM (
			          SELECT
			            id AS id,
			            CONCAT(unique_id) AS unique_id,
                        photo as profile_image,
			            CONCAT(first_name, " ", last_name) AS name,
			            email_id AS email,
			            "staff" AS type,
			            school_id
			          FROM staff
			      UNION
			        SELECT
			          id AS id,
			          CONCAT(unique_id) AS unique_id,
                      image as profile_image,
			          CONCAT(first_name, " ", last_name) AS name,
			          email,
			          "student" AS type,
			          school_id
			        FROM admissions
			      UNION
			        SELECT
			          CONCAT(t1.id) AS id,
			          CONCAT(t1.unique_id) AS unique_id,
                      parentImage as profile_image,
			          CONCAT(t1.full_name) AS name,
			          t1.email,
			          "parent" AS type,
			          t2.school_id AS school_id
			        FROM parents AS t1
			        LEFT JOIN admissions AS t2 ON  t1.student_id = t2.id
			      ) AS final
			  WHERE 1 ) AS r ON  t1.like_by = r.unique_id
			WHERE t1.post_id = ? ORDER BY t1.id DESC
    `;
    db.driver.execQuery(query_str, [req.body.post_id], function(error, likes) {
        if (error) {
            console.log(error);
            return apiSendError(req, res, 500, "Something wnt wrong!");
        }
        var response = {};
        response['data'] = likes;
        response['code'] = 200;
        return res.send(response);
    });
};

exports.apiFollowerList = function(req, res) {
    if (typeof req.body.unique_id == 'undefined') {
        return apiSendError(req, res, 422, 'unique_id required!');
    }
    var query_str = `
    		SELECT   t1.*,r.id as user_id,  r.name, r.profile_image,r.type
			FROM followers AS t1
			LEFT JOIN ( SELECT * FROM (
			          SELECT
			            id AS id,
			            CONCAT(unique_id) AS unique_id,
                        photo as profile_image,
			            CONCAT(first_name, " ", last_name) AS name,
			            email_id AS email,
			            "staff" AS type,
			            school_id
			          FROM staff
			      UNION
			        SELECT
			          id AS id,
			          CONCAT(unique_id) AS unique_id,
                      image as profile_image,
			          CONCAT(first_name, " ", last_name) AS name,
			          email,
			          "student" AS type,
			          school_id
			        FROM admissions
			      UNION
			        SELECT
			          CONCAT(t1.id) AS id,
			          CONCAT(t1.unique_id) AS unique_id,
                      parentImage as profile_image,
			          CONCAT(t1.full_name) AS name,
			          t1.email,
			          "parent" AS type,
			          t2.school_id AS school_id
			        FROM parents AS t1
			        LEFT JOIN admissions AS t2 ON  t1.student_id = t2.id
			      ) AS final
			  WHERE 1 ) AS r ON  t1.follower_id = r.unique_id
			WHERE t1.following_id = ? ORDER BY t1.id DESC
    `;
    db.driver.execQuery(query_str, [req.body.unique_id], function(error, rows) {
        if (error) {
            console.log(error);
            return apiSendError(req, res, 500, "Something wnt wrong!");
        }
        if(rows.length == 0){
            var response = {};
            response['data'] = [];
            response['code'] = 200;
            return res.send(response);
        }
        var final_arr = [];
        var row_count = 0;
        rows.forEach(function(value) {
            var sql = "select * from followers WHERE follower_id = ? AND following_id = ?";
            db.driver.execQuery(sql, [req.body.unique_id, value.follower_id], function(error, done) {
                if (error) {
                    return apiSendError(req, res, 404, "No data found.");
                }
                value.is_following = (done.length > 0) ? 1 : 0;
                final_arr.push(value);
                row_count++;
                if (row_count == rows.length) {
                  for (var i = 0; i < final_arr.length; i++) {
                    if (final_arr[i].follower_id  === final_arr[i].following_id) {
                      final_arr.splice(i, 1);
                    }
                  }
                    var response = {};
                    response['data'] = final_arr;
                    response['code'] = 200;
                    return res.send(response);
                }
            });
        });
        // var response = {};
        // response['data'] = likes;
        // response['code'] = 200;
        // return res.send(response);
    });
};

exports.apiFollowingList = function(req, res) {
    if (typeof req.body.unique_id == 'undefined') {
        return apiSendError(req, res, 422, 'unique_id required!');
    }
    var query_str = `
    		SELECT   t1.*,r.id as user_id,  r.name, r.profile_image, 1 as is_following ,r.type
			FROM followers AS t1
			LEFT JOIN ( SELECT * FROM (
			          SELECT
			            id AS id,
			            CONCAT(unique_id) AS unique_id,
                        photo as profile_image,
			            CONCAT(first_name, " ", last_name) AS name,
			            email_id AS email,
			            "staff" AS type,
			            school_id
			          FROM staff
			      UNION
			        SELECT
			          id AS id,
			          CONCAT(unique_id) AS unique_id,
                      image as profile_image,
			          CONCAT(first_name, " ", last_name) AS name,
			          email,
			          "student" AS type,
			          school_id
			        FROM admissions
			      UNION
			        SELECT
			          CONCAT(t1.id) AS id,
			          CONCAT(t1.unique_id) AS unique_id,
                      parentImage as profile_image,
			          CONCAT(t1.full_name) AS name,
			          t1.email,
			          "parent" AS type,
			          t2.school_id AS school_id
			        FROM parents AS t1
			        LEFT JOIN admissions AS t2 ON  t1.student_id = t2.id
			      ) AS final
			  WHERE 1 ) AS r ON  t1.following_id = r.unique_id
			WHERE t1.follower_id = ? ORDER BY t1.id DESC
    `;
    db.driver.execQuery(query_str, [req.body.unique_id], function(error, likes) {
        if (error) {
            console.log(error);
            return apiSendError(req, res, 500, "Something wnt wrong!");
        }
        for (var i = 0; i < likes.length; i++) {
          if (likes[i].follower_id == likes[i].following_id) {
              likes.splice(i,1);
          }
        }
        var response = {};
        response['data'] = likes;
        response['code'] = 200;
        return res.send(response);
    });
};
