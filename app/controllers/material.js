var db = require('orm').db,
    Materials = db.models.materials;

exports.register = function(req, res) {
    if (typeof req.body.file_name == 'undefined') {
        return sendError(req, res, 422, 'Please add meterial file');
    }

    newMaterials = new Materials();
    newMaterials.school_id = req.body.school_id;
    newMaterials.staff_id = req.body.staff_id;
    newMaterials.grade_id = req.body.grade_id;
    newMaterials.subject_id = req.body.subject_id;
    newMaterials.title = req.body.title;
    newMaterials.description = req.body.description;
    newMaterials.due_date = req.body.due_date;
    newMaterials.file_name = req.body.file_name;
    newMaterials.cover_pic = (typeof req.body.cover_pic !== undefined) ? req.body.cover_pic : null;
    newMaterials.publisher = (typeof req.body.publisher !== undefined) ? req.body.publisher : null;
    newMaterials.isbn_number = (typeof req.body.isbn_number !== undefined) ? req.body.isbn_number : null;
    newMaterials.author = (typeof req.body.author !== undefined) ? req.body.author : null;
    newMaterials.save(function(err, created_Materials) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, 'Please check your parameters!!');
        }
        return res.send(created_Materials);
    });
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'Material id required!!');
    }
    Materials.find({
        id: req.body.id
    }, 1, function(err, materials) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, 'Please check your parameters!!');
        }
        materials[0].school_id = req.body.school_id;
        materials[0].subject_id = req.body.subject_id;
        materials[0].grade_id = req.body.grade_id;
        materials[0].title = req.body.title;
        materials[0].description = req.body.description;
        materials[0].due_date = req.body.due_date;
        materials[0].file_name = (typeof req.body.file_name == 'undefined' || req.body.file_name === "") ? materials[0].file_name : req.body.file_name;
        materials[0].cover_pic = (typeof req.body.cover_pic == 'undefined' || req.body.cover_pic === "") ? materials[0].cover_pic : req.body.cover_pic;
        materials[0].publisher = (typeof req.body.publisher == 'undefined' || req.body.publisher === "") ? materials[0].publisher : req.body.publisher;
        materials[0].isbn_number = (typeof req.body.isbn_number == 'undefined' || req.body.isbn_number === "") ? materials[0].isbn_number : req.body.isbn_number;
        materials[0].author = (typeof req.body.author == 'undefined' || req.body.author === "") ? materials[0].author : req.body.author;
            
        materials[0].save(function(err, updated_materials) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(updated_materials);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'Appointment id required!!');
    }
    Materials.find({
        id: req.body.id
    }, 1, function(err, materials) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        materials[0].remove(function(err) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(materials[0]);
        });
    });
};

exports.getMaterial = function(req, res) {
    if (typeof req.query.id == 'undefined') {
        return sendError(req, res, 401, "ID required field");
    }
    Materials.find({
        id: req.query.id
    }, 1, function(err, materials) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        if (!materials.length) return sendError(req, res, 404, "Materials not found");
        return res.send(materials[0]);
    });
};

exports.getAllMaterials = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id === null) {
        return sendError(req, res, 422, "School ID required");
    }
    var query_str = `SELECT t1.*,t2.grade,t3.school_name , t4.subject_name
                    FROM materials as t1 
                    LEFT JOIN grades as t2 ON t1.grade_id = t2.id 
                    LEFT JOIN schools as t3 ON t1.school_id = t3.id 
                    LEFT JOIN subjects as t4 ON t1.subject_id = t4.id 
                    WHERE t3.id = ? GROUP BY t1.id`;
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getStudentMaterials = function(req, res) {
    if (typeof req.body.grade_id == 'undefined' || req.body.grade_id === null) {
        return sendError(req, res, 422, "Grade ID required");
    }
    var current_date = new Date().toISOString().split("T")[0].toString();
    var query_str = `SELECT t1.*, CONCAT(t2.first_name, " ", t2.last_name) as staff_name, t3.grade,  t4.subject_name
                     FROM materials as t1
                     LEFT JOIN staff as t2 ON t1.staff_id = t2.id
                     LEFT JOIN grades as t3 ON t1.grade_id = t3.id
                     LEFT JOIN subjects as t4 ON t1.subject_id = t4.id
                     WHERE t1.grade_id = ? AND t1.due_date >=  \'` + current_date + `\'`;

    db.driver.execQuery(query_str, [req.body.grade_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

var fs = require('fs');
exports.uploadFile = function(req, res) {
    var fs = require('fs');
    var materials = __dirname + "/../../public/upload/materials/";
    if (!fs.existsSync(materials)) {
        fs.mkdirSync(materials);
    }
    var file = req.files.file;
    var fileExtension = file.name.split(".")[1];
    if (fileExtension) {
      fileName = file.path.split('/');
      fs.rename(file.path, materials + fileName[fileName.length - 1] +"."+ fileExtension);
      return res.send(fileName[fileName.length - 1] +"."+ fileExtension);
    }
    else {
      return sendError(req, res, 400, "Bad Request check your parameters");
    }

};

exports.getAllDistrictMaterials = function(req, res) {
    if (typeof req.body.district_id == 'undefined' || req.body.district_id === null) {
        return sendError(req, res, 422, "School ID required");
    }
    var query_str = 'SELECT t1.*,t2.grade,t3.school_name FROM materials as t1 LEFT JOIN grades as t2 ON t1.grade_id = t2.id LEFT JOIN schools as t3 ON t1.school_id = t3.id WHERE t3.parent_id = ? GROUP BY t1.id';
    db.driver.execQuery(query_str, [req.body.district_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
