var db = require('orm').db,
    Admissions = db.models.admissions,
    Issues = db.models.issues,
    Staff = db.models.staff,
    Response = db.models.response;

// exports.saveResponse = function(req, res) {
//     if (typeof req.body.id == 'undefined') {
//         return sendError(req, res, 404, 'Issue id required!!');
//     }
//     Issues.find({
//         id: req.body.id
//     }, 1, function(err, issues) {
//         if (err) {
//             console.log(err);
//             return res.send(err);
//         }
//         issues[0].response = req.body.message;
//         issues[0].save(function(err, updated_issues) {
//             if (err) {
//                 console.log(err);
//                 return res.send(err);
//             }
//             return res.send(updated_issues);
//         });
//     });
// };

exports.saveResponse = function(req, res) {
    newResponse = new Response();
    newResponse.issue_id = req.body.issue_id;
    newResponse.student_id = req.body.student_id;
    newResponse.staff_id = req.body.staff_id;
    newResponse.parent_id = req.body.parent_id;
    newResponse.message = req.body.message;
    newResponse.save(function(err, created_response) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        return res.send(created_response);
    });
};
