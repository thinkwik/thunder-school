app.controller('reportTabController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        this.tab = 1;
        this.setTab = function(tabId) {
            this.tab = tabId;
        };
        this.isSet = function(tabId) {
            return this.tab === tabId;
        };
    }
]);

app.controller('createStudentReportController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore','$window','DownloadReport',
    function($scope, $http, $state, toaster, Upload, $cookieStore,$window,DownloadReport) {
        $scope.getAllClasses = function(){
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/class/getAllSchoolClass',
                data : data
            }).then(function successCallback(response) {
                $scope.classes = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Report', error.data.message);
            });
        };
        $scope.generateReport = function() {
            if($scope.class_ids.length == 0) {
                toaster.pop('info','Classes','Select at least one class.');
            } else {
                var data = {
                    classes: $scope.class_ids
                };
                $http({
                    method: 'POST',
                    url: '/generate/student_report',
                    data : data
                }).then(function successCallback(response) {
                    DownloadReport.download(response.data);
                }, function errorCallback(error) {
                    toaster.pop('error', 'Report', error.data.message);
                });
            }
        };
        $scope.getAllClasses();
    }
]);

app.controller('createStaffReportController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore','$window','DownloadReport',
    function($scope, $http, $state, toaster, Upload, $cookieStore,$window,DownloadReport) {
        $scope.generateReport = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/generate/staff_report',
                data : data
            }).then(function successCallback(response) {
                DownloadReport.download(response.data);
            }, function errorCallback(error) {
                toaster.pop('error', 'Report', error.data.message);
            });
        };
        $scope.generateReportParent = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/generate/parent_report',
                data : data
            }).then(function successCallback(response) {
                DownloadReport.download(response.data);
            }, function errorCallback(error) {
                toaster.pop('error', 'Report', error.data.message);
            });
        };
    }
]);
app.controller('studentGradeReportController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore','$window','DownloadReport',
    function($scope, $http, $state, toaster, Upload, $cookieStore,$window,DownloadReport) {
        $scope.getAllSchoolGrades = function(){
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/grade/getAllSchoolGrades',
                data : data
            }).then(function successCallback(response) {
                $scope.grades = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Report', error.data.message);
            });
        };
        $scope.generateReport = function() {
            if($scope.grade_ids.length == 0) {
                toaster.pop('info','Grade','Select at least one grade.');
            } else {
                var data = {
                    grades: $scope.grade_ids
                };
                $http({
                    method: 'POST',
                    url: '/generate/student_grade_report',
                    data : data
                }).then(function successCallback(response) {
                    DownloadReport.download(response.data);
                }, function errorCallback(error) {
                    toaster.pop('error', 'Report', error.data.message);
                });
            }
        };

        $scope.generateHomeworkReport = function() {
            if($scope.grade_ids.length == 0) {
                toaster.pop('info','Grade','Select at least one grade.');
            } else {
                var data = {
                    grades: $scope.grade_ids
                };
                $http({
                    method: 'POST',
                    url: '/generate/student_homework_report',
                    data : data
                }).then(function successCallback(response) {
                    DownloadReport.download(response.data);
                }, function errorCallback(error) {
                    toaster.pop('error', 'Report', error.data.message);
                });
            }
        };
        $scope.getAllSchoolGrades();
    }
]);
app.controller('createReportController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore','$window','DownloadReport',
    function($scope, $http, $state, toaster, Upload, $cookieStore,$window,DownloadReport) {

    }
]);
