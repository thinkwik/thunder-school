app.controller('studentsExamReportController', ['$scope', '$http', '$state', '$cookieStore', 'toaster', '$interval', '$timeout',
    function($scope, $http, $state, $cookieStore, toaster, $interval, $timeout) {
    	$scope.get_question_report = function(){
    		var data = {
    			exam_id : $state.params.exam_id,
    			student_id: $state.params.student_id
    		};
    		$http({
                method: 'POST',
                url: '/exam/questionAnswerReport',
                data:data
            }).then(function successCallback(response) {
              if(response.data.length > 0){
            		$scope.question_report_list = response.data;
            		$scope.student_name = response.data[0].student_name;
            		$scope.total_achived_marks = response.data[0].total_achived_marks;
            		$scope.total_marks = response.data[0].total_marks;
            		$scope.student_image = response.data[0].student_image;
            	}
            }, function errorCallback(error) {
                toaster.pop('error', 'Exam Report', 'Something went wrong! getting Report data');
            });
    	};
    }
]);
app.controller('parentExamReportController', ['$scope', '$http', '$state', '$cookieStore', 'toaster', '$interval', '$timeout',
    function($scope, $http, $state, $cookieStore, toaster, $interval, $timeout) {
        $scope.get_question_report = function(){
            var data = {
                exam_id : $state.params.exam_id,
                student_id: $state.params.student_id
            };
            $http({
                method: 'POST',
                url: '/exam/questionAnswerReport',
                data:data
            }).then(function successCallback(response) {
              if(response.data.length > 0){
                    $scope.question_report_list = response.data;
                    $scope.student_name = response.data[0].student_name;
                    $scope.total_achived_marks = response.data[0].total_achived_marks;
                    $scope.total_marks = response.data[0].total_marks;
                    $scope.student_image = response.data[0].student_image;
                }
            }, function errorCallback(error) {
                toaster.pop('error', 'Exam Report', 'Something went wrong! getting Report data');
            });
        };
    }
]);

app.controller('staffExamReportController', ['$scope', '$http', '$state', '$cookieStore', 'toaster', '$interval', '$timeout',
    function($scope, $http, $state, $cookieStore, toaster, $interval, $timeout) {
        $scope.view_ans_div = false;
        $scope.get_question_report = function(){
            var data = {
                exam_id : $state.params.exam_id,
                student_id: $state.params.student_id
            };
            $http({
                method: 'POST',
                url: '/exam/questionAnswerReport',
                data:data
            }).then(function successCallback(response) {
              if(response.data.length > 0){
                    $scope.question_report_list = response.data;
                    $scope.student_name = response.data[0].student_name;
                    $scope.total_achived_marks = response.data[0].total_achived_marks;
                    $scope.total_marks = response.data[0].total_marks;
                    $scope.student_image = response.data[0].student_image;
                }
            }, function errorCallback(error) {
                toaster.pop('error', 'Exam Report', 'Something went wrong! getting Report data');
            });
        };

        $scope.viewAnswer = function(qa){
            $scope.view_question = qa.question;
            $scope.view_answer = qa.given_answer;
            $scope.view_ans_div = true;
        };

        $scope.updateMarks = function(qa){
            var data = {
                id:qa.id,
                achived_marks : qa.achived_marks,
                exam_id:qa.exam_id,
                student_id:qa.student_id
            };
            $http({
                method: 'POST',
                url: '/exam/updateQuestionAnswerReport',
                data:data
            }).then(function successCallback(response) {
                $scope.get_question_report();
            }, function errorCallback(error) {
                toaster.pop('error', 'Exam Report', 'Something went wrong! getting Report data');
            });
        }
    }
]);