app.controller('districtSchoolCreateController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.districtCreateSchool = function() {
            var data = $scope.district;
            data.parent_id = $cookieStore.get('loggedInDistrict').id;
            data.school_type = 'school';
            $http({
                method: 'POST',
                url: '/district/school/register',
                data: data
            }).then(function successCallback(response) {
                $state.go('districts.district_school_list');
                toaster.pop('success', 'School', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Logout', 'Something went wrong!');
            });
        };
        $scope.cancelDistrictSchool = function() {
            $state.go('districts.district_school_list');
        };
    }
]);

app.controller('districtSchoolListController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.createDistrictSchoolRedirect = function() {
            $state.go('districts.district_school_create');
        };

        $scope.activeSchool = function(school_id) {
            var data = {
                id: school_id
            };
            $http({
                method: 'POST',
                url: '/district/school/active',
                data: data
            }).then(function successCallback(response) {

                $scope.getAllDistrictSchools();
            }, function errorCallback(error) {
                toaster.pop('error', 'Active', 'Something went wrong!');
            });
        };

        $scope.deleteSchool = function(school_id) {
            var data = {
                id: school_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/district/school/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllDistrictSchools();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Delete', 'Something went wrong!');
                });
            }
        };

        $scope.getAllDistrictSchools = function() {
            var data = {
                district_id: $cookieStore.get('loggedInDistrict').id
            };
            $http({
                method: 'POST',
                url: '/district/getAllSchools',
                data: data
            }).then(function successCallback(response) {
                $scope.schools = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'District', 'Something went wrong!');
            });
        };
        $scope.editSchool = function(school_id) {
            $state.go('districts.district_school_edit', {
                school_id: school_id
            });
        };
        $scope.inactiveSchool = function(school_id) {
            var data = {
                id: school_id
            };
            $http({
                method: 'POST',
                url: '/district/school/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllDistrictSchools();
            }, function errorCallback(error) {
                toaster.pop('error', 'Inactive', 'Something went wrong!');
            });
        };

        $scope.activeSchool = function(school_id) {
            var data = {
                id: school_id
            };
            $http({
                method: 'POST',
                url: '/school/active',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllDistrictSchools();
            }, function errorCallback(error) {
                toaster.pop('error', 'School', error.data.message);
            });
        };

        $scope.loginSchool = function(school_id) {
            var data = {
                id: school_id
            };
            $http({
                method: 'POST',
                url: '/school/tryLogin',
                data: data
            }).then(function successCallback(response) {
                $cookieStore.put('loggedInSchool', response.data);
                // $state.go('schools.dashboard');
                window.open('/schools/school/dashboard', '_blank');
            }, function errorCallback(error) {
                toaster.pop('error', 'School', error.data.message);
            });
        };
    }
]);
app.controller('districtSchoolEditController', ['$scope', '$http', '$state', 'toaster',
    function($scope, $http, $state, toaster) {
        $scope.cancelDistrictSchool = function() {
            $state.go('districts.district_school_list');
        };
        $scope.getSchoolEdit = function() {
            $http({
                method: 'GET',
                url: '/school/edit?id=' + $state.params.school_id
            }).then(function successCallback(response) {
                $scope.school = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'School', error.data.message);
            });
        };
        $scope.saveEditDistrictSchool = function() {
            var data = $scope.school;
            $http({
                method: 'POST',
                url: '/school/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Disctict School', "Updated Successfully.");
                $state.go('districts.district_school_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'School', error.data.message);
            });
        };

    }
]);
