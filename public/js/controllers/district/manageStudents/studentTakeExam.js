app.controller('takeExamController', ['$scope', '$http', '$state', '$cookieStore', 'toaster', '$interval', '$timeout',
    function($scope, $http, $state, $cookieStore, toaster, $interval, $timeout) {
        $scope.go_ahead_button = false;
        $scope.exam_div_show = false;
        $scope.save_and_next_button_show = true;
        $scope.exam_finished = false;
        $scope.exam_id = $state.params.exam_id;
        $scope.student_id = $cookieStore.get('loggedInStudent').id;
        $scope.exam_completed_div = false;

        $scope.viewReportRedirect = function(){
            $state.go('students.view_exam_report',{
                exam_id:$scope.exam_id,
                student_id:$scope.student_id
            });
        };
        $scope.verifyStudent = function() {
            var data = {
                exam_id: $scope.exam_id,
                student_id: $scope.student_id
            };
            $http({
                method: 'POST',
                url: '/exam/verifyStudent',
                data: data
            }).then(function successCallback(response) {
                $scope.getExamTime();
            }, function errorCallback(error) {
                $scope.go_ahead_button = false;
            });
        };
        $scope.getExamTime = function() {
            var data = {
                exam_id: $scope.exam_id,
                student_id: $scope.student_id
            };
            $http({
                method: 'POST',
                url: '/exam/getExamTime',
                data: data
            }).then(function successCallback(response) {
                $scope.countdownVal = response.data.seconds;
                $scope.go_ahead_button = true;
            }, function errorCallback(error) {
                $scope.go_ahead_button = false;
            });
        };

        $scope.start_exam = function() {
            if (confirm("Have you read instructions below and Agreed ? ")) {
                Loading(true);
                var data = {
                    exam_id: $scope.exam_id,
                    student_id: $scope.student_id
                };
                $http({
                    method: 'POST',
                    url: '/exam/startExamInit',
                    data: data
                }).then(function successCallback(response) {
                    $timeout(function() {
                        Loading(false);
                        $scope.go_ahead_button = false;
                        $scope.exam_div_show = true;
                        $scope.get_question();
                    }, 2000);
                    $timeout(function() {
                        $scope.$broadcast('timer-start');
                    }, 3400);

                }, function errorCallback(error) {
                    Loading(false);
                    toaster.pop("info", "Questions", error.data.message);
                });
            };
        };

        $scope.get_question = function() {
            Loading(true);
            var data = {
                exam_id: $scope.exam_id,
                student_id: $scope.student_id
            };
            $http({
                method: 'POST',
                url: '/exam/getStudentQuestion',
                data: data
            }).then(function successCallback(response) {
                $timeout(function() {
                    if (response.data == "exam_completed") {
                        $scope.exam_result_cal();
                        $scope.save_and_next_button_show = false;
                    } else {
                        $scope.resetForm(response.data);
                    }
                    Loading(false);
                }, 1000);
            }, function errorCallback(error) {
                Loading(false);
                toaster.pop("error", "Reload Page", "Something went wrong!");
            });
        };

        $scope.finished = function() {
            Loading(true);
            var data = {
                exam_id: $scope.exam_id,
                student_id: $scope.student_id
            };
            $http({
                method: 'POST',
                url: '/exam/forceCompleteExam',
                data: data
            }).then(function successCallback(response) {
                $scope.get_question();
            }, function errorCallback(error) {
                Loading(false);
                toaster.pop("error", "Reload Page", "Something went wrong!");
            });
        };

        $scope.submit_and_next = function() {
            var answer = "";
            if ($scope.current_question_type == "muliple_choice") {
                answer = $scope.muliple_choice_answer;
                if (answer == "") {
                    alert("Please Select any option, Then go ahead.");
                    return false;
                }
            } else {
                answer = $("#essay_answer_text").val();
                console.log(answer);
                if (answer == "") {
                    alert("Please Write something, Then go ahead.");
                    return false;
                }
            }
            if (answer != "") {
                var data = {
                    qa_id: $scope.current_question_id,
                    type: $scope.current_question_type,
                    answer: answer
                };
                $http({
                    method: 'POST',
                    url: '/exam/addStudentAnswer',
                    data: data
                }).then(function successCallback(response) {
                    $scope.get_question();
                }, function errorCallback(error) {
                    toaster.pop("error", "Reload Page", "Something went wrong!");
                });
            }
        };

        $scope.exam_result_cal = function() {
            Loading(true);
            var data = {
                exam_id: $scope.exam_id,
                student_id: $scope.student_id
            };
            $http({
                method: 'POST',
                url: '/exam/calculateExamResult',
                data: data
            }).then(function successCallback(response) {
                Loading(false);
                $scope.exam_completed_div = true;
                $scope.exam_div_show = false;
            }, function errorCallback(error) {
                Loading(false);
                toaster.pop("error", "Reload Page", "Something went wrong!");
            });
        };

        $scope.resetForm = function(new_question) {
            $scope.current_question_id = new_question.qa_id;
            $scope.current_question_type = new_question.type;
            $scope.current_question = new_question.question;
            $scope.option_a = new_question.option_a;
            $scope.option_b = new_question.option_b;
            $scope.option_c = new_question.option_c;
            $scope.option_d = new_question.option_d;
            $scope.muliple_choice_answer = "";
            $scope.essay_answer = "";
            $(".option_class.option_a").css("background-color", "#fff");
            $(".option_class.option_b").css("background-color", "#fff");
            $(".option_class.option_c").css("background-color", "#fff");
            $(".option_class.option_d").css("background-color", "#fff");
        };
        $scope.selectOption = function(val) {
            $scope.muliple_choice_answer = val;
            $(".option_class.option_a").css("background-color", "#fff");
            $(".option_class.option_b").css("background-color", "#fff");
            $(".option_class.option_c").css("background-color", "#fff");
            $(".option_class.option_d").css("background-color", "#fff");
            $(".option_class." + val).css('background-color', "rgb(173, 178, 195)");
        }
    }
]);
