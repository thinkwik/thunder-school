var db = require('orm').db;
var modts = require('orm-timestamps');
School = db.models.schools;

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Event = db.define('events', {
    title: String,
    school_id: {
        type: 'integer'
    },
    start_date: String,
    end_date: String,
    address: String,
    country: String,
    state: String,
    city: String,
    pin_code: String,
    content: String,
    status: ['0', '1']
}, {
    timestamp: true,
    methods: {

    }
});



