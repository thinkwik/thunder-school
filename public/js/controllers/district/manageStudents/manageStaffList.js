app.controller('studentStaffListController', ['$scope', '$http', '$state', '$cookieStore', 'toaster',
    function($scope, $http, $state, $cookieStore, toaster) {
        $scope.getAllStudentClasses = function() {
            data = {
                student_id: $cookieStore.get('loggedInStudent').id,
                grade_id : $cookieStore.get('loggedInStudent').grade_id
            };
            $http({
                method: 'POST',
                url: '/staff/getAllStudentClasses',
                data: data
            }).then(function successCallback(response) {
                $scope.staffs = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Staff', 'Something went wrong! getting data');
            });
        };
    }
]);
