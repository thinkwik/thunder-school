var db = require('orm').db,
    Attendance = db.models.attendance;
AttendanceReports = db.models.attendance_reports;

exports.attend = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'AttendanceReports id required!!');
    }
    AttendanceReports.find({
        id: req.body.id
    }, 1, function(err, reports) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, 'Something went wrong!');
        }
        reports[0].status = (reports[0].status == "0") ? "1" : "0";
        reports[0].save(function(err, saved_report) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, 'AttendanceReports id required!');
            }
            return res.send(saved_report);
        });
    });
};

exports.notAttend = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'AttendanceReports id required!!');
    }
    AttendanceReports.find({
        id: req.body.id
    }, 1, function(err, reports) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, 'Something went wrong!');
        }
        reports[0].status = (reports[0].status == "1") ? "0" : "1";
        reports[0].save(function(err, saved_report) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, 'AttendanceReports id required!');
            }
            return res.send(saved_report);
        });
    });
};

exports.makeAttendAll = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Homework id required!!');
    }

    var query_str = 'UPDATE `attendance_reports` SET `status`= ? WHERE `attendance_id` = ?';
    db.driver.execQuery(query_str, ["1", req.body.id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(200);
    });
};

exports.makeNotAttendAll = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Homework id required!!');
    }

    var query_str = 'UPDATE `attendance_reports` SET `status`= ? WHERE `attendance_id` = ?';
    db.driver.execQuery(query_str, ["0", req.body.id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(200);
    });
};

exports.getAllStudentList = function(req, res) {
    if (req.body.attendance_id == 'undefined') {
        return sendError(req, res, 422, "Attendance ID required");
    }
    var query_str = 'SELECT t1.*, CONCAT(t2.first_name, " ", t2.last_name) as student_name FROM `attendance_reports` as t1  LEFT JOIN admissions as t2 ON t1.student_id = t2.id  WHERE t1.attendance_id = ?';
    db.driver.execQuery(query_str, [req.body.attendance_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getAllStudentAttendence = function(req, res) {
    if (typeof req.body.student_id == 'undefined') {
        return sendError(req, res, 422, "Student ID required");
    }
    var query_str = `SELECT t1.id, t1.status, t2.date, CONCAT(t3.first_name, ' ', t3.last_name) as staff_name, t4.grade, t5.subject_name ,t6.class_name
                        FROM attendance_reports as t1 
                        LEFT JOIN attendance as t2 ON t1.attendance_id = t2.id 
                        LEFT JOIN staff as t3 ON t2.staff_id = t3.id 
                        LEFT JOIN grades as t4 ON t2.grade_id = t4.id 
                        LEFT JOIN subjects as t5 ON t2.subject_id = t5.id 
                        LEFT JOIN classes as t6 ON t2.class_id = t6.id
                        WHERE t1.student_id = ? 
                        ORDER BY t1.id desc 
                        LIMIT 30`;
    db.driver.execQuery(query_str, [req.body.student_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
