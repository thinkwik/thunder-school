app.controller('allEventsListController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.addNewEventRedirect = function() {
            $state.go('users.event_create');
        };
        $scope.eventResources = function(event_id){
            $state.go('users.event_resource_list',{
                event_id:event_id
            });
        };
        $scope.getAllEventList = function() {
            var data = {
                temp: 'dssadas'
            };
            $http({
                method: 'POST',
                url: '/event/getAllEvents',
                data: data
            }).then(function successCallback(response) {
                $scope.events = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Events', 'Something went wrong! getting all events');
            });
        };
        $scope.activeEvent = function(event_id) {
            var data = {
                id: event_id
            };
            $http({
                method: 'POST',
                url: '/event/active',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllEventList();
            }, function errorCallback(error) {
                toaster.pop('error', 'Events', error.data.message);
            });
        };
        $scope.inactiveEvent = function(event_id) {
            var data = {
                id: event_id
            };
            $http({
                method: 'POST',
                url: '/event/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllEventList();
            }, function errorCallback(error) {
                toaster.pop('error', 'Events', error.data.message);
            });
        };
        $scope.deleteEvent = function(event_id) {
            var data = {
                id: event_id
            };
            $http({
                method: 'POST',
                url: '/event/delete',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllEventList();
            }, function errorCallback(error) {
                toaster.pop('error', 'Events', error.data.message);
            });
        };
        $scope.editEvent = function(event_id) {
            $state.go('users.event_edit', {
                event_id: event_id
            });
        };
    }
]);

app.controller('editEventController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.getEvent = function() {
            $http({
                method: 'GET',
                url: '/event/edit?id=' + $state.params.event_id
            }).then(function successCallback(response) {
                $scope.event = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Events', 'Something went wrong! getting event');
            });
        };
        $scope.cancelEvent = function() {
            $state.go('users.events_list');
        };
        $scope.saveEditEvent = function() {
            var data = $scope.event;
            console.log(data);
            $http({
                method: 'POST',
                url: '/event/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Events', "Updated successfully.");
                $state.go('users.events_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'Events', error.data.message);
            });
        };
    }

]);

app.controller('newEventController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.schools = [];
        $scope.getAllUniSchools = function() {
            var data = {
                temp:'dfsf'
            };
            $http({
                method: 'POST',
                url: '/school/getUniversalSchools',
                data: data
            }).then(function successCallback(response) {
                $scope.schools = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Schools', error.message);
            });
        };
        $scope.createEvent = function() {
            var data = $scope.event;
            $http({
                method: 'POST',
                url: '/event/register',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Events', "added successfully.");
                $state.go('users.events_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'Events', error.message);
            });
        };
        $scope.cancelEvent = function() {
            $state.go('users.events_list');
        };
    }
]);
