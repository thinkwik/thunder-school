app.config(['$compileProvider', function($compileProvider) {
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(|blob|):/);
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|file|blob|pdf|doc):/);
}]);

app.controller('listStudentMaterialController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', '$window',
    function($scope, $http, $state, toaster, $cookieStore, $window) {
        $scope.getAllMaterialsForStudents = function() {
            var data = {
                grade_id: $cookieStore.get('loggedInStudent').grade_id
            };
            $http({
                method: 'POST',
                url: '/material/getStudentMaterials',
                data: data
            }).then(function successCallback(response) {
                var materials = response.data;
                $scope.materials = materials;
            }, function errorCallback(error) {
                toaster.pop('error', 'Materials', 'cannot get all the Materials! try reloading page.');
                console.log(error.data.message);
            });
        };

        $scope.DownloadMaterial = function(material) {
            if (material.file_name) {
                var materialFileName = material.file_name;
                materialFileName = materialFileName.split(".");
                var url = $window.location.origin + '/upload/materials/' + material.file_name;
                var request = new XMLHttpRequest();
                request.open('HEAD', url, false);
                request.send();
                if (request.status == 200) {
                    var element = angular.element('<a/>');
                    element.attr({
                        href: url,
                        target: '_blank',
                        download: material.title + '.' + materialFileName[1]
                    })[0].click();
                } else {
                    toaster.pop('error', 'Materials', 'Material not found!');
                    return false;
                }
            } else {
                toaster.pop('error', 'Materials', 'Material not found!');
                return false;
            }
        };
    }
]);

app.controller('createMaterialController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        $scope.material = {};
        $scope.material.cover_pic = null;
        
        $scope.getSchoolGrades = function() {
            var data = {
                school_id: $cookieStore.get('loggedInStaff').school_id
            };
            $http({
                method: 'POST',
                url: '/grade/getAllSchoolGrades',
                data: data
            }).then(function successCallback(response) {
                $scope.grades = response.data;
                //$scope.getSchoolSubjects();
            }, function errorCallback(error) {
                toaster.pop('error', 'Grades', 'Something went wrong!');
            });
        };
        
        $scope.uploadFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected !');
            Loading(true);
            Upload.upload({
                url: '/material/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.filename = success.data;
                    $scope.saveDisable = true;
                },
                function(error) {
                    toaster.pop('error', "Error", error.data.message);
                    $scope.saveDisable = false;
                });
        };

        $scope.uploadCoverFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected !');
            Loading(true);
            Upload.upload({
                url: '/material/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.material.cover_pic = success.data;
                },
                function(error) {
                    Loading(false);
                    toaster.pop('error', "Error", error.data.message);
                });
        };

        $scope.listMaterials = function() {
            $state.go('schoolstaff.list_material');
        };

        $scope.getSchoolSubjects = function(grade_id) {
            $scope.material.subject_id = "";
            var data = {
                grade_id: grade_id,
                school_id: $cookieStore.get('loggedInStaff').school_id
            };
            $http({
                method: 'POST',
                url: '/subject/getSchoolGradesSubjectWise',
                data: data
            }).then(function successCallback(response) {
                $scope.subjects = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Subjects', error.data.message);
            });
        };

        $scope.changeSubjects = function(grade_id) {
            $scope.material.subject_id = "";
            $scope.getSchoolSubjects(grade_id);
        };

        $scope.createMaterials = function() {
            data = $scope.material;
            data.school_id = $cookieStore.get('loggedInStaff').school_id;
            data.staff_id = $cookieStore.get('loggedInStaff').id;
            data.file_name = $scope.filename;
            $http({
                method: "POST",
                url: "/material/register",
                data: data
            }).then(function successCallback(response) {
                $state.go('schoolstaff.list_material');
                toaster.pop('success', 'Material', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Material', error.data.message);
            });
        };
    }
]);

app.controller('listMaterialController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', '$window',
    function($scope, $http, $state, toaster, $cookieStore, $window) {
        $scope.addNewMaterial = function() {
            $state.go('schoolstaff.create_material');
        };

        $scope.deleteMaterials = function(material_id) {
            var data = {
                id: material_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/material/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllMaterials();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Materials', error.data.message);
                });
            }
        };

        $scope.getAllMaterials = function() {
            var data = {
                school_id: $cookieStore.get('loggedInStaff').school_id
            };
            $http({
                method: 'POST',
                url: '/material/getAllMaterials',
                data: data
            }).then(function successCallback(response) {
                var materials = response.data;
                $scope.materials = materials;
            }, function errorCallback(error) {
                toaster.pop('error', 'Materials', 'cannot get all the Materials! try reloading page.');
            });
        };

        $scope.DownloadMaterial = function(material) {
            if (material.file_name) {
                var materialFileName = material.file_name;
                materialFileName = materialFileName.split(".");
                var url = $window.location.origin + '/upload/materials/' + material.file_name;
                var request = new XMLHttpRequest();
                request.open('HEAD', url, false);
                request.send();
                if (request.status == 200) {
                    var element = angular.element('<a/>');
                    element.attr({
                        href: url,
                        target: '_blank',
                        download: material.title + '.' + materialFileName[1]
                    })[0].click();
                } else {
                    toaster.pop('error', 'Materials', 'Material not found!');
                    return false;
                }
            } else {
                toaster.pop('error', 'Materials', 'Material not found!');
                return false;
            }
        };

        $scope.editMaterial = function(material_id) {
            $state.go('schoolstaff.edit_material', {
                material_id: material_id
            });
        };
    }
]);


app.controller('editMaterialController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload', '$window',
    function($scope, $http, $state, toaster, $cookieStore, Upload, $window) {
        $scope.material = {};
        $scope.material.cover_pic = null;
        
        $scope.getSchoolGrades = function() {
            var data = {
                school_id: $cookieStore.get('loggedInStaff').school_id
            };
            $http({
                method: 'POST',
                url: '/grade/getAllSchoolGrades',
                data: data
            }).then(function successCallback(response) {
                $scope.grades = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Grades', 'Something went wrong!');
            });
        };

        $scope.DownloadMaterial = function(material) {
            if (material.file_name) {
                var materialFileName = material.file_name;
                materialFileName = materialFileName.split(".");
                var url = $window.location.origin + '/upload/materials/' + material.file_name;
                var request = new XMLHttpRequest();
                request.open('HEAD', url, false);
                request.send();
                if (request.status == 200) {
                    var element = angular.element('<a/>');
                    element.attr({
                        href: url,
                        target: '_blank',
                        download: material.title + '.' + materialFileName[1]
                    })[0].click();
                } else {
                    toaster.pop('error', 'Materials', 'Material not found!');
                    return false;
                }
            } else {
                toaster.pop('error', 'Materials', 'Material not found!');
                return false;
            }
        };

        $scope.uploadFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected !');
            Loading(true);
            Upload.upload({
                url: '/material/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.filename = success.data;
                    $scope.saveDisable = true;
                },
                function(error) {
                    toaster.pop('error', "Error", error.data.message);
                    $scope.saveDisable = false;
                });
        };

        $scope.uploadCoverFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected !');
            Loading(true);
            Upload.upload({
                url: '/material/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.material.cover_pic = success.data;
                },
                function(error) {
                    Loading(false);
                    toaster.pop('error', "Error", error.data.message);
                });
        };

        $scope.changeSubjects = function(grade_id) {
            $scope.material.subject_id = "";
            $scope.getSchoolSubjects(grade_id);
        };
        $scope.getSchoolSubjects = function(grade_id) {
            var data = {
                grade_id: grade_id,
                school_id: $cookieStore.get('loggedInStaff').school_id
            };
            $http({
                method: 'POST',
                url: '/subject/getSchoolGradesSubjectWise',
                data: data
            }).then(function successCallback(response) {
                $scope.subjects = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Exam', error.data.message);
            });
        };


        $scope.getEditMaterial = function() {
            $http({
                method: 'GET',
                url: '/material/edit?id=' + $state.params.material_id
            }).then(function successCallback(response) {
                $scope.material = response.data;
                $scope.getSchoolGrades();
                $scope.getSchoolSubjects(response.data.grade_id);
            }, function errorCallback(error) {
                toaster.pop('error', 'Materials', 'cannot get all the Materials! try reloading page.');
            });
        };

        $scope.saveEditMaterials = function() {
            data = $scope.material;
            data.file_name = $scope.filename;
            $http({
                method: 'POST',
                url: '/material/edit',
                data: data
            }).then(function successCallback(response) {
                $state.go('schoolstaff.list_material');
                toaster.pop('success', 'Material', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Material', error.data.message);
            });
        };

        $scope.listMaterials = function() {
            $state.go('schoolstaff.list_material');
        };

    }
]);
