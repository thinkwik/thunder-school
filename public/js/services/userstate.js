app.config(['$httpProvider',
    function($httpProvider) {
        $httpProvider.interceptors.push(function($q, $rootScope, $location, $cookieStore) {
            var url = $location.$$url;
            var parts = url.split('/');
            var route = parts[1];
            if (route === "" || route == "users" || route === "school" || route === "schools" || route === "districts") {
                return {
                    'responseError': function(rejection) {
                        var status = rejection.status;
                        var config = rejection.config;
                        var method = config.method;
                        var url = config.url;
                        if (status === 444) {
                            $cookieStore.remove('loggedInUser');
                            $cookieStore.remove('loggedInDistrict');
                            $cookieStore.remove('loggedInSchool');
                            $cookieStore.remove('loggedInStaff');
                            $cookieStore.remove('loggedInStudent');
                            $location.path('/');
                        } else {

                        }
                        return $q.reject(rejection);
                    }
                };
            }
            if (route == "parent") {
                return {
                    'responseError': function(rejection) {
                        var status = rejection.status;
                        var config = rejection.config;
                        var method = config.method;
                        var url = config.url;
                        if (status === 444) {
                            $cookieStore.remove('loggedInUser');
                            $cookieStore.remove('loggedInDistrict');
                            $cookieStore.remove('loggedInSchool');
                            $cookieStore.remove('loggedInStaff');
                            $cookieStore.remove('loggedInStudent');
                            $location.path('/parent/login');
                        } else {

                        }
                        return $q.reject(rejection);
                    }
                };
            }
            if (route == "student") {
                return {
                    'responseError': function(rejection) {
                        var status = rejection.status;
                        var config = rejection.config;
                        var method = config.method;
                        var url = config.url;
                        if (status === 444) {
                            $cookieStore.remove('loggedInUser');
                            $cookieStore.remove('loggedInDistrict');
                            $cookieStore.remove('loggedInSchool');
                            $cookieStore.remove('loggedInStaff');
                            $cookieStore.remove('loggedInStudent');
                            $location.path('/student/login');
                        } else {

                        }
                        return $q.reject(rejection);
                    }
                };
            }
            if (route == "staff") {
                return {
                    'responseError': function(rejection) {
                        var status = rejection.status;
                        var config = rejection.config;
                        var method = config.method;
                        var url = config.url;
                        if (status === 444) {
                            $cookieStore.remove('loggedInUser');
                            $cookieStore.remove('loggedInDistrict');
                            $cookieStore.remove('loggedInSchool');
                            $cookieStore.remove('loggedInStaff');
                            $cookieStore.remove('loggedInStudent');
                            $location.path('/staff/login');
                        } else {

                        }
                        return $q.reject(rejection);
                    }
                };
            }
            return {
                'responseError': function(rejection) {
                    var status = rejection.status;
                    var config = rejection.config;
                    var method = config.method;
                    var url = config.url;
                    if (status === 444) {
                        $cookieStore.remove('loggedInUser');
                        $cookieStore.remove('loggedInDistrict');
                        $cookieStore.remove('loggedInSchool');
                        $cookieStore.remove('loggedInStaff');
                        $cookieStore.remove('loggedInStudent');
                        $location.path('/');
                    } else {

                    }
                    return $q.reject(rejection);
                }
            };
        });

        // $httpProvider.interceptors.push(function($q, $rootScope, $localStorage, $location) {
        //     return {
        //         'request': function(config) {
        //             if (angular.isDefined($localStorage.authToken)) {
        //                 config.headers['Authorization'] = 'Bearer ' + $localStorage.authToken.access_token;
        //             }
        //             return config || $q.when(config);
        //         }
        //     };
        // });
    }
]);
