var db = require('orm').db,
    EmailNotification = db.models.email_notification;

exports.getAllNotifications = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = 'SELECT t1.*,t2.school_name  FROM `email_notification` as t1  LEFT JOIN schools as t2 on t1.school_id = t2.id  WHERE  t2.id = ?';
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Notice id required!!');
    }
    EmailNotification.find({
        id: req.body.id
    }, 1, function(err, email_notifications) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        email_notifications[0].remove(function(err) {
            if (err) {
                console.log(err)
                return sendError(req, res, 400, "Something went wrong!");
            }
            return res.send(email_notifications[0]);
        });
    });
};

exports.getNotification = function(req, res) {
    if (typeof req.query.id == 'undefined') return sendError(req, res, 401, "ID required field");
    EmailNotification.find({
        id: req.query.id
    }, 1, function(err, email_notifications) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        if (!email_notifications.length) {
            return sendError(req, res, 404, "Notice not found");
        }
        return res.send(email_notifications[0]);
    });
};


exports.register = function(req, res) {
    newEmailNotification = new EmailNotification();
    newEmailNotification.school_id = req.body.school_id;
    newEmailNotification.school_auth_person_id = req.body.school_auth_person_id;
    newEmailNotification.message = req.body.message;
    newEmailNotification.send_notice_to = req.body.send_email_notice;
    var type = [];
    var arr = req.body.send_email_notice.split(',');
    if (arr.length <= 0 || req.body.send_email_notice == '') {
        return sendError(req, res, 422, "Please check your parameters!");
    } else if (arr.length <= 1) {
        type.push(req.body.send_email_notice);
        type.push(req.body.send_email_notice);
        type.push(req.body.send_email_notice);
    } else if (arr.length <= 2) {
        type.push(arr[0]);
        type.push(arr[1]);
        type.push(arr[0]);
    } else if (arr.length <= 3) {
        type.push(arr[0]);
        type.push(arr[1]);
        type.push(arr[2]);
    }
    newEmailNotification.save(function(err, created_email_notification) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, 'Something went wrong!');
        }
        var query_str = `SELECT *
                FROM (
                    SELECT
                        CONCAT(id) as id,
                        CONCAT(first_name," ",last_name, " - (Staff)") as name,
                        "staff" as type,
                        school_id,
                        email_id as email
                      FROM staff
                      UNION
                      SELECT
                        CONCAT(id) as id,
                        CONCAT(first_name," ",last_name, " - (Student)") as name,
                        "student" as type,
                        school_id,
                        email as email
                      FROM admissions
                      UNION
                      SELECT
                        CONCAT(t1.id) as id,
                        CONCAT(t1.full_name, " - (Parent)") as name,
                        "parents" as type,
                        t2.school_id as school_id,
                        t1.email as email
                      FROM parents as t1
                      LEFT JOIN admissions as t2 ON t1.student_id = t2.id
                      ) as final
                WHERE final.school_id = ? AND (final.type IN (?) OR final.type IN (?) OR final.type IN (?)) `;
        db.driver.execQuery(query_str, [req.body.school_id, type[0], type[1], type[2]], function(err, rows) {
            if (err) {
                cb(err, null);
            }
            if (rows.length == 0) {
                return res.send(488);
            }
            var loopCount = 0;
            var allUserEmails = [];
            rows.forEach(function(user) {
                if (user.email != '' && user.email != null && user.email.length > 9) {
                    loopCount++;
                    allUserEmails.push(user.email);
                } else {
                    loopCount++;
                }
                if (loopCount >= rows.length) {
                    var ui_message = req.body.message;
                    var email_html = `
                                     <body style="font-family:arial; font-size:16px; color:#4c4c4c; padding:0; margin:0;">
                                         <table width="100%" style="font-family:arial; font-size:16px; border:0px solid #000; max-width:700px; padding-top:30px;"  align="center" border="0" cellspacing="0" cellpadding="0">
                                             <tbody>
                                                 <tr>
                                                     <td>
                                                         <table width="100%" style="font-family:arial; font-size:16px; border:1px solid #d6d6d8; max-width:700px;"  align="center" border="0" cellspacing="0" cellpadding="0">
                                                             <tbody>
                                                                 <tr>
                                                                     <td style="padding:50px 40px 40px 40px; text-align:center; font-family:arial;">
                                                                         <h1 style="font-family:arial; font-size:40px; color:#30333b; font-weight:normal; padding:0; margin:0;">Hello, <span style="color:rgb(242,53,95); font-weight:bold; text-transform:uppercase;">User</span></h1>
                                                                     </td>
                                                                 </tr>
                                                                 <tr>
                                                                     <td style="padding:0 40px 30px 40px; text-align:center; font-family:arial;">
                                                                         <p style="font-family:arial; font-size:22px; color:#737373; font-weight:normal; padding:0; margin:0;">` + ui_message + `<br />
                                                                        </p>
                                                                     </td>
                                                                 </tr>
                                                                 <tr>
                                                                     <td style="padding:0 40px 50px 40px; text-align:center;">
                                                                     </td>
                                                                 </tr>
                                                                 <tr>
                                                                     <td style="padding:35px 40px;">
                                                                         <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                             <tr>
                                                                                 <td valign="bottom" style="text-align:right; font-family:arial; font-size:14px; color:#6a6c72;">Kind Regards <br /><b style="text-transform:uppercase;">Thunder System</b></td>
                                                                             </tr>
                                                                             <tr>
                                                                                 <td valign="bottom" style="text-align:right; font-family:arial; font-size:10px;">* You need to login into system first before joining conference.</td>
                                                                             </tr>
                                                                         </table>
                                                                     </td>
                                                                 </tr>
                                                             </tbody>
                                                         </table>
                                                     </td>
                                                 </tr>
                                                 <tr>
                                                     <td style="font-family:arial; font-size:10px; padding:20px 0 30px 0; color:#a6a6a6; text-transform:uppercase; text-align:center;">
                                                         &#169; 2016 Copyright Thunder System
                                                     </td>
                                                 </tr>
                                             </tbody>
                                         </table>
                                     </body>
                                     `;
                    allUserEmails = allUserEmails.toString();
                    var froms = 'binit@thinkwik.com';
                    var to = allUserEmails;
                    var subject = "Thunder-School Notification";
                    sendMail(req, res, froms, to, subject, email_html, function(err, success) {
                        if (err) {
                            console.log(err);
                            return sendError(req, res, 422, "Something went wrong");
                        }
                        return res.send(200);
                    });
                }
            })
        });
    });
};
