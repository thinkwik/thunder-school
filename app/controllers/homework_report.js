var db = require('orm').db,
    Homeworks = db.models.homeworks;
HomeworkReports = db.models.homework_reports;


exports.complete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'HomeworkReports id required!!');
    }
    HomeworkReports.find({
        id: req.body.id
    }, 1, function(err, reports) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        reports[0].status = (reports[0].status == "0") ? "1" : "0";
        reports[0].save(function(err, saved_homework_report) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, 'HomeworkReports id required!');
            }
            return res.send(saved_homework_report);
        });
    });
};

exports.notCompleted = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'HomeworkReports id required!!');
    }
    HomeworkReports.find({
        id: req.body.id
    }, 1, function(err, reports) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        reports[0].status = (reports[0].status == "1") ? "0" : "1";
        reports[0].save(function(err, saved_homework_report) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, 'HomeworkReports id required!');
            }
            return res.send(saved_homework_report);
        });
    });
};

exports.makeCompleteAll = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Homework id required!!');
    }

    var query_str = 'UPDATE `homework_reports` SET `status`= ? WHERE `homework_id` = ?';
    db.driver.execQuery(query_str, ["1", req.body.id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.makeInCompleteAll = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Homework id required!!');
    }

    var query_str = 'UPDATE `homework_reports` SET `status`= ? WHERE `homework_id` = ?';
    db.driver.execQuery(query_str, ["0", req.body.id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(200);
    });
};

exports.getAllStudentList = function(req, res) {
    if (req.body.homework_id == 'undefined') {
        return sendError(req, res, 422, "Homework ID required");
    }
    var query_str = 'SELECT t1.*, CONCAT(t2.first_name, " ", t2.last_name) as student_name FROM `homework_reports` as t1  LEFT JOIN admissions as t2 ON t1.student_id = t2.id  WHERE t1.homework_id = ?';
    db.driver.execQuery(query_str, [req.body.homework_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

var fs = require('fs');
exports.uploadStudentHomeworkFile = function(req, res) {
    var fs = require('fs');
    var studenthomework = __dirname + "/../../public/upload/studenthomeworks/";
    if (!fs.existsSync(studenthomework)) {
        fs.mkdirSync(studenthomework);
    }
    var file = req.files.file;
    var fileExtension = file.name.split(".")[1];
    if (fileExtension) {
      fileName = file.path.split('/');
      fs.rename(file.path, studenthomework + fileName[fileName.length - 1] +"."+ fileExtension);
      return res.send(fileName[fileName.length - 1] +"."+ fileExtension);
    }
    else {
      return sendError(req, res, 400, "Bad Request check your parameters");
    }
};


exports.updateGrades = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'HomeworkReports id required!!');
    }
    HomeworkReports.find({
        id: req.body.id
    }, function(err, grades_achieved) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, 'HomeworkReports id required!');
        }
        if (grades_achieved.length === 0) {
            return sendError(req, res, 422, 'Homework not found!');
        }
        grades_achieved[0].achieved_grade = req.body.achieved_grade;
        grades_achieved[0].save(function(err, saved_grades) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, 'HomeworkReports id required!');
            }
            return res.send(saved_grades);
        });
    });
};

exports.addStudentHomework = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'HomeworkReports id required!!');
    }
    HomeworkReports.find({
        id: req.body.id
    }, function(err, submitted_homework) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, 'HomeworkReports id required!');
        }
        if (submitted_homework.length === 0) {
            return sendError(req, res, 422, 'HomeworkReports not found!');
        }
        submitted_homework[0].file_path = req.body.student_submitted;
        submitted_homework[0].save(function(err, saved_homework) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, 'HomeworkReports id required!');
            }
            return res.send(saved_homework);
        });
    });
};
