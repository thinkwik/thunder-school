var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Todo = db.define('todos', {
    
    user_id: {
        type: 'integer'
    },
    description:String,
    date: String,
    type: ['admin', 'student','staff','parent'],
    status: ['0', '1']
}, {
    timestamp: true,
    methods: {

    }
});
