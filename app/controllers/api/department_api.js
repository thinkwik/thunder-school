  var db = require('orm').db,
      School = db.models.schools,
      Department = db.models.departments;

  exports.getAllSchoolDepartment = function(req, res) {
      if (typeof req.body.school_id == 'undefined' || req.body.school_id === null) {
          return sendError(req, res, 422, "refer parameters");
      }
      var query_str = `SELECT t1.*,t2.school_name FROM departments as t1
                       LEFT JOIN schools as t2 on t1.school_id = t2.id
                       WHERE  t2.id = ?`;
      db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
          if (err) {
              console.log(err);
              return sendError(req, res, 400, "Bad Request check your parameters");
          }
          var response = {};
          response['data'] = rows;
          response['code'] = 200;
          return res.send(response);
      });
  };
