var bCrypt = require('bcrypt-nodejs');
var shortid = require('shortid');
var db = require('orm').db,
    Staff = db.models.staff;

var createHash = function(password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
};

exports.register = function(req, res) {

    if (typeof req.body.school_id != 'undefined') {
        var new_Staff = new Staff();
        new_Staff.session_id = req.body.session_id;
        new_Staff.school_id = req.body.school_id;
        new_Staff.unique_id = shortid.generate();
        new_Staff.department_id = req.body.department_id;
        new_Staff.designation_id = req.body.designation_id;
        new_Staff.role_id = (typeof req.body.role_id != 'undefined') ? req.body.role_id : 0;
        new_Staff.type = req.body.type;
        new_Staff.photo = req.body.imagename;
        new_Staff.first_name = req.body.first_name;
        new_Staff.middle_name = req.body.middle_name;
        new_Staff.last_name = req.body.last_name;
        new_Staff.gender = req.body.gender;
        new_Staff.address_1 = req.body.address_1;
        new_Staff.address_2 = req.body.address_2;
        new_Staff.city = req.body.city;
        new_Staff.state = req.body.state;
        new_Staff.country = req.body.country;
        new_Staff.pincode = req.body.pincode;
        new_Staff.email_id = req.body.email_id;
        new_Staff.phonenumber = req.body.phonenumber;
        new_Staff.officenumber = req.body.officenumber;
        new_Staff.date_of_birth = req.body.date_of_birth;
        new_Staff.date_of_joining = req.body.date_of_joining;
        new_Staff.employee_username = req.body.employee_username;
        new_Staff.password = createHash(req.body.password);
       
       
        new_Staff.notes = (typeof req.body.notes != 'undefined') ? req.body.notes : null;
        new_Staff.medicalinfo = (typeof req.body.medicalinfo != 'undefined') ? req.body.medicalinfo : null;
        new_Staff.social_behavioral = (typeof req.body.social_behavioral != 'undefined') ? req.body.social_behavioral : null;
        new_Staff.physical_handicaps = (typeof req.body.physical_handicaps != 'undefined') ? req.body.physical_handicaps : null;
        new_Staff.medical_immunizations = (typeof req.body.medical_immunizations != 'undefined' && Array.isArray(req.body.medical_immunizations) ) ? req.body.medical_immunizations.join(',') : null;
        new_Staff.medical_allergies = (typeof req.body.medical_allergies != 'undefined' && Array.isArray(req.body.medical_allergies)) ? req.body.medical_allergies.join(',') : null;
       

        new_Staff.facebook_profile = req.body.facebook_profile;
        new_Staff.linkedin_profile = req.body.linkedin_profile;
        new_Staff.twitter_profile = req.body.twitter_profile;
        new_Staff.socialsecuritynumber = req.body.socialsecuritynumber;
        new_Staff.emergency_contacts = (typeof req.body.emergency_contacts != 'undefined') ? req.body.emergency_contacts : "";
        new_Staff.profile_status = (typeof req.body.profile_status != 'undefined') ? req.body.profile_status : "1";
        new_Staff.status = (typeof req.body.status != 'undefined') ? req.body.status : "1";
        new_Staff.save(function(err, saved_staff) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, "Bad Request check your parameters");
            }
            res.send(saved_staff);
        });
    } else {
        return sendError(req, res, 422, 'school_id required for register new Staff');
    }
};

exports.getStaff = function(req, res) {
    if (typeof req.query.id == 'undefined') return res.send("id required for getting json");
    Staff.find({
        id: req.query.id
    }, 1, function(err, staffs) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        if (!staffs.length) return res.send("Staff not found with perticular id");
        console.log(staffs[0]);
        return res.send(staffs[0]);
    });
};

exports.getAllStaff = function(req, res) {
    if (typeof req.body.district_id == 'undefined' || req.body.district_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = `SELECT t1.*,t2.designation,t3.school_name 
                    FROM staff as t1 
                    LEFT JOIN designations as t2 on t1.designation_id = t2.id 
                    LEFT JOIN schools as t3 on t1.school_id = t3.id 
                    WHERE  t3.parent_id = ?`;
    db.driver.execQuery(query_str, [req.body.district_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'staff id required');
    }
    console.log(req.body);
    Staff.find({
        id: req.body.id
    }, 1, function(err, staff) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        if (!staff.length) {
            return sendError(req, res, 404, "Staff not found");
        }
        staff[0].session_id = req.body.session_id;
        staff[0].school_id = req.body.school_id;
        staff[0].department_id = req.body.department_id;
        staff[0].designation_id = req.body.designation_id;
        staff[0].role_id = (typeof req.body.role_id != 'undefined') ? req.body.role_id : staff[0].role_id;
        staff[0].type = req.body.type;
        staff[0].photo = (typeof req.body.photo == 'undefined' || req.body.photo === "") ? staff[0].photo : req.body.photo;
        staff[0].first_name = req.body.first_name;
        staff[0].middle_name = (typeof req.body.middle_name != 'undefined') ? req.body.middle_name : staff[0].middle_name;
        staff[0].last_name = req.body.last_name;
        staff[0].gender = req.body.gender;
        staff[0].address_1 = req.body.address_1;
        staff[0].address_2 = req.body.address_2;
        staff[0].city = req.body.city;
        staff[0].state = req.body.state;
        staff[0].country = req.body.country;
        staff[0].pincode = req.body.pincode;
        staff[0].email_id = req.body.email_id;
        staff[0].phonenumber = req.body.phonenumber;
        staff[0].officenumber = req.body.officenumber;
        staff[0].date_of_birth = req.body.date_of_birth;
        staff[0].date_of_joining = req.body.date_of_joining;
        staff[0].employee_username = req.body.employee_username;
        staff[0].password = req.body.password;
        
        staff[0].notes = (typeof req.body.notes != 'undefined') ? req.body.notes : staff[0].notes;
        staff[0].medicalinfo = (typeof req.body.medicalinfo != 'undefined') ? req.body.medicalinfo : staff[0].medicalinfo;
        staff[0].social_behavioral = (typeof req.body.social_behavioral != 'undefined') ? req.body.social_behavioral : staff[0].social_behavioral;
        staff[0].physical_handicaps = (typeof req.body.physical_handicaps != 'undefined') ? req.body.physical_handicaps : staff[0].physical_handicaps;
        staff[0].medical_immunizations = (typeof req.body.medical_immunizations != 'undefined' && Array.isArray(req.body.medical_immunizations) ) ? req.body.medical_immunizations.join(',') : staff[0].medical_immunizations;
        staff[0].medical_allergies = (typeof req.body.medical_allergies != 'undefined' && Array.isArray(req.body.medical_allergies)) ? req.body.medical_allergies.join(',') : staff[0].medical_allergies;        
        
        staff[0].facebook_profile = (typeof req.body.facebook_profile != 'undefined') ? req.body.facebook_profile : staff[0].facebook_profile;
        staff[0].linkedin_profile = (typeof req.body.linkedin_profile != 'undefined') ? req.body.linkedin_profile : staff[0].linkedin_profile;
        staff[0].twitter_profile = (typeof req.body.twitter_profile != 'undefined') ? req.body.twitter_profile : staff[0].twitter_profile;
        staff[0].socialsecuritynumber = (typeof req.body.socialsecuritynumber != 'undefined') ? req.body.socialsecuritynumber : staff[0].socialsecuritynumber;
        staff[0].emergency_contacts = (typeof req.body.emergency_contacts != 'undefined') ? req.body.emergency_contacts : staff[0].emergency_contacts;        
        staff[0].status = (typeof req.body.status != 'undefined') ? req.body.status : "0";
        staff[0].save(function(err, updated_staff) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            return res.send(updated_staff);
        });
    });
};

exports.active = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'Staff id required');
    }
    Staff.find({
        id: req.body.id
    }, 1, function(err, staff) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        staff[0].status = "1";
        staff[0].save(function(err, activated_staff) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            return res.send(activated_staff);
        });
    });
};

exports.inactive = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'Staff id required');
    }
    Staff.find({
        id: req.body.id
    }, 1, function(err, staff) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        staff[0].status = "0";
        staff[0].save(function(err, inactivated_staff) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            return res.send(inactivated_staff);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'Staff id required');
    }
    Staff.find({
        id: req.body.id
    }, 1, function(err, staff) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        staff[0].remove(function(err) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, "Bad Request check your parameters");
            }
            return res.send(staff[0]);
        });
    });
};


exports.getSchoolsAllStaff = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = `SELECT t1.*,t2.designation,t3.school_name 
                    FROM staff as t1 
                    LEFT JOIN designations as t2 on t1.designation_id = t2.id 
                    LEFT JOIN schools as t3 on t1.school_id = t3.id 
                    WHERE  t3.id = ?`;
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.tryStaffLogin = function(req, res) {
    if (typeof req.body.id == 'undefined' || req.body.id == null){
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = `SELECT t1.*, t2.modules as security_roles FROM staff as t1 
                     LEFT JOIN staff_roles as t2 on t1.role_id = t2.id
                     WHERE t1.id = ? `;

    db.driver.execQuery(query_str, [req.body.id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        if(rows.length == 0) {
            return res.sendError(req, res, 404, "Staff not found.");
        }
        return res.send(rows[0]);
    });
};

exports.listAllAppointments = function(req, res) {
    if (typeof req.body.staff_id == 'undefined' || req.body.staff_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = `SELECT t1.*,CONCAT(t2.first_name,' ',t2.last_name) as student_name ,t3.full_name as parent_name,CONCAT(t4.first_name,' ',t4.last_name) as staff_name 
                    FROM appointments as t1 
                    LEFT JOIN admissions as t2 on t1.student_id = t2.id 
                    LEFT JOIN parents as t3 on t1.parent_id = t3.id 
                    LEFT JOIN staff as t4 on t1.staff_id = t4.id 
                    WHERE t1.staff_id = ? GROUP BY t1.id`;
    db.driver.execQuery(query_str, [req.body.staff_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.listAllSchoolAppointments = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = `SELECT t1.*,CONCAT(t2.first_name,' ',t2.last_name) as student_name ,t3.full_name as parent_name,CONCAT(t4.first_name,' ',t4.last_name) as staff_name 
                    FROM appointments as t1 
                    LEFT JOIN admissions as t2 on t1.student_id = t2.id 
                    LEFT JOIN parents as t3 on t1.parent_id = t3.id 
                    LEFT JOIN staff as t4 on t1.staff_id = t4.id 
                    WHERE t4.school_id = ? GROUP BY t1.id`;
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.parentListAllAppointments = function(req, res) {
    if (typeof req.body.parent_id == 'undefined' || req.body.parent_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = `SELECT t1.*,CONCAT(t2.first_name,' ',t2.last_name) as student_name ,t3.full_name as parent_name,CONCAT(t4.first_name,' ',t4.last_name) as staff_name 
                    FROM appointments as t1 
                    LEFT JOIN admissions as t2 on t1.student_id = t2.id 
                    LEFT JOIN parents as t3 on t1.parent_id = t3.id 
                    LEFT JOIN staff as t4 on t1.staff_id = t4.id 
                    WHERE t1.parent_id = ?`;
    db.driver.execQuery(query_str, [req.body.parent_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.studentListAllAppointments = function(req, res) {
    if (typeof req.body.student_id == 'undefined' || req.body.student_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = `SELECT t1.*,CONCAT(t2.first_name,' ',t2.last_name) as student_name ,t3.full_name as parent_name,CONCAT(t4.first_name,' ',t4.last_name) as staff_name 
                    FROM appointments as t1 
                    LEFT JOIN admissions as t2 on t1.student_id = t2.id 
                    LEFT JOIN parents as t3 on t1.parent_id = t3.id 
                    LEFT JOIN staff as t4 on t1.staff_id = t4.id 
                    WHERE t1.student_id = ?`;
    db.driver.execQuery(query_str, [req.body.student_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};


exports.getStaffEmailList = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = "SELECT t1.email_id FROM staff as t1 WHERE t1.school_id = ?";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getStudentEmailList = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = "SELECT t1.email  FROM admissions as t1 WHERE t1.school_id = ?";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getParentEmailList = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = "SELECT t1.student_id,t1.email as parentsEmail FROM parents as t1 LEFT JOIN admissions as t2 on t1.student_id = t2.id  WHERE t2.school_id = ?";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};


exports.getAllMySchedules = function(req, res) {
    if (typeof req.body.staff_id === 'undefined' || req.body.staff_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = `SELECT t1.*, t2.grade, t3.subject_name 
                    FROM classes as t1 
                    LEFT JOIN grades as t2 on t1.grade_id = t2.id 
                    LEFT JOIN subjects as t3 ON t1.subject_id = t3.id 
                    WHERE t1.staff_id = ? 
                    ORDER BY CASE t1.day
                       WHEN 'Monday' THEN 1
                       WHEN 'Tuesday' THEN 2
                       WHEN 'Wednesday' THEN 3
                       WHEN 'Thursday' THEN 4
                       WHEN 'Friday' THEN 5
                       WHEN 'Saturday' THEN 6
                       ELSE 7
                    END ,t1.start_time ASC`;
    db.driver.execQuery(query_str, [req.body.staff_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getAllStudentClasses = function(req, res) {
    if (typeof req.body.student_id == 'undefined' || typeof req.body.grade_id == 'undefined') {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = `SELECT t1.*, t3.grade, t4.subject_name, CONCAT(t5.first_name, " ", t5.last_name) as staff_name,t6.department_name, CONCAT(t7.room_number," ",t7.floor," ",t7.building_name," ",t7.street_name) as location
                    FROM classes as t1
                    LEFT JOIN student_classes as t2 ON t1.id = t2.class_id
                    LEFT JOIN grades as t3 ON t3.id = t1.grade_id
                    LEFT JOIN subjects as t4 ON t4.id = t1.subject_id
                    LEFT JOIN staff as t5 ON t5.id = t1.staff_id
                    LEFT JOIN departments as t6 ON t6.id = t4.department_id
                    LEFT JOIN locations as t7 ON t1.location_id = t7.id
                    WHERE t2.student_id = ? AND t2.grade_id = ? GROUP BY t1.id 
                    ORDER BY t1.start_time ASC`;
    db.driver.execQuery(query_str, [req.body.student_id, req.body.grade_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};



exports.staffLogin = function(req, res) {
    Staff.find({
        email_id: req.body.email
    }, 1, function(err, result) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        } else if (result.length > 0) {
            if (req.body.password) {
                var hashFromDB = result[0].password;
                var plainPassFromUser = req.body.password;
                if (isValidPassword(result[0], plainPassFromUser)) {
                    var query_str = `SELECT t1.*, t2.modules as security_roles FROM staff as t1 
                                     LEFT JOIN staff_roles as t2 on t1.role_id = t2.id
                                     WHERE t1.id = ? `;

                    db.driver.execQuery(query_str, [result[0].id], function(err, rows) {
                        if (err) {
                            console.log(err);
                            return sendError(req, res, 422, "Bad Request check your parameters");
                        }
                        if(rows.length == 0) {
                            return res.sendError(req, res, 404, "Staff not found.");
                        }
                        return res.send(rows[0]);
                    });
                } else {
                    return sendError(req, res, 422, "Password not match!");
                }
            } else {
                return sendError(req, res, 404, "Please enter Password");
            }
        } else {
            return sendError(req, res, 404, "Staff not found");
        }
    });
};

var isValidPassword = function(staff, password) {
    return bCrypt.compareSync(password, staff.password);
};

var fs = require('fs');
exports.uploadFile = function(req, res) {
    var fs = require('fs');
    var staffImages = __dirname + "/../../public/upload/staffImages/";
    if (!fs.existsSync(staffImages)) {
        fs.mkdirSync(staffImages);
    }
    var file = req.files.file;
    var fileExtension = file.name.split(".")[1];
    if (fileExtension) {
      fileName = file.path.split('/');
      fs.rename(file.path, staffImages + fileName[fileName.length - 1] +"."+ fileExtension);
      return res.send(fileName[fileName.length - 1] +"."+ fileExtension);
    }
    else {
      return sendError(req, res, 400, "Bad Request check your parameters");
    }
};
