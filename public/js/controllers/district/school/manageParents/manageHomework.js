app.controller('showStudentHomeworkToParentsController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', '$window',
    function($scope, $http, $state, toaster, $cookieStore, $window) {
        $scope.getStudentsHomework = function() {
            var data = {
                student_id: $cookieStore.get("loggedInParent").student_id
            };
            $http({
                method: 'POST',
                url: '/homework/getHomeworkStudentWise',
                data: data
            }).then(function successCallback(response) {
                $scope.homeworks = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Active', 'Something went wrong!');
            });
        };

        $scope.DownloadHomework = function(homework) {
              if (homework.file_path) {
                var homeworkFileName = homework.file_path;
                homeworkFileName = homeworkFileName.split(".");
                var url = $window.location.origin + '/upload/studenthomeworks/' + homework.file_path;
                var request = new XMLHttpRequest();
                request.open('HEAD', url, false);
                request.send();
                if (request.status == 200) {
                    var element = angular.element('<a/>');
                    element.attr({
                        href: url,
                        target: '_blank',
                        download: homework.title + '.' + homeworkFileName[1]
                    })[0].click();
                } else {
                    toaster.pop('error', 'Homework', 'Homework not found!');
                    return false;
                }
            } else {
                toaster.pop('error', 'Homework', 'Homework not found!');
                return false;
            }
        };
    }
]);
