app.controller('addStudentClassesController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.getGrades = function() {
            var data = {
                school_id: $cookieStore.get('loggedInStaff').school_id
            };
            $http({
                method: 'POST',
                url: '/grade/getAllSchoolGrades',
                data: data
            }).then(function successCallback(response) {
                $scope.grades = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Student Grades', error.data.message);
            });
        };

        $scope.getStudents = function(grade_id) {
            var data = {
                school_id: $cookieStore.get('loggedInStaff').school_id,
                grade_id: grade_id
            };
            $http({
                method: 'POST',
                url: '/student/getAllStudentGradeWise',
                data: data
            }).then(function successCallback(response) {
                $scope.students = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Student', error.data.message);
            });
        };

        $scope.addStudentToClass = function() {
            var data = $scope.student_classes;
            $http({
                method: 'POST',
                url: '/create/student_class',
                data: data
            }).then(function successCallback(response) {
                $scope.classes = response.data;
                toaster.pop('success', 'Student Classes', "Student added Successfully in class!");
                $scope.ListStudentClasses();
                $scope.getClasses($scope.student_classes.grade_id);
            }, function errorCallback(error) {
                toaster.pop('error', 'Student Classes', error.data.message);
            });
        };

        $scope.deleteStudentClass = function(studentClassId) {
            var data = {
                id: studentClassId
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/student_class/deleteStudentClass',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getParticularStudentClass();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Student Class', error.data.message);
                });
            }
        };

        $scope.ListStudentClasses = function() {
            var studentData = {
                school_id: $cookieStore.get('loggedInStaff').school_id,
                grade_id: $scope.student_classes.grade_id,
                student_id: $scope.student_classes.student_id
            };
            $http({
                method: 'POST',
                url: '/class/getAllStudentClasses',
                data: studentData
            }).then(function successCallback(response) {
                $scope.studentClassesList = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Student Classes', error.data.message);
            });
        };

        $scope.getClasses = function(grade_id) {
            var data = {
                grade_id: grade_id
            };
            $http({
                method: 'POST',
                url: '/class/getClassGradeWise',
                data: data
            }).then(function successCallback(response) {
                $scope.classes = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Student Classes', error.data.message);
            });
        };

        $scope.getParticularStudentClass = function() {
            var studentData = {
                grade_id: $scope.student_classes.grade_id,
                student_id: $scope.student_classes.student_id
            };
            $http({
                method: 'POST',
                url: '/class/getAllStudentClasses',
                data: studentData
            }).then(function successCallback(response) {
                $scope.studentClassesList = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Student Classes', error.data.message);
            });
        };

        $scope.getStudentClasses = function(grade_id, student_id) {
            var data = {
                school_id: $cookieStore.get('loggedInStaff').school_id,
                grade_id: grade_id,
                student_id: student_id
            };
            $http({
                method: 'POST',
                url: '/class/getAllClassesGradeStudentWise',
                data: data
            }).then(function successCallback(response) {
                $scope.classes = response.data;
                var studentData = {
                    school_id: $cookieStore.get('loggedInStaff').school_id,
                    grade_id: grade_id,
                    student_id: student_id
                };
                $http({
                    method: 'POST',
                    url: '/class/getAllStudentClasses',
                    data: studentData
                }).then(function successCallback(response) {
                    $scope.studentClassesList = response.data;
                }, function errorCallback(error) {
                    toaster.pop('error', 'Student Classes', error.data.message);
                });
            }, function errorCallback(error) {
                toaster.pop('error', 'Student Classes', error.data.message);
            });
        };
    }
]);
