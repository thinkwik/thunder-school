  var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Admissions = db.define('admissions', {
    school_id: {
        type: 'integer'
    },
    unique_id: String,
    grade_id: {
        type: 'integer'
    },
    session_id: {
        type: 'integer'
    },
    staff_id: {
        type: 'integer'
    },
    source_id: {
        type: 'integer'
    },
    first_name: String,
    last_name: String,
    middle_name: String,
    password: String,
    gender: String,
    image: String,
    birth_date: String,
    email: String,
    phone: String,
    mobile_number: String,
    inquiry_by: String,
    source_detail: String,
    address: String,
    city: String,
    zipcode: Number,
    state: String,
    country: String,
    inquiry_status: String,
    comments: String,
    academic_history: String,
    immunization: String,
    academics: String,
    sports: String,
    remarks: String,
    height: String,
    weight: String,
    blood_group: String,
    dieases_information: String,
    admission_status: String,
    forgot_password_token:String,
    status: ['0', '1'],
    profile_status: ['0', '1'],
    medicalinfo:String,
    notes:String,
    social_behavioral:String,
    physical_handicaps:String,
    medical_immunizations: {
        type: 'integer'
    },
    medical_allergies: {
        type: 'integer'
    }
}, {
    timestamp: true,
    methods: {

    }
});
