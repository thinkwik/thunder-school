var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Response = db.define('response', {
    issue_id: {
        type: 'integer'
    },
    student_id: {
        type: 'integer'
    },
    staff_id: {
        type: 'integer'
    },
    parent_id: {
        type: 'integer'
    },
    message: String,
}, {
    timestamp: true,
    methods: {

    }
});
