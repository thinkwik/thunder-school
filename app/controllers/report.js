var bCrypt = require('bcrypt-nodejs');
var shortid = require('shortid');
var excelbuilder = require('msexcel-builder');
var path = require('path');
var appDir = path.dirname(require.main.filename);
var fs = require('fs');

var db = require('orm').db;



exports.all_student_data = function(req, res) {
	console.log(req.body);
	
};

generatefile = function(filename ,headers, rows, cb) {
	var new_file_name = filename + "_"+ shortid.generate() +'.xlsx';
 	var workbook = excelbuilder.createWorkbook(appDir+'/public/upload/generatedReports/', new_file_name);

	var work_sheet = workbook.createSheet('Sheet', headers.length, rows.length+1);

	// Fill headers data
	var row_count = 1;
	for (var i = row_count; i <= headers.length; i++) {
		work_sheet.set(i, row_count, headers[i-1]);
		work_sheet.font(i, row_count, {bold:'true'});
	}
	row_count++;

	//Fill Row data	
	for (var i = 0; i < rows.length; i++) {		
	
	//convert josn object to array
	var temp_row__arr  = [];
	for (var k in rows[i]) {
	    if (rows[i].hasOwnProperty(k)) {
	         temp_row__arr.push(rows[i][k]);
	    }
	}

	for (var j = 0; j < temp_row__arr.length; j++) {
		work_sheet.align(j , row_count, 'left');
		work_sheet.set(j+1, row_count, temp_row__arr[j]);
	}
	
	row_count++;
	
	}

	// Save it
	  workbook.save(function(err){
	    if (err){
	    	console.log(err);
	    	cb('something went wrong while generating file',null);
	    }
	    cb(null,new_file_name);
	  });
};

exports.student_report = function(req, res) {
	if(req.body.classes == 'undefined') {
		return sendError(req, res, 400, "Bad Request check your parameters");
	}
	var query_str = `SELECT t1.id, 
					       t1.unique_id, 
					       t1.first_name, 
					       t1.last_name, 
					       t1.gender, 
					       t3.grade, 
					       t1.birth_date, 
					       t1.email, 
					       t1.mobile_number, 
					       Concat(t1.address, '') AS address, 
					       Concat(t1.city, '') AS city, 
					       Concat(t1.state, '') AS state, 
					       Concat(t1.country, '') AS country, 
					       Concat(t1.zipcode, '') AS zipcode, 
					       t1.blood_group, 
					       t1.inquiry_by, 
					       t1.source_detail, 
					       t1.comments, 
					       t1.academic_history, 
					       t1.academics, 
					       t1.sports, 
					       t1.remarks, 
					       t1.height, 
					       t1.weight, 
					       t1.admission_status, 
					       t1.notes, 
					       t1.medicalinfo, 
					       t1.social_behavioral, 
					       t1.physical_handicaps, 
					       Group_concat(DISTINCT t5.name) AS list_medical_immunizations, 
					       Group_concat(DISTINCT t6.name) AS list_medical_allergies 
					FROM   admissions AS t1 
					       left join student_classes AS t2 
					              ON t1.id = t2.student_id 
					       left join grades AS t3 
					              ON t1.grade_id = t3.id 
					       left join medical_info t5 
					              ON Find_in_set(t5.id, t1.medical_immunizations) 
					       left join medical_info t6 
					              ON Find_in_set(t6.id, t1.medical_allergies) 
					WHERE  t2.class_id IN ? 
					GROUP  BY t1.id `;
    db.driver.execQuery(query_str, [req.body.classes], function(err, rows, fields) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        var headers = ["Student ID","Unique ID","First Name", "Last Name", "Gender", "Grade", "Birth Date","Email", "Mobile Number",
        "Address","City","State","Country","ZipCode","Blood Group","Inquiry By","Source Detail", "Comments",
        "Academic History", "Academics", "Sports", "Remarks", "Height", "Weight", "Admission Status", "Notes",
        "Medical Info", "Social Behavioral", "Physical Handicaps","Medical Immunizations","Medical Allergies"];

        generatefile('student_report_file',headers,rows,function(err, success_file_name) {
	 		if(err) {
	 			console.log(err);
	 			return sendError(req, res, 400, "Bad Request check your parameters");
	 		}
	 		return res.send(success_file_name);
	 	});
    });
};

exports.staff_report = function(req, res) {
	if(req.body.school_id == 'undefined') {
		return sendError(req, res, 400, "Bad Request check your parameters");
	}
	var query_str = `SELECT t1.id, 
				       t1.unique_id, 
				       t1.first_name, 
				       t1.last_name, 
				       t2.department_name, 
				       t3.designation, 
				       t4.role_name, 
				       t1.type, 
				       t1.email_id, 
				       t1.phonenumber, 
				       t1.date_of_birth, 
				       t1.date_of_joining, 
				       t1.socialsecuritynumber, 
				       t1.gender, 
				       t1.address_1, 
				       t1.city, 
				       t1.state, 
				       t1.country, 
				       t1.pincode, 
				       t1.officenumber, 
				       t1.medicalinfo, 
				       t1.social_behavioral, 
				       t1.physical_handicaps, 
				       Group_concat(DISTINCT t5.NAME) AS list_medical_immunizations, 
				       Group_concat(DISTINCT t6.NAME) AS list_medical_allergies, 
				       t1.emergency_contacts, 
				       t1.notes ,
				       Group_concat(DISTINCT t7.class_name) AS classes_name
				FROM   staff AS t1 
				       LEFT JOIN departments AS t2 ON t1.department_id = t2.id 
				       LEFT JOIN designations AS t3 ON t1.department_id = t3.id 
				       LEFT JOIN staff_roles AS t4 ON t1.role_id = t4.id 
				       LEFT JOIN medical_info t5 ON Find_in_set(t5.id, t1.medical_immunizations) 
				       LEFT JOIN medical_info t6 ON Find_in_set(t6.id, t1.medical_allergies) 
				       LEFT JOIN classes t7 ON t1.id = t7.staff_id
				WHERE  t1.school_id = ? 
				GROUP  BY t1.id `;
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows, fields) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        var headers = ["Staff ID","Unique ID","First Name", "Last Name", "Department", "Designation", "Role",
        "Type", "Email", "Mobile Number", "Date Of Birth", "Date Of Joining", "Social Securitynumber", "Gender",
        "Address", "City", "State", "Country", "Pincode", "Office Number","Medical Info","Social Behavioral",
        "Physical Handicaps", "Medical Immunizations", "Medical Allergies", "Emergency Contacts", "Notes", "Classes"];

        generatefile('staff_report_file',headers,rows,function(err, success_file_name) {
	 		if(err) {
	 			console.log(err);
	 			return sendError(req, res, 400, "Bad Request check your parameters");
	 		}
	 		return res.send(success_file_name);
	 	});
    });
};

exports.parent_report = function(req, res) {
	if(req.body.school_id == 'undefined') {
		return sendError(req, res, 400, "Bad Request check your parameters");
	}
	var query_str = `SELECT t1.id, 
					       t1.unique_id, 
					       t1.full_name, 
					       Concat(t2.first_name, ' ', t2.last_name) AS student_name, 
					       t3.relation_name, 
					       t1.email, 
					       t1.phone, 
					       t1.mobile, 
					       t1.work_phone_number, 
					       t1.address, 
					       t1.city, 
					       t1.state, 
					       t1.country, 
					       t1.zipcode, 
					       t1.gender, 
					       t1.notes 
					FROM   parents AS t1 
					       LEFT JOIN admissions AS t2 
					              ON t1.student_id = t2.id 
					       LEFT JOIN relations AS t3 
					              ON t1.relation = t3.id 
					WHERE  t2.school_id = ? 
					GROUP  BY t1.id `;
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows, fields) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        var headers = ["Parent ID","Unique ID","Parent Name", "Student Name", "Relation", "Email", "Phone Number",
        "Mobile Number", "Work Phone Number", "Address", "City", "State", "Country", "ZipCode", "Gender", "Notes"];

        generatefile('parent_report_file',headers,rows,function(err, success_file_name) {
	 		if(err) {
	 			console.log(err);
	 			return sendError(req, res, 400, "Bad Request check your parameters");
	 		}
	 		return res.send(success_file_name);
	 	});
    });
};

exports.student_grade_report = function(req, res) {
	if(req.body.grades == 'undefined') {
		return sendError(req, res, 400, "Bad Request check your parameters");
	}

	var query_str = `SELECT all_rows.first_name, all_rows.last_name, all_rows.grade, all_rows.current_marks, all_rows.total_marks, tt.scale, all_rows.birth_date, all_rows.email, all_rows.mobile_number, all_rows.address
					FROM 
					( SELECT *
						FROM
						  ( SELECT t2.first_name,
						           t2.last_name,
						           t3.grade,
						           ROUND(SUM(t1.achived_marks)) AS current_marks,
						           ROUND(SUM(t1.total_marks)) AS total_marks,
						           t2.birth_date,
						           t2.email,
						           t2.mobile_number,
						           CONCAT(t2.address, ' ', t2.city, ' ',t2.state, ' ',t2.zipcode) AS address
						   FROM exam_results AS t1
						   LEFT JOIN admissions AS t2 ON t1.student_id = t2.id
						   LEFT JOIN grades AS t3 ON t2.grade_id = t3.id
						   WHERE t3.id IN ?  GROUP BY t2.id
						   
						   UNION

						   SELECT t2.first_name,
						                t2.last_name,
						                t3.grade,
						                0 AS current_marks,
						                0 AS total_marks,
						                t2.birth_date,
						                t2.email,
						                t2.mobile_number,
						                CONCAT(t2.address, ' ', t2.city, ' ',t2.state, ' ',t2.zipcode) AS address
						   FROM admissions AS t2
						   LEFT JOIN grades AS t3 ON t2.grade_id = t3.id
						   WHERE t3.id IN ? GROUP BY t2.id) AS t
						   
					) as all_rows

				LEFT JOIN school_grade_scale as tt ON tt.from_per <= round(( all_rows.current_marks/all_rows.total_marks * 100 ),2) AND  round(( all_rows.current_marks/all_rows.total_marks * 100 ),2) <= tt.to_per 

				GROUP BY all_rows.email ORDER BY all_rows.grade`;

    db.driver.execQuery(query_str, [req.body.grades,req.body.grades], function(err, rows, fields) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        var headers = ["First Name", "Last Name", "Grade", "Achived Marks", "Total Marks", "Achived Scale" ,"Birth Date", "Email", "Mobile Number", "Address"];

        generatefile('grade_report_file',headers,rows,function(err, success_file_name) {
	 		if(err) {
	 			console.log(err);
	 			return sendError(req, res, 400, "Bad Request check your parameters");
	 		}
	 		return res.send(success_file_name);
	 	});
    });
};

exports.student_homework_report = function(req, res) {
	if(req.body.grades == 'undefined') {
		return sendError(req, res, 400, "Bad Request check your parameters");
	}

	var query_str = `SELECT all_rows.first_name, all_rows.last_name, all_rows.grade, all_rows.current_marks, all_rows.total_marks, tt.scale, all_rows.birth_date, all_rows.email, all_rows.mobile_number, all_rows.address
					FROM 
					( SELECT *
						FROM
						  ( SELECT t2.first_name,
						           t2.last_name,
						           t3.grade,
						           ROUND(SUM(t1.achieved_grade)) AS current_marks,
						           ROUND(SUM(100)) AS total_marks,
						           t2.birth_date,
						           t2.email,
						           t2.mobile_number,
						           CONCAT(t2.address, ' ', t2.city, ' ',t2.state, ' ',t2.zipcode) AS address
						   FROM homework_reports AS t1
						   LEFT JOIN admissions AS t2 ON t1.student_id = t2.id
						   LEFT JOIN grades AS t3 ON t2.grade_id = t3.id
						   WHERE t3.id IN ? GROUP BY t2.id
						   
						   UNION

						   SELECT t2.first_name,
						                t2.last_name,
						                t3.grade,
						                0 AS current_marks,
						                0 AS total_marks,
						                t2.birth_date,
						                t2.email,
						                t2.mobile_number,
						                CONCAT(t2.address, ' ', t2.city, ' ',t2.state, ' ',t2.zipcode) AS address
						   FROM admissions AS t2
						   LEFT JOIN grades AS t3 ON t2.grade_id = t3.id
						   WHERE t3.id IN ?  GROUP BY t2.id) AS t
						   
					) as all_rows

				LEFT JOIN school_grade_scale as tt ON tt.from_per <= round(( all_rows.current_marks/all_rows.total_marks * 100 ),2) AND  round(( all_rows.current_marks/all_rows.total_marks * 100 ),2) <= tt.to_per 

				GROUP BY all_rows.email ORDER BY all_rows.grade`;

    db.driver.execQuery(query_str, [req.body.grades,req.body.grades], function(err, rows, fields) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        var headers = ["First Name", "Last Name", "Grade", "Achived Marks", "Total Marks", "Achived Scale" ,"Birth Date", "Email", "Mobile Number", "Address"];

        generatefile('homework_report_file',headers,rows,function(err, success_file_name) {
	 		if(err) {
	 			console.log(err);
	 			return sendError(req, res, 400, "Bad Request check your parameters");
	 		}
	 		return res.send(success_file_name);
	 	});
    });
};


