app.controller('examResultListStaffController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
	function($scope, $http, $state, toaster, $cookieStore) {
		$scope.addNewExamResultRedirect = function() {
			$state.go('schoolstaff.exam_result_create');
		};
		$scope.sendGradeExamReportEmail = function(exam_id,student_id) {
            var data = {
                exam_id: exam_id,
                student_id: student_id
            };
            $http({
                method: 'POST',
                url: '/exam/sendEmailExamGrade',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Email Report', "Report has been sent.");
            }, function errorCallback(error) {
                toaster.pop('error', 'Email Report', "something went wrong!");
            });
        };
		$scope.getAllExamResultList = function() {
			var data = {
				school_id: $cookieStore.get('loggedInStaff').school_id
			};
			$http({
				method: 'POST',
				url: '/exam_result/getAllExamResults',
				data: data
			}).then(function successCallback(response) {
				$scope.results = response.data;
			}, function errorCallback(error) {
				toaster.pop('error', 'Exams', 'Something went wrong! while getting all exams');
			});
		};
		$scope.viewReport = function(exam_id, student_id){
			$state.go('schoolstaff.view_exam_report', {
				exam_id: exam_id,
				student_id: student_id
			});	
		};
		// $scope.deleteExamResult = function(result_id) {
		// 	var data = {
		// 		id: result_id
		// 	};
		// 	$http({
		// 		method: 'POST',
		// 		url: '/exam_result/delete',
		// 		data: data
		// 	}).then(function successCallback(response) {
		// 		$scope.getAllExamResultList();
		// 	}, function errorCallback(error) {
		// 		toaster.pop('error', 'Event Resource', error.data.message);
		// 	});
		// };
		// $scope.editExamResult = function(result_id) {
		// 	$state.go('schoolstaff.exam_result_edit', {
		// 		result_id: result_id
		// 	});
		// };
		
	}
]);

app.controller('examResultCreateStaffController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
	function($scope, $http, $state, toaster, $cookieStore) {
		$scope.getCreateExamResultRequiredData = function(){
			var school_id = $cookieStore.get('loggedInStaff').school_id;
			var data = {school_id:school_id};
			$http({
				method: 'POST',
				url: '/exam/getAllExams',
				data: data
			}).then(function successCallback(response) {
				$scope.exams = response.data;
				$scope.getSchoolStudentsList();
			}, function errorCallback(error) {
				toaster.pop('error', 'Exam Result', error.data.message);
			});
		};

		$scope.getSchoolStudentsList = function(){
			var school_id = $cookieStore.get('loggedInStaff').school_id;
			var data = {district_id:school_id};
			$http({
				method: 'POST',
				url: '/student/getSchoolStudentsList',
				data: data
			}).then(function successCallback(response) {
				$scope.students = response.data;
			}, function errorCallback(error) {
				toaster.pop('error', 'Exam Result', error.data.message);
			});
		};

		$scope.cancelExamResult = function() {
			$state.go('schoolstaff.exam_result_list');
		};
		$scope.createExamResult = function() {
			var data = $scope.result;
			$http({
				method: 'POST',
				url: '/exam_result/register',
				data: data
			}).then(function successCallback(response) {
				toaster.pop('success', 'Exam Result', "added successfully.");
				$state.go('schoolstaff.exam_result_list');
			}, function errorCallback(error) {
				toaster.pop('error', 'Exam Result', error.data.message);
			});
		};
	}
]);

app.controller('examResultEditStaffController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
	function($scope, $http, $state, toaster, $cookieStore) {
		$scope.cancelExamResult = function() {
			$state.go('schoolstaff.exam_list');
		};
		$scope.getExamResult = function() {
			$http({
				method: 'GET',
				url: '/exam_result/edit?id=' + $state.params.result_id
			}).then(function successCallback(response) {
				$scope.result = response.data;
				$scope.getSchoolStudentsList();
				$scope.getAllExams();
			}, function errorCallback(error) {
				toaster.pop('error', 'Event Resource', error.data.message);
			});
		};

		$scope.getSchoolStudentsList = function(){
			var school_id = $cookieStore.get('loggedInStaff').school_id;
			var data = {district_id:school_id};
			$http({
				method: 'POST',
				url: '/student/getSchoolStudentsList',
				data: data
			}).then(function successCallback(response) {
				$scope.students = response.data;
			}, function errorCallback(error) {
				toaster.pop('error', 'Exam Result', error.data.message);
			});
		};

		$scope.getAllExams = function(){
			var school_id = $cookieStore.get('loggedInStaff').school_id;
			var data = {school_id:school_id};
			$http({
				method: 'POST',
				url: '/exam/getAllExams',
				data: data
			}).then(function successCallback(response) {
				$scope.exams = response.data;
			}, function errorCallback(error) {
				toaster.pop('error', 'Exam Result', error.data.message);
			});
		};

		$scope.editExamResultSave = function() {
			var data = $scope.result;
			$http({
				method: 'POST',
				url: '/exam_result/edit',
				data: data
			}).then(function successCallback(response) {
				toaster.pop('success', 'Exam Result', "Updated successfully.");
				$state.go('schoolstaff.exam_result_list');
			}, function errorCallback(error) {
				toaster.pop('error', 'Exam Result', error.data.message);
			});
		};
		$scope.cancelExamResult = function() {
			$state.go('schoolstaff.exam_result_list');
		};
	}
]);