var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Timetables = db.define('timetables', {
    school_id: {
        type: 'integer'
    },
    grade_id: {
        type: 'integer'
    },
    subject_id: {
        type: 'integer'
    },
    day: String,
    start_time: String,
    end_time: String,
}, {
    timestamp: true,
    methods: {

    }
});
