app.controller('listEventsController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        $scope.listEvents = function() {
            var data = {
                school_id: $cookieStore.get('loggedInStaff').school_id
            };
            $http({
                method: 'POST',
                url: '/gallery/listAllStaffevents',
                data: data
            }).then(function successCallback(response) {
                var events = response.data;
                $scope.events = events;
            }, function errorCallback(error) {
                toaster.pop('error', 'Events', 'cannot get all the Events! try reloading page.');
            });
        };

        $scope.ViewGallery = function(gallery_id) {
            $state.go('schoolstaff.event_gallery_view', {
                gallery_id: gallery_id
            });
        };
    }
]);

app.controller('viewEventGalleryAlbumController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload', '$window',
    function($scope, $http, $state, toaster, $cookieStore, Upload, $window) {
        $scope.viewAlbum = function() {
            var data = {
                gallery_id: $state.params.gallery_id
            };
            $http({
                method: 'POST',
                url: '/event/viewAlbum',
                data: data
            }).then(function successCallback(response) {
               var data = response.data;
               var newImageArray =[];
               $.each(data, function(i) {
                   $.each(data[i], function(key, val) {
                       if (key == 'image_name') {
                         $.ajax({
                             url: $window.location.origin + '/upload/galleryImages/' + val,
                             type: 'HEAD',
                             error: function() {},success: function() {
                               newImageArray.push(data[i]);
                               $scope.galleryImages = newImageArray;
                             }
                         });
                       }
                   });
               });
                 if(newImageArray.length) {
                   $scope.galleryImages = newImageArray;
                 }else {
                   var galleryImages = response.data;
                   $scope.galleryImages = galleryImages;
                 }
            }, function errorCallback(error) {
                toaster.pop('error', 'Gallery', error.data.message);
            });
        };
    }
]);
