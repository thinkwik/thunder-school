app.controller('createEmailNotificationsController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        $scope.listEmailNotifications = function() {
            $state.go('schools.email_notice_list');
        };

        $scope.createEmailNotification = function() {
            data = $scope.emailnotification;
            data.school_id = $cookieStore.get("loggedInSchool").id;
            data.school_auth_person_id = $cookieStore.get("loggedInSchool").authorized_user_id;
            var send_email_notice = $scope.emailnotifications;
            $scope.noticeArray = [];
            angular.forEach(send_email_notice, function(emailnotification) {
                if (emailnotification.selected) $scope.noticeArray.push(emailnotification.name);
            });
            data.send_email_notice = $scope.noticeArray.toString();
            $http({
                method: "POST",
                url: "/email_notification/create",
                data: data
            }).then(function successCallback(response) {
                $state.go('schools.email_notice_list');
                toaster.pop('success', 'Email Notification', 'Saved Successfully.');
            }, function errorCallback(error) {
                // toaster.pop('error', 'Email Notification', error.data.message);
                console.log(error);
            });
        };

        $scope.emailnotifications = [{
            id: 1,
            name: 'staff'
        }, {
            id: 2,
            name: 'student'
        }, {
            id: 3,
            name: 'parents'
        }];
    }
]);


app.controller('listEmailNotificationsController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.addNewEmailNotifications = function() {
            $state.go('schools.email_notice_create');
        };

        $scope.getAllEmailNotifications = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/email_notification/getAllNotifications',
                data: data
            }).then(function successCallback(response) {
                var emailnotifications = response.data;
                $scope.emailnotifications = emailnotifications;
            }, function errorCallback(error) {
                toaster.pop('error', 'Email Notification', 'cannot get all the notifications! try reloading page.');
            });
        };

        $scope.deleteEmailNotification = function(email_notification_id) {
            var data = {
                id: email_notification_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/email_notification/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllEmailNotifications();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Email Notification', error.data.message);
                });
            }
        };
    }
]);
