var bCrypt = require('bcrypt-nodejs');
var db = require('orm').db,
    Admissions = db.models.admissions;
Grade = db.models.grades;
Sessions = db.models.sessions;

exports.index = function(req, res) {
    return res.send("Students dashboard");
};
exports.studentLogin = function(req, res) {
    Admissions.find({
        'email': req.body.email
    }, 1, function(err, result) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        } else if (result.length > 0) {
            if (typeof req.body.password != 'undefined') {
                var hashFromDB = result[0].password;
                var plainPassFromUser = req.body.password;
                bCrypt.compare(plainPassFromUser, hashFromDB, function(err, matches) {
                    if (err) {
                        return sendError(req, res, 422, "Error while checking password");
                    } else if (matches) {
                        var school_id = result[0].school_id;
                        var query_str = 'SELECT t1.parent_id FROM `schools` as t1   WHERE t1.id = ' + school_id;
                        db.driver.execQuery(query_str, function(err, parent_id) {
                            if (err) {
                                console.log(err);
                                return sendError(req, res, 400, "Bad Request check your parameters");
                            }
                            result[0].school_parent_id = parent_id[0].parent_id;
                            return res.send(result[0]);
                        });
                    } else {
                        return sendError(req, res, 422, "Password does not match!");
                    }
                });
            } else {
                return sendError(req, res, 422, "Please enter Password");
            }
        } else {
            return sendError(req, res, 404, "Student not found!");
        }
    });
};

exports.changePassword = function(req, res) {
    Admissions.find({
        id: req.body.id
    }, 1, function(err, students) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (students.length <= 0) {
            return sendError(req, res, 404, "Student not found");
        } else {
            student = students[0];
            if (!isValidPassword(student, req.body.oldPassword)) {
                return sendError(req, res, 422, "You have entered wrong password!");
            } else if (req.body.newPassword != req.body.confirmNewPassword) {
                return sendError(req, res, 422, "new password and confirm new password must be same");
            }
            student.password = createHash(req.body.newPassword);
            student.save(function(err, saved_student) {
                if (err) {
                    return sendError(req, res, 500, "Something went wrong!Please try again");
                }
                return res.send(saved_student);
            });
        }
    });
};

exports.logout = function(req, res) {
    if (typeof req.student != 'undefined') {
        console.log(req.student.email + " logged out");
    }
    req.logout();
    return res.send({
        message: "Student logged out successfully"
    });
};
