var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var studentEventregistration = db.define('student_event_registrations', {
    event_id: {
        type: 'integer'
    },
    school_id: {
        type: 'integer'
    },
    student_id: {
        type: 'integer'
    },
    student_status: ['0', '1']
}, {
    timestamp: true,
    methods: {

    }
});
