app.controller('AppCtrl', ['$scope', '$translate', '$localStorage', '$window',
    function($scope, $translate, $localStorage, $window) {
        // config
        $scope.app = {
            name: 'Thunder-School',
            version: '1.3.3'
        };
    }
]);
app.controller('TypeaheadDemoCtrl', ['$scope', '$http',
    function($scope, $http) {
        $scope.selected = undefined;
        $scope.states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Dakota', 'North Carolina', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'];
        // Any function returning a promise object can be used to load values asynchronously
        $scope.getLocation = function(val) {
            return $http.get('http://maps.googleapis.com/maps/api/geocode/json', {
                params: {
                    address: val,
                    sensor: false
                }
            }).then(function(res) {
                var addresses = [];
                angular.forEach(res.data.results, function(item) {
                    addresses.push(item.formatted_address);
                });
                return addresses;
            });
        };
    }
]);

app.controller('headerController', ['$scope', '$translate', '$localStorage', '$cookieStore','$state',
    function($scope, $translate, $localStorage, $cookieStore,$state) {
        $scope.user = $cookieStore.get('loggedInUser');
        $scope.userProfile = function(user_id) {
            $state.go('users.edit_profile', {
                user_id: user_id
            });
        };
    }
]);

app.controller('studentHeaderController', ['$scope', '$translate', '$localStorage', '$cookieStore','$state',
    function($scope, $translate, $localStorage, $cookieStore,$state) {
        $scope.student = $cookieStore.get('loggedInStudent');
        $scope.studentProfile = function(student_id) {
            $state.go('students.edit_profile');
        };
    }
]);


app.controller('parentHeaderController', ['$scope', '$translate', '$localStorage', '$cookieStore','$state',
    function($scope, $translate, $localStorage, $cookieStore,$state) {
        $scope.parent = $cookieStore.get('loggedInParent');
        $scope.parentProfile = function(parent_id) {
            $state.go('parents.edit_profile');
        };
    }
]);

app.controller('staffHeaderController', ['$scope', '$translate', '$localStorage', '$cookieStore','$state',
    function($scope, $translate, $localStorage, $cookieStore,$state) {
        $scope.staff = $cookieStore.get('loggedInStaff');
        $scope.staffProfile = function(staff_id) {
            $state.go('schoolstaff.edit_profile');
        };
    }
]);

app.controller('districtHeaderController', ['$scope', '$translate', '$localStorage', '$cookieStore','$state',
    function($scope, $translate, $localStorage, $cookieStore,$state) {
      $scope.user = $cookieStore.get('loggedInDistrict');
        $scope.userProfile = function() {
            $state.go('districts.edit_profile');
        };
    }
]);


app.controller('schoolHeaderController', ['$scope', '$translate','$http', '$localStorage', '$cookieStore','$state',
    function($scope, $translate, $http,$localStorage, $cookieStore,$state) {
      //$scope.user = $cookieStore.get('loggedInSchool');

        $scope.getSchoolHeaderUser = function() {
            $http({
                method: 'GET',
                url: '/user/edit?id=' + $cookieStore.get('loggedInSchool').authorized_user_id
            }).then(function successCallback(response) {
                $scope.user = response.data;
            }, function errorCallback(error) {
                console.log(error);
            });
        };
        $scope.getSchoolHeaderUser();
        $scope.schoolAdminProfile = function() {
            $state.go('schools.edit_profile');
        };
    }
]);
