app.controller('listDistrictDeptController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.inactiveSchoolDept = function(schooldept_id) {
            var data = {
                id: schooldept_id
            };
            $http({
                method: 'POST',
                url: '/school/department/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllDistDepartment();
            }, function errorCallback(error) {
                toaster.pop('error', 'School Department', error.data.message);
            });
        };

        $scope.activeSchoolDept = function(schooldept_id) {
            var data = {
                id: schooldept_id
            };
            $http({
                method: 'POST',
                url: '/school/department/active',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllDistDepartment();
            }, function errorCallback(error) {
                toaster.pop('error', 'School Department', error.data.message);
            });
        };

        $scope.deleteSchoolDept = function(schooldept_id) {
            var data = {
                id: schooldept_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/school/department/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllDistDepartment();
                }, function errorCallback(error) {
                    toaster.pop('error', 'School Department', error.data.message);
                });
            }
        };

        $scope.getAllDistDepartment = function() {
            var data = {
                district_id: $cookieStore.get('loggedInDistrict').id
            };
            $http({
                method: 'POST',
                url: '/school/department/getAllDistDepartment',
                data: data
            }).then(function successCallback(response) {
                $scope.schooldepts = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Department', 'cannot get all the Department! try reloading page.');
            });
        };

        $scope.editSchoolDept = function(department_id) {
            $state.go('districts.schooldepartment_edit', {
                department_id: department_id
            });
        };

        $scope.addNewDeptRedirect = function() {
            $state.go('districts.schooldepartment_create');
        };

    }
]);

app.controller('editDistrictDeptController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.getEditSchoolDept = function(department_id) {
            var data = {
                district_id: $cookieStore.get('loggedInDistrict').id
            };
            $http({
                method: 'POST',
                url: '/school/getDistrictSchools',
                data: data
            }).then(function successCallback(response) {
                $scope.schools = response.data;
                $http({
                    method: 'GET',
                    url: '/school/department/edit?id=' + $state.params.department_id
                }).then(function successCallback(response) {
                    $scope.schooldept = response.data;
                }, function errorCallback(error) {
                    toaster.pop('error', 'Department', 'cannot get all the department! try reloading page.');
                });
            }, function errorCallback(error) {
                toaster.pop('error', 'District', 'Something went wrong!');
            });
        };

        $scope.saveEditSchoolDept = function() {
            data = $scope.schooldept;
            $http({
                method: 'POST',
                url: '/school/department/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Department', 'Saved Successfully.');
                $state.go('districts.schooldepartment_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'Department', error.data.message);
            });
        };

        $scope.cancelSchoolDeptEdit = function() {
            var school_id = $cookieStore.get('loggedInDistrict').id;
            $state.go('districts.schooldepartment_list');
        };
    }
]);

app.controller('createDistrictDeptController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.getAlldistrictSchools = function() {
            var data = {
                district_id: $cookieStore.get('loggedInDistrict').id
            };
            $http({
                method: 'POST',
                url: '/school/getDistrictSchools',
                data: data
            }).then(function successCallback(response) {
                $scope.schools = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'District', 'Something went wrong!');
            });
        };
        $scope.createSchoolDept = function() {
            data = $scope.schooldept;
            $http({
                method: "POST",
                url: "/school/department/register",
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'School Department', 'Saved Successfully.');
                $state.go('districts.schooldepartment_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'School Department', error.data.message);
            });
        };
        $scope.cancelSchoolDept = function() {
            $state.go('districts.schooldepartment_list');
        };
    }
]);
