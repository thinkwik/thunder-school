app.controller('createTimeTableController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.cancelTimeTable = function() {
            $state.go('schoolstaff.list_timetable');
        };

        $scope.getSchoolGrades = function() {
            var data = {
                school_id: $cookieStore.get('loggedInStaff').school_id
            };
            $http({
                method: 'POST',
                url: '/grade/getAllSchoolGrades',
                data: data
            }).then(function successCallback(response) {
                $scope.grades = response.data;
                $scope.getAllSchoolSubject();
            }, function errorCallback(error) {
                toaster.pop('error', 'Grades', 'Something went wrong!');
            });
        };

        $scope.getAllSchoolSubject = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/subject/getAllSchoolSubjects',
                data: data
            }).then(function successCallback(response) {
                var subjects = response.data;
                $scope.subjects = subjects;
            }, function errorCallback(error) {
                toaster.pop('error', 'Subject', 'cannot get all the Subjects! try reloading page.');
            });
        };

        $scope.createTimeTable = function() {
            data = $scope.timetable;
            data.school_id = $cookieStore.get('loggedInSchool').id;
            $http({
                method: "POST",
                url: "/timetable/register",
                data: data
            }).then(function successCallback(response) {
                $scope.cancelTimeTable();
                toaster.pop('success', 'Timetable', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Timetable', error.data.message);
            });
        };
    }
]);

app.controller('listTimeTableController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.addNewTimetable = function() {
            $state.go('schoolstaff.create_timetable');
        };

        $scope.inactiveTimetable = function(timetable_id) {
            var data = {
                id: timetable_id
            };
            $http({
                method: 'POST',
                url: '/timetable/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllSchoolTimeTable();
            }, function errorCallback(error) {
                toaster.pop('error', 'TimeTable', error.data.message);
            });
        };

        $scope.activeTimetable = function(timetable_id) {
            var data = {
                id: timetable_id
            };
            $http({
                method: 'POST',
                url: '/timetable/active',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllSchoolTimeTable();
            }, function errorCallback(error) {
                toaster.pop('error', 'TimeTable', error.data.message);
            });
        };

        $scope.deleteTimetable = function(timetable_id) {
            var data = {
                id: timetable_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/timetable/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllSchoolTimeTable();
                }, function errorCallback(error) {
                    toaster.pop('error', 'TimeTable', error.data.message);
                });
            }
        };

        $scope.getAllSchoolTimeTable = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/timetable/getAllSchoolTimeTable',
                data: data
            }).then(function successCallback(response) {
              var timetables= response.data;
                $scope.timetables = timetables;
            }, function errorCallback(error) {
                toaster.pop('error', 'Timetable', 'cannot get all the Timetable! try reloading page.');
            });
        };

        $scope.editTimeTable = function(timetable_id) {
            $state.go('schoolstaff.edit_timetable', {
                timetable_id: timetable_id
            });
        };
    }
]);

app.controller('editTimeTableController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.getEditTimetable = function() {
            $http({
                method: 'GET',
                url: '/timetable/edit?id=' + $state.params.timetable_id
            }).then(function successCallback(response) {
                $scope.timetable = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Timetable', 'cannot get all the Timetable! try reloading page.');
            });
        };

        $scope.saveEditTimetable = function() {
            data = $scope.timetable;
            $http({
                method: 'POST',
                url: '/timetable/edit',
                data: data
            }).then(function successCallback(response) {
                $state.go('schoolstaff.list_timetable');
                toaster.pop('success', 'Timetable', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Timetable', error.data.message);
            });
        };

        $scope.cancelEditTimetableForm = function() {
            $state.go('schoolstaff.list_timetable');
        };
    }
]);
