var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Eventregistration = db.define('event_registrations', {
    event_id: {
        type: 'integer'
    },
    school_id: {
        type: 'integer'
    },
    reg_name: String,
    email: String,
    mobile: String,
    message: String,
    status: ['0', '1']
}, {
    timestamp: true,
    methods: {

    }
});
