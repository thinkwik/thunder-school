app.controller('districtAdminTodoCreateController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
      $scope.start_date = function() {
          var date = new Date();
          date.setDate(date.getDate() - 1);
          return $scope.minDate = date.toString();
      };

        $scope.createTodo = function() {
            var data = $scope.todo;
            data.user_id = $cookieStore.get('loggedInDistrict').authorized_user_id;
            data.type = "admin";
            $http({
                method: 'POST',
                url: '/todo/register',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Todo', "Created successfully.");
                $state.go('districts.todo_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'Todo', error.data.message);
            });
        };
        $scope.cancel = function() {
            $state.go('districts.todo_list');
        };
    }
]);

app.controller('districtAdminTodoListController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.getAllMyTodos = function(){
            var data = {};
            data.user_id = $cookieStore.get('loggedInDistrict').authorized_user_id;
            data.type = "admin";
            $http({
                method: 'POST',
                url: '/todo/getMyTodos',
                data: data
            }).then(function successCallback(response) {
                $scope.todos = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Todo', error.data.message);
            });
        };
        $scope.deleteTodo = function(todo_id){
            var data = {
                id:todo_id
            };
            $http({
                method: 'POST',
                url: '/todo/delete',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllMyTodos();
            }, function errorCallback(error) {
                toaster.pop('error', 'Todo', error.data.message);
            });
        };
        $scope.doneTodo = function(todo_id){
            var data = {
                id:todo_id
            };
            $http({
                method: 'POST',
                url: '/todo/done',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllMyTodos();
            }, function errorCallback(error) {
                toaster.pop('error', 'Todo', error.data.message);
            });
        };
        $scope.pendingTodo = function(todo_id){
            var data = {
                id:todo_id
            };
            $http({
                method: 'POST',
                url: '/todo/pending',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllMyTodos();
            }, function errorCallback(error) {
                toaster.pop('error', 'Todo', error.data.message);
            });
        };
        $scope.addNewTodoRedirect = function() {
            $state.go('districts.todo_create');
        };
    }
]);
