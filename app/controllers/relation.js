var bCrypt = require('bcrypt-nodejs');
var db = require('orm').db,
    Relation = db.models.relations;

exports.register = function(req, res) {
    new_relation = new Relation();
    new_relation.school_id = req.body.school_id;
    new_relation.relation_name = req.body.relation_name;
    new_relation.save(function(err, saved_relation) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(saved_relation);
    });
};

exports.getRelation = function(req, res) {
    if (typeof req.query.id == 'undefined') {
        return sendError(req, res, 422, 'Relation id required');
    }
    Relation.find({
        id: req.query.id
    }, 1, function(err, relations) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!relations.length) {
            return sendError(req, res, 404, "Relations not found");
        }
        return res.send(relations[0]);
    });
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'Relation id required');
    }
    Relation.find({
        id: req.body.id
    }, 1, function(err, relations) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!relations.length) {
            return sendError(req, res, 404, "Relations not found");
        }
        // relations[0].school_id = req.body.school_id;
        relations[0].relation_name = req.body.relation_name;
        relations[0].save(function(err, relations) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "something went wrong in saving relation.");
            }
            return res.send(relations);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Relation id required!!');
    }
    Relation.find({
        id: req.body.id
    }, 1, function(err, relations) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        if (!relations.length) {
            return sendError(req, res, 404, "Relations not found");
        }
        relations[0].remove(function(err) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, 'Check your parameters!!');
            }
            return res.send(relations[0]);
        });
    });
};

exports.getAllRelations = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id == "null") {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = "SELECT t1.*,t2.school_name FROM `relations` as t1 LEFT JOIN schools as t2 on t1.school_id = t2.id WHERE  t2.id = ?";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
