app.controller('examListStaffController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.addNewExamRedirect = function() {
            $state.go('schoolstaff.exam_create');
        };
        $scope.getAllExamList = function() {
            var data = {
                school_id: $cookieStore.get('loggedInStaff').school_id
            };
            $http({
                method: 'POST',
                url: '/exam/getAllExams',
                data: data
            }).then(function successCallback(response) {
                $scope.exams = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Exams', 'Something went wrong! while getting all exams');
            });
        };
        $scope.deleteExam = function(exam_id) {
            var data = {
                id: exam_id
            };
            $http({
                method: 'POST',
                url: '/exam/delete',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllExamList();
            }, function errorCallback(error) {
                toaster.pop('error', 'Exam', error.data.message);
            });
        };
        $scope.calGradeExam = function(exam_id) {
            var data = {
                exam_id: exam_id
            };
            $http({
                method: 'POST',
                url: '/exam/gradeCalculation',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Grade Claculation', "Calculated and updated Scale for all results related to this exam.");
            }, function errorCallback(error) {
                toaster.pop('error', 'Grade Claculation', error.data.message);
            });
        };
        $scope.sendGradeExamReportEmail = function(exam_id) {
            var data = {
                exam_id: exam_id
            };
            $http({
                method: 'POST',
                url: '/exam/sendEmailExamGrade',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Email Report', "Report has been sent.");
            }, function errorCallback(error) {
                toaster.pop('error', 'Email Report', "something went wrong!");
            });
        };
        $scope.editExam = function(exam_id) {
            $state.go('schoolstaff.exam_edit', {
                exam_id: exam_id
            });
        };
        $scope.exam_qa = function(exam_id) {
            $state.go('schoolstaff.exam_qa_list', {
                exam_id: exam_id
            });
        };
    }
]);

app.controller('examCreateStaffController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {

        $scope.start_date = function() {
            var date = new Date();
            date.setDate(date.getDate() - 1);
            if ($scope.exam && $scope.exam.end_date) {
              $scope.exam.end_date = '';
            }
            return $scope.minDate = date.toString();
        };

        $scope.end_date = function() {
          if ($scope.exam && $scope.exam.start_date) {
              var date = new Date($scope.exam.start_date);
              date.setDate(date.getDate() - 1);
              return $scope.maxDate = date.toString();
          }
        };

        $scope.getSessions = function() {
            var data = {
                school_id: $cookieStore.get('loggedInStaff').school_id
            };
            $http({
                method: 'POST',
                url: '/session/getAllSchoolSessions',
                data: data
            }).then(function successCallback(response) {
                $scope.sessions = response.data;
                $scope.getGrades();
            }, function errorCallback(error) {
                toaster.pop('error', 'School', 'Something went wrong!');
            });
        };

        $scope.getClassSubjectWise = function(subject_id) {
            var data = {
                subject_id: subject_id
            };
            $http({
                method: 'POST',
                url: '/class/getClassSubjectWise',
                data: data
            }).then(function successCallback(response) {
                $scope.classes = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Student Classes', error.data.message);
            });
        };

        $scope.getGrades = function() {
            var data = {
                school_id: $cookieStore.get('loggedInStaff').school_id
            };
            $http({
                method: 'POST',
                url: '/grade/getAllSchoolGrades',
                data: data
            }).then(function successCallback(response) {
                $scope.grades = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Grades', 'Something went wrong!');
            });
        };

        $scope.getSubjectGradeWise = function(grade_id) {
          $scope.classes = [];
          $scope.subjects = [];
          $scope.exam.class_id = "";
          $scope.exam.subject_id = "";
            var data = {
                grade_id: grade_id,
                school_id: $cookieStore.get('loggedInStaff').school_id
            };
            $http({
                method: 'POST',
                url: '/subject/getSchoolGradesSubjectWise',
                data: data
            }).then(function successCallback(response) {
                $scope.subjects = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Subjects', 'Something went wrong!');
            });
        };

        // $scope.getCreateExamRequiredData = function() {
        //     var school_id = $cookieStore.get('loggedInStaff').school_id;
        //     var data = {
        //         school_id: school_id
        //     };
        //     $http({
        //         method: 'POST',
        //         url: '/session/getAllSchoolSessions',
        //         data: data
        //     }).then(function successCallback(response) {
        //         $scope.sessions = response.data;
        //     }, function errorCallback(error) {
        //         toaster.pop('error', 'Exam', error.data.message);
        //     });
        //     $http({
        //         method: 'POST',
        //         url: '/grade/getAllSchoolGrades',
        //         data: data
        //     }).then(function successCallback(response) {
        //         $scope.grades = response.data;
        //     }, function errorCallback(error) {
        //         toaster.pop('error', 'Exam', error.data.message);
        //     });
        //     $http({
        //         method: 'POST',
        //         url: '/subject/getAllSchoolSubjects',
        //         data: data
        //     }).then(function successCallback(response) {
        //         $scope.subjects = response.data;
        //     }, function errorCallback(error) {
        //         toaster.pop('error', 'Exam', error.data.message);
        //     });
        // };

        $scope.cancelExam = function() {
            $state.go('schoolstaff.exam_list');
        };

        $scope.createExam = function() {
            var data = $scope.exam;
            data.school_id = $cookieStore.get('loggedInStaff').school_id;
            data.staff_id = $cookieStore.get('loggedInStaff').id;
            $http({
                method: 'POST',
                url: '/exam/register',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Exam', "added successfully.");
                $state.go('schoolstaff.exam_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'Exam', error.data.message);
            });
        };

    }
]);

app.controller('examEditStaffController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {

        $scope.start_date = function() {
            var date = new Date();
            date.setDate(date.getDate() - 1);
            if ($scope.exam && $scope.exam.end_date) {
              $scope.exam.end_date = '';
            }
            return $scope.minDate = date.toString();
        };

        $scope.end_date = function() {
          if ($scope.exam && $scope.exam.start_date) {
              var date = new Date($scope.exam.start_date);
              date.setDate(date.getDate() - 1);
              return $scope.maxDate = date.toString();
          }
        };

        $scope.getClassGradeWise = function(grade_id) {
            var data = {
                grade_id: grade_id
            };
            console.log(data);
            $http({
                method: 'POST',
                url: '/class/getClassGradeWise',
                data: data
            }).then(function successCallback(response) {
                $scope.classes = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Student Classes', error.data.message);
            });
        };

        $scope.cancelExam = function() {
            $state.go('schoolstaff.exam_list');
        };

        $scope.getExam = function() {
            Loading(true);
            $http({
                method: 'GET',
                url: '/exam/edit?id=' + $state.params.exam_id
            }).then(function successCallback(response) {
                var school_id = response.data.school_id;
                var data = {
                    school_id: school_id
                };
                $http({
                    method: 'POST',
                    url: '/session/getAllSchoolSessions',
                    data: data
                }).then(function successCallback(response2) {
                    $scope.sessions = response2.data;
                    $http({
                        method: 'POST',
                        url: '/grade/getAllSchoolGrades',
                        data: data
                    }).then(function successCallback(response3) {
                        $scope.grades = response3.data;
                        $http({
                            method: 'POST',
                            url: '/subject/getAllSchoolSubjects',
                            data: data
                        }).then(function successCallback(response4) {
                            Loading(false);
                            $scope.subjects = response4.data;
                            $scope.exam = response.data;

                        }, function errorCallback(error) {
                            Loading(false);
                            toaster.pop('error', 'Exam', error.data.message);
                        });
                    }, function errorCallback(error) {
                        Loading(false);
                        toaster.pop('error', 'Exam', error.data.message);
                    });
                }, function errorCallback(error) {
                    Loading(false);
                    toaster.pop('error', 'Exam', error.data.message);
                });
            }, function errorCallback(error) {
                Loading(false);
                toaster.pop('error', 'Event Resource', error.data.message);
            });
        };

        $scope.editExamSave = function() {
            var data = $scope.exam;
            $http({
                method: 'POST',
                url: '/exam/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Exam', "Updated successfully.");
                $state.go('schoolstaff.exam_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'Exam', error.data.message);
            });
        };

    }
]);


app.controller('createExamQuestionAnswerController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.listQusetionAnswer = function() {
            $state.go('schoolstaff.exam_qa_list', {
                exam_id: $state.params.exam_id
            });
        };

        $scope.addQuestion = function() {
            var data = $scope.qa;
            data.exam_id = $state.params.exam_id;
            $http({
                method: 'POST',
                url: '/exam/qa/create',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Question Answer', "Added successfully.");
                $scope.qa = "";
            }, function errorCallback(error) {
                toaster.pop('error', 'Question Answer', error.data.message);
            });
        };
    }
]);

app.controller('listExamQuestionAnswerController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.addQuestionAnswer = function() {
            $state.go("schoolstaff.exam_qa_create", {
                exam_id: $state.params.exam_id
            });
        };

        $scope.deleteEXAMQA = function(qa_id) {
            var data = {
                id: qa_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
            $http({
                method: 'POST',
                url: '/exam/qa/delete',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Question Answer', "Removed successfully.");
                $scope.getAllExamQA();
            }, function errorCallback(error) {
                toaster.pop('error', 'Question Answer', error.data.message);
            });
          }
        };

        $scope.getAllExamQA = function() {
            var data = {
                exam_id: $state.params.exam_id
            };
            $http({
                method: 'POST',
                url: '/exam/qa/getAllQuestionAnswer',
                data: data
            }).then(function successCallback(response) {
                var QAs = response.data;
                $scope.QAs = QAs;
            }, function errorCallback(error) {
                toaster.pop('error', 'Question Answer', 'cannot get all the Question Answer! try reloading page.');
            });
        };

    }
]);
