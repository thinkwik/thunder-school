var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Source = db.define('sources', {
    school_id: {
        type: 'integer'
    },
    source_title: String,
}, {
    timestamp: true,
    methods: {

    }
});
