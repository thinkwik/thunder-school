var db = require('orm').db,
    Categories = db.models.product_category,
    Products = db.models.products;
var fs = require('fs');
exports.register = function(req, res) {
    if (typeof req.body.category_id != 'undefined') {
        newProduct = new Products();
        newProduct.category_id = req.body.category_id;
        newProduct.product_name = req.body.product_name;
        newProduct.product_image = req.body.product_image;
        newProduct.product_quantity = req.body.product_quantity;
        newProduct.product_price = req.body.product_price;
        newProduct.product_discount = req.body.product_discount;
        newProduct.product_notes = req.body.product_notes;
        newProduct.status = (typeof req.body.status != 'undefined') ? req.body.status : '1';
        newProduct.save(function(err, created_product) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, 'Check your parameters!!');
            }
            return res.send(created_product);
        });
    } else {
        return sendError(req, res, 422, 'Category Id required for register new Product');
    }
};


exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Product id required!!');
    }
    Products.find({
        id: req.body.id
    }, 1, function(err, products) {
        if (err) {
            return res.send(err);
        }
        products[0].category_id = req.body.category_id;
        products[0].product_name = req.body.product_name;
        products[0].product_image = (typeof req.body.product_image != 'undefined' && req.body.product_image != "") ? req.body.product_image : products[0].product_image;
        products[0].product_quantity = req.body.product_quantity;
        products[0].product_price = req.body.product_price;
        products[0].product_discount = req.body.product_discount;
        products[0].product_notes = req.body.product_notes;
        products[0].status = (typeof req.body.status != 'undefined') ? req.body.status : '0';
        products[0].save(function(err, updated_product) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, 'Check your parameters!!');
            }
            return res.send(updated_product);
        });
    });
};

exports.active = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Product id required!!');
    }
    Products.find({
        id: req.body.id
    }, 1, function(err, product) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, 'Check your parameters!!');
        }
        product[0].status = "1";
        product[0].save(function(err, activated_Product) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(activated_Product);
        });
    });
};

exports.inactive = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Product id required!!');
    }
    Products.find({
        id: req.body.id
    }, 1, function(err, Product) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, 'Check your parameters!!');
        }
        Product[0].status = "0";
        Product[0].save(function(err, inactivated_Product) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(inactivated_Product);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Product id required!!');
    }
    Products.find({
        id: req.body.id
    }, 1, function(err, Products) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        Products[0].remove(function(err) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, 'Check your parameters!!');
            }
            return res.send(Products[0]);
        });
    });
};

exports.getProduct = function(req, res) {
    console.log(req.query);
    if (typeof req.query.id == 'undefined') return res.send("id required for getting json");
    Products.find({
        id: req.query.id
    }, 1, function(err, products) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, 'Check your parameters!!');
        }
        if (!(products.length)) return res.send("Products not found with perticular id");
        return res.send(products[0]);
    });
};



exports.getAllDistProducts = function(req, res) {
    if (typeof req.body.district_id == 'undefined' || req.body.district_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = "SELECT t1.*,t2.name FROM products as t1 LEFT JOIN product_category as t2 on t1.category_id = t2.id LEFT JOIN schools as t3 ON t2.school_id = t3.id WHERE t3.parent_id = ?";
    db.driver.execQuery(query_str, [req.body.district_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, 'Check your parameters!!');
        }
        return res.send(rows);
    });
};

exports.getAllSchoolProducts = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = "SELECT t1.*,t2.name FROM products as t1 LEFT JOIN product_category as t2 on t1.category_id = t2.id LEFT JOIN schools as t3 ON t2.school_id = t3.id WHERE t3.id = ?";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, 'Check your parameters!!');
        }
        return res.send(rows);
    });
};

var fs = require('fs');
exports.uploadFile = function(req, res) {
    var fs = require('fs');
    var productImages = __dirname + "/../../public/upload/productImages/";
    if (!fs.existsSync(productImages)) {
        fs.mkdirSync(productImages);
    }
    var file = req.files.file;
    var fileExtension = file.name.split(".")[1];
    if (fileExtension) {
      fileName = file.path.split('/');
      fs.rename(file.path, productImages + fileName[fileName.length - 1] +"."+ fileExtension);
      return res.send(fileName[fileName.length - 1] +"."+ fileExtension);
    }
    else {
      return sendError(req, res, 400, "Bad Request check your parameters");
    }

};
