var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Category = db.define('product_category', {
    school_id: {
        type: 'integer'
    },
    name: String,
    description: String,
    status: ['0', '1']
}, {
    timestamp: true,
    methods: {

    }
});
