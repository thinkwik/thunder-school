var bCrypt = require('bcrypt-nodejs');
var db = require('orm').db,
    Grade = db.models.grades,
    Department = db.models.departments,
    Subjects = db.models.subjects;

exports.register = function(req, res) {
    if (typeof req.body.grade_id != 'undefined') {
        var new_Subject = new Subjects();
        new_Subject.school_id = req.body.school_id;
        new_Subject.grade_id = req.body.grade_id;
        new_Subject.department_id = req.body.department_id;
        new_Subject.session_id = req.body.session_id;
        new_Subject.subject_name = req.body.subject_name;
        new_Subject.type = req.body.type;
        new_Subject.description = req.body.description;
        new_Subject.status = (typeof req.body.status != 'undefined') ? req.body.status : "1";
        new_Subject.save(function(err, saved_subject) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            res.send(saved_subject);
        });
    } else {
        return sendError(req, res, 422, 'grade_id required for register new Subjects');
    }
};

exports.getDistrictSubject = function(req, res) {
    if (typeof req.query.id == 'undefined') return res.send("id required for getting json");
    Subjects.find({
        id: req.query.id
    }, 1, function(err, subjects) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!subjects.length) return res.send("Subject not found with perticular id");
        return res.send(subjects[0]);
    });
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'Subject id required');
    }
    Subjects.find({
        id: req.body.id
    }, 1, function(err, subjects) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!subjects.length) {
            return sendError(req, res, 404, "Subject not found");
        }
        subjects[0].school_id = req.body.school_id;
        subjects[0].grade_id = req.body.grade_id;
        subjects[0].department_id = req.body.department_id;
        subjects[0].session_id = req.body.session_id;
        subjects[0].subject_name = req.body.subject_name;
        subjects[0].type = req.body.type;
        subjects[0].description = req.body.description;
        subjects[0].status = (typeof req.body.status != 'undefined') ? req.body.status : "0";
        subjects[0].save(function(err, updated_subjects) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            return res.send(updated_subjects);
        });
    });
};

exports.active = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'Subject id required');
    }
    Subjects.find({
        id: req.body.id
    }, 1, function(err, subjects) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        subjects[0].status = "1";
        subjects[0].save(function(err, activated_subjects) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            return res.send(activated_subjects);
        });
    });
};

exports.inactive = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'Subject id required');
    }
    Subjects.find({
        id: req.body.id
    }, 1, function(err, subjects) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        subjects[0].status = "0";
        subjects[0].save(function(err, inactivated_subjects) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            return res.send(inactivated_subjects);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'Subject id required');
    }
    Subjects.find({
        id: req.body.id
    }, 1, function(err, subjects) {
        if (err) {
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        subjects[0].remove(function(err) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            return res.send(subjects[0]);
        });
    });
};

exports.getAllDistrictSubjects = function(req, res) {
    if (typeof req.body.district_id == 'undefined' || req.body.district_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = "SELECT t1.*,t2.school_name,t3.grade, t4.department_name FROM subjects as t1 LEFT JOIN schools as t2 ON t1.school_id = t2.id LEFT JOIN grades as t3 ON t1.grade_id = t3.id LEFT JOIN departments as t4 ON t1.department_id = t4.id WHERE t2.parent_id = ?";
    db.driver.execQuery(query_str, [req.body.district_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getAllSchoolSubjects = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = "SELECT t1.*,t2.school_name,t3.grade, t4.department_name FROM subjects as t1 LEFT JOIN schools as t2 ON t1.school_id = t2.id LEFT JOIN grades as t3 ON t1.grade_id = t3.id LEFT JOIN departments as t4 ON t1.department_id = t4.id WHERE t2.id = ?";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};



exports.getAllSchoolSubjectsGradeWise = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || typeof req.body.grade_id == 'undefined') {
        return sendError(req, res, 422, "Select grade first");
    }
    var query_str = "SELECT t1.*,t2.school_name,t3.grade, t4.department_name FROM subjects as t1 LEFT JOIN schools as t2 ON t1.school_id = t2.id LEFT JOIN grades as t3 ON t1.grade_id = t3.id LEFT JOIN departments as t4 ON t1.department_id = t4.id WHERE t2.id = ? AND t1.grade_id= ?";
    db.driver.execQuery(query_str, [req.body.school_id, req.body.grade_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
