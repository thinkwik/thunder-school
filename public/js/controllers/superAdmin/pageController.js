app.controller('listChildPagesController', ['$scope', '$http', '$state', 'toaster', '$cookieStore','Upload',
    function($scope, $http, $state, toaster, $cookieStore,Upload) {
        $scope.addNewPage = function() {
            $state.go('users.child_pages_create');
        };

        $scope.getAllPages = function() {
            $http({
                method: 'POST',
                url: '/page/getAllPages'
            }).then(function successCallback(response) {
                $scope.Pages = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Pages', error.data.message);
            });
        };

        $scope.inactivePage = function(page_id) {
            var data = {
                id: page_id
            };
            $http({
                method: 'POST',
                url: '/page/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllPages();
            }, function errorCallback(error) {
                toaster.pop('error', 'Pages', error.data.message);
            });
        };
        $scope.activePage = function(page_id) {
            var data = {
                id: page_id
            };
            $http({
                method: 'POST',
                url: '/page/active',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllPages();
            }, function errorCallback(error) {
                toaster.pop('error', 'Pages', error.data.message);
            });
        };
        $scope.deletePage = function(page_id) {
            var data = {
                id: page_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/page/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllPages();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Pages', error.data.message);
                });
            }
        };
        $scope.editPage = function(page_id) {
            $state.go('users.child_pages_edit', {
                page_id: page_id
            });
        };
    }
]);

app.controller('editChildPagesController', ['$scope', '$http', '$state', 'toaster', '$cookieStore','Upload',
    function($scope, $http, $state, toaster, $cookieStore,Upload) {
      $http({
          method: "POST",
          url: "/menus/getAllChildMenus"
      }).then(function successCallback(response) {
          $scope.ChildMenus = response.data;
          $http({
              method: "POST",
              url: "/parent/pages/getAllParentPages"
          }).then(function successCallback(response) {
              $scope.ParentPages = response.data;
              $scope.getEditPage();
          }, function errorCallback(error) {
              toaster.pop('error', 'Pages', error.data.message);
          });
      }, function errorCallback(error) {
          toaster.pop('error', 'Menus', error.data.message);
      });

        $scope.SavePage = function() {
            var data = $scope.page;
            $http({
                method: 'POST',
                url: '/page/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Pages', 'updated successfully.');
                $state.go('users.child_pages_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'Pages', error.data.message);
            });
        };

        $scope.uploadFile = function(file) {
            if (!file) return console.log("No file selected !");
            Loading(true);
            Upload.upload({
                url: '/page/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                Loading(false);
                $scope.saveDisable = true;
                $scope.filename = success.data;
            });
        };

        $scope.getEditPage = function() {
            $http({
                method: 'GET',
                url: '/page/edit?id=' + $state.params.page_id
            }).then(function successCallback(response) {
                $scope.page = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Page', error.data.message);
            });
        };
        $scope.cancelPage = function() {
            $state.go('users.child_pages_list');
        };
    }
]);

app.controller('createChildPagesController', ['$scope', '$http', '$state', 'toaster', '$cookieStore','Upload',
    function($scope, $http, $state, toaster, $cookieStore,Upload) {
        $http({
            method: "POST",
            url: "/menus/getAllChildMenus"
        }).then(function successCallback(response) {
            $scope.ChildMenus = response.data;
            $http({
                method: "POST",
                url: "/parent/pages/getAllParentPages"
            }).then(function successCallback(response) {
                $scope.ParentPages = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Pages', error.data.message);
            });
        }, function errorCallback(error) {
            toaster.pop('error', 'Menus', error.data.message);
        });

        $scope.cancelPage = function() {
            $state.go('users.child_pages_list');
        };

        $scope.createPage = function() {
            var data = $scope.page;
            data.filenames = $scope.filename;
            $http({
                method: 'POST',
                url: '/page/register',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Pages', 'added successfully.');
                $state.go('users.child_pages_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'Pages', error.data.message);
            });
        };

        $scope.uploadFile = function(file) {
            if (!file) return console.log("No file selected !");
            Loading(true);
            Upload.upload({
                url: '/page/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                Loading(false);
                $scope.saveDisable = true;
                $scope.filename = success.data;
            });
        };
    }
]);
