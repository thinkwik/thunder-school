var db = require('orm').db,
    School = db.models.schools,
    Sessions = db.models.sessions;

exports.total_registered_districts = function(req, res) {
    var query_str = "SELECT count(*) as total_registered_districts FROM `schools` WHERE `parent_id` = ? AND school_type = ?";
    db.driver.execQuery(query_str, ['0', 'district'], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
exports.total_registered_schools = function(req, res) {
    var query_str = "SELECT count(*) as total_registered_schools FROM `schools` WHERE `parent_id` <> ? AND school_type = ?";
    db.driver.execQuery(query_str, ['0', 'school'], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
exports.districts_lists = function(req, res) {
    var query_str = "SELECT * FROM `schools` WHERE `parent_id` = ? AND school_type = ? ORDER BY `id` DESC LIMIT 7";
    db.driver.execQuery(query_str, ['0', 'district'], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
exports.schools_lists = function(req, res) {
    var query_str = "SELECT * FROM `schools` WHERE `parent_id` <> ? AND school_type = ? ORDER BY `id` DESC LIMIT 7";
    db.driver.execQuery(query_str, ['0', 'school'], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
exports.events_lists = function(req, res) {
    var query_str = "SELECT *, CONCAT(start_date) as start FROM `events` WHERE start_date >= CURDATE() ORDER BY `id` ASC LIMIT 7";
    db.driver.execQuery(query_str, ['1'], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
exports.events_lists_calendar = function(req, res) {
    var query_str = "SELECT *, CONCAT(start_date) as start FROM `events` WHERE ? ORDER BY `id` ASC";
    db.driver.execQuery(query_str, ['1'], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.payment_received = function(req, res) {
    return res.send("welcome page");
};
exports.total_students = function(req, res) {
    var query_str = "SELECT count(*) as total_students FROM `admissions` WHERE ?";
    db.driver.execQuery(query_str, ['1'], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
exports.chart_data = function(req, res) {
    var query_str = "select DATE(created_at) as date, COALESCE(count(*),0)  as count from admissions WHERE DATE(created_at) >= DATE(NOW()) - INTERVAL 7 DAY GROUP BY DATE(created_at)";
    db.driver.execQuery(query_str, ['1'], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};






//---------------district--------------------------
exports.district_total_registered_schools = function(req, res) {
    if (typeof req.body.district_id == 'undefined' && req.body.district_id == "") {
        return sendError(req, res, 422, "District ID required!");
    }
    var query_str = "SELECT count(*) as total_registered_schools FROM `schools` WHERE `parent_id` = ? AND school_type = ?";
    db.driver.execQuery(query_str, [req.body.district_id, 'school'], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
exports.district_schools_lists = function(req, res) {
    if (typeof req.body.district_id == 'undefined' && req.body.district_id == "") {
        return sendError(req, res, 422, "District ID required!");
    }
    var query_str = "SELECT * FROM `schools` WHERE `parent_id` = ? AND school_type = ? ORDER BY `id` DESC";
    db.driver.execQuery(query_str, [req.body.district_id, 'school'], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
exports.district_events_lists = function(req, res) {
    // console.log(req.body.district_id);
    if (typeof req.body.district_id == 'undefined' && req.body.district_id == "") {
        return sendError(req, res, 422, "District ID required!");
    }
    var query_str = "SELECT t1.* FROM `events` as t1 LEFT JOIN `schools` as t2 ON t1.school_id = t2.id  WHERE t2.parent_id = ? AND t1.start_date >= CURDATE() ORDER BY `id` ASC";
    db.driver.execQuery(query_str, [req.body.district_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
exports.district_events_lists_calendar = function(req, res) {
    // console.log(req.body.district_id);
    if (typeof req.body.district_id == 'undefined' && req.body.district_id == "") {
        return sendError(req, res, 422, "District ID required!");
    }
    var query_str = "SELECT t1.*, CONCAT(t1.start_date) as start FROM `events` as t1 LEFT JOIN `schools` as t2 ON t1.school_id = t2.id  WHERE t2.parent_id = ? ORDER BY `id` ASC";
    db.driver.execQuery(query_str, [req.body.district_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.district_payment_received = function(req, res) {
    return res.send("welcome page");
};
exports.district_total_students = function(req, res) {
    if (typeof req.body.district_id == 'undefined' && req.body.district_id == "") {
        return sendError(req, res, 422, "District ID required!");
    }
    var query_str = "SELECT count(*) as total_students FROM `admissions` as t1 LEFT JOIN `schools` as t2 ON t1.school_id = t2.id  WHERE t2.parent_id = ? ";
    db.driver.execQuery(query_str, [req.body.district_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
exports.district_total_departments = function(req, res) {
    if (typeof req.body.district_id == 'undefined' && req.body.district_id == "") {
        return sendError(req, res, 422, "District ID required!");
    }
    var query_str = "SELECT count(*) as total_departments FROM `departments` as t1 LEFT JOIN `schools` as t2 ON t1.school_id = t2.id  WHERE t2.parent_id = ? ";
    db.driver.execQuery(query_str, [req.body.district_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.district_chart_data = function(req, res) {
    if (typeof req.body.district_id == 'undefined' && req.body.district_id == "") {
        return sendError(req, res, 422, "District ID required!");
    }
    var query_str = "select DATE(t1.created_at) as date, COALESCE(count(*),0)  as count from admissions as t1 LEFT JOIN schools as t2 ON t1.school_id = t2.id WHERE DATE(t1.created_at) >= DATE(NOW()) - INTERVAL 7 DAY AND t2.parent_id = ?  GROUP BY DATE(created_at)";
    db.driver.execQuery(query_str, [req.body.district_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};


//---------------school--------------------------
exports.school_events_lists = function(req, res) {
    if (typeof req.body.school_id == 'undefined' && req.body.school_id == "") {
        return sendError(req, res, 422, "school ID required!");
    }
    var query_str = "SELECT t1.* FROM `events` as t1 LEFT JOIN `schools` as t2 ON t1.school_id = t2.id  WHERE t2.id = ? AND t1.start_date >= CURDATE() ORDER BY `id` ASC";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
exports.school_events_lists_calendar = function(req, res) {
    if (typeof req.body.school_id == 'undefined' && req.body.school_id == "") {
        return sendError(req, res, 422, "school ID required!");
    }
    var query_str = "SELECT t1.*, CONCAT(t1.start_date) as start FROM `events` as t1 LEFT JOIN `schools` as t2 ON t1.school_id = t2.id  WHERE t2.id = ? ORDER BY `id` ASC";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.school_students_lists = function(req, res) {
    if (typeof req.body.school_id == 'undefined' && req.body.school_id == "") {
        return sendError(req, res, 422, "school ID required!");
    }
    var query_str = "SELECT t1.*, CONCAT(t1.first_name,' ',t1.last_name) as student_name FROM `admissions` as t1 LEFT JOIN `schools` as t2 ON t1.school_id = t2.id  WHERE t2.id = ? ORDER BY `id` DESC";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
exports.school_staffs_lists = function(req, res) {
    if (typeof req.body.school_id == 'undefined' && req.body.school_id == "") {
        return sendError(req, res, 422, "school ID required!");
    }
    var query_str = "SELECT t1.*, CONCAT(t1.first_name,' ',t1.last_name) as staff_name FROM `staff` as t1 LEFT JOIN `schools` as t2 ON t1.school_id = t2.id  WHERE t2.id = ? ORDER BY `id` DESC";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.school_payment_received = function(req, res) {
    return res.send("welcome page");
};
exports.school_total_students = function(req, res) {
    if (typeof req.body.school_id == 'undefined' && req.body.school_id == "") {
        return sendError(req, res, 422, "school ID required!");
    }
    var query_str = "SELECT count(*) as total_students FROM `admissions` as t1 LEFT JOIN `schools` as t2 ON t1.school_id = t2.id  WHERE t2.id = ? ";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
exports.school_total_staffs = function(req, res) {
    if (typeof req.body.school_id == 'undefined' && req.body.school_id == "") {
        return sendError(req, res, 422, "school ID required!");
    }
    var query_str = "SELECT count(*) as total_staffs FROM `staff` as t1 LEFT JOIN `schools` as t2 ON t1.school_id = t2.id  WHERE t2.id = ? ";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.school_total_departments = function(req, res) {
    if (typeof req.body.school_id == 'undefined' && req.body.school_id == "") {
        return sendError(req, res, 422, "School ID required!");
    }
    var query_str = "SELECT count(*) as total_departments FROM `departments` as t1 LEFT JOIN `schools` as t2 ON t1.school_id = t2.id  WHERE t2.id = ? ";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.school_chart_data = function(req, res) {
    if (typeof req.body.school_id == 'undefined' && req.body.school_id == "") {
        return sendError(req, res, 422, "School ID required!");
    }
    var query_str = "select DATE(created_at) as date, COALESCE(count(*),0)  as count from admissions WHERE DATE(created_at) >= DATE(NOW()) - INTERVAL 7 DAY AND school_id = ? GROUP BY DATE(created_at)";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};



//---------------staff--------------------------
exports.staff_events_lists = function(req, res) {
    if (typeof req.body.school_id == 'undefined' && req.body.school_id == "") {
        return sendError(req, res, 422, "school ID required!");
    }
    var query_str = "SELECT t1.* FROM `events` as t1 LEFT JOIN `schools` as t2 ON t1.school_id = t2.id  WHERE t2.id = ? AND t1.start_date >= CURDATE() ORDER BY `id` ASC";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
exports.staff_events_lists_calendar = function(req, res) {
    if (typeof req.body.school_id == 'undefined' && req.body.school_id == "") {
        return sendError(req, res, 422, "school ID required!");
    }
    var query_str = "SELECT t1.*, CONCAT(t1.start_date) as start FROM `events` as t1 LEFT JOIN `schools` as t2 ON t1.school_id = t2.id  WHERE t2.id = ? ORDER BY `id` ASC";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.staff_students_lists = function(req, res) {
    if (typeof req.body.school_id == 'undefined' && req.body.school_id == "") {
        return sendError(req, res, 422, "school ID required!");
    }
    var query_str = "SELECT t1.*, CONCAT(t1.first_name,' ',t1.last_name) as student_name FROM `admissions` as t1 LEFT JOIN `schools` as t2 ON t1.school_id = t2.id  WHERE t2.id = ? ORDER BY `id` DESC";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
exports.staff_total_students = function(req, res) {
    if (typeof req.body.school_id == 'undefined' && req.body.school_id == "") {
        return sendError(req, res, 422, "school ID required!");
    }
    var query_str = "SELECT count(*) as total_students FROM `admissions` as t1 LEFT JOIN `schools` as t2 ON t1.school_id = t2.id  WHERE t2.id = ? ";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
exports.staff_total_schedules = function(req, res) {
    if (typeof req.body.staff_id == 'undefined' && req.body.staff_id == "") {
        return sendError(req, res, 422, "school ID required!");
    }
    var query_str = "SELECT count(*) as total_schedules FROM `lectures`  WHERE staff_id = ? ";
    db.driver.execQuery(query_str, [req.body.staff_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
exports.staff_total_assignments = function(req, res) {
    if (typeof req.body.staff_id == 'undefined' && req.body.staff_id == "") {
        return sendError(req, res, 422, "school ID required!");
    }
    var query_str = "SELECT count(*) as total_assignments FROM `assignments` WHERE staff_id = ? ";
    db.driver.execQuery(query_str, [req.body.staff_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.staff_total_pending_appointments = function(req, res) {
    if (typeof req.body.staff_id == 'undefined' && req.body.staff_id == "") {
        return sendError(req, res, 422, "school ID required!");
    }
    var query_str = "SELECT count(*) as total_pending_appointments FROM `appointments`  WHERE staff_id = ? AND status IN ('0')";
    db.driver.execQuery(query_str, [req.body.staff_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};




//---------------student--------------------------
exports.student_events_lists = function(req, res) {
    if (typeof req.body.school_id == 'undefined' && req.body.school_id == "") {
        return sendError(req, res, 422, "school ID required!");
    }
    var query_str = "SELECT t1.* FROM `events` as t1 LEFT JOIN `schools` as t2 ON t1.school_id = t2.id  WHERE t2.id = ? AND t1.start_date >= CURDATE() ORDER BY `id` ASC";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
exports.student_events_lists_calendar = function(req, res) {
    if (typeof req.body.school_id == 'undefined' && req.body.school_id == "") {
        return sendError(req, res, 422, "school ID required!");
    }
    var query_str = "SELECT t1.*, CONCAT(t1.start_date) as start FROM `events` as t1 LEFT JOIN `schools` as t2 ON t1.school_id = t2.id  WHERE t2.id = ? ORDER BY `id` ASC";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
exports.student_pending_assignments = function(req, res) {
    if (typeof req.body.grade_id == 'undefined' && req.body.grade_id == "") {
        return sendError(req, res, 422, "Grade ID required!");
    }
    var query_str = "SELECT * FROM `assignments` as t1 WHERE t1.grade_id = ? AND t1.due_date >= CURDATE() ORDER BY t1.due_date ASC";
    db.driver.execQuery(query_str, [req.body.grade_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
exports.student_total_staffs = function(req, res) {
    if (typeof req.body.grade_id == 'undefined' && req.body.grade_id == "") {
        return sendError(req, res, 422, "Grade ID required!");
    }
    var query_str = "SELECT COUNT(*) as total_staffs FROM `staff` as t1 LEFT JOIN lectures as t2 ON t2.staff_id = t1.id WHERE t2.grade_id = ?";
    db.driver.execQuery(query_str, [req.body.grade_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
exports.student_total_parents = function(req, res) {
    if (typeof req.body.student_id == 'undefined' && req.body.student_id == "") {
        return sendError(req, res, 422, "Student ID required!");
    }
    var query_str = "SELECT COUNT(*) as total_parents FROM `parents` WHERE student_id = ?";
    db.driver.execQuery(query_str, [req.body.student_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
exports.student_total_exams = function(req, res) {
    if (typeof req.body.grade_id == 'undefined' && req.body.grade_id == "") {
        return sendError(req, res, 422, "Grade ID required!");
    }
    var query_str = "SELECT COUNT(*) as total_exams FROM `exams` WHERE grade_id = ? AND start_date >= CURDATE()";
    db.driver.execQuery(query_str, [req.body.grade_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
exports.student_total_assignments = function(req, res) {
    if (typeof req.body.grade_id == 'undefined' && req.body.grade_id == "") {
        return sendError(req, res, 422, "Grade ID required!");
    }
    var query_str = "SELECT count(*) as total_assignments FROM `assignments` as t1 WHERE t1.grade_id = ? AND t1.due_date >= CURDATE() ORDER BY t1.due_date ASC";
    db.driver.execQuery(query_str, [req.body.grade_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
