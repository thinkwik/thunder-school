var db = require('orm').db,
    School = db.models.schools,
    Categories = db.models.product_category;

exports.register = function(req, res) {
    if (typeof req.body.school_id != 'undefined') {
        newCategory = new Categories();
        newCategory.school_id = req.body.school_id;
        newCategory.name = req.body.category_name;
        newCategory.description = req.body.category_description;
        newCategory.status = (typeof req.body.status != 'undefined') ? req.body.status : '1';
        newCategory.save(function(err, created_category) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, 'Check your parameters!!');
            }
            return res.send(created_category);
        });
    } else {
        return sendError(req, res, 422, 'school_id required for register new grade');
    }
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Category id required!!');
    }
    Categories.find({
        id: req.body.id
    }, 1, function(err, category) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, 'Check your parameters!!');
        }
        category[0].school_id = req.body.school_id;
        category[0].name = req.body.name;
        category[0].description = req.body.description;
        category[0].status = req.body.status;
        category[0].save(function(err, updated_category) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(updated_category);
        });
    });
};

exports.active = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Category id required!!');
    }
    Categories.find({
        id: req.body.id
    }, 1, function(err, Category) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, 'Check your parameters!!');
        }
        Category[0].status = "1";
        Category[0].save(function(err, activated_Category) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(activated_Category);
        });
    });
};

exports.inactive = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Category id required!!');
    }
    Categories.find({
        id: req.body.id
    }, 1, function(err, Category) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, 'Check your parameters!!');
        }
        Category[0].status = "0";
        Category[0].save(function(err, inactivated_Category) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(inactivated_Category);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Category id required!!');
    }
    Categories.find({
        id: req.body.id
    }, 1, function(err, Categories) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, 'Check your parameters!!');
        }
        Categories[0].remove(function(err) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(Categories[0]);
        });
    });
};

exports.getCategory = function(req, res) {
    if (typeof req.query.id == 'undefined') return res.send("id required for getting json");
    Categories.find({
        id: req.query.id
    }, 1, function(err, categories) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, 'Check your parameters!!');
        }
        if (!categories.length) return res.send("Categoriy not found with perticular id");
        return res.send(categories[0]);
    });
};

exports.getAllDistCategories = function(req, res) {
    if (typeof req.body.district_id == 'undefined' || req.body.district_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = "SELECT t1.*,t2.school_name FROM product_category as t1 LEFT JOIN schools as t2 on t1.school_id = t2.id WHERE  t2.parent_id = ?";
    db.driver.execQuery(query_str, [req.body.district_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, 'Check your parameters!!');
        }
        return res.send(rows);
    });
};

exports.getAllSchoolCategories = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = "SELECT t1.*,t2.school_name FROM product_category as t1 LEFT JOIN schools as t2 on t1.school_id = t2.id WHERE  t2.id = ?";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, 'Check your parameters!!');
        }
        return res.send(rows);
    });
};
