app.config(function($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider.otherwise("/");
    //Manage School Admins
    $stateProvider.state('schools', {
            url: '/schools',
            templateUrl: '/partials/profile/schools.html',
            abstract: true
        })
        .state('schools.changepassword', {
            url: '/changepassword',
            templateUrl: '/partials/superAdmin/district/school/header/changePassword.html'
        })
        .state('schools.edit_profile', {
            url: '/edit_profile',
            templateUrl: '/partials/superAdmin/district/school/header/editProfile.html',
            controller: 'editSchoolAdminController'
        })
        //Manage Dashboard
        .state('schools.dashboard', {
            url: '/school/dashboard',
            templateUrl: '/partials/superAdmin/district/school/manageDashboard/schoolAdmindashboard.html',
            controller: 'schoolAdminDashboardController'
        })
        //Manage To-Dos
        .state('schools.todo_create', {
            url: '/todo/create',
            templateUrl: '/partials/superAdmin/district/school/manageTodo/createTodo.html',
            controller: 'districtAdminTodoCreateController'
        })
        .state('schools.todo_list', {
            url: '/todo/list',
            templateUrl: '/partials/superAdmin/district/school/manageTodo/listTodo.html',
            controller: 'districtAdminTodoListController'
        })
        // Manage Notification
        .state('schools.notice_list', {
            url: '/school/notice/list',
            templateUrl: '/partials/superAdmin/district/school/manageNotice/listNotice.html',
            controller: 'listNoticesController'
        })
        .state('schools.notice_create', {
            url: '/school/notice/create',
            templateUrl: '/partials/superAdmin/district/school/manageNotice/createNotice.html',
            controller: 'createNoticeController'
        })
        .state('schools.notice_edit', {
            url: '/school/notice/edit/:notice_id',
            templateUrl: '/partials/superAdmin/district/school/manageNotice/editNotice.html',
            controller: 'editNoticeController'
        })
        // manage Events
        .state('schools.events_list', {
            url: '/school/events/list',
            templateUrl: '/partials/superAdmin/district/school/manageEvent/listEvent.html',
            controller: "schoolAllEventsListController"
        })
        .state('schools.event_create', {
            url: '/school/events/create',
            templateUrl: '/partials/superAdmin/district/school/manageEvent/createEvent.html',
            controller: "schoolNewEventController"
        })
        .state('schools.event_edit', {
            url: '/school/events/edit/:event_id',
            templateUrl: '/partials/superAdmin/district/school/manageEvent/editEvent.html',
            controller: 'schoolEditEventController'
        })
        //Manage Event Resource
        .state('schools.event_resource_list', {
            url: '/school/events/:event_id/resource/list',
            templateUrl: '/partials/superAdmin/district/school/manageEvent/listEventResource.html',
            controller: "schoolAllEventResourceListController"
        })
        .state('schools.event_resource_create', {
            url: '/school/events/:event_id/resource/create',
            templateUrl: '/partials/superAdmin/district/school/manageEvent/addEventResource.html',
            controller: "schoolNewEventResourceController"
        })
        .state('schools.event_resource_edit', {
            url: '/school/events/:event_id/resource/edit/:resource_id',
            templateUrl: '/partials/superAdmin/district/school/manageEvent/editEventResource.html',
            controller: 'schoolEditEventResourceController'
        })
        //Manage Event Registration
        .state('schools.event_registration_list', {
            url: '/events/registration/list',
            templateUrl: '/partials/superAdmin/district/school/manageEvent/listEventRegistration.html',
            controller: "schoolAllEventRegistrationsListController"
        })
        .state('schools.student_parents_event_registration_list', {
            url: '/events/registration/list',
            templateUrl: '/partials/superAdmin/district/school/manageEvent/listStudentParentEvents.html',
            controller: "schoolAllStudentsParentsEventRegistrationsListController"
        })
        .state('schools.event_registration_create', {
            url: '/events/registration/create',
            templateUrl: '/partials/superAdmin/district/school/manageEvent/addEventRegistration.html',
            controller: "schoolNewEventRegistrationController"
        })
        .state('schools.event_registration_edit', {
            url: '/events/registration/edit/:registration_id',
            templateUrl: '/partials/superAdmin/district/school/manageEvent/editEventRegistration.html',
            controller: 'schoolEditEventRegistrationController'
        })
        //Manage School Roles
        .state('schools.school_roles_list', {
            url: '/school/role/list',
            templateUrl: '/partials/superAdmin/district/school/manageRole/listRole.html',
            controller: "schoolRoleListController"
        })
        .state('schools.school_roles_create', {
            url: '/school/role/create',
            templateUrl: 'partials/superAdmin/district/school/manageRole/addRole.html',
            controller: "schoolRoleCreateController"
        })
        .state('schools.school_roles_edit', {
            url: '/school/role/edit/:role_id',
            templateUrl: 'partials/superAdmin/district/school/manageRole/editRole.html',
            controller: 'schoolRoleEditController'
        })
        //Manage School Roles
        .state('schools.staff_roles_list', {
            url: '/staff/role/list',
            templateUrl: '/partials/superAdmin/district/school/manageStaffRole/listRole.html',
            controller: "staffRoleListController"
        })
        .state('schools.staff_roles_create', {
            url: '/staff/role/create',
            templateUrl: 'partials/superAdmin/district/school/manageStaffRole/addRole.html',
            controller: "staffRoleCreateController"
        })
        .state('schools.staff_roles_edit', {
            url: '/staff/role/edit/:role_id',
            templateUrl: 'partials/superAdmin/district/school/manageStaffRole/editRole.html',
            controller: 'staffRoleEditController'
        })
        //Manage School Admin
        .state('schools.schoolAdmin_list', {
            url: '/admin/manage/list',
            templateUrl: '/partials/superAdmin/district/school/manageAdmin/listSchoolAdmins.html',
            controller: 'schoolSchoolSuperAdminListController'
        })
        .state('schools.schoolAdmin_create', {
            url: '/admin/manage/create',
            templateUrl: '/partials/superAdmin/district/school/manageAdmin/addSchoolAdmin.html',
            controller: 'schoolSchoolSuperAdminCreateController'
        })
        .state('schools.schoolAdmin_edit', {
            url: '/admin/manage/edit/:user_id',
            templateUrl: '/partials/superAdmin/district/school/manageAdmin/editSchoolAdmin.html',
            controller: 'schoolSchoolSuperAdminEditController'
        })
        //Manage School Fees
        .state('schools.school_fees_list', {
            url: '/school/fee/list',
            templateUrl: '/partials/superAdmin/district/school/manageFee/listFee.html',
            controller: "schoolFeeListController"
        })
        .state('schools.school_fees_create', {
            url: '/school/fee/create',
            templateUrl: 'partials/superAdmin/district/school/manageFee/addFee.html',
            controller: "schoolFeeCreateController"
        })
        .state('schools.school_fees_edit', {
            url: '/school/fee/edit/:fee_id',
            templateUrl: 'partials/superAdmin/district/school/manageFee/editFee.html',
            controller: 'schoolFeeEditController'
        })
        // Manage ProductCategory
        .state('schools.productCategory_list', {
            url: '/school/category/list',
            templateUrl: '/partials/superAdmin/district/school/manageE-commercePortal/manageCategory/listCategory.html',
            controller: 'listSchoolProductCategoryController'
        })
        .state('schools.productCategory_create', {
            url: '/school/category/create',
            templateUrl: '/partials/superAdmin/district/school/manageE-commercePortal/manageCategory/createCategory.html',
            controller: 'createSchoolProductCategoryController'
        })
        .state('schools.productCategory_edit', {
            url: '/school/category/edit/:category_id',
            templateUrl: '/partials/superAdmin/district/school/manageE-commercePortal/manageCategory/editCategory.html',
            controller: 'editSchoolProductCategoryController'
        })
        // Manage Sources
        .state('schools.source_list', {
            url: '/source/list',
            templateUrl: '/partials/superAdmin/district/manageSource/listSource.html',
            controller: "listSourceController"
        })
        .state('schools.source_create', {
            url: '/source/create',
            templateUrl: '/partials/superAdmin/district/manageSource/createSource.html',
            controller: "createSourceController"
        })
        .state('schools.source_edit', {
            url: '/source/edit/:source_id',
            templateUrl: '/partials/superAdmin/district/manageSource/editSource.html',
            controller: 'editSourceController'
        })
        //Promote Students
        .state('schools.promote_student', {
            url: '/promote/students',
            templateUrl: '/partials/superAdmin/district/school/promoteStudents/promoteStudents.html',
            controller: 'promoteSchoolStudentsController'
        })

        // Manage Products
        .state('schools.product_list', {
            url: '/school/products/list',
            templateUrl: '/partials/superAdmin/district/school/manageE-commercePortal/manageProducts/listProduct.html',
            controller: 'listSchoolProductController'
        })
        .state('schools.product_create', {
            url: '/school/products/create',
            templateUrl: '/partials/superAdmin/district/school/manageE-commercePortal/manageProducts/createProduct.html',
            controller: 'createSchoolProductController'
        })
        .state('schools.product_edit', {
            url: '/school/products/edit/:product_id',
            templateUrl: '/partials/superAdmin/district/school/manageE-commercePortal/manageProducts/editProduct.html',
            controller: 'editSchoolProductController'
        })
        //manage Department
        .state('schools.schooldepartment_list', {
            url: '/school/department/list',
            templateUrl: '/partials/superAdmin/district/school/manageAcademics/manageSchoolDpts/listSchoolDept.html',
            controller: 'listSchoolDeptController'
        })
        .state('schools.schooldepartment_create', {
            url: '/school/department/create',
            templateUrl: '/partials/superAdmin/district/school/manageAcademics/manageSchoolDpts/createSchoolDept.html',
            controller: 'createSchoolDeptController'
        })
        .state('schools.schooldepartment_edit', {
            url: '/school/department/edit/:department_id',
            templateUrl: '/partials/superAdmin/district/school/manageAcademics/manageSchoolDpts/editSchoolDept.html',
            controller: 'editSchoolDeptController'
        })
        // Manage Sesions
        .state('schools.session_list', {
            url: '/school/session/list',
            templateUrl: '/partials/superAdmin/district/school/manageSession/listSession.html',
            controller: 'listSchoolSessionController'
        })
        .state('schools.session_create', {
            url: '/school/session/create',
            templateUrl: '/partials/superAdmin/district/school/manageSession/createSession.html',
            controller: 'createSchoolSessionController'
        })
        .state('schools.session_edit', {
            url: '/school/session/edit/:session_id',
            templateUrl: '/partials/superAdmin/district/school/manageSession/editSession.html',
            controller: 'editSchoolSessionController'
        })
        //manage Grades
        .state('schools.grades_list', {
            url: '/school/grades/list',
            templateUrl: '/partials/superAdmin/district/school/manageAcademics/manageGrades/listGrades.html',
            controller: 'listSchoolGradesController'
        })
        .state('schools.grades_create', {
            url: '/school/grades/create',
            templateUrl: '/partials/superAdmin/district/school/manageAcademics/manageGrades/createGrades.html',
            controller: 'createSchoolGradesController'
        })
        .state('schools.grades_edit', {
            url: '/school/grades/edit/:grade_id',
            templateUrl: '/partials/superAdmin/district/school/manageAcademics/manageGrades/editGrades.html',
            controller: 'editSchoolGradesController'
        })
        // Manage Designationss
        .state('schools.designation_list', {
            url: '/school/designation/list',
            templateUrl: '/partials/superAdmin/district/school/manageAcademics/manageDesignation/listDesignation.html',
            controller: 'listSchoolDesignationController'
        })
        .state('schools.designation_create', {
            url: '/school/designation/create',
            templateUrl: '/partials/superAdmin/district/school/manageAcademics/manageDesignation/createDesignation.html',
            controller: 'createSchoolDesignationController'
        })
        .state('schools.designation_edit', {
            url: '/school/designation/edit/:designation_id',
            templateUrl: '/partials/superAdmin/district/school/manageAcademics/manageDesignation/editDesignation.html',
            controller: 'editSchoolDesignationController'
        })
        //manage Class
        .state('schools.class_list', {
            url: '/school/class/list',
            templateUrl: '/partials/superAdmin/district/school/manageAcademics/manageClasses/listClass.html',
            controller: 'listSchoolClassController'
        })
        .state('schools.class_create', {
            url: '/school/class/create',
            templateUrl: '/partials/superAdmin/district/school/manageAcademics/manageClasses/createClass.html',
            controller: 'createSchoolClassController'
        })
        .state('schools.class_edit', {
            url: '/school/class/edit/:class_id',
            templateUrl: '/partials/superAdmin/district/school/manageAcademics/manageClasses/editClass.html',
            controller: 'editSchoolClassController'
        })
        //manage Subjects
        .state('schools.subject_list', {
            url: '/school/subject/list',
            templateUrl: '/partials/superAdmin/district/school/manageAcademics/manageSubjects/listSubject.html',
            controller: 'listSchoolSubjectController'
        })
        .state('schools.subject_create', {
            url: '/school/subject/create',
            templateUrl: '/partials/superAdmin/district/school/manageAcademics/manageSubjects/createSubject.html',
            controller: 'createSchoolSubjectController'
        })
        .state('schools.subject_edit', {
            url: '/school/subject/edit/:subject_id',
            templateUrl: '/partials/superAdmin/district/school/manageAcademics/manageSubjects/editSubject.html',
            controller: 'editSchoolSubjectController'
        })
        // Manage Staff
        .state('schools.staff_list', {
            url: '/school/staff/list',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/listStaff.html',
            controller: 'schoolListStaffController'
        })
        .state('schools.staff_create', {
            url: '/school/staff/create',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/createStaff.html',
            controller: 'schoolCreateStaffController'
        })
        .state('schools.staff_edit', {
            url: '/school/staff/edit/:staff_id',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/editStaff.html',
            controller: 'schoolEditStaffController'
        })
        // Manage Medical
        .state('schools.medical_info_list', {
            url: '/school/medical_info/list',
            templateUrl: '/partials/superAdmin/district/school/medicalInfo/list.html',
            controller: 'schoolListMedicalInfoController'
        })
        .state('schools.medical_info_create', {
            url: '/school/medical_info/create',
            templateUrl: '/partials/superAdmin/district/school/medicalInfo/create.html',
            controller: 'schoolCreateMedicalInfoController'
        })
        .state('schools.medical_info_edit', {
            url: '/school/medical_info/edit/:info_id',
            templateUrl: '/partials/superAdmin/district/school/medicalInfo/edit.html',
            controller: 'schoolEditMedicalInfoController'
        })
        
        // Manage Admission
        .state('schools.admission_list', {
            url: '/admission/list',
            templateUrl: '/partials/superAdmin/district/manageSchoolAdmin/manageAdmission/listAdmission.html',
            controller: 'listSchoolAdmissionController'
        })
        .state('schools.student_create', {
            url: '/student/create/:student_id',
            templateUrl: '/partials/superAdmin/district/manageSchoolAdmin/manageStudents/createStudents.html',
            controller: 'editStaffSchoolAdmissionController'
        })
        .state('schools.student_academics', {
            url: '/student/create/:student_id',
            templateUrl: '/partials/superAdmin/district/manageSchoolAdmin/manageStudents/createStudentAcademics.html',
            controller: 'editStaffSchoolAdmissionController'
        })
        .state('schools.student_skills', {
            url: '/student/create/:student_id',
            templateUrl: '/partials/superAdmin/district/manageSchoolAdmin/manageStudents/createStudentSkills.html',
            controller: 'editStaffSchoolAdmissionController'
        })
        .state('schools.admission_create', {
            url: '/admission/create',
            templateUrl: '/partials/superAdmin/district/manageSchoolAdmin/manageAdmission/createAdmission.html',
            controller: 'createSchoolAdmissionController'
        })
        .state('schools.admission_edit', {
            url: '/admission/edit/:student_id',
            templateUrl: '/partials/superAdmin/district/manageSchoolAdmin/manageAdmission/editAdmission.html',
            controller: 'editSchoolAdmissionController'
        })
        .state('schools.student_list', {
            url: '/student/list',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageStudents/listStudent.html',
            controller: 'listStudentController'
        })
        .state('schools.student_profile_view', {
            url: '/student/profile/:student_id',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageStudents/viewStudentProfile.html',
            controller: 'viewStudentProfileController'
        })

        .state('schools.student_parents_create', {
            url: '/student/parents/create/:student_id',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageStudents/createStudentParents.html',
            controller: 'saveSchoolStudentsParentsController'
        })
        .state('schools.view_followUp', {
            url: '/student/viewfollowup/:student_id',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageStudents/viewFollowUp.html',
            controller: 'viewFollowUpSchoolController'
        })
        .state('schools.create_followUp', {
            url: '/student/createFollowup/:student_id',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageStudents/createFollowUp.html',
            controller: 'createFollowUpSchoolController'
        })
        .state('schools.student_parents_edit', {
            url: '/student/parents/edit/:parent_id',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageStudents/editStudentParents.html',
            controller: 'editStudentsParentsController'
        })


        //Manage Material
        .state('schools.create_material', {
            url: '/material/create',
            templateUrl: '/partials/superAdmin/district/school/manageMaterials/createMaterial.html',
            controller: 'createSchoolMaterialController'
        }).state('schools.list_material', {
            url: '/material/list',
            templateUrl: '/partials/superAdmin/district/school/manageMaterials/listMaterials.html',
            controller: 'listSchoolMaterialController'
        }).state('schools.edit_material', {
            url: '/material/edit/:material_id',
            templateUrl: '/partials/superAdmin/district/school/manageMaterials/editMaterial.html',
            controller: 'editSchoolMaterialController'
        })
        // Manage Lectures
        .state('schools.create_lecture', {
            url: '/lecture/create',
            templateUrl: '/partials/superAdmin/district/school/manageLectures/createLecture.html',
            controller: 'createLectureController'
        })
        .state('schools.list_lecture', {
            url: '/lecture/list',
            templateUrl: '/partials/superAdmin/district/school/manageLectures/listLecture.html',
            controller: 'listLectureController'
        })
        .state('schools.edit_lecture', {
            url: '/lecture/edit/:lecture_id',
            templateUrl: '/partials/superAdmin/district/school/manageLectures/editLecture.html',
            controller: 'editLectureController'
        })
        // Manage Gallery
        .state('schools.gallery_list', {
            url: '/gallery/list',
            templateUrl: '/partials/superAdmin/district/school/manageGallery/listGallery.html',
            controller: "listSchoolGalleryController"
        })
        .state('schools.gallery_create', {
            url: '/gallery/create',
            templateUrl: '/partials/superAdmin/district/school/manageGallery/createGallery.html',
            controller: "createSchoolGalleryController"
        })
        .state('schools.gallery_edit', {
            url: '/gallery/edit/:gallery_id',
            templateUrl: '/partials/superAdmin/district/school/manageGallery/editGallery.html',
            controller: 'editSchoolGalleryController'
        })
        .state('schools.gallery_view', {
            url: '/gallery/view/:gallery_id',
            templateUrl: '/partials/superAdmin/district/school/manageGallery/viewAlbum.html',
            controller: 'viewSchoolGalleryAlbumController'
        })
        .state('schools.image_add', {
            url: '/gallery/addimage/:gallery_id',
            templateUrl: '/partials/superAdmin/district/manageGallery/addImage.html',
            controller: 'addDistrictImageGalleryController'
        })
        // Manage Emails
        .state('schools.list_emails', {
            url: '/email/:material_id',
            templateUrl: '/partials/superAdmin/district/school/manageEmails/manageEmails.html',
            controller: 'listSchoolAdminEmailsController'
        })
        .state('schools.create_emails', {
            url: '/email/:material_id',
            templateUrl: '/partials/superAdmin/district/school/manageEmails/createEmail.html',
            controller: 'createSchoolAdminEmailsController'
        })
        //Manage Locations
        .state('schools.create_location', {
            url: '/location/create',
            templateUrl: '/partials/superAdmin/district/school/manageLocation/createLocation.html',
            controller: 'createLocationController'
        })
        .state('schools.edit_location', {
            url: '/location/edit/:location_id',
            templateUrl: '/partials/superAdmin/district/school/manageLocation/editLocation.html',
            controller: 'editLocationController'
        })
        .state('schools.list_location', {
            url: '/location/list',
            templateUrl: '/partials/superAdmin/district/school/manageLocation/listLocation.html',
            controller: 'listLocationController'
        })
        //Manage Student Relations
        .state('schools.create_relations', {
            url: '/relation/create',
            templateUrl: '/partials/superAdmin/district/school/manageRelations/createRelation.html',
            controller: 'createRelationController'
        })
        .state('schools.edit_relations', {
            url: '/relation/edit/:relation_id',
            templateUrl: '/partials/superAdmin/district/school/manageRelations/editRelation.html',
            controller: 'editRelationController'
        })
        .state('schools.list_relations', {
            url: '/relation/list',
            templateUrl: '/partials/superAdmin/district/school/manageRelations/listRelation.html',
            controller: 'listRelationController'
        })
        // Manage Student Email Notification
        .state('schools.email_notice_list', {
            url: '/school/email_notice/list',
            templateUrl: '/partials/superAdmin/district/school/manageEmailNotification/listEmailNotification.html',
            controller: 'listEmailNotificationsController'
        })
        .state('schools.email_notice_create', {
            url: '/school/email_notice/create',
            templateUrl: '/partials/superAdmin/district/school/manageEmailNotification/createNotification.html',
            controller: 'createEmailNotificationsController'
        })
        //Mange BookStore
        .state('schools.list_book_store', {
            url: '/school/bookstore/list',
            templateUrl: '/partials/superAdmin/district/school/bookStore/listBookStore.html',
            controller: 'listBookStoreController'
        })
        //Manage Epub books
        .state('schools.view_epub_book', {
            url: '/bookstore/epub/view/:book_id',
            templateUrl: '/partials/superAdmin/district/school/bookStore/viewEpubBook.html',
            controller: 'viewSchoolEpubBookStoreController'
        })
        .state('schools.create_book_store', {
            url: '/school/bookstore/create',
            templateUrl: '/partials/superAdmin/district/school/bookStore/createBookStore.html',
            controller: 'createBookStoreController'
        })
        .state('schools.edit_book_store', {
            url: '/school/bookstore/edit/:book_id',
            templateUrl: '/partials/superAdmin/district/school/bookStore/editBookStore.html',
            controller: 'editBookStoreController'
        })
        .state('schools.view_book', {
            url: '/school/bookstore/view/:book_id',
            templateUrl: '/partials/superAdmin/district/school/bookStore/viewBook.html',
            controller: 'viewBookSchoolAdminController'
        })
        .state('schools.list_book_category', {
            url: '/book/category/list',
            templateUrl: '/partials/superAdmin/district/school/bookCategory/listBookCategory.html',
            controller: "listBookCategoryController"
        })
        .state('schools.create_book_category', {
            url: '/book/category/create',
            templateUrl: '/partials/superAdmin/district/school/bookCategory/createBookCategory.html',
            controller: "createBookCategoryController"
        })

        //Manage Templates
        .state('schools.templates_list', {
            url: '/template/list',
            templateUrl: '/partials/superAdmin/district/school/manageTemplates/listTemplate.html',
            controller: "listSchoolTemplateController"
        })
        .state('schools.templates_create', {
            url: '/template/create',
            templateUrl: '/partials/superAdmin/district/school/manageTemplates/createTemplate.html',
            controller: "createSchoolTemplateController"
        })
        .state('schools.templates_edit', {
            url: '/template/edit/:template_id',
            templateUrl: '/partials/superAdmin/district/school/manageTemplates/editTemplate.html',
            controller: "editSchoolTemplateController"
        })
        //Manage Templates
        .state('schools.grade_scale_list', {
            url: '/grade_scale/list',
            templateUrl: '/partials/superAdmin/district/school/manageGradeScale/listTemplate.html',
            controller: "listGradeScaleController"
        })
        .state('schools.grade_scale_create', {
            url: '/grade_scale/create',
            templateUrl: '/partials/superAdmin/district/school/manageGradeScale/createTemplate.html',
            controller: "createGradeScaleController"
        })
        .state('schools.grade_scale_edit', {
            url: '/grade_scale_edit/:grade_scale_id',
            templateUrl: '/partials/superAdmin/district/school/manageGradeScale/editTemplate.html',
            controller: "editGradeScaleController"
        })
        .state('schools.generate_report', {
            url: '/generate_report',
            templateUrl: '/partials/superAdmin/district/school/manageReports/createReport.html',
            controller: "createReportController"
        })

    $locationProvider.html5Mode(true);
});
