var bCrypt = require('bcrypt-nodejs');
var db = require('orm').db,
    Parent = db.models.parents;

var createHash = function(password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
};
var isValidPassword = function(staff, password) {
    return bCrypt.compareSync(password, staff.password);
};

var encrypt = require('../../../config/encrypt_decrypt').encrypt;
var decrypt = require('../../../config/encrypt_decrypt').decrypt;
var serialize = require('node-serialize');

exports.tryLogin = function(req, res) {
    if (typeof req.body.email == 'undefined' || req.body.password == 'undefined') {
        return apiSendError(req, res, 422, "Please check your parameters");
    }
    var student_grade_id;
    Parent.find({
        email: req.body.email
    }, 1, function(err, parents) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 422, "Bad Request check your parameters");
        } else if (parents.length > 0) {
            if (parents.length > 0 && !student_grade_id) {
                var query_str = 'SELECT staff_id,school_id,grade_id from admissions WHERE id = ?';
                db.driver.execQuery(query_str, [parents[0].student_id], function(err, rows) {
                    if (err) {
                        console.log(err);
                        return apiSendError(req, res, 400, "Bad Request check your parameters");
                    }
                    school_id = rows[0].school_id;
                    staff_id = rows[0].staff_id;
                    student_grade_id = rows[0].grade_id;
                    if (req.body.password && student_grade_id && school_id) {
                        var hashFromDB = parents[0].password;
                        var plainPassFromUser = req.body.password;
                        if (isValidPassword(parents[0], plainPassFromUser)) {
                            var data = {
                                id: parents[0].id,
                                type: "parent",
                                email: parents[0].email
                            };
                            parents[0].staff_id = rows[0].staff_id;
                            parents[0].school_id = rows[0].school_id;
                            parents[0].student_grade_id = rows[0].grade_id;
                            var query_str = `SELECT * from followers WHERE follower_id = ?`;
                            db.driver.execQuery(query_str, [parents[0].unique_id], function(err, rows) {
                                if (err) {
                                    console.log(err);
                                    return sendError(req, res, 400, "Bad Request check your parameters");
                                }
                                if (!rows) {
                                    parents[0].Following = 0;
                                }
                                parents[0].Following = rows.length;
                                var query_str = `SELECT * from followers WHERE following_id = ?`;
                                db.driver.execQuery(query_str, [parents[0].unique_id], function(err, rowsss) {
                                    if (err) {
                                        console.log(err);
                                        return sendError(req, res, 400, "Bad Request check your parameters");
                                    }
                                    if (!rowsss) {
                                        parents[0].Followers = 0;
                                    }
                                    parents[0].Followers = rowsss.length;
                                    var data = {
                                        id: parents[0].id,
                                        type: "parent",
                                        email: parents[0].email
                                    };
                                    var response = {};
                                    response['data'] = parents[0];
                                    response['access_token'] = encrypt(serialize.serialize(data));
                                    response['code'] = 200;
                                    return res.send(response);

                                });
                            });
                        }
                    } else {
                        return apiSendError(req, res, 404, "Please enter Password");
                    }
                });
            }
        } else {
            return apiSendError(req, res, 404, "Parent not found");
        }
    });
};

exports.me = function(req, res) {
    Parent.find({
        id: req.api_token_data.id
    }, 1, function(err, parent) {
        if (err) {
            return apiSendError(req, res, 500, "Something went wrong!");
        }
        if (!parent.length) {
            return apiSendError(req, res, 404, "Parent not found!");
        }
        var response = {};
        response['data'] = parent[0];
        response['code'] = 200;
        return res.send(response);
    });
};

exports.profile = function(req, res) {
    if (typeof req.body.parent_id == 'undefined') {
        return apiSendError(req, res, 422, "Other Parent id required");
    }
    Parent.find({
        id: req.body.parent_id
    }, 1, function(err, parent) {
        if (err) {
            return apiSendError(req, res, 500, "Something went wrong!");
        }
        if (!parent.length) {
            return apiSendError(req, res, 404, "Parent not found!");
        }
        var response = {};
        response['data'] = parent[0];
        response['code'] = 200;
        return res.send(response);
    });
};

exports.changePassword = function(req, res) {
    if (req.body.id !== 'undefined' && req.body.id) {
        Parent.find({
            id: req.body.id
        }, 1, function(err, parents) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 422, "Bad Request check your parameters");
            }
            if (!parents.length) {
                return apiSendError(req, res, 404, "Parents not found");
            } else {
                parent = parents[0];
                if (!isValidPassword(parent, req.body.oldPassword)) {
                    return apiSendError(req, res, 422, "You have entered wrong password!");
                } else if (req.body.newPassword != req.body.confirmNewPassword) {
                    return apiSendError(req, res, 422, "new password and confirm new password must be same");
                }
                parent.password = createHash(req.body.newPassword);
                parent.save(function(err, saved_parent) {
                    if (err) {
                        console.log(err);
                        return apiSendError(req, res, 500, "Something went wrong!Please try again");
                    }
                    var response = {};
                    response['data'] = [];
                    response['code'] = 200;
                    return res.send(response);
                });
            }
        });
    } else {
        return apiSendError(req, res, 422, "Bad Request check your parameters");
    }
};

exports.getParentProfile = function(req, res) {
    if (typeof req.query.id == 'undefined' || typeof req.query.id === null) {
        return apiSendError(req, res, 422, "id required");
    }
    var query_str = 'SELECT t1.* FROM `parents` as t1 LEFT JOIN admissions as t2 ON t1.student_id = t2.id WHERE t1.id = ?';
    db.driver.execQuery(query_str, [req.query.id], function(err, parents) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 400, "Bad Request check your parameters");
        }
        db.driver.execQuery(query_str, [parents[0].unique_id], function(err, rows) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            if (!rows) {
                parents[0].Following = 0;
            }
            parents[0].Following = rows.length;
            var query_str = `SELECT * from followers WHERE following_id = ?`;
            db.driver.execQuery(query_str, [parents[0].unique_id], function(err, rowsss) {
                if (err) {
                    console.log(err);
                    return sendError(req, res, 400, "Bad Request check your parameters");
                }
                if (!rowsss) {
                    parents[0].Followers = 0;
                }
                parents[0].Followers = rowsss.length;
                var response = {};
                response['data'] = parents[0];
                response['code'] = 200;
                return res.send(response);
            })
        })
    });
};

exports.edit = function(req, res) {
    var editedPhoto;
    if (typeof req.body.id == 'undefined') {
        return apiSendError(req, res, 404, 'Parent id required!!');
    }
    Parent.find({
        id: req.body.id
    }, function(err, parents) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 422, "Bad Request check your parameters");
        }
        if (!parents.length) {
            return apiSendError(req, res, 404, "Parents not found");
        }
        if (req.files) {
            if (!req.files.parentImage) {
                return sendError(req, res, 400, "Bad Request check your parameters");
            } else {
                var fs = require('fs');
                var parentsImage = __dirname + "/../../../public/upload/parents/";
                if (!fs.existsSync(parentsImage)) {
                    fs.mkdirSync(parentsImage);
                }
                var file = req.files.parentImage;
                var fileExtension = file.name.split(".")[1];
                if (fileExtension) {
                    fileName = file.path.split('/');
                    fs.rename(file.path, parentsImage + fileName[fileName.length - 1] + "." + fileExtension);
                    editedPhoto = fileName[fileName.length - 1] + "." + fileExtension;
                }
            }
        }
        parents[0].full_name = req.body.full_name;
        parents[0].relation = req.body.relation;
        parents[0].gender = req.body.gender;
        parents[0].mobile = req.body.mobile;
        parents[0].parentImage = (typeof editedPhoto == 'undefined' || editedPhoto === "") ? parents[0].parentImage : editedPhoto;
        parents[0].address = req.body.address;
        parents[0].city = req.body.city;
        parents[0].state = req.body.state;
        // parents[0].country = req.body.country;
        parents[0].zipcode = req.body.zipcode;
        parents[0].profile_status = req.body.profile_status;
        parents[0].save(function(err, updated_parents) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 422, "Bad Request check your parameters");
            }
            var response = {};
            response['data'] = [];
            response['code'] = 200;
            return res.send(response);
        });
    });
};

exports.getAllChildren = function(req, res) {
    if (typeof req.body.email == 'undefined' || typeof req.body.email === null) {
        return sendError(req, res, 422, "id required");
    }
    var query_str = 'SELECT t2.* FROM parents as t1 LEFT JOIN admissions as t2 ON t1.student_id = t2.id WHERE t1.email = ?';
    db.driver.execQuery(query_str, [req.body.email], function(err, rows) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!rows.length) {
            return apiSendError(req, res, 404, "Parents not found");
        }
        var response = {};
        response['data'] = rows;
        response['code'] = 200;
        return res.send(response);
    });
};



// exports.forgotPassword = function(req, res) {
//
//   console.log(req.body);
//
//     // if (req.body.id !== 'undefined' && req.body.id) {
//     //     Parent.find({
//     //         id: req.body.id
//     //     }, 1, function(err, parents) {
//     //         if (err) {
//     //             console.log(err);
//     //             return apiSendError(req, res, 422, "Bad Request check your parameters");
//     //         }
//     //         if (!parents.length) {
//     //             return apiSendError(req, res, 404, "Parents not found");
//     //         } else {
//     //             parent = parents[0];
//     //             if (!isValidPassword(parent, req.body.oldPassword)) {
//     //                 return apiSendError(req, res, 422, "You have entered wrong password!");
//     //             } else if (req.body.newPassword != req.body.confirmNewPassword) {
//     //                 return apiSendError(req, res, 422, "new password and confirm new password must be same");
//     //             }
//     //             parent.password = createHash(req.body.newPassword);
//     //             parent.save(function(err, saved_parent) {
//     //                 if (err) {
//     //                     console.log(err);
//     //                     return apiSendError(req, res, 500, "Something went wrong!Please try again");
//     //                 }
//     //                 var response = {};
//     //                 response['data'] = [];
//     //                 response['code'] = 200;
//     //                 return res.send(response);
//     //             });
//     //         }
//     //     });
//     // } else {
//     //     return apiSendError(req, res, 422, "Bad Request check your parameters");
//     // }
// };
// exports.sendMail = function(req, res) {
//     var from = 'binit@thinkwik.com';
//     var to = 'binit@thinkwik.com,parth@thinkwik.com';
//     var subject = 'Test Mail from Thunder Mail';
//     var html = "Hii This is mail from logged in user " + req.user.email + "thanks";
//     sendMail(req, res, from, to, subject, html, function(err, success) {
//         if (err) {
//             console.log(err);
//             console.log("something wnet wrong");
//             return res.send('error');
//         }
//         return res.send("success");
//     });
//
// };
