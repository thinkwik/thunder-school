var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Package = db.define('payments', {
    school_id: {
        type: 'integer'
    },
    payment_description: String,
    package_id: {
        type: 'integer'
    }
}, {
    timestamp: true,
    methods: {

    }
});