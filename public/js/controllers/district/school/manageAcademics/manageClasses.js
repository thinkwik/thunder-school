app.controller('createSchoolClassController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.listClasses = function() {
            $state.go('schools.class_list');
        };

        $scope.getSchoolDepartments = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/school/department/getAllSchoolDepartment',
                data: data
            }).then(function successCallback(response) {
                $scope.departments = response.data;
                $scope.getSchoolStaffs();
                $scope.getSchoolGrades();
                $scope.getLocations();
            }, function errorCallback(error) {
                toaster.pop('error', 'Departments', 'Something went wrong!');
            });
        };

        $scope.getSchoolStaffs = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/staff/getSchoolsAllStaff',
                data: data
            }).then(function successCallback(response) {
                $scope.staffs = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'School', 'Something went wrong!');
            });
        };


        $scope.getLocations = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/location/getAllLocations',
                data: data
            }).then(function successCallback(response) {
                $scope.locations = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Locations', 'Something went wrong!');
            });
        };

        $scope.getSchoolGrades = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/grade/getAllSchoolGrades',
                data: data
            }).then(function successCallback(response) {
                $scope.grades = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Grades', 'Something went wrong!');
            });
        };

        $scope.getSubjectGradeWise = function(grade_id) {
            $scope.class.subject_id = "";
            var data = {
                grade_id: grade_id,
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/subject/getSchoolGradesSubjectWise',
                data: data
            }).then(function successCallback(response) {
                $scope.subjects = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Subjects', 'Something went wrong!');
            });
        };

        $scope.createClass = function() {
            data = $scope.class;
            data.start_time = $("#starttime").val();
            data.end_time = $("#endtime").val();
            data.school_id = $cookieStore.get('loggedInSchool').id;
            if($scope.class.day.length <= 0 ) {
                toaster.pop('error', "Day", "Please select at list one day");
                return false;
            }
            data.day = $scope.class.day.join(",");
            $http({
                method: "POST",
                url: "/class/register",
                data: data
            }).then(function successCallback(response) {
                $state.go('schools.class_list');
                toaster.pop('success', 'Class', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Class', error.data.message);
            });
        };
    }
]);

app.controller('listSchoolClassController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.addNewClass = function() {
            $state.go('schools.class_create');
        };

        $scope.inactiveClass = function(class_id) {
            var data = {
                id: class_id
            };
            $http({
                method: 'POST',
                url: '/class/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllSchoolClass();
            }, function errorCallback(error) {
                toaster.pop('error', 'Class', error.data.message);
            });
        };

        $scope.activeClass = function(class_id) {
            var data = {
                id: class_id
            };
            $http({
                method: 'POST',
                url: '/class/active',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllSchoolClass();
            }, function errorCallback(error) {
                toaster.pop('error', 'Class', error.data.message);
            });
        };

        $scope.deleteClass = function(class_id) {
            var data = {
                id: class_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/class/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllSchoolClass();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Class', error.data.message);
                });
            }
        };

        $scope.editClass = function(class_id) {
            $state.go('schools.class_edit', {
                class_id: class_id
            });
        };

        $scope.getAllSchoolClass = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/class/getAllSchoolClass',
                data: data
            }).then(function successCallback(response) {
                var classes = response.data;
                $scope.classes = classes;
            }, function errorCallback(error) {
                toaster.pop('error', 'Class', 'cannot get all the Class! try reloading page.');
            });
        };
    }
]);

app.controller('editSchoolClassController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.getSchoolDepartments = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/school/department/getAllSchoolDepartment',
                data: data
            }).then(function successCallback(response) {
                $scope.departments = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Departments', 'Something went wrong!');
            });
        };

        $scope.getSchoolStaffs = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/staff/getSchoolsAllStaff',
                data: data
            }).then(function successCallback(response) {
                $scope.staffs = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'School', 'Something went wrong!');
            });
        };

        $scope.getSchoolGrades = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/grade/getAllSchoolGrades',
                data: data
            }).then(function successCallback(response) {
                $scope.grades = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Grades', 'Something went wrong!');
            });
        };

        $scope.getSubjectGradeWise = function(grade_id) {
            var data = {
                grade_id: grade_id,
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/subject/getSchoolGradesSubjectWise',
                data: data
            }).then(function successCallback(response) {
                $scope.subjects = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Subjects', 'Something went wrong!');
            });
        };


        $scope.getLocations = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/location/getAllLocations',
                data: data
            }).then(function successCallback(response) {
                $scope.locations = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Locations', 'Something went wrong!');
            });
        };


        $scope.getEditClass = function() {
            $http({
                method: 'GET',
                url: '/class/edit?id=' + $state.params.class_id
            }).then(function successCallback(response) {
                $scope.getSchoolDepartments();
                $scope.getSchoolStaffs();
                $scope.getLocations(response.data.location_id);
                $scope.getSchoolGrades();
                $scope.getSubjectGradeWise(response.data.grade_id);
                $scope.class = response.data;
                if(response.data.day != null && response.data.day.length > 0) {
                    $scope.class.day = response.data.day.split(',');
                } else {
                    $scope.class.day = [];
                }    
            }, function errorCallback(error) {
                toaster.pop('error', 'Class', 'cannot get all the Class! try reloading page.');
            });
        };

        $scope.saveEditClass = function() {
            data = $scope.class;
            data.start_time = $("#starttime").val();
            data.end_time = $("#endtime").val();
            if($scope.class.day.length <= 0 ) {
                toaster.pop('error', "Day", "Please select at list one day");
                return false;
            }
            data.day = $scope.class.day.join(",");
            $http({
                method: 'POST',
                url: '/class/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Class', 'Saved Successfully.');
                $state.go('schools.class_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'Class', error.data.message);
            });
        };

        $scope.listClasses = function() {
            $state.go('schools.class_list');
        };
    }
]);
