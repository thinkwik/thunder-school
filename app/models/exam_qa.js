var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Exam_QA = db.define('exam_qa', {
    exam_id: {
        type: 'integer'
    },
    question: String,
    type: String,
    option_a: String,
    option_b: String,
    option_c: String,
    option_d: String,
    answer: String,
    max_mark: Number
}, {
    timestamp: true,
    methods: {}
});
