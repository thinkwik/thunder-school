function setToHappen(fn, d){
    var t = d.getTime() - (new Date()).getTime();
    return setTimeout(fn, t);
}
app.controller('staffDashboardController', ['$scope', '$http', '$state', 'toaster', '$cookieStore','$compile',
    function($scope, $http, $state, toaster, $cookieStore, $compile) {
        $scope.redirectToExamCreate = function(){
            $state.go('schoolstaff.exam_create');
        };
        $scope.totalStudentRedirect = function(){
            $state.go('schoolstaff.student_list');
        };
        $scope.mySchedulesRedirect = function(){
            $state.go('schoolstaff.list_myschedules');
        };
        $scope.pendingAppointmentRedirect = function(){
            $state.go('schoolstaff.appointment_list');
        };
        $scope.totalAssignmentsRedirect = function(){
            $state.go('schoolstaff.list_assignment');
        };
       $scope.getAllStaffDashboardData = function(){
            $scope.showAppointments = [];
            $scope.calAppointments = [];
            $scope.staff_total_students();
            $scope.staff_total_assignments();
            $scope.staff_total_schedules();
            $scope.staff_total_pending_appointments();
            $scope.staff_events_lists();
            $scope.staff_students_lists();
            $scope.staff_events_lists_calendar();
            $scope.staff_video_conferance_calendar();
            $scope.staff_appointments();            
        };
        $scope.staff_appointments = function() {
            var data ={
                staff_id:$cookieStore.get('loggedInStaff').id
            };
            $http({
                method: 'POST',
                url: '/staff/appointment/listAllAppointments',
                data:data
            }).then(function successCallback(response) {
                $scope.showAppointments = [];
                if(response.data.length) {
                    $scope.calAppointments = response.data;
                    Notification.requestPermission(function (permission) {
                      // If the user accepts, let's create a notification
                      if (permission === "granted") {
                        response.data.forEach(function(value) {
                            var mail_datetime = new Date(value.datetime);
                            tost_datetime = new Date(mail_datetime.getTime() - 15*60000);
                            var next_three_days = new Date();
                            next_three_days.setDate(next_three_days.getDate()+3);
                            if(mail_datetime >= new Date() && mail_datetime <= next_three_days) {
                                $scope.$apply(function() {
                                    $scope.showAppointments.push(value);
                                });
                                setToHappen(function () {
                                    var body = "At Time : " + value.datetime + "\n";
                                    body+= "Description : "+ value.description;
                                    var options = {
                                        icon:"/img/1472530977_tmp_Socrates_Logo.jpg",
                                        body: body,
                                        requireInteraction:true
                                    };
                                    var with_str = (value.parent_id == null)  ? value.student_name : value.parent_name;
                                    var n = new Notification('You have an appointment with ' + with_str ,options);
                                }, tost_datetime);
                            }
                        });
                      }
                    });
                }
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong! getting Dashobard data');
            });
        };
        $scope.appointArrived = function(appointment_id) {
            var data ={
                id:appointment_id
            };
            $http({
                method: 'POST',
                url: '/appointment/active',
                data:data
            }).then(function successCallback(response) {
                $scope.staff_appointments();
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong!');
            });
        };
        $scope.staff_total_students = function(){
        	var data ={
        		school_id:$cookieStore.get('loggedInStaff').school_id
        	};
            $http({
                method: 'POST',
                url: '/dashboard/staff/total_students',
                data:data
            }).then(function successCallback(response) {
              $scope.total_students = (response.data[0].total_students !== undefined) ? response.data[0].total_students : "0";
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong! getting Dashobard data');
            });
        };
        $scope.staff_total_schedules = function(){
            var data ={
                staff_id:$cookieStore.get('loggedInStaff').id
            };
            $http({
                method: 'POST',
                url: '/dashboard/staff/total_schedules',
                data:data
            }).then(function successCallback(response) {
              $scope.total_schedules = (response.data[0].total_schedules !== undefined) ? response.data[0].total_schedules : "0";
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong! getting Dashobard data');
            });
        };
        $scope.staff_total_assignments = function(){
            var data ={
                staff_id:$cookieStore.get('loggedInStaff').id
            };
            $http({
                method: 'POST',
                url: '/dashboard/staff/total_assignments',
                data:data
            }).then(function successCallback(response) {
              $scope.total_assignments = (response.data[0].total_assignments !== undefined) ? response.data[0].total_assignments : "0";
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong! getting Dashobard data');
            });
        };
         $scope.staff_total_pending_appointments = function(){
            var data ={
                staff_id:$cookieStore.get('loggedInStaff').id
            };
            $http({
                method: 'POST',
                url: '/dashboard/staff/total_pending_appointments',
                data:data
            }).then(function successCallback(response) {
              $scope.total_pending_appointments = (response.data[0].total_pending_appointments !== undefined) ? response.data[0].total_pending_appointments : "0";
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong! getting Dashobard data');
            });
        };
        $scope.staff_events_lists = function(){
        	var data ={
        		school_id:$cookieStore.get('loggedInStaff').school_id
        	};
            $http({
                method: 'POST',
                url: '/dashboard/staff/events_lists',
                data:data
            }).then(function successCallback(response) {
               $scope.events_lists = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong! getting Dashobard data');
            });
        };
        $scope.staff_students_lists = function(){
        	var data ={
        		school_id:$cookieStore.get('loggedInStaff').school_id
        	};
            $http({
                method: 'POST',
                url: '/dashboard/staff/students_lists',
                data:data
            }).then(function successCallback(response) {
               $scope.students_lists = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong! getting Dashobard data');
            });
        };
        $scope.studentCollapsed = [{name: 'one',isCollapsed: true}];


        //calendar options and config

        $scope.uiConfig = {
          calendar:{
            height: 450,
            editable: false,
            header:{
              //left: 'month basicWeek basicDay agendaWeek agendaDay',
              left: 'month basicWeek',
              center: 'title',
              right: 'today prev,next'
            },
            eventClick: $scope.alertEventOnClick,
            eventDrop: $scope.alertOnDrop,
            eventResize: $scope.alertOnResize,
            eventRender: $scope.eventRender
          }
        };

        $scope.events = [];
        $scope.eventSources = [$scope.events];
        
        $scope.staff_events_lists_calendar = function(){
            var data ={
                school_id:$cookieStore.get('loggedInStaff').school_id
            };
            $http({
                method: 'POST',
                url: '/dashboard/staff/events_lists/calendar',
                data:data
            }).then(function successCallback(response) {
               angular.forEach(response.data,function(value, key){
                $scope.events.push({
                    title: value.title,
                    description: (typeof value.content != 'undefined' ) ? value.content.substring(17, 125).replace("</div>", "")+"..." : "No Description",
                    start: value.start_date,
                    end: value.end_date,
                    stick:true
                });
               });
               $scope.add_my_todos();
               $scope.add_appointments();
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong! getting Dashobard data');
            });
        };

        $scope.eventRender = function( event, element, view ) { 
            element.attr({
                'data-toggle' : 'tooltip',
                'title' : event.description
            });
            element.tooltip();
            $compile(element)($scope);
        };

        $scope.todo_lists = [];
        $scope.add_my_todos = function() {
            var data = {};
            data.user_id = $cookieStore.get('loggedInStaff').id;
            data.type = "staff";
            $http({
                method: 'POST',
                url: '/todo/getMyTodos',
                data:data
            }).then(function successCallback(response) {
                $scope.todo_lists = response.data;
               angular.forEach(response.data,function(value, key){
                $scope.events.push({
                    title: value.description,
                    description: 'To-Do Task',
                    start: new Date(value.date),
                    allDay: true,
                    color: (value.status == "1") ? "#27c24c" : "#ee3939",
                    stick:true
                });
               });
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong! getting Dashobard data');
            });
        };

        $scope.add_appointments = function() {
            setTimeout(function() {
                if($scope.calAppointments.length){
                    angular.forEach($scope.calAppointments,function(value, key){
                        var status = "Pending";
                        var color = "#ee3939";
                        if(value.status == "1") {
                            status = "Accepted";
                            color = "#27c24c";
                        } else if (value.status == "2") {
                            status = "Declined";
                            color = "#ee3939";
                        } else if (value.status == "3") {
                            status = "Rescheduled";
                            color = "#23b7e5";
                        } else if (value.status == "4") {
                            status = "No Show";
                            color = "#ee3939";
                        } else if (value.status == "5") {
                            status = "Completed";
                            color = "#27c24c";
                        }
                        var with_str = (value.parent_id == null)  ? value.student_name : value.parent_name;
                        $scope.$apply(function() {
                            $scope.events.push({
                                title: "Appointment - " + status,
                                description: "Appointment with " + with_str+ ' : ' +value.description,
                                start: new Date(value.datetime),
                                allDay: true,
                                color: color,
                                stick:true
                            });
                        });                        
                    });
                }                
            }, 1500);            
        };

        $scope.staff_video_conferance_calendar = function(){
            var data = {
                unique_id : $cookieStore.get('loggedInStaff').unique_id
            };
            $http({
                method: 'POST',
                url: '/video/myConferanceList',
                data:data
            }).then(function successCallback(response) {
               angular.forEach(response.data,function(value, key){
                $scope.events.push({
                    title: value.room_name,
                    description: 'Video Conferance Invitation',
                    start: value.scheduled_date,
                    allDay: true,
                    color: "#5b6f60",
                    stick:true
                });
               });
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong! getting Dashobard data');
            });
        };
    }
]);
