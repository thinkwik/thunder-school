var db = require('orm').db,
    Medical_info = db.models.medical_info;

exports.register = function(req, res) {
    if (typeof req.body.school_id == 'undefined' ||
        typeof req.body.type == 'undefined' ||
        typeof req.body.name == 'undefined'
    ) {
        return sendError(req, res, 422, 'Please check your parameters.');
    }
    var m_info = new Medical_info();
    m_info.school_id = req.body.school_id;
    m_info.type = req.body.type;
    m_info.name = req.body.name;
    m_info.save(function(err, created_info) {
        if (err) {
            console.log(err);
            return sendError(req, res, 500, 'something went wrong.');
        }
        return res.send(created_info);
    });
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'Medical Info id required!!');
    }
    Medical_info.find({
        id: req.body.id
    }, 1, function(err, infos) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, 'Please check your parameters!!');
        }
        infos[0].school_id = req.body.school_id;
        infos[0].name = req.body.name;
        infos[0].type = req.body.type;
        infos[0].save(function(err, updated_info) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(updated_info);
        });
    });
};


exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'id required!!');
    }
    Medical_info.find({
        id: req.body.id
    }, 1, function(err, infos) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        infos[0].remove(function(err) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(infos[0]);
        });
    });
};



exports.getMedicalInfo = function(req, res) {
    if (typeof req.query.id == 'undefined') {
        return sendError(req, res, 401, "ID required field");
    }
    Medical_info.find({
        id: req.query.id
    }, 1, function(err, infos) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        if (!infos.length) return sendError(req, res, 404, "INfo not found");
        return res.send(infos[0]);
    });
};

exports.getAllMedicalInfos = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id === null) {
        return sendError(req, res, 422, "School ID required");
    }
    var query_str = `SELECT t1.*,t2.school_name
                    FROM medical_info as t1
                    LEFT JOIN schools as t2 ON t1.school_id = t2.id 
                    WHERE t1.school_id = ? GROUP BY t1.id`;
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};