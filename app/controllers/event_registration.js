var bCrypt = require('bcrypt-nodejs');
var db = require('orm').db,
    EventRegistration = db.models.event_registrations;
    parentEventregistration = db.models.parent_event_registrations;
    studentEventregistration = db.models.student_event_registrations;

exports.register = function(req, res) {
    if (typeof req.body.event_id == 'undefined') {
        return sendError(req, res, 422, 'required parameter not fullfiled for register new resource');
    }
    var new_reg = new EventRegistration();
    new_reg.reg_name = req.body.reg_name;
    new_reg.email = req.body.email;
    new_reg.mobile = req.body.mobile;
    new_reg.message = req.body.message;
    new_reg.event_id = (typeof req.body.event_id != 'undefined') ? req.body.event_id : "1";
    new_reg.status = (typeof req.body.status != 'undefined') ? req.body.status : "1";
    new_reg.save(function(err, registration) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        res.send(registration);
    });
};

exports.studentRegistration = function(req, res) {
    var newStudentRegistration = new studentEventregistration();
    newStudentRegistration.school_id = req.body.school_id;
    newStudentRegistration.student_id = req.body.student_id;
    newStudentRegistration.event_id = req.body.event_id;
    newStudentRegistration.student_status = (typeof req.body.student_status != 'undefined') ? req.body.student_status : "1";
    newStudentRegistration.save(function(err, registration) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        res.send(registration);
    });
};


exports.parentRegistration = function(req, res) {
    var newParentRegistration = new parentEventregistration();
    newParentRegistration.school_id = req.body.school_id;
    newParentRegistration.parent_id = req.body.parent_id;
    newParentRegistration.event_id = req.body.event_id;
    newParentRegistration.parent_status = (typeof req.body.parent_status != 'undefined') ? req.body.parent_status : "1";
    newParentRegistration.save(function(err, registration) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        res.send(registration);
    });
};

exports.getEventRegistration = function(req, res) {
    if ((typeof req.query.id == 'undefined')) {
        return sendError(req, res, 422, "required parameter not fullfilled");
    }
    EventRegistration.find({
        id: req.query.id
    }, 1, function(err, registrations) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!registrations.length) {
            return sendError(req, res, 404, "registration not found with perticular id");
        }
        return res.send(registrations[0]);
    });
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'registration id required');
    }
    EventRegistration.find({
        id: req.body.id
    }, 1, function(err, registrations) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (registrations.length === 0) {
            return sendError(req, res, 404, "registration not found");
        }
        registrations[0].event_id = (typeof req.body.event_id != 'undefined') ? req.body.event_id : registrations[0].event_id;
        registrations[0].reg_name = req.body.reg_name;
        registrations[0].email = req.body.email;
        registrations[0].mobile = req.body.mobile;
        registrations[0].message = req.body.message;
        registrations[0].status = (typeof req.body.status != 'undefined') ? req.body.status : registrations[0].status;
        registrations[0].save(function(err, updated_registration) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            return res.send(updated_registration);
        });
    });
};

exports.active = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'registration id required');
    }
    EventRegistration.find({
        id: req.body.id
    }, 1, function(err, registrations) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        registrations[0].status = "1";
        registrations[0].save(function(err, activated_registration) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "something went wrong in activating registration");
            }
            return res.send(activated_registration);
        });
    });
};

exports.inactive = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'registration id required');
    }
    EventRegistration.find({
        id: req.body.id
    }, 1, function(err, registrations) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        registrations[0].status = "0";
        registrations[0].save(function(err, activated_registration) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "something went wrong in inactivating registration");
            }
            return res.send(activated_registration);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'registration id required');
    }
    EventRegistration.find({
        id: req.body.id
    }, 1, function(err, registrations) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!registrations.length) {
            return sendError(req, res, 404, "Registration not found");
        }
        registrations[0].remove(function(err) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "Something went wrong in deleting registration");
            }
            return res.send(registrations[0]);
        });
    });
};

exports.deleteStudent = function(req, res) {
    if (req.body.student_id && req.body.school_id && req.body.reg_name && req.body.email && req.body.mobile) {
        var query_str = "DELETE FROM `event_registrations` WHERE student_id = ? AND school_id =? AND  reg_name=? AND email = ? AND mobile = ?";
        db.driver.execQuery(query_str, [req.body.student_id, req.body.school_id, req.body.reg_name, req.body.email, req.body.mobile], function(err, rows) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, "Bad Request check your parameters");
            }
            return sendError(req, res, 200, "Removed successfully!");
        });
    } else {
        return sendError(req, res, 400, "Bad Request check your parameters");
    }
};

exports.getAllEventRegistrations = function(req, res) {
    var query_str = "SELECT t1.*,t2.title as event_name  FROM `event_registrations` as t1 LEFT JOIN events as t2 ON t1.event_id = t2.id  WHERE ?";
    db.driver.execQuery(query_str, ['1'], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getAllEventDistrictRegistrations = function(req, res) {
    if (typeof req.body.district_id == 'undefined' || req.body.district_id === null) {
        return sendError(req, res, 422, "district_id required");
    }
    var query_str = "SELECT t1.*, t2.title as event_name  FROM `event_registrations` as t1 LEFT JOIN events as t2 ON t1.event_id = t2.id LEFT JOIN schools as t3 ON t2.school_id = t3.id WHERE t3.parent_id = ?";
    db.driver.execQuery(query_str, [req.body.district_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getAllEventSchoolRegistrations = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id === null) {
        return sendError(req, res, 422, "district_id required");
    }
    var query_str = "SELECT t1.*, t2.title as event_name  FROM `event_registrations` as t1 LEFT JOIN events as t2 ON t1.event_id = t2.id LEFT JOIN schools as t3 ON t2.school_id = t3.id WHERE t3.id = ?";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};




exports.listAllEventSchoolRegistrations = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id === null) {
        return sendError(req, res, 422, "district_id required");
    }
    var query_str =`SELECT t1.*, t2.title as event_name,t4.school_name ,t5.first_name,t5.last_name, t6.full_name,t3.parent_status FROM
    student_event_registrations as t1
    LEFT JOIN admissions as t5 ON t1.student_id = t5.id
    LEFT JOIN events as t2 ON t1.event_id = t2.id
    LEFT JOIN parent_event_registrations as t3 ON t1.event_id = t3.event_id
    LEFT JOIN parents as t6 ON t3.parent_id = t6.id
    LEFT JOIN schools as t4 ON t2.school_id = t4.id
    WHERE t4.id = ?`;
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
