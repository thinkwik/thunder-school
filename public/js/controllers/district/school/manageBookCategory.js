app.controller('createBookCategoryController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore',
    function($scope, $http, $state, toaster, Upload, $cookieStore) {
      $scope.listBookCategory = function(){
        $state.go('schools.list_book_category');
      };
      $scope.addBookCategory = function() {
          data = $scope.book;
          $http({
              method: 'POST',
              url: '/book_category/register',
              data: data
          }).then(function successCallback(response) {
              $state.go('schools.list_book_category');
              toaster.pop('success', "Book Category", "Category added successfully!");
          }, function errorCallback(error) {
              toaster.pop('error', "Book Category", errror.data.message);
          });
      };
    }
]);

app.controller('listBookCategoryController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore', '$window',
    function($scope, $http, $state, toaster, Upload, $cookieStore, $window) {
      $scope.addNewBookCategory = function() {
        $state.go('schools.create_book_category');
      };
      $scope.deleteBookCategory = function(c_id) {
        data = {
            id: c_id
          };
          $http({
              method: 'POST',
              url: '/book_category/delete',
              data: data
          }).then(function successCallback(response) {
              $scope.getAllSchoolBookCategory();
          }, function errorCallback(error) {
              toaster.pop('error', "Book Category", "something went wrong");
          });
      }
      $scope.getAllSchoolBookCategory = function() {
          data = {
            asd: "asd"
          };
          $http({
              method: 'POST',
              url: '/book_category/getAllSchoolBookCategory',
              data: data
          }).then(function successCallback(response) {
              $scope.categories = response.data;
          }, function errorCallback(error) {
              toaster.pop('error', "Book Category", "something went wrong");
          });
      };
    }
]);