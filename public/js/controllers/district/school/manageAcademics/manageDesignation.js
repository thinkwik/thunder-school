app.controller('createSchoolDesignationController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.addDesignation = function() {
            $state.go('schools.designation_create');
        };

        $scope.canceldesignationCreate = function() {
            $state.go('schools.designation_list');
        };

        $scope.createdesignation = function() {
            data = $scope.designation;
            data.school_id = $cookieStore.get('loggedInSchool').id;
            $http({
                method: "POST",
                url: "/designation/register",
                data: data
            }).then(function successCallback(response) {
                $scope.canceldesignationCreate();
                toaster.pop('success', 'Designation', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Designation', error.data.message);
            });
        };
    }
]);

app.controller('listSchoolDesignationController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.addNewDesignation = function() {
            $state.go('schools.designation_create');
        };

        $scope.inactiveDesignation = function(designation_id) {
            var data = {
                id: designation_id
            };
            $http({
                method: 'POST',
                url: '/designation/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllSchoolDesignations();
            }, function errorCallback(error) {
                toaster.pop('error', 'Designation', error.data.message);
            });
        };

        $scope.activeDesignation = function(designation_id) {
            var data = {
                id: designation_id
            };
            $http({
                method: 'POST',
                url: '/designation/active',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllSchoolDesignations();
            }, function errorCallback(error) {
                toaster.pop('error', 'Designation', error.data.message);
            });
        };

        $scope.deleteDesignation = function(designation_id) {
            var data = {
                id: designation_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/designation/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllSchoolDesignations();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Designation', error.data.message);
                });
            }
        };

        $scope.getAllSchoolDesignations = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/designation/getAllSchoolDesignations',
                data: data
            }).then(function successCallback(response) {
                var designations = response.data;
                $scope.designations = designations;
            }, function errorCallback(error) {
                toaster.pop('error', 'Designations', 'cannot get all the Designations! try reloading page.');
            });
        };


        $scope.editDesignation = function(designation_id) {
            $state.go('schools.designation_edit', {
                designation_id: designation_id
            });
        };
    }
]);

app.controller('editSchoolDesignationController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.getEditDesignation = function() {
            $http({
                method: 'GET',
                url: '/designation/edit?id=' + $state.params.designation_id
            }).then(function successCallback(response) {
                $scope.designation = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Designation', 'cannot get all the Designation! try reloading page.');
            });
        };

        $scope.saveEditDesignation = function() {
            data = $scope.designation;
            $http({
                method: 'POST',
                url: '/designation/edit',
                data: data
            }).then(function successCallback(response) {
                $state.go('schools.designation_list');
                toaster.pop('success', 'Designation', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Designation', error.data.message);
            });
        };

        $scope.cancelEditDesignationForm = function() {
            $state.go('schools.designation_list');
        };
    }
]);
