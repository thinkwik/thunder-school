app.controller('createSchoolProductController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        $scope.categories = [];
        if (typeof $cookieStore.get('loggedInSchool').id != 'undefined') {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/category/getAllSchoolCategories',
                data: data
            }).then(function successCallback(response) {
                $scope.categories = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Product', 'Something went wrong!');
            });
        } else {
            toaster.pop('error', 'Login', 'something went wrong! Please try re-login');
        }

        $scope.addProduct = function() {
            $state.go('schools.product_create');
        };

        $scope.cancelProductForm = function() {
            $state.go('schools.product_list');
        };

        $scope.createProduct = function() {
            data = $scope.product;
            data.product_image = $scope.uploadedImageName;
            console.log(data);
            $http({
                method: "POST",
                url: "/product/register",
                data: data
            }).then(function successCallback(response) {
                $state.go('schools.product_list');
                toaster.pop('success', 'Product', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Product', error.data.message);
            });
        };

        $scope.uploadFile = function(file) {
            if (!file) return console.log("No file selected !");
            Loading(true);
            Upload.upload({
                url: '/product/upload',
                data: {
                    file: file
                }
            }).then(function successCallback(response) {
                Loading(false);
                $scope.uploadedImageName = response.data;
            }, function errorCallback(error) {
                Loading(false);
                toaster.pop('error', 'Product', error.data.message);
            });
        };
    }
]);

app.controller('listSchoolProductController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {

        $scope.addNewProduct = function() {
            $state.go('schools.product_create');
        };


        $scope.inactiveProduct = function(product_id) {
            console.log(product_id);
            var data = {
                id: product_id
            };
            $http({
                method: 'POST',
                url: '/product/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllSchoolProducts();
            }, function errorCallback(error) {
                toaster.pop('error', 'Products', error.data.message);
            });
        };

        $scope.activeProduct = function(product_id) {
            console.log(product_id);
            var data = {
                id: product_id
            };
            $http({
                method: 'POST',
                url: '/product/active',
                data: data
            }).then(function successCallback(response) {
                console.log(response);
                $scope.getAllSchoolProducts();
            }, function errorCallback(error) {
                toaster.pop('error', 'Products', error.data.message);
            });
        };

        $scope.deleteProduct = function(product_id) {
            var data = {
                id: product_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/product/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllSchoolProducts();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Products', error.data.message);
                });
            }
        };

        $scope.getAllSchoolProducts = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/product/getAllSchoolProducts',
                data: data
            }).then(function successCallback(response) {
                var products = response.data;
                $scope.products = products;
            }, function errorCallback(error) {
                toaster.pop('error', 'Products', 'cannot get all the Products! try reloading page.');
            });
        };

        $scope.editProduct = function(product_id) {
            $state.go('schools.product_edit', {
                product_id: product_id
            });
        };
    }
]);

app.controller('editSchoolProductController', ['$scope', '$http', '$state', 'toaster', '$cookieStore','Upload',
    function($scope, $http, $state, toaster, $cookieStore,Upload) {
        $scope.categories = [];
        if (typeof $cookieStore.get('loggedInSchool').id != 'undefined') {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/category/getAllSchoolCategories',
                data: data
            }).then(function successCallback(response) {
                $scope.categories = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Product', 'Something went wrong!');
            });
        } else {
            toaster.pop('error', 'Login', 'something went wrong! Please try re-login');
        }

        $scope.getEditProduct = function() {
            console.log($state.params.product_id);
            $http({
                method: 'GET',
                url: '/product/edit?id=' + $state.params.product_id,
            }).then(function successCallback(response) {
                console.log(response);
                $scope.product = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Product', 'cannot get all the Product! try reloading page.');
            });
        };

        $scope.uploadFile = function(file) {
            if (!file) return console.log("No file selected !");
            Loading(true);
            Upload.upload({
                url: '/product/upload',
                data: {
                    file: file
                }
            }).then(function successCallback(response) {
                Loading(false);
                $scope.uploadedImageName = response.data;
            }, function errorCallback(error) {
                Loading(false);
                toaster.pop('error', 'Product', error.data.message);
            });
        };

        $scope.saveEditProduct = function() {
            data = $scope.product;
            if ($scope.uploadedImageName) {
              data.product_image = $scope.uploadedImageName;
            }
            $http({
                method: 'POST',
                url: '/product/edit',
                data: data
            }).then(function successCallback(response) {
                $state.go('schools.product_list');
                toaster.pop('success', 'Product', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Product', error.data.message);
            });
        };

        $scope.cancelProductForm = function() {
            $state.go('schools.product_list');
        };
    }
]);
