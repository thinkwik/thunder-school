app.controller('createDistrictGradesController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.getSchools = null;
        $scope.SchoolList = [];
        var district = $cookieStore.get('loggedInDistrict');
        if (typeof district != 'undefined') {
            var data = {
                district_id: district.id
            };
            $http({
                method: 'POST',
                url: '/school/getDistrictSchools',
                data: data
            }).then(function successCallback(response) {
                $scope.SchoolList = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'District', 'Something went wrong!');
            });
        } else {
            toaster.pop('error', 'Login', 'something went wrong! Please try re-login');
        }

        $scope.addGrade = function() {
            $state.go('districts.grades_create');
        };

        $scope.cancelGradeCreate = function() {
            $state.go('districts.grades_list');
        };

        $scope.createGrade = function() {
            data = $scope.grade;
            $http({
                method: "POST",
                url: "/grade/register",
                data: data
            }).then(function successCallback(response) {
                $scope.cancelGradeCreate();
                toaster.pop('success', 'Grade', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Grade', error.data.message);
            });
        };

        $scope.cancelGradeForm = function() {
            $state.go('districts.grades_list');
        };
    }
]);

app.controller('listDistrictGradesController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.addNewGrade = function() {
            $state.go('districts.grades_create');
        };

        $scope.inactiveGrades = function(grade_id) {
            var data = {
                id: grade_id
            };
            $http({
                method: 'POST',
                url: '/grade/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllDistrictGrades();
            }, function errorCallback(error) {
                toaster.pop('error', 'Grade', error.data.message);
            });
        };

        $scope.activeGrades = function(grade_id) {
            var data = {
                id: grade_id
            };
            $http({
                method: 'POST',
                url: '/grade/active',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllDistrictGrades();
            }, function errorCallback(error) {
                toaster.pop('error', 'Grade', error.data.message);
            });
        };

        $scope.deleteGrades = function(grade_id) {
            var data = {
                id: grade_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/grade/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllDistrictGrades();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Grade', error.data.message);
                });
            }
        };

        $scope.getAllDistrictGrades = function() {
            var data = {
                district_id: $cookieStore.get('loggedInDistrict').id
            };
            $http({
                method: 'POST',
                url: '/grade/getAllDistrictGrades',
                data: data
            }).then(function successCallback(response) {
                var grades = response.data;
                $scope.grades = grades;
            }, function errorCallback(error) {
                toaster.pop('error', 'Grades', 'cannot get all the Grades! try reloading page.');
                console.log(error.data.message);
            });
        };


        $scope.editGrades = function(grade_id) {
            $state.go('districts.grades_edit', {
                grade_id: grade_id
            });
        };
    }
]);

app.controller('editDistrictGradesController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.getSchools = null;
        $scope.SchoolList = [];
        var district = $cookieStore.get('loggedInDistrict');
        if (typeof district != 'undefined') {
            var data = {
                district_id: district.id
            };
            $http({
                method: 'POST',
                url: '/school/getDistrictSchools',
                data: data
            }).then(function successCallback(response) {
                $scope.SchoolList = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'District', 'Something went wrong!');
            });
        } else {
            toaster.pop('error', 'Login', 'something went wrong! Please try re-login');
        }

        $scope.getEditGrade = function() {
            $http({
                method: 'GET',
                url: '/grade/edit?id=' + $state.params.grade_id
            }).then(function successCallback(response) {
                $scope.grade = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Grade', 'cannot get all the Grades! try reloading page.');
            });
        };

        $scope.saveEditGrade = function() {
            data = $scope.grade;
            $http({
                method: 'POST',
                url: '/grade/edit',
                data: data
            }).then(function successCallback(response) {
                $state.go('districts.grades_list');
                toaster.pop('success', 'Grade', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Grade', error.data.message);
            });
        };

        $scope.cancelGradesList = function() {
            $state.go('districts.grades_list');
        };
    }
]);
