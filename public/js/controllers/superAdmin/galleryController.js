 app.controller('createAllSchoolGalleryController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore',
     function($scope, $http, $state, toaster, Upload, $cookieStore) {
         $scope.createGallery = function() {
             data = $scope.gallery;
             $http({
                     url: '/gallery/register',
                     method: "POST",
                     data: data
                 })
                 .then(function(response) {
                         $state.go('users.gallery_list');
                         toaster.pop('success', "Gallery", "Gallery created successfully!!!");
                     },
                     function(response) {
                         toaster.pop('error', "Error", "Something went wrong!!!");
                     });
         };

         $scope.getAllEventList = function() {
             var data = {
                 district_id: $cookieStore.get('loggedInDistrict').id
             };
             $http({
                 method: 'POST',
                 url: '/event/getAllDistrictEvents',
                 data: data
             }).then(function successCallback(response) {
                 $scope.events = response.data;
             }, function errorCallback(error) {
                 toaster.pop('error', 'Events', error.message);
             });
         };


         $scope.getAllSchoolList = function() {
             $http({
                 method: 'POST',
                 url: '/school/getAllSchools'
             }).then(function successCallback(response) {
                 $scope.schools = response.data;
                 $scope.getAllEventList();
             }, function errorCallback(error) {
                 toaster.pop('error', 'District', 'cannot get all the schools! try reloading page.');
             });
         };

         $scope.cancelGallery = function() {
             $state.go('users.gallery_list');
         };
     }
 ]);

 app.controller('listAllSchoolGalleryController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore',
     function($scope, $http, $state, toaster, Upload, $cookieStore) {
         $scope.listGallery = function() {
             var data = {
                 district_id: $cookieStore.get('loggedInDistrict').id
             };
             $http({
                 method: 'POST',
                 url: '/gallery/getAllDistrictEventsGalleries',
                 data: data
             }).then(function successCallback(response) {
                 var galleries = response.data;
                 $scope.galleries = galleries;
             }, function errorCallback(error) {
                 toaster.pop('error', 'Galleries', 'cannot get all the galleries! try reloading page.');
                 console.log(error.data.message);
             });
         };

         $scope.activeGallery = function(gallery_id) {
             var data = {
                 id: gallery_id
             };
             $http({
                 method: 'POST',
                 url: '/gallery/active',
                 data: data
             }).then(function successCallback(response) {
                 $scope.listGallery();
             }, function errorCallback(error) {
                 console.log(error.data.message);
             });
         };

         $scope.deleteGallery = function(gallery_id) {
             var data = {
                 id: gallery_id
             };
             var result = confirm("Please confirm, Do you really want to delete ?");
             if (result) {
                 $http({
                     method: 'POST',
                     url: '/gallery/delete',
                     data: data
                 }).then(function successCallback(response) {
                     $scope.listGallery();
                 }, function errorCallback(error) {
                     console.log(error.data.message);
                 });
             }
         };

         $scope.inactiveGallery = function(gallery_id) {
             var data = {
                 id: gallery_id
             };
             $http({
                 method: 'POST',
                 url: '/gallery/inactive',
                 data: data
             }).then(function successCallback(response) {
                 $scope.listGallery();
             }, function errorCallback(error) {
                 console.log(error.data.message);
             });
         };

         $scope.editGallery = function(gallery_id) {
             $state.go('users.gallery_edit', {
                 gallery_id: gallery_id
             });
         };


         $scope.getEditGallery = function() {
             var data = $scope.gallery;
             $http({
                 method: 'GET',
                 url: '/gallery/edit?id=' + $state.params.gallery_id,
                 data: data
             }).then(function successCallback(response) {
                 $scope.gallery = response.data;
             }, function errorCallback(error) {
                 toaster.pop('error', 'Gallery', error.data.message);
             });
         };

         $scope.addNewGallery = function() {
             $state.go('users.gallery_create');
         };

         $scope.addItem = function(gallery_id) {
             $state.go('users.image_add', {
                 gallery_id: gallery_id
             });
         };

         $scope.viewAlbum = function(gallery_id) {
             $state.go('users.gallery_view', {
                 gallery_id: gallery_id
             });
         };
     }
 ]);

 app.controller('editAllSchoolGalleryController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore',
     function($scope, $http, $state, toaster, Upload, $cookieStore) {
         $scope.cancelGallery = function() {
             $state.go('users.gallery_list');
         };

         $scope.saveEditGallery = function() {
             var data = $scope.gallery;
             $http({
                 method: 'POST',
                 url: '/gallery/edit',
                 data: data
             }).then(function successCallback(response) {
                 $state.go('users.gallery_list');
                 toaster.pop('success', 'Gallery', 'Updated Successfully.');
             }, function errorCallback(error) {
                 toaster.pop('error', 'Gallery', error.data.message);
             });
         };

         $scope.getDistrictSchools = function() {
             var data = {
                 district_id: $cookieStore.get('loggedInDistrict').id
             };
             $http({
                 method: 'POST',
                 url: '/school/getDistrictSchools',
                 data: data
             }).then(function successCallback(response) {
                 $scope.schools = response.data;
                 $scope.getAllEventList();
             }, function errorCallback(error) {
                 toaster.pop('error', 'District', 'Something went wrong!');
             });
         };

         $scope.getAllEventList = function() {
             var data = {
                 district_id: $cookieStore.get('loggedInDistrict').id
             };
             $http({
                 method: 'POST',
                 url: '/event/getAllDistrictEvents',
                 data: data
             }).then(function successCallback(response) {
                 $scope.events = response.data;
                 $scope.getEditGallery();
             }, function errorCallback(error) {
                 toaster.pop('error', 'Events', error.message);
             });
         };

         $scope.getEditGallery = function() {
             $http({
                 method: 'GET',
                 url: '/gallery/edit?id=' + $state.params.gallery_id
             }).then(function successCallback(response) {
                 $scope.gallery = response.data;
             }, function errorCallback(error) {
                 toaster.pop('error', 'Gallery', error.data.message);
             });
         };
     }
 ]);

 app.controller('viewAllSchoolGalleryAlbumController', ['$scope', '$http', '$state', 'toaster', 'Upload',
     function($scope, $http, $state, toaster, Upload) {
         $scope.uploadFile = function(file) {
             if (!file) return console.log("No file selected !");
             Upload.upload({
                 url: '/gallery/upload',
                 data: {
                     file: file
                 }
             }).then(function(success) {
                 $scope.filename = success.data;
                 var data = {
                     gallery_id: parseInt($state.params.gallery_id),
                     image_name: $scope.filename
                 };
                 $http({
                         url: '/gallery/addImage',
                         method: "POST",
                         data: data
                     })
                     .then(function(response) {

                             toaster.pop('success', "Gallery", "Gallery updated successfully!!!");
                             $scope.viewAlbum();
                         },
                         function(response) {
                             toaster.pop('error', "Error", "Something went wrong!!!");
                         });
             });
         };
      
         $scope.viewAlbum = function() {
             var data = {
                 gallery_id: $state.params.gallery_id
             };
             $http({
                 method: 'POST',
                 url: '/gallery/viewAlbum',
                 data: data
             }).then(function successCallback(response) {
                 var galleryImages = response.data;
                 $scope.galleryImages = galleryImages;
             }, function errorCallback(error) {
                 toaster.pop('error', 'Gallery', error.data.message);
             });
         };

         $scope.deleteImage = function(image_id) {
             var data = {
                 id: image_id
             };
             var result = confirm("Please confirm, Do you really want to delete ?");
             if (result) {
                 $http({
                     method: 'POST',
                     url: '/gallery/Album/deleteImage',
                     data: data
                 }).then(function successCallback(response) {
                     $scope.viewAlbum();
                     toaster.pop('success', "Gallery", "Gallery updated successfully!!!");
                 }, function errorCallback(error) {
                     toaster.pop('error', 'Gallery', error.data.message);
                 });
             }
         };
     }
 ]);
