var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var ChatRoom = db.define('chat_rooms', {
    school_id: {
        type: 'integer'
    },
    creater_id:String,
    room_id:String,
    room_name:String,
    scheduled_date:Date,
    status: ['0', '1']
}, {
    timestamp: true,
    methods: {

    }
});
