app.controller('forgotPasswordController', ['$scope', '$translate', '$localStorage', '$window', '$http', '$state','toaster',
    function($scope, $translate, $localStorage, $window, $http, $state,toaster) {
        $scope.forgotPassword = function() {
            $scope.authError = null;
            $scope.authSuccess = null;
            var data = $scope.fgtpwd;
            $http({
                url: '/forgot_password',
                method: 'POST',
                data: data
            }).then(function successCallback(response) {
                console.log(response);
                toaster.pop('success', 'Forgot Password', "Email sent Successfully");
                $scope.authError = null;
                $scope.authSuccess = "Email sent Successfully";
            }, function errorCallback(error) {
                console.log(error);
                toaster.pop('error', 'Forgot Password', "Please check your parameters");
                $scope.authSuccess = null;
                $scope.authError = "something went wrong please try reloading page!";
            })
        };
    }
]);
app.controller('resetPasswordController', ['$scope', '$translate', '$localStorage', '$window', '$http', '$state', '$location','toaster',
    function($scope, $translate, $localStorage, $window, $http, $state, $location,toaster) {
        $scope.authError = null;
        $scope.authSuccess = null;
        var obj = $location.search();
        $scope.token = obj.token;
        $scope.email = obj.email;
        $scope.type = obj.type;
        $scope.resetPassword = function() {
            var data = $scope.resetpwd;
            data.token = $scope.token;
            data.email = $scope.email;
            data.type = $scope.type;
            if ($scope.resetpwd.new_password != $scope.resetpwd.confirm_new_password) {
              $scope.authSuccess = null;
              $scope.authError = "Both password must be same!";
              return false;
            }
            $http({
                url: '/reset_password',
                method: 'POST',
                data: data
            }).then(function successCallback(response) {
                $scope.authError =null;
                console.log(response);
                toaster.pop('success', 'Forgot Password', "Your password updated successfully!");
                $scope.authSuccess = "Your password updated successfully!";
            }, function errorCallback(error) {
                $scope.authSuccess = null;
                console.log(error);
                toaster.pop('error', 'Forgot Password', "seems like link has been expired!");
                $scope.authError = "seems like link has been expired!";
            })
        };
    }
]);
