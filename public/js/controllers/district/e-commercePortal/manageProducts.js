app.controller('createDistrictProductController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        if (typeof $cookieStore.get('loggedInDistrict').id != 'undefined') {
            var data = {
                district_id: $cookieStore.get('loggedInDistrict').id
            };
            $http({
                method: 'POST',
                url: '/category/getAllDistCategories',
                data: data
            }).then(function successCallback(response) {
                $scope.categories = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Product', 'Something went wrong!');
            });
        } else {
            toaster.pop('error', 'Login', 'something went wrong! Please try re-login');
        }

        $scope.addProduct = function() {
            $state.go('districts.product_create');
        };

        $scope.cancelProductForm = function() {
            $state.go('districts.product_list');
        };

        $scope.createProduct = function() {
            data = $scope.product;
            data.product_image = $scope.uploadedImageName;
            $http({
                method: "POST",
                url: "/product/register",
                data: data
            }).then(function successCallback(response) {
                $state.go('districts.product_list');
                toaster.pop('success', 'Product', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Product', error.data.message);
            });
        };

        $scope.uploadFile = function(file) {
            if (!file) return console.log("No file selected !");
            Loading(true);
            Upload.upload({
                url: '/product/upload',
                data: {
                    file: file
                }
            }).then(function successCallback(response) {
                Loading(false);
                $scope.uploadedImageName = response.data;
            }, function errorCallback(error) {
                Loading(false);
                toaster.pop('error', 'Product', error.data.message);
            });
        };
    }
]);

app.controller('listDistrictProductController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {

        $scope.addNewProduct = function() {
            $state.go('districts.product_create');
        };


        $scope.inactiveProduct = function(product_id) {
            var data = {
                id: product_id
            };
            $http({
                method: 'POST',
                url: '/product/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllProducts();
            }, function errorCallback(error) {
                toaster.pop('error', 'Products', error.data.message);
            });
        };

        $scope.activeProduct = function(product_id) {
            var data = {
                id: product_id
            };
            $http({
                method: 'POST',
                url: '/product/active',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllProducts();
            }, function errorCallback(error) {
                toaster.pop('error', 'Products', error.data.message);
            });
        };

        $scope.deleteProduct = function(product_id) {
            var data = {
                id: product_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/product/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllProducts();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Products', error.data.message);
                });
            }
        };

        $scope.getAllProducts = function() {
            var data = {
                district_id: $cookieStore.get('loggedInDistrict').id
            };
            $http({
                method: 'POST',
                url: '/product/getAllDistProducts',
                data: data
            }).then(function successCallback(response) {
                var products = response.data;
                $scope.products = products;
            }, function errorCallback(error) {
                toaster.pop('error', 'Products', 'cannot get all the Products! try reloading page.');
            });
        };

        $scope.editProduct = function(product_id) {
            $state.go('districts.product_edit', {
                product_id: product_id
            });
        };
    }
]);

app.controller('editDistrictProductController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        $scope.categories = [];
        if (typeof $cookieStore.get('loggedInDistrict').id != 'undefined') {
            var data = {
                district_id: $cookieStore.get('loggedInDistrict').id
            };
            $http({
                method: 'POST',
                url: '/category/getAllDistCategories',
                data: data
            }).then(function successCallback(response) {
                $scope.categories = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Product', 'Something went wrong!');
            });
        } else {
            toaster.pop('error', 'Login', 'something went wrong! Please try re-login');
        }

        $scope.getEditProduct = function() {
            $http({
                method: 'GET',
                url: '/product/edit?id=' + $state.params.product_id,
            }).then(function successCallback(response) {
                $scope.product = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Product', 'cannot get all the Product! try reloading page.');
            });
        };

        $scope.saveEditProduct = function() {
            data = $scope.product;
            if ($scope.uploadedImageName) {
                data.product_image = $scope.uploadedImageName;
            } else {
                // console.log(data.product_image);
            }
            $http({
                method: 'POST',
                url: '/product/edit',
                data: data
            }).then(function successCallback(response) {
                $state.go('districts.product_list');
                toaster.pop('success', 'Product', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Product', error.data.message);
            });
        };

        $scope.cancelProductForm = function() {
            $state.go('districts.product_list');
        };

        $scope.uploadFile = function(file) {
            if (!file) return console.log("No file selected !");
            Loading(true);
            Upload.upload({
                url: '/product/upload',
                data: {
                    file: file
                }
            }).then(function successCallback(response) {
                Loading(false);
                $scope.uploadedImageName = response.data;
            }, function errorCallback(error) {
                Loading(false);
                toaster.pop('error', 'Product', error.data.message);
            });
        };
    }
]);
