var db = require('orm').db,
    Issues = db.models.issues,
    Issues_messages = db.models.issue_messages;

exports.register = function(req, res) {
    if (typeof req.body.subject == 'undefined' ||
        typeof req.body.priority == 'undefined' ||
        typeof req.body.staff_id == 'undefined' ||
        typeof req.body.description == 'undefined'
    ) {
        return sendError(req, res, 422, 'Please check your parameters.');
    }
    if (typeof req.body.student_id == 'undefined' && typeof req.body.parent_id == 'undefined') {
        return sendError(req, res, 422, 'student or parent id required.');
    }
    newIssue = new Issues();
    newIssue.student_id = (req.body.student_id != null && req.body.student_id != "") ? req.body.student_id : null;
    newIssue.parent_id = (req.body.parent_id != null && req.body.parent_id != "") ? req.body.parent_id : null;
    newIssue.subject = req.body.subject;
    newIssue.priority = req.body.priority;
    newIssue.staff_id = req.body.staff_id;
    newIssue.description = req.body.description;
    newIssue.status = (typeof req.body.status != 'undefined') ? req.body.status : '1';
    newIssue.save(function(err, created_issue) {
        if (err) {
            console.log(err);
            return sendError(req, res, 500, 'something went wrong.');
        }
        return res.send(created_issue);
    });
};

exports.getAllIssues = function(req, res) {
    Issues.find({}, function(err, allIssues) {
        if (err) {
            console.log(err);
            return sendError(req, res, 500, 'something went wrong.');
        }
        return res.send(allIssues);
    });
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'Appointment id required!!');
    }
    Issues.find({
        id: req.body.id
    }, 1, function(err, issues) {
        if (err) {
            console.log(err);
            return sendError(req, res, 500, 'something went wrong.');
        }
        issues[0].student_id = req.body.student_id;
        issues[0].subject = req.body.subject;
        issues[0].priority = req.body.priority;
        issues[0].staff_id = req.body.staff_id;
        issues[0].parent_id = req.body.parent_id;
        issues[0].description = req.body.description;
        issues[0].status = (typeof req.body.status != 'undefined') ? req.body.status : issues[0].status;
        issues[0].save(function(err, updated_issues) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, 'something went wrong.');
            }
            return res.send(updated_issues);
        });
    });
};

exports.active = function(req, res) {
    if (typeof req.body.id === 'undefined') {
        return sendError(req, res, 404, 'Issue id required!!');
    }
    Issues.find({
        id: req.body.id
    }, 1, function(err, issues) {
        if (err) {
            console.log(err);
            return sendError(req, res, 500, 'something went wrong.');
        }
        issues[0].status = "1";
        issues[0].save(function(err, activated_issue) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, 'something went wrong.');
            }
            return res.send(activated_issue);
        });
    });
};

exports.inactive = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Appointment id required!!');
    }
    Issues.find({
        id: req.body.id
    }, 1, function(err, issues) {
        if (err) {
            console.log(err);
            return sendError(req, res, 500, 'something went wrong.');
        }
        issues[0].status = "0";
        issues[0].save(function(err, inactivated_issue) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, 'something went wrong.');
            }
            return res.send(inactivated_issue);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Appointment id required!!');
    }
    Issues.find({
        id: req.body.id
    }, 1, function(err, issues) {
        if (err) {
            console.log(err);
            return sendError(req, res, 500, 'something went wrong.');
        }
        if (issues.length == 0) {
            return sendError(req, res, 404, 'issue not found.');
        }
        issues[0].remove(function(err) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, 'something went wrong.');
            }
            return res.send(issues[0]);
        });
    });
};

exports.getIssue = function(req, res) {
    if (typeof req.query.id == 'undefined') {
        return sendError(req, res, 401, "ID required field");
    }
    Issues.find({
        id: req.query.id
    }, 1, function(err, issues) {
        if (err) {
            console.log(err);
            return sendError(req, res, 500, 'something went wrong.');
        }
        if (!(issues.length)) return sendError(req, res, 404, "Issues not found");
        return res.send(issues[0]);
    });
};

exports.getAllParentsIssues = function(req, res) {
    if (typeof req.body.parent_id == 'undefined' || req.body.parent_id == 'null') {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = "SELECT t1.*,CONCAT(t2.first_name,' ',t2.last_name) as staff_name,t3.full_name FROM `issues` as t1 LEFT JOIN staff as t2 on t1.staff_id = t2.id LEFT JOIN parents as t3 on t1.parent_id = t3.id WHERE t1.parent_id = ?";
    db.driver.execQuery(query_str, [req.body.parent_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getAllIssues = function(req, res) {
    if (typeof req.body.staff_id == 'undefined' || req.body.staff_id == 'null') {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = "SELECT t1.*,CONCAT(t2.first_name,' ',t2.last_name) as student_name,t3.full_name as parent_name FROM `issues` as t1 LEFT JOIN admissions as t2 on t1.student_id = t2.id LEFT JOIN parents as t3 on t1.parent_id = t3.id WHERE t1.staff_id = ?";
    db.driver.execQuery(query_str, [req.body.staff_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.updateStatus = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Issue id required!!');
    }
    Issues.find({
        id: req.body.id
    }, 1, function(err, issues) {
        if (err) {
            console.log(err);
            return sendError(req, res, 500, 'something went wrong.');
        }
        issues[0].status = req.body.status;
        issues[0].save(function(err, status_changed) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, 'something went wrong.');
            }
            return res.send(status_changed);
        });
    });

};

exports.getAllStudentsIssues = function(req, res) {
    if (typeof req.body.student_id == 'undefined' || req.body.student_id == 'null') {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = "SELECT t1.*,CONCAT(t2.first_name,' ',t2.last_name) as staff_name,CONCAT(t3.first_name,' ',t3.last_name) as student_name FROM `issues` as t1 LEFT JOIN staff as t2 on t1.staff_id = t2.id LEFT JOIN admissions as t3 on t1.student_id = t3.id LEFT JOIN schools as t4 on t3.school_id = t4.id WHERE t1.student_id = ?";
    db.driver.execQuery(query_str, [req.body.student_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.saveIssuesMessges = function(req, res) {
    newIssues_messages = new Issues_messages();
    newIssues_messages.issue_id = req.body.issue_id;
    newIssues_messages.message_from = req.body.message_from;
    newIssues_messages.message_to = req.body.message_to;
    newIssues_messages.message = req.body.message;
    newIssues_messages.save(function(err, created_Issues_messages) {
        if (err) {
            console.log(err);
            return sendError(req, res, 500, 'something went wrong.');
        }
        return res.send(created_Issues_messages);
    });
};

exports.showIssueMessagesToStaff = function(req, res) {
    if (typeof req.body.id == 'undefined' || req.body.id == 'null') {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = 'SELECT t1.*,t2.*,CONCAT(t3.first_name," ",t3.last_name) as staff_name,CONCAT(t4.first_name," ",t4.last_name) as student_name,t5.full_name FROM issue_messages as t1 LEFT JOIN issues as t2 on t1.issue_id =t2.id LEFT JOIN staff as t3 on t2.staff_id = t3.id LEFT JOIN admissions as t4 on t2.student_id = t4.id LEFT JOIN parents as t5 on t2.parent_id = t5.id LEFT JOIN schools as t6 on t4.school_id = t6.id WHERE t2.id = ?';
    db.driver.execQuery(query_str, [req.body.id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.viewIssue = function(req, res) {
    if (typeof req.body.id == 'undefined' || req.body.id == 'null') {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = 'SELECT t1.*,t2.*,CONCAT(t3.first_name," ",t3.last_name) as staff_name,CONCAT(t4.first_name," ",t4.last_name) as student_name,t5.full_name FROM issue_messages as t1 LEFT JOIN issues as t2 on t1.issue_id = t2.id LEFT JOIN staff as t3 on t2.staff_id = t3.id LEFT JOIN admissions as t4 on t2.student_id = t4.id LEFT JOIN parents as t5 on t2.parent_id = t5.id LEFT JOIN schools as t6 on t4.school_id = t6.id WHERE t2.id = ?';
    db.driver.execQuery(query_str, [req.body.id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
