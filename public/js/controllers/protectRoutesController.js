app.controller('protectUserRoutesController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
		if (!(typeof $cookieStore.get('loggedInUser') != 'undefined' && typeof $cookieStore.get('loggedInUser').id != 'undefined')) {
		  	$state.go('signin');
		}
    }
]);
app.controller('protectStaffRoutesController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
    	if (!(typeof $cookieStore.get('loggedInStaff') != 'undefined' && typeof $cookieStore.get('loggedInStaff').id != 'undefined')) {
		  $state.go('staffSignin');
		}
    }
]);
app.controller('protectStudentRoutesController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
    	if (!(typeof $cookieStore.get('loggedInStudent') != 'undefined' && typeof $cookieStore.get('loggedInStudent').id != 'undefined')) {
		  $state.go('studentSignin');
		}
    }
]);
app.controller('protectParentRoutesController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
    	if (!(typeof $cookieStore.get('loggedInParent') != 'undefined' && typeof $cookieStore.get('loggedInParent').id != 'undefined')) {
		  $state.go('parentSignin');
		}
    }
]);