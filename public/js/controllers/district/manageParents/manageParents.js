app.controller('parentSigninFormController', ['$scope', '$http', '$state', '$cookieStore', 'toaster',
    function($scope, $http, $state, $cookieStore, toaster) {
        $scope.parentsLogin = function() {
            var data = $scope.parent;
            $http({
                method: 'POST',
                url: '/parents/parentsLogin',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Todo', "Created successfully.");
                $cookieStore.put('loggedInParent', response.data);
                $state.go('parents.dashboard');
            }, function errorCallback(error) {
                $scope.authError = error.data.message;
            });
        };
    }
]);


app.controller('parentLogOutController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.logout = function(parent_id) {
            $http({
                method: 'POST',
                url: '/logout'
            }).then(function successCallback(response) {
                angular.forEach($cookieStore, function(v, k) {
                    $cookieStore.remove(k);
                });
                $cookieStore.remove('loggedInParent');
                $cookieStore.remove('loggedInStudent');
                $cookieStore.remove('loggedInStaff');
                $cookieStore.remove('loggedInUser');
                $cookieStore.remove('loggedInDistrict');
                $cookieStore.remove('loggedInSchool');
            }, function errorCallback(error) {
                toaster.pop('error', 'Logout', 'Something went wrong!');
            });
        };
    }
]);

app.controller('editParentsProfileController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.cancelParentsForm = function(parent_id) {
            $state.go('parents.dashboard');
        };

        $scope.getParents = function() {
            var data = {
                parent_id: $cookieStore.get('loggedInParent').id
            };
            $http({
                method: 'POST',
                url: '/parents/getStudentsParentsEdit',
                data: data
            }).then(function successCallback(response) {
                $scope.parents = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Parents Profile', error.data.message);
            });
        };

        $scope.saveParents = function() {
            data = $scope.parents;
            $http({
                method: 'POST',
                url: '/parents/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Parents', 'Saved Successfully.');
                $state.go('parents.dashboard');
            }, function errorCallback(error) {
                toaster.pop('error', 'Parents', error.data.message);
            });
        };
    }
]);

app.controller('changeParentsPasswordController', ['$scope', '$http', '$translate', '$localStorage', '$cookieStore', 'toaster',
    function($scope, $http, $translate, $localStorage, $cookieStore, toaster) {
        $scope.changePassword = function() {
            if ($scope.newPassword != $scope.confirmNewPassword) {
                toaster.pop('error', 'Password mis match', 'New Password and Confirm New password Must be same!');
            } else {
                var data = {
                    id: $cookieStore.get('loggedInParent').id,
                    oldPassword: $scope.oldPassword,
                    newPassword: $scope.newPassword,
                    confirmNewPassword: $scope.confirmNewPassword
                };
                $http({
                    method: 'POST',
                    url: '/parent/change_password',
                    data: data
                }).then(function successCallback(response) {
                    toaster.pop('success', 'Password', 'Your Password has been changed.');
                }, function errorCallback(error) {
                    toaster.pop('error', 'Password', error.data.message);
                });
            }
        };
    }
]);
