var db = require('orm').db,
    Materials = db.models.materials;
var fs = require('fs');

exports.register = function(req, res) {
    if (typeof req.body.school_id == 'undefined' ||
        typeof req.body.staff_id == 'undefined' ||
        typeof req.body.grade_id == 'undefined' ||
        typeof req.body.subject_id == 'undefined'
    ) {
        return apiSendError(req, res, 422, "Please check your parameters");
    }
    newMaterials = new Materials();
    newMaterials.school_id = req.body.school_id;
    newMaterials.staff_id = req.body.staff_id;
    newMaterials.grade_id = req.body.grade_id;
    newMaterials.subject_id = req.body.subject_id;
    newMaterials.title = req.body.title;
    newMaterials.description = req.body.description;
    newMaterials.due_date = req.body.due_date;
    //newMaterials.file_name = req.body.file_name;
    var material_file = null;
    if (req.files) {
      if (!req.files.material_file) {
        return apiSendError(req, res, 422, 'Material file is required!');
      }else {
        var file = req.files.material_file;
        fileName = file.path.split('/');
        type = (typeof file.name != 'undefined' && file.name != "") ? "." + file.name.split('.')[1] : "";
        fs.rename(file.path, __dirname + "/../../../public/upload/materials/" + fileName[fileName.length - 1] + type, function(err) {
            if (err) {
                console.log(err);
            }
        });
        material_file = fileName[fileName.length - 1] + type;
      }
    }
    newMaterials.file_name = material_file;
    newMaterials.save(function(err, created_Materials) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 422, 'Please check your parameters!!');
        }
        var response = {};
        response['data'] = [];
        response['code'] = 200;
        return res.send(response);
    });
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined' ||
        typeof req.body.title == 'undefined' ||
        typeof req.body.description == 'undefined' ||
        typeof req.body.due_date == 'undefined'
    ) {
        return apiSendError(req, res, 422, "Please check your parameters");
    }
    Materials.find({
        id: req.body.id
    }, 1, function(err, materials) {
        if (err) {
              console.log(err);
            return apiSendError(req, res, 422, 'Please check your parameters!!');
        }
        materials[0].title = req.body.title;
        materials[0].description = req.body.description;
        materials[0].due_date = req.body.due_date;
        var material_file = materials[0].file_name;
        if (req.files) {
          if (!req.files.material_file) {
            return apiSendError(req, res, 422, "Please check your parameters");
          }else {
            var file = req.files.material_file;
            fileName = file.path.split('/');
            type = (typeof file.name != 'undefined' && file.name != "") ? "." + file.name.split('.')[1] : "";
            fs.rename(file.path, __dirname + "/../../../public/upload/materials/" + fileName[fileName.length - 1] + type, function(err) {
                if (err) {
                    console.log(err);
                }
            });
            material_file = fileName[fileName.length - 1]  + type;
          }
        }
        materials[0].file_name = (material_file) ? material_file : materials[0].file_name;
        materials[0].save(function(err, updated_materials) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 500, "Something went wrong!");
            }
            var response = {};
            response['data'] = [];
            response['code'] = 200;
            return res.send(response);
        });
    });
};

exports.apiGetAllStaffMaterials = function(req, res) {
    if (typeof req.body.school_id == 'undefined') {
        return apiSendError(req, res, 422, "School ID required");
    }
    var query_str = `SELECT t1.*,t2.grade,t3.school_name FROM materials as t1
    LEFT JOIN grades as t2 ON t1.grade_id = t2.id
    LEFT JOIN schools as t3 ON t1.school_id = t3.id
    WHERE t3.id = ? AND t1.file_name <> "" GROUP BY t1.id ORDER BY t1.due_date ASC`;
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 400, "Bad Request check your parameters");
        }
        var response = {};
        response['data'] = rows;
        response['code'] = 200;
        return res.send(response);
    });
};

exports.apiGetStudentMaterials = function(req, res) {
    if (typeof req.body.grade_id == 'undefined') {
        return apiSendError(req, res, 422, "Grade ID required");
    }
    var current_date = new Date().toISOString().split("T")[0].toString();
    var query_str = `SELECT t1.*, CONCAT(t2.first_name, " ", t2.last_name) as staff_name, t3.grade,  t4.subject_name
                     FROM materials as t1
                     LEFT JOIN staff as t2 ON t1.staff_id = t2.id
                     LEFT JOIN grades as t3 ON t1.grade_id = t3.id
                     LEFT JOIN subjects as t4 ON t1.subject_id = t4.id
                     WHERE t1.grade_id = ? AND t1.due_date >=  \'` + current_date + `\' ORDER BY t1.due_date DESC`;

    db.driver.execQuery(query_str, [req.body.grade_id], function(err, rows) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 400, "Bad Request check your parameters");
        }
        var response = {};
        response['data'] = rows;
        response['code'] = 200;
        return res.send(response);
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return apiSendError(req, res, 422, 'Class id required');
    }
    Materials.find({
        id: req.body.id
    }, 1, function(err, materials_list) {
        if (err) {
            return apiSendError(req, res, 400, "Bad Request check your parameters");
        }
        if (materials_list.length == 0) {
            return apiSendError(req, res, 404, "material not found.");
        }
        materials_list[0].remove(function(err) {
            if (err) {
                return apiSendError(req, res, 400, "Bad Request check your parameters");
            }
            var response = {};
            response['data'] = [];
            response['code'] = 200;
            return res.send(response);
        });
    });
};

var fs = require('fs');
exports.uploadFile = function(req, res) {
    var fs = require('fs');
    var materials = __dirname + "/../../../public/upload/materials/";
    if (!fs.existsSync(materials)) {
        fs.mkdirSync(materials);
    }
    var file = req.files.file;
    var fileExtension = file.name.split(".")[1];
    if (fileExtension) {
        fileName = file.path.split('/');
        fs.rename(file.path, materials + fileName[fileName.length - 1] + "." + fileExtension);
        var response = {};
        response['data'] = fileName[fileName.length - 1] + "." + fileExtension;
        response['code'] = 200;
        return res.send(response);
    } else {
        return apiSendError(req, res, 400, "Bad Request check your parameters");
    }

};
