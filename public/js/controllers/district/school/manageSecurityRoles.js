app.controller('staffNavbarController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore',
    function($scope, $http, $state, toaster, Upload, $cookieStore) {
    	$scope.security_roles = $cookieStore.get('loggedInStaff').security_roles;
    }
]);
app.controller('schoolNavbarController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore',
    function($scope, $http, $state, toaster, Upload, $cookieStore) {
    	$scope.security_roles = $cookieStore.get('loggedInSchool').security_roles;
    	$scope.school_admin_flag = false;
    	if(typeof $cookieStore.get('loggedInSchool').school_admin_flag != 'undefined' && $cookieStore.get('loggedInSchool').school_admin_flag == 'admin' ) {
    		$scope.school_admin_flag = true;
    	}
    }
]);

