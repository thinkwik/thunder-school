app.controller('createDistrictMaterialController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        $scope.getSchools = function() {
            var data = {
                district_id: $cookieStore.get('loggedInDistrict').id
            };
            $http({
                method: 'POST',
                url: '/school/getDistrictSchools',
                data: data
            }).then(function successCallback(response) {
                $scope.Schools = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'District', 'Something went wrong!');
            });
        };

        $scope.getSchoolGrades = function(school_id) {
            $scope.material.grade_id = "";
            var data = {
                school_id: school_id
            };
            $http({
                method: 'POST',
                url: '/grade/getAllSchoolGrades',
                data: data
            }).then(function successCallback(response) {
                $scope.grades = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Grades', 'Something went wrong!');
            });
        };

        $scope.changeSubjects = function(grade_id, school_id) {
            $scope.material.subject_id = "";
            var data = {
                grade_id: grade_id,
                school_id: school_id
            };
            $http({
                method: 'POST',
                url: '/subject/getSchoolGradesSubjectWise',
                data: data
            }).then(function successCallback(response) {
                $scope.subjects = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Subjects', error.data.message);
            });
        };

        $scope.uploadFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected !');
            Loading(true);
            Upload.upload({
                url: '/material/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.filename = success.data;
                    $scope.saveDisable = true;
                },
                function(error) {
                    toaster.pop('error', "Error", error.data.message);
                    $scope.saveDisable = false;
                });
        };

        $scope.listMaterials = function() {
            $state.go('districts.list_material');
        };

        $scope.createMaterials = function() {
            data = $scope.material;
            data.file_name = $scope.filename;
            $http({
                method: "POST",
                url: "/material/register",
                data: data
            }).then(function successCallback(response) {
                $state.go('districts.list_material');
                toaster.pop('success', 'Material', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Material', error.data.message);
            });
        };

    }
]);

app.controller('listDistrictMaterialController', ['$scope', '$http', '$state', 'toaster', '$cookieStore','$window',
    function($scope, $http, $state, toaster, $cookieStore,$window) {
        $scope.addNewMaterial = function() {
            $state.go('districts.create_material');
        };

        $scope.deleteMaterials = function(material_id) {
            var data = {
                id: material_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/material/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllMaterials();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Materials', error.data.message);
                });
            }
        };

        $scope.getAllMaterials = function() {
            var data = {
                district_id: $cookieStore.get('loggedInDistrict').id
            };
            $http({
                method: 'POST',
                url: '/material/getAllDistrictMaterials',
                data: data
            }).then(function successCallback(response) {
                var materials = response.data;
                $scope.materials = materials;
            }, function errorCallback(error) {
                toaster.pop('error', 'Materials', 'cannot get all the Materials! try reloading page.');
            });
        };

        $scope.editMaterial = function(material_id) {
            $state.go('districts.edit_material', {
                material_id: material_id
            });
        };

        $scope.DownloadMaterial = function(material) {
          if(material.file_name) {
              var materialFileName = material.file_name;
              materialFileName = materialFileName.split(".");
              var url =  $window.location.origin + '/upload/materials/' + material.file_name;
              var request = new XMLHttpRequest();
              request.open('HEAD', url, false);
              request.send();
              if(request.status == 200) {
                var element = angular.element('<a/>');
                element.attr({
                    href: url,
                    target: '_blank',
                    download: material.title + '.' + materialFileName[1]
                })[0].click();
              } else {
                toaster.pop('error', 'Materials', 'Material not found!');
                return false;
              }
          } else {
            toaster.pop('error', 'Materials', 'Material not found!');
            return false;
          }
      };
    }
]);

app.controller('editDistrictMaterialController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload','$window',
    function($scope, $http, $state, toaster, $cookieStore, Upload,$window) {

        $scope.getEditMaterial = function() {
            $http({
                method: 'GET',
                url: '/material/edit?id=' + $state.params.material_id
            }).then(function successCallback(response) {
                $scope.material = response.data;
                $scope.getSchools(response.data.school_id);
                $scope.getSchoolGrades(response.data.school_id);
                $scope.changeSubjects(response.data.grade_id, response.data.school_id);
            }, function errorCallback(error) {
                toaster.pop('error', 'Materials', 'cannot get all the Materials! try reloading page.');
            });
        };

        $scope.getSchools = function() {
            var data = {
                district_id: $cookieStore.get('loggedInDistrict').id
            };
            $http({
                method: 'POST',
                url: '/school/getDistrictSchools',
                data: data
            }).then(function successCallback(response) {
                $scope.Schools = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'District', 'Something went wrong!');
            });
        };

        $scope.getSchoolGrades = function(school_id) {
            var data = {
                school_id: school_id
            };
            $http({
                method: 'POST',
                url: '/grade/getAllSchoolGrades',
                data: data
            }).then(function successCallback(response) {
                $scope.grades = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Grades', 'Something went wrong!');
            });
        };

        $scope.changeSubjects = function(grade_id, school_id) {
            var data = {
                grade_id: grade_id,
                school_id: school_id
            };

            $http({
                method: 'POST',
                url: '/subject/getSchoolGradesSubjectWise',
                data: data
            }).then(function successCallback(response) {
                $scope.subjects = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Subjects', error.data.message);
            });
        };


        $scope.uploadFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected !');
            Loading(true);
            Upload.upload({
                url: '/material/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.filename = success.data;
                    $scope.saveDisable = true;
                },
                function(error) {
                    toaster.pop('error', "Error", error.data.message);
                    $scope.saveDisable = false;
                });
        };

        $scope.saveEditMaterials = function() {
            data = $scope.material;
            data.file_name = $scope.filename;
            $http({
                method: 'POST',
                url: '/material/edit',
                data: data
            }).then(function successCallback(response) {
                $state.go('districts.list_material');
                toaster.pop('success', 'Material', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Material', error.data.message);
            });
        };


        $scope.DownloadMaterial = function(material) {
          if(material.file_name) {
              var materialFileName = material.file_name;
              materialFileName = materialFileName.split(".");
              var url =  $window.location.origin + '/upload/materials/' + material.file_name;
              var request = new XMLHttpRequest();
              request.open('HEAD', url, false);
              request.send();
              if(request.status == 200) {
                var element = angular.element('<a/>');
                element.attr({
                    href: url,
                    target: '_blank',
                    download: material.title + '.' + materialFileName[1]
                })[0].click();
              } else {
                toaster.pop('error', 'Materials', 'Material not found!');
                return false;
              }
          } else {
            toaster.pop('error', 'Materials', 'Material not found!');
            return false;
          }
      };

        $scope.listMaterials = function() {
            $state.go('districts.list_material');
        };

    }
]);
