var db = require('orm').db,
    ChatRoom = db.models.chat_rooms,
    ChatRoomParticipants = db.models.chat_room_participants,
    GroupMessages = db.models.group_messages;
var shortid = require('shortid');

exports.register = function(req, res) {
    if (typeof req.body.school_id != 'undefined' && typeof req.body.creater_id != 'undefined' && typeof req.body.participants != 'undefined' && req.body.participants.length > 0) {
        //add creater unique id to participants list
        req.body.participants.push(req.body.creater_id);
        var new_chat_room = new ChatRoom();
        new_chat_room.school_id = req.body.school_id;
        new_chat_room.creater_id = req.body.creater_id.split("**")[0];
        new_chat_room.room_id = shortid.generate();
        new_chat_room.room_name = req.body.room_name;
        new_chat_room.scheduled_date = req.body.scheduled_date;
        if (req.body.scheduled_date.substring(req.body.scheduled_date.length - 2, req.body.scheduled_date.length) === 'pm') {
            dat = req.body.scheduled_date.split(' ');
            mar = dat[1].split(':');
            mar[0] = (parseInt(mar[0]) == 12) ? "12" : parseInt(mar[0]) + 12;
            new_chat_room.scheduled_date = dat[0] + " " + mar[0] + ":" + mar[1].substring(0, 2) + ":00";
        } else {
            dat = req.body.scheduled_date.split(' ');
            mar = dat[1].split(':');
            mar[0] = (parseInt(mar[0]) == 12) ? "00" : parseInt(mar[0]);
            new_chat_room.scheduled_date = dat[0] + " " + mar[0] + ":" + mar[1].substring(0, 2) + ":01";
        }
        new_chat_room.status = "1";
        new_chat_room.save(function(err, created_chat_room) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, "Bad Request check your parameters");
            }
            req.body.participants.forEach(function(value, key) {
                var parsed_data = value.split("**");
                var unique_id = parsed_data[0];
                var type = parsed_data[1];
                //create url_link
                var link = "";
                if (type == "staff") {
                    link = "https://" + req.headers.host + "/school/" + type + "/video_conference/live?room_id=" + created_chat_room.room_id;
                } else {
                    link = "https://" + req.headers.host + "/" + type + "/video_conference/live?room_id=" + created_chat_room.room_id;
                }
                var ui_message = "You got a new invitation for video conference.";
                var templet_url_query = "SELECT ui_data FROM email_templates WHERE ui_key =? LIMIT 1";
                db.driver.execQuery(templet_url_query, ['video_conference'], function(video_err, ui_rows) {
                    if (video_err) {
                        console.log(video_err);
                        return sendError(req, res, 422, "Bad Request check your parameters");
                    }
                    if (ui_rows.length > 0) {
                        ui_message = ui_rows[0].ui_data;
                    }

                    var email_html = `
                    <body style="font-family:arial; font-size:16px; color:#4c4c4c; padding:0; margin:0;">
                        <table width="100%" style="font-family:arial; font-size:16px; border:0px solid #000; max-width:700px; padding-top:30px;"  align="center" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td>
                                        <table width="100%" style="font-family:arial; font-size:16px; border:1px solid #d6d6d8; max-width:700px;"  align="center" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td style="padding:50px 40px 40px 40px; text-align:center; font-family:arial;">
                                                        <h1 style="font-family:arial; font-size:40px; color:#30333b; font-weight:normal; padding:0; margin:0;">Hello, <span style="color:rgb(242,53,95); font-weight:bold; text-transform:uppercase;">User</span></h1>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding:0 40px 30px 40px; text-align:center; font-family:arial;">
                                                        <p style="font-family:arial; font-size:22px; color:#737373; font-weight:normal; padding:0; margin:0;">`+ui_message+`<br />
                                                            Title : ` + req.body.room_name + ` <br /> Date : ` + created_chat_room.scheduled_date.split(" ")[0] + ` <br /> Time : ` + created_chat_room.scheduled_date.split(" ")[1] + `</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding:0 40px 50px 40px; text-align:center;">
                                                        <a href="` + link + `" style="text-decoration:none; outline:none;">
                                                            <img style="display:inline-block;" src="https://socratessms.com:8080/img/join_link.png" alt="Join Link" />
                                                        </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding:35px 40px;">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td valign="bottom" style="text-align:right; font-family:arial; font-size:14px; color:#6a6c72;">Kind Regards <br /><b style="text-transform:uppercase;">Thunder System</b></td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="bottom" style="text-align:right; font-family:arial; font-size:10px;">* You need to login into system first before joining conference.</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-family:arial; font-size:10px; padding:20px 0 30px 0; color:#a6a6a6; text-transform:uppercase; text-align:center;">
                                        &#169; 2016 Copyright Thunder System
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </body>
                    `;
                    var data = {
                        chat_room_id: created_chat_room.id,
                        unique_id: unique_id,
                        sent_url_link: link
                    };
                    ChatRoomParticipants.create(data, function(err) {
                        if (err) {
                            console.log(err);
                            return sendError(req, res, 500, "Something went wrong! while added participants");
                        }
                    });
                    var html = email_html;
                    var email_subject = created_chat_room.room_name + " - Video Conference Invitation";
                    sendMail(req, res, req.body.creater_id.split("**")[2], parsed_data[2], email_subject, html, function(err, success) {
                        if (err) {
                            console.log(err);
                        }
                    });
                });
            });
            return res.send(created_chat_room);
        });
    } else {
        return sendError(req, res, 422, 'please check your parameters');
    }
};

exports.getUserConfigCheck = function(req, res) {
    if (typeof req.body.unique_id == 'undefined' && typeof req.body.room_id == 'undefined') {
        return sendError(req, res, 422, 'please check your parameters.');
    }
    var query_str = "SELECT t1.unique_id, t2.room_id, t2.room_name, t2.status FROM chat_room_participants as t1 LEFT JOIN chat_rooms as t2 ON t1.chat_room_id = t2.id WHERE t2.room_id = ? AND t1.unique_id = ? ";
    db.driver.execQuery(query_str, [req.body.room_id, req.body.unique_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        if (!rows.length) {
            return sendError(req, res, 422, "you are not authorized for this chat.");
        }
        return res.send(rows);
    });
};
exports.getMessagingHistory = function(req, res) { 
    if(typeof req.body.room_id == 'undefined') {
        return sendError(req, res, 422, 'please check your parameters.');
    }
    var query_str = `SELECT t1.chat_room_id as room_id, t1.from_user as unique_id, t1.message , t1.created_at as datetime , CONCAT(COALESCE(t2.first_name,''),COALESCE(t3.first_name,''),COALESCE(t4.full_name,''),' ', COALESCE(t2.last_name,''),COALESCE(t3.last_name,'')) AS 'from'
                    FROM group_messages as t1
                    LEFT JOIN staff as t2 ON t1.from_user = t2.unique_id
                    LEFT JOIN admissions as t3 ON t1.from_user = t3.unique_id
                    LEFT JOIN parents as t4 ON t1.from_user = t4.unique_id

                    WHERE t1.chat_room_id = ? ORDER BY t1.id ASC limit 20`;
    db.driver.execQuery(query_str, [req.body.room_id], function(err, rows) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 422, "Bad Request check your parameters");
        }
        if (!rows.length) {
            return apiSendError(req, res, 422, "you are not authorized for this chat.");
        }
        return res.send(rows);
    });
};