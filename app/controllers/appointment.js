var db = require('orm').db,
    Admissions = db.models.admissions,
    Appointments = db.models.appointments,
    Staff = db.models.staff;

exports.register = function(req, res) {
    newAppointment = new Appointments();
    newAppointment.staff_id = req.body.staff_id;
    newAppointment.student_id = req.body.student_id;
    newAppointment.parent_id = req.body.parent_id;
    newAppointment.appointment_type = req.body.appointment_type;
    newAppointment.datetime = req.body.datetime;
    newAppointment.description = req.body.description;
    newAppointment.status = (typeof req.body.status != 'undefined') ? req.body.status : '0';
    newAppointment.student_attend = (typeof req.body.student_attend != 'undefined') ? req.body.student_attend : '0';
    newAppointment.phone_number = (typeof req.body.phone_number != 'undefined') ? req.body.phone_number : null;
    newAppointment.save(function(err, created_appointment) {
        if (err) {
            console.log(err);
            return sendError(req, res, 404, 'Appointment id required!!');
        }
        return res.send(created_appointment);
    });
};

exports.getAllAppointments = function(req, res) {
    Appointments.find({}, function(err, allAppointments) {
        if (err) {
            console.log(err);
            return sendError(req, res, 404, 'Appointment id required!!');
        }
        return res.send(allAppointments);
    });
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Appointment id required!!');
    }
    Appointments.find({
        id: req.body.id
    }, 1, function(err, appointments) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        appointments[0].staff_id = req.body.staff_id;
        appointments[0].student_id = (typeof req.body.student_id != 'undefined') ? req.body.student_id : appointments[0].student_id;
        appointments[0].parent_id = (typeof req.body.parent_id != 'undefined') ? req.body.parent_id : appointments[0].parent_id;
        appointments[0].appointment_type = req.body.appointment_type;
        appointments[0].datetime = req.body.datetime;
        appointments[0].description = req.body.description;
        appointments[0].status = (typeof req.body.status != 'undefined') ? req.body.status : appointments[0].status;
        appointments[0].student_attend = (typeof req.body.student_attend != 'undefined') ? req.body.student_attend : appointments[0].student_attend;
        appointments[0].phone_number = (typeof req.body.phone_number != 'undefined') ? req.body.phone_number : appointments[0].phone_number;
        appointments[0].save(function(err, updated_appointment) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(updated_appointment);
        });
    });
};

exports.active = function(req, res) {
    if (typeof req.body.id === 'undefined') {
        return sendError(req, res, 404, 'Appointment id required!!');
    }
    Appointments.find({
        id: req.body.id
    }, 1, function(err, appointment) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        appointment[0].status =  (appointment[0].status == "0") ? "1" : "0";
        appointment[0].save(function(err, activated_appointment) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(activated_appointment);
        });
    });
};

exports.inactive = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Appointment id required!!');
    }
    Appointments.find({
        id: req.body.id
    }, 1, function(err, appointment) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        appointment[0].status = "0";
        appointment[0].save(function(err, inactivated_appointment) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(inactivated_appointment);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Appointment id required!!');
    }
    Appointments.find({
        id: req.body.id
    }, 1, function(err, appointments) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        appointments[0].remove(function(err) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(appointments[0]);
        });
    });
};

exports.getAppointment = function(req, res) {
    if (typeof req.query.id == 'undefined') {
        return sendError(req, res, 401, "ID required field");
    }
    Appointments.find({
        id: req.query.id
    }, 1, function(err, appointments) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        if (!appointments.length) return sendError(req, res, 404, "Appointments not found");
        return res.send(appointments[0]);
    });
};

exports.getAllStudentsAppointments = function(req, res) {
    if (typeof req.body.student_id == 'undefined' || req.body.student_id == 'null') {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = "SELECT t1.*,CONCAT(t2.first_name,' ',t2.last_name) as staff_name,CONCAT(t3.first_name,' ',t3.last_name) as student_name FROM `appointments` as t1 LEFT JOIN staff as t2 on t1.staff_id = t2.id LEFT JOIN admissions as t3 on t1.student_id = t3.id LEFT JOIN schools as t4 on t3.school_id = t4.id WHERE t1.student_id = ?";
    db.driver.execQuery(query_str, [req.body.student_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getAllParentsAppointments = function(req, res) {
    if (typeof req.body.parent_id == 'undefined' || req.body.parent_id == 'null') {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = "SELECT t1.*,CONCAT(t2.first_name,t2.last_name) as staff_name,CONCAT(t3.first_name,' ',t3.last_name) as student_name,t5.full_name FROM `appointments` as t1 LEFT JOIN staff as t2 on t1.staff_id = t2.id LEFT JOIN admissions as t3 on t1.student_id = t3.id LEFT JOIN parents as t5 on t1.parent_id = t5.id LEFT JOIN schools as t4 on t3.school_id = t4.id WHERE t1.parent_id = ?";
    db.driver.execQuery(query_str, [req.body.parent_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.changeStatus = function(req, res) {
    if (typeof req.body.id == 'undefined' || req.body.id == 'null') {
        return sendError(req, res, 422, "refer parameters");
    }
    Appointments.find({
        id: req.body.id
    }, 1, function(err, appointment) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        appointment[0].status = req.body.status;
        appointment[0].save(function(err, updated_status) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(updated_status);
        });
    });
};

exports.changeStudentAttend = function(req, res) {
    if (typeof req.body.id == 'undefined' || req.body.id == 'null') {
        return sendError(req, res, 422, "refer parameters");
    }
    Appointments.find({
        id: req.body.id
    }, 1, function(err, appointment) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        appointment[0].student_attend = req.body.student_attend;
        appointment[0].save(function(err, updated_student_attend) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(updated_student_attend);
        });
    });
};
