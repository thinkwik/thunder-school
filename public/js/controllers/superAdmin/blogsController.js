app.controller('blogController', ['$scope', '$http', '$state', 'toaster', 'Upload',
    function($scope, $http, $state, toaster, Upload) {
        $scope.saveDisable = false;
        $scope.createBlog = function() {
            var data = {
                'blog_title': $scope.blog_title,
                'blog_content': $scope.blog_content,
                'image_url': $scope.filename
            };
            $http({
                method: 'POST',
                url: '/blog/register',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', "Blogs", "Blog created successfully!!!");
                $state.go('users.blogs_list');
            }, function errorCallback(error) {
                toaster.pop('error', "Error", error.data.message);
            });
        };
        $scope.cancelBlog = function() {
            $state.go('users.blogs_list');
        };

        $scope.uploadFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected !');
            Loading(true);
            Upload.upload({
                url: '/blog/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.filename = success.data;
                    $scope.saveDisable = true;
                },
                function(error) {
                    toaster.pop('error', "Error", error.data.message);
                    $scope.saveDisable = false;
                });
        };
    }
]);

app.controller('listBlogController', ['$scope', '$http', '$state', 'toaster', 'Upload',
    function($scope, $http, $state, toaster, Upload) {
        $scope.listBlog = function() {
            $http({
                method: 'POST',
                url: '/blog/getAllBlogs'
            }).then(function successCallback(response) {
                var blogs = response.data;
                $scope.blogs = blogs;
            }, function errorCallback(error) {
                toaster.pop('error', 'Blogs', 'cannot get all the blogs! try reloading page.');
            });
        };

        $scope.activeBlog = function(blog_id) {
            var data = {
                id: blog_id
            };
            $http({
                method: 'POST',
                url: '/blog/active',
                data: data
            }).then(function successCallback(response) {
                $scope.listBlog();
            }, function errorCallback(error) {
                toaster.pop('error', 'Blog', error.data.message);
            });
        };

        $scope.deleteBlog = function(blog_id) {
            var data = {
                id: blog_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/blog/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.listBlog();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Blog', error.data.message);
                });
            }
        };

        $scope.inactiveBlog = function(blog_id) {
            var data = {
                id: blog_id
            };
            $http({
                method: 'POST',
                url: '/blog/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.listBlog();
            }, function errorCallback(error) {
                toaster.pop('error', 'Blog', error.data.message);
            });
        };

        $scope.editBlog = function(blog_id) {
            $state.go('users.blog_edit', {
                blog_id: blog_id
            });
        };

        $scope.getEditBlog = function() {
            var data = $scope.blog;
            $http({
                method: 'GET',
                url: '/blog/edit?id=' + $state.params.blog_id,
                data: data
            }).then(function successCallback(response) {

                $scope.blog = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Blogs', error.data.message);
            });
        };
    }
]);



app.controller('editBlogController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$rootScope','$window',
    function($scope, $http, $state, toaster, Upload, $rootScope,$window) {
        $scope.cancelBlog = function() {
            $state.go('users.blogs_list');
        };

        $scope.saveEditBlog = function(blog_id) {
            var data = {
                'id': $scope.blog.id,
                'blog_title': $scope.blog.blog_name,
                'blog_content': $scope.blog.blog_content,
                'image_url': $scope.filename
            };
            $http({
                method: 'POST',
                url: '/blog/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Blogs', 'Updated Successfully.');
                $state.go('users.blogs_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'Blogs', error.data.message);
            });
        };

        $scope.getEditBlog = function() {
            $http({
                method: 'GET',
                url: '/blog/edit?id=' + $state.params.blog_id
            }).then(function successCallback(response) {
                console.log(response.data)
                $scope.blog = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Blogs', 'cannot get all the Blogs! try reloading page.');
            });
        };

        $scope.uploadFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected !');
            Loading(true);
            Upload.upload({
                url: '/blog/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.filename = success.data;
                },
                function(error) {
                    toaster.pop('error', "Error", error.data.message);
                    Loading(false);
                });
        };
    }
]);
