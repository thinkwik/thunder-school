var bCrypt = require('bcrypt-nodejs');
var db = require('orm').db,
    Child_Menus = db.models.child_menus;
ParentPages = db.models.parentPages;
Page = db.models.pages;

exports.register = function(req, res) {
    new_pages = new Page();
    new_pages.parent_page_id = req.body.parent_page_id;
    new_pages.parents_menu_id = req.body.menu_id;
    new_pages.title = req.body.title;
    new_pages.content = req.body.content;
    new_pages.image = req.body.filenames;
    new_pages.status = (typeof req.body.status != 'undefined') ? req.body.status : '0';
    new_pages.save(function(err, saved_pages) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(saved_pages);
    });
};

exports.getPage = function(req, res) {
    if (typeof req.query.id == 'undefined') {
        return sendError(req, res, 422, 'page id required');
    }
    Page.find({
        id: req.query.id
    }, 1, function(err, pages) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!pages.length) {
            return sendError(req, res, 404, "Page not found");
        }
        return res.send(pages[0]);
    });
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'Page id required');
    }
    Page.find({
        id: req.body.id
    }, 1, function(err, pages) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!pages.length) {
            return sendError(req, res, 404, "page not found");
        }
        pages[0].parent_menu_id = req.body.parent_menu_id;
        pages[0].parent_page_id = req.body.parent_page_id;
        pages[0].title = req.body.title;
        pages[0].content = req.body.content;
        pages[0].image = req.body.image;
        pages[0].status = (typeof req.body.status != 'undefined') ? req.body.status : '1';
        pages[0].save(function(err, pages) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "something went wrong in saving page");
            }
            return res.send(pages);
        });
    });
};

exports.active = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'page id required');
    }
    Page.find({
        id: req.body.id
    }, 1, function(err, pages) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        pages[0].status = "1";
        pages[0].save(function(err, activated_page) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "something went wrong in activating page");
            }
            return res.send(activated_page);
        });
    });
};

exports.inactive = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'menu id required');
    }
    Page.find({
        id: req.body.id
    }, 1, function(err, pages) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        pages[0].status = "0";
        pages[0].save(function(err, inactivated_pages) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "something went wrong in inactivating pages");
            }
            return res.send(inactivated_pages);
        });
    });
};



exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'Page id required');
    }
    Page.find({
        id: req.body.id
    }, 1, function(err, pages) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (pages.length === 0) {
            return sendError(req, res, 404, "Page not found");
        }
        pages[0].remove(function(err) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "Something went wrong in deleting page!Please try again");
            }
            return res.send(pages[0]);
        });
    });
};

exports.getAllPages = function(req, res) {
    var query_str = "SELECT t1.*,t2.title as parent_page_title,t3.title as menu FROM pages as t1 LEFT JOIN parent_menus as t2 on t1.parents_menu_id = t2.id LEFT JOIN parentPages as t3 on t1.parent_page_id = t3.id";
    db.driver.execQuery(query_str, function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

var fs = require('fs');
exports.uploadFile = function(req, res) {
    var fs = require('fs');
    var pageImages = __dirname + "/../../public/upload/pageImages/";
    if (!fs.existsSync(pageImages)) {
        fs.mkdirSync(pageImages);
    }
    var file = req.files.file;
    var fileExtension = file.name.split(".")[1];
    if (fileExtension) {
      fileName = file.path.split('/');
      fs.rename(file.path, pageImages + fileName[fileName.length - 1] +"."+ fileExtension);
      return res.send(fileName[fileName.length - 1] +"."+ fileExtension);
    }
    else {
      return sendError(req, res, 400, "Bad Request check your parameters");
    }

};
