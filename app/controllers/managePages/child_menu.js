var bCrypt = require('bcrypt-nodejs');
var db = require('orm').db,
    Parent_Menus = db.models.parent_menus;
Child_Menus = db.models.child_menus;

exports.register = function(req, res) {
    new_child_menus = new Child_Menus();
    new_child_menus.title = req.body.title;
    new_child_menus.parent_menu_id = req.body.parent_menu_id;
    new_child_menus.position = req.body.position;
    new_child_menus.status = (typeof req.body.status != 'undefined') ? req.body.status : '1';
    new_child_menus.save(function(err, saved_child_menu) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(saved_child_menu);
    });
};

exports.getChildMenu = function(req, res) {
    if (typeof req.query.id == 'undefined') {
        return sendError(req, res, 422, 'menu id required');
    }
    Child_Menus.find({
        id: req.query.id
    }, 1, function(err, childMenus) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!childMenus.length) {
            return sendError(req, res, 404, "Menu not found");
        }
        return res.send(childMenus[0]);
    });
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'menu id required');
    }
    Child_Menus.find({
        id: req.body.id
    }, 1, function(err, child_menus) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!child_menus.length) {
            return sendError(req, res, 404, "menu not found");
        }
        child_menus[0].title = req.body.title;
        child_menus[0].parent_menu_id = req.body.parent_menu_id;
        child_menus[0].position = req.body.position;
        child_menus[0].status = (typeof req.body.status != 'undefined') ? req.body.status : '0';
        child_menus[0].save(function(err, saved_child_menus) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "something went wrong in saving menu");
            }
            return res.send(saved_child_menus);
        });
    });
};

exports.active = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'menu id required');
    }
    Child_Menus.find({
        id: req.body.id
    }, 1, function(err, child_menus) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        child_menus[0].status = "1";
        child_menus[0].save(function(err, activated_child_menu) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "something went wrong in activating menu");
            }
            return res.send(activated_child_menu);
        });
    });
};

exports.inactive = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'menu id required');
    }
    Child_Menus.find({
        id: req.body.id
    }, 1, function(err, child_menus) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        child_menus[0].status = "0";
        child_menus[0].save(function(err, inactivated_child_menus) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "something went wrong in inactivating menu");
            }
            return res.send(inactivated_child_menus);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'menu id required');
    }
    Child_Menus.find({
        id: req.body.id
    }, 1, function(err, child_menus) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!child_menus.length) {
            return sendError(req, res, 404, "Menu not found");
        }
        child_menus[0].remove(function(err) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "Something went wrong in deleting menu!Please try again");
            }
            return res.send(child_menus[0]);
        });
    });
};

exports.getAllChildMenus = function(req, res) {
    var query_str = "SELECT t1.*,t2.title as parent_title FROM child_menus as t1 INNER JOIN parent_menus as t2 WHERE t1.parent_menu_id = t2.id";
    db.driver.execQuery(query_str, function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
