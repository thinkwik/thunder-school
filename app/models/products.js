var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Products = db.define('products', {
    category_id: {
        type: 'integer'
    },
    product_name: String,
    product_image: String,
    product_quantity: String,
    product_price: String,
    product_discount: 'integer',
    product_notes: String,
    status: ['0', '1']
}, {
    timestamp: true,
    methods: {

    }
});
