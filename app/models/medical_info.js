var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Medical_info = db.define('medical_info', {
    school_id: {
        type: 'integer'
    },
    type: String,
    name: String,
}, {
    timestamp: true,
    methods: {

    }
});
