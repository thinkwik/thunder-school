app.controller('studentSigninFormController', ['$scope', '$http', '$state', '$cookieStore', 'toaster',
    function($scope, $http, $state, $cookieStore, toaster) {
        $scope.studentLogin = function() {
            var data = $scope.student;
            $http({
                method: 'POST',
                url: '/student/studentLogin',
                data: data
            }).then(function successCallback(response) {
                $cookieStore.put('loggedInStudent', response.data);
                $state.go('students.dashboard');
            }, function errorCallback(error) {
                $scope.authError = error.data.message;
            });
        };
    }
]);

app.controller('studentLogoutController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.logout = function(student_id) {
            $http({
                method: 'POST',
                url: '/logout'
            }).then(function successCallback(response) {
                angular.forEach($cookieStore, function(v, k) {
                    $cookieStore.remove(k);
                });
                $cookieStore.remove('loggedInParent');
                $cookieStore.remove('loggedInStudent');
                $cookieStore.remove('loggedInStaff');
                $cookieStore.remove('loggedInUser');
                $cookieStore.remove('loggedInDistrict');
                $cookieStore.remove('loggedInSchool');
            }, function errorCallback(error) {
                toaster.pop('error', 'Logout', 'Something went wrong!');
            });
        };
    }
]);

app.controller('studentAttendenceListController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.getAllStudentAttendence = function(student_id) {
            var data  = {
                student_id:$cookieStore.get('loggedInStudent').id
            }
            $http({
                method: 'POST',
                url: '/attendance_report/getAllStudentAttendence',
                data:data
            }).then(function successCallback(response) {
                $scope.attendance_list = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Attendance', 'Something went wrong!');
            });
        };
    }
]);

app.controller('parentAttendenceListController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.getAllStudentAttendence = function(student_id) {
            var data  = {
                student_id:$cookieStore.get('loggedInParent').student_id
            }
            $http({
                method: 'POST',
                url: '/attendance_report/getAllStudentAttendence',
                data:data
            }).then(function successCallback(response) {
                $scope.attendance_list = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Attendance', 'Something went wrong!');
            });
        };
    }
]);

app.controller('editStudentProfileController', ['$scope', '$http', '$state', '$cookieStore', 'toaster',
    function($scope, $http, $state, $cookieStore, toaster) {

        $scope.cancelStudentsForm = function() {
            $state.go('students.dashboard');
        };

      $scope.saveStudent = function() {
            data = $scope.admission;
            data.image = $scope.image;
            $http({
                method: 'POST',
                url: '/student/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Admission Inquiry', 'Saved Successfully.');
                $state.go('students.dashboard');
            }, function errorCallback(error) {
                toaster.pop('error', 'Admission Inquiry', error.data.message);
            });
        };

        $scope.uploadFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected !');
            Loading(true);
            Upload.upload({
                url: '/student/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.image = success.data;
                    $scope.saveDisable = true;
                },
                function(error) {
                    Loading(false);
                    toaster.pop('error', "Error", error.data.message);
                    $scope.saveDisable = false;
                });
        };

        $scope.getStudent = function() {
            var data = {
                student_id: $cookieStore.get('loggedInStudent').id
            };
            $http({
                method: 'POST',
                url: '/student/getStudent',
                data: data
            }).then(function successCallback(response) {
                $scope.admission = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Student Profile', 'cannot get Student Profile! try reloading page.');
            });
        };
    }
]);

app.controller('changeStudentPasswordController', ['$scope', '$http', '$translate', '$localStorage', '$cookieStore', 'toaster',
    function($scope, $http, $translate, $localStorage, $cookieStore, toaster) {
        $scope.changePassword = function() {
            if ($scope.newPassword != $scope.confirmNewPassword) {
                toaster.pop('error', 'Password mis match', 'New Password and Confirm New password Must be same!');
            } else {
                var data = {
                    id:$cookieStore.get('loggedInStudent').id,
                    oldPassword: $scope.oldPassword,
                    newPassword: $scope.newPassword,
                    confirmNewPassword: $scope.confirmNewPassword
                };
                $http({
                    method: 'POST',
                    url: '/student/change_password',
                    data: data
                }).then(function successCallback(response) {
                    toaster.pop('success', 'Password', 'Your Password has been changed.');
                }, function errorCallback(error) {
                    toaster.pop('error', 'Password', error.data.message);
                });
            }
        };
    }
]);
