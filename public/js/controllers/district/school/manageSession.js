app.controller('createSchoolSessionController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.addSession = function() {
            $state.go('schools.session_create');
        };

        $scope.cancelSessionCreate = function() {
            $state.go('schools.session_list');
        };

        $scope.createSession = function() {
            data = $scope.session;
            data.school_id = $cookieStore.get('loggedInSchool').id;
            data.created_by = $cookieStore.get('loggedInUser').id;
            $http({
                method: "POST",
                url: "/session/register",
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Session', 'Saved Successfully.');
                $state.go('schools.session_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'Session', error.data.message);
            });
        };

        $scope.cancelSessionForm = function() {
            $state.go('schools.session_list');
        };
    }
]);

app.controller('listSchoolSessionController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.addNewSession = function() {
            $state.go('schools.session_create');
        };

        $scope.inactiveSession = function(session_id) {
            var data = {
                id: session_id
            };
            $http({
                method: 'POST',
                url: '/session/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllSessions();
            }, function errorCallback(error) {
                toaster.pop('error', 'Session', error.data.message);
            });
        };

        $scope.activeSession = function(session_id) {
            var data = {
                id: session_id
            };
            $http({
                method: 'POST',
                url: '/session/active',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllSessions();
            }, function errorCallback(error) {
                toaster.pop('error', 'Session', error.data.message);
            });
        };

        $scope.deleteSession = function(session_id) {
            var data = {
                id: session_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/session/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllSessions();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Session', error.data.message);
                });
            }
        };

        $scope.getAllSessions = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/session/getAllSchoolSessions',
                data: data
            }).then(function successCallback(response) {
                var sessions = response.data;
                $scope.sessions = sessions;
            }, function errorCallback(error) {
                toaster.pop('error', 'Session', 'cannot get all the Sessions! try reloading page.');
                console.log(error.data.message);
            });
        };

        $scope.editSession = function(session_id) {
            $state.go('schools.session_edit', {
                session_id: session_id
            });
        };
    }
]);

app.controller('editSchoolSessionController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.getEditSession = function() {
            $http({
                method: 'GET',
                url: '/session/edit?id=' + $state.params.session_id
            }).then(function successCallback(response) {
                $scope.session = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Session', 'cannot get all the Sessions! try reloading page.');
            });
        };

        $scope.saveEditSession = function() {
            data = $scope.session;
            $http({
                method: 'POST',
                url: '/session/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Session', 'Saved Successfully.');
                    $state.go('schools.session_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'Session', error.data.message);
            });
        };

        $scope.cancelSessionForm = function() {
            $state.go('schools.session_list');
        };
    }
]);
