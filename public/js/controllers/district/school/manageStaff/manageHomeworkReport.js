app.controller('listHomeworkReportController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', '$window',
    function($scope, $http, $state, toaster, $cookieStore, $window) {

        $scope.getHemoworkRequiredData = function() {
            $scope.getHomework();
            $scope.getAllStudentList();
        };

        $scope.getHomework = function() {
            var data = {
                id: $state.params.homework_id
            };
            $http({
                method: 'POST',
                url: '/homework/getHomework',
                data: data
            }).then(function successCallback(response) {
                $scope.homework = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Reports', 'Something went wrong!');
            });
        };

        $scope.makeCompleteAll = function() {
            var data = {
                id: $state.params.homework_id
            };
            $http({
                method: 'POST',
                url: '/homework_report/makeCompleteAll',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllStudentList();
            }, function errorCallback(error) {
                toaster.pop('error', 'Reports', 'Something went wrong!');
            });
        };

        $scope.makeInCompleteAll = function() {
            var data = {
                id: $state.params.homework_id
            };
            $http({
                method: 'POST',
                url: '/homework_report/makeInCompleteAll',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllStudentList();
            }, function errorCallback(error) {
                toaster.pop('error', 'Reports', 'Something went wrong!');
            });
        };

        $scope.getAllStudentList = function() {
            var data = {
                homework_id: $state.params.homework_id
            };
            $http({
                method: 'POST',
                url: '/homework_report/getAllStudentList',
                data: data
            }).then(function successCallback(response) {
                $scope.homeworks_report_lists = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Reports', 'Something went wrong!');
            });
        };

        $scope.completeHomework = function(report_id) {
            var data = {
                id: report_id
            };
            $http({
                method: 'POST',
                url: '/homework_report/complete',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllStudentList();
            }, function errorCallback(error) {
                toaster.pop('error', 'Reports', 'Something went wrong!');
            });
        };

        $scope.DownloadStudentHomework = function(student_homework) {
            if (student_homework.file_path) {
                var studentAssignmentFileName = student_homework.file_path;
                studentAssignmentFileName = studentAssignmentFileName.split(".");
                var url = $window.location.origin + '/upload/studenthomeworks/' + student_homework.file_path;
                var request = new XMLHttpRequest();
                request.open('HEAD', url, false);
                request.send();
                if (request.status == 200) {
                    var element = angular.element('<a/>');
                    element.attr({
                        href: url,
                        target: '_blank',
                        download: student_homework.title + '.' + studentAssignmentFileName[1]
                    })[0].click();
                } else {
                    toaster.pop('error', 'Homework', 'Homework not found!');
                    return false;
                }
            } else {
                toaster.pop('error', 'Homework', 'Homework not found!');
                return false;
            }
        }

        $scope.updateStudentGrades = function(homework) {
            var data = {
                id: homework.id,
                achieved_grade: homework.achieved_grade
            };
            $http({
                method: 'POST',
                url: '/update/homework/grades',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Homework Reports', 'Grades updated successfully!');
                $scope.getAllStudentList();
            }, function errorCallback(error) {
                toaster.pop('error', 'Homework Reports', error.data.message);
            });
        };
    }
]);
