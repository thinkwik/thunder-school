// signin controller
app.controller('SigninFormController', ['$scope', '$http', '$state', '$cookieStore',
    function($scope, $http, $state, $cookieStore) {
        $scope.user = {};
        $scope.authError = null;
        $scope.login = function() {
            Loading(true);
            $scope.authError = null;
            // Try to login
            var data = {
                username: $scope.user.email,
                password: $scope.user.password
            };
            $http({
                method: 'POST',
                url: '/login',
                data: data
            }).then(function successCallback(response) {
                if (typeof response.data.user != 'undefined' && typeof response.data.user.user_type != 'undefined') {
                    if (response.data.user.user_type == "admin") {
                        $cookieStore.put('loggedInUser', response.data.user);
                        Loading(false);
                        $state.go('users.dashboard');
                    } else {
                        var data = {
                            school_id: response.data.user.school_id,
                            user_id: response.data.user.id
                        };
                        $http({
                            method: 'POST',
                            url: '/user/getSchoolByAuthorizedPerson',
                            data: data
                        }).then(function successCallback(response2) {
                            Loading(false);
                            if (response2.data.parent_id == "0") {
                                console.log(response2.data);
                                $cookieStore.put('loggedInDistrict', response2.data);
                                $state.go('districts.dashboard');
                            } else {
                                if(response.data.user.id == response2.data.authorized_user_id) {
                                    response2.data.school_admin_flag = 'admin';
                                }
                                //make other admin authorized user and then login
                                response2.data.authorized_user_id = response.data.user.id;
                                $cookieStore.put('loggedInSchool', response2.data);
                                $state.go('schools.dashboard');
                            }
                        }, function errorCallback(error2) {
                            Loading(false);
                            $scope.authError = error2.data.message;
                        });
                    }
                } else {
                    Loading(false);
                    $scope.authError = "Something went wrong! Please try reload page";
                }
            }, function errorCallback(error) {
                Loading(false);
                $scope.authError = error.data.message;
            });
        };
    }
]);

// admin user list
app.controller('superAdminListController', ['$scope', '$http', '$state',
    function($scope, $http, $state) {
        $scope.users = [];
        $scope.offset = 0;
        $scope.superadminusers = function() {
            var data = {
                offset: $scope.offset
            };
            $http({
                method: 'POST',
                url: '/superAdmin/getAllSuperAdmins',
                data: data
            }).then(function successCallback(response) {
                var users = response.data;
                $scope.users = users;
            }, function errorCallback(error) {
                if (error.data.error_code == 444) {
                    $state.go('signin');
                }
                console.log(error.data.message);
            });
        };

        $scope.inactiveUser = function(user_id) {
            var data = {
                id: user_id
            };
            $http({
                method: 'POST',
                url: '/user/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.superadminusers();
            }, function errorCallback(error) {
                console.log(error.data.message);
            });
        };

        $scope.activeUser = function(user_id) {
            var data = {
                id: user_id
            };
            $http({
                method: 'POST',
                url: '/user/active',
                data: data
            }).then(function successCallback(response) {
                $scope.superadminusers();
            }, function errorCallback(error) {
                console.log(error.data.message);
            });
        };

        $scope.deleteUser = function(user_id) {
            var data = {
                id: user_id
            };
            $http({
                method: 'POST',
                url: '/user/delete',
                data: data
            }).then(function successCallback(response) {
                $scope.superadminusers();
            }, function errorCallback(error) {
                console.log(error.data.message);
            });
        };

        $scope.editUser = function(user_id) {
            $state.go('users.edit_profile', {
                user_id: user_id
            });
        };

        $scope.ChengeOffest = function(increment) {
            $scope.offset = $scope.offset + increment * 10;
            $scope.superadminusers();
        };
    }
]);

app.controller('editUserController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        $scope.getUser = function() {
            $http({
                method: 'GET',
                url: '/user/edit?id=' + $cookieStore.get("loggedInUser").id,
            }).then(function successCallback(response) {
                $scope.user = response.data;
            }, function errorCallback(error) {
                console.log(error.data.message);
            });
        };
        $scope.saveEditUser = function() {
            var data = $scope.user;
            data.profile_image = $scope.profile_image;
            $http({
                method: 'POST',
                url: '/user/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Success', 'User updated.');
                $state.go('users.dashboard');
            }, function errorCallback(error) {
                toaster.pop('error', 'Success', error.data.message);
            });
        };
        $scope.cancel = function() {
            $state.go('users.dashboard');
        };

        $scope.uploadFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected !');
            Loading(true);
            Upload.upload({
                url: '/user/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.profile_image = success.data;
                    $scope.saveDisable = true;
                },
                function(error) {
                    toaster.pop('error', "Error", error.data.message);
                    $scope.saveDisable = false;
                });
        };
    }
]);


app.controller('changePasswordController', ['$scope', '$http', '$translate', '$localStorage', '$cookieStore', 'toaster',
    function($scope, $http, $translate, $localStorage, $cookieStore, toaster) {
        $scope.changePassword = function() {
            if ($scope.newPassword != $scope.confirmNewPassword) {
                toaster.pop('error', 'Password mis match', 'New Password and Confirm New password Must be same!');
            } else {
                var data = {
                    oldPassword: $scope.oldPassword,
                    newPassword: $scope.newPassword,
                    confirmNewPassword: $scope.confirmNewPassword
                };
                $http({
                    method: 'POST',
                    url: '/user/changepassword',
                    data: data
                }).then(function successCallback(response) {
                    toaster.pop('success', 'Password', 'Your Password has been changed.');
                    $state.go('users.dashboard');
                }, function errorCallback(error) {
                    toaster.pop('error', 'Password', error.data.message);
                });
            }
        };
    }
]);

app.controller('changePasswordControllerDistrict', ['$scope', '$http', '$translate', '$localStorage', '$cookieStore', 'toaster', '$state',
    function($scope, $http, $translate, $localStorage, $cookieStore, toaster, $state) {
        $scope.changePassword = function() {
            if ($scope.newPassword != $scope.confirmNewPassword) {
                toaster.pop('error', 'Password mis match', 'New Password and Confirm New password Must be same!');
            } else {
                var data = {
                    oldPassword: $scope.oldPassword,
                    newPassword: $scope.newPassword,
                    confirmNewPassword: $scope.confirmNewPassword
                };
                $http({
                    method: 'POST',
                    url: '/user/changepassword',
                    data: data
                }).then(function successCallback(response) {
                    toaster.pop('success', 'Password', 'Your Password has been changed.');
                    $state.go('districts.dashboard');
                }, function errorCallback(error) {
                    toaster.pop('error', 'Password', error.data.message);
                });
            }
        };
    }
]);

app.controller('changePasswordControllerSchool', ['$scope', '$http', '$translate', '$localStorage', '$cookieStore', 'toaster', '$state',
    function($scope, $http, $translate, $localStorage, $cookieStore, toaster, $state) {
        $scope.changePassword = function() {
            if ($scope.newPassword != $scope.confirmNewPassword) {
                toaster.pop('error', 'Password mis match', 'New Password and Confirm New password Must be same!');
            } else {
                var data = {
                    oldPassword: $scope.oldPassword,
                    newPassword: $scope.newPassword,
                    confirmNewPassword: $scope.confirmNewPassword
                };
                $http({
                    method: 'POST',
                    url: '/user/changepassword',
                    data: data
                }).then(function successCallback(response) {
                    toaster.pop('success', 'Password', 'Your Password has been changed.');
                    $state.go('schools.dashboard');
                }, function errorCallback(error) {
                    toaster.pop('error', 'Password', error.data.message);
                });
            }
        };
    }
]);

app.controller('editDistrictAdminController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        $scope.getDistrictAdmin = function() {
            $http({
                method: 'GET',
                url: '/user/edit?id=' + $cookieStore.get("loggedInDistrict").authorized_user_id,
            }).then(function successCallback(response) {
                $scope.user = response.data;
            }, function errorCallback(error) {
                console.log(error.data.message);
            });
        };
        $scope.updateDistrictAdmin = function() {
            var data = $scope.user;
            data.profile_image = $scope.profile_image;
            $http({
                method: 'POST',
                url: '/user/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Success', 'User updated.');
                $state.go('districts.dashboard');
            }, function errorCallback(error) {
                toaster.pop('error', 'Success', error.data.message);
            });
        };

        $scope.uploadFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected !');
            Loading(true);
            Upload.upload({
                url: '/user/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.profile_image = success.data;
                    $scope.saveDisable = true;
                },
                function(error) {
                    toaster.pop('error', "Error", error.data.message);
                    $scope.saveDisable = false;
                });
        };

        $scope.cancel = function() {
            $state.go('districts.dashboard');
        };
    }
]);

app.controller('editSchoolAdminController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        $scope.getSchoolAdmin = function() {
            $http({
                method: 'GET',
                url: '/user/edit?id=' + $cookieStore.get("loggedInSchool").authorized_user_id,
            }).then(function successCallback(response) {
                $scope.user = response.data;
            }, function errorCallback(error) {
                console.log(error.data.message);
            });
        };
        $scope.updateSchoolAdmin = function() {
            var data = $scope.user;
            data.profile_image = $scope.profile_image;
            $http({
                method: 'POST',
                url: '/user/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Success', 'User updated.');
                $state.go('schools.dashboard');
            }, function errorCallback(error) {
                toaster.pop('error', 'Success', error.data.message);
            });
        };

        $scope.uploadFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected !');
            Loading(true);
            Upload.upload({
                url: '/user/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.profile_image = success.data;
                    $scope.saveDisable = true;
                },
                function(error) {
                    toaster.pop('error', "Error", error.data.message);
                    $scope.saveDisable = false;
                });
        };

        $scope.cancel = function() {
            $state.go('schools.dashboard');
        };
    }
]);

app.controller('liveBlogController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        $scope.getBlogs = function() {
            $http({
                method: 'POST',
                url: '/blog/getAllBlogs'
            }).then(function successCallback(response) {
                var blogs = response.data;
                $scope.blogs = blogs;
                console.log(blogs);
            }, function errorCallback(error) {
                toaster.pop('error', 'Blogs', 'cannot get all the blogs! try reloading page.');
            });
        };
    }
]);
