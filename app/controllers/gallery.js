var db = require('orm').db,
    Gallery = db.models.gallery;

exports.register = function(req, res) {
    newGallery = new Gallery();
    newGallery.school_id = req.body.school_id;
    newGallery.event_id = req.body.event_id;
    newGallery.gallery_title = req.body.gallery_title;
    newGallery.status = (typeof req.body.status != 'undefined') ? req.body.status : '1';
    newGallery.save(function(err, created_gallery) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(created_gallery);
    });
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Gallery id required!!');
    }
    Gallery.find({
        id: req.body.id
    }, 1, function(err, gallery) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        if (!gallery.length) {
            return sendError(req, res, 404, 'no gallery found');
        }
        gallery[0].school_id = req.body.school_id;
        gallery[0].event_id = req.body.event_id;
        gallery[0].gallery_title = req.body.gallery_title;
        gallery[0].status = (typeof req.body.status != 'undefined') ? req.body.status : gallery[0].status;
        gallery[0].save(function(err, updated_gallery) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, 'please check your parameters');
            }
            return res.send(updated_gallery);
        });
    });
};

exports.active = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Gallery id required!!');
    }
    Gallery.find({
        id: req.body.id
    }, 1, function(err, gallery) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        gallery[0].status = "1";
        gallery[0].save(function(err, activated_gallery) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(activated_gallery);
        });
    });
};

exports.inactive = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Gallery id required!!');
    }
    Gallery.find({
        id: req.body.id
    }, 1, function(err, gallery) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        gallery[0].status = "0";
        gallery[0].save(function(err, inactivated_gallery) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(inactivated_gallery);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Gallery id required!!');
    }
    Gallery.find({
        id: req.body.id
    }, 1, function(err, gallery) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        gallery[0].remove(function(err) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, "Bad Request check your parameters");
            }
            return res.send(gallery[0]);
        });
    });
};

exports.getGallery = function(req, res) {
    if (typeof req.query.id == 'undefined') return sendError(req, res, 401, "ID required field");
    Gallery.find({
        id: req.query.id
    }, 1, function(err, galleries) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        if (!galleries.length) return sendError(req, res, 404, "Gallery not found");
        return res.send(galleries[0]);
    });
};




exports.getAllDistrictEventsGalleries = function(req, res) {
    if (typeof req.body.district_id == 'undefined' || req.body.district_id == 'null') {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = "SELECT t1.*,t2.title,t3.school_name FROM gallery as t1 LEFT JOIN events as t2 ON t1.event_id = t2.id LEFT JOIN schools as t3 ON t1.school_id = t3.id WHERE t3.parent_id = ?";
    db.driver.execQuery(query_str, [req.body.district_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getAllSchoolEventsGalleries = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id == 'null') {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = "SELECT t1.*,t2.title,t3.school_name FROM gallery as t1 LEFT JOIN events as t2 ON t1.event_id = t2.id LEFT JOIN schools as t3 ON t1.school_id = t3.id WHERE t3.id = ?";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.listAllStudentevents = function(req, res) {
    if (typeof req.body.district_id == 'undefined' || typeof req.body.district_id === null) {
        return sendError(req, res, 422, "district id required");
    }
    var query_str = 'SELECT t1.id as gallery_id,t1.gallery_title, t2.title,t2.start_date,t2.end_date,t2.address ,t3.school_name FROM `gallery` as t1 LEFT JOIN events as t2 ON t1.event_id = t2.id LEFT JOIN schools as t3 ON t1.school_id = t3.id WHERE t3.parent_id = ?';
    db.driver.execQuery(query_str, [req.body.district_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

var fs = require('fs');
exports.uploadFile = function(req, res) {
    var fs = require('fs');
    var galleryImages = __dirname + "/../../public/upload/galleryImages/";
    if (!fs.existsSync(galleryImages)) {
        fs.mkdirSync(galleryImages);
    }
    var file = req.files.file;
    var fileExtension = file.name.split(".")[1];
    if (fileExtension) {
      fileName = file.path.split('/');
      fs.rename(file.path, galleryImages + fileName[fileName.length - 1] +"."+ fileExtension);
      return res.send(fileName[fileName.length - 1] +"."+ fileExtension);
    }
    else {
      return sendError(req, res, 400, "Bad Request check your parameters");
    }

};
