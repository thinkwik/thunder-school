app.controller('schoolFeeListController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
	function($scope, $http, $state, toaster, $cookieStore) {
		$scope.createFeeRedirect = function() {
			$state.go('schools.school_fees_create');
		};
		$scope.getAllSchoolFees = function() {
			var data = {
				school_id: $cookieStore.get('loggedInSchool').id
			};
			$http({
				method: 'POST',
				url: '/fee/getAllSchoolFees',
				data: data
			}).then(function successCallback(response) {
				$scope.fees = response.data;
			}, function errorCallback(error) {
				toaster.pop('error', 'Fee', error.data.message);
			});
		};
		$scope.deleteFee = function(fee_id) {
			var data = {
				id: fee_id
			};
			$http({
				method: 'POST',
				url: '/fee/delete',
				data: data
			}).then(function successCallback(response) {
				$scope.getAllSchoolFees();
			}, function errorCallback(error) {
				toaster.pop('error', 'Fee', error.data.message);
			});
		};
		$scope.editFee = function(fee_id) {
			$state.go('schools.school_fees_edit', {
				fee_id: fee_id
			});
		};
	}
]);

app.controller('schoolFeeCreateController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
	function($scope, $http, $state, toaster, $cookieStore) {
		$scope.getAllSchoolGrades = function(school_id) {
			var data = {
				school_id: $cookieStore.get('loggedInSchool').id
			};
			$http({
				method: 'POST',
				url: '/grade/getAllSchoolGrades',
				data: data
			}).then(function successCallback(response) {
				$scope.grades = response.data;
			}, function errorCallback(error) {
				toaster.pop('error', 'Grade', error.data.message);
			});
		};
		$scope.createNewFee = function() {
			var data = $scope.fee;
			data.school_id = $cookieStore.get('loggedInSchool').id;
			$http({
				method: 'POST',
				url: '/fee/register',
				data: data
			}).then(function successCallback(response) {
				toaster.pop('success', 'New Fee', 'added successfully.');
				$state.go('schools.school_fees_list');
			}, function errorCallback(error) {
				toaster.pop('error', 'Grade', error.data.message);
			});
		};
		$scope.cancelFee = function() {
			$state.go('schools.school_fees_list');
		};
	}
]);

app.controller('schoolFeeEditController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
	function($scope, $http, $state, toaster, $cookieStore) {
		$scope.cancelFee = function() {
			$state.go('schools.school_fees_list');
		};
		$scope.editSaveFee = function() {
			var data = $scope.fee;
			$http({
				method: 'POST',
				url: '/fee/edit',
				data: data
			}).then(function successCallback(response) {
				toaster.pop('success', 'New Fee', 'Updated successfully.');
				$state.go('schools.school_fees_list');
			}, function errorCallback(error) {
				toaster.pop('error', 'Fee', error.data.message);
			});
		};
		$scope.getFeeForEdit = function() {
			$http({
				method: 'GET',
				url: '/fee/edit?id=' + $state.params.fee_id
			}).then(function successCallback(response1) {
				var data2 = {
					school_id: response1.data.school_id
				};
				$http({
					method: 'POST',
					url: '/grade/getAllSchoolGrades',
					data: data2
				}).then(function successCallback(response2) {
					$scope.grades = response2.data;
					$scope.fee = response1.data;
					Loading(true);
					setTimeout(function() {
						$scope.fee = response1.data;
					}, 2000);
					Loading(false);
				}, function errorCallback(error) {
					toaster.pop('error', 'Grade', error.data.message);
				});
			}, function errorCallback(error) {
				toaster.pop('error', 'Fee', error.data.message);
			});
		};
	}
]);