var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Homeworks = db.define('homeworks', {
    school_id: {
        type: 'integer'
    },
    staff_id: {
        type: 'integer'
    },
    grade_id: {
        type: 'integer'
    },
    class_id: {
        type: 'integer'
    },
    subject_id: {
        type: 'integer'
    },
    title: String,
    description: String,
    due_date: String,
    file: String,
    type: String,
    start_time: String,
    end_time: String,
    priority: String,
}, {
    timestamp: true,
    methods: {

    }
});
