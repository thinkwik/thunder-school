var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var SchoolRole = db.define('school_roles', {
    role_name: String,
    school_id: {
        type: 'integer'
    },
    description: String,
    modules:String,
    status: ['0', '1']
}, {
    timestamp: true,
    methods: {

    }
});