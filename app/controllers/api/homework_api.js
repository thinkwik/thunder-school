var bCrypt = require('bcrypt-nodejs');
var db = require('orm').db,
    Homework = db.models.homeworks,
    HomeworkReports = db.models.homework_reports;
var fs = require('fs');

exports.register = function(req, res) {
    newHomework = new Homework();
    newHomework.title = req.body.title;
    newHomework.due_date = req.body.due_date;
    newHomework.school_id = req.body.school_id;
    newHomework.staff_id = req.api_token_data.id;
    newHomework.grade_id = req.body.grade_id;
    newHomework.subject_id = req.body.subject_id;
    newHomework.class_id = req.body.class_id;
    newHomework.start_time = req.body.start_time;
    newHomework.end_time = req.body.end_time;
    newHomework.description = req.body.description;
    var homework_file = null;
    if (req.files.homework_file != 'undefined') {
      if (req.files.homework_file == 'undefined') {
        return apiSendError(req, res, 400, "Bad Request check your parameters");
      }else {
        var file = req.files.homework_file;
        fileName = file.path.split('/');
        type = (typeof file.name != 'undefined' && file.name != "") ? "." + file.name.split('.')[1] : "";
        fs.rename(file.path, __dirname + "/../../../public/upload/homeworks/" + fileName[fileName.length - 1] + type, function(err) {
            if (err) {
                console.log(err);
                var error_message = (err.message.split(':')[1]) ? err.message.split(':')[1] : "Somthing went wrong!";
                return apiSendError(req, res, 422, error_message);
            }
        });
        homework_file = fileName[fileName.length - 1] + type;
      }
    }
    newHomework.file = homework_file;
    newHomework.type = req.body.type;
    newHomework.priority = req.body.priority;
    newHomework.state = req.body.state;
    newHomework.save(function(err, created_homework) {
        if (err) {
            console.log(err);            
            return apiSendError(req, res, 422, "Somthing went wrong while creating homework");
        }
        var query_str = 'SELECT t1.student_id as id FROM student_classes as t1  WHERE t1.grade_id = ? AND t1.class_id = ?';
        db.driver.execQuery(query_str, [created_homework.grade_id, created_homework.class_id], function(err, all_students) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 400, "Bad Request check your parameters");
            }
            if (all_students.length > 0) {
                all_students.forEach(function(value, key) {
                    var data = {
                        homework_id: created_homework.id,
                        student_id: value.id,
                        achieved_grade: null,
                        file_path: null,
                        status: "0"
                    };
                    HomeworkReports.create(data, function(err) {
                        if (err) {
                            console.log(err);
                            return apiSendError(req, res, 500, "Something went wrong!");
                        }
                    });
                });
                var response = {};
                response['data'] = [];
                response['code'] = 200;
                return res.send(response);
            } else {
                var response = {};
                response['data'] = [];
                response['code'] = 200;
                return res.send(response);
            }
        });
    });
};

exports.apiGetHomeworkListStaff = function(req, res) {
    if (req.body.id) {
        var query_str = `SELECT t1.*,t2.grade,CONCAT(t3.first_name," ",t3.last_name) as staff_name,t6.subject_name
                      FROM homeworks as t1
                      LEFT JOIN grades as t2 ON t1.grade_id = t2.id
                      LEFT JOIN classes as t5 ON t1.class_id = t5.id
                      LEFT JOIN subjects as t6 ON t1.subject_id = t6.id
                      LEFT JOIN staff as t3 ON t1.staff_id = t3.id
                      WHERE t3.id = ? ORDER BY t1.due_date DESC`;
        db.driver.execQuery(query_str, [req.body.id], function(err, rows) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 400, "Bad Request check your parameters");
            } console.log(rows);
            var response = {};
            response['data'] = rows;
            response['code'] = 200;
            return res.send(response);
        });
    } else {
        return apiSendError(req, res, 400, "Bad Request check your parameters");
    }
};

exports.apiGetAllStudentList = function(req, res) {
    if (typeof req.body.homework_id == 'undefined') {
        return apiSendError(req, res, 422, "Homework ID required");
    }
    var query_str = `SELECT t1.comment,t1.achieved_grade,t1.status,t1.file_path as studentUploadedFile,t1.created_at,
                     t1.homework_id,t1.student_id as id,
                     CONCAT(t2.first_name, " ", t2.last_name) as student_name,t2.image as studentPrfileImage
                     FROM homework_reports as t1
                     LEFT JOIN admissions as t2 ON t1.student_id = t2.id
                     WHERE t1.homework_id = ? ORDER BY t1.created_at DESC `;
    db.driver.execQuery(query_str, [req.body.homework_id], function(err, rows) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 400, "Bad Request check your parameters");
        }
        var response = {};
        response['data'] = rows;
        response['code'] = 200;
        return res.send(response);
    });
};

exports.complete = function(req, res) {
    if (req.body.homework_report_id !== 'undefined' && req.body.homework_report_id) {
        HomeworkReports.find({
            id: req.body.homework_report_id
        }, 1, function(err, reports) {
            if (err && !reports) {
                console.log(err);
                return apiSendError(req, res, 400, "Bad Request check your parameters");
            }
            if (reports.length === 0) {
                return apiSendError(req, res, 400, 'HomeworkReports id not found!');
            }
            reports[0].status = (reports[0].status == "0") ? "1" : "0";
            reports[0].save(function(err, saved_report) {
                if (err) {
                    console.log(err);
                    return apiSendError(req, res, 422, 'HomeworkReports id required!');
                }
                var response = {};
                response['data'] = [];
                response['code'] = 200;
                return res.send(response);
            });
        });
    } else {
        return apiSendError(req, res, 422, 'HomeworkReports id required!!');
    }
};

exports.apiGetHomeworkStudentWise = function(req, res) {
    if (typeof req.body.student_id == 'undefined' || req.body.student_id === null) {
        return apiSendError(req, res, 422, "student ID required");
    }
    var query_str = `SELECT t1.achieved_grade, t1.comment, t1.file_path as studentUploadedHomework,t1.status,
                    t2.subject_id,t2.class_id,t2.title as homework_title,t2.description as homework_description,t2.file as staffUploadedHomework,
                    t2.due_date,CONCAT(t3.first_name," ",t3.last_name) as staff_name,
                    t4.subject_name,t5.class_name
                    FROM homework_reports as t1
                    LEFT JOIN homeworks as t2 ON t1.homework_id = t2.id
                    LEFT JOIN staff as t3 ON t2.staff_id = t3.id
                    LEFT JOIN subjects as t4 ON t2.subject_id = t4.id
                    LEFT JOIN classes as t5 ON t2.class_id = t5.id
                    WHERE t1.student_id = ? ORDER BY t2.due_date DESC`;
    db.driver.execQuery(query_str, [req.body.student_id], function(err, rows) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!rows.length) {
            return apiSendError(req, res, 404, "Reports not found!");
        }
        var response = {};
        response['data'] = rows;
        response['code'] = 200;
        return res.send(response);
    });
};

exports.apiEditHomework = function(req, res) {
    if (req.body.homework_id && req.body.title && req.body.description && req.body.due_date ) {
        Homework.find({
            id: req.body.homework_id
        }, 1, function(err, homeworks) {
            if (err) {
                console.log(err);
                var error_message = (err.message.split(':')[1]) ? err.message.split(':')[1] : "Somthing went wrong!";
                return apiSendError(req, res, 422, error_message);
            }
            homeworks[0].title = req.body.title;
            homeworks[0].description = req.body.description;
            homeworks[0].due_date = req.body.due_date;
            var homework_file = null;
            if (req.files) {
              if (!req.files.homework_file) {
                return apiSendError(req, res, 400, "Bad Request check your parameters");
              }else {
                var file = req.files.homework_file;
                fileName = file.path.split('/');
                type = (typeof file.name != 'undefined' && file.name != "") ? "." + file.name.split('.')[1] : "";
                fs.rename(file.path, __dirname + "/../../../public/upload/homeworks/" + fileName[fileName.length - 1] + type, function(err) {
                    if (err) {
                        console.log(err);
                        var error_message = (err.message.split(':')[1]) ? err.message.split(':')[1] : "Somthing went wrong!";
                        return apiSendError(req, res, 422, error_message);
                    }
                });
                homework_file = fileName[fileName.length - 1] + type;
              }
            }
            homeworks[0].file = (homework_file) ? homework_file : homeworks[0].file;
            homeworks[0].start_time = req.body.start_time;
            homeworks[0].end_time = req.body.end_time;
            homeworks[0].priority = (req.body.priority) ? req.body.priority : homeworks[0].priority;
            homeworks[0].save(function(err, updated_homework) {
                if (err) {
                    console.log(err);
                    return apiSendError(req, res, 400, "Bad Request check your parameters");
                }
                var response = {};
                response['data'] = [];
                response['code'] = 200;
                return res.send(response);
            });
        });
    } else {
        return apiSendError(req, res, 400, "Bad Request check your parameters");
    }
};
exports.apiDeleteHomework = function(req, res) {
    if (typeof req.body.homework_id == 'undefined') {
        return apiSendError(req, res, 404, 'Homeworks id required!!');
    }
    Homework.find({
        id: req.body.homework_id
    }, 1, function(err, homeworks) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 400, 'Bad Request check your parameters!!');
        }
        homeworks[0].remove(function(err) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 422, 'HomeworkReports id required!');
            }
            var response = {};
            response['data'] = [];
            response['code'] = 200;
            return res.send(response);
        });
    });
};

var fs = require('fs');
exports.uploadFile = function(req, res) {
    var homework_file = null;
    if (typeof req.body.student_list_homework_id != 'undefined' && typeof req.files.homework_file != 'undefined') {
        var file = req.files.homework_file;
        fileName = file.path.split('/');
        type = (typeof file.type != 'undefined' && file.type != "") ? "." + file.type.split('/')[1] : "";
        fs.rename(file.path, __dirname + "/../../../public/upload/homeworks/" + fileName[fileName.length - 1] + type, function(err) {
            if (err) {
                console.log(err);
                var error_message = (err.message.split(':')[1]) ? err.message.split(':')[1] : "Somthing went wrong!";
                return apiSendError(req, res, 422, error_message);
            }
        });
        homework_file = fileName[fileName.length - 1] + type;
        var query_str = "UPDATE `homework_reports` SET `status`= ? ,`file_path`= ? WHERE `id` = ?";
        db.driver.execQuery(query_str, ['1',homework_file,req.body.student_list_homework_id], function(err, rows) {
            if(err){
                console.log(err);
                return apiSendError(req, res, 500, 'Something went wrong!');
            }
            var response = {};
            response['data'] = [];
            response['code'] = 200;
            return res.send(response);
        });
    } else {
        return apiSendError(req, res, 422, 'student_list_homework_id and homework_file required');
    }
};


var fs = require('fs');
exports.uploadStudentHomeworkFile = function(req, res) {
    var fs = require('fs');
    var studenthomework = __dirname + "/../../../public/upload/studenthomeworks/";
    if (!fs.existsSync(studenthomework)) {
        fs.mkdirSync(studenthomework);
    }
    var file = req.files.file;
    var fileExtension = file.name.split(".")[1];
    if (fileExtension) {
        fileName = file.path.split('/');
        fs.rename(file.path, studenthomework + fileName[fileName.length - 1] + "." + fileExtension);
        var response = {};
        response['data'] = (fileName[fileName.length - 1] + "." + fileExtension);
        response['code'] = 200;
        return res.send(response);
    } else {
        return sendError(req, res, 400, "Bad Request check your parameters");
    }
};


exports.apiUpdateGrades = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return apiSendError(req, res, 404, 'HomeworkReports id required!!');
    }
    HomeworkReports.find({
        id: req.body.id
    }, function(err, grades_achieved) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 422, 'HomeworkReports id required!');
        }
        if (grades_achieved.length === 0) {
            return apiSendError(req, res, 422, 'Homework not found!');
        }
        grades_achieved[0].achieved_grade = req.body.achieved_grade;
        grades_achieved[0].save(function(err, saved_grades) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 422, 'HomeworkReports id required!');
            }
            var response = {};
            response['data'] = [];
            response['code'] = 200;
            return res.send(response);
        });
    });
};
