app.config(function($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider.otherwise("/student/login");
    //Manage Student Login
    $stateProvider.state('studentSignin', {
            url: "/student/login",
            templateUrl: "/partials/auth/studentSignin.html",
            controller: 'studentSigninFormController'
        })
        //Manage Profile
        .state('students', {
            url: '/student',
            templateUrl: '/partials/profile/students.html',
            abstract: true,
            controller: "protectStudentRoutesController"
        })
        .state('students.changeParentPassword', {
            url: '/changepassword',
            templateUrl: '/partials/students/changePassword.html'
        })
        .state('students.edit_profile', {
            url: '/edit_profile',
            templateUrl: '/partials/students/editProfile.html',
            controller: 'editStudentProfileController'
        })
        //Manage Dashboard
        .state('students.dashboard', {
            url: '/dashboard',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageStudents/manageDashboard/studentDashboard.html',
            controller: 'studentDashboardController'
        })
        //Manage Video Conference
        .state('students.video_conference', {
            url: '/video_conference/create',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageStudents/manageVideoConference/createConference.html',
            controller: 'studentCreateVideoConferenceController'
        })
        .state('students.video_conference_live', {
            url: '/video_conference/live',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageStudents/manageVideoConference/groupCall.html'
                //controller: 'videoConferenceLiveController'
        })
        //Manage Exam
        .state('students.take_exam', {
            url: '/exam/live/:exam_id',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageStudents/manageTakeExam/takeExam.html'
                //controller: 'takeExamController'
        })
        .state('students.exam_qa', {
            url: '/exam_qa',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageStudents/manageExams/exam_qa.html'
        })
        .state('students.view_exam_report', {
            url: '/exam/report_view/:exam_id/:student_id',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageStudents/manageExams/reportExam.html',
            controller: 'studentsExamReportController'
        })
        .state('students.exam_list', {
            url: '/exam/list',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageStudents/manageExams/listExams.html',
            controller: 'studentsExamListController'
        })
        //Manage Exam Results
        .state('students.exam_result_list', {
            url: '/exam_result/list',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageStudents/manageExams/listExamResults.html',
            controller: 'studentsResultListStaffController'
        })
        //Manage TO-DO
        .state('students.todo_create', {
            url: '/todo/create',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageStudents/manageTodo/createTodo.html',
            controller: 'studentTodoCreateController'
        })
        .state('students.todo_list', {
            url: '/todo/list',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageStudents/manageTodo/listTodo.html',
            controller: 'studentTodoListController'
        })

    // Manage Students Parents
    .state('students.parents_list', {
            url: '/parents/information',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageStudents/manageParents/prentsInformation.html',
            controller: 'studentParentsInformationController'
        })
        //Manage Attendance
        .state('students.attendance_list', {
            url: '/attendance/list',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageStudents/manageAttendance/listAttendance.html',
            controller: 'studentAttendenceListController'
        })
        // Manage Appointments
        .state('students.appointment_create', {
            url: '/appointment/create',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageStudents/manageAppointment/createAppointment.html',
            controller: 'createStudentAppointmentController'
        })
        .state('students.appointment_list', {
            url: '/appointment/list',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageStudents/manageAppointment/listAppointment.html',
            controller: 'listStudentAppointmentController'
        })
        .state('students.appointment_edit', {
            url: '/appointment/:appointment_id',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageStudents/manageAppointment/editappointment.html',
            controller: 'editStudentAppointmentController'
        })
        // Manage  Issues
        .state('students.issues_create', {
            url: '/issues/create',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageStudents/manageIssues/createIssues.html',
            controller: 'createStudentIssuesController'
        })
        .state('students.issues_list', {
            url: '/issues/list',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageStudents/manageIssues/listIssues.html',
            controller: 'listStudentIssuesController'
        })
        // .state('students.issues_edit', {
        //     url: '/issues/:issue_id',
        //     templateUrl: '/partials/superAdmin/district/manageStaff/manageStudents/manageIssues/editIssues.html',
        //     controller: 'editStudentIssuesController'
        // })
        .state('students.issues_view', {
            url: '/issues/view/:issue_id',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageStudents/manageIssues/viewIssue.html',
            controller: 'viewStudentIssuesController'
        })
        //Manage Events
        .state('students.event_list', {
            url: '/event/list',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageStudents/manageEvents/listEvent.html',
            controller: 'studentListEventsController'
        })
        .state('students.event_gallery_view', {
            url: '/gallery/view/:gallery_id',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageStudents/manageEvents/eventGallery.html',
            controller: 'viewEventGalleryAlbumController'
        })
        //Manage Emails
        .state('students.email_list', {
            url: '/email/list',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageStudents/manageEmails/manageEmails.html',
            controller: 'listStudentEmailController'
        })
        .state('students.email_view', {
            url: '/email/view/:mail_id',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageStudents/manageEmails/viewEmail.html',
            controller: 'viewStudentEmailController'
        })
        //Manage Materials
        .state('students.material_list', {
            url: '/material/list',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageStudents/manageMaterials/listmaterial.html',
            controller: 'listStudentMaterialController'
        })
        //Manage Assignment
        .state('students.assignment_list', {
            url: '/assignment/list',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageStudents/manageAssignnments/listAssignment.html',
            controller: 'listStudentAssignmentController'
        })
        //Manage Assignment Report
        .state('students.assignment_reports', {
            url: '/assignment/report/:assignment_id',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageStudents/manageAssignnments/assignmentReports.html',
            controller: 'studentAssignmentReportsController'
        })
        // Manage Students Notices
        .state('students.notices_list', {
            url: '/notices/list',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageStudents/manageNotices/listNotice.html',
            controller: 'listStudentsNoticesController'
        })
        //Manage Homework
        .state('students.list_homework', {
            url: '/list/homework',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageStudents/manageHomework/listHomework.html',
            controller: 'listStudentHomeworkController'
        })
        //Manage Homework Report
        .state('students.homework_reports', {
            url: '/homework/report/:homework_id',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageStudents/manageHomework/homeworkReports.html',
            controller: 'studentHomeworkReportsController'
        })
        //View Classes/Staffs
        .state('students.view_staff', {
            url: '/staff/list',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageStudents/viewStaffList/studentViewStaffList.html',
            controller: 'studentStaffListController'
        })
        //Manage Books
        .state('students.list_books', {
            url: '/list/books',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageStudents/manageBooks/listBooks.html',
            controller: 'listStudentBooksController'
        })
        .state('students.view_books', {
            url: '/books/view/:book_id',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageStudents/manageBooks/viewBooks.html',
            controller: 'viewStudentBooksController'
        })
        .state('students.view_epub_book', {
            url: '/bookstore/epub/view/:book_id',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageStudents/manageBooks/viewEpubBook.html',
            controller: 'viewStudentEpubBookStoreController'
        })

        //Manage Student Signout
        .state('studentSignout', {
            url: "/student/signout",
            templateUrl: "/partials/auth/signout/studentSignout.html",
            controller: 'studentLogoutController'
        })

    $locationProvider.html5Mode(true);
});
