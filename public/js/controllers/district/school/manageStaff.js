app.controller('schoolCreateStaffController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        // School Session needs to be changed/ask

        $scope.getSchoolSessions = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/session/getAllSchoolSessions',
                data: data
            }).then(function successCallback(response) {
                $scope.Sessions = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'School', 'Something went wrong!');
            });
        };

        $scope.getSchoolDepartmentsDesignation = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/school/department/getAllSchoolDepartment',
                data: data
            }).then(function successCallback(response) {
                $scope.Departments = response.data;
                $scope.getStaffRoles();
                $scope.getSchoolDesignations();
                $scope.getSchoolSessions();
                $scope.getAllInfo();
            }, function errorCallback(error) {
                toaster.pop('error', 'School', 'Something went wrong!');
            });
        };

        $scope.getSchoolDesignations = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/designation/getAllSchoolDesignations',
                data: data
            }).then(function successCallback(response) {
                $scope.Designations = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'School', 'Something went wrong!');
            });
        };

        $scope.getStaffRoles = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/staff/role/getAllStaffRoleSchool',
                data: data
            }).then(function successCallback(response) {
                $scope.roles = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'School', 'Something went wrong!');
            });
        };

        $scope.getAllInfo = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/medical_info/getAllMedicalInfos',
                data: data
            }).then(function successCallback(response) {
                $scope.infos = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Medical Info', 'cannot get all the Staff! try reloading page.');
            });
        };

        $scope.addStaff = function() {
            $state.go('schools.staff_create');
        };

        $scope.cancelStaffCreate = function() {
            $state.go('schools.staff_list');
        };

        $scope.createStaff = function() {
            data = $scope.staff;
            data.school_id = $cookieStore.get('loggedInSchool').id;
            data.imagename = $scope.imagename;
            $http({
                method: "POST",
                url: "/staff/register",
                data: data
            }).then(function successCallback(response) {
                $scope.cancelStaffCreate();
                toaster.pop('success', 'Staff', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Staff', error.data.message);
            });
        };

        $scope.cancelStaffForm = function() {
            $state.go('schools.staff_list');
        };

        $scope.uploadFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected !');
            Loading(true);
            Upload.upload({
                url: '/staff/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.imagename = success.data;
                    $scope.saveDisable = true;
                },
                function(error) {
                    toaster.pop('error', "Error", error.data.message);
                    $scope.saveDisable = false;
                });
        };
    }
]);

app.controller('schoolListStaffController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.addNewStaff = function() {
            $state.go('schools.staff_create');
        };

        $scope.loginSchoolStaff = function(staff_id) {
            var data = {
                id: staff_id
            };
            $http({
                method: 'POST',
                url: '/staff/tryLogin',
                data: data
            }).then(function successCallback(response) {
                $cookieStore.put('loggedInStaff', response.data);
                window.open('/school/staff/dashboard', '_blank');
            }, function errorCallback(error) {
                toaster.pop('error', 'School Staff', error.data.message);
            });
        };

        $scope.inactiveStaff = function(staff_id) {
            var data = {
                id: staff_id
            };
            $http({
                method: 'POST',
                url: '/staff/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllStaff();
            }, function errorCallback(error) {
                toaster.pop('error', 'Staff', error.data.message);
            });
        };

        $scope.activeStaff = function(staff_id) {
            var data = {
                id: staff_id
            };
            $http({
                method: 'POST',
                url: '/staff/active',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllStaff();
            }, function errorCallback(error) {
                toaster.pop('error', 'Staff', error.data.message);
            });
        };

        $scope.deleteStaff = function(staff_id) {
            var data = {
                id: staff_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/staff/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllStaff();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Staff', error.data.message);
                });
            }
        };

        $scope.getAllStaff = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/staff/getSchoolsAllStaff',
                data: data
            }).then(function successCallback(response) {
                var staffs = response.data;
                $scope.staffs = staffs;
            }, function errorCallback(error) {
                toaster.pop('error', 'Staff', 'cannot get all the Staff! try reloading page.');
                console.log(error.data.message);
            });
        };

        $scope.editStaff = function(staff_id) {
            $state.go('schools.staff_edit', {
                staff_id: staff_id
            });
        };
    }
]);

app.controller('schoolEditStaffController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        $scope.getSchoolDesignations = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/designation/getAllSchoolDesignations',
                data: data
            }).then(function successCallback(response) {
                $scope.Designations = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'School', 'Something went wrong!');
            });
        };

        $scope.getAllInfo = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/medical_info/getAllMedicalInfos',
                data: data
            }).then(function successCallback(response) {
                $scope.infos = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Medical Info', 'cannot get all the Staff! try reloading page.');
            });
        };

        $scope.getSchoolDepartments = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/school/department/getAllSchoolDepartment',
                data: data
            }).then(function successCallback(response) {
                $scope.Departments = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'School', 'Something went wrong!');
            });
        };

        // School Session needs to be changed/ask
        //
        // $scope.getSchoolSessions = function() {
        //     var data = {
        //         school_id: $cookieStore.get('loggedInSchool').id
        //     };
        //     console.log(data);
        //     $http({
        //         method: 'POST',
        //         url: '/session/getAllSessions',
        //         data: data
        //     }).then(function successCallback(response) {
        //         $scope.Sessions = response.data;
        //     }, function errorCallback(error) {
        //         toaster.pop('error', 'School', 'Something went wrong!');
        //     });
        // };


        $scope.getStaffRoles = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/staff/role/getAllStaffRoleSchool',
                data: data
            }).then(function successCallback(response) {
                $scope.roles = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'School', 'Something went wrong!');
            });
        };

        $scope.getEditStaff = function() {
            $http({
                method: 'GET',
                url: '/staff/edit?id=' + $state.params.staff_id
            }).then(function successCallback(response) {
                $scope.staff = response.data;
                $scope.staff.medical_allergies = (response.data.medical_allergies != null && response.data.medical_allergies != "") ? response.data.medical_allergies.split(',') : [];
                $scope.staff.medical_immunizations = (response.data.medical_immunizations != null && response.data.medical_immunizations != "") ? response.data.medical_immunizations.split(',') : [];
                $scope.getSchoolDesignations();
                $scope.getSchoolDepartments();
                $scope.getStaffRoles();
                $scope.getAllInfo();
            }, function errorCallback(error) {
                toaster.pop('error', 'Staff', 'cannot get all the Staff! try reloading page.');
            });
        };

        $scope.saveEditStaff = function() {
            data = $scope.staff;
            console.log(data);
            if ($scope.imagename) {
                data.photo = $scope.imagename;
            }
            $http({
                method: 'POST',
                url: '/staff/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Staff', 'Saved Successfully.');
                $state.go('schools.staff_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'Staff', error.data.message);
            });
        };

        $scope.uploadFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected !');
            Loading(true);
            Upload.upload({
                url: '/staff/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.imagename = success.data;
                    $scope.saveDisable = true;
                },
                function(error) {
                    toaster.pop('error', "Error", error.data.message);
                    $scope.saveDisable = false;
                });
        };

        $scope.cancelStaffForm = function() {
            $state.go('schools.staff_list');
        };
    }
]);


//
