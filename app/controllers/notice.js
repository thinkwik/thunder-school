var db = require('orm').db,
    Notice = db.models.notices;
var sms = require('../../config/twilio_message.js');



exports.getAllNotices = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = `SELECT t1.*,t2.school_name
                    FROM notices as t1  
                    LEFT JOIN schools as t2 on t1.school_id = t2.id  
                    WHERE  t2.id = ? ORDER BY t1.created_at DESC`;
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Notice id required!!');
    }
    Notice.find({
        id: req.body.id
    }, 1, function(err, notices) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        notices[0].school_id = req.body.school_id;
        notices[0].school_auth_person_id = req.body.school_auth_person_id;
        notices[0].message = req.body.message;
        notices[0].send_notice_to = req.body.send_notice;
        notices[0].save(function(err, updated_notice) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(updated_notice);
        });
    });
};

exports.active = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Notice id required!!');
    }
    Notice.find({
        id: req.body.id
    }, 1, function(err, notice) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        notice[0].status = "1";
        notice[0].save(function(err, activated_notice) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(activated_notice);
        });
    });
};

exports.inactive = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Notice id required!!');
    }
    Notice.find({
        id: req.body.id
    }, 1, function(err, notice) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        notice[0].status = "0";
        notice[0].save(function(err, inactivated_notice) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(inactivated_notice);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Notice id required!!');
    }
    Notice.find({
        id: req.body.id
    }, 1, function(err, notices) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        notices[0].remove(function(err) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(notices[0]);
        });
    });
};

exports.getNotice = function(req, res) {
    if (typeof req.query.id == 'undefined') return sendError(req, res, 401, "ID required field");
    Notice.find({
        id: req.query.id
    }, 1, function(err, notices) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        if (!notices.length) return sendError(req, res, 404, "Notice not found");
        return res.send(notices[0]);
    });
};


exports.register_email = function(req, res) {
    newNotice = new Notice();
    newNotice.school_id = req.body.school_id;
    newNotice.school_auth_person_id = req.body.school_auth_person_id;
    newNotice.message = req.body.message;
    newNotice.send_notice_to = req.body.send_notice;
    var type = [];
    var arr = req.body.send_notice.split(',');
    if (arr.length <= 0 || req.body.send_notice == '') {
        return sendError(req, res, 422, "Please check your parameters!");
    } else if (arr.length <= 1) {
        type.push(req.body.send_notice);
        type.push(req.body.send_notice);
        type.push(req.body.send_notice);
    } else if (arr.length <= 2) {
        type.push(arr[0]);
        type.push(arr[1]);
        type.push(arr[0]);
    } else if (arr.length <= 3) {
        type.push(arr[0]);
        type.push(arr[1]);
        type.push(arr[2]);
    }
    var grade_id = "";
    if (req.body.grade_id !== undefined && req.body.grade_id != "") {
        grade_id = req.body.grade_id;
    }
    var gender = "";
    if (req.body.gender !== undefined && req.body.gender != "") {
        gender = req.body.gender;
    }
    var class_id = "";
    if (req.body.class_id !== undefined && req.body.class_id != "") {
        class_id = req.body.class_id;
    }
    newNotice.save(function(err, created_notice) {
        if (err) {
            return sendError(req, res, 500, "Something went wrong!");
        }

        send_notice_email(req, res, req.body.school_id, type, req.body.message, grade_id, class_id, gender, function(err, users) {
            if (err) {
                return sendError(req, res, 500, "Something went wrong!");
            }
            return res.send(created_notice);
        })
    });
};

exports.register_sms = function(req, res) {
    newNotice = new Notice();
    newNotice.school_id = req.body.school_id;
    newNotice.school_auth_person_id = req.body.school_auth_person_id;
    newNotice.message = req.body.message;
    newNotice.send_notice_to = req.body.send_notice;
    var type = [];
    var arr = req.body.send_notice.split(',');
    if (arr.length <= 0 || req.body.send_notice == '') {
        return sendError(req, res, 422, "Please check your parameters!");
    } else if (arr.length <= 1) {
        type.push(req.body.send_notice);
        type.push(req.body.send_notice);
        type.push(req.body.send_notice);
    } else if (arr.length <= 2) {
        type.push(arr[0]);
        type.push(arr[1]);
        type.push(arr[0]);
    } else if (arr.length <= 3) {
        type.push(arr[0]);
        type.push(arr[1]);
        type.push(arr[2]);
    }
    var grade_id = "";
    if (req.body.grade_id !== undefined && req.body.grade_id != "") {
        grade_id = req.body.grade_id;
    }
    var gender = "";
    if (req.body.gender !== undefined && req.body.gender != "") {
        gender = req.body.gender;
    }
    var class_id = "";
    if (req.body.class_id !== undefined && req.body.class_id != "") {
        class_id = req.body.class_id;
    }
    newNotice.save(function(err, created_notice) {
        if (err) {
            console.log(err);
            return sendError(req, res, 500, "Something went wrong!");
        }

        send_notice_sms(req, res, req.body.school_id, type, req.body.message, grade_id, class_id, gender, function(err, users) {
            if (err) {
                return sendError(req, res, 500, "Something went wrong!");
            }
            return res.send(created_notice);
        })
    });
};

send_notice_sms = function(req, res, school_id, type, message, grade_id, class_id, gender, cb) {
    var query_str = `SELECT *
                    FROM (
                        SELECT
                            CONCAT(t1.id) as id,
                            CONCAT(t1.first_name) as first_name,
                            CONCAT(t1.last_name) as last_name,
                            CONCAT(t1.first_name," ",t1.last_name) as name,
                            "staff" as type,
                            t1.school_id,
                            t1.phonenumber as mobile_number,
                            0 as grade_id,
                            t1.gender,
                            t1.email_id as email,
                            "" as grade_name,
                            t2.school_name,
                            t3.session_name,
                            "0" as total_marks,
                            "0" as achived_marks,
                            "0" as achived_grade,
                            "" as child_name,
                            "" as classes,
                            "" as classes_name
                          FROM staff as t1
                          LEFT JOIN schools as t2 ON t1.school_id = t2.id
                          LEFT JOIN sessions as t3 ON t1.session_id = t3.id
                          UNION
                          SELECT
                            CONCAT(t1.id) as id,
                            CONCAT(t1.first_name) as first_name,
                            CONCAT(t1.last_name) as last_name,
                            CONCAT(t1.first_name," ",t1.last_name) as name,
                            "student" as type,
                            t1.school_id,
                            t1.mobile_number as mobile_number,
                            t1.grade_id,
                            t1.gender,
                            t1.email,
                            t3.grade as grade_name,
                            t2.school_name,
                            t4.session_name,
                            COALESCE(SUM(t5.total_marks),0) as total_marks,
                            COALESCE(SUM(t5.achived_marks),0) as achived_marks,
                            COALESCE(SUM(t5.achived_grade),0) as achived_grade,
                            "" as child_name,
                            GROUP_CONCAT(t6.class_id) as classes,
                            GROUP_CONCAT(t7.class_name) as classes_name
                          FROM admissions as t1
                          LEFT JOIN schools as t2 ON t1.school_id = t2.id
                          LEFT JOIN grades as t3 ON t1.grade_id = t3.id
                          LEFT JOIN sessions as t4 ON t1.session_id = t4.id
                          LEFT JOIN exam_results as t5 ON t5.student_id = t1.id
                          LEFT JOIN student_classes as t6 ON t6.student_id = t1.id
                          LEFT JOIN classes as t7 ON t6.class_id = t7.id
                          GROUP BY t1.id
                          UNION
                          SELECT
                            CONCAT(t1.id) as id,
                            CONCAT(t1.full_name) as first_name,
                            "" as last_name,
                            CONCAT(t1.full_name) as name,
                            "parents" as type,
                            t2.school_id as school_id,
                            t1.mobile as mobile_number,
                            t2.grade_id as grade_id,
                            t1.gender,
                            t1.email,
                            t5.grade as grade_name,
                            t3.school_name,
                            t4.session_name,
                            COALESCE(SUM(t6.total_marks),0) as total_marks,
                            COALESCE(SUM(t6.achived_marks),0) as achived_marks,
                            COALESCE(SUM(t6.achived_grade),0) as achived_grade,
                            CONCAT(t2.first_name," ",t2.last_name) as child_name,
                            GROUP_CONCAT(t7.class_id) as classes,
                            GROUP_CONCAT(t8.class_name) as classes_name
                          FROM parents as t1
                          LEFT JOIN admissions as t2 ON t1.student_id = t2.id
                          LEFT JOIN schools as t3 ON t2.school_id = t3.id
                          LEFT JOIN sessions as t4 ON t2.session_id = t4.id
                          LEFT JOIN grades as t5 ON t2.grade_id = t5.id
                          LEFT JOIN exam_results as t6 ON t6.student_id = t2.id
                          LEFT JOIN student_classes as t7 ON t7.student_id = t2.id
                          LEFT JOIN classes as t8 ON t7.class_id = t8.id
                          GROUP BY t2.id
                          ) as final
                    WHERE final.school_id = ? AND (final.type IN (?) OR final.type IN (?) OR final.type IN (?)) `;
    db.driver.execQuery(query_str, [school_id, type[0], type[1], type[2]], function(err, rows) {
        if (err) {
            cb(err, null);
        }
        console.log(rows);
        if (rows.length == 0) {
            cb(null, true);
        }
        var loopCount = 0;
        rows.forEach(function(user) {
            if (user.mobile_number != '' && user.mobile_number != null && user.mobile_number.length > 9) {
                var flag = true;
                if (grade_id != "") {
                    flag = false;
                    if ((user.type == 'staff') || ((user.type == 'student' || user.type == 'parents') && user.grade_id == grade_id)) {
                        flag == true;
                    }
                }
                if (class_id != "" && class_id != null) {
                    flag = false;
                    if ((user.type == 'staff') || ((user.type == 'student' || user.type == 'parents') && user.classes != null && user.classes.indexOf(class_id) != -1 )) {
                        flag == true;
                    }
                }
                if (gender != "" && user.gender != gender) {
                    flag = false;
                }
                loopCount++;
                if (flag) {
                    /*message dynamic change*/
                    var dy_message = message;
                    dy_message = dy_message.replace("<<FIRST_NAME>>", user.first_name);
                    dy_message = dy_message.replace("<<LAST_NAME>>", user.last_name);
                    dy_message = dy_message.replace("<<FULL_NAME>>", user.name);
                    dy_message = dy_message.replace("<<EMAIL>>", user.email);
                    dy_message = dy_message.replace("<<SCHOOL_NAME>>", user.school_name);
                    dy_message = dy_message.replace("<<TYPE>>", user.type);
                    dy_message = dy_message.replace("<<GRADE>>", user.grade_name);
                    dy_message = dy_message.replace("<<SESSION_NAME>>", user.session_name);
                    dy_message = dy_message.replace("<<MOBILE_NUMBER>>", user.mobile_number);
                    dy_message = dy_message.replace("<<TOTAL_MARKS>>", parseFloat(Math.round(user.total_marks * 100) / 100).toFixed(2));
                    dy_message = dy_message.replace("<<ACHIVED_MARKS>>", parseFloat(Math.round(user.achived_marks * 100) / 100).toFixed(2));
                    dy_message = dy_message.replace("<<ACHIVED_GRADE>>", parseFloat(Math.round(user.achived_grade * 100) / 100).toFixed(2));
                    dy_message = dy_message.replace("<<CHILD_NAME>>", user.child_name);

                    console.log(dy_message);
                    sms.sendMessage(true, user.mobile_number, dy_message, function(err, success) {
                        if (err) {
                            console.log(err);
                        }
                        if (loopCount >= rows.length) {
                            cb(null, true);
                        }
                    });
                } else {
                    if (loopCount >= rows.length) {
                        cb(null, true);
                    }
                }

            } else {
                loopCount++;
                if (loopCount >= rows.length) {
                    cb(null, true);
                }
            }
        })
    });
};


send_notice_email = function(req, res, school_id, type, message, grade_id, class_id, gender, cb) {
    var query_str = `SELECT *
                    FROM (
                        SELECT
                            CONCAT(t1.id) as id,
                            CONCAT(t1.first_name) as first_name,
                            CONCAT(t1.last_name) as last_name,
                            CONCAT(t1.first_name," ",t1.last_name) as name,
                            "staff" as type,
                            t1.school_id,
                            t1.phonenumber as mobile_number,
                            0 as grade_id,
                            t1.gender,
                            t1.email_id as email,
                            "" as grade_name,
                            t2.school_name,
                            t3.session_name,
                            "0" as total_marks,
                            "0" as achived_marks,
                            "0" as achived_grade,
                            "" as child_name,
                            "" as classes,
                            "" as classes_name
                          FROM staff as t1
                          LEFT JOIN schools as t2 ON t1.school_id = t2.id
                          LEFT JOIN sessions as t3 ON t1.session_id = t3.id
                          UNION
                          SELECT
                            CONCAT(t1.id) as id,
                            CONCAT(t1.first_name) as first_name,
                            CONCAT(t1.last_name) as last_name,
                            CONCAT(t1.first_name," ",t1.last_name) as name,
                            "student" as type,
                            t1.school_id,
                            t1.mobile_number as mobile_number,
                            t1.grade_id,
                            t1.gender,
                            t1.email,
                            t3.grade as grade_name,
                            t2.school_name,
                            t4.session_name,
                            COALESCE(SUM(t5.total_marks),0) as total_marks,
                            COALESCE(SUM(t5.achived_marks),0) as achived_marks,
                            COALESCE(SUM(t5.achived_grade),0) as achived_grade,
                            "" as child_name,
                            GROUP_CONCAT(t6.class_id) as classes,
                            GROUP_CONCAT(t7.class_name) as classes_name
                          FROM admissions as t1
                          LEFT JOIN schools as t2 ON t1.school_id = t2.id
                          LEFT JOIN grades as t3 ON t1.grade_id = t3.id
                          LEFT JOIN sessions as t4 ON t1.session_id = t4.id
                          LEFT JOIN exam_results as t5 ON t5.student_id = t1.id
                          LEFT JOIN student_classes as t6 ON t6.student_id = t1.id
                          LEFT JOIN classes as t7 ON t6.class_id = t7.id
                          GROUP BY t1.id
                          UNION
                          SELECT
                            CONCAT(t1.id) as id,
                            CONCAT(t1.full_name) as first_name,
                            "" as last_name,
                            CONCAT(t1.full_name) as name,
                            "parents" as type,
                            t2.school_id as school_id,
                            t1.mobile as mobile_number,
                            t2.grade_id as grade_id,
                            t1.gender,
                            t1.email,
                            t5.grade as grade_name,
                            t3.school_name,
                            t4.session_name,
                            COALESCE(SUM(t6.total_marks),0) as total_marks,
                            COALESCE(SUM(t6.achived_marks),0) as achived_marks,
                            COALESCE(SUM(t6.achived_grade),0) as achived_grade,
                            CONCAT(t2.first_name," ",t2.last_name) as child_name,
                            GROUP_CONCAT(t7.class_id) as classes,
                            GROUP_CONCAT(t8.class_name) as classes_name
                          FROM parents as t1
                          LEFT JOIN admissions as t2 ON t1.student_id = t2.id
                          LEFT JOIN schools as t3 ON t2.school_id = t3.id
                          LEFT JOIN sessions as t4 ON t2.session_id = t4.id
                          LEFT JOIN grades as t5 ON t2.grade_id = t5.id
                          LEFT JOIN exam_results as t6 ON t6.student_id = t2.id
                          LEFT JOIN student_classes as t7 ON t7.student_id = t2.id
                          LEFT JOIN classes as t8 ON t7.class_id = t8.id
                          GROUP BY t2.id
                          ) as final
                    WHERE final.school_id = ? AND (final.type IN (?) OR final.type IN (?) OR final.type IN (?)) `;
    db.driver.execQuery(query_str, [school_id, type[0], type[1], type[2]], function(err, rows) {
        if (err) {
            cb(err, null);
        }
        if (rows.length == 0) {
            cb(null, true);
        }
        var loopCount = 0;
        rows.forEach(function(user) {
            if (user.email != '' && user.email != null) {
                var flag = true;
                if (grade_id != "") {
                    flag = false;
                    if ((user.type == 'staff') || ((user.type == 'student' || user.type == 'parents') && user.grade_id == grade_id)) {
                        flag == true;
                    }
                }
                if (class_id != "" && class_id != null) {
                    flag = false;
                    if ((user.type == 'staff') || ((user.type == 'student' || user.type == 'parents') && user.classes != null && user.classes.indexOf(class_id) != -1 )) {
                        flag == true;
                    }
                }
                if (gender != "" && user.gender != gender) {
                    flag = false;
                }
                loopCount++;
                if (flag) {
                    /*message dynamic change*/
                    var dy_message = message;
                    dy_message = dy_message.replace("<<FIRST_NAME>>", user.first_name);
                    dy_message = dy_message.replace("<<LAST_NAME>>", user.last_name);
                    dy_message = dy_message.replace("<<FULL_NAME>>", user.name);
                    dy_message = dy_message.replace("<<EMAIL>>", user.email);
                    dy_message = dy_message.replace("<<SCHOOL_NAME>>", user.school_name);
                    dy_message = dy_message.replace("<<TYPE>>", user.type);
                    dy_message = dy_message.replace("<<GRADE>>", user.grade_name);
                    dy_message = dy_message.replace("<<SESSION_NAME>>", user.session_name);
                    dy_message = dy_message.replace("<<MOBILE_NUMBER>>", user.mobile_number);
                    dy_message = dy_message.replace("<<TOTAL_MARKS>>", parseFloat(Math.round(user.total_marks * 100) / 100).toFixed(2));
                    dy_message = dy_message.replace("<<ACHIVED_MARKS>>", parseFloat(Math.round(user.achived_marks * 100) / 100).toFixed(2));
                    dy_message = dy_message.replace("<<ACHIVED_GRADE>>", parseFloat(Math.round(user.achived_grade * 100) / 100).toFixed(2));
                    dy_message = dy_message.replace("<<CHILD_NAME>>", user.child_name);

                    var email_html = `
                    <body style="font-family:arial; font-size:16px; color:#4c4c4c; padding:0; margin:0;">
                        <table width="100%" style="font-family:arial; font-size:16px; border:0px solid #000; max-width:700px; padding-top:30px;"  align="center" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td>
                                        <table width="100%" style="font-family:arial; font-size:16px; border:1px solid #d6d6d8; max-width:700px;"  align="center" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td style="padding:50px 40px 40px 40px; text-align:center; font-family:arial;">
                                                        <h1 style="font-family:arial; font-size:34px; color:#30333b; font-weight:normal; padding:0; margin:0;">Hello, <span style="color:rgb(242,53,95); font-weight:bold; text-transform:uppercase;">` + user.name + `</span></h1>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding:0 40px 30px 40px; text-align:center; font-family:arial;">
                                                        <p style="font-family:arial; font-size:22px; color:#737373; font-weight:normal; padding:0; margin:0;">
                                                            ` + dy_message + `
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                   <td style="padding:35px 40px;">
                                                         <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                             <tr>
                                                                 <td valign="bottom" style="text-align:right; font-family:arial; font-size:14px; color:#6a6c72;">Kind Regards <br />
                                                                    <b style="text-transform:uppercase;">` + req.body.school_auth_person_name + `</b><br />
                                                                    Email: ` + req.body.school_auth_person_email + `<br />
                                                                    Mobile: ` + req.body.school_auth_person_mobile + `<br />
                                                                </td>
                                                             </tr>
                                                         </table>
                                                     </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-family:arial; font-size:10px; padding:20px 0 30px 0; color:#a6a6a6; text-transform:uppercase; text-align:center;">
                                        &#169; 2016 Copyright Thunder System
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </body>
                    `;
                    var from = 'binit@thinkwik.com';
                    var to = [user.email];
                    var subject = "Email Notification from Thunder";
                    sendMail(req, res, from, to, subject, email_html, function(err, success) {
                        if (err) {
                        }
                        if (loopCount > rows.length) {
                            cb(null, true);
                        }
                    });
                } else {
                    if (loopCount >= rows.length) {
                        cb(null, true);
                    }
                }
            } else {
                loopCount++;
                if (loopCount >= rows.length) {
                    cb(null, true);
                }
            }
        })
    });
};