var db = require('orm').db,
    Issues = db.models.issues,
    Issues_messages = db.models.issue_messages;

addIssueMessage = function(issue_id, message_from, message_to, message, cb) {
    newIssues_messages = new Issues_messages();
    newIssues_messages.issue_id = issue_id;
    newIssues_messages.message_from = message_from;
    newIssues_messages.message_to = message_to;
    newIssues_messages.message = message;
    newIssues_messages.save(function(err, created_Issues_messages) {
        if (err) {
            cb(true, null);
        }
        cb(null, true);
    });
};

exports.register = function(req, res) {
    if (typeof req.body.subject == 'undefined' || typeof req.body.priority == 'undefined' || typeof req.body.staff_id == 'undefined' || typeof req.body.description == 'undefined') {
        return apiSendError(req, res, 422, 'Please check your parameters.');
    }
    if (typeof req.body.student_id == 'undefined' && typeof req.body.parent_id == 'undefined') {
        return apiSendError(req, res, 422, 'student or parent id required.');
    }
    newIssue = new Issues();
    newIssue.student_id = (req.body.student_id != null && req.body.student_id != "") ? req.body.student_id : null;
    newIssue.parent_id = (req.body.parent_id != null && req.body.parent_id != "") ? req.body.parent_id : null;
    newIssue.subject = req.body.subject;
    newIssue.priority = req.body.priority;
    newIssue.staff_id = req.body.staff_id;
    newIssue.description = req.body.description;
    newIssue.status = (typeof req.body.status != 'undefined') ? req.body.status : '1';
    newIssue.save(function(err, created_issue) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 500, 'Something went wrong');
        }
        message_from = (created_issue.student_id != null) ? created_issue.student_id : created_issue.parent_id;
        addIssueMessage(created_issue.id, message_from, created_issue.staff_id, created_issue.description, function(err, done) {
            if (err) {
                console.log(err);
            }
        });
        var response = {};
        response['data'] = [];
        response['code'] = 200;
        return res.send(response);
    });
};

exports.apiGetAllParentsIssues = function(req, res) {
    if (typeof req.body.parent_id == 'undefined' || req.body.parent_id == 'null') {
        return apiSendError(req, res, 422, "refer parameters");
    }
    var query_str = `SELECT t1.*,CONCAT(t2.first_name,' ',t2.last_name) as staff_name,t3.full_name
                     FROM issues as t1
                     LEFT JOIN staff as t2 on t1.staff_id = t2.id
                     LEFT JOIN parents as t3 on t1.parent_id = t3.id
                     WHERE t1.parent_id = ?
                     ORDER BY t1.created_at DESC`;
    db.driver.execQuery(query_str, [req.body.parent_id], function(err, rows) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 422, "Bad Request check your parameters");
        }
        var response = {};
        response['data'] = rows;
        response['code'] = 200;
        return res.send(response);
    });
};

exports.apiGetAllStaffIssues = function(req, res) {
    if (typeof req.body.staff_id == 'undefined' || req.body.staff_id == 'null') {
        return apiSendError(req, res, 422, "refer parameters");
    }
    var query_str = `SELECT t1.*,CONCAT(t2.first_name,' ',t2.last_name) as student_name,t3.full_name as parent_name
    FROM issues as t1
    LEFT JOIN admissions as t2 on t1.student_id = t2.id
    LEFT JOIN parents as t3 on t1.parent_id = t3.id
    WHERE t1.staff_id = ?
    ORDER BY t1.created_at DESC`;
    db.driver.execQuery(query_str, [req.body.staff_id], function(err, rows) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 422, "Bad Request check your parameters");
        }
        var response = {};
        response['data'] = rows;
        response['code'] = 200;
        return res.send(response);
    });
};

exports.apiGetAllStudentsIssues = function(req, res) {
    if (typeof req.body.student_id == 'undefined' || req.body.student_id == 'null') {
        return apiSendError(req, res, 422, "refer parameters");
    }
    var query_str = `SELECT t1.*,CONCAT(t2.first_name,' ',t2.last_name) as staff_name,CONCAT(t3.first_name,' ',t3.last_name) as student_name FROM issues as t1
    LEFT JOIN staff as t2 on t1.staff_id = t2.id
    LEFT JOIN admissions as t3 on t1.student_id = t3.id
    LEFT JOIN schools as t4 on t3.school_id = t4.id
    WHERE t1.student_id = ?
    ORDER BY t1.created_at DESC`;
    db.driver.execQuery(query_str, [req.body.student_id], function(err, rows) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 422, "Bad Request check your parameters");
        }
        var response = {};
        response['data'] = rows;
        response['code'] = 200;
        return res.send(response);
    });
};

exports.apiParentSendMessage = function(req, res) {
    if (typeof req.body.issue_id == 'undefined' || typeof req.body.parent_id == 'undefined' || typeof req.body.message == 'undefined') {
        return apiSendError(req, res, 422, "please check your parameters!");
    }
    Issues.find({
        id: req.body.issue_id
    }, 1, function(err, issues) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 404, 'issue not found.');
        }
        if (issues.length == 0) {
            return apiSendError(req, res, 404, 'issue not found.');
        }
        var message_to = issues[0].staff_id;
        addIssueMessage(req.body.issue_id, req.body.parent_id, message_to, req.body.message, function(err, done) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 500, "Something went wrong!");
            }
            var response = {};
            response['data'] = [];
            response['code'] = 200;
            return res.send(response);
        });
    });
};

exports.apiStudentSendMessage = function(req, res) {
    if (typeof req.body.issue_id == 'undefined' || typeof req.body.student_id == 'undefined' || typeof req.body.message == 'undefined') {
        return apiSendError(req, res, 422, "please check your parameters!");
    }
    Issues.find({
        id: req.body.issue_id
    }, 1, function(err, issues) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 404, 'issue not found.');
        }
        if (issues.length == 0) {
            return apiSendError(req, res, 404, 'issue not found.');
        }
        var message_to = issues[0].staff_id;
        addIssueMessage(req.body.issue_id, req.body.student_id, message_to, req.body.message, function(err, done) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 500, "Something went wrong!");
            }
            var response = {};
            response['data'] = [];
            response['code'] = 200;
            return res.send(response);
        });
    });
};

exports.apiStaffSendMessage = function(req, res) {
    var IssueStatus;
    if (typeof req.body.staff_id == 'undefined' || typeof req.body.issue_id == 'undefined' || typeof req.body.message == 'undefined') {
        return apiSendError(req, res, 422, "please check your parameters!");
    }
    if (req.body.status) {
      Issues.find({
          id: req.body.issue_id
      }, 1, function(err, issueStatus) {
          if (err) {
              console.log(err);
              return apiSendError(req, res, 500, 'something went wrong.');
          }
          if (!issueStatus.length) {
              return apiSendError(req, res, 404, 'Issue not found.');
          }
          issueStatus[0].status = req.body.status;
          issueStatus[0].save(function(err, status_changed) {
              if (err) {
                  console.log(err);
                  return apiSendError(req, res, 500, 'something went wrong.');
              }
              if (!status_changed) {
                  return apiSendError(req, res, 404, 'Issue not found.');
              }
              console.log(status_changed);
          });
      });
    }
    Issues.find({
        id: req.body.issue_id
    }, 1, function(err, issues) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 404, 'issue not found.');
        }
        if (issues.length == 0) {
            return apiSendError(req, res, 404, 'issue not found.');
        }
        var message_to = (issues[0].student_id != null) ? issues[0].student_id : issues[0].parent_id;
        addIssueMessage(req.body.issue_id, req.body.staff_id, message_to, req.body.message, function(err, done) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 500, "Something went wrong!");
            }
            var response = {};
            response['data'] = [];
            response['code'] = 200;
            return res.send(response);
        });
    });
};

exports.delete = function(req, res) {
    if (req.body.id !== 'undefined' && req.body.id) {
        Issues.find({
            id: req.body.id
        }, 1, function(err, issues) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 500, 'something went wrong.');
            }
            if (!issues.length) {
                return apiSendError(req, res, 404, 'issue not found.');
            }
            issues[0].remove(function(err) {
                if (err) {
                    console.log(err);
                    return sendError(req, res, 500, 'something went wrong.');
                }
                var response = {};
                response['data'] = [];
                response['code'] = 200;
                return res.send(response);
            });
        });
    } else {
        return apiSendError(req, res, 422, "please check your parameters!");
    }
};

exports.saveIssuesMessgesResponseStudent = function(req, res) {
    if (req.body.message && req.body.message_to && req.body.message_from && req.body.issue_id) {
        newIssues_messages = new Issues_messages();
        newIssues_messages.issue_id = req.body.issue_id;
        newIssues_messages.message_from = req.body.message_from;
        newIssues_messages.message_to = req.body.message_to;
        newIssues_messages.message = req.body.message;
        newIssues_messages.save(function(err, created_Issues_messages) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, 'something went wrong.');
            }
            var response = {};
            response['data'] = [];
            response['code'] = 200;
            return res.send(response);
        });
    } else {
        return sendError(req, res, 422, "please check your parameter!");
    }
};

exports.saveIssuesMessgesResponseParent = function(req, res) {
    if (req.body.message && req.body.message_to && req.body.message_from && req.body.issue_id) {
        newIssues_messages = new Issues_messages();
        newIssues_messages.issue_id = req.body.issue_id;
        newIssues_messages.message_from = req.body.message_from;
        newIssues_messages.message_to = req.body.message_to;
        newIssues_messages.message = req.body.message;
        newIssues_messages.save(function(err, created_Issues_messages) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, 'something went wrong.');
            }
            var response = {};
            response['data'] = [];
            response['code'] = 200;
            return res.send(response);
        });
    } else {
        return sendError(req, res, 422, "please check your parameter!");
    }
};

exports.apiUpdateStatus = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Issue id required!!');
    }
    Issues.find({
        id: req.body.id
    }, 1, function(err, issues) {
        if (err) {
            console.log(err);
            return sendError(req, res, 500, 'something went wrong.');
        }
        issues[0].status = req.body.status;
        issues[0].save(function(err, status_changed) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, 'something went wrong.');
            }
            if (!status_changed) {
                return sendError(req, res, 404, 'Issue not found.');
            }
            var response = {};
            response['data'] = [];
            response['code'] = 200;
            return res.send(response);
        });
    });

};
