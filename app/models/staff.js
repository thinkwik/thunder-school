var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Staff = db.define('staff', {
    session_id: {
        type: 'integer'
    },
    school_id: {
        type: 'integer'
    },
    unique_id: String,
    department_id: {
        type: 'integer'
    },
    designation_id: {
        type: 'integer'
    },
    role_id: {
        type: 'integer'
    },
    type: String,
    photo: String,
    first_name: String,
    middle_name: String,
    last_name: String,
    gender: String,
    address_1: String,
    address_2: String,
    city: String,
    state: String,
    country: String,
    pincode: String,
    email_id: String,
    phonenumber: String,
    officenumber: String,
    date_of_birth: String,
    date_of_joining: String,
    employee_username: String,
    password: String,
    medicalinfo:String,
    social_behavioral:String,
    physical_handicaps:String,
    medical_immunizations: {
        type: 'integer'
    },
    medical_allergies: {
        type: 'integer'
    },
    facebook_profile: String,
    linkedin_profile: String,
    twitter_profile: String,
    socialsecuritynumber: String,
    forgot_password_token:String,
    emergency_contacts:String,
    notes:String,
    status: ['0', '1'],
    profile_status: ['0', '1']
}, {
    timestamp: true,
    methods: {

    }
});



