app.controller('createTimeTableController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        var district = $cookieStore.get('loggedInDistrict');
        $scope.getGrades = function() {
            var data = {
                district_id: $cookieStore.get('loggedInDistrict').id
            };
            $http({
                method: 'POST',
                url: '/grade/getAllDistrictGrades',
                data: data
            }).then(function successCallback(response) {
                $scope.grades = response.data;
                $scope.getSubjects();
                $scope.getClasses();
            }, function errorCallback(error) {
                toaster.pop('error', 'District', 'Something went wrong!');
            });
        };

        $scope.getSubjects = function() {
            var data = {
                district_id: $cookieStore.get('loggedInDistrict').id
            };
            $http({
                method: 'POST',
                url: '/subject/getAllDistrictSubjects',
                data: data
            }).then(function successCallback(response) {
                $scope.subjects = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'District', 'Something went wrong!');
            });
        };

        $scope.getClasses = function() {
            var data = {
                district_id: $cookieStore.get('loggedInDistrict').id
            };
            $http({
                method: 'POST',
                url: '/class/getAllDistrictClass',
                data: data
            }).then(function successCallback(response) {
                $scope.classes = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'District', 'Something went wrong!');
            });
        };

        $scope.timeTableList = function() {
            $state.go('districtstaff.timetable_list');
        };

        $scope.createTimetableSchedule = function() {
            data = $scope.Schedule;
            data.created_by = $cookieStore.get('loggedInUser').id;
            $http({
                method: "POST",
                url: "/timetable/register",
                data: data
            }).then(function successCallback(response) {
                $scope.timeTableList();
                toaster.pop('success', 'Time-table', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Time-table', error.data.message);
            });
        };


    }
]);

app.controller('listTimeTableController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.timeTableCreateRoute = function() {
            $state.go('districtstaff.timetable_create');
        };

        $scope.inactiveTimetableSchedule = function(timetable_id) {
            var data = {
                id: timetable_id
            };
            $http({
                method: 'POST',
                url: '/timetable/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllTimetable();
            }, function errorCallback(error) {
                toaster.pop('error', 'Time-table', error.data.message);
            });
        };

        $scope.activeTimetableSchedule = function(timetable_id) {
            var data = {
                id: timetable_id
            };
            $http({
                method: 'POST',
                url: '/timetable/active',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllTimetable();
            }, function errorCallback(error) {
                toaster.pop('error', 'Time-table', error.data.message);
            });
        };

        $scope.deleteTimetableSchedule = function(timetable_id) {
            var data = {
                id: timetable_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/timetable/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllTimetable();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Time-table', error.data.message);
                });
            }
        };

        $scope.getAllTimetableSchedule = function() {
            var data = {
                district_id: $cookieStore.get('loggedInDistrict').id
            }
            $http({
                method: 'POST',
                url: '/timetable/getAllTimetableSchedule',
                data: data
            }).then(function successCallback(response) {
                var schedules = response.data;
                $scope.schedules = schedules;
            }, function errorCallback(error) {
                toaster.pop('error', 'Time-table', 'cannot get Time-table! try reloading page.')
                console.log(error.data.message);
            });
        };

        $scope.editTimetable = function(timetable_id) {
            $state.go('districtstaff.timetable_edit', {
                timetable_id: timetable_id
            });
        };

    }
]);

app.controller('editTimeTableController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.getGrades = function() {
            var data = {
                district_id: $cookieStore.get('loggedInDistrict').id;
            };
            $http({
                method: 'POST',
                url: '/grade/getAllDistrictGrades',
                data: data
            }).then(function successCallback(response) {
                $scope.grades = response.data;
                $scope.getSubjects();
                $scope.getClasses();
            }, function errorCallback(error) {
                toaster.pop('error', 'District', 'Something went wrong!');
            });
        };
        $scope.getSubjects = function() {
            var data = {
                district_id: $cookieStore.get('loggedInDistrict').id;
            };
            $http({
                method: 'POST',
                url: '/subject/getAllDistrictSubjects',
                data: data
            }).then(function successCallback(response) {
                $scope.subjects = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'District', 'Something went wrong!');
            });
        };
        $scope.getClasses = function() {
            var data = {
                district_id: $cookieStore.get('loggedInDistrict').id;
            };
            $http({
                method: 'POST',
                url: '/class/getAllDistrictClass',
                data: data
            }).then(function successCallback(response) {
                $scope.classes = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'District', 'Something went wrong!');
            });
        };

        $scope.getEditSession = function() {
            $http({
                method: 'GET',
                url: '/session/edit?id=' + $state.params.session_id
            }).then(function successCallback(response) {
                $scope.session = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Session', 'cannot get all the Sessions! try reloading page.')
            });
        };

        $scope.saveEditSession = function() {
            data = $scope.session;
            data.created_by = $cookieStore.get('loggedInUser').id;
            $http({
                method: 'POST',
                url: '/session/edit',
                data: data
            }).then(function successCallback(response) {
                $scope.cancelSessionForm();
                toaster.pop('success', 'Session', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Session', error.data.message);
            });
        };

        $scope.cancelSessionForm = function() {
            $state.go('districts.session_list');
        };
    }
]);
