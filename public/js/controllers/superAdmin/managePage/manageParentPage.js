app.controller('listParentPagesController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {

        $scope.addNewPage = function() {
            $state.go('users.parent_pages_create');
        };

        $scope.getAllParentPages = function() {
            $http({
                method: 'POST',
                url: '/parent/pages/getAllParentPages'
            }).then(function successCallback(response) {
                $scope.ParentPages = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Pages', error.data.message);
            });
        };

        $scope.inactiveParentPage = function(parent_page_id) {
            var data = {
                id: parent_page_id
            };
            $http({
                method: 'POST',
                url: '/parent/pages/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllParentPages();
            }, function errorCallback(error) {
                toaster.pop('error', 'Pages', error.data.message);
            });
        };

        $scope.activeParentPage = function(parent_page_id) {
            var data = {
                id: parent_page_id
            };
            $http({
                method: 'POST',
                url: '/parent/pages/active',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllParentPages();
            }, function errorCallback(error) {
                toaster.pop('error', 'Pages', error.data.message);
            });
        };

        $scope.deleteParentPage = function(parent_page_id) {
            var data = {
                id: parent_page_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/parent/pages/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllParentPages();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Pages', error.data.message);
                });
            }
        };

        $scope.editParentPage = function(parent_page_id) {
            $state.go('users.parent_pages_edit', {
                parent_page_id: parent_page_id
            });
        };
    }
]);

app.controller('editParentPagesController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {

        $scope.editSaveParentPage = function() {
            var data = $scope.page;
            $http({
                method: 'POST',
                url: '/parent/pages/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Pages', 'updated successfully.');
                $state.go('users.parent_pages_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'Pages', error.data.message);
            });
        };

        $scope.getEditParentPage = function() {
            $http({
                method: 'GET',
                url: '/parent/pages/edit?id=' + $state.params.parent_page_id
            }).then(function successCallback(response) {
                $scope.page = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Pages', error.data.message);
            });
        };

        $scope.cancelPageForm = function(parent_page_id) {
            $state.go('users.parent_pages_list');
        };
    }
]);

app.controller('createParentPagesController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {

        $scope.cancelPage = function() {
            $state.go('users.parent_pages_list');
        };
        
        $scope.createPage = function() {
            var data = $scope.page;
            $http({
                method: 'POST',
                url: '/parent/pages/register',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Pages', 'added successfully.');
                $state.go('users.parent_pages_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'Pages', error.data.message);
            });
        };
    }
]);
