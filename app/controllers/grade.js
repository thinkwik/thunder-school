var bCrypt = require('bcrypt-nodejs');
var db = require('orm').db,
    School = db.models.schools,
    Grades = db.models.grades;

exports.register = function(req, res) {
    if (typeof req.body.school_id != 'undefined') {
        var new_grades = new Grades();
        new_grades.school_id = req.body.school_id;
        new_grades.grade = req.body.grade;
        new_grades.position = req.body.position;
        new_grades.status = (typeof req.body.status != 'undefined') ? req.body.status : "1";
        new_grades.save(function(err, saved_grades) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, "Bad Request check your parameters");
            }
            res.send(saved_grades);
        });

    } else {
        return sendError(req, res, 422, 'school_id required for register new grade');
    }
};

exports.getGrade = function(req, res) {
    if (typeof req.query.id == 'undefined' || req.query.id == "null") {
        return res.send("id required for getting json");
    }
    Grades.find({
        id: req.query.id
    }, 1, function(err, grades) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        if (!grades.length) {
            return res.send("Grades not found with perticular id");
        }
        return res.send(grades[0]);
    });
};

exports.getAllDistrictGrades = function(req, res) {
    if (typeof req.body.district_id == 'undefined' || req.body.district_id == "null") {
        return sendError(req, res, 422, "refer parameters");
    }

    var query_str = "SELECT t1.*,t2.school_name FROM `grades` as t1 LEFT JOIN schools as t2 on t1.school_id = t2.id WHERE  t2.parent_id = ? ORDER BY t1.school_id ASC, t1.position ASC";
    db.driver.execQuery(query_str, [req.body.district_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'grade id required');
    }
    Grades.find({
        id: req.body.id
    }, 1, function(err, grade) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        if (!grade.length) {
            return sendError(req, res, 404, "Grade not found");
        }
        grade[0].school_id = req.body.school_id;
        grade[0].grade = req.body.grade;
        grade[0].position = req.body.position;
        grade[0].status = req.body.status;
        grade[0].save(function(err, updated_grade) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            return res.send(updated_grade);
        });
    });
};

exports.active = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'grade id required');
    }
    Grades.find({
        id: req.body.id
    }, 1, function(err, grade) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        grade[0].status = "1";
        grade[0].save(function(err, activated_grade) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            return res.send(activated_grade);
        });
    });
};

exports.inactive = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'grade id required');
    }
    Grades.find({
        id: req.body.id
    }, 1, function(err, grade) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        grade[0].status = "0";
        grade[0].save(function(err, inactivated_grade) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            return res.send(inactivated_grade);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'grade id required');
    }
    Grades.find({
        id: req.body.id
    }, 1, function(err, grade) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        grade[0].remove(function(err) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, "Bad Request check your parameters");
            }
            return res.send(grade[0]);
        });
    });
};

exports.getAllSchoolGrades = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id == "null") {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = "SELECT t1.*,t2.school_name FROM `grades` as t1 LEFT JOIN schools as t2 on t1.school_id = t2.id WHERE  t2.id = ? ORDER BY t1.school_id ASC, t1.position ASC";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
