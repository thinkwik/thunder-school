var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Parent_Menus = db.define('parent_menus', {
    title: String,
    status: ['0', '1']
}, {
    timestamp: true,
    methods: {

    }
});
