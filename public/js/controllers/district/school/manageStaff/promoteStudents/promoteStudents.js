app.controller('promoteStudentsController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        $scope.getSchoolGrades = function() {
            var data = {
                school_id: $cookieStore.get('loggedInStaff').school_id
            };
            $http({
                method: 'POST',
                url: '/grade/getAllSchoolGrades',
                data: data
            }).then(function successCallback(response) {
                $scope.grades = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Grades', 'Something went wrong!');
            });
        };

        $scope.getStudentGradeWise = function(grade_id) {
            var data = {
                grade_id: grade_id,
                school_id: $cookieStore.get('loggedInStaff').school_id
            };
            $http({
                method: 'POST',
                url: '/student/getStudentsListGradeWise',
                data: data
            }).then(function successCallback(response) {
                $scope.students = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Students', 'Something went wrong!');
            });
        };

        $scope.promoteStudent = function() {
          if(confirm("Are you sure you want to promote this students ?")){
            var data = $scope.promote;
            $http({
                method: 'POST',
                url: '/student/studentPromotion',
                data: data
            }).then(function successCallback(response) {
                $scope.promote.student_id = "";
                $scope.promote.student_promoted_to = "";
                $scope.promote.select_grade = "";
                $scope.promote = [];
                $scope.getSchoolGrades();
                $scope.getStudentGradeWise();
                toaster.pop('success', 'Students', 'Promoted Successfully!');
            }, function errorCallback(error) {
                toaster.pop('error', 'Students', 'Something went wrong!');
            });
          }
        };

    }
]);
