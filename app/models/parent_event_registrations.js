var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var parentEventregistration = db.define('parent_event_registrations', {
    event_id: {
        type: 'integer'
    },
    school_id: {
        type: 'integer'
    },
    parent_id: {
        type: 'integer'
    },
    parent_status: ['0', '1']
}, {
    timestamp: true,
    methods: {

    }
});
