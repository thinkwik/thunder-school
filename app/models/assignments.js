var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Assignments = db.define('assignments', {
    school_id: {
        type: 'integer'
    },
    staff_id: {
        type: 'integer'
    },
    grade_id: {
        type: 'integer'
    },
    class_id: {
        type: 'integer'
    },
    subject_id: {
        type: 'integer'
    },
    title: String,
    description: String,
    due_date: String,
    file_name: String
}, {
    timestamp: true,
    methods: {

    }
});
