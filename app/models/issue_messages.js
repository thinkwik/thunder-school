var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Issues_messages = db.define('issue_messages', {
    issue_id: {
        type: 'integer'
    },
    message_from: {
        type: 'integer'
    },
    message_to: {
        type: 'integer'
    },
    message: String,
}, {
    timestamp: true,
    methods: {

    }
});
