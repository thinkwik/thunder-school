app.config(function($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider.otherwise("/staff/login");
    //Manage Staff Login
    $stateProvider.state('staffSignin', {
            url: "/staff/login",
            templateUrl: "/partials/auth/staffSignin.html",
            controller: 'staffSigninFormController'
        })
        //Manage Profile
        .state('schoolstaff', {
            url: '/school/staff',
            templateUrl: '/partials/profile/staff.html',
            abstract: true,
            controller: 'protectStaffRoutesController'
        })
        .state('schoolstaff.changePassword', {
            url: '/changepassword',
            templateUrl: '/partials/staff/changePassword.html'
        })
        .state('schoolstaff.edit_profile', {
            url: '/edit',
            templateUrl: '/partials/staff/editProfile.html',
            controller: 'editStaffProfileController'
        })
        //Manage Dashboard
        .state('schoolstaff.dashboard', {
            url: '/dashboard',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageDashboard/dashboard.html',
            controller: 'staffDashboardController'
        })
        //Manage To-Dos
        .state('schoolstaff.todo_create', {
            url: '/todo/create',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageTodo/createTodo.html',
            controller: 'staffTodoCreateController'
        })
        .state('schoolstaff.todo_list', {
            url: '/todo/list',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageTodo/listTodo.html',
            controller: 'staffTodoListController'
        })
        // Manage Admissions
        .state('schoolstaff.admission_list', {
            url: '/admission/list',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageAdmission/listAdmission.html',
            controller: 'listStaffAdmissionController'
        })
        .state('schoolstaff.admission_create', {
            url: '/admission/create',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageAdmission/createAdmission.html',
            controller: 'createStaffAdmissionController'
        })
        .state('schoolstaff.admission_edit', {
            url: '/admission/edit/:student_id',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageAdmission/editAdmission.html',
            controller: 'editStaffAdmissionController'
        })
        // Manage Students
        .state('schoolstaff.student_create', {
            url: '/student/create/:student_id',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageStudents/createStudents.html',
            controller: 'editStaffAdmissionController'
        })
        .state('schoolstaff.student_edit', {
            url: '/student/edit/:student_id',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageStudents/editStudentIqnuiry.html',
            controller: 'editStaffAdmissionInquiryController'
        })
        // Manage Issues
        .state('schoolstaff.issues_list', {
            url: '/issues/list',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageIssues/listIssues.html',
            controller: 'listStaffIssuesController'
        })
        .state('schoolstaff.view_issue', {
            url: '/issues/view/:issue_id',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageIssues/viewIssue.html',
            controller: 'viewStaffIssuesController'
        })
        .state('schoolstaff.responseToIssue', {
            url: '/issues/response/:issue_id',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageIssues/viewIssue.html',
            controller: 'responseIssuesController'
        })
        // Manage Student FollowUps
        .state('schoolstaff.create_followUp', {
            url: '/student/createFollowup/:student_id',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageStudents/createFollowUp.html',
            controller: 'createFollowUpController'
        })
        .state('schoolstaff.view_followUp', {
            url: '/student/viewfollowup/:student_id',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageStudents/viewFollowUp.html',
            controller: 'viewFollowUpController'
        })
        .state('schoolstaff.student_academics', {
            url: '/student/create/:student_id',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageStudents/createStudentAcademics.html',
            controller: 'editStaffAdmissionController'
        })
        .state('schoolstaff.student_skills', {
            url: '/student/create/:student_id',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageStudents/createStudentSkills.html',
            controller: 'editStaffAdmissionController'
        })
        .state('schoolstaff.student_parents_create', {
            url: '/student/parents/create/:student_id',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageStudents/createStudentParents.html',
            controller: 'saveStudentsParentsController'
        })
        .state('schoolstaff.student_parents_edit', {
            url: '/student/parents/edit/:parent_id',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageStudents/editStudentParents.html',
            controller: 'editStudentsParentsController'
        })
        .state('schoolstaff.student_list', {
            url: '/student/list',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageStudents/listStudent.html',
            controller: 'listStudentStaffController'
        })
        .state('schoolstaff.student_profile_view', {
            url: '/student/profile/:student_id',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageStudents/viewStudentProfileForStaff.html',
            controller: 'viewStudentProfileController'
        })
        //Manage Emails
        .state('schoolstaff.email_list', {
            url: '/staff/email/lists',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageEmails/manageEmails.html',
            controller: 'viewStaffProfileController'
        })
        .state('schoolstaff.email_view', {
            url: '/staff/email/view/:mail_id',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageEmails/viewEmails.html',
            controller: 'viewEmailToStaffController'
        })
        //Manage Templates
        .state('schoolstaff.templates_list', {
            url: '/template/list',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageTemplates/listTemplate.html',
            controller: "listStaffTemplateController"
        })
        .state('schoolstaff.templates_create', {
            url: '/template/create',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageTemplates/createTemplate.html',
            controller: "createStaffTemplateController"
        })
        .state('schoolstaff.templates_edit', {
            url: '/template/edit/:template_id',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageTemplates/editTemplate.html',
            controller: "editStaffTemplateController"
        })
        //Manage Appointments
        .state('schoolstaff.appointment_list', {
            url: '/appointment/list',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageApppointments/listappointment.html',
            controller: 'listStaffAppointmentController'
        })
        // .state('schoolstaff.appointment_edit', {
        //     url: '/appointment/edit/:appointment_id',
        //     templateUrl: '/partials/superAdmin/district/manageStaff/manageApppointments/editApppointment.html',
        //     controller: 'editStaffAppointmentController'
        // })
        .state('schoolstaff.appointment_create', {
            url: '/appointment/create',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageApppointments/createApppointment.html',
            controller: 'createStaffAppointmentController'
        })
        //Manage Materials
        .state('schoolstaff.create_material', {
            url: '/material/create',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageMaterials/createMaterial.html',
            controller: 'createMaterialController'
        }).state('schoolstaff.list_material', {
            url: '/material/list',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageMaterials/listMaterials.html',
            controller: 'listMaterialController'
        }).state('schoolstaff.edit_material', {
            url: '/material/edit/:material_id',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageMaterials/editMaterial.html',
            controller: 'editMaterialController'
        })
        //Manage Staff Time-Tables / Lectures
        .state('schoolstaff.create_lecture', {
            url: '/lecture/create',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageLectures/createLecture.html',
            controller: 'createStaffLectureController'
        })
        .state('schoolstaff.list_lecture', {
            url: '/lecture/list',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageLectures/listLecture.html',
            controller: 'listStaffLectureController'
        })
        .state('schoolstaff.edit_lecture', {
            url: '/lecture/edit/:lecture_id',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageLectures/editLecture.html',
            controller: 'editStaffLectureController'
        })
        //Manage Myclasses
        .state('schoolstaff.list_myschedules', {
            url: '/myschedules/list',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageMyClasses/listMyClasses.html',
            controller: 'myScheduleListStaffController'
        })
        //Manage Exams
        .state('schoolstaff.exam_list', {
            url: '/exam/list',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageExam/listExam.html',
            controller: 'examListStaffController'
        })
        .state('schoolstaff.exam_create', {
            url: '/exam/create',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageExam/createExam.html',
            controller: 'examCreateStaffController'
        })
        .state('schoolstaff.exam_edit', {
            url: '/exam/edit/:exam_id',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageExam/editExam.html',
            controller: 'examEditStaffController'
        })
        .state('schoolstaff.exam_qa_list', {
            url: '/exam/qa/list/:exam_id',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageExam/listQuestionAnswer.html',
            controller: 'listExamQuestionAnswerController'
        })
        .state('schoolstaff.exam_qa_create', {
            url: '/exam/qa/create/:exam_id',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageExam/createQuestionAnswer.html',
            controller: 'createExamQuestionAnswerController'
        })
        //Manage Exam Results
        .state('schoolstaff.exam_result_list', {
            url: '/exam_result/list',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageExamResult/listExamResult.html',
            controller: 'examResultListStaffController'
        })
        .state('schoolstaff.exam_result_create', {
            url: '/exam_result/create',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageExamResult/createExamResult.html',
            controller: 'examResultCreateStaffController'
        })
        .state('schoolstaff.exam_result_edit', {
            url: '/exam_result/edit/:result_id',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageExamResult/editExamResult.html',
            controller: 'examResultEditStaffController'
        })
        // Manage Assignment
        .state('schoolstaff.create_assignment', {
            url: '/assignment/create',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageAssignments/createAssignments.html',
            controller: 'createAssignmentController'
        })
        .state('schoolstaff.list_assignment', {
            url: '/assignment/list',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageAssignments/listAssignments.html',
            controller: 'listAssignmentController'
        })
        .state('schoolstaff.edit_assignment', {
            url: '/assignment/edit/:assignment_id',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageAssignments/editAssignments.html',
            controller: 'editAssignmentController'
        })
        .state('schoolstaff.assignment_report_list', {
            url: '/assignment_report/list/:assignment_id',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageAssignments/listAssignmentReport.html',
            controller: 'listStaffAssignmentReportController'
        })
        .state('schoolstaff.view_exam_report', {
            url: '/exam/report_view/:exam_id/:student_id',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageExam/reportExam.html',
            controller: 'staffExamReportController'
        })
        // Manage Homework
        .state('schoolstaff.homework_create', {
            url: '/homework/create',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageHomework/createHomework.html',
            controller: 'createHomeworkController'
        })
        .state('schoolstaff.homework_list', {
            url: '/homework/list',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageHomework/listHomework.html',
            controller: 'listHomeworkController'
        })
        .state('schoolstaff.homework_report_list', {
            url: '/homework_report/list/:homework_id',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageHomework/listHomeworkReport.html',
            controller: 'listHomeworkReportController'
        })
        .state('schoolstaff.homework_edit', {
            url: '/homework/edit/:homework_id',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageHomework/editHomework.html',
            controller: 'editHomeworkController'
        })
        // Manage Attendance
        .state('schoolstaff.create_attendance', {
            url: '/attendance/create',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageAttendence/createAttendence.html',
            controller: 'createAttendanceController'
        })
        .state('schoolstaff.list_attendance', {
            url: '/attendance/list',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageAttendence/listAttendence.html',
            controller: 'listAttendanceController'
        })
        .state('schoolstaff.edit_attendance', {
            url: '/attendance/edit/:attendance_id',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageAttendence/editAttendence.html',
            controller: 'editAttendanceController'
        })
        .state('schoolstaff.attendance_report_list', {
            url: '/attendance_report/list/:attendance_id',
            templateUrl: '/partials/superAdmin/district/manageStaff/manageAttendence/listAttendanceReport.html',
            controller: 'listAttendanceReportController'
        })
        .state('schoolstaff.promote_student', {
            url: '/promote/students',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/promoteStudents/promoteStudents.html',
            controller: 'promoteStudentsController'
        })
        //Manage Video Conference
        .state('schoolstaff.video_conference', {
            url: '/video_conference/create',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageVideoConference/createConference.html',
            controller: 'createVideoConferenceController'
        })
        .state('schoolstaff.video_conference_live', {
            url: '/video_conference/live',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageVideoConference/groupCall.html'
                //controller: 'videoConferenceLiveController'
        })
        //Manage Student Classes
        .state('schoolstaff.manage_student_classes', {
            url: '/manage/student_classes',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageStudentClasses/addStudentClasses.html',
            controller: 'addStudentClassesController'
        })
        //Manage Multiple Classes
        .state('schoolstaff.manage_student_multiple_classes', {
            url: "/classes/addmultipleclasses",
            templateUrl: "/partials/superAdmin/district/school/manageStaff/manageMultipleClasses/listMultipleClasses.html",
            controller: 'manageMultipleClassesController'
        })
        //Manage Events
        .state('schoolstaff.event_list', {
            url: '/event/list',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageEvents/listEvents.html',
            controller: 'listEventsController'
        })
        .state('schoolstaff.event_gallery_view', {
            url: '/gallery/view/:gallery_id',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/manageEvents/eventGallery.html',
            controller: 'viewEventGalleryAlbumController'
        })
        //Manage Staff Logout
        .state('staffSignout', {
            url: "/staff/signout",
            templateUrl: "/partials/auth/signout/staffSignout.html",
            controller: 'staffLogoutController'
        })
        //Mange BookStore
        .state('schoolstaff.list_book_store', {
            url: '/bookstore/list',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/bookStore/listBookStore.html',
            controller: 'listStaffBookStoreController'
        })
        .state('schoolstaff.create_book_store', {
            url: '/bookstore/create',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/bookStore/createBookStore.html',
            controller: 'createStaffBookStoreController'
        })
        .state('schoolstaff.edit_book_store', {
            url: '/bookstore/edit/:book_id',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/bookStore/editBookStore.html',
            controller: 'editStaffBookStoreController'
        })
        .state('schoolstaff.view_book', {
            url: '/bookstore/view/:book_id',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/bookStore/viewBooks.html',
            controller: 'viewStaffBookStoreController'
        })
        .state('schoolstaff.view_epub_book', {
            url: '/bookstore/epub/view/:book_id',
            templateUrl: '/partials/superAdmin/district/school/manageStaff/bookStore/viewEpubBook.html',
            controller: 'viewStaffEpubBookStoreController'
        })

    $locationProvider.html5Mode(true);

});
