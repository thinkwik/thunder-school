var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var BookStore = db.define('bookstore', {
  category: String,
  publisher_name: String,
  title: String,
  image: String,
  book: String,
  isbn_number: String,
  author_name: String,
  pages: Number,
  description: String,
  status: ['0', '1'],
}, {
    timestamp: true,
    methods: {

    }
});
