  app.controller('viewStudentProfileController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
      function($scope, $http, $state, toaster, $cookieStore) {
          this.tab = 3;

          this.setTab = function(tabId) {
              this.tab = tabId;
          };

          this.isSet = function(tabId) {
              return this.tab === tabId;
          };
      }
  ]);

  app.controller('getStaffEmailListController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
      function($scope, $http, $state, toaster, $cookieStore) {
          $scope.listStaff = function() {
              var data = {
                  school_id: $cookieStore.get('loggedInStudent').school_id
              };
              $http({
                  method: 'POST',
                  url: '/send/staff',
                  data: data
              }).then(function successCallback(response) {
                  var staffs = response.data;
                  $scope.staffs = staffs;
                  var arrOfStaffs = [];
                  for (var i = 0; i < staffs.length; i++) {
                      eachStaffs = staffs[i].email_id;
                      arrOfStaffs.push(eachStaffs);
                  }
                  for (var x = 0; i < arrOfStaffs.length; i++) {
                      var obj = {};
                      for (var j = 0; j < arrOfStaffs[x].length; j++) {
                          obj[text[j]] = arrOfStaffs[x][j];
                      }
                      arrOfStaffs.push(obj);
                  }
                  var staff_emails = arrOfStaffs;
                  $scope.loadStaffs = function(query) {
                      return staff_emails;
                  };
              }, function errorCallback(error) {
                  toaster.pop('error', 'Staff', 'cannot get all the Staff! try reloading page.');
              });
          };

          $scope.sendEmailToStaff = function() {
            var data = $scope.staffMail;
            data.email_from = $cookieStore.get('loggedInStudent').email;
            tags = data.tags;
            var emails = [];
            for (var i = 0; i < tags.length; i++) {
                emails.push(data.tags[i].text);
            }
            data.email_to = emails.join(",");
            if (data.email_to != 'undefined' && data.email_to != "") {
                $http({
                    method: 'POST',
                    url: '/staff/sendmail',
                    data: data
                }).then(function successCallback(response) {
                    $scope.master = {};
                    $scope.staffMail = angular.copy($scope.master);
                    toaster.pop('success', 'Email', 'Sent successfully!');
                }, function errorCallback(error) {
                    toaster.pop('error', 'Email', 'Something went wrong!');
                });
            } else {
                toaster.pop("error", "Please add emails");
            }
        };


      }
  ]);

  app.controller('getStudentEmailListController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
      function($scope, $http, $state, toaster, $cookieStore) {
          $scope.listStudent = function() {
              var data = {
                  school_id: $cookieStore.get('loggedInStudent').school_id
              };
              $http({
                  method: 'POST',
                  url: '/student/mails',
                  data: data
              }).then(function successCallback(response) {
                  var students = response.data;
                  $scope.students = students;
                  var arrOfStudents = [];
                  for (var i = 0; i < students.length; i++) {
                      eachStudents = students[i].email;
                      arrOfStudents.push(eachStudents);
                  }
                  for (var x = 0; i < arrOfStudents.length; i++) {
                      var obj = {};
                      for (var j = 0; j < arrOfStudents[x].length; j++) {
                          obj[text[j]] = arrOfStudents[x][j];
                      }
                      arrOfStudents.push(obj);
                  }
                  var data = arrOfStudents;
                  $scope.loadTags = function(query) {
                      return data;
                  };
              }, function errorCallback(error) {
                  toaster.pop('error', 'Staff', 'cannot get all the Staff! try reloading page.');
              });
          };

          $scope.sendEmail = function() {
            var data = $scope.studentMail;
            data.email_from = $cookieStore.get('loggedInStudent').email;
            tags = data.tags;
            var emails = [];
            for (var i = 0; i < tags.length; i++) {
                emails.push(data.tags[i].text);
            }
            data.email_to = emails.join(",");
            if (data.email_to != 'undefined' && data.email_to != "") {
                $http({
                    method: 'POST',
                    url: '/student/sendmail',
                    data: data
                }).then(function successCallback(response) {
                    $scope.master = {};
                    $scope.studentMail = angular.copy($scope.master);
                    toaster.pop('success', 'Email', 'Sent successfully!');
                }, function errorCallback(error) {
                    toaster.pop('error', 'Email', 'Something went wrong!');
                });
            } else {
                toaster.pop("error", "Please add emails");
            }
        };
      }
  ]);

  app.controller('emailListController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', '$filter', '$resource',
      function($scope, $http, $state, toaster, $cookieStore, $filter, $resource) {}
  ]);

  app.controller('listStudentEmailController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
      function($scope, $http, $state, toaster, $cookieStore) {
          $scope.listStudent = function() {
              var data = {
                  email: $cookieStore.get('loggedInStudent').email
              };
              $http({
                  method: 'POST',
                  url: '/students/emails/list',
                  data: data
              }).then(function successCallback(response) {
                  var mails = response.data;
                  $scope.mails = mails;
              }, function errorCallback(error) {
                  toaster.pop('error', 'Emails', 'cannot get all the Emails! try reloading page.');
              });
          };

          $scope.deleteMail = function(mail_id) {
              var data = {
                  id: mail_id
              };
              var result = confirm("Please confirm, Do you really want to delete ?");
              if (result) {
                  $http({
                      method: 'POST',
                      url: '/students/mail/delete',
                      data: data
                  }).then(function successCallback(response) {
                      $scope.listStudent();
                  }, function errorCallback(error) {
                      toaster.pop('error', 'Issue', error.data.message);
                  });
              }
          };

          $scope.viewMail = function(mail_id) {
              $state.go('students.email_view', {
                  mail_id: mail_id
              });
          };
      }
  ]);

  app.controller('viewStudentEmailController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
      function($scope, $http, $state, toaster, $cookieStore) {
          $scope.getMail = function() {
              $http({
                  method: 'POST',
                  url: '/student/mail/edit?id=' + $state.params.mail_id
              }).then(function successCallback(response) {
                  $scope.mail = response.data;
              }, function errorCallback(error) {
                  toaster.pop('error', 'Mail', 'cannot get all the Mail! try reloading page.');
              });
          };

          $scope.sendMail = function() {
              console.log($scope);
              var data = {
                  email_to: $scope.mail.email_from,
                  email_from: $scope.mail.email_to,
                  subject: $scope.mail.subject,
                  content: $scope.mail.content
              };
              $http({
                  method: 'POST',
                  url: '/send/mail/staff',
                  data: data
              }).then(function successCallback(response) {
                  toaster.pop('success', 'Email', 'Sent successfully!');
              }, function errorCallback(error) {
                  console.log(error);
                  toaster.pop('error', 'Mail', 'cannot send Mail!');
              });
          };
      }
  ]);

app.controller('listStudentEmailController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        this.tab_sub = 1;
        this.setTab_sub = function(tabId) {
            this.tab_sub = tabId;
        };
        this.isSet_sub = function(tabId) {
            return this.tab_sub === tabId;
        };
    }
]);

app.controller('studentAllSentReceiveEmailListController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.studentAllSentReceiveEmailList = function(){
            $scope.studentAllSentEmailList();
            $scope.studentAllReceivedEmailList();
        };
        $scope.studentAllSentEmailList = function(){
            var data = {
                email_id: $cookieStore.get('loggedInStudent').email
            };
            $http({
                method: 'POST',
                url: '/mail/sentList',
                data: data
            }).then(function successCallback(response) {
                $scope.email_lists = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Mail', error.message.data);
            });
        };
        $scope.studentAllReceivedEmailList = function(){
            var data = {
                email_id: $cookieStore.get('loggedInStudent').email
            };
            $http({
                method: 'POST',
                url: '/mail/receivedList',
                data: data
            }).then(function successCallback(response) {
                $scope.email_received_lists = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Mail', error.message.data);
            });
        };
    }
]);
