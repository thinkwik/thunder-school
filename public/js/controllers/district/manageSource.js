app.controller('createSourceController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore',
    function($scope, $http, $state, toaster, Upload, $cookieStore) {
        $scope.createSource = function() {
            data = $scope.source;
            data.school_id = $cookieStore.get('loggedInSchool').id;
            $http({
                    url: '/source/register',
                    method: "POST",
                    data: data
                })
                .then(function(response) {
                        $state.go('schools.source_list');
                        toaster.pop('success', "Source", "Source created successfully!!!");
                    },
                    function(response) {
                        toaster.pop('error', "Error", "Something went wrong!!!");
                    });
        };

        $scope.cancelSource = function() {
            $state.go('schools.source_list');
        };
    }
]);

app.controller('listSourceController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore',
    function($scope, $http, $state, toaster, Upload, $cookieStore) {
        $scope.listSource = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/source/getAllSources',
                data: data
            }).then(function successCallback(response) {
                var sources = response.data;
                $scope.sources = sources;
            }, function errorCallback(error) {
                toaster.pop('error', 'Source', 'cannot get all the source! try reloading page.');
                console.log(error.data.message);
            });
        };


        $scope.deleteSource = function(source_id) {
            var data = {
                id: source_id
            };
            console.log(data);
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/source/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.listSource();
                }, function errorCallback(error) {
                    console.log(error.data.message);
                });
            }
        };

        $scope.editSource = function(source_id) {
            $state.go('schools.source_edit', {
                source_id: source_id
            });
        };

        $scope.addNewSource = function() {
            $state.go('schools.source_create');
        };
    }
]);

app.controller('editSourceController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore',
    function($scope, $http, $state, toaster, Upload, $cookieStore) {
        $scope.cancelSource = function() {
            $state.go('schools.source_list');
        };

        $scope.saveEditSource = function() {
            var data = $scope.source;
            $http({
                method: 'POST',
                url: '/source/edit',
                data: data
            }).then(function successCallback(response) {
                $state.go('schools.source_list');
                toaster.pop('success', 'Source', 'Updated Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Source', error.data.message);
            });
        };

        $scope.getEditSource = function() {
            $http({
                method: 'GET',
                url: '/source/edit?id=' + $state.params.source_id
            }).then(function successCallback(response) {
                $scope.source = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Source', error.data.message);
            });
        };
    }
]);
