var db = require('orm').db,
    Lectures = db.models.lectures;

exports.register = function(req, res) {
    newLecture = new Lectures();
    newLecture.school_id = req.body.school_id;
    newLecture.session_id = req.body.session_id;
    newLecture.staff_id = req.body.staff_id;
    newLecture.grade_id = req.body.grade_id;
    newLecture.department_id = req.body.department_id;
    newLecture.subject_id = req.body.subject_id;
    newLecture.date = req.body.date;
    newLecture.start_time = req.body.start_time;
    newLecture.end_time = req.body.end_time;
    newLecture.save(function(err, created_lecture) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 422, "Bad Request check your parameters");
        }
        var response = {};
        response['data'] = [];
        response['code'] = 200;
        return res.send(response);
    });
};

exports.edit = function(req, res) {
    if (req.body.id && req.body.date && req.body.start_time && req.body.end_time && req.body.session_id && req.body.department_id) {
        Lectures.find({
            id: req.body.id
        }, 1, function(err, lectures) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 422, "Bad Request check your parameters");
            }
            lectures[0].session_id = req.body.session_id;
            lectures[0].department_id = req.body.department_id;
            lectures[0].date = req.body.date;
            lectures[0].start_time = req.body.start_time;
            lectures[0].end_time = req.body.end_time;
            lectures[0].save(function(err, updated_lectures) {
                if (err) {
                    console.log(err);
                    return apiSendError(req, res, 422, "Bad Request check your parameters");
                }
                var response = {};
                response['data'] = updated_lectures;
                response['code'] = 200;
                return res.send(response);
            });
        });
    } else {
        return apiSendError(req, res, 422, "Bad Request check your parameters");
    }
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return apiSendError(req, res, 404, 'Lecture id required!!');
    }
    Lectures.find({
        id: req.body.id
    }, function(error, lecturess) {
        if (error) {
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!lecturess.length) {
            return sendError(req, res, 404, "Lectures not found");
        }
        lecturess[0].remove(function(err) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 400, "Bad Request check your parameters");
            }
            var response = {};
            response['data'] = [];
            response['code'] = 200;
            return res.send(response);
        });
    })
};

exports.getLecture = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return apiSendError(req, res, 404, 'Lecture id required!!');
    }
    Lectures.find({
        id: req.body.id
    }, 1, function(err, lectures) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 422, "Bad Request check your parameters");
        }
        if (!lectures.length) {
            return apiSendError(req, res, 404, "lectures not found");
        }
        var response = {};
        response['data'] = lectures[0];
        response['code'] = 200;
        return res.send(response);
    });
};

exports.getAllLecturesSchoolWise = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id == 'null') {
        return apiSendError(req, res, 422, "refer parameters");
    }
    var query_str = 'SELECT t1.*,CONCAT(t2.first_name," ",t2.last_name) as staff_name,t3.grade,t4.subject_name,t5.session_name,t6.department_name FROM lectures as t1 LEFT JOIN staff as t2 on t1.staff_id = t2.id LEFT JOIN grades as t3 on t1.grade_id = t3.id LEFT JOIN subjects as t4 on t1.subject_id = t4.id LEFT JOIN sessions as t5 on t1.session_id = t5.id LEFT JOIN departments as t6 on t1.department_id = t6.id WHERE t1.school_id = ?';
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 422, "Bad Request check your parameters");
        }
        if (!rows.length) {
            return apiSendError(req, res, 404, "Lectures not found");
        }
        var response = {};
        response['data'] = rows;
        response['code'] = 200;
        return res.send(response);
    });
};

exports.getAllLecturesStaffWise = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id == 'null' && req.body.staff_id == 'undefined' || req.body.staff_id == 'null') {
        return apiSendError(req, res, 422, "refer parameters");
    }
    var query_str = `SELECT t1.*,CONCAT(t2.first_name," ",t2.last_name) as staff_name,t3.grade,t4.subject_name,t5.session_name,t6.department_name
                     FROM lectures as t1
                     LEFT JOIN staff as t2 on t1.staff_id = t2.id
                     LEFT JOIN grades as t3 on t1.grade_id = t3.id
                     LEFT JOIN subjects as t4 on t1.subject_id = t4.id
                     LEFT JOIN sessions as t5 on t1.session_id = t5.id
                     LEFT JOIN departments as t6 on t1.department_id = t6.id
                     WHERE t1.school_id = ? AND t1.staff_id = ? ORDER BY t1.date DESC`;
    db.driver.execQuery(query_str, [req.body.school_id, req.body.staff_id], function(err, rows) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 422, "Bad Request check your parameters");
        }
        if (!rows.length) {
            return apiSendError(req, res, 404, "Lectures not found");
        }
        var response = {};
        response['data'] = rows;
        response['code'] = 200;
        return res.send(response);
    });
};
