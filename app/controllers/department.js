var bCrypt = require('bcrypt-nodejs');
var db = require('orm').db,
    School = db.models.schools,
    Department = db.models.departments;

exports.register = function(req, res) {
    if (typeof req.body.school_id != 'undefined') {
        var new_deprtment = new Department();
        new_deprtment.school_id = req.body.school_id;
        new_deprtment.department_name = req.body.department_name;
        new_deprtment.department_description = req.body.department_description;
        new_deprtment.status = (typeof req.body.status != 'undefined') ? req.body.status : "1";
        new_deprtment.save(function(err, saved_dapratment) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            res.send(saved_dapratment);
        });

    } else {
        return sendError(req, res, 422, 'school_id required for register new department');
    }
};

exports.getDepartment = function(req, res) {
    if (typeof req.query.id == 'undefined') return res.send("id required for getting json");
    Department.find({
        id: req.query.id
    }, 1, function(err, departments) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!departments.length) return res.send("department not found with perticular id");
        return res.send(departments[0]);
    });
};

exports.getAllDistDepartment = function(req, res) {
    if (typeof req.body.district_id == 'undefined' || req.body.district_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = "SELECT t1.*,t2.school_name FROM `departments` as t1 LEFT JOIN schools as t2 on t1.school_id = t2.id WHERE  t2.parent_id = ?";
    db.driver.execQuery(query_str, [req.body.district_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};


exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'department id required');
    }
    Department.find({
        id: req.body.id
    }, 1, function(err, departments) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!departments.length) {
            return sendError(req, res, 404, "Department not found");
        }
        departments[0].department_name = req.body.department_name;
        departments[0].department_description = req.body.department_description;
        departments[0].school_id = req.body.school_id;
        departments[0].status = req.body.status;
        departments[0].save(function(err, updated_dapartment) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            return res.send(updated_dapartment);
        });
    });
};

exports.active = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'department id required');
    }
    Department.find({
        id: req.body.id
    }, 1, function(err, departments) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        departments[0].status = "1";
        departments[0].save(function(err, activated_dapartment) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            return res.send(activated_dapartment);
        });
    });
};

exports.inactive = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'department id required');
    }
    Department.find({
        id: req.body.id
    }, 1, function(err, departments) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        departments[0].status = "0";
        departments[0].save(function(err, inactivated_dapartment) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            return res.send(inactivated_dapartment);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'department id required');
    }
    Department.find({
        id: req.body.id
    }, 1, function(err, departments) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        departments[0].remove(function(err) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            return res.send(departments[0]);
        });
    });
};

exports.getAllSchoolDepartment = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = "SELECT t1.*,t2.school_name FROM `departments` as t1 LEFT JOIN schools as t2 on t1.school_id = t2.id WHERE  t2.id = ?";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
