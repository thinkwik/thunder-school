var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';
var config = {
    development: {
        root: rootPath,
        app: {
            name: 'thunder_school'
        },
        port: 3000,
        db: 'mysql://root:thinkwik10@localhost/thunder_school'
    },
    test: {
        root: rootPath,
        app: {
            name: 'thunder-school'
        },
        port: 3000,
        db: 'mysql://root@localhost/thunder_school_test'
    },
    production: {
        root: rootPath,
        app: {
            name: 'thunder-school'
        },
        port: 8080,
        db: 'mysql://socrates_root:Thinkwik@123@localhost/socrates_thunder_school'
    }
};
module.exports = config[env];
