app.config(['$compileProvider', function($compileProvider) {
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(|blob|):/);
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|file|blob|pdf|doc):/);
}]);

app.controller('listStudentAssignmentController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', '$window',
    function($scope, $http, $state, toaster, $cookieStore, $window) {
        $scope.getAllAssignmentsForStudents = function() {
            var data = {
                student_id: $cookieStore.get('loggedInStudent').id
            };
            $http({
                method: 'POST',
                url: '/assignment/getAllStudentAssignments',
                data: data
            }).then(function successCallback(response) {
                var assignments = response.data;
                $scope.assignments = assignments;
            }, function errorCallback(error) {
                toaster.pop('error', 'Assignments', 'cannot get all the Assignments! try reloading page.');
            });
        };

        $scope.submitAssignment = function(assignment) {
            assignment_id = assignment.id;
            $state.go('students.assignment_reports', {
                assignment_id: assignment_id
            });
        };

        $scope.DownloadAssignment = function(assignment) {
            if (assignment.file_name) {
                var assFileName = assignment.file_name;
                assFileName = assFileName.split(".");
                var url = $window.location.origin + '/upload/assignments/' + assignment.file_name;
                var request = new XMLHttpRequest();
                request.open('HEAD', url, false);
                request.send();
                if (request.status == 200) {
                    var element = angular.element('<a/>');
                    element.attr({
                        href: url,
                        target: '_blank',
                        download: assignment.title + '.' + assFileName[1]
                    })[0].click();
                } else {
                    toaster.pop('error', 'Assignments', 'Assignment not found!');
                    return false;
                }
            } else {
                toaster.pop('error', 'Assignments', 'Assignment not found!');
                return false;
            }
        };
    }
]);

app.controller('createAssignmentController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        $scope.getSchoolGrades = function() {
            var data = {
                school_id: $cookieStore.get('loggedInStaff').school_id
            };
            $http({
                method: 'POST',
                url: '/grade/getAllSchoolGrades',
                data: data
            }).then(function successCallback(response) {
                $scope.grades = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Grades', 'Something went wrong!');
            });
        };

        $scope.getSubject = function(grade_id) {
            $scope.classes = [];
            $scope.subjects = [];
            $scope.assignment.class_id = "";
            $scope.assignment.subject_id = "";
            var data = {
                grade_id: grade_id,
                school_id: $cookieStore.get('loggedInStaff').school_id
            };
            $http({
                method: 'POST',
                url: '/subject/getSchoolGradesSubjectWise',
                data: data
            }).then(function successCallback(response) {
                $scope.subjects = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Subjects', 'Something went wrong!');
            });
        };
        $scope.getClassSubjectWise = function(subject_id) {
            var data = {
                subject_id: subject_id
            };
            $http({
                method: 'POST',
                url: '/class/getClassSubjectWise',
                data: data
            }).then(function successCallback(response) {
                $scope.classes = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Student Classes', error.data.message);
            });
        };

        $scope.uploadFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected !');
            Loading(true);
            Upload.upload({
                url: '/assignment/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.filename = success.data;
                    $scope.saveDisable = true;
                },
                function(error) {
                    Loading(false);
                    toaster.pop('error', "Error", error.data.message);
                    $scope.saveDisable = false;
                });
        };

        $scope.listAssignments = function() {
            $state.go('schoolstaff.list_assignment');
        };

        $scope.createAssignments = function() {
            data = $scope.assignment;
            data.school_id = $cookieStore.get('loggedInStaff').school_id;
            data.file_name = $scope.filename;
            data.staff_id = $cookieStore.get('loggedInStaff').id;
            $http({
                method: "POST",
                url: "/assignment/register",
                data: data
            }).then(function successCallback(response) {
                $state.go('schoolstaff.list_assignment');
                toaster.pop('success', 'Assignment', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Assignment', error.data.message);
            });
        };
    }
]);

app.controller('listAssignmentController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', '$window',
    function($scope, $http, $state, toaster, $cookieStore, $window) {
        $scope.addNewAssignment = function() {
            $state.go('schoolstaff.create_assignment');
        };

        $scope.deleteAssignment = function(assignment_id) {
            var data = {
                id: assignment_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/assignment/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllAssignments();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Assignment', error.data.message);
                });
            }
        };

        $scope.getAllAssignments = function() {
            var data = {
                staff_id: $cookieStore.get('loggedInStaff').id
            };
            $http({
                method: 'POST',
                url: '/assignment/getAllAssignments',
                data: data
            }).then(function successCallback(response) {
                var newAssignmentArr = [];
                var assignments = response.data;
                for (var i = 0; i < assignments.length; i++) {
                    if (new Date() > new Date(assignments[i].created_at) && new Date() < new Date(assignments[i].due_date)) {
                        newAssignmentArr.push(assignments[i]);
                    } else {
                        console.log();
                    }
                }
                $scope.assignments = newAssignmentArr;
            }, function errorCallback(error) {
                toaster.pop('error', 'Assignments', 'cannot get all the Assignments! try reloading page.');
            });
        };

        $scope.editAssignment = function(assignment_id) {
            $state.go('schoolstaff.edit_assignment', {
                assignment_id: assignment_id
            });
        };

        $scope.assignmentReport = function(assignment_id) {
            $state.go('schoolstaff.assignment_report_list', {
                assignment_id: assignment_id
            });
        };

        $scope.downloadAssignment = function(assignment) {
            var assFileName = assignment.file_name;
            assFileName = assFileName.split(".");
            if (assignment.file_name) {
                var url = $window.location.origin + '/upload/assignments/' + assignment.file_name;
                var request = new XMLHttpRequest();
                request.open('HEAD', url, false);
                request.send();
                if (request.status == 200) {
                    var element = angular.element('<a/>');
                    element.attr({
                        href: url,
                        target: '_blank',
                        download: assignment.title + '.' + assFileName[1]
                    })[0].click();
                } else {
                    toaster.pop('error', 'Assignments', 'Assignment not found!');
                    return false;
                }
            } else {
                toaster.pop('error', 'Assignments', 'Assignment not found!');
                return false;
            }
        }
    }
]);

app.controller('listStaffAssignmentReportController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload', '$window',
    function($scope, $http, $state, toaster, $cookieStore, Upload, $window) {
        $scope.getStudentsAssignment = function() {
            var data = {
                assignment_id: $state.params.assignment_id
            };
            $http({
                method: 'POST',
                url: '/assignment/getAllStudentList',
                data: data
            }).then(function successCallback(response) {
                $scope.assignment_report_lists = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Assignment', 'cannot get all Students Assignments! try reloading page.');
            });
        };

        $scope.listAssignment = function() {
            $state.go('schoolstaff.list_assignment');
        };

        $scope.DownloadStudentAssignment = function(uploaded_assignment) {
            if (uploaded_assignment.file_path) {
              var fileName = uploaded_assignment.file_name;
              fileName = uploaded_assignment.split(".");
                var url = $window.location.origin + '/upload/studentassignments/' + uploaded_assignment.file_path;
                var request = new XMLHttpRequest();
                request.open('HEAD', url, false);
                request.send();
                if (request.status == 200) {
                    var element = angular.element('<a/>');
                    element.attr({
                        href: url,
                        target: '_blank',
                        download: uploaded_assignment.student_name  + '.' + fileName[1]
                    })[0].click();
                } else {
                    toaster.pop('error', 'Assignments', 'Assignment not found!');
                    return false;
                }
            } else {
                toaster.pop('error', 'Assignments', 'Assignment not found!');
                return false;
            }
        };

        $scope.studentCompletedAssignment = function(assignment_report_list) {
            var data = {
                id: assignment_report_list
            };
            $http({
                method: 'POST',
                url: '/assignment_report/completed',
                data: data
            }).then(function successCallback(response) {
                $scope.getStudentsAssignment();
            }, function errorCallback(error) {
                toaster.pop('error', 'Assignment Reports', error.data.message);
            });
        };

        $scope.studentInCompletedAssignment = function(assignment_report_list) {
            var data = {
                id: assignment_report_list
            };
            $http({
                method: 'POST',
                url: '/assignment_report/notCompleted',
                data: data
            }).then(function successCallback(response) {
                $scope.getStudentsAssignment();
            }, function errorCallback(error) {
                toaster.pop('error', 'Assignment Reports', error.data.message);
            });
        };

        $scope.updateStudentGrades = function(assignment_report) {
            var data = {
                id: assignment_report.id,
                achieved_grade: assignment_report.achieved_grade
            };
            $http({
                method: 'POST',
                url: '/update/grades',
                data: data
            }).then(function successCallback(response) {
                $scope.getStudentsAssignment();
                toaster.pop('success', 'Assignment Reports', 'Grades updated successfully!');
            }, function errorCallback(error) {
                toaster.pop('error', 'Assignment Reports', error.data.message);
            });
        };
    }
]);

app.controller('editAssignmentController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        $scope.getSchoolGrades = function() {
            var data = {
                school_id: $cookieStore.get('loggedInStaff').school_id
            };
            $http({
                method: 'POST',
                url: '/grade/getAllSchoolGrades',
                data: data
            }).then(function successCallback(response) {
                $scope.grades = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Grades', 'Something went wrong!');
            });
        };

        $scope.getSubject = function(grade_id) {
            var data = {
                grade_id: grade_id,
                school_id: $cookieStore.get('loggedInStaff').school_id
            };
            $http({
                method: 'POST',
                url: '/subject/getSchoolGradesSubjectWise',
                data: data
            }).then(function successCallback(response) {
                $scope.subjects = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Subjects', 'Something went wrong!');
            });
        };

        $scope.getClass = function(subject_id) {
            var data = {
                subject_id: subject_id
            };
            $http({
                method: 'POST',
                url: '/class/getAllSubjectClasses',
                data: data
            }).then(function successCallback(response) {
                $scope.classes = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Subjects', 'Something went wrong!');
            });
        };

        $scope.uploadFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected !');
            Loading(true);
            Upload.upload({
                url: '/assignment/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.filename = success.data;
                    $scope.saveDisable = true;
                },
                function(error) {
                    toaster.pop('error', "Error", error.data.message);
                    $scope.saveDisable = false;
                });
        };

        $scope.getEditAssignment = function() {
            $http({
                method: 'GET',
                url: '/assignment/edit?id=' + $state.params.assignment_id
            }).then(function successCallback(response) {
                $scope.assignment = response.data;
                $scope.getSchoolGrades();
                $scope.getSubject(response.data.grade_id);
                $scope.getClass(response.data.subject_id);
            }, function errorCallback(error) {
                toaster.pop('error', 'Assignment', 'cannot get all the Assignments! try reloading page.');
            });
        };

        $scope.saveEditAssignment = function() {
            data = $scope.assignment;
            $http({
                method: 'POST',
                url: '/assignment/edit',
                data: data
            }).then(function successCallback(response) {
                $state.go('schoolstaff.list_assignment');
                toaster.pop('success', 'Assignment', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Assignment', error.data.message);
            });
        };

        $scope.listAssignments = function() {
            $state.go('schoolstaff.list_assignment');
        };

    }
]);

app.controller('studentAssignmentReportsController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', '$window', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, $window, Upload) {
        $scope.submitAssignment = function() {
            var data = {
                id: $state.params.assignment_id,
                student_submitted: $scope.filename
            };
            $http({
                method: 'POST',
                url: '/assignment/submission',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Assignments', 'Submitted successfully!');
                $state.go('students.assignment_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'Assignments', 'Something went wrong!');
            });
        };

        $scope.getAssignment = function() {
            var data = {
                id: $state.params.assignment_id,
                student_id: $cookieStore.get("loggedInStudent").id
            };
            $http({
                method: 'POST',
                url: '/assignment/getStudentsAssignment',
                data: data
            }).then(function successCallback(response) {
                $scope.assignment = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Assignment Report', 'Something went wrong!');
            });
        };

        $scope.listAssignment = function() {
            $state.go('students.assignment_list');
        };

        $scope.uploadFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected !');
            Loading(true);
            Upload.upload({
                url: '/assignment/upload/studentAssignment',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.filename = success.data;
                    $scope.saveDisable = true;
                },
                function(error) {
                    toaster.pop('error', "Error", error.data.message);
                    $scope.saveDisable = false;
                });
        };
    }
]);
