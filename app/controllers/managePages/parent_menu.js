var bCrypt = require('bcrypt-nodejs');
var db = require('orm').db,
    Parent_Menus = db.models.parent_menus;

exports.register = function(req, res) {
    new_parent_menus = new Parent_Menus();
    new_parent_menus.title = req.body.title;
    new_parent_menus.status = (typeof req.body.status != 'undefined') ? req.body.status : '1';
    new_parent_menus.save(function(err, saved_parent_menu) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(saved_parent_menu);
    });
};

exports.getParentMenu = function(req, res) {
    if (typeof req.query.id == 'undefined') {
        return sendError(req, res, 422, 'menu id required');
    }
    Parent_Menus.find({
        id: req.query.id
    }, 1, function(err, parentMenus) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!parentMenus.length) {
            return sendError(req, res, 404, "Menu not found");
        }
        return res.send(parentMenus[0]);
    });
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'menu id required');
    }
    Parent_Menus.find({
        id: req.body.id
    }, 1, function(err, parent_menus) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!parent_menus.length) {
            return sendError(req, res, 404, "menu not found");
        }
        parent_menus[0].title = req.body.title;
        parent_menus[0].status = (typeof req.body.status != 'undefined') ? req.body.status : parent_menus[0].status;
        parent_menus[0].save(function(err, saved_parent_menus) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "something went wrong in saving menu");
            }
            return res.send(saved_parent_menus);
        });
    });
};

exports.getAllParentMenus = function(req, res) {
    Parent_Menus.find({}, function(err, parent_menus) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(parent_menus);
    });
};

exports.active = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'menu id required');
    }
    Parent_Menus.find({
        id: req.body.id
    }, 1, function(err, parent_menus) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        parent_menus[0].status = "1";
        parent_menus[0].save(function(err, activated_parent_menu) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "something went wrong in activating menu");
            }
            return res.send(activated_parent_menu);
        });
    });
};

exports.inactive = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'menu id required');
    }
    Parent_Menus.find({
        id: req.body.id
    }, 1, function(err, parent_menus) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        parent_menus[0].status = "0";
        parent_menus[0].save(function(err, inactivated_parent_menu) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "something went wrong in inactivating menu");
            }
            return res.send(inactivated_parent_menu);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'menu id required');
    }
    Parent_Menus.find({
        id: req.body.id
    }, 1, function(err, parent_menus) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!parent_menus.length) {
            return sendError(req, res, 404, "Menu not found");
        }
        parent_menus[0].remove(function(err) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "Something went wrong in deleting menu!Please try again");
            }
            return res.send(parent_menus[0]);
        });
    });
};
