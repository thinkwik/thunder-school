var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Email_Templates = db.define('email_templates', {
    ui_key: String,
    ui_data: String
}, {
    timestamp: true,
    methods: {

    }
});
