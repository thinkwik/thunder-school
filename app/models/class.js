var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Classes = db.define('classes', {
    school_id: {
        type: 'integer'
    },
    class_name: String,
    staff_id: {
        type: 'integer'
    },
    department_id: {
        type: 'integer'
    },
    location_id: {
        type: 'integer'
    },
    grade_id: {
        type: 'integer'
    },
    subject_id: {
        type: 'integer'
    },
    day: String,
    start_time: String,
    end_time: String,
    max_students:Number,
    status: ['0', '1']
}, {
    timestamp: true,
    methods: {

    }
});
