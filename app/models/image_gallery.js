var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Image_gallery = db.define('image_gallery', {
    gallery_id: {
        type: 'integer'
    },
    image_name: String,
    image_description: String
}, {
    timestamp: true,
    methods: {

    }
});
