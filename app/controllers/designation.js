var db = require('orm').db,
    School = db.models.schools,
    Designations = db.models.designations;

exports.register = function(req, res) {
    if (typeof req.body.school_id != 'undefined') {
        newDesignation = new Designations();
        newDesignation.school_id = req.body.school_id;
        newDesignation.designation = req.body.designation;
        newDesignation.description = req.body.description;
        newDesignation.status = (typeof req.body.status != 'undefined') ? req.body.status : '1';
        newDesignation.save(function(err, created_designaion) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, "Bad Request check your parameters");
            }
            return res.send(created_designaion);
        });
    } else {
        return sendError(req, res, 422, 'school_id required for register new designation');
    }
};

exports.getAllDesignations = function(req, res) {
    if (typeof req.body.district_id == 'undefined' || req.body.district_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = "SELECT t1.*,t2.school_name FROM `designations` as t1 LEFT JOIN schools as t2 on t1.school_id = t2.id WHERE  t2.parent_id = ?";
    db.driver.execQuery(query_str, [req.body.district_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Designation id required!!');
    }
    Designations.find({
        id: req.body.id
    }, 1, function(err, designation) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        designation[0].school_id = req.body.school_id;
        designation[0].designation = req.body.designation;
        designation[0].description = req.body.description;
        designation[0].status = req.body.status;
        designation[0].save(function(err, updated_designation) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, "Bad Request check your parameters");
            }
            return res.send(updated_designation);
        });
    });
};

exports.active = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 400, 'Designation id required!!');
    }
    Designations.find({
        id: req.body.id
    }, 1, function(err, designation) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        designation[0].status = "1";
        designation[0].save(function(err, activated_designation) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(activated_designation);
        });
    });
};

exports.inactive = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Designation id required!!');
    }
    Designations.find({
        id: req.body.id
    }, 1, function(err, designation) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        designation[0].status = "0";
        designation[0].save(function(err, inactivated_designation) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(inactivated_designation);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Designation id required!!');
    }
    Designations.find({
        id: req.body.id
    }, 1, function(err, designations) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        designations[0].remove(function(err) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(designations[0]);
        });
    });
};

exports.getDesignation = function(req, res) {
    if (typeof req.query.id == 'undefined') return sendError(req, res, 401, "ID required field");
    Designations.find({
        id: req.query.id
    }, 1, function(err, designations) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        if (!designations.length) return sendError(req, res, 404, "Designations not found");
        return res.send(designations[0]);
    });
};


exports.getAllSchoolDesignations = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = "SELECT t1.*,t2.school_name FROM `designations` as t1 LEFT JOIN schools as t2 on t1.school_id = t2.id WHERE  t2.id = ?";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
