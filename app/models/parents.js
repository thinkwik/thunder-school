var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Parents = db.define('parents', {
    student_id: {
        type: 'integer'
    },
    unique_id: String,
    full_name: String,
    password:String,
    relation: String,
    gender: String,
    email: String,
    phone: String,
    mobile: String,
    work_phone_number: String,
    parentImage: String,
    address: String,
    city: String,
    state: String,
    zipcode:String,
    country: String,
    notes: String,
    forgot_password_token:String,
    status: ['0', '1'],
    profile_status: ['0', '1']
}, {
    timestamp: true,
    methods: {

    }
});
