app.controller('newsletterController', ['$scope', '$http', '$state', 'toaster',
    function($scope, $http, $state, toaster) {
        $scope.cancelNewsletterlist = function(files) {
            $state.go('users.newsleter_list');
        };

        $scope.addNewsletter = function() {
            $state.go('users.newsleter_create');
        };

        $scope.createNewsletter = function() {
            var data = $scope.newsletter;
            var users_to_understand = data.users_to_understand;
            var checkBoxes = document.getElementsByClassName('users_to_understand');
            var isChecked = false;
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].checked) {
                    isChecked = true;
                }
            }
            if (isChecked) {
                $scope.newsLetterArray = [];
                users_to_understand = $.map(users_to_understand, function(value, index) {
                    if (index) {
                        if (value === true) {
                            $scope.newsLetterArray.push(index);
                        }
                    }
                });
                data.users_to_understand = $scope.newsLetterArray.toString();
                $http({
                        url: '/letter/register',
                        method: "POST",
                        data: data
                    })
                    .then(function(response) {
                            toaster.pop('success', "Done", "Newsletter created successfully!!!");
                            $state.go('users.newsleter_list');
                        },
                        function(response) {
                            toaster.pop('error', "Error", "Something went wrong!!!");
                        });
            } else {
                toaster.pop('error', "Error", "Please Select atleast one checkbox!!!");
            }

        };

        $scope.listNewsletter = function() {
            var data = {
                offset: 'fdfdsfdfs'
            };
            $http({
                method: 'POST',
                url: '/letter/getAllnewsletters',
                data: data
            }).then(function successCallback(response) {
                var newsletters = response.data;
                $scope.newsletters = newsletters;
            }, function errorCallback(error) {
                toaster.pop('error', 'Newsletter', 'cannot get all the newsletter! try reloading page.');
            });
        };

        $scope.activeNewsletter = function(newsletter_id) {
            var data = {
                id: newsletter_id
            };
            $http({
                method: 'POST',
                url: '/letter/active',
                data: data
            }).then(function successCallback(response) {
                $scope.listNewsletter();
            }, function errorCallback(error) {
                toaster.pop('error', 'Newsletter', error.data.message);
            });
        };

        $scope.deleteNewsletter = function(newsletter_id) {
            var data = {
                id: newsletter_id
            };
            $http({
                method: 'POST',
                url: '/letter/delete',
                data: data
            }).then(function successCallback(response) {
                $scope.listNewsletter();
            }, function errorCallback(error) {
                toaster.pop('error', 'Newsletter', error.data.message);
            });
        };

        $scope.inactiveNewsletter = function(newsletter_id) {
            var data = {
                id: newsletter_id
            };
            $http({
                method: 'POST',
                url: '/letter/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.listNewsletter();
            }, function errorCallback(error) {
                toaster.pop('error', 'Newsletter', error.data.message);
            });
        };

        $scope.editNewsletter = function(newsletter_id) {
            $state.go('users.newsleter_edit', {
                newsletter_id: newsletter_id
            });
        };
    }
]);

app.controller('editNewsletterController', ['$scope', '$http', '$state', 'toaster',
    function($scope, $http, $state, toaster) {
        $scope.getEditNewsletter = function() {
            $http({
                method: 'GET',
                url: '/letter/edit?id=' + $state.params.newsletter_id
            }).then(function successCallback(response) {
                $scope.newsletter = response.data;
                var arr = response.data.users_to_understand;
                var array1 = arr.split(',');
                var data = {};
                for (var i = 0; i < array1.length; i++) {
                    data[array1[i]] = true;
                }
                $scope.newsletter.users_to_understand = data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Newsletter', 'cannot get all the newsletter! try reloading page.');
            });
        };

        $scope.saveEditNewsletter = function() {
            var data = $scope.newsletter;
            var users_to_understand = data.users_to_understand;
            var checkBoxes = document.getElementsByClassName('users_to_understand');
            var isChecked = false;
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].checked) {
                    isChecked = true;
                }
            }
            if (isChecked) {
                $scope.newsLetterArray = [];
                users_to_understand = $.map(users_to_understand, function(value, index) {
                    if (index) {
                        if (value === true) {
                            $scope.newsLetterArray.push(index);
                        }
                    }
                });
                data.users_to_understand = $scope.newsLetterArray.toString();
                $http({
                    method: 'POST',
                    url: '/letter/edit',
                    data: data
                }).then(function successCallback(response) {
                    toaster.pop('success', 'Newsletter', 'Saved Successfully.');
                    $state.go('users.newsleter_list');
                }, function errorCallback(error) {
                    toaster.pop('error', 'Newsletter', error.data.message);
                });
            } else {
                toaster.pop('error', 'Newsletter', "Please Select atleast one checkbox!!!");
            }
        };
    }
]);
