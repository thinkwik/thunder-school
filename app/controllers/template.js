var bCrypt = require('bcrypt-nodejs');
var db = require('orm').db,
    Templates = db.models.email_templates;

exports.register = function(req, res) {
    new_template = new Templates();
    new_template.ui_key = req.body.key;
    new_template.ui_data = req.body.ui_data;
    new_template.save(function(err, saved_template) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(saved_template);
    });
};

exports.getTemplate = function(req, res) {
    if (typeof req.query.id == 'undefined') {
        return sendError(req, res, 422, 'Template id required');
    }
    Templates.find({
        id: req.query.id
    }, function(err, templates) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!templates.length) {
            return sendError(req, res, 404, "Templates not found");
        }
        return res.send(templates[0]);
    });
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'Template id required');
    }
    Templates.find({
        id: req.body.id
    }, 1, function(err, templates) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!templates.length) {
            return sendError(req, res, 404, "Templates not found");
        }
        templates[0].ui_key = req.body.ui_key;
        templates[0].ui_data = req.body.ui_data;
        templates[0].save(function(err, updated_template) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "something went wrong in saving template");
            }
            return res.send(templates);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'Template id required');
    }
    Templates.find({
        id: req.body.id
    }, 1, function(err, templates) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!templates.length) {
            return sendError(req, res, 404, "Templates not found");
        }
        templates[0].remove(function(err) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "Something went wrong in deleting template!Please try again");
            }
            return res.send(templates[0]);
        });
    });
};

exports.getAllTemplate = function(req, res) {
    Templates.find({}, function(err, allTemplates) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        return res.send(allTemplates);
    });
};





























// var query_str = 'SELECT t1.*,t2.grade,CONCAT(t3.first_name,t3.last_name) as staff_name,t6.subject_name FROM attendance as t1 LEFT JOIN grades as t2 ON t1.grade_id = t2.id LEFT JOIN subjects as t6 ON t1.subject_id = t6.id LEFT JOIN staff as t3 ON t1.staff_id = t3.id WHERE t3.id = ?';
// db.driver.execQuery(query_str, [req.body.staff_id], function(err, rows) {
//     if (err) {
//         console.log(err);
//         return sendError(req, res, 400, "Bad Request check your parameters");
//     }
//     return res.send(rows);
// });
