var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var followers = db.define('followers', {
    follower_id: String,
    following_id:String
}, {
    timestamp: true,
    methods: {
    }
});
