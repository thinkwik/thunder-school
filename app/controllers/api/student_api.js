var bCrypt = require('bcrypt-nodejs');
var db = require('orm').db,
    Student = db.models.admissions;

var createHash = function(password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
};
var isValidPassword = function(staff, password) {
    return bCrypt.compareSync(password, staff.password);
};

var encrypt = require('../../../config/encrypt_decrypt').encrypt;
var decrypt = require('../../../config/encrypt_decrypt').decrypt;
var serialize = require('node-serialize');

exports.tryLogin = function(req, res) {
    if (typeof req.body.email == 'undefined' || req.body.password == 'undefined') {
        return apiSendError(req, res, 422, "Please check your parameters");
    }
    Student.find({
        email: req.body.email
    }, 1, function(err, students) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 422, "Bad Request check your parameters");
        }
          if (students.length > 0) {
            if (req.body.password) {
                var hashFromDB = students[0].password;
                var plainPassFromUser = req.body.password;
                if (isValidPassword(students[0], plainPassFromUser)) {
                    //set access token
                    var data = {
                        id: students[0].id,
                        type: "student",
                        email: students[0].email
                    };
                    var query_str = `SELECT * from followers WHERE follower_id = ?`;
                    db.driver.execQuery(query_str, [students[0].unique_id], function(err, rows) {
                        if (err) {
                            console.log(err);
                            return sendError(req, res, 400, "Bad Request check your parameters");
                        }
                        if (!rows) {
                            students[0].Following = 0;
                        }
                        students[0].Following = rows.length;
                        var query_str = `SELECT * from followers WHERE following_id = ?`;
                        db.driver.execQuery(query_str, [students[0].unique_id], function(err, rowsss) {
                            if (err) {
                                console.log(err);
                                return sendError(req, res, 400, "Bad Request check your parameters");
                            }
                            if (!rowsss) {
                                students[0].Followers = 0;
                            }
                            students[0].Followers = rowsss.length;
                            var response = {};
                            response['data'] = students[0];
                            response['access_token'] = encrypt(serialize.serialize(data));
                            response['code'] = 200;
                            return res.send(response);
                        })
                    })
                }else{
                    return apiSendError(req, res, 404, "Please enter correct Password");
                }
            } else {
                return apiSendError(req, res, 404, "Please enter Password");
            }
        } else {
            return apiSendError(req, res, 404, "Parent not found");
        }
    });
};

exports.me = function(req, res) {
    Student.find({
        id: req.api_token_data.id
    }, 1, function(err, student) {
        if (err) {
            return apiSendError(req, res, 500, "Something went wrong!");
        }
        if (!student.length) {
            return apiSendError(req, res, 404, "Student not found!");
        }
        var response = {};
        response['data'] = student[0];
        response['code'] = 200;
        return res.send(response);
    });
};

exports.profile = function(req, res) {
    if (typeof req.body.student_id == 'undefined') {
        return apiSendError(req, res, 422, "Other Student id required");
    }
    Student.find({
        id: req.body.student_id
    }, 1, function(err, student) {
        if (err) {
            return apiSendError(req, res, 500, "Something went wrong!");
        }
        if (!student.length) {
            return apiSendError(req, res, 404, "Student not found!");
        }
        var response = {};
        response['data'] = student[0];
        response['code'] = 200;
        return res.send(response);
    });
};

exports.apiMyschedule = function(req, res) {
    if (typeof req.body.student_id == 'undefined' || typeof req.body.grade_id == 'undefined') {
        return apiSendError(req, res, 422, "refer parameters");
    }
    var query_str = `SELECT t1.*, t3.grade, t4.subject_name, CONCAT(t5.first_name, " ", t5.last_name) as staff_name  , CONCAT(t6.room_number,"-",t6.floor," ",t6.building_name," ",t6.street_name) as location
                    FROM classes as t1
                    LEFT JOIN student_classes as t2 ON t1.id = t2.class_id
                    LEFT JOIN grades as t3 ON t3.id = t1.grade_id
                    LEFT JOIN subjects as t4 ON t4.id = t1.subject_id
                    LEFT JOIN staff as t5 ON t5.id = t1.staff_id
                    LEFT JOIN locations as t6 on t6.id = t1.location_id
                    WHERE t2.student_id = ? AND t2.grade_id = ? GROUP BY t1.id ORDER BY t1.id DESC`;
    db.driver.execQuery(query_str, [req.body.student_id, req.body.grade_id], function(err, rows) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 400, "Bad Request check your parameters");
        }
        var response = {};
        response['data'] = rows;
        response['code'] = 200;
        return res.send(response);
    });
};

exports.changePassword = function(req, res) {
    if (req.body.id !== 'undefined' && req.body.id) {
        Student.find({
            id: req.body.id
        }, 1, function(err, students) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 422, "Bad Request check your parameters");
            }
            if (!students.length) {
                return apiSendError(req, res, 404, "Student not found");
            } else {
                student = students[0];
                if (!isValidPassword(student, req.body.oldPassword)) {
                    return apiSendError(req, res, 422, "You have entered wrong password!");
                } else if (req.body.newPassword != req.body.confirmNewPassword) {
                    return apiSendError(req, res, 422, "new password and confirm new password must be same");
                }
                student.password = createHash(req.body.newPassword);
                student.save(function(err, saved_student) {
                    if (err) {
                        console.log(err);
                        return apiSendError(req, res, 500, "Something went wrong!Please try again");
                    }
                    var response = {};
                    response['data'] = [];
                    response['code'] = 200;
                    return res.send(response);
                });
            }
        });
    } else {
        return apiSendError(req, res, 422, "Bad Request check your parameters");
    }
};

exports.getStudentProfile = function(req, res) {
    if (typeof req.query.id == 'undefined') return sendError(req, res, 401, "ID required field");
    Student.find({
        id: req.query.id
    }, 1, function(err, students) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 422, "Bad Request check your parameters");
        }
        if (!students.length) {
            return apiSendError(req, res, 404, "Students not found");
        }
        var query_str = `SELECT * from followers WHERE follower_id = ?`;
        db.driver.execQuery(query_str, [students[0].unique_id], function(err, rows) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            if (!rows) {
                students[0].Following = 0;
            }
            students[0].Following = rows.length;
            var query_str = `SELECT * from followers WHERE following_id = ?`;
            db.driver.execQuery(query_str, [students[0].unique_id], function(err, rowsss) {
                if (err) {
                    console.log(err);
                    return sendError(req, res, 400, "Bad Request check your parameters");
                }
                if (!rowsss) {
                    students[0].Followers = 0;
                }
                students[0].Followers = rowsss.length;
                var response = {};
                response['data'] = students[0];
                response['code'] = 200;
                return res.send(response);
            })
        })
    });
};

exports.edit = function(req, res) {
    var editedPhoto;
    if (typeof req.body.id == 'undefined') {
        return apiSendError(req, res, 404, 'Admission id required!!');
    }
    Student.find({
        id: req.body.id
    }, 1, function(err, students) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 422, "Bad Request check your parameters");
        }
        if (!students.length) {
            return apiSendError(req, res, 404, "Student not found");
        }
        if (req.files) {
          if (!req.files.image) {
            return sendError(req, res, 400, "Bad Request check your parameters");
          }else {
            var fs = require('fs');
            var studentImages = __dirname + "/../../../public/upload/studentImages/";
            if (!fs.existsSync(studentImages)) {
                fs.mkdirSync(studentImages);
            }
            var file = req.files.image;
            var fileExtension = file.name.split(".")[1];
            if (fileExtension) {
                fileName = file.path.split('/');
                fs.rename(file.path, studentImages + fileName[fileName.length - 1] + "." + fileExtension);
                editedPhoto = fileName[fileName.length - 1] + "." + fileExtension;
                // return res.send(fileName[fileName.length - 1] +"."+ fileExtension);
              }
            }
        }
        if (req.body.first_name && req.body.last_name && req.body.gender && req.body.birth_date && req.body.address && req.body.city && req.body.mobile_number
            && req.body.zipcode && req.body.state) {
            students[0].first_name = req.body.first_name;
            students[0].last_name = req.body.last_name;
            students[0].gender = req.body.gender;
            students[0].birth_date = req.body.birth_date;
            students[0].address = req.body.address;
            students[0].city = req.body.city;
            students[0].zipcode = req.body.zipcode;
            students[0].state = req.body.state;
            students[0].image = (typeof editedPhoto == 'undefined' || editedPhoto === "") ? students[0].image : editedPhoto;
            students[0].mobile_number = req.body.mobile_number;
            students[0].profile_status = req.body.profile_status;
            students[0].save(function(err, updated_students) {
                if (err) {
                    console.log(err);
                    return apiSendError(req, res, 422, "Bad Request check your parameters");
                }
                var response = {};
                response['data'] = [];
                response['code'] = 200;
                return res.send(response);
            });
        } else {
            return apiSendError(req, res, 422, "Bad Request check your parameters");
        }
    });
};

exports.getAllStudentClasses = function(req, res) {
    if (req.body.student_id && req.body.grade_id) {
            var query_str = `SELECT t1.*, t3.grade, t4.subject_name, CONCAT(t5.first_name, " ", t5.last_name) as staff_name,t6.department_name
                        FROM classes as t1
                        LEFT JOIN student_classes as t2 ON t1.id = t2.class_id
                        LEFT JOIN grades as t3 ON t3.id = t1.grade_id
                        LEFT JOIN subjects as t4 ON t4.id = t1.subject_id
                        LEFT JOIN staff as t5 ON t5.id = t1.staff_id
                        LEFT JOIN departments as t6 ON t6.id = t4.department_id
                        WHERE t2.student_id = ? AND t2.grade_id = ? GROUP BY t1.id
                        ORDER BY CASE t1.day
                                   WHEN 'Monday' THEN 1
                                   WHEN 'Tuesday' THEN 2
                                   WHEN 'Wednesday' THEN 3
                                   WHEN 'Thursday' THEN 4
                                   WHEN 'Friday' THEN 5
                                   WHEN 'Saturday' THEN 6
                                   ELSE 7
                                 END ,t1.start_time ASC`;
        db.driver.execQuery(query_str, [req.body.student_id, req.body.grade_id], function(err, rows) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            if (!rows.length) {
                return sendError(req, res, 404, "Student classes not found");
            }
            var response = {};
            response['data'] = rows;
            response['code'] = 200;
            return res.send(response);
        });
    } else {
        return sendError(req, res, 422, "refer parameters");
    }
};
