app.controller('saveStudentsParentsController', ['$scope', '$http', '$state', 'toaster', '$cookieStore','Upload',
    function($scope, $http, $state, toaster, $cookieStore,Upload) {
        $scope.createStudentsParents = function() {
            data = $scope.parents;
            data.student_id = $state.params.student_id;
            data.parentImage = $scope.parentImage;
            $http({
                method: 'POST',
                url: '/parents/register',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Parents', 'Saved Successfully.');
                $state.go('schoolstaff.admission_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'Parents', error.data.message);
            });
        };

        $scope.createStudentsParentsTab = function() {
            data = $scope.parents;
            data.student_id = $state.params.student_id;
            data.parentImage = $scope.parentImage;
            $http({
                method: "POST",
                url: "/parents/edit",
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Parents Profile', 'Update Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Parents Profile', error.data.message);
            });
        };

        $scope.getStudentRelations = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/relation/getAllRelations',
                data: data
            }).then(function successCallback(response) {
                $scope.relations = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Relations', 'Something went wrong!');
            });
        };

        $scope.getStudentsParentsList = function() {
            var data = {
                student_id: $state.params.student_id
            };
            $http({
                method: 'POST',
                url: '/parents/getStudentsParents',
                data: data
            }).then(function successCallback(response) {
                $scope.parents = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Session', 'Something went wrong!');
            });
        };

        $scope.deleteParent = function(student_id) {
            var data = {
                id: student_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/parents/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getStudentsParentsList();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Parents', error.data.message);
                });
            }
        };

        $scope.editParent = function(parent_id) {
            $state.go('schools.student_parents_edit', {
                parent_id: parent_id
            });
        };

        $scope.addNewParents = function() {
            var student_id = $state.params.student_id;
            $state.go('schools.student_parents_create', {
                student_id: student_id
            });
        };

        $scope.cancelParentsForm = function() {
            window.history.back();
        };

        $scope.uploadFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected!');
            Loading(true);
            Upload.upload({
                url: '/parents/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.parentImage = success.data;
                },
                function(error) {
                    toaster.pop('error', "Error", error.data.message);
                    Loading(false);
                });
        };    }
]);

app.controller('saveSchoolStudentsParentsController', ['$scope', '$http', '$state', 'toaster', '$cookieStore','Upload',
    function($scope, $http, $state, toaster, $cookieStore,Upload) {
        $scope.createStudentsParents = function() {
            data = $scope.parents;
            data.student_id = $state.params.student_id;
            data.parentImage = $scope.parentImage;
            $http({
                method: 'POST',
                url: '/parents/register',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Parents', 'Saved Successfully.');
                $state.go('schools.admission_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'Parents', error.data.message);
            });
        };

        $scope.createStudentsParentsTab = function() {
            data = $scope.parents;
            data.student_id = $state.params.student_id;
            data.parentImage = $scope.parentImage;
            $http({
                method: "POST",
                url: "/parents/edit",
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Parents Profile', 'Update Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Parents Profile', error.data.message);
            });
        };

        $scope.getStudentRelations = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/relation/getAllRelations',
                data: data
            }).then(function successCallback(response) {
                $scope.relations = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Relations', 'Something went wrong!');
            });
        };

        $scope.getStudentsParentsList = function() {
            var data = {
                student_id: $state.params.student_id
            };
            $http({
                method: 'POST',
                url: '/parents/getStudentsParents',
                data: data
            }).then(function successCallback(response) {
                $scope.parents = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Session', 'Something went wrong!');
            });
        };

        $scope.deleteParent = function(student_id) {
            var data = {
                id: student_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/parents/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getStudentsParentsList();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Parents', error.data.message);
                });
            }
        };

        $scope.editParent = function(parent_id) {
            $state.go('schools.student_parents_edit', {
                parent_id: parent_id
            });
        };

        $scope.addNewParents = function() {
            var student_id = $state.params.student_id;
            $state.go('schools.student_parents_create', {
                student_id: student_id
            });
        };

        $scope.cancelParentsForm = function() {
            window.history.back();
        };

        $scope.uploadFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected!');
            Loading(true);
            Upload.upload({
                url: '/parents/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.parentImage = success.data;
                },
                function(error) {
                    toaster.pop('error', "Error", error.data.message);
                    Loading(false);
                });
        };    }
]);

app.controller('editStudentsParentsController', ['$scope', '$http', '$state', 'toaster', '$cookieStore','Upload',
    function($scope, $http, $state, toaster, $cookieStore,Upload) {
        $scope.getStudentsParents = function(parent_id) {
            var data = {
                parent_id: $state.params.parent_id
            };
            $http({
                method: 'POST',
                url: '/parents/getStudentsParentsEdit',
                data: data
            }).then(function successCallback(response) {
                $scope.parents = response.data;
                $scope.getStudentRelations(response.data.relation);
            }, function errorCallback(error) {
                toaster.pop('error', 'Parents Profile', error.data.message);
            });
        };

        $scope.saveUpdatedStudentsParents = function() {
            data = $scope.parents;
            if ($scope.parentImage) {
              data.parentImage = $scope.parentImage;
            }
            $http({
                method: 'POST',
                url: '/parents/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('sucess', 'Parents', 'Updated successfully!');
                window.history.back();
            }, function errorCallback(error) {
                toaster.pop('error', 'Parents', 'cannot get all the parents! try reloading page.');
            });
        };

        $scope.getStudentRelations = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/relation/getAllRelations',
                data: data
            }).then(function successCallback(response) {
                $scope.relations = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Relations', 'Something went wrong!');
            });
        };

        $scope.cancelParentsForm = function() {
            window.history.back();
        };

        $scope.uploadFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected!');
            Loading(true);
            Upload.upload({
                url: '/parents/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.parentImage = success.data;
                },
                function(error) {
                    toaster.pop('error', "Error", error.data.message);
                    Loading(false);
                });
        };
    }
]);


app.controller('saveStudentsParentsStaffController', ['$scope', '$http', '$state', 'toaster', '$cookieStore','Upload',
    function($scope, $http, $state, toaster, $cookieStore,Upload) {
        $scope.createStudentsParents = function() {
            data = $scope.parents;
            data.student_id = $state.params.student_id;
            data.parentImage = $scope.parentImage;
            $http({
                method: 'POST',
                url: '/parents/register',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Parents', 'Saved Successfully.');
                $state.go('schoolstaff.admission_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'Parents', error.data.message);
            });
        };

        $scope.createStudentsParentsTab = function() {
            data = $scope.parents;
            data.student_id = $state.params.student_id;
            data.parentImage = $scope.parentImage;
            $http({
                method: "POST",
                url: "/parents/edit",
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Parents Profile', 'Update Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Parents Profile', error.data.message);
            });
        };

        $scope.getStudentRelations = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/relation/getAllRelations',
                data: data
            }).then(function successCallback(response) {
                $scope.relations = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Relations', 'Something went wrong!');
            });
        };

        $scope.getStudentsParentsList = function() {
            var data = {
                student_id: $state.params.student_id
            };
            $http({
                method: 'POST',
                url: '/parents/getStudentsParents',
                data: data
            }).then(function successCallback(response) {
                $scope.parents = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Session', 'Something went wrong!');
            });
        };

        $scope.deleteParent = function(student_id) {
            var data = {
                id: student_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/parents/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getStudentsParentsList();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Parents', error.data.message);
                });
            }
        };

        $scope.editParent = function(parent_id) {
            $state.go('schoolstaff.student_parents_edit', {
                parent_id: parent_id
            });
        };

        $scope.addNewParents = function() {
            var student_id = $state.params.student_id;
            $state.go('schoolstaff.student_parents_create', {
                student_id: student_id
            });
        };

        $scope.cancelParentsForm = function() {
            window.history.back();
        };

        $scope.uploadFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected!');
            Loading(true);
            Upload.upload({
                url: '/parents/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.parentImage = success.data;
                },
                function(error) {
                    toaster.pop('error', "Error", error.data.message);
                    Loading(false);
                });
        };    }
]);
