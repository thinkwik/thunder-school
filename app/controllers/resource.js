var bCrypt = require('bcrypt-nodejs');
var db = require('orm').db,
    EventResource = db.models.resources;

exports.register = function(req, res) {
    if (typeof req.body.event_id == 'undefined') {
        return sendError(req, res, 422, 'required parameter not fullfiled for register new resource');
    }
    var new_event_res = new EventResource();
    new_event_res.resource_name = req.body.resource_name;
    new_event_res.resource_value = req.body.resource_value;
    new_event_res.cost = req.body.cost;
    new_event_res.total_cost = req.body.total_cost;
    new_event_res.arrival_date = req.body.arrival_date;
    new_event_res.vendor_phone = req.body.vendor_phone;
    new_event_res.event_id = (typeof req.body.event_id != 'undefined') ? req.body.event_id : "1";
    new_event_res.status = (typeof req.body.status != 'undefined') ? req.body.status : "pending";
    new_event_res.save(function(err, saved_res) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        res.send(saved_res);
    });
};

exports.getEventResource = function(req, res) {
    if ((typeof req.query.id == 'undefined')) {
        return sendError(req, res, 422, "required parameter not fullfilled");
    }
    EventResource.find({
        id: req.query.id
    }, 1, function(err, resources) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (resources.length === 0) {
            return sendError(req, res, 404, "resource not found with perticular id");
        }
        return res.send(resources[0]);
    });
};
exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'resource id required');
    }
    EventResource.find({
        id: req.body.id
    }, 1, function(err, resources) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!resources.length) {
            return sendError(req, res, 404, "resource not found");
        }
        resources[0].event_id = (typeof req.body.event_id != 'undefined') ? req.body.event_id : resources[0].event_id;
        resources[0].resource_name = req.body.resource_name;
        resources[0].resource_value = req.body.resource_value;
        resources[0].cost = req.body.cost;
        resources[0].total_cost = req.body.total_cost;
        resources[0].arrival_date = req.body.arrival_date;
        resources[0].vendor_phone = req.body.vendor_phone;
        resources[0].status = (typeof req.body.status != 'undefined') ? req.body.status : resources[0].status;
        resources[0].save(function(err, updated_resource) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            return res.send(updated_resource);
        });
    });
};

exports.active = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'resource id required');
    }
    EventResource.find({
        id: req.body.id
    }, 1, function(err, resources) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        resources[0].status = "1";
        resources[0].save(function(err, activated_resource) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "something went wrong in activating resource");
            }
            return res.send(activated_resource);
        });
    });
};

exports.inactive = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'resource id required');
    }
    EventResource.find({
        id: req.body.id
    }, 1, function(err, resources) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        resources[0].status = "0";
        resources[0].save(function(err, activated_resource) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "something went wrong in inactivating resource");
            }
            return res.send(activated_resource);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'resource id required');
    }
    EventResource.find({
        id: req.body.id
    }, 1, function(err, resources) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!resources.length) {
            return sendError(req, res, 404, "Resource not found");
        }
        resources[0].remove(function(err) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "Something went wrong in deleting resource");
            }
            return res.send(resources[0]);
        });
    });
};

exports.getAllEventResources = function(req, res) {
    if (typeof req.body.event_id == 'undefined') {
        return sendError(req, res, 422, "event id required");
    }
    EventResource.find({
        event_id: req.body.event_id
    }, function(err, resources) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(resources);
    });
};
