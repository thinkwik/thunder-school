var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var ChatRoomParticipant = db.define('chat_room_participants', {
    chat_room_id: {
        type: 'integer'
    },
    unique_id:String,
    sent_url_link:String
}, {
    timestamp: true,
    methods: {

    }
});
