app.controller('manageMultipleClassesController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', '$window',
    function($scope, $http, $state, toaster, $cookieStore, $window) {
        $scope.getGrades = function() {
            var data = {
                school_id: $cookieStore.get('loggedInStaff').school_id
            };
            $http({
                method: 'POST',
                url: '/grade/getAllSchoolGrades',
                data: data
            }).then(function successCallback(response) {
                $scope.grades = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Student Grades', error.data.message);
            });
        };

        $scope.getStudents = function(grade_id) {
            $scope.student_classes.class_id = "";
            $scope.student_classes.student_id = "";
            var data = {
                school_id: $cookieStore.get('loggedInStaff').school_id,
                grade_id: grade_id
            };
            $http({
                method: 'POST',
                url: '/student/getAllStudentGradeWise',
                data: data
            }).then(function successCallback(response) {
                $scope.students = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Students List', error.data.message);
            });
        };

        $scope.getClasses = function(grade_id) {
            var data = {
                grade_id: grade_id
            };
            $http({
                method: 'POST',
                url: '/class/getClassGradeWise',
                data: data
            }).then(function successCallback(response) {
                $scope.classes = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Student Classes', error.data.message);
            });
        };


        $scope.ListStudentClasses = function() {
            var studentData = {
                school_id: $cookieStore.get('loggedInStaff').school_id,
                grade_id: $scope.student_classes.grade_id,
                student_id: $scope.student_classes.student_id
            };
            $http({
                method: 'POST',
                url: '/class/getAllStudentClasses',
                data: studentData
            }).then(function successCallback(response) {
                $scope.studentClassesList = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Student Class List', error.data.message);
            });
        };
        //
        //
        // $scope.getParticularStudentClass = function() {
        //     var studentData = {
        //         grade_id: $scope.student_classes.grade_id,
        //         student_id: $scope.student_classes.student_id
        //     };
        //     $http({
        //         method: 'POST',
        //         url: '/class/getAllStudentClasses',
        //         data: studentData
        //     }).then(function successCallback(response) {
        //         $scope.studentClassesList = response.data;
        //     }, function errorCallback(error) {
        //         toaster.pop('error', 'Student Classes', error.data.message);
        //     });
        // };
        //
        // $scope.getStudentClasses = function(grade_id, student_id) {
        //     var data = {
        //         school_id: $cookieStore.get('loggedInStaff').school_id,
        //         grade_id: grade_id,
        //         student_id: student_id
        //     };
        //     $http({
        //         method: 'POST',
        //         url: '/class/getAllClassesGradeStudentWise',
        //         data: data
        //     }).then(function successCallback(response) {
        //         $scope.classes = response.data;
        //         var studentData = {
        //             school_id: $cookieStore.get('loggedInStaff').school_id,
        //             grade_id: grade_id,
        //             student_id: student_id
        //         };
        //         $http({
        //             method: 'POST',
        //             url: '/class/getAllStudentClasses',
        //             data: studentData
        //         }).then(function successCallback(response) {
        //             $scope.studentClassesList = response.data;
        //         }, function errorCallback(error) {
        //             toaster.pop('error', 'Student Classes', error.data.message);
        //         });
        //     }, function errorCallback(error) {
        //         toaster.pop('error', 'Student Classes', error.data.message);
        //     });
        // };

        $scope.addStudentsToMultipleClass = function () {
          var data = $scope.student_classes;
          $http({
              method:"POST",
              url:"class/addMultipleStudentInOneClass",
              data:data
          }).then(function successCallback(response) {
              toaster.pop('success', 'Student Multiple Class', "Class removed successfully!");
          }, function errorCallback(error) {
              toaster.pop('error', 'Student  Classes', error.data.message);
          })
        };

        $scope.deleteStudentClass = function(studentClassId) {
            var data = {
                id: studentClassId
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/student_class/deleteStudentClass',
                    data: data
                }).then(function successCallback(response) {
                    toaster.pop('success', 'Student Class', "Class removed successfully!");
                    $scope.getParticularStudentClass();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Student Class', error.data.message);
                });
            }
        };

    }
]);
