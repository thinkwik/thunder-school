var bCrypt = require('bcrypt-nodejs');
var shortid = require('shortid');
var db = require('orm').db,
    User = db.models.users;
School = db.models.schools;

var createHash = function(password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
};


exports.fargot_password_all = function(req, res) {
    var email = [];
    email['staff'] = "email_id";
    email['admissions'] = "email";
    email['parents'] = "email";
    email['users'] = "email";
    if (req.body.email && req.body.type) {
        var query_str = `SELECT * FROM ` + req.body.type + ` WHERE ` + email[req.body.type] + ` = ? LIMIT 1 `;
        db.driver.execQuery(query_str, [req.body.email], function(err, rows) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "Something went wrong!");
            }
            if (rows.length <= 0) {
                return sendError(req, res, 404, "Please make sure your email is correct.");
            }
            var token = shortid.generate();
            var query_str_2 = `UPDATE ` + req.body.type + ` SET forgot_password_token = ? WHERE id = ?`;
            db.driver.execQuery(query_str_2, [token, rows[0].id], function(err, users) {
                if (err) {
                    console.log(err);
                    return sendError(req, res, 500, "Something went wrong!");
                }
                var to = req.body.email;
                var subject = "Reset Password Link";
                var link = "https://" + req.headers.host + "/reset_password?token=" + token + "&type=" + req.body.type + "&email=" + req.body.email;
                
                var ui_message = "";
                var templet_url_query = "SELECT ui_data FROM email_templates WHERE ui_key = ? LIMIT 1";
                db.driver.execQuery(templet_url_query, ['forgot_password'], function(video_err, ui_rows) {
                    if (video_err) {
                        console.log(video_err);
                        return sendError(req, res, 422, "Bad Request check your parameters");
                    }
                    if (ui_rows.length > 0) {
                        ui_message = ui_rows[0].ui_data;
                    }

                    var email_html = `
                    <body style="font-family:arial; font-size:16px; color:#4c4c4c; padding:0; margin:0;">
                        <table width="100%" style="font-family:arial; font-size:16px; border:0px solid #000; max-width:700px; padding-top:30px;"  align="center" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td>
                                        <table width="100%" style="font-family:arial; font-size:16px; border:1px solid #d6d6d8; max-width:700px;"  align="center" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td style="padding:50px 40px 40px 40px; text-align:center; font-family:arial;">
                                                        <h1 style="font-family:arial; font-size:40px; color:#30333b; font-weight:normal; padding:0; margin:0;">Hello, <span style="color:rgb(242,53,95); font-weight:bold; text-transform:uppercase;">User</span></h1>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding:0 40px 30px 40px; text-align:center; font-family:arial;">
                                                        <p style="font-family:arial; font-size:22px; color:#737373; font-weight:normal; padding:0; margin:0;">
                                                            `+ui_message+`
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding:0 40px 50px 40px; text-align:center;">
                                                        <a href="` + link + `" style="text-decoration:none; outline:none;">
                                                            <img style="display:inline-block;" src="https://socratessms.com:8080/img/join_link.png" alt="Join Link" />
                                                        </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding:35px 40px;">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td valign="bottom" style="text-align:right; font-family:arial; font-size:14px; color:#6a6c72;">Kind Regards <br /><b style="text-transform:uppercase;">Thunder System</b></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-family:arial; font-size:10px; padding:20px 0 30px 0; color:#a6a6a6; text-transform:uppercase; text-align:center;">
                                        &#169; 2016 Copyright Thunder System
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </body>
                    `;
                    sendMail(req, res, null, to, subject, email_html, function(err, success) {
                        if (err) {
                            console.log(err);
                            return sendError(req, res, 500, "Something went wrong!");
                        }
                        return res.send(200);
                    });



                });
            });

        });
    } else {
        return sendError(req, res, 488, "Bad Request check your parameters");
    }
};

exports.reset_password_all = function(req, res) {
    var email = [];
    email['staff'] = "email_id";
    email['admissions'] = "email";
    email['parents'] = "email";
    email['users'] = "email";
    if (req.body.email && req.body.type && req.body.token && req.body.new_password && req.body.confirm_new_password) {
        var query_str = `SELECT * FROM ` + req.body.type + ` WHERE ` + email[req.body.type] + ` = ? AND forgot_password_token = ? LIMIT 1 `;
        db.driver.execQuery(query_str, [req.body.email, req.body.token], function(err, rows) {
            if (req.body.new_password != req.body.confirm_new_password) {
                return sendError(req, res, 500, "both password must be same");
            }
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "Something went wrong!");
            }
            if (rows.length <= 0) {
                return sendError(req, res, 404, "Your link has been expired, please try again");
            }
            var password = createHash(req.body.new_password);
            var query_str_2 = `UPDATE ` + req.body.type + ` SET forgot_password_token = ? , password = ? WHERE id = ?`;
            db.driver.execQuery(query_str_2, [null, password, rows[0].id], function(err, users) {
                if (err) {
                    console.log(err);
                    return sendError(req, res, 500, "Something went wrong!");
                }
                return res.send(200);
            });

        });
    } else {
        return sendError(req, res, 400, "Bad Request check your parameters");
    }
};

exports.index = function(req, res) {
    return res.send("welcome page");
};


exports.changePassword = function(req, res) {
    User.find({
        id: req.user.id
    }, 1, function(err, users) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!users.length) {
            return sendError(req, res, 404, "User not found");
        } else {
            user = users[0];
            if (!isValidPassword(user, req.body.oldPassword)) {
                return sendError(req, res, 422, "You have entered wrong password!");
            } else if (req.body.newPassword != req.body.confirmNewPassword) {
                return sendError(req, res, 422, "new password and confirm new password must be same");
            }
            user.password = createHash(req.body.newPassword);
            user.save(function(err, saved_user) {
                if (err) {
                    return sendError(req, res, 500, "Something went wrong!Please try again");
                }
                return res.send(saved_user);
            });
        }
    });

};

exports.logout = function(req, res) {
    if (typeof req.user != 'undefined') {
        console.log(req.user.email + " logged out");
    }
    req.logout();
    return res.send({
        message: "user logged out successfully"
    });
};

exports.getUser = function(req, res) {
    if (req.query.id !== '' && req.query.id !== undefined) {
        User.find({
            id: req.query.id
        }, 1, function(err, users) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            if (users.length) {
                user = users[0];
                user.password = null;
                return res.send(user);
            } else {
                return sendError(req, res, 404, "User not found");
            }
        });
    } else {
        return sendError(req, res, 422, "require user's id in request parameter");
    }
};

exports.edit = function(req, res) {
    console.log(req.body);
    if (req.body.id !== '' || req.body.id !== undefined) {
        User.find({
            id: req.body.id
        }, 1, function(err, users) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            if (users.length) {
                if (req.body.password !== undefined && req.body.password !== '' && req.body.password !== null) {
                    users[0].password = createHash(req.body.password);
                }
                users[0].first_name = req.body.first_name;
                users[0].last_name = req.body.last_name;
                users[0].email = req.body.email;
                users[0].mobile = req.body.mobile;
                users[0].profile_image = (typeof req.body.profile_image == 'undefined' || req.body.profile_image === "") ? users[0].profile_image : req.body.profile_image;
                users[0].school_id = (typeof req.body.school_id != 'undefined') ? req.body.school_id : users[0].school_id;
                users[0].role_id = (typeof req.body.role_id != 'undefined') ? req.body.role_id : users[0].role_id;
                users[0].user_type = (typeof req.body.user_type != 'undefined') ? req.body.user_type : users[0].user_type;
                users[0].status = req.body.status;
                users[0].gender = (typeof req.body.gender != 'undefined') ? req.body.gender : users[0].gender;
                users[0].birth_date = (typeof req.body.birth_date != 'undefined') ? req.body.birth_date : users[0].birth_date;
                users[0].address = (typeof req.body.address != 'undefined') ? req.body.address : users[0].address;
                users[0].city = (typeof req.body.city != 'undefined') ? req.body.city : users[0].city;
                users[0].zipcode = (typeof req.body.zipcode != 'undefined') ? req.body.zipcode : users[0].zipcode;
                users[0].state = (typeof req.body.state != 'undefined') ? req.body.state : users[0].state;
                users[0].country = (typeof req.body.country != 'undefined') ? req.body.country : users[0].country;
                users[0].height = (typeof req.body.height != 'undefined') ? req.body.height : users[0].height;
                users[0].weight = (typeof req.body.weight != 'undefined') ? req.body.weight : users[0].weight;
                users[0].blood_group = (typeof req.body.blood_group != 'undefined') ? req.body.blood_group : users[0].blood_group;
                users[0].dieases_information = (typeof req.body.dieases_information != 'undefined') ? req.body.dieases_information : users[0].dieases_information;
                users[0].emergency_contacts = (typeof req.body.emergency_contacts != 'undefined') ? req.body.emergency_contacts : users[0].emergency_contacts;
                
                users[0].notes = (typeof req.body.notes != 'undefined') ? req.body.notes : users[0].notes;
                users[0].medicalinfo = (typeof req.body.medicalinfo != 'undefined') ? req.body.medicalinfo : users[0].medicalinfo;
                users[0].social_behavioral = (typeof req.body.social_behavioral != 'undefined') ? req.body.social_behavioral : users[0].social_behavioral;
                users[0].physical_handicaps = (typeof req.body.physical_handicaps != 'undefined') ? req.body.physical_handicaps : users[0].physical_handicaps;
                users[0].medical_immunizations = (typeof req.body.medical_immunizations != 'undefined' && Array.isArray(req.body.medical_immunizations) ) ? req.body.medical_immunizations.join(',') : users[0].medical_immunizations;
                users[0].medical_allergies = (typeof req.body.medical_allergies != 'undefined' && Array.isArray(req.body.medical_allergies) ) ? req.body.medical_allergies.join(',') : users[0].medical_allergies;        
        
        
                users[0].save(function(err, success) {
                    if (err) {
                        return sendError(req, res, 500, "something went wrong while updating user");
                    }
                    return res.send(users[0]);
                });
            } else {
                return sendError(req, res, 404, "User not found");
            }

        });
    } else {
        return sendError(req, res, 422, "require user's id in request parameter");
    }
};

exports.active = function(req, res) {
    if (req.body.id !== '' || req.body.id !== undefined) {
        User.find({
            id: req.body.id
        }, 1, function(err, users) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            if (users.length) {
                users[0].status = "1";
                users[0].save(function(err, success) {
                    if (err) {
                        return sendError(req, res, 500, "something went wrong while activating user");
                    }
                    return res.send(users[0]);
                });
            } else {
                return sendError(req, res, 404, "User not found");
            }

        });
    } else {
        return sendError(req, res, 422, "require user's id in request parameter");
    }
};

exports.inactive = function(req, res) {
    if (req.body.id !== '' || req.body.id !== undefined) {
        User.find({
            id: req.body.id
        }, 1, function(err, users) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            if (users.length) {
                users[0].status = "0";
                users[0].save(function(err, success) {
                    if (err) {
                        console.log(err);
                        return sendError(req, res, 500, "something went wrong while inactivating user");
                    }
                    return res.send(users[0]);
                });
            } else {
                return sendError(req, res, 404, "User not found");
            }
        });
    } else {
        return sendError(req, res, 422, "require user's id in request parameter");
    }
};

exports.delete = function(req, res) {
    if (req.body.id !== '' || req.body.id !== undefined) {
        User.find({
            id: req.body.id
        }, 1, function(err, users) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            if (users.length) {
                users[0].remove(function(err, success) {
                    if (err) {
                        console.log(err);
                        return sendError(req, res, 500, "something went wrong while deleting user");
                    }
                    return res.send(users[0]);
                });
            } else {
                return sendError(req, res, 404, "User not found");
            }
        });
    } else {
        return sendError(req, res, 422, "require user's id in request parameter");
    }
};

exports.getAllSuperAdmins = function(req, res) {
    var findUser = {
        user_type: "admin"
    };
    var offset = 0;
    if (typeof req.body != 'undefined' && typeof req.body.offset != 'undefined') {
        offset = req.body.offset;
    }
    User.find(findUser, {
        offset: offset,
        limit: 10
    }, function(err, superAdmins) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!superAdmins.length) {
            return res.send([]);
        }
        return res.send(superAdmins);
    });
};

exports.getAllSchoolAdminsByDistrict = function(req, res) {
    if (typeof req.body.district_id == 'undefined' || req.body.district_id === null) {
        return sendError(req, res, 422, "District ID required");
    }
    var query_str = "SELECT t1.*,t2.school_name, t2.authorized_user_id FROM `users` as t1 LEFT JOIN schools as t2 ON t1.school_id = t2.id WHERE t2.school_type = ? AND t2.parent_id = ?";
    db.driver.execQuery(query_str, ['school', req.body.district_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
exports.getAllSchoolAdminsBySchool = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id === null) {
        return sendError(req, res, 422, "School ID required");
    }
    var query_str = "SELECT t1.*,t2.school_name, t2.authorized_user_id FROM `users` as t1 LEFT JOIN schools as t2 ON t1.school_id = t2.id WHERE t2.school_type = ? AND t2.id = ?";
    db.driver.execQuery(query_str, ['school', req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.schoolAdminRegister = function(req, res) {
    var newSchoolAdmin = new User();
    newSchoolAdmin.first_name = req.body.first_name;
    newSchoolAdmin.last_name = req.body.last_name;
    newSchoolAdmin.email = req.body.email;
    newSchoolAdmin.password = createHash(req.body.password);
    newSchoolAdmin.role_id = (typeof req.body.role_id != 'undefined') ? req.body.role_id : "0";
    newSchoolAdmin.profile_image = (typeof req.body.profile_image != 'undefined') ? req.body.profile_image : null;    
    newSchoolAdmin.user_type = 'school_admin';
    newSchoolAdmin.school_id = req.body.school_id;
    newSchoolAdmin.mobile = req.body.mobile;
    newSchoolAdmin.status = (typeof req.body.status != 'undefined') ? req.body.status : "0";
    newSchoolAdmin.gender = (typeof req.body.gender != 'undefined') ? req.body.gender : null;
    newSchoolAdmin.birth_date = (typeof req.body.birth_date != 'undefined') ? req.body.birth_date : null;
    newSchoolAdmin.address = (typeof req.body.address != 'undefined') ? req.body.address : null;
    newSchoolAdmin.city = (typeof req.body.city != 'undefined') ? req.body.city : null;
    newSchoolAdmin.zipcode = (typeof req.body.zipcode != 'undefined') ? req.body.zipcode : null;
    newSchoolAdmin.state = (typeof req.body.state != 'undefined') ? req.body.state : null;
    newSchoolAdmin.country = (typeof req.body.country != 'undefined') ? req.body.country : null;
    newSchoolAdmin.height = (typeof req.body.height != 'undefined') ? req.body.height : null;
    newSchoolAdmin.weight = (typeof req.body.weight != 'undefined') ? req.body.weight : null;
    newSchoolAdmin.blood_group = (typeof req.body.blood_group != 'undefined') ? req.body.blood_group : null;
    newSchoolAdmin.dieases_information = (typeof req.body.dieases_information != 'undefined') ? req.body.dieases_information : null;

    newSchoolAdmin.notes = (typeof req.body.notes != 'undefined') ? req.body.notes : null;
    newSchoolAdmin.medicalinfo = (typeof req.body.medicalinfo != 'undefined') ? req.body.medicalinfo : null;
    newSchoolAdmin.social_behavioral = (typeof req.body.social_behavioral != 'undefined') ? req.body.social_behavioral : null;
    newSchoolAdmin.physical_handicaps = (typeof req.body.physical_handicaps != 'undefined') ? req.body.physical_handicaps : null;
    newSchoolAdmin.medical_immunizations = (typeof req.body.medical_immunizations != 'undefined' && Array.isArray(req.body.medical_immunizations) ) ? req.body.medical_immunizations.join(',') : null;
    newSchoolAdmin.medical_allergies = (typeof req.body.medical_allergies != 'undefined' && Array.isArray(req.body.medical_allergies) ) ? req.body.medical_allergies.join(',') : null;
   


    newSchoolAdmin.save(function(err, inserted_admin) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "please check your parameter");
        }
        return res.send(inserted_admin);
    });
};

var isValidPassword = function(user, password) {
    return bCrypt.compareSync(password, user.password);
};

exports.createSuperAdmin = function(req, res) {
    var newSuperAdmin = new User();
    newSuperAdmin.first_name = req.body.first_name;
    newSuperAdmin.last_name = req.body.last_name;
    newSuperAdmin.email = req.body.email;
    newSuperAdmin.password = createHash(req.body.password);
    newSuperAdmin.role_id = (typeof req.body.role_id != 'undefined') ? req.body.role_id : "0";
    newSuperAdmin.user_type = 'admin';
    newSuperAdmin.mobile = req.body.mobile;
    newSuperAdmin.profile_image = req.body.profile_image;
    newSuperAdmin.school_id = req.body.school_id;
    newSuperAdmin.status = (typeof req.body.status != 'undefined') ? req.body.status : "1";
    newSuperAdmin.notes = (typeof req.body.notes != 'undefined') ? req.body.notes : null;
    newSuperAdmin.emergency_contacts = (typeof req.body.emergency_contacts != 'undefined') ? req.body.emergency_contacts : null;
    newSuperAdmin.country = (typeof req.body.country != 'undefined') ? req.body.country : null;
    newSuperAdmin.height = (typeof req.body.height != 'undefined') ? req.body.height : null;
    newSuperAdmin.weight = (typeof req.body.weight != 'undefined') ? req.body.weight : null;
    newSuperAdmin.blood_group = (typeof req.body.blood_group != 'undefined') ? req.body.blood_group : null;
    newSuperAdmin.emergency_contacts = (typeof req.body.emergency_contacts != 'undefined') ? req.body.emergency_contacts : null;

    newSuperAdmin.save(function(err, superAdminCreated) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "please check your parameter");
        }
        return res.send(superAdminCreated);
    });
};

// exports.getSchoolByAuthorizedPerson = function(req, res) {
//     if (typeof req.body.user_id != 'undefined') {
//         School.find({
//             authorized_user_id: req.body.user_id
//         }, 1, function(err, schools) {
//             if (err) {
//                 console.log(err);
//                 return sendError(req, res, 500, "Something went wrong!");
//             }
//             if (schools.length <= 0) {
//                 return sendError(req, res, 404, "Connot find school linked to user!");
//             }
//             return res.send(schools[0]);
//         });
//     } else {
//         return sendError(req, res, 500, "Something went wrong!");
//     }
// };

exports.getSchoolByAuthorizedPerson = function(req, res) {
    console.log(req.body);
    if (typeof req.body.school_id == 'undefined' && typeof req.body.user_id == 'undefined') {
        return sendError(req, res, 422, "Bad Request check your parameters");
    }
    // var query_str = `SELECT t1.*, t2.id as user_id, CONCAT(t2.first_name, ' ', t2.last_name) as ap_name, t2.email as email,t2.profile_image, t2.mobile as ap_mobile , t3.modules as security_roles, '' as school_admin_flag
    //                 FROM schools as t1 
    //                 LEFT JOIN users as t2 ON t1.authorized_user_id = t2.id 
    //                 LEFT JOIN school_roles as t3 ON t2.role_id = t3.id
    //                 WHERE t1.id = ? AND t2.id = ? `;

    var query_str = `SELECT t1.*, t2.id as user_id, CONCAT(t2.first_name, ' ', t2.last_name) as ap_name, t2.email as email,t2.profile_image, t2.mobile as ap_mobile , t3.modules as security_roles, '' as school_admin_flag
                    FROM users as t2 
                    LEFT JOIN schools as t1 ON t2.school_id = t1.id
                    LEFT JOIN school_roles as t3 ON t2.role_id = t3.id
                    WHERE t1.id = ? AND t2.id = ? `;
    db.driver.execQuery(query_str, [req.body.school_id, req.body.user_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 500, "Something went wrong! please try again.");
        }
        if (!rows.length) {
            return sendError(req, res, 404, "User not found");
        }
        //console.log(rows[0]);
        return res.send(rows[0]);
    });
};



var fs = require('fs');
exports.uploadFile = function(req, res) {
    var fs = require('fs');
    var users = __dirname + "/../../public/upload/users/";
    if (!fs.existsSync(users)) {
        fs.mkdirSync(users);
    }
    var file = req.files.file;
    var fileExtension = file.name.split(".")[1];
    if (fileExtension) {
        fileName = file.path.split('/');
        fs.rename(file.path, users + fileName[fileName.length - 1] + "." + fileExtension);
        return res.send(fileName[fileName.length - 1] + "." + fileExtension);
    } else {
        return sendError(req, res, 400, "Bad Request check your parameters");
    }

};
