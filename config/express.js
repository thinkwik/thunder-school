var express = require('express');

module.exports = function(app, config)
{
  app.configure(function ()
  {
    app.use(express.compress());
    app.use(express.static(config.root + '/public'));
    app.use('/node_modules', express.static(config.root + '/node_modules'));
    app.set('port', config.port);
    app.set('views', config.root + '/public/');
    app.engine('html', require('ejs').renderFile);
    app.set('view engine', 'html');
    // app.use(express.favicon(config.root + '/app/public/img/favicon.ico'));
    app.use(express.logger('dev'));
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(app.router);
    // app.use(function(req, res)
    // {
    //   res.status(404).render('404', { title: '404' });
    // });
  });
};
