var bCrypt = require('bcrypt-nodejs');
var db = require('orm').db,
    Staff = db.models.staff;

var createHash = function(password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
};
var isValidPassword = function(staff, password) {
    return bCrypt.compareSync(password, staff.password);
};

var encrypt = require('../../../config/encrypt_decrypt').encrypt;
var decrypt = require('../../../config/encrypt_decrypt').decrypt;
var serialize = require('node-serialize');

exports.tryLogin = function(req, res) {
    if (typeof req.body.email == 'undefined' || req.body.password == 'undefined') {
        return apiSendError(req, res, 422, "Please check your parameters");
    }
    Staff.find({
        email_id: req.body.email
    }, 1, function(err, result) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 422, "Bad Request check your parameters");
        } else if (result.length > 0) {
            if (req.body.password) {
                var hashFromDB = result[0].password;
                var plainPassFromUser = req.body.password;
                if (isValidPassword(result[0], plainPassFromUser)) {
                    var query_str = `SELECT * from followers WHERE follower_id = ?`;
                    db.driver.execQuery(query_str, [result[0].unique_id], function(err, rows) {
                        if (err) {
                            console.log(err);
                            return sendError(req, res, 400, "Bad Request check your parameters");
                        }
                        if (!rows) {
                            result[0].Following = 0;
                        }
                        result[0].Following = rows.length;
                        var query_str = `SELECT * from followers WHERE following_id = ?`;
                        db.driver.execQuery(query_str, [result[0].unique_id], function(err, rowsss) {
                            if (err) {
                                console.log(err);
                                return sendError(req, res, 400, "Bad Request check your parameters");
                            }
                            if (!rowsss) {
                                result[0].Followers = 0;
                            }
                            result[0].Followers = rowsss.length;
                            var data = {
                                id: result[0].id,
                                type: "staff",
                                email: result[0].email_id
                            };
                            var response = {};
                            response['data'] = result[0];
                            response['access_token'] = encrypt(serialize.serialize(data));
                            response['code'] = 200;
                            return res.send(response);
                        });
                    });
                }
            } else {
                return apiSendError(req, res, 404, "Please enter Password");
            }
        } else {
            return apiSendError(req, res, 404, "Staff not found");
        }
    });
};

exports.me = function(req, res) {
    Staff.find({
        id: req.api_token_data.id
    }, 1, function(err, staff) {
        if (err) {
            return apiSendError(req, res, 500, "Something went wrong!");
        }
        if (!staff.length) {
            return apiSendError(req, res, 404, "Staff not found!");
        }
        var response = {};
        response['data'] = staff[0];
        response['code'] = 200;
        return res.send(response);
    });
};

exports.profile = function(req, res) {
    if (typeof req.body.staff_id != 'undefined' && req.body.staff_id) {
        Staff.find({
            id: req.body.staff_id
        }, 1, function(err, staff) {
            if (err) {
                return apiSendError(req, res, 400, "Bad Request check your parameters.");
            }
            if (!staff.length) {
                return apiSendError(req, res, 404, "Staff not found!");
            }
            var response = {};
            response['data'] = staff[0];
            response['code'] = 200;
            return res.send(response);
        });
    } else {
        return apiSendError(req, res, 422, "Other staff id required");
    }
};

exports.apiGetAllSchoolGrades = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id == "null") {
        return apiSendError(req, res, 422, "Please check your parameters");
    }
    var query_str = "SELECT t1.*,t2.school_name FROM `grades` as t1 LEFT JOIN schools as t2 on t1.school_id = t2.id WHERE  t2.id = ? ORDER BY t1.school_id ASC, t1.position ASC";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 422, "Bad Request check your parameters");
        }
        var response = {};
        response['data'] = rows;
        response['code'] = 200;
        return res.send(response);
    });
};

exports.apiGetAllGradeSubjects = function(req, res) {
    if (req.body.grade_id) {
        var query_str = `SELECT t1.*,t2.school_name,t3.grade, t4.department_name
      FROM subjects as t1
      LEFT JOIN schools as t2 ON t1.school_id = t2.id
      LEFT JOIN grades as t3 ON t1.grade_id = t3.id
      LEFT JOIN departments as t4 ON t1.department_id = t4.id
      WHERE t1.grade_id= ? GROUP BY t1.id`;
        db.driver.execQuery(query_str, [req.body.grade_id], function(err, rows) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 400, "Bad Request check your parameters");
            }
            if (!rows.length) {
                return apiSendError(req, res, 404, "Subject not found");
            }
            var response = {};
            response['data'] = rows;
            response['code'] = 200;
            return res.send(response);
        });
    } else {
        return apiSendError(req, res, 422, "Please check your paramenters");
    }
};

exports.apiGetAllSubjectClasses = function(req, res) {
    if (typeof req.body.subject_id == 'undefined') {
        return apiSendError(req, res, 422, "Please check your paramenters");
    }
    var query_str = "SELECT * FROM `classes` WHERE `subject_id` = ?";
    db.driver.execQuery(query_str, [req.body.subject_id], function(err, rows) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 400, "Bad Request check your parameters");
        }
        var response = {};
        response['data'] = rows;
        response['code'] = 200;
        return res.send(response);
    });
};

exports.apiMyschedule = function(req, res) {
    if (typeof req.body.staff_id == 'undefined') {
        return apiSendError(req, res, 422, "refer parameters");
    }
    var query_str = `SELECT t1.*, t2.grade, t3.subject_name, CONCAT(t4.room_number,"-",t4.floor," ",t4.building_name," ",t4.street_name) as location
                    FROM classes as t1
                    LEFT JOIN grades as t2 on t1.grade_id = t2.id
                    LEFT JOIN subjects as t3 ON t1.subject_id = t3.id
                    LEFT JOIN locations as t4 on t4.id = t1.location_id
                    WHERE t1.staff_id = ?`;
    db.driver.execQuery(query_str, [req.body.staff_id], function(err, rows) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 400, "Bad Request check your parameters");
        }
        var response = {};
        response['data'] = rows;
        response['code'] = 200;
        return res.send(response);
    });
};

exports.apiGetSchoolAllStaff = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id === null) {
        return apiSendError(req, res, 422, "refer parameters");
    }
    var query_str = "SELECT t1.id, CONCAT(t1.first_name, ' ', t1.last_name) as staff_name, t1.email_id, t2.designation,t3.school_name FROM `staff` as t1 LEFT JOIN designations as t2 on t1.designation_id = t2.id LEFT JOIN schools as t3 on t1.school_id = t3.id WHERE  t3.id = ?";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 422, "Bad Request check your parameters");
        }
        var response = {};
        response['data'] = rows;
        response['code'] = 200;
        return res.send(response);
    });
};

exports.changePassword = function(req, res) {
    if (req.body.id !== 'undefined' && req.body.id) {
        Staff.find({
            id: req.body.id
        }, 1, function(err, staffs) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 422, "Bad Request check your parameters");
            }
            if (!staffs.length) {
                return apiSendError(req, res, 404, "Staff not found");
            } else {
                staff = staffs[0];
                if (!isValidPassword(staff, req.body.oldPassword)) {
                    return apiSendError(req, res, 422, "You have entered wrong password!");
                } else if (req.body.newPassword != req.body.confirmNewPassword) {
                    return apiSendError(req, res, 422, "new password and confirm new password must be same");
                }
                staff.password = createHash(req.body.newPassword);
                staff.save(function(err, saved_staff) {
                    if (err) {
                        console.log(err);
                        return apiSendError(req, res, 500, "Something went wrong!Please try again");
                    }
                    var response = {};
                    response['data'] = [];
                    response['code'] = 200;
                    return res.send(response);
                });
            }
        });
    } else {
        return apiSendError(req, res, 422, "Bad Request check your parameters");
    }
};

exports.getStaffProfile = function(req, res) {
    if (typeof req.query.id == 'undefined') return res.send("id required for getting json");
    Staff.find({
        id: req.query.id
    }, 1, function(err, staffs) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 422, "Bad Request check your parameters");
        }
        if (!staffs.length) {
            return res.apiSendError("Staff not found with perticular id");
        }
        var query_str = `SELECT * from followers WHERE follower_id = ?`;
        db.driver.execQuery(query_str, [staffs[0].unique_id], function(err, rows) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            if (!rows) {
                staffs[0].Following = 0;
            }
            staffs[0].Following = rows.length;
            var query_str = `SELECT * from followers WHERE following_id = ?`;
            db.driver.execQuery(query_str, [staffs[0].unique_id], function(err, rowsss) {
                if (err) {
                    console.log(err);
                    return sendError(req, res, 400, "Bad Request check your parameters");
                }
                if (!rowsss) {
                    staffs[0].Followers = 0;
                }
                staffs[0].Followers = rowsss.length;
                var response = {};
                response['data'] = staffs[0];
                response['code'] = 200;
                return res.send(response);
            });
        });
    });
};

exports.edit = function(req, res) {
    var editedPhoto;
    if (typeof req.body.id == 'undefined') {
        return apiSendError(req, res, 422, "Bad Request check your parameters");
    }
    Staff.find({
        id: req.body.id
    }, function(err, staff) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 422, "Bad Request check your parameters");
        }
        if (!staff.length) {
            return apiSendError(req, res, 404, "Staff not found");
        }
        if (req.files !== undefined && req.files.photo !== undefined && req.files.photo != null) {
            var fs = require('fs');
            var staffImages = __dirname + "/../../../public/upload/staffImages/";
            if (!fs.existsSync(staffImages)) {
                fs.mkdirSync(staffImages);
            }
            var file = req.files.photo;
            var fileExtension = file.name.split(".")[1];
            if (fileExtension) {
                fileName = file.path.split('/');
                fs.rename(file.path, staffImages + fileName[fileName.length - 1] + "." + fileExtension);
                editedPhoto = fileName[fileName.length - 1] + "." + fileExtension;
            }
        }

        if (req.body.id && req.body.first_name && req.body.last_name && req.body.address_1 && req.body.city && req.body.phonenumber && req.body.date_of_birth && req.body.date_of_joining && req.body.employee_username) {
            staff[0].first_name = req.body.first_name;
            staff[0].last_name = req.body.last_name;
            staff[0].photo = (typeof editedPhoto == 'undefined' || editedPhoto === "") ? staff[0].photo : editedPhoto;
            staff[0].address_1 = req.body.address_1;
            staff[0].city = req.body.city;
            // staff[0].country = req.body.country;
            staff[0].state = req.body.state;
            staff[0].pincode = req.body.pincode;
            staff[0].phonenumber = req.body.phonenumber;
            staff[0].date_of_birth = req.body.date_of_birth;
            staff[0].date_of_joining = req.body.date_of_joining;
            staff[0].employee_username = req.body.employee_username;
            staff[0].profile_status = req.body.profile_status;
            staff[0].status = (typeof req.body.status != 'undefined') ? req.body.status : "0";
            staff[0].save(function(err, updated_staff) {
                if (err) {
                    console.log(err);
                    return apiSendError(req, res, 400, "Bad Request check your parameters");
                }
                var response = {};
                response['data'] = [];
                response['code'] = 200;
                return res.send(response);
            });
        } else {
            return apiSendError(req, res, 422, "Bad Request check your parameters");
        }
    });
};
