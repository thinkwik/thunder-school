var bCrypt = require('bcrypt-nodejs');
var db = require('orm').db,
    School = db.models.schools,
    Sessions = db.models.sessions;


exports.getAllSchoolSessions = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = "SELECT t1.*,t2.school_name,CONCAT(t3.first_name, ' ', t3.last_name) as user_name FROM `sessions` as t1 LEFT JOIN schools as t2 on t1.school_id = t2.id LEFT JOIN users as t3 on t1.created_by = t3.id  WHERE  t2.id = ?";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        var response = {};
        response['data'] = rows;
        response['code'] = 200;
        return res.send(response);
    });
};
