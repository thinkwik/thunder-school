app.controller('listStaffIssuesController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.getAllIssues = function() {
            var data = {
                staff_id: $cookieStore.get('loggedInStaff').id
            };
            $http({
                method: 'POST',
                url: '/issues/getAllIssues',
                data: data
            }).then(function successCallback(response) {
                var issues = response.data;
                $scope.issues = issues;
            }, function errorCallback(error) {
                toaster.pop('error', 'Issue', 'cannot get all the Issues! try reloading page.');
            });
        };

        $scope.response = function(issue_id) {
            window.open('school/staff/issues/response/' + issue_id, '_blank');
        };

        $scope.viewIssues = function(issue_id) {
            window.open('school/staff/issues/view/' + issue_id, '_blank');
        };

        $scope.removeIssue = function(issue_id) {
            var data = {
                id: issue_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/issues/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllIssues();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Issues', error.data.message);
                });
            }
        };

        $scope.changeStatus = function(issue_status) {
            var data = {
                id: issue_status.id,
                status: issue_status.status
            };
            $http({
                method: 'POST',
                url: '/issues/updateStatus',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Issue', 'Status updated successfully!');
            }, function errorCallback(error) {
                toaster.pop('error', 'Issue', 'cannot get all the Issues! try reloading page.');
            });
        };
    }
]);

app.controller('viewStaffIssuesController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', "$interval",
    function($scope, $http, $state, toaster, $cookieStore, $interval) {
        $scope.viewMessage = function() {
            var data = {
                id: $state.params.issue_id
            };
            $http({
                method: 'POST',
                url: '/issue_messages/showIssueMessagesToStaff',
                data: data
            }).then(function successCallback(response) {
                var issuesData = response.data;
                $scope.issuesData = issuesData;
                $scope.response = null;
                toaster.pop('success', 'Issue', 'Status updated successfully!');
                $scope.getAllIssues();
            }, function errorCallback(error) {
                toaster.pop('error', 'Issue', 'cannot get all the Issues! try reloading page.');
            });
        };

        $scope.getAllIssues = function() {
            var data = {
                staff_id: $cookieStore.get('loggedInStaff').id
            };
            $http({
                method: 'POST',
                url: '/issues/getAllIssues',
                data: data
            }).then(function successCallback(response) {
                var issues = response.data;
                $scope.issues = issues;
            }, function errorCallback(error) {
                toaster.pop('error', 'Issue', 'cannot get all the Issues! try reloading page.');
            });
        };

        $scope.btnBack = function() {
            $state.go('schoolstaff.issues_list');
        };

        $scope.btnRefresh = function() {
            $scope.viewMessage();
        };
    }
]);

app.controller('responseIssuesController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.responseMessage = function() {
            if ($scope.$parent.issues[0].student_id) {
                var data = {
                    issue_id: $state.params.issue_id,
                    message_from: $cookieStore.get('loggedInStaff').id,
                    message_to: $scope.$parent.issues[0].student_id,
                    message: $scope.response.message
                };
            } else {
                var data = {
                    issue_id: $state.params.issue_id,
                    message_from: $cookieStore.get('loggedInStaff').id,
                    message_to: $scope.$parent.issues[0].parent_id,
                    message: $scope.response.message
                };
            }
            $http({
                method: 'POST',
                url: '/issue_messages/saveIssuesMessges',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Issue', 'response sent successfully!');
                $scope.response = null;
            }, function errorCallback(error) {
                toaster.pop('error', 'Issue', 'cannot get all the Issues! try reloading page.');
            });
        };
    }
]);
