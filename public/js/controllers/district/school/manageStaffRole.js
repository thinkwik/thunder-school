app.controller('staffRoleListController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
	function($scope, $http, $state, toaster, $cookieStore) {
		$scope.getAllRoleList = function() {
			data = {
				school_id: $cookieStore.get('loggedInSchool').id
			};
			$http({
				method: 'POST',
				url: '/staff/role/getAllStaffRoleSchool',
				data: data
			}).then(function successCallback(response) {
				$scope.schoolroles = response.data;
			}, function errorCallback(error) {
				toaster.pop('error', 'Schools', error.data.message);
			});
		};
		$scope.addNewRoleRedirect = function() {
			$state.go('schools.staff_roles_create');
		};
		$scope.activeRole = function(role_id) {
			var data = {
				id: role_id
			};
			$http({
				method: 'POST',
				url: '/staff/role/active',
				data: data
			}).then(function successCallback(response) {
				$scope.getAllRoleList();
			}, function errorCallback(error) {
				toaster.pop('error', 'Role', error.data.message);
			});
		};
		$scope.inactiveRole = function(role_id) {
			var data = {
				id: role_id
			};
			$http({
				method: 'POST',
				url: '/staff/role/inactive',
				data: data
			}).then(function successCallback(response) {
				$scope.getAllRoleList();
			}, function errorCallback(error) {
				toaster.pop('error', 'Role', error.data.message);
			});
		};
		$scope.deleteRole = function(role_id) {
			var data = {
				id: role_id
			};
			$http({
				method: 'POST',
				url: '/staff/role/delete',
				data: data
			}).then(function successCallback(response) {
				$scope.getAllRoleList();
			}, function errorCallback(error) {
				toaster.pop('error', 'Role', error.data.message);
			});
		};
		$scope.editRole = function(role_id) {
			$state.go('schools.staff_roles_edit', {
				role_id: role_id
			});
		};
	}
]);

app.controller('staffRoleCreateController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
	function($scope, $http, $state, toaster, $cookieStore) {
		$scope.createRole = function() {
			var data = $scope.role;
			data.school_id = $cookieStore.get('loggedInSchool').id;
			var modules = data.modules;
			modules = $.map(modules, function(value, index) {
				return [index];
			});
			data.modules = modules.join(',');
			$http({
				method: 'POST',
				url: '/staff/role/register',
				data: data
			}).then(function successCallback(response) {
				toaster.pop('success', 'Role', 'Added Successfully.');
				$state.go('schools.staff_roles_list');
			}, function errorCallback(error) {
				toaster.pop('error', 'Schools', error.data.message);
			});
		};
		$scope.cancelRole = function() {
			$state.go('schools.staff_roles_list');
		};
	}
]);

app.controller('staffRoleEditController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
	function($scope, $http, $state, toaster, $cookieStore) {
		$scope.getEditRole = function() {
			$http({
				method: 'GET',
				url: '/staff/role/edit?id=' + $state.params.role_id
			}).then(function successCallback(response) {
				$scope.role = response.data;
				var arr = response.data.modules;
				var array1 = arr.split(',');
				var data = {};
				for (var i = 0; i < array1.length; i++) {
					data[array1[i]] = true;
				}
				$scope.role.modules = data;
			}, function errorCallback(error) {
				toaster.pop('error', 'Schools', error.data.message);
			});
		};
		$scope.editSaveRole = function() {
			var data = $scope.role;
			var modules = data.modules;
			console.log(modules);
			modules = $.map(modules, function(value, index) {
				if(value){
					return [index];
				}
			});
			data.modules = modules.join(',');
			console.log(data.modules);
			$http({
				method: 'POST',
				url: '/staff/role/edit',
				data: data
			}).then(function successCallback(response) {
				toaster.pop('success', 'Role', 'Updated successfully.');
				$state.go('schools.staff_roles_list');
			}, function errorCallback(error) {
				toaster.pop('error', 'Role', "Something went wrong! please try again");
				$state.go('schools.staff_roles_list');
			});
		};
		$scope.cancelRole = function() {
			$state.go('schools.staff_roles_list');
		};
	}
]);
