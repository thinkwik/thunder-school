app.controller('createRelationController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore',
    function($scope, $http, $state, toaster, Upload, $cookieStore) {
        $scope.createRelation = function() {
            data = $scope.relation;
            data.school_id = $cookieStore.get('loggedInSchool').id;
            $http({
                method: 'POST',
                url: '/relation/register',
                data: data
            }).then(function successCallback(response) {
                $state.go('schools.list_relations');
                toaster.pop('success', "Relations", "Relation added successfully!!!");
            }, function errorCallback(err) {
                toaster.pop('error', "Error", "Something went wrong!!!");
            });
        };

        $scope.listRelations = function() {
            $state.go('schools.list_relations');
        };
    }
]);

app.controller('listRelationController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore',
    function($scope, $http, $state, toaster, Upload, $cookieStore) {
        $scope.getAllRelations = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/relation/getAllRelations',
                data: data
            }).then(function successCallback(response) {
                var relations = response.data;
                $scope.relations = relations;
            }, function errorCallback(error) {
                toaster.pop('error', 'Relations', 'cannot get all the relations! try reloading page.');
            });
        };

        $scope.deleteRelation = function(relation_id) {
            var data = {
                id: relation_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/relation/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllRelations();
                }, function errorCallback(error) {
                    toaster.pop('error', "Error", "Something went wrong!!!");
                });
            }
        };

        $scope.editRelation = function(relation_id) {
            $state.go('schools.edit_relations', {
                relation_id: relation_id
            });
        };

        $scope.addNewRelation = function() {
            $state.go('schools.create_relations');
        };
    }
]);

app.controller('editRelationController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore',
    function($scope, $http, $state, toaster, Upload, $cookieStore) {
        $scope.listRelation = function() {
            $state.go('schools.list_relations');
        };

        $scope.saveEditRelation = function() {
            var data = $scope.relation;
            $http({
                method: 'POST',
                url: '/relation/edit',
                data: data
            }).then(function successCallback(response) {
                $state.go('schools.list_relations');
                toaster.pop('success', 'Relations', 'Updated Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Relations', error.data.message);
            });
        };

        $scope.getEditRelation = function() {
            $http({
                method: 'GET',
                url: '/relation/edit?id=' + $state.params.relation_id
            }).then(function successCallback(response) {
                $scope.relation = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Relations', error.data.message);
            });
        };
    }
]);
