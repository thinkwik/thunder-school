var bCrypt = require('bcrypt-nodejs');
var db = require('orm').db,
    Event = db.models.events,
    parentEventregistration = db.models.parent_event_registrations,
    studentEventregistration = db.models.student_event_registrations;

exports.getAllDistrictEvents = function(req, res) {
    if (req.body.district_id && req.body.district_id !== 'undefined') {
        var query_str = "SELECT t1.* , t2.school_name FROM `events` as t1 LEFT JOIN schools as t2 ON t1.school_id = t2.id WHERE t2.parent_id = ?";
        db.driver.execQuery(query_str, [req.body.district_id], function(err, rows) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 400, "Bad Request check your parameters");
            }
            if (!rows.length) {
                return apiSendError(req, res, 404, "District Events not found.");
            }
            var response = {};
            response['data'] = rows;
            response['code'] = 200;
            return res.send(response);
        });
    } else {
        return apiSendError(req, res, 422, "District Id required");
    }
};

exports.getAllSchoolEvents = function(req, res) {
    if (req.body.school_id && req.body.school_id !== 'undefined') {
        var query_str = "SELECT t1.* , t2.school_name FROM `events` as t1 LEFT JOIN schools as t2 ON t1.school_id = t2.id WHERE t2.id = ?";
        db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 400, "Bad Request check your parameters");
            }
            if (!rows.length) {
                return apiSendError(req, res, 404, "School Events not found.");
            }
            var response = {};
            response['data'] = rows;
            response['code'] = 200;
            return res.send(response);
        });
    } else {
        return apiSendError(req, res, 422, "School Id required");
    }
};

exports.staff_events = function(req, res) {
    if (req.body.school_id !== 'undefined' && req.body.school_id) {
        var query_str = "SELECT t1.* FROM `events` as t1 LEFT JOIN `schools` as t2 ON t1.school_id = t2.id WHERE t2.id = ? ORDER BY `id` ASC";
        db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 422, "Bad Request check your parameters");
            }
            if (!rows.length) {
                return apiSendError(req, res, 404, "School Events not found.");
            }
            var response = {};
            response['data'] = rows;
            response['code'] = 200;
            return res.send(response);
        });
    } else {
        return apiSendError(req, res, 422, "school ID required!");
    }
};


exports.student_events = function(req, res) {
    if (req.body.school_id !== 'undefined' && req.body.school_id) {
        var query_str = `SELECT t1.id as gallery_id,t1.gallery_title, t2.title,t2.start_date,t2.end_date,t2.address ,t3.school_name
        FROM gallery as t1
        LEFT JOIN events as t2 ON t1.event_id = t2.id
        LEFT JOIN schools as t3 ON t1.school_id = t3.id
        WHERE t3.id = ? ORDER BY t2.start_date ASC`;
        db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 422, "Bad Request check your parameters");
            }
            if (!rows.length) {
                return apiSendError(req, res, 404, "School Events not found.");
            }
            var response = {};
            response['data'] = rows;
            response['code'] = 200;
            return res.send(response);
        });
    } else {
        return apiSendError(req, res, 422, "school ID required!");
    }
};

exports.parent_events = function(req, res) {
    if (req.body.school_id !== 'undefined' && req.body.school_id) {
        var query_str = `SELECT t1.id as gallery_id,t1.gallery_title, t2.title,t2.start_date,t2.end_date,t2.address ,t3.school_name
        FROM gallery as t1
        LEFT JOIN events as t2 ON t1.event_id = t2.id
        LEFT JOIN schools as t3 ON t1.school_id = t3.id
        WHERE t3.id = ? ORDER BY t2.start_date ASC`;
        db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 422, "Bad Request check your parameters");
            }
            if (!rows.length) {
                return apiSendError(req, res, 404, "School Events not found.");
            }
            var response = {};
            response['data'] = rows;
            response['code'] = 200;
            return res.send(response);
        });
    } else {
        return apiSendError(req, res, 422, "school ID required!");
    }
};

exports.viewAlbum = function(req, res) {
    if (req.body.gallery_id !== 'undefined' && req.body.gallery_id) {
        var query_str = "SELECT t1.*,t2.gallery_title FROM image_gallery as t1 LEFT JOIN gallery as t2 on t1.gallery_id = t2.id WHERE  t2.id = ?";
        db.driver.execQuery(query_str, [req.body.gallery_id], function(err, rows) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 422, "Bad Request check your parameters");
            }
            if (!rows.length) {
                return apiSendError(req, res, 404, "School Events not found.");
            }
            var response = {};
            response['data'] = rows;
            response['code'] = 200;
            return res.send(response);
        });
    } else {
        return apiSendError(req, res, 422, "refer parameters");
    }
};





exports.studentRegistration = function(req, res) {
    var newStudentRegistration = new studentEventregistration();
    newStudentRegistration.school_id = req.body.school_id;
    newStudentRegistration.student_id = req.body.student_id;
    newStudentRegistration.event_id = req.body.event_id;
    newStudentRegistration.student_status = (typeof req.body.student_status != 'undefined') ? req.body.student_status : "1";
    newStudentRegistration.save(function(err, registration) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        res.send(registration);
    });
};


exports.parentRegistration = function(req, res) {
    var newParentRegistration = new parentEventregistration();
    newParentRegistration.school_id = req.body.school_id;
    newParentRegistration.parent_id = req.body.parent_id;
    newParentRegistration.event_id = req.body.event_id;
    newParentRegistration.parent_status = (typeof req.body.parent_status != 'undefined') ? req.body.parent_status : "1";
    newParentRegistration.save(function(err, registration) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        res.send(registration);
    });
};
