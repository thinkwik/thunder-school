var db = require('orm').db,
    Attendance = db.models.attendance,
    AttendanceReports = db.models.attendance_reports;

exports.register = function(req, res) {
    if (typeof req.body.staff_id == 'undefined' || typeof req.body.grade_id == 'undefined') {
        return apiSendError(req, res, 422, 'Id required!!');
    }
    newAttendance = new Attendance();
    newAttendance.school_id = req.body.school_id;
    newAttendance.staff_id = req.body.staff_id;
    newAttendance.grade_id = req.body.grade_id;
    newAttendance.subject_id = req.body.subject_id;
    newAttendance.class_id = req.body.class_id;
    newAttendance.date = req.body.date;
    newAttendance.description = req.body.description;
    newAttendance.save(function(err, created_Attendance) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 500, 'Something went wrong!');
        }
        var query_str = 'SELECT t1.student_id as id FROM student_classes as t1  WHERE t1.grade_id = ? AND t1.class_id = ?';
        db.driver.execQuery(query_str, [created_Attendance.grade_id, created_Attendance.class_id], function(err, all_students) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 400, "Bad Request check your parameters");
            }
            if (all_students.length > 0) {
                all_students.forEach(function(value, key) {
                    var data = {
                        attendance_id: created_Attendance.id,
                        student_id: value.id,
                        status: "0"
                    };
                    AttendanceReports.create(data, function(err) {
                        if (err) {
                            console.log(err);
                            return apiSendError(req, res, 500, "Something went wrong!");
                        }
                    });
                });
                var response = {};
                response['data'] = [];
                response['code'] = 200;
                return res.send(response);
            } else {
                var response = {};
                response['data'] = [];
                response['code'] = 200;
                return res.send(response);
            }
        });
    });
};

exports.apiGetAttendanceStaffwise = function(req, res) {
    if (typeof req.body.staff_id == 'undefined' || req.body.staff_id === null) {
        return apiSendError(req, res, 422, "Staff ID required");
    }
    var query_str = `SELECT t1.*, t2.grade,CONCAT(t3.first_name,t3.last_name) as staff_name,t4.subject_name
                    FROM attendance as t1
                    LEFT JOIN grades as t2 ON t1.grade_id = t2.id
                    LEFT JOIN staff as t3 ON t1.staff_id = t3.id
                    LEFT JOIN subjects as t4 ON t1.subject_id = t4.id
                    WHERE t1.staff_id = ? ORDER BY t1.date DESC`;
    db.driver.execQuery(query_str, [req.body.staff_id], function(err, rows) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 400, "Bad Request check your parameters");
        }
        var response = {};
        response['data'] = rows;
        response['code'] = 200;
        return res.send(response);
    });
};

exports.apiAttend = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return apiSendError(req, res, 404, 'AttendanceReports id required!!');
    }
    AttendanceReports.find({
        id: req.body.id
    }, 1, function(err, reports) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 422, 'Something went wrong!');
        }
        reports[0].status = (reports[0].status == "0") ? "1" : "0";
        reports[0].save(function(err, saved_report) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 422, 'AttendanceReports id required!');
            }
            var response = {};
            response['data'] = [];
            response['code'] = 200;
            return res.send(response);
        });
    });
};

exports.apiMakeAttendAll = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return apiSendError(req, res, 404, 'Homework id required!!');
    }
    var query_str = 'UPDATE `attendance_reports` SET `status`= ? WHERE `attendance_id` = ?';
    db.driver.execQuery(query_str, ["1", req.body.id], function(err, rows) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 400, "Bad Request check your parameters");
        }
        var response = {};
        response['data'] = [];
        response['code'] = 200;
        return res.send(response);
    });
};

exports.apiMakeNotAttendAll = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return apiSendError(req, res, 404, 'Homework id required!!');
    }
    var query_str = 'UPDATE `attendance_reports` SET `status`= ? WHERE `attendance_id` = ?';
    db.driver.execQuery(query_str, ["0", req.body.id], function(err, rows) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 400, "Bad Request check your parameters");
        }
        var response = {};
        response['data'] = [];
        response['code'] = 200;
        return res.send(response);
    });
};

exports.apiGetAllStudentList = function(req, res) {
    if (typeof req.body.attendance_id == 'undefined') {
        return apiSendError(req, res, 422, "Attendance ID required");
    }
    var query_str = `SELECT t1.*, CONCAT(t2.first_name, " ", t2.last_name) as student_name ,t2.image as student_image
                     FROM attendance_reports as t1
                     LEFT JOIN admissions as t2 ON t1.student_id = t2.id
                     WHERE t1.attendance_id = ? ORDER BY t1.created_at DESC,t1.status DESC`;
    db.driver.execQuery(query_str, [req.body.attendance_id], function(err, rows) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 400, "Bad Request check your parameters");
        }
        var response = {};
        response['data'] = rows;
        response['code'] = 200;
        return res.send(response);
    });
};

exports.apiGetAllStudentAttendence = function(req, res) {
    if (typeof req.body.student_id == 'undefined') {
        return apiSendError(req, res, 422, "Student ID required");
    }
    var query_str = `SELECT t1.id, t1.status, t2.date, CONCAT(t3.first_name, ' ', t3.last_name) as staff_name, t4.grade, t5.subject_name
                    FROM attendance_reports as t1
                    LEFT JOIN attendance as t2 ON t1.attendance_id = t2.id
                    LEFT JOIN staff as t3 ON t2.staff_id = t3.id
                    LEFT JOIN grades as t4 ON t2.grade_id = t4.id
                    LEFT JOIN subjects as t5 ON t2.subject_id = t5.id
                    WHERE t1.student_id = ? ORDER BY t1.status ASC,t2.date DESC LIMIT  30`;
    db.driver.execQuery(query_str, [req.body.student_id], function(err, rows) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 400, "Bad Request check your parameters");
        }
        var response = {};
        response['data'] = rows;
        response['code'] = 200;
        return res.send(response);
    });
};

exports.getAttendance = function(req, res) {
    if (typeof req.query.id == 'undefined') {
        return apiSendError(req, res, 404, 'Assignment id required!!');
    }
    Attendance.find({
        id: req.query.id
    }, 1, function(err, attendances) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 404, 'Attendance not found!!');
        }
        if (!attendances.length) {
            return apiSendError(req, res, 404, "Attendance not found");
        }
        var response = {};
        response['data'] = attendances[0];
        response['code'] = 200;
        return res.send(response);
    });
};

exports.edit = function(req, res) {
    if (req.body.id) {
        Attendance.find({
            id: req.body.id
        }, 1, function(err, attendance) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 404, 'Attendance not found!!');
            }
            attendance[0].date = req.body.date;
            attendance[0].description = req.body.description;
            attendance[0].save(function(err, updated_attendance) {
                if (err) {
                    console.log(err);
                    return apiSendError(req, res, 400, "Bad Request check your parameters");
                }
                var response = {};
                response['data'] = [];
                response['code'] = 200;
                return res.send(response);
            });
        });
    } else {
        return apiSendError(req, res, 404, 'Attendance id required!!');
    }
};

exports.apiDeleteAttendance = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return apiSendError(req, res, 404, 'Attendance id required!!');
    }
    Attendance.find({
        id: req.body.id
    }, 1, function(err, attendance) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 404, 'Attendance not found!!');
        }
        attendance[0].remove(function(err) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 400, "Bad Request check your parameters");
            }
            var response = {};
            response['data'] = [];
            response['code'] = 200;
            return res.send(response);
        });
    });
};
