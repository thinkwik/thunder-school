app.controller('createSchoolProductCategoryController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {

        $scope.addCategory = function() {
            $state.go('schools.productCategory_create');
        };

        $scope.cancelCategoryForm = function() {
            $state.go('schools.productCategory_list');
        };

        $scope.createCategory = function() {
            data = $scope.category;
            data.school_id = $cookieStore.get('loggedInSchool').id;
            $http({
                method: "POST",
                url: "/category/register",
                data: data
            }).then(function successCallback(response) {
                $state.go('schools.productCategory_list');
                toaster.pop('success', 'Category', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Category', error.data.message);
            });
        };
    }
]);

app.controller('listSchoolProductCategoryController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.addNewCategory = function() {
            $state.go('schools.productCategory_create');
        };

        $scope.inactiveCategory = function(category_id) {
            var data = {
                id: category_id
            };
            $http({
                method: 'POST',
                url: '/category/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllCategories();
            }, function errorCallback(error) {
                toaster.pop('error', 'Category', error.data.message);
            });
        };

        $scope.activeCategory = function(category_id) {
            var data = {
                id: category_id
            };
            $http({
                method: 'POST',
                url: '/category/active',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllCategories();
            }, function errorCallback(error) {
                toaster.pop('error', 'Category', error.data.message);
            });
        };

        $scope.deletecategory = function(category_id) {
            var data = {
                id: category_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/category/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllCategories();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Category', error.data.message);
                });
            }
        };

        $scope.getAllCategories = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/category/getAllSchoolCategories',
                data: data
            }).then(function successCallback(response) {
                var categories = response.data;
                $scope.categories = categories;
            }, function errorCallback(error) {
                toaster.pop('error', 'Category', 'cannot get all the Categories! try reloading page.');
            });
        };

        $scope.editCategory = function(category_id) {
            $state.go('schools.productCategory_edit', {
                category_id: category_id
            });
        };
    }
]);

app.controller('editSchoolProductCategoryController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.getEditCategory = function() {
            $http({
                method: 'GET',
                url: '/category/edit?id=' + $state.params.category_id
            }).then(function successCallback(response) {
                $scope.category = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Category', 'cannot get all the Category! try reloading page.');
            });
        };

        $scope.saveEditCategory = function() {
            data = $scope.category;
            $http({
                method: 'POST',
                url: '/category/edit',
                data: data
            }).then(function successCallback(response) {
                $state.go('schools.productCategory_list');
                toaster.pop('success', 'Category', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Category', error.data.message);
            });
        };

        $scope.cancelCategoryForm = function() {
            $state.go('schools.productCategory_list');
        };
    }
]);
