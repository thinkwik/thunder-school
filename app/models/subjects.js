var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Subjects = db.define('subjects', {
    school_id: {
        type: 'integer'
    },
    grade_id: {
        type: 'integer'
    },
    department_id: {
        type: 'integer'
    },
    session_id: {
        type: 'integer'
    },
    subject_name: String,
    type: String,
    description: String,
    status: ['0', '1']
}, {
    timestamp: true,
    methods: {

    }
});
