var db = require('orm').db,
    Lectures = db.models.lectures;

exports.register = function(req, res) {
    newLecture = new Lectures();
    newLecture.school_id = req.body.school_id;
    newLecture.session_id = req.body.session_id;
    newLecture.staff_id = req.body.staff_id;
    newLecture.grade_id = req.body.grade_id;
    newLecture.department_id = req.body.department_id;
    newLecture.subject_id = req.body.subject_id;
    newLecture.location_id = (req.body.location_id !== undefined) ? req.bo.location_id : null;
    newLecture.date = req.body.date;
    newLecture.start_time = req.body.start_time;
    newLecture.end_time = req.body.end_time;
    newLecture.save(function(err, created_lecture) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        return res.send(created_lecture);
    });
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Lecture id required!!');
    }
    Lectures.find({
        id: req.body.id
    }, 1, function(err, lectures) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        lectures[0].School_id = req.body.School_id;
        lectures[0].session_id = req.body.session_id;
        lectures[0].staff_id = req.body.staff_id;
        lectures[0].grade_id = req.body.grade_id;
        lectures[0].Subject_id = req.body.Subject_id;
        lectures[0].department_id = req.body.department_id;
        lectures[0].location_id = (req.body.location_id !== undefined) ? req.body.location_id : lectures[0].location_id;
        lectures[0].date = req.body.date;
        lectures[0].start_time = req.body.start_time;
        lectures[0].end_time = req.body.end_time;
        lectures[0].student_id = req.body.student_id;
        lectures[0].save(function(err, updated_lectures) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(updated_lectures);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Lecture id required!!');
    }
    Lectures.find({
        id: req.body.id
    }, 1, function(err, lectures) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        lectures[0].remove(function(err) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(lectures[0]);
        });
    });
};

exports.getLecture = function(req, res) {
    if (typeof req.query.id == 'undefined') {
        return sendError(req, res, 401, "ID required field");
    }
    Lectures.find({
        id: req.query.id
    }, 1, function(err, lectures) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        if (!(lectures.length)) return sendError(req, res, 404, "Lecture not found");
        return res.send(lectures[0]);
    });
};

exports.getAllLecturesSchoolWise = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id == 'null') {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = 'SELECT t1.*,CONCAT(t2.first_name," ",t2.last_name) as staff_name,t3.grade,t4.subject_name,t5.session_name,t6.department_name FROM lectures as t1 LEFT JOIN staff as t2 on t1.staff_id = t2.id LEFT JOIN grades as t3 on t1.grade_id = t3.id LEFT JOIN subjects as t4 on t1.subject_id = t4.id LEFT JOIN sessions as t5 on t1.session_id = t5.id LEFT JOIN departments as t6 on t1.department_id = t6.id WHERE t1.school_id = ?';
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getAllLecturesStaffWise = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id == 'null' && req.body.staff_id == 'undefined' || req.body.staff_id == 'null') {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = 'SELECT t1.*,CONCAT(t2.first_name," ",t2.last_name) as staff_name,t3.grade,t4.subject_name,t5.session_name,t6.department_name FROM lectures as t1 LEFT JOIN staff as t2 on t1.staff_id = t2.id LEFT JOIN grades as t3 on t1.grade_id = t3.id LEFT JOIN subjects as t4 on t1.subject_id = t4.id LEFT JOIN sessions as t5 on t1.session_id = t5.id LEFT JOIN departments as t6 on t1.department_id = t6.id WHERE t1.school_id = ? AND t1.staff_id = ?';
    db.driver.execQuery(query_str, [req.body.school_id, req.body.staff_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
