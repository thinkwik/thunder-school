var db = require('orm').db,
    Admissions = db.models.admissions,
    InquiryFollowUps = db.models.inquiryfollowups;

exports.register = function(req, res) {
    newInquiryFollowUps = new InquiryFollowUps();
    newInquiryFollowUps.student_id = req.body.student_id;
    newInquiryFollowUps.followup = req.body.followup;
    newInquiryFollowUps.remarks = req.body.remarks;
    newInquiryFollowUps.next_follow_date = req.body.next_follow_date;
    newInquiryFollowUps.normal_status = req.body.normal_status;
    newInquiryFollowUps.inquiry_status = req.body.inquiry_status;
    newInquiryFollowUps.status = (typeof req.body.status != 'undefined') ? req.body.status : '1';
    newInquiryFollowUps.save(function(err, created_InquiryFollowUps) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        return res.send(created_InquiryFollowUps);
    });
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'InquiryFollowUp id required!!');
    }
    InquiryFollowUps.find({
        id: req.body.id
    }, 1, function(err, followup) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        followup[0].student_id = req.body.student_id;
        followup[0].followup = req.body.followup;
        followup[0].remarks = req.body.remarks;
        followup[0].next_follow_date = req.body.next_follow_date;
        followup[0].normal_status = req.body.normal_status;
        followup[0].inquiry_status = req.body.inquiry_status;
        followup[0].status = (typeof req.body.status != 'undefined') ? req.body.status : followup[0].status;
        followup[0].save(function(err, updated_followup) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(updated_followup);
        });
    });
};

exports.active = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'InquiryFollowUps id required!!');
    }
    InquiryFollowUps.find({
        id: req.body.id
    }, 1, function(err, followup) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        followup[0].status = "1";
        followup[0].save(function(err, activated_followup) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(activated_followup);
        });
    });
};

exports.inactive = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'InquiryFollowUps id required!!');
    }
    InquiryFollowUps.find({
        id: req.body.id
    }, 1, function(err, followup) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        followup[0].status = "0";
        followup[0].save(function(err, inactivated_followup) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(inactivated_followup);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'InquiryFollowUps id required!!');
    }
    InquiryFollowUps.find({
        id: req.body.id
    }, 1, function(err, followup) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        followup[0].remove(function(err) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(followup[0]);
        });
    });
};

exports.getInquiryFollowUp = function(req, res) {
    InquiryFollowUps.find({
        student_id: req.body.student_id
    }, function(err, allInquiryFollowUps) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(allInquiryFollowUps);
    });
};
