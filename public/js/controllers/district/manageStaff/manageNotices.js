app.controller('createNoticeController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        $scope.listNotices = function() {
            $state.go('schools.notice_list');
        };

        $scope.grade_show = false;
        $scope.notice = {};
        $scope.notice.send_option = 'sms';
        $scope.notice.grade_id = '';
        $scope.notice.class_id = '';
        $scope.student_checked = false;
        $scope.parents_checked = false;
        $scope.checkStudent = function(type, flag){
            if(type == 'student' && flag == true ){
                $scope.grade_show = true;
                $scope.student_checked = true;
            } else if(type == 'student' && flag == false ){
                $scope.student_checked = false;
                if($scope.parents_checked == false){
                    $scope.grade_show = false;
                }
            } else if(type == 'parents' && flag == true ){
                $scope.grade_show = true;
                $scope.parents_checked = true;
            } else if(type == 'parents' && flag == false ){
                $scope.parents_checked = false;
                if($scope.student_checked == false){
                    $scope.grade_show = false;
                }
            } else if($scope.student_checked == false && $scope.parents_checked == false) {
                $scope.grade_show = false;
            }
        };

        $scope.createNotices = function() {
            data = $scope.notice;
            data.school_id = $cookieStore.get("loggedInSchool").id;
            data.school_auth_person_id = $cookieStore.get("loggedInSchool").authorized_user_id;
            data.school_auth_person_name = $cookieStore.get("loggedInSchool").ap_name;
            data.school_auth_person_email = $cookieStore.get("loggedInSchool").email;
            data.school_auth_person_mobile = $cookieStore.get("loggedInSchool").ap_mobile;            
            var send_notice = $scope.notices;
            $scope.noticeArray = [];
            angular.forEach(send_notice, function(notice) {
                if (notice.selected) $scope.noticeArray.push(notice.name);
            });
            data.send_notice = $scope.noticeArray.toString();

            Loading(true);
            if($scope.notice.send_option == 'sms'){
                $scope.createNoticesSMS(data);
            } else if($scope.notice.send_option == 'email'){
                $scope.createNoticesEmail(data);
            } else {
                $scope.createNoticesEmail(data);
                $scope.createNoticesSMS(data);
            }
            setTimeout(function(){
                Loading(false);
                toaster.pop('success', 'Notice', 'Send Successfully.');
                $state.go('schools.notice_list');
            }, 4500);
        };

        $scope.createNoticesEmail = function(data) {
            
            $http({
                method: "POST",
                url: "/notice/create_email",
                data: data
            }).then(function successCallback(response) {
            }, function errorCallback(error) {
                toaster.pop('error', 'Notice', error.data.message);
            });
        };

        $scope.createNoticesSMS = function(data) {
            $http({
                method: "POST",
                url: "/notice/create_sms",
                data: data
            }).then(function successCallback(response) {
            }, function errorCallback(error) {
                toaster.pop('error', 'Notice', error.data.message);
            });
        };

        

        

        $scope.getAllSchoolGrades = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/grade/getAllSchoolGrades',
                data: data
            }).then(function successCallback(response) {
                $scope.grades = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Grade', 'cannot get all the grades! try reloading page.');
            });
        };

        $scope.getAllSchoolClasses = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/class/getAllSchoolClass',
                data: data
            }).then(function successCallback(response) {
                $scope.classes = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Grade', 'cannot get all the grades! try reloading page.');
            });
        };


        $scope.init = function(){
            $scope.getAllSchoolGrades();
            $scope.getAllSchoolClasses();
        };
        $scope.init();

        $scope.notices = [{
            id: 1,
            name: 'staff'
        }, {
            id: 2,
            name: 'student'
        }, {
            id: 3,
            name: 'parents'
        }];
    }
]);

app.controller('listNoticesController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.addNewNotice = function() {
            $state.go('schools.notice_create');
        };

        $scope.getAllNotices = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/notice/getAllNotices',
                data: data
            }).then(function successCallback(response) {
                var notices = response.data;
                $scope.notices = notices;
            }, function errorCallback(error) {
                toaster.pop('error', 'Notice', 'cannot get all the notices! try reloading page.');
            });
        };

        $scope.editNotice = function(notice_id) {
            $state.go('schools.notice_edit', {
                notice_id: notice_id
            });
        };

        $scope.deleteNotice = function(notice_id) {
            var data = {
                id: notice_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/notice/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllNotices();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Notice', error.data.message);
                });
            }
        };
    }
]);


app.controller('editNoticeController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        $scope.getEditNotice = function() {
            $http({
                method: 'GET',
                url: '/notice/edit?id=' + $state.params.notice_id
            }).then(function successCallback(response) {
                $scope.notice = response.data;
                var arr = response.data.send_notice_to;
                var array1 = arr.split(',');
                var data = {};
                for (var i = 0; i < array1.length; i++) {
                    data[array1[i]] = true;
                }
                $scope.notice.send_notice = data;
                $scope.notice = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Notices', 'cannot get all the notices! try reloading page.');
            });
        };

        $scope.updateNotice = function() {
            data = $scope.notice;
            var send_notice = data.send_notice;
            $scope.noticeArray = [];
            send_notice = $.map(send_notice, function(value, index) {
                if (index) {
                    if (value === true) {
                        $scope.noticeArray.push(index);
                    }
                }
            });
            data.send_notice = $scope.noticeArray.toString();
            $http({
                method: 'POST',
                url: '/notice/edit',
                data: data
            }).then(function successCallback(response) {
                $state.go('schools.notice_list');
                toaster.pop('success', 'Notice', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Notice', error.data.message);
            });
        };

        $scope.listNotices = function() {
            $state.go('schools.notice_list');
        };

    }
]);
