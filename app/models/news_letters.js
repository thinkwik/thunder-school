var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Blogs = db.define('news_letters', {
    subject: String,
    users_to_understand: String,
    content: String,
    time_period:String,
    status: ['0', '1']
}, {
    timestamp: true,
    methods: {}
});
