// app.controller('saveStudentsParentsController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
//     function($scope, $http, $state, toaster, $cookieStore) {
//         $scope.createStudentsParents = function() {
//             data = $scope.parents;
//             data.student_id = $state.params.student_id;
//             $http({
//                 method: "POST",
//                 url: "/parents/register",
//                 data: data
//             }).then(function successCallback(response) {
//                 toaster.pop('success', 'Parents', 'Saved Successfully.');
//                 $state.go('schoolstaff.student_list');
//             }, function errorCallback(error) {
//                 toaster.pop('error', 'Parents', error.data.message);
//             });
//         };
//
//         $scope.createStudentsParentsTab = function() {
//             data = $scope.parents;
//             data.student_id = $state.params.student_id;
//             $http({
//                 method: "POST",
//                 url: "/parents/edit",
//                 data: data
//             }).then(function successCallback(response) {
//                 toaster.pop('success', 'Parents Profile', 'Update Successfully.');
//             }, function errorCallback(error) {
//                 toaster.pop('error', 'Parents Profile', error.data.message);
//             });
//         };
//
//         $scope.getStudentsParents = function() {
//             var data = {
//                 student_id: $state.params.student_id
//             };
//             console.log(data);
//             $http({
//                 method: 'POST',
//                 url: '/parents/getStudentsParents',
//                 data: data
//             }).then(function successCallback(response) {
//                 console.log(response.data);
//                 $scope.parents = response.data;
//             }, function errorCallback(error) {
//                 toaster.pop('error', 'Session', 'Something went wrong!');
//             });
//         };
//
//         $scope.deleteParent = function(student_id) {
//             var data = {
//                 id: student_id
//             };
//             var result = confirm("Please confirm, Do you really want to delete ?");
//             if (result) {
//                 $http({
//                     method: 'POST',
//                     url: '/parents/delete',
//                     data: data
//                 }).then(function successCallback(response) {
//                     $scope.getStudentsParents();
//                 }, function errorCallback(error) {
//                     toaster.pop('error', 'Parents', error.data.message);
//                 });
//             }
//         };
//
//         $scope.editParent = function(student_id) {
//             $state.go('schoolstaff.student_parents_edit', {
//                 student_id: student_id
//             });
//         };
//
//
//         $scope.addNewParents = function() {
//             var student_id = $state.params.student_id;
//             $state.go('schoolstaff.student_parents', {
//                 student_id: student_id
//             });
//         };
//
//     }
// ]);
//
//
//
//
// app.controller('editStudentsParentsController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
//     function($scope, $http, $state, toaster, $cookieStore) {
//         $scope.editStudentsParents = function(student_id) {
//             data = $scope.parents;
//             $http({
//                 method: 'POST',
//                 url: '/parents/edit',
//                 data: data
//             }).then(function successCallback(response) {
//                 $state.go('schoolstaff.student_profile_view');
//                 toaster.pop('success', 'Parents Profile', 'Updated Successfully.');
//             }, function errorCallback(error) {
//                 toaster.pop('error', 'Parents Profile', error.data.message);
//             });
//         };
//
//         $scope.getStudentsParents = function(student_id) {
//             var data = {
//                 id: student_id
//             };
//             $http({
//                 method: 'GET',
//                 url: '/parents/edit?id=' + $state.params.student_id,
//             }).then(function successCallback(response) {
//                 $scope.parents = response.data;
//             }, function errorCallback(error) {
//                 toaster.pop('error', 'Parents', 'cannot get all the parents! try reloading page.');
//             });
//         };
//
//         $scope.cancelParentsForm = function() {
//             $state.go('schoolstaff.student_profile_view');
//         };
//     }
// ]);
