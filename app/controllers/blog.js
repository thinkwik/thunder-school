var db = require('orm').db,
    Blogs = db.models.blogs;

exports.register = function(req, res) {
    newBlog = new Blogs();
    newBlog.blog_name = req.body.blog_title;
    newBlog.blog_content = req.body.blog_content;
    newBlog.image_url = req.body.image_url;
    newBlog.status = (typeof req.body.status != 'undefined') ? req.body.status : '1';
    newBlog.save(function(err, created_blog) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(created_blog);
    });
};

exports.getAllBlogs = function(req, res) {

    Blogs.find({}, function(err, allBlogs) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(allBlogs);
    });
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Blog id required!!');
    }
    Blogs.find({
        id: req.body.id
    }, 1, function(err, blogs) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        if (!blogs.length) {
            return sendError(req, res, 404, 'No blog found');
        }
        blogs[0].blog_name = req.body.blog_title;
        blogs[0].blog_content = req.body.blog_content;
        blogs[0].image_url = (typeof req.body.image_url == 'undefined' || typeof req.body.image_url === "" || typeof req.body.image_url === undefined) ? blogs[0].image_url : req.body.image_url;
        blogs[0].status = (typeof req.body.status != 'undefined') ? req.body.status : blogs[0].status;
        blogs[0].save(function(err, updated_blog) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, 'please check your parameters');
            }
            return res.send(updated_blog);
        });
    });
};

exports.active = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Blog id required!!');
    }
    Blogs.find({
        id: req.body.id
    }, 1, function(err, blog) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        blog[0].status = "1";
        blog[0].save(function(err, activated_blog) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(activated_blog);
        });
    });
};

exports.inactive = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Blog id required!!');
    }
    Blogs.find({
        id: req.body.id
    }, 1, function(err, blog) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        blog[0].status = "0";
        blog[0].save(function(err, inactivated_blog) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(inactivated_blog);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Blog id required!!');
    }
    Blogs.find({
        id: req.body.id
    }, 1, function(err, blogs) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        blogs[0].remove(function(err) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(blogs[0]);
        });
    });
};

exports.getBlog = function(req, res) {
    if (typeof req.query.id == 'undefined') return sendError(req, res, 401, "ID required field");
    var query_str = 'SELECT * FROM blogs where id = ? order by id desc';
    db.driver.execQuery(query_str, [req.query.id], function(err, blogs) {
    if (err) {
        console.log(err);
        return sendError(req, res, 400, "Bad Request check your parameters");
    }
    return res.send(blogs[0]);
    });
};

exports.uploadFile = function(req, res) {
    var fs = require('fs');
    var blogImages = __dirname + "/../../public/upload/blogImages/";
    if (!fs.existsSync(blogImages)) {
        fs.mkdirSync(blogImages);
    }
    var file = req.files.file;
    var fileExtension = file.name.split(".")[1];
    if (fileExtension) {
      fileName = file.path.split('/');
      fs.rename(file.path, blogImages + fileName[fileName.length - 1] +"."+ fileExtension);
      return res.send(fileName[fileName.length - 1] +"."+ fileExtension);
    }
    else {
      return sendError(req, res, 400, "Bad Request check your parameters");
    }

};
