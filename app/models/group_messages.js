var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var GroupMessage = db.define('group_messages', {
    from_user:String,
    chat_room_id:{
        type: 'integer'
    },
    message:String
}, {
    timestamp: true,
    methods: {

    }
});
