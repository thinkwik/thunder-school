app.config(['$compileProvider', function($compileProvider) {
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(|blob|):/);
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|file|blob|pdf|doc):/);
}]);

app.controller('downloadParentAssignmentController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', '$window',
    function($scope, $http, $state, toaster, $cookieStore, $window) {
        $scope.getAllAssignments = function() {
            var data = {
                student_id: $cookieStore.get('loggedInParent').student_id
            };
            $http({
                method: 'POST',
                url: '/assignment/getAllStudentAssignments',
                data: data
            }).then(function successCallback(response) {
                var assignments = response.data;
                $scope.assignments = assignments;
            }, function errorCallback(error) {
                toaster.pop('error', 'Assignments', 'cannot get all the Assignments! try reloading page.');
                console.log(error.data.message);
            });
        };

        $scope.DownloadAssignment = function(assignment) {
            if(assignment.file_name) {
                var assFileName = assignment.file_name;
                assFileName = assFileName.split(".");
                var url =  $window.location.origin + '/upload/assignments/' + assignment.file_name;
                var request = new XMLHttpRequest();
                request.open('HEAD', url, false);
                request.send();
                if(request.status == 200) {
                  var element = angular.element('<a/>');
                  element.attr({
                      href: url,
                      target: '_blank',
                      download: assignment.title + '.' + assFileName[1]
                  })[0].click();
                } else {
                  toaster.pop('error', 'Assignments', 'Assignment not found!');
                  return false;
                }
            } else {
              toaster.pop('error', 'Assignments', 'Assignment not found!');
              return false;
            }
        };
    }
]);
