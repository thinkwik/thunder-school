app.config(function($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider.otherwise("/");
    //Manage District Admins
    $stateProvider.state('districts', {
            url: '/districts',
            templateUrl: '/partials/profile/districts.html',
            abstract: true
        })
        //Manage Dashboard
        .state('districts.dashboard', {
            url: '/school/dashboard',
            templateUrl: '/partials/superAdmin/district/manageDashboard/dashboard.html',
            controller: 'districtAdminDashboardController'
        })
        //Manage Profile
        .state('districts.edit_profile', {
            url: '/user_profile',
            templateUrl: '/partials/superAdmin/district/header/editProfile.html',
            controller: 'editDistrictAdminController'
        })
        .state('districts.changepassword', {
            url: '/changepassword',
            templateUrl: '/partials/superAdmin/district/header/changePassword.html'
        })
        //Manage To-Dos
        .state('districts.todo_create', {
            url: '/todo/create',
            templateUrl: '/partials/superAdmin/district/manageTodo/createTodo.html',
            controller: 'districtAdminTodoCreateController'
        })
        .state('districts.todo_list', {
            url: '/todo/list',
            templateUrl: '/partials/superAdmin/district/manageTodo/listTodo.html',
            controller: 'districtAdminTodoListController'
        })
        //Manage School Lists
        .state('districts.district_school_list', {
            url: '/school/list',
            templateUrl: '/partials/superAdmin/district/manageSchool/listSchool.html',
            controller: 'districtSchoolListController'
        })
        .state('districts.district_school_create', {
            url: '/school/create',
            templateUrl: '/partials/superAdmin/district/manageSchool/createSchool.html',
            controller: 'districtSchoolCreateController'
        })
        .state('districts.district_school_edit', {
            url: '/school/edit/:school_id',
            templateUrl: '/partials/superAdmin/district/manageSchool/editSchool.html',
            controller: 'districtSchoolEditController'
        })
        // Manage Gallery
        .state('districts.gallery_list', {
            url: '/gallery/list',
            templateUrl: '/partials/superAdmin/district/manageGallery/listGallery.html',
            controller: "listDistrictGalleryController"
        })
        .state('districts.gallery_create', {
            url: '/gallery/create',
            templateUrl: '/partials/superAdmin/district/manageGallery/createGallery.html',
            controller: "createDistrictGalleryController"
        })
        .state('districts.gallery_edit', {
            url: '/gallery/edit/:gallery_id',
            templateUrl: '/partials/superAdmin/district/manageGallery/editGallery.html',
            controller: 'editDistrictGalleryController'
        })
        .state('districts.gallery_view', {
            url: '/gallery/view/:gallery_id',
            templateUrl: '/partials/superAdmin/district/manageGallery/viewAlbum.html',
            controller: 'viewDistrictGalleryAlbumController'
        })
        .state('districts.image_add', {
            url: '/gallery/addimage/:gallery_id',
            templateUrl: '/partials/superAdmin/district/manageGallery/addImage.html',
            controller: 'addDistrictImageGalleryController'
        })
        //Manage Events
        .state('districts.events_list', {
            url: '/district/events/list',
            templateUrl: '/partials/superAdmin/district/manageEvent/listEvent.html',
            controller: "districtAllEventsListController"
        })
        .state('districts.event_create', {
            url: '/district/events/create',
            templateUrl: '/partials/superAdmin/district/manageEvent/createEvent.html',
            controller: "districtNewEventController"
        })
        .state('districts.event_edit', {
            url: '/district/events/edit/:event_id',
            templateUrl: '/partials/superAdmin/district/manageEvent/editEvent.html',
            controller: 'districtEditEventController'
        })
        //Manage Event Resources
        .state('districts.event_resource_list', {
            url: '/district/events/:event_id/resource/list',
            templateUrl: '/partials/superAdmin/district/manageEvent/listEventResource.html',
            controller: "districtAllEventResourceListController"
        })
        .state('districts.event_resource_create', {
            url: '/district/events/:event_id/resource/create',
            templateUrl: '/partials/superAdmin/district/manageEvent/addEventResource.html',
            controller: "districtNewEventResourceController"
        })
        .state('districts.event_resource_edit', {
            url: '/district/events/:event_id/resource/edit/:resource_id',
            templateUrl: '/partials/superAdmin/district/manageEvent/editEventResource.html',
            controller: 'districtEditEventResourceController'
        })
        //Manage Event Registration
        .state('districts.event_registration_list', {
            url: '/events/registration/list',
            templateUrl: '/partials/superAdmin/district/manageEvent/listEventRegistration.html',
            controller: "districtAllEventRegistrationsListController"
        })
        .state('districts.event_registration_create', {
            url: '/events/registration/create',
            templateUrl: '/partials/superAdmin/district/manageEvent/addEventRegistration.html',
            controller: "districtNewEventRegistrationController"
        })
        .state('districts.event_registration_edit', {
            url: '/events/registration/edit/:registration_id',
            templateUrl: '/partials/superAdmin/district/manageEvent/editEventRegistration.html',
            controller: 'districtEditEventRegistrationController'
        })
        //Manage School Admins
        .state('districts.schoolAdmin_list', {
            url: '/admin/manage/list',
            templateUrl: '/partials/superAdmin/district/manageAdmin/listSchoolAdmins.html',
            controller: 'districtSchoolSuperAdminListController'
        })
        .state('districts.schoolAdmin_create', {
            url: '/admin/manage/create',
            templateUrl: '/partials/superAdmin/district/manageAdmin/addSchoolAdmin.html',
            controller: 'districtSchoolSuperAdminCreateController'
        })
        .state('districts.schoolAdmin_edit', {
            url: '/admin/manage/edit/:user_id',
            templateUrl: '/partials/superAdmin/district/manageAdmin/editSchoolAdmin.html',
            controller: 'districtSchoolSuperAdminEditController'
        })
        // Manage ProductCategory
        .state('districts.productCategory_list', {
            url: '/school/category/list',
            templateUrl: '/partials/superAdmin/district/manageE-commercePortal/manageCategory/listCategory.html',
            controller: 'listDistrictProductCategoryController'
        })
        .state('districts.productCategory_create', {
            url: '/school/category/create',
            templateUrl: '/partials/superAdmin/district/manageE-commercePortal/manageCategory/createCategory.html',
            controller: 'createDistrictProductCategoryController'
        })
        .state('districts.productCategory_edit', {
            url: '/school/category/edit/:category_id',
            templateUrl: '/partials/superAdmin/district/manageE-commercePortal/manageCategory/editCategory.html',
            controller: 'editDistrictProductCategoryController'
        })
        // Manage Products
        .state('districts.product_list', {
            url: '/school/products/list',
            templateUrl: '/partials/superAdmin/district/manageE-commercePortal/manageProducts/listProduct.html',
            controller: 'listDistrictProductController'
        })
        .state('districts.product_create', {
            url: '/school/products/create',
            templateUrl: '/partials/superAdmin/district/manageE-commercePortal/manageProducts/createProduct.html',
            controller: 'createDistrictProductController'
        })
        .state('districts.product_edit', {
            url: '/school/products/edit/:product_id',
            templateUrl: '/partials/superAdmin/district/manageE-commercePortal/manageProducts/editProduct.html',
            controller: 'editDistrictProductController'
        })
        //manage Grades
        .state('districts.grades_list', {
            url: '/school/grades/list',
            templateUrl: '/partials/superAdmin/district/manageAcademics/manageGrades/listGrades.html',
            controller: 'listDistrictGradesController'
        })
        .state('districts.grades_create', {
            url: '/school/grades/create',
            templateUrl: '/partials/superAdmin/district/manageAcademics/manageGrades/createGrades.html',
            controller: 'createDistrictGradesController'
        })
        .state('districts.grades_edit', {
            url: '/school/grades/edit/:grade_id',
            templateUrl: '/partials/superAdmin/district/manageAcademics/manageGrades/editGrades.html',
            controller: 'editDistrictGradesController'
        })
        //manage Class
        .state('districts.class_list', {
            url: '/school/class/list',
            templateUrl: '/partials/superAdmin/district/manageAcademics/manageClasses/listClass.html',
            controller: 'listDistrictClassController'
        })
        .state('districts.class_create', {
            url: '/school/class/create',
            templateUrl: '/partials/superAdmin/district/manageAcademics/manageClasses/createClass.html',
            controller: 'createDistrictClassController'
        })
        .state('districts.class_edit', {
            url: '/school/class/edit/:class_id',
            templateUrl: '/partials/superAdmin/district/manageAcademics/manageClasses/editClass.html',
            controller: 'editDistrictClassController'
        })
        //manage Subjects
        .state('districts.subject_list', {
            url: '/school/subject/list',
            templateUrl: '/partials/superAdmin/district/manageAcademics/manageSubjects/listSubject.html',
            controller: 'listDistrictSubjectController'
        })
        .state('districts.subject_create', {
            url: '/school/subject/create',
            templateUrl: '/partials/superAdmin/district/manageAcademics/manageSubjects/createSubject.html',
            controller: 'createDistrictSubjectController'
        })
        .state('districts.subject_edit', {
            url: '/school/subject/edit/:subject_id',
            templateUrl: '/partials/superAdmin/district/manageAcademics/manageSubjects/editSubject.html',
            controller: 'editDistrictSubjectController'
        })
        //manage Department
        .state('districts.schooldepartment_list', {
            url: '/school/department/list',
            templateUrl: '/partials/superAdmin/district/manageAcademics/manageSchoolDpts/listSchoolDept.html',
            controller: 'listDistrictDeptController'
        })
        .state('districts.schooldepartment_create', {
            url: '/school/department/create',
            templateUrl: '/partials/superAdmin/district/manageAcademics/manageSchoolDpts/createSchoolDept.html',
            controller: 'createDistrictDeptController'
        })
        .state('districts.schooldepartment_edit', {
            url: '/school/department/edit/:department_id',
            templateUrl: '/partials/superAdmin/district/manageAcademics/manageSchoolDpts/editSchoolDept.html',
            controller: 'editDistrictDeptController'
        })
        // Manage Designations
        .state('districts.designation_list', {
            url: '/school/designation/list',
            templateUrl: '/partials/superAdmin/district/manageAcademics/manageDesignation/listDesignation.html',
            controller: 'listDistrictDesignationController'
        })
        .state('districts.designation_create', {
            url: '/school/designation/create',
            templateUrl: '/partials/superAdmin/district/manageAcademics/manageDesignation/createDesignation.html',
            controller: 'createDistrictDesignationController'
        })
        .state('districts.designation_edit', {
            url: '/school/designation/edit/:designation_id',
            templateUrl: '/partials/superAdmin/district/manageAcademics/manageDesignation/editDesignation.html',
            controller: 'editDistrictDesignationController'
        })
        //Manage Materials
        .state('districts.create_material', {
            url: '/material/create',
            templateUrl: '/partials/superAdmin/district/manageMaterials/createMaterial.html',
            controller: 'createDistrictMaterialController'
        }).state('districts.list_material', {
            url: '/material/list',
            templateUrl: '/partials/superAdmin/district/manageMaterials/listMaterials.html',
            controller: 'listDistrictMaterialController'
        }).state('districts.edit_material', {
            url: '/material/edit/:material_id',
            templateUrl: '/partials/superAdmin/district/manageMaterials/editMaterial.html',
            controller: 'editDistrictMaterialController'
        })
        // Manage Sesion
        .state('districts.session_list', {
            url: '/school/session/list',
            templateUrl: '/partials/superAdmin/district/manageSession/listSession.html',
            controller: 'districtListSessionController'
        })
        .state('districts.session_create', {
            url: '/school/session/create',
            templateUrl: '/partials/superAdmin/district/manageSession/createSession.html',
            controller: 'districtCreateSessionController'
        })
        .state('districts.session_edit', {
            url: '/school/session/edit/:session_id',
            templateUrl: '/partials/superAdmin/district/manageSession/editSession.html',
            controller: 'districtEditSessionController'
        })
        // Manage School Roles
        .state('districts.school_roles_list', {
            url: '/school/role/list',
            templateUrl: '/partials/superAdmin/district/manageRole/listRole.html',
            controller: "districtRoleListController"
        })
        .state('districts.school_roles_create', {
            url: '/school/role/create',
            templateUrl: 'partials/superAdmin/district/manageRole/addRole.html',
            controller: "districtRoleCreateController"
        })
        .state('districts.school_roles_edit', {
            url: '/school/role/edit/:role_id',
            templateUrl: 'partials/superAdmin/district/manageRole/editRole.html',
            controller: 'districtRoleEditController'
        })
        // Manage School Fees
        .state('districts.school_fees_list', {
            url: '/school/fee/list',
            templateUrl: '/partials/superAdmin/district/manageFee/listFee.html',
            controller: "districtFeeListController"
        })
        .state('districts.school_fees_create', {
            url: '/school/fee/create',
            templateUrl: 'partials/superAdmin/district/manageFee/addFee.html',
            controller: "districtFeeCreateController"
        })
        .state('districts.school_fees_edit', {
            url: '/school/fee/edit/:fee_id',
            templateUrl: 'partials/superAdmin/district/manageFee/editFee.html',
            controller: 'districtFeeEditController'
        })
        // Manage School Staff
        .state('districts.staff_list', {
            url: '/school/staff/list',
            templateUrl: '/partials/superAdmin/district/manageStaff/listStaff.html',
            controller: 'districtListStaffController'
        })
        .state('districts.staff_create', {
            url: '/school/staff/create',
            templateUrl: '/partials/superAdmin/district/manageStaff/createStaff.html',
            controller: 'districtCreateStaffController'
        })
        .state('districts.staff_edit', {
            url: '/school/staff/edit/:staff_id',
            templateUrl: '/partials/superAdmin/district/manageStaff/editStaff.html',
            controller: 'districtEditStaffController'
        })
        //Mange BookStore
        .state('districts.list_book_store', {
            url: '/school/bookstore/list',
            templateUrl: '/partials/superAdmin/district/school/bookStore/listBookStore.html',
            controller: 'districtListBookStoreController'
        })
        //Manage Epub books
        .state('districts.view_epub_book', {
            url: '/bookstore/epub/view/:book_id',
            templateUrl: '/partials/superAdmin/district/school/bookStore/viewEpubBook.html',
            controller: 'districtViewSchoolEpubBookStoreController'
        })
        .state('districts.create_book_store', {
            url: '/school/bookstore/create',
            templateUrl: '/partials/superAdmin/district/school/bookStore/createBookStore.html',
            controller: 'districtCreateBookStoreController'
        })
        .state('districts.edit_book_store', {
            url: '/school/bookstore/edit/:book_id',
            templateUrl: '/partials/superAdmin/district/school/bookStore/editBookStore.html',
            controller: 'districtEditBookStoreController'
        })
        .state('districts.view_book', {
            url: '/school/bookstore/view/:book_id',
            templateUrl: '/partials/superAdmin/district/school/bookStore/viewBook.html',
            controller: 'districtViewBookSchoolAdminController'
        })
        .state('districts.list_book_category', {
            url: '/book/category/list',
            templateUrl: '/partials/superAdmin/district/school/bookCategory/listBookCategory.html',
            controller: "districtListBookCategoryController"
        })
        .state('districts.create_book_category', {
            url: '/book/category/create',
            templateUrl: '/partials/superAdmin/district/school/bookCategory/createBookCategory.html',
            controller: "districtCreateBookCategoryController"
        })

    $locationProvider.html5Mode(true);
});
