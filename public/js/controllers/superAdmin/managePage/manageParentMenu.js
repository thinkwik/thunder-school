app.controller('listParentMenusController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.addNewMenu = function() {
            $state.go('users.parent_menus_create');
        };
        $scope.getAllParentMenus = function() {
            $http({
                method: 'POST',
                url: '/parent/menus/getAllParentMenus'
            }).then(function successCallback(response) {
                $scope.ParentMenus = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Menus', error.data.message);
            });
        };
        $scope.inactiveParentMenu = function(parent_menu_id) {
            var data = {
                id: parent_menu_id
            };
            $http({
                method: 'POST',
                url: '/parent/menus/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllParentMenus();
            }, function errorCallback(error) {
                toaster.pop('error', 'Menus', error.data.message);
            });
        };
        $scope.activeParentMenu = function(parent_menu_id) {
            var data = {
                id: parent_menu_id
            };
            $http({
                method: 'POST',
                url: '/parent/menus/active',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllParentMenus();
            }, function errorCallback(error) {
                toaster.pop('error', 'Menus', error.data.message);
            });
        };
        $scope.deleteParentMenu = function(parent_menu_id) {
            var data = {
                id: parent_menu_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
            $http({
                method: 'POST',
                url: '/parent/menus/delete',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllParentMenus();
            }, function errorCallback(error) {
                toaster.pop('error', 'Menus', error.data.message);
            });
          }
        };
        $scope.editParentMenu = function(parent_menu_id) {
            $state.go('users.parent_menus_edit', {
                parent_menu_id: parent_menu_id
            });
        };
    }
]);

app.controller('editParentMenusController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.editSaveParentMenu = function() {
            var data = $scope.menu;
            $http({
                method: 'POST',
                url: '/parent/menus/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Menus', 'updated successfully.');
                $state.go('users.parent_menus_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'Menus', error.data.message);
            });
        };
        $scope.getEditParentMenu = function() {
            $http({
                method: 'GET',
                url: '/parent/menus/edit?id=' + $state.params.parent_menu_id
            }).then(function successCallback(response) {
                $scope.menu = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Menu', error.data.message);
            });
        };
        $scope.cancelMenu = function() {
            $state.go('users.parent_menus_list');
        };
    }

]);

app.controller('createParentMenusController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.cancelMenu = function() {
            $state.go('users.parent_menus_list');
        };
        $scope.createMenu = function() {
            var data = $scope.menu;
            $http({
                method: 'POST',
                url: '/parent/menus/register',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Menus', 'added successfully.');
                $state.go('users.parent_menus_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'Menus', error.data.message);
            });
        };
    }
]);
