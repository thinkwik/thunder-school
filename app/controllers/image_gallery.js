var db = require('orm').db,
    Gallery = db.models.gallery,
    Image_gallery = db.models.image_gallery;

exports.register = function(req, res) {
    if (typeof req.body.gallery_id == 'undefined' || req.body.image_name === null || req.body.image_description === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    newImage_gallery = new Image_gallery();
    newImage_gallery.gallery_id = req.body.gallery_id;
    newImage_gallery.image_name = req.body.image_name;
    newImage_gallery.image_description = req.body.image_description;
    newImage_gallery.save(function(err, addedImage) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(addedImage);
    });
};

exports.viewAlbum = function(req, res) {
    if (typeof req.body.gallery_id == 'undefined' || req.body.gallery_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = "SELECT t1.*,t2.gallery_title FROM image_gallery as t1 LEFT JOIN gallery as t2 on t1.gallery_id = t2.id WHERE  t2.id = ?";
    db.driver.execQuery(query_str, [req.body.gallery_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        for (var i = 0; i < rows.length; i++) {
            var file = rows[i].image_name;
            file = file.split('.')[1];
            rows.type = file;
        }
        return res.send(rows);
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Image id required!!');
    }
    Image_gallery.find({
        id: req.body.id
    }, 1, function(err, image_gallery) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        image_gallery[0].remove(function(err) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, "Bad Request check your parameters");
            }
            return res.send(image_gallery[0]);
        });
    });
};

exports.listAllStudentevents = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || typeof req.body.school_id === null) {
        return sendError(req, res, 422, "school id required");
    }
    var query_str = `SELECT t1.id as gallery_id,t1.gallery_title,t2.title,t2.start_date,t2.end_date,t2.address,t2.id as event_id,t4.student_status,t3.school_name
                    FROM gallery as t1 LEFT JOIN events as t2 ON t1.event_id = t2.id
                    LEFT JOIN student_event_registrations as t4 ON t1.event_id = t4.event_id
                    LEFT JOIN schools as t3 ON t1.school_id = ?
                    GROUP BY t1.event_id`;
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};


exports.listAllParentevents = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || typeof req.body.school_id === null) {
        return sendError(req, res, 422, "school id required");
    }
    var query_str = `SELECT t1.id as gallery_id,t1.gallery_title,t2.title,t2.start_date,t2.end_date,t2.address,t2.id as event_id,t4.parent_status,t3.school_name
                    FROM gallery as t1 LEFT JOIN events as t2 ON t1.event_id = t2.id
                    LEFT JOIN parent_event_registrations as t4 ON t1.event_id = t4.event_id
                    LEFT JOIN schools as t3 ON t1.school_id = ?
                    GROUP BY t1.event_id`;
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.listAllStaffevents = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || typeof req.body.school_id === null) {
        return sendError(req, res, 422, "school id required");
    }
    var query_str = 'SELECT t1.id as gallery_id,t1.gallery_title, t2.title,t2.start_date,t2.end_date,t2.address ,t3.school_name FROM `gallery` as t1 LEFT JOIN events as t2 ON t1.event_id = t2.id LEFT JOIN schools as t3 ON t1.school_id = ? GROUP BY t1.event_id';
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
