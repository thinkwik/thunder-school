app.controller('districtAllEventResourceListController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
	function($scope, $http, $state, toaster, $cookieStore) {
		$scope.addNewEventResourceRedirect = function() {
			$state.go('districts.event_resource_create', {
				event_id: $state.params.event_id
			});
		};
		$scope.eventResources = function(event_id) {
			$state.go('districts.event_resource_list', {
				event_id: event_id
			});
		};
		$scope.getAllEventResourceList = function() {
			var data = {
				event_id: $state.params.event_id
			};
			$http({
				method: 'POST',
				url: '/event/resource/getAllEventResources',
				data: data
			}).then(function successCallback(response) {
				$scope.resources = response.data;
			}, function errorCallback(error) {
				toaster.pop('error', 'Event Resource', 'Something went wrong! getting all resources');
			});
		};
		$scope.activeResource = function(resource_id) {
			var data = {
				id: resource_id
			};
			$http({
				method: 'POST',
				url: '/event/resource/active',
				data: data
			}).then(function successCallback(response) {
				$scope.getAllEventResourceList();
			}, function errorCallback(error) {
				toaster.pop('error', 'Event Resource', error.data.message);
			});
		};
		$scope.inactiveResource = function(resource_id) {
			var data = {
				id: resource_id
			};
			$http({
				method: 'POST',
				url: '/event/resource/inactive',
				data: data
			}).then(function successCallback(response) {
				$scope.getAllEventResourceList();
			}, function errorCallback(error) {
				toaster.pop('error', 'Event Resource', error.data.message);
			});
		};
		$scope.deleteResource = function(resource_id) {
			var data = {
				id: resource_id
			};
			$http({
				method: 'POST',
				url: '/event/resource/delete',
				data: data
			}).then(function successCallback(response) {
				$scope.getAllEventResourceList();
			}, function errorCallback(error) {
				toaster.pop('error', 'Event Resource', error.data.message);
			});
		};
		$scope.editResource = function(resource_id) {
			$state.go('districts.event_resource_edit', {
				event_id: $state.params.event_id,
				resource_id: resource_id
			});
		};
	}
]);

app.controller('districtNewEventResourceController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
	function($scope, $http, $state, toaster, $cookieStore) {
		$scope.changeInTotal = function() {
			var total = 0;
			if ($scope.resource.cost && $scope.resource.resource_value) {
				total = $scope.resource.cost * $scope.resource.resource_value;
			}
			if (isNaN(total)) {
				toaster.pop('error', 'Quality/Cost', 'both can only be numbers');
			} else {
				$scope.resource.total_cost = total;
			}
		};
		$scope.cancelResource = function() {
			$state.go('districts.event_resource_list', {
				event_id: $state.params.event_id
			});
		};
		$scope.createResource = function() {
			var data = $scope.resource;
			data.event_id = $state.params.event_id;
			$http({
				method: 'POST',
				url: '/event/resource/register',
				data: data
			}).then(function successCallback(response) {
				toaster.pop('success', 'Event Resource', "added successfully.");
				$state.go('districts.event_resource_list', {
					event_id: $state.params.event_id
				});
			}, function errorCallback(error) {
				toaster.pop('error', 'Event Resource', error.data.message);
			});
		};
	}
]);

app.controller('districtEditEventResourceController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
	function($scope, $http, $state, toaster, $cookieStore) {
		$scope.changeInTotal = function() {
			var total = 0;
			if ($scope.resource.cost && $scope.resource.resource_value) {
				total = $scope.resource.cost * $scope.resource.resource_value;
			}
			if (isNaN(total)) {
				toaster.pop('error', 'Quality/Cost', 'both can only be numbers');
			} else {
				$scope.resource.total_cost = total;
			}
		};
		$scope.cancelResource = function() {
			$state.go('districts.event_resource_list', {
				event_id: $state.params.event_id
			});
		};
		$scope.getEventResource = function() {
			$http({
				method: 'GET',
				url: '/event/resource/edit?id=' + $state.params.resource_id
			}).then(function successCallback(response) {
				$scope.resource = response.data;
			}, function errorCallback(error) {
				toaster.pop('error', 'Event Resource', error.data.message);
			});
		};
		$scope.editSaveResource = function() {
			var data = $scope.resource;
			$http({
				method: 'POST',
				url: '/event/resource/edit',
				data: data
			}).then(function successCallback(response) {
				toaster.pop('success', 'Event Resource', "Updated successfully.");
				$state.go('districts.event_resource_list', {
					event_id: $state.params.event_id
				});
			}, function errorCallback(error) {
				toaster.pop('error', 'Event Resource', error.data.message);
			});
		};
	}
]);
