app.controller('createStaffAdmissionController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        $scope.addClass = function() {
            $state.go('schoolstaff.admission_create');
        };

        $scope.cancelAdmissionForm = function() {
            $state.go('schoolstaff.admission_list');
        };

        $scope.getSchoolGrades = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/grade/getAllSchoolGrades',
                data: data
            }).then(function successCallback(response) {
                $scope.grades = response.data;
                $scope.getSchoolSessions();
                $scope.getSchoolSources();
                $scope.getAllInfo();
            }, function errorCallback(error) {
                toaster.pop('error', 'Grades', 'Something went wrong!');
            });
        };

         $scope.getAllInfo = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/medical_info/getAllMedicalInfos',
                data: data
            }).then(function successCallback(response) {
                $scope.infos = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Medical Info', 'cannot get all the Staff! try reloading page.');
            });
        };
        $scope.getSchoolSessions = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/session/getAllSchoolSessions',
                data: data
            }).then(function successCallback(response) {
                $scope.sessions = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Session', 'Something went wrong!');
            });
        };


        $scope.getSchoolSources = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/sources/getAllSchoolSources',
                data: data
            }).then(function successCallback(response) {
                $scope.sources = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Source', 'Something went wrong!');
            });
        };

        $scope.uploadFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected !');
            Loading(true);
            Upload.upload({
                url: '/student/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.image = success.data;
                    $scope.admission.image = success.data
                    $scope.saveDisable = true;
                },
                function(error) {
                    Loading(false);
                    toaster.pop('error', "Error", error.data.message);
                    $scope.saveDisable = false;
                });
        };

        $scope.createAdmissionInquiry = function() {
            data = $scope.admission;
            data.school_id = $cookieStore.get('loggedInSchool').id;
            // data.staff_id = $cookieStore.get('loggedInStaff').id;
            data.image = $scope.image;
            data.admission_status = "false";
            $http({
                method: "POST",
                url: "/student/register",
                data: data
            }).then(function successCallback(response) {
                $scope.cancelAdmissionForm();
                toaster.pop('success', 'Admission Inquiry', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Admission Inquiry', error.data.message);
            });
        };
    }
]);

app.controller('listStaffAdmissionController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.createAdmissionInquiry = function() {
            $state.go('schoolstaff.admission_create');
        };

        $scope.studentProfile = function(student_id) {
            $state.go('schoolstaff.student_create', {
                student_id: student_id
            });
        };

        $scope.editProfile = function(student_id) {
            $state.go('schoolstaff.student_edit', {
                student_id: student_id
            });
        };

        $scope.viewFollowups = function(student_id) {
            $state.go('schoolstaff.view_followUp', {
                student_id: student_id
            });
        };

        $scope.inactiveClass = function(student_id) {
            var data = {
                id: student_id
            };
            $http({
                method: 'POST',
                url: '/class/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllSchoolStaff();
            }, function errorCallback(error) {
                toaster.pop('error', 'Admission Inquiry', error.data.message);
            });
        };

        $scope.activeClass = function(student_id) {
            var data = {
                id: student_id
            };
            $http({
                method: 'POST',
                url: '/class/active',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllSchoolStaff();
            }, function errorCallback(error) {
                toaster.pop('error', 'Admission Inquiry', error.data.message);
            });
        };

        $scope.deleteClass = function(student_id) {
            var data = {
                id: student_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/class/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllSchoolStaff();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Admission Inquiry', error.data.message);
                });
            }
        };

        $scope.deleteAdmission = function(admission_id) {
            var data = {
                id: admission_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/student/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllSchoolStaff();
                    toaster.pop('success', 'Admission', "Admission removed successfully!");
                }, function errorCallback(error) {
                    toaster.pop('error', 'Admission', error.data.message);
                });
            }
        };

        $scope.editClass = function(student_id) {
            $state.go('schoolstaff.admission_edit', {
                student_id: student_id
            });
        };

        $scope.getAllSchoolStaff = function() {
            var school_id = 0;

            if($cookieStore.get('loggedInSchool')){
                school_id = $cookieStore.get('loggedInSchool').id;
            } else if ($cookieStore.get('loggedInStaff')){
                school_id = $cookieStore.get('loggedInStaff').school_id;
            }

            var data = {
                school_id: school_id
            };
            $http({
                method: 'POST',
                url: '/student/getAllAdmissionInquiry',
                data: data
            }).then(function successCallback(response) {
                var admissions = response.data;
                $scope.admissions = admissions;
            }, function errorCallback(error) {
                toaster.pop('error', 'Admission Inquiry', 'cannot get all the Inquiries! try reloading page.');
            });
        };
    }
]);

app.controller('editStaffAdmissionController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        $scope.getSchoolGrades = function() {
            var school_id = 0;

            if($cookieStore.get('loggedInSchool')){
                school_id = $cookieStore.get('loggedInSchool').id;
            } else if ($cookieStore.get('loggedInStaff')){
                school_id = $cookieStore.get('loggedInStaff').school_id;
            }

            var data = {
                school_id: school_id
            };
            $http({
                method: 'POST',
                url: '/grade/getAllSchoolGrades',
                data: data
            }).then(function successCallback(response) {
                $scope.grades = response.data;
                $scope.getSchoolSessions();
                $scope.getAllInfo();
            }, function errorCallback(error) {
                toaster.pop('error', 'Grades', 'Something went wrong!');
            });
        };

        $scope.getSchoolSessions = function() {
            var school_id = 0;

            if($cookieStore.get('loggedInSchool')){
                school_id = $cookieStore.get('loggedInSchool').id;
            } else if ($cookieStore.get('loggedInStaff')){
                school_id = $cookieStore.get('loggedInStaff').school_id;
            }

            var data = {
                school_id: school_id
            };
            $http({
                method: 'POST',
                url: '/session/getAllSchoolSessions',
                data: data
            }).then(function successCallback(response) {
                $scope.sessions = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Session', 'Something went wrong!');
            });
        };

        $scope.getEditAdmission = function() {
            $http({
                method: 'GET',
                url: '/student/edit?id=' + $state.params.student_id
            }).then(function successCallback(response) {
                $scope.getSchoolGrades();
                $scope.admission = response.data;
                $scope.admission.medical_allergies = (response.data.medical_allergies != null && response.data.medical_allergies != "") ? response.data.medical_allergies.split(',') : [];
                $scope.admission.medical_immunizations = (response.data.medical_immunizations != null && response.data.medical_immunizations != "") ? response.data.medical_immunizations.split(',') : [];

                // var arr = response.data.sports;
                // if (arr) {
                //     var array1 = arr.split(',');
                //     var data = {};
                //     for (var i = 0; i < array1.length; i++) {
                //         data[array1[i]] = true;
                //     }
                //     $scope.sports = data;
                // }
            }, function errorCallback(error) {
                toaster.pop('error', 'Admission Inquiry', 'cannot get all the Inquiries! try reloading page.');
            });
        };

        $scope.getAllInfo = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/medical_info/getAllMedicalInfos',
                data: data
            }).then(function successCallback(response) {
                $scope.infos = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Medical Info', 'cannot get all the Staff! try reloading page.');
            });
        };

        $scope.saveEditAdmission = function() {
            data = $scope.admission;
            data.image = $scope.image;
            // if (!data.staff_id) {
            //     data.staff_id = $cookieStore.get("loggedInStaff").id;
            // }
            $http({
                method: 'POST',
                url: '/student/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Admission Inquiry', 'Saved Successfully.');
                var student_id = response.data.id;
                $state.go('schoolstaff.student_academics', {
                    student_id: student_id
                });
            }, function errorCallback(error) {
                toaster.pop('error', 'Admission Inquiry', error.data.message);
            });
        };

        $scope.saveEditAdmissionTab = function() {
            var data = $scope.admission;
            // if (!data.staff_id) {
            //     data.staff_id = $cookieStore.get("loggedInStaff").id;
            // }
            data.image = $scope.image;
            $http({
                method: 'POST',
                url: '/student/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Student Profile', 'Updated Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Student Profile', error.data.message);
            });
        };

        $scope.saveEditAcademics = function() {
            data = $scope.admission;
            // if (!data.staff_id) {
            //     data.staff_id = $cookieStore.get("loggedInStaff").id;
            // }
            $http({
                method: 'POST',
                url: '/student/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Admission Inquiry', 'Saved Successfully.');
                var student_id = response.data.id;
                $state.go('schoolstaff.student_skills', {
                    student_id: student_id
                });
            }, function errorCallback(error) {
                toaster.pop('error', 'Admission Inquiry', error.data.message);
            });
        };

        $scope.saveEditAcademicsTab = function() {
            data = $scope.admission;
            // if (!data.staff_id) {
            //     data.staff_id = $cookieStore.get("loggedInStaff").id;
            // }
            $http({
                method: 'POST',
                url: '/student/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Student Details', 'Updated Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Student Details', error.data.message);
            });
        };

        $scope.sports = [{
            id: 1,
            name: 'football'
        }, {
            id: 2,
            name: 'baseball'
        }, {
            id: 3,
            name: 'track'
        }, {
            id: 4,
            name: 'swimming'
        }, {
            id: 5,
            name: 'basketball'
        }, {
            id: 6,
            name: 'other'
        }];

        $scope.saveSkills = function() {
            data = $scope.admission;
            var sports = $scope.sports;
            $scope.sportsArray = [];
            angular.forEach(sports, function(selected_sports) {
                if (selected_sports.name){
                  $scope.sportsArray.push(selected_sports.name);  
                } 
            });
            data.sports = $scope.sportsArray.join(',');
            data.admission_status = "true";
            $http({
                method: 'POST',
                url: '/student/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Admission Inquiry', 'Saved Successfully.');
                var student_id = $scope.admission.id;
                $state.go('schoolstaff.student_parents_create', {
                    student_id: student_id
                });
            }, function errorCallback(error) {
                toaster.pop('error', 'Admission Inquiry', error.data.message);
            });
        };

        $scope.cancelAdmissionForm = function() {
            $state.go('schoolstaff.admission_list');
        };

        $scope.moveToAcademics = function(student_id) {
            $state.go('schoolstaff.student_academics', {
                student_id: student_id
            });
        };

        $scope.uploadFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected !');
            Loading(true);
            Upload.upload({
                url: '/student/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.image = success.data;
                    $scope.admission.image = success.data;
                    $scope.saveDisable = true;
                },
                function(error) {
                    Loading(false);
                    toaster.pop('error', "Error", error.data.message);
                    $scope.saveDisable = false;
                });
        };
    }
]);

app.controller('editStaffSchoolAdmissionController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        $scope.getSchoolGrades = function() {
            var school_id = 0;

            if($cookieStore.get('loggedInSchool')){
                school_id = $cookieStore.get('loggedInSchool').id;
            } else if ($cookieStore.get('loggedInStaff')){
                school_id = $cookieStore.get('loggedInStaff').school_id;
            }

            var data = {
                school_id: school_id
            };
            $http({
                method: 'POST',
                url: '/grade/getAllSchoolGrades',
                data: data
            }).then(function successCallback(response) {
                $scope.grades = response.data;
                $scope.getSchoolSessions();
            }, function errorCallback(error) {
                toaster.pop('error', 'Grades', 'Something went wrong!');
            });
        };

        $scope.getSchoolSessions = function() {
            var school_id = 0;

            if($cookieStore.get('loggedInSchool')){
                school_id = $cookieStore.get('loggedInSchool').id;
            } else if ($cookieStore.get('loggedInStaff')){
                school_id = $cookieStore.get('loggedInStaff').school_id;
            }

            var data = {
                school_id: school_id
            };
            $http({
                method: 'POST',
                url: '/session/getAllSchoolSessions',
                data: data
            }).then(function successCallback(response) {
                $scope.sessions = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Session', 'Something went wrong!');
            });
        };

        $scope.getEditAdmission = function() {
            $http({
                method: 'GET',
                url: '/student/edit?id=' + $state.params.student_id
            }).then(function successCallback(response) {
                $scope.getSchoolGrades();
                $scope.admission = response.data;
                $scope.admission.medical_allergies = (response.data.medical_allergies != null && response.data.medical_allergies != "") ? response.data.medical_allergies.split(',') : [];
                $scope.admission.medical_immunizations = (response.data.medical_immunizations != null && response.data.medical_immunizations != "") ? response.data.medical_immunizations.split(',') : [];
                var arr = response.data.sports;
                if (arr) {
                    var array1 = arr.split(',');
                    var data = {};
                    for (var i = 0; i < array1.length; i++) {
                        data[array1[i]] = true;
                    }
                    $scope.sports = data;
                }
                $scope.getAllInfo();
            }, function errorCallback(error) {
                toaster.pop('error', 'Admission Inquiry', 'cannot get all the Inquiries! try reloading page.');
            });
        };

        $scope.getAllInfo = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/medical_info/getAllMedicalInfos',
                data: data
            }).then(function successCallback(response) {
                $scope.infos = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Medical Info', 'cannot get all the Staff! try reloading page.');
            });
        };

        $scope.saveEditAdmission = function() {
            data = $scope.admission;
            data.image = $scope.image;
            // if (!data.staff_id) {
            //     data.staff_id = $cookieStore.get("loggedInStaff").id;
            // }
            $http({
                method: 'POST',
                url: '/student/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Admission Inquiry', 'Saved Successfully.');
                var student_id = response.data.id;
                $state.go('schools.student_academics', {
                    student_id: student_id
                });
            }, function errorCallback(error) {
                toaster.pop('error', 'Admission Inquiry', error.data.message);
            });
        };

        $scope.saveEditAdmissionTab = function() {
            var data = $scope.admission;
            // if (!data.staff_id) {
            //     data.staff_id = $cookieStore.get("loggedInStaff").id;
            // }
            data.image = $scope.image;
            $http({
                method: 'POST',
                url: '/student/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Student Profile', 'Updated Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Student Profile', error.data.message);
            });
        };

        $scope.saveEditAcademics = function() {
            data = $scope.admission;
            // if (!data.staff_id) {
            //     data.staff_id = $cookieStore.get("loggedInStaff").id;
            // }
            $http({
                method: 'POST',
                url: '/student/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Admission Inquiry', 'Saved Successfully.');
                var student_id = response.data.id;
                $state.go('schools.student_skills', {
                    student_id: student_id
                });
            }, function errorCallback(error) {
                toaster.pop('error', 'Admission Inquiry', error.data.message);
            });
        };

        $scope.saveEditAcademicsTab = function() {
            data = $scope.admission;
            // if (!data.staff_id) {
            //     data.staff_id = $cookieStore.get("loggedInStaff").id;
            // }
            $http({
                method: 'POST',
                url: '/student/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Student Details', 'Updated Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Student Details', error.data.message);
            });
        };

        $scope.sports = [{
            id: 1,
            name: 'football'
        }, {
            id: 2,
            name: 'baseball'
        }, {
            id: 3,
            name: 'track'
        }, {
            id: 4,
            name: 'swimming'
        }, {
            id: 5,
            name: 'basketball'
        }, {
            id: 6,
            name: 'other'
        }];

        $scope.saveSkills = function() {
            data = $scope.admission;
            var sports = $scope.sports;
            $scope.sportsArray = [];
            angular.forEach(sports, function(selected_sports) {
                if (selected_sports.name) {
                    $scope.sportsArray.push(selected_sports.name);   
                }
            });
            data.sports = $scope.sportsArray.toString();
            data.admission_status = "true";
            $http({
                method: 'POST',
                url: '/student/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Admission Inquiry', 'Saved Successfully.');
                var student_id = $scope.admission.id;
                $state.go('schools.student_parents_create', {
                    student_id: student_id
                });
            }, function errorCallback(error) {
                toaster.pop('error', 'Admission Inquiry', error.data.message);
            });
        };

        $scope.cancelAdmissionForm = function() {
            $state.go('schools.admission_list');
        };

        $scope.moveToAcademics = function(student_id) {
            $state.go('schools.student_academics', {
                student_id: student_id
            });
        };

        $scope.uploadFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected !');
            Loading(true);
            Upload.upload({
                url: '/student/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.image = success.data;
                    $scope.saveDisable = true;
                },
                function(error) {
                    Loading(false);
                    toaster.pop('error', "Error", error.data.message);
                    $scope.saveDisable = false;
                });
        };
    }
]);

app.controller('listStudentController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.getStudentsList = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/student/getAllAdmissionInquiry',
                data: data
            }).then(function successCallback(response) {
                var students = response.data;
                $scope.students = students;
            }, function errorCallback(error) {
                toaster.pop('error', 'Admission Inquiry', 'cannot get all the Inquiries! try reloading page.');
            });
        };

        $scope.viewDetails = function(student_id) {
            $state.go('schools.student_profile_view', {
                student_id: student_id
            });
        };
    }
]);

app.controller('listStudentStaffController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.getStudentsList = function() {
            var data = {
                school_id: $cookieStore.get('loggedInStaff').school_id
            };
            $http({
                method: 'POST',
                url: '/student/getAllAdmissionInquiry',
                data: data
            }).then(function successCallback(response) {
                var students = response.data;
                $scope.students = students;
            }, function errorCallback(error) {
                toaster.pop('error', 'Admission Inquiry', 'cannot get all the Inquiries! try reloading page.');
            });
        };

        $scope.viewDetails = function(student_id) {
            $state.go('schoolstaff.student_profile_view', {
                student_id: student_id
            });
        };
    }
]);

app.controller('createFollowUpSchoolController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.createFollowUp = function(student_id) {
            var data = $scope.student;
            data.student_id = $state.params.student_id;
            $http({
                method: 'POST',
                url: '/followUp/register',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Follow Up', 'Saved Successfully.');
                $scope.student_id = $state.params.student_id;
                $state.go('schools.view_followUp', {
                    student_id: $state.params.student_id
                });
            }, function errorCallback(error) {
                toaster.pop('error', 'Follow Up', 'cannot create Follow Up! try reloading page.');
            });
        };

        $scope.cancelFollowup = function() {
            var student_id = $state.params.student_id;
            $state.go('schools.view_followUp', {
                student_id: student_id
            });
        };
    }
]);

app.controller('createFollowUpController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.createFollowUp = function(student_id) {
            var data = $scope.student;
            data.student_id = $state.params.student_id;
            $http({
                method: 'POST',
                url: '/followUp/register',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Follow Up', 'Saved Successfully.');
                $scope.student_id = $state.params.student_id;
                $state.go('schoolstaff.view_followUp', {
                    student_id: $state.params.student_id
                });
            }, function errorCallback(error) {
                toaster.pop('error', 'Follow Up', 'cannot create Follow Up! try reloading page.');
            });
        };

        $scope.cancelFollowup = function() {
            var student_id = $state.params.student_id;
            $state.go('schoolstaff.view_followUp', {
                student_id: student_id
            });
        };
    }
]);

app.controller('viewFollowUpController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.getStudentsFollowUpList = function() {
            var data = {
                student_id: $state.params.student_id
            };
            $http({
                method: 'POST',
                url: '/followUp/list',
                data: data
            }).then(function successCallback(response) {
                var followups = response.data;
                $scope.followups = followups;
                $scope.student_id = $state.params.student_id;
            }, function errorCallback(error) {
                toaster.pop('error', 'Follow Up', 'cannot get all the Follow Up! try reloading page.');
            });
        };

        $scope.activeInquiry = function(followup_id) {
            var data = {
                id: followup_id
            };
            $http({
                method: 'POST',
                url: '/followUp/active',
                data: data
            }).then(function successCallback(response) {
                $scope.getStudentsFollowUpList();
            }, function errorCallback(error) {
                toaster.pop('error', 'Follow Up', error.data.message);
            });
        };

        $scope.inactiveInquiry = function(followup_id) {
            var data = {
                id: followup_id
            };
            $http({
                method: 'POST',
                url: '/followUp/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.getStudentsFollowUpList();
            }, function errorCallback(error) {
                toaster.pop('error', 'Follow Up', error.data.message);
            });
        };

        $scope.deleteInquiry = function(followup_id) {
            var data = {
                id: followup_id
            };
            var result = confirm("Do you really want to delete this Followup?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/followUp/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getStudentsFollowUpList();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Follow Up', error.data.message);
                });
            }
        };

        $scope.createFollowUpredirect = function(student_id) {
            $state.go('schoolstaff.create_followUp', {
                student_id: student_id
            });
        };
    }
]);
app.controller('viewFollowUpSchoolController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.getStudentsFollowUpList = function() {
            var data = {
                student_id: $state.params.student_id
            };
            $http({
                method: 'POST',
                url: '/followUp/list',
                data: data
            }).then(function successCallback(response) {
                var followups = response.data;
                $scope.followups = followups;
                $scope.student_id = $state.params.student_id;
            }, function errorCallback(error) {
                toaster.pop('error', 'Follow Up', 'cannot get all the Follow Up! try reloading page.');
            });
        };

        $scope.activeInquiry = function(followup_id) {
            var data = {
                id: followup_id
            };
            $http({
                method: 'POST',
                url: '/followUp/active',
                data: data
            }).then(function successCallback(response) {
                $scope.getStudentsFollowUpList();
            }, function errorCallback(error) {
                toaster.pop('error', 'Follow Up', error.data.message);
            });
        };

        $scope.inactiveInquiry = function(followup_id) {
            var data = {
                id: followup_id
            };
            $http({
                method: 'POST',
                url: '/followUp/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.getStudentsFollowUpList();
            }, function errorCallback(error) {
                toaster.pop('error', 'Follow Up', error.data.message);
            });
        };

        $scope.deleteInquiry = function(followup_id) {
            var data = {
                id: followup_id
            };
            var result = confirm("Do you really want to delete this Followup?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/followUp/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getStudentsFollowUpList();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Follow Up', error.data.message);
                });
            }
        };

        $scope.createFollowUpredirect = function(student_id) {
            $state.go('schools.create_followUp', {
                student_id: student_id
            });
        };
    }
]);


app.controller('viewStudentProfileController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        this.tab = 1;

        this.setTab = function(tabId) {
            this.tab = tabId;
        };

        this.isSet = function(tabId) {
            return this.tab === tabId;
        };
    }
]);

app.controller('studentsController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.getStudentProfile = function() {
            var data = {
                student_id: $state.params.student_id
            };
            $http({
                method: 'POST',
                url: '/student/getAllAdmissionInquiry',
                data: data
            }).then(function successCallback(response) {
                var students = response.data;
                $scope.students = students;
            }, function errorCallback(error) {
                toaster.pop('error', 'Student Profile', 'cannot get Student Profile! try reloading page.');
            });
        };
    }
]);

app.controller('studentViewProfile', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.getStudent = function() {
            var data = {
                student_id: $state.params.student_id
            };
            $http({
                method: 'POST',
                url: '/student/getStudent',
                data: data
            }).then(function successCallback(response) {
                var students = response.data;
                $scope.students = students;
            }, function errorCallback(error) {
                toaster.pop('error', 'Student Profile', 'cannot get Student Profile! try reloading page.');
            });
        };
    }
]);

app.controller('editStaffAdmissionInquiryController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        $scope.getSchoolGrades = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/grade/getAllSchoolGrades',
                data: data
            }).then(function successCallback(response) {
                $scope.grades = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Grades', 'Something went wrong!');
            });
        };

        $scope.getSchoolSessions = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/session/getAllSchoolSessions',
                data: data
            }).then(function successCallback(response) {
                $scope.sessions = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Session', 'Something went wrong!');
            });
        };

        $scope.getSchoolSources = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/sources/getAllSchoolSources',
                data: data
            }).then(function successCallback(response) {
                $scope.sources = response.data;
                $scope.admission.source = response.data.source_id;
            }, function errorCallback(error) {
                toaster.pop('error', 'Source', 'Something went wrong!');
            });
        };

        $scope.getEditAdmission = function() {
            $http({
                method: 'GET',
                url: '/student/edit?id=' + $state.params.student_id
            }).then(function successCallback(response) {
                $scope.getSchoolGrades();
                $scope.getSchoolSources(response.data.source_id);
                $scope.getSchoolSessions(response.data.session_id);
                $scope.admission = response.data;
                var arr = response.data.sports;
                if (arr) {
                    var array1 = arr.split(',');
                    var data = {};
                    for (var i = 0; i < array1.length; i++) {
                        data[array1[i]] = true;
                    }
                    $scope.sports = data;
                }
            }, function errorCallback(error) {
                toaster.pop('error', 'Admission Inquiry', 'cannot get all the Inquiries! try reloading page.');
            });
        };

        $scope.saveEditAdmission = function() {
            data = $scope.admission;
            data.image = $scope.image;
            // if (!data.staff_id) {
            //     data.staff_id = $cookieStore.get("loggedInStaff").id;
            // }
            $http({
                method: 'POST',
                url: '/student/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Admission Inquiry', 'Saved Successfully.');
                $state.go('schoolstaff.admission_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'Admission Inquiry', error.data.message);
            });
        };

        $scope.uploadFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected !');
            Loading(true);
            Upload.upload({
                url: '/student/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.image = success.data;
                    $scope.admission.image = $scope.image;
                    $scope.saveDisable = true;
                },
                function(error) {
                    Loading(false);
                    toaster.pop('error', "Error", error.data.message);
                    $scope.saveDisable = false;
                });
        };

        $scope.cancelAdmissionForm = function() {
            $state.go('schoolstaff.admission_list');
        };
    }

]);
