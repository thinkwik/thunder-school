var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var User = db.define('users', {
    first_name: String,
    last_name: String,
    email: String,
    password: String,
    mobile: String,
    profile_image:String,
    role_id: {
        type: 'integer'
    },
    school_id: {
        type: 'integer'
    },
    user_type: String,
    forgot_password_token:String,
    status: ['0', '1'],
    gender: String,
    birth_date: String,
    address: String,
    city: String,
    zipcode: Number,
    state: String,
    country: String,
    height: String,
    weight: String,
    blood_group: String,
    dieases_information:String,
    notes:String,
    medicalinfo:String,
    social_behavioral:String,
    physical_handicaps:String,
    medical_immunizations: {
        type: 'integer'
    },
    medical_allergies: {
        type: 'integer'
    },
    emergency_contacts:String,
}, {
    timestamp: true,
    methods: {

    }
});
