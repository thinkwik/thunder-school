var bCrypt = require('bcrypt-nodejs');
var moment = require('moment');
var db = require('orm').db,
    Exam = db.models.exams,
    ExamResult = db.models.exam_results,
    StudentQAResult = db.models.exam_qa_results;

exports.apiGetStaffExams = function(req, res) {
    if (typeof req.body.staff_id == 'undefined') {
        return apiSendError(req, res, 422, 'staff id required');
    }
    var query_str = `SELECT t1.* , t2.school_name, t3.session_name, t4.grade
    				, t5.subject_name, COALESCE(SUM(t6.max_mark),0) as total_marks,t7.class_name , CONCAT(t8.room_number,"-",t8.floor," ",t8.building_name," ",t8.street_name) as location
				    FROM exams as t1
				    LEFT JOIN schools as t2 ON t1.school_id = t2.id
				    LEFT JOIN sessions as t3 ON t1.session_id = t3.id
				    LEFT JOIN grades as t4 ON t1.grade_id = t4.id
				    LEFT JOIN subjects as t5 ON t1.subject_id = t5.id
				    LEFT JOIN exam_qa as t6 ON t1.id = t6.exam_id
                    LEFT JOIN classes as t7 ON t1.class_id = t7.id
                    LEFT JOIN locations as t8 ON t7.location_id = t8.id
            WHERE t1.staff_id = ? GROUP BY t1.id ORDER BY t1.start_date DESC`;
    db.driver.execQuery(query_str, [req.body.staff_id], function(err, rows) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 400, "Bad Request check your parameters");
        }
        var response = {};
        response['data'] = rows;
        response['code'] = 200;
        return res.send(response);
    });
};

exports.apiGetStudentsExam = function(req, res) {
    if (typeof req.body.student_id == 'undefined') {
        return apiSendError(req, res, 422, 'Student ID required');
    }
    var query_str = `SELECT t1.* , COALESCE(SUM(t6.max_mark),0) as total_marks ,t2.exam_head,t2.start_date,t2.end_date,t2.start_time,t2.end_time, t3.session_name, t4.grade, t5.subject_name, t7.class_name , CONCAT(t8.room_number,"-",t8.floor," ",t8.building_name," ",t8.street_name) as location
    	FROM exam_results as t1
        LEFT JOIN exams as t2 ON t1.exam_id = t2.id
        LEFT JOIN sessions as t3 ON t2.session_id = t3.id
        LEFT JOIN grades as t4 ON t2.grade_id = t4.id
        LEFT JOIN subjects as t5 ON t2.subject_id = t5.id
        LEFT JOIN exam_qa as t6 ON t1.exam_id = t6.exam_id
        LEFT JOIN classes as t7 ON t2.class_id = t7.id
        LEFT JOIN locations as t8 ON t7.location_id = t8.id
        WHERE t1.student_id = ? GROUP BY t1.id
        ORDER BY t1.created_at DESC,t1.status ASC`;
        // var query_str = `SELECT t1.* , COALESCE(SUM(t6.max_mark),0) as total_marks ,t2.exam_head,t2.start_date,t2.end_date,t2.start_time,t2.end_time, t3.session_name, t4.grade, t5.subject_name FROM exam_results as t1 LEFT JOIN exams as t2 ON t1.exam_id = t2.id LEFT JOIN sessions as t3 ON t2.session_id = t3.id LEFT JOIN grades as t4 ON t2.grade_id = t4.id LEFT JOIN subjects as t5 ON t2.subject_id = t5.id LEFT JOIN exam_qa as t6 ON t1.exam_id = t6.exam_id WHERE t1.student_id = ? GROUP BY t1.id HAVING total_marks <> 0 ORDER BY t1.created_at DESC`;
    db.driver.execQuery(query_str, [req.body.student_id], function(err, rows) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 400, "Bad Request check your parameters");
        }
        var response = {};
        response['data'] = rows;
        response['code'] = 200;
        return res.send(response);
    });
};

exports.apiGetAllExamResults = function(req, res) {
    if (typeof req.body.school_id == 'undefined') {
        return apiSendError(req, res, 422, 'school id required');
    }
    var query_str = `SELECT t1.*, t2.subject_id,CONCAT(t7.first_name ," ",t7.last_name) as student_name ,t3.school_name, t4.session_name, t5.grade, t6.subject_name , t8.class_name
                    FROM exam_results as t1
                    LEFT JOIN exams as t2 ON t1.exam_id = t2.id
                    LEFT JOIN schools as t3 ON t2.school_id = t3.id
                    LEFT JOIN sessions as t4 ON t2.session_id = t4.id
                    LEFT JOIN grades as t5 ON t2.grade_id = t5.id
                    LEFT JOIN subjects as t6 ON t2.subject_id = t6.id
                    LEFT JOIN admissions as t7 ON t1.student_id = t7.id
                    LEFT JOIN classes as t8 ON t2.class_id = t8.id
                    WHERE t2.school_id = ? GROUP BY t1.id ORDER BY t1.id DESC`;
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 400, "Bad Request check your parameters");
        }
        var response = {};
        response['data'] = rows;
        response['code'] = 200;
        return res.send(response);
    });
};

exports.apiGetAllExamResultsUptoClass = function(req, res) {
    if (typeof req.body.school_id == 'undefined') {
        return apiSendError(req, res, 422, 'school id required');
    }
    var query_str = `SELECT t3.school_name, t4.session_name, t5.grade, t6.subject_name , t8.class_name, t8.id as class_id
                    FROM exam_results as t1
                    LEFT JOIN exams as t2 ON t1.exam_id = t2.id
                    LEFT JOIN schools as t3 ON t2.school_id = t3.id
                    LEFT JOIN sessions as t4 ON t2.session_id = t4.id
                    LEFT JOIN grades as t5 ON t2.grade_id = t5.id
                    LEFT JOIN subjects as t6 ON t2.subject_id = t6.id
                    LEFT JOIN classes as t8 ON t2.class_id = t8.id
                    WHERE t2.school_id = ? GROUP BY t2.id ORDER BY t1.created_at DESC, t1.status ASC`;
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 400, "Bad Request check your parameters");
        }
        var response = {};
        response['data'] = rows;
        response['code'] = 200;
        return res.send(response);
    });
};

exports.apiGetAllExamResultsUptoResult = function(req, res) {
    if (typeof req.body.school_id == 'undefined' && typeof req.body.class_id == 'undefined') {
        return apiSendError(req, res, 422, 'school id required');
    }
    var query_str = `SELECT t1.*, t2.subject_id,CONCAT(t7.first_name ," ",t7.last_name) as student_name, t7.image as photo ,t3.school_name, t4.session_name, t5.grade, t6.subject_name , t8.class_name, t8.id as class_id
                    FROM exam_results as t1
                    LEFT JOIN exams as t2 ON t1.exam_id = t2.id
                    LEFT JOIN schools as t3 ON t2.school_id = t3.id
                    LEFT JOIN sessions as t4 ON t2.session_id = t4.id
                    LEFT JOIN grades as t5 ON t2.grade_id = t5.id
                    LEFT JOIN subjects as t6 ON t2.subject_id = t6.id
                    LEFT JOIN admissions as t7 ON t1.student_id = t7.id
                    LEFT JOIN classes as t8 ON t2.class_id = t8.id
                      WHERE t2.school_id = ? AND t8.id = ? GROUP BY t1.id ORDER BY t1.created_at DESC`;
    db.driver.execQuery(query_str, [req.body.school_id, req.body.class_id], function(err, rows) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 400, "Bad Request check your parameters");
        }
        var response = {};
        response['data'] = rows;
        response['code'] = 200;
        return res.send(response);
    });
};

exports.apiGetStudentExamResults = function(req, res) {
    if (typeof req.body.student_id == 'undefined') {
        return apiSendError(req, res, 422, 'school id required');
    }
    var query_str = `SELECT t1.*  , t3.school_name, t4.session_name, t5.grade, t6.subject_name, CONCAT(t7.first_name ," ",t7.last_name) as student_name , t8.class_name
                    FROM exam_results as t1
                    LEFT JOIN exams as t2 ON t1.exam_id = t2.id
                    LEFT JOIN schools as t3 ON t2.school_id = t3.id
                    LEFT JOIN sessions as t4 ON t2.session_id = t4.id
                    LEFT JOIN grades as t5 ON t2.grade_id = t5.id
                    LEFT JOIN subjects as t6 ON t2.subject_id = t6.id
                    LEFT JOIN admissions as t7 ON t1.student_id = t7.id
                    LEFT JOIN classes as t8 ON t2.class_id = t8.id
                    WHERE t1.student_id = ? GROUP BY t1.id ORDER BY t1.created_at DESC`;
    db.driver.execQuery(query_str, [req.body.student_id], function(err, rows) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 400, "Bad Request check your parameters");
        }
        var response = {};
        response['data'] = rows;
        response['code'] = 200;
        return res.send(response);
    });
};
