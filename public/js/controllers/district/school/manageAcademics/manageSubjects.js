app.controller('createSchoolSubjectController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.addSubject = function() {
            $state.go('schools.subject_create');
        };

        $scope.cancelSubjectForm = function() {
            $state.go('schools.subject_list');
        };

        $scope.getSchoolDepartments = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/school/department/getAllSchoolDepartment',
                data: data
            }).then(function successCallback(response) {
                $scope.departments = response.data;
                $scope.getSchoolGrades();
                // $scope.getSchoolSessions();
            }, function errorCallback(error) {
                toaster.pop('error', 'School', 'Something went wrong!');
            });
        };

        // School Session needs to be changed/ask

        // $scope.getSchoolSessions = function() {
        //     var data = {
        //     school_id: $cookieStore.get('loggedInSchool').id
        // }
        //     $http({
        //         method: 'POST',
        //         url: '/session/getAllSchoolSessions',
        //         data: data
        //     }).then(function successCallback(response) {
        //         $scope.Sessions = response.data;
        //     }, function errorCallback(error) {
        //         toaster.pop('error', 'Schools', 'Something went wrong!');
        //     });
        // };

        $scope.getSchoolGrades = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/grade/getAllSchoolGrades',
                data: data
            }).then(function successCallback(response) {
                $scope.grades = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'School', 'Something went wrong!');
            });
        };

        $scope.createSubject = function() {
            data = $scope.subject;
            data.school_id = $cookieStore.get('loggedInSchool').id;
            $http({
                method: "POST",
                url: "/subject/register",
                data: data
            }).then(function successCallback(response) {
                $scope.cancelSubjectForm();
                toaster.pop('success', 'Subject', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Subject', error.data.message);
            });
        };
    }
]);

app.controller('listSchoolSubjectController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.addNewSubject = function() {
            $state.go('schools.subject_create');
        };

        $scope.inactiveSubject = function(subject_id) {
            var data = {
                id: subject_id
            };
            $http({
                method: 'POST',
                url: '/subject/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllSchoolSubject();
            }, function errorCallback(error) {
                toaster.pop('error', 'Subject', error.data.message);
            });
        };

        $scope.activeSubject = function(subject_id) {
            var data = {
                id: subject_id
            };
            $http({
                method: 'POST',
                url: '/subject/active',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllSchoolSubject();
            }, function errorCallback(error) {
                toaster.pop('error', 'Subject', error.data.message);
            });
        };

        $scope.deleteSubject = function(subject_id) {
            var data = {
                id: subject_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/subject/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllSchoolSubject();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Subject', error.data.message);
                });
            }
        };

        $scope.getAllSchoolSubject = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/subject/getAllSchoolSubjects',
                data: data
            }).then(function successCallback(response) {
                var subjects = response.data;
                $scope.subjects = subjects;
            }, function errorCallback(error) {
                toaster.pop('error', 'Subject', 'cannot get all the Subjects! try reloading page.');
            });
        };

        $scope.editSubject = function(subject_id) {
            $state.go('schools.subject_edit', {
                subject_id: subject_id
            });
        };
    }
]);

app.controller('editSchoolSubjectController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.getSchoolDepartments = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/school/department/getAllSchoolDepartment',
                data: data
            }).then(function successCallback(response) {
                $scope.departments = response.data;
                $scope.getSchoolGrades();
                // $scope.getSchoolSessions();
            }, function errorCallback(error) {
                toaster.pop('error', 'School', 'Something went wrong!');
            });
        };

        // School Session needs to be changed/ask

        // $scope.getSchoolSessions = function() {
        //     var data = {
        //         school_id: $scope.staff.school_id
        //     };
        //     $http({
        //         method: 'POST',
        //         url: '/session/getAllSchoolSessions',
        //         data: data
        //     }).then(function successCallback(response) {
        //         $scope.Sessions = response.data;
        //     }, function errorCallback(error) {
        //         toaster.pop('error', 'School', 'Something went wrong!');
        //     });
        // };

        $scope.getSchoolGrades = function() {
            var data = {
                school_id: $scope.subject.school_id
            };
            $http({
                method: 'POST',
                url: '/grade/getAllSchoolGrades',
                data: data
            }).then(function successCallback(response) {
                $scope.grades = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'School', 'Something went wrong!');
            });
        };

        $scope.getEditSubject = function() {
            $http({
                method: 'GET',
                url: '/subject/edit?id=' + $state.params.subject_id
            }).then(function successCallback(response) {
                $scope.subject = response.data;
                $scope.getSchoolDepartments();
            }, function errorCallback(error) {
                toaster.pop('error', 'Subject', 'cannot get all the Subjects! try reloading page.');
            });
        };

        $scope.saveEditSubject = function() {
            data = $scope.subject;
            $http({
                method: 'POST',
                url: '/subject/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Subject', 'Saved Successfully.');
                $state.go('schools.subject_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'Subject', error.data.message);
            });
        };

        $scope.cancelSubjectForm = function() {
            $state.go('schools.subject_list');
        };
    }
]);
