var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var ParentPages = db.define('parentPages', {
    title: String,
    status: ['0', '1']
}, {
    timestamp: true,
    methods: {

    }
});
