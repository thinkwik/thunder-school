var LocalStrategy = require('passport-local').Strategy;
var bCrypt = require('bcrypt-nodejs');
var db = require('orm').db,
    User = db.models.users;

module.exports = function(app, passport) {

    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
        User.find({
            id: id
        }, function(err, user) {
            done(err, user[0]);
        });
    });

    passport.use('login', new LocalStrategy({
            passReqToCallback: true
        },
        function(req, username, password, done) {
            User.find({
                    email: username
                }, 1,
                function(err, users) {

                    if (err) {
                        return done(err);
                    }
                    // Username does not exist, log error & redirect back
                    if (users.length <= 0) {
                        return done(null, false, req.flash('message', 'User Not Found with username : ' + username));
                    }
                    user = users[0];
                    if (!isValidPassword(user, password)) {
                        return done(null, false, req.flash('message', 'Invalid Password'));
                    }
                    // User and password both match, return user from
                    // done method which will be treated like success
                    return done(null, user, req.flash('message', user));
                }
            );
        }));

    passport.use('signup', new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true
        },
        function(req, username, password, done) {
            User.find({
                email: username
            }, function(err, users) {
                user = users[0];
                if (err) {
                    return done(err, req.flash('message', err));
                }
                // already exists
                if (user) {
                    return done(null, false, req.flash('message', 'User already exists with same email'));
                } else {
                    var newUser = new User();
                    // set the user's local credentials
                    newUser.password = createHash(password);
                    newUser.email = req.body.email;
                    newUser.first_name = req.body.first_name;
                    newUser.last_name = req.body.last_name;
                    newUser.mobile = req.body.mobile;
                    newUser.role_id = req.body.role_id;
                    newUser.school_id = (typeof req.body.school_id != 'undefined') ? req.body.school_id : "0";
                    newUser.status = req.body.status;
                    newUser.user_type = req.body.user_type;
                    // save the user
                    newUser.save(function(err) {
                        if (err) {
                            return done(null, false, req.flash('message', 'Error in Saving user please check your requested paramenter'));
                        }
                        return done(null, newUser, req.flash('message', newUser));
                    });
                }
            });
        }));
};


var isValidPassword = function(user, password) {
    return bCrypt.compareSync(password, user.password);
}

var createHash = function(password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
}