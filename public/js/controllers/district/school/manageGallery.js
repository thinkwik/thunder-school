app.controller('createSchoolGalleryController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore',
    function($scope, $http, $state, toaster, Upload, $cookieStore) {
        $scope.createGallery = function() {
            data = $scope.gallery;
            data.school_id = $cookieStore.get("loggedInSchool").id;
            $http({
                    url: '/gallery/register',
                    method: "POST",
                    data: data
                })
                .then(function(response) {
                        $state.go('schools.gallery_list');
                        toaster.pop('success', "Gallery", "Gallery created successfully!!!");
                    },
                    function(response) {
                        toaster.pop('error', "Error", "Something went wrong!!!");
                    });
        };

        $scope.getAllEventList = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/event/getAllSchoolEvents',
                data: data
            }).then(function successCallback(response) {
                $scope.events = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Events', error.message);
            });
        };

        $scope.cancelGallery = function() {
            $state.go('schools.gallery_list');
        };
    }
]);

app.controller('listSchoolGalleryController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore',
    function($scope, $http, $state, toaster, Upload, $cookieStore) {
        $scope.listGallery = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/gallery/getAllSchoolEventsGalleries',
                data: data
            }).then(function successCallback(response) {
                var galleries = response.data;
                $scope.galleries = galleries;
            }, function errorCallback(error) {
                toaster.pop('error', 'Galleries', 'cannot get all the gallery! try reloading page.');
                console.log(error.data.message);
            });
        };

        $scope.activeGallery = function(gallery_id) {
            var data = {
                id: gallery_id
            };
            $http({
                method: 'POST',
                url: '/gallery/active',
                data: data
            }).then(function successCallback(response) {
                $scope.listGallery();
            }, function errorCallback(error) {
                console.log(error.data.message);
            });
        };

        $scope.deleteGallery = function(gallery_id) {
            var data = {
                id: gallery_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/gallery/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.listGallery();
                }, function errorCallback(error) {
                    console.log(error.data.message);
                });
            }
        };

        $scope.inactiveGallery = function(gallery_id) {
            var data = {
                id: gallery_id
            };
            $http({
                method: 'POST',
                url: '/gallery/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.listGallery();
            }, function errorCallback(error) {
                console.log(error.data.message);
            });
        };

        $scope.editGallery = function(gallery_id) {
            $state.go('schools.gallery_edit', {
                gallery_id: gallery_id
            });
        };


        $scope.getEditGallery = function() {
            var data = $scope.gallery;
            $http({
                method: 'GET',
                url: '/gallery/edit?id=' + $state.params.gallery_id,
                data: data
            }).then(function successCallback(response) {
                $scope.gallery = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Gallery', error.data.message);
            });
        };

        $scope.addNewGallery = function() {
            $state.go('schools.gallery_create');
        };

        $scope.addItem = function(gallery_id) {
            $state.go('schools.image_add', {
                gallery_id: gallery_id
            });
        };

        $scope.viewAlbum = function(gallery_id) {
            $state.go('schools.gallery_view', {
                gallery_id: gallery_id
            });
        };
    }
]);

app.controller('editSchoolGalleryController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore',
    function($scope, $http, $state, toaster, Upload, $cookieStore) {
        $scope.cancelGallery = function() {
            $state.go('schools.gallery_list');
        };

        $scope.saveEditGallery = function() {
            var data = $scope.gallery;
            $http({
                method: 'POST',
                url: '/gallery/edit',
                data: data
            }).then(function successCallback(response) {
                $state.go('schools.gallery_list');
                toaster.pop('success', 'Gallery', 'Updated Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Gallery', error.data.message);
            });
        };

        $scope.getAllEventList = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/event/getAllSchoolEvents',
                data: data
            }).then(function successCallback(response) {
                $scope.events = response.data;
                }, function errorCallback(error) {
                toaster.pop('error', 'Events', error.message);
            });
        };

        $scope.getEditGallery = function() {
            $http({
                method: 'GET',
                url: '/gallery/edit?id=' + $state.params.gallery_id
            }).then(function successCallback(response) {
                $scope.gallery = response.data;
                $scope.getAllEventList();
            }, function errorCallback(error) {
                toaster.pop('error', 'Gallery', error.data.message);
            });
        };
    }
]);

app.controller('viewSchoolGalleryAlbumController', ['$scope', '$http', '$state', 'toaster', 'Upload',
    function($scope, $http, $state, toaster, Upload) {
        $scope.uploadFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected !');
            Loading(true);
            Upload.upload({
                url: '/gallery/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.filename = success.data;
                    $scope.saveDisable = true;
                },
                function(error) {
                    toaster.pop('error', "Error", error.data.message);
                    $scope.saveDisable = false;
                });
        };

        $scope.saveImage = function() {
            if ($scope.filename === null || $scope.filename === "") {
                return toaster.pop('info', "Gallery", "Please select image!!!");
            }
            var data = {
                gallery_id: parseInt($state.params.gallery_id),
                image_name: $scope.filename,
                image_description: $scope.image_description
            };
            $http({
                url: '/gallery/addImage',
                method: "POST",
                data: data
            }).then(function successCallback(response) {
                $scope.viewAlbum();
                $scope.image_name = null;
                $scope.filename = null;
                toaster.pop('success', "Gallery", "Gallery updated successfully!!!");
            }, function errorCallback(error) {
                toaster.pop('error', 'Gallery', 'Something went wrong');
            });
        };

        $scope.galleryList = function() {
            $state.go('schools.gallery_list');
        };

        $scope.viewAlbum = function() {
            var data = {
                gallery_id: $state.params.gallery_id
            };
            $http({
                method: 'POST',
                url: '/gallery/viewAlbum',
                data: data
            }).then(function successCallback(response) {
                var galleryImages = response.data;
                $scope.galleryImages = galleryImages;
            }, function errorCallback(error) {
                toaster.pop('error', 'Gallery', error.data.message);
            });
        };

        $scope.deleteImage = function(image_id) {
            var data = {
                id: image_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/gallery/Album/deleteImage',
                    data: data
                }).then(function successCallback(response) {
                    $scope.viewAlbum();
                    toaster.pop('success', "Gallery", "Gallery updated successfully!!!");
                }, function errorCallback(error) {
                    toaster.pop('error', 'Gallery', error.data.message);
                });
            }
        };
    }
]);
