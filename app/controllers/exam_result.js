var bCrypt = require('bcrypt-nodejs');
var db = require('orm').db,
    ExamResult = db.models.exam_results;

exports.register = function(req, res) {
    if (typeof req.body.exam_id == 'undefined') {
        return sendError(req, res, 422, 'required parameter not fullfiled for register Exam');
    }
    var new_result = new ExamResult();
    new_result.exam_id = req.body.exam_id;
    new_result.student_id = req.body.student_id;
    new_result.result_date = req.body.result_date;
    new_result.achived_marks = req.body.achived_marks;
    new_result.total_marks = req.body.total_marks;
    new_result.status = req.body.status;
    new_result.save(function(err, saved_result) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        res.send(saved_result);
    });
};

exports.getExamResult = function(req, res) {
    if (typeof req.query.id == 'undefined') return res.send("id required for getting Exam");
    ExamResult.find({
        id: req.query.id
    }, 1, function(err, results) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!results.length) {
            return sendError(req, res, 404, "exam not found with perticular id");
        }
        return res.send(results[0]);
    });
};
exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'exam id required');
    }
    ExamResult.find({
        id: req.body.id
    }, 1, function(err, results) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!results.length) {
            return sendError(req, res, 404, "Exam not found");
        }
        results[0].exam_id = req.body.exam_id;
        results[0].student_id = req.body.student_id;
        results[0].achived_marks = req.body.achived_marks;
        results[0].result_date = req.body.result_date;
        results[0].save(function(err, updated_result) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            return res.send(updated_result);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'exam id required');
    }
    ExamResult.find({
        id: req.body.id
    }, 1, function(err, results) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!results.length) {
            return sendError(req, res, 404, "Exam not found");
        }
        results[0].remove(function(err) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "Something went wrong in deleting Exam");
            }
            return res.send(results[0]);
        });
    });
};

exports.getAllExamResults = function(req, res) {
    if (typeof req.body.school_id == 'undefined') {
        return sendError(req, res, 422, 'school id required');
    }
    var query_str = `SELECT t1.*, t2.subject_id,CONCAT(t7.first_name ," ",t7.last_name) as student_name ,t3.school_name, t4.session_name, t5.grade, t6.subject_name , t8.class_name
                    FROM exam_results as t1
                    LEFT JOIN exams as t2 ON t1.exam_id = t2.id
                    LEFT JOIN schools as t3 ON t2.school_id = t3.id
                    LEFT JOIN sessions as t4 ON t2.session_id = t4.id
                    LEFT JOIN grades as t5 ON t2.grade_id = t5.id
                    LEFT JOIN subjects as t6 ON t2.subject_id = t6.id
                    LEFT JOIN admissions as t7 ON t1.student_id = t7.id
                    LEFT JOIN classes as t8 ON t2.class_id = t8.id
                    WHERE t2.school_id = ? GROUP BY t1.id ORDER BY t1.id DESC`;
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getStudentExamResults = function(req, res) {
    if (typeof req.body.student_id == 'undefined') {
        return sendError(req, res, 422, 'school id required');
    }
    var query_str = `SELECT t1.*  , t3.school_name, t4.session_name, t5.grade, t6.subject_name, CONCAT(t7.first_name ," ",t7.last_name) as student_name , t8.class_name
                    FROM exam_results as t1
                    LEFT JOIN exams as t2 ON t1.exam_id = t2.id
                    LEFT JOIN schools as t3 ON t2.school_id = t3.id
                    LEFT JOIN sessions as t4 ON t2.session_id = t4.id
                    LEFT JOIN grades as t5 ON t2.grade_id = t5.id
                    LEFT JOIN subjects as t6 ON t2.subject_id = t6.id
                    LEFT JOIN admissions as t7 ON t1.student_id = t7.id
                    LEFT JOIN classes as t8 ON t2.class_id = t8.id
                    WHERE t1.student_id = ? GROUP BY t1.id ORDER BY t1.id DESC`;
    db.driver.execQuery(query_str, [req.body.student_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
