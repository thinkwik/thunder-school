var bCrypt = require('bcrypt-nodejs');
var db = require('orm').db,
    SchoolFee = db.models.school_fees;

exports.register = function(req, res) {
    if (typeof req.body.school_id == 'undefined') {
        return sendError(req, res, 422, 'required parameter not fullfiled for register new resource');
    }
    var new_school_fee = new SchoolFee();
    new_school_fee.title = req.body.title;
    new_school_fee.school_id = req.body.school_id;
    new_school_fee.grade_id = req.body.grade_id;
    new_school_fee.amount = req.body.amount;
    new_school_fee.status = req.body.status;
    new_school_fee.save(function(err, saved_fee) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        res.send(saved_fee);
    });
};

exports.getFee = function(req, res) {
    if (typeof req.query.id == 'undefined') {
        return sendError(req, res, 422, 'fee id required');
    }
    SchoolFee.find({
        id: req.query.id
    }, 1, function(err, fees) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!fees.length) {
            return sendError(req, res, 404, "role not found");
        }
        return res.send(fees[0]);
    });
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'fee id required');
    }
    SchoolFee.find({
        id: req.body.id
    }, 1, function(err, fees) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!fees.length) {
            return sendError(req, res, 404, "role not found");
        }

        fees[0].title = req.body.title;
        fees[0].school_id = (typeof req.body.school_id != 'undefined') ? req.body.school_id : fees[0].school_id;
        fees[0].grade_id = (typeof req.body.grade_id != 'undefined') ? req.body.grade_id : fees[0].grade_id;
        fees[0].amount = req.body.amount;
        fees[0].status = (typeof req.body.status != 'undefined') ? req.body.status : fees[0].status;
        fees[0].save(function(err, updated_fee) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            return res.send(updated_fee);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'fee id required');
    }
    SchoolFee.find({
        id: req.body.id
    }, 1, function(err, fees) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (fees.length == 0) {
            return sendError(req, res, 404, "fee data not found");
        }
        fees[0].remove(function(err) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "Something went wrong in deleting fee");
            }
            return res.send(fees[0]);
        });
    });
};

exports.getAllDistrictFees = function(req, res) {
    if (typeof req.body.district_id == 'undefined' || req.body.district_id == null) {
        return sendError(req, res, 422, "district id required");
    }
    var query_str = "SELECT t1.*, t2.school_name, t3.grade as grade_name FROM school_fees as t1 LEFT JOIN schools as t2 ON t1.school_id = t2.id LEFT JOIN grades as t3 ON t1.grade_id = t3.id WHERE t2.parent_id = ?";
    db.driver.execQuery(query_str, [req.body.district_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getAllSchoolFees = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id == null) {
        return sendError(req, res, 422, "district id required");
    }
    var query_str = "SELECT t1.*, t2.school_name, t3.grade as grade_name FROM school_fees as t1 LEFT JOIN schools as t2 ON t1.school_id = t2.id LEFT JOIN grades as t3 ON t1.grade_id = t3.id WHERE t2.id = ?";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
