app.controller('listParentsNoticesController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.getAllNotices = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/notice/getAllNotices',
                data: data
            }).then(function successCallback(response) {
                var notices = response.data;
                $scope.notices = notices;
            }, function errorCallback(error) {
                toaster.pop('error', 'Notice', 'cannot get all the notices! try reloading page.');
            });
        };
    }
]);
