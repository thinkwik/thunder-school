app.controller('schoolAllEventsListController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.addNewEventRedirect = function() {
            $state.go('schools.event_create');
        };
        $scope.eventResources = function(event_id) {
            $state.go('schools.event_resource_list', {
                event_id: event_id
            });
        };
        $scope.getAllEventList = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/event/getAllSchoolEvents',
                data: data
            }).then(function successCallback(response) {
                $scope.events = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Events', 'Something went wrong! getting all events');
            });
        };
        $scope.activeEvent = function(event_id) {
            var data = {
                id: event_id
            };
            $http({
                method: 'POST',
                url: '/event/active',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllEventList();
            }, function errorCallback(error) {
                toaster.pop('error', 'Events', error.data.message);
            });
        };
        $scope.inactiveEvent = function(event_id) {
            var data = {
                id: event_id
            };
            $http({
                method: 'POST',
                url: '/event/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllEventList();
            }, function errorCallback(error) {
                toaster.pop('error', 'Events', error.data.message);
            });
        };
        $scope.deleteEvent = function(event_id) {
            var data = {
                id: event_id
            };
            $http({
                method: 'POST',
                url: '/event/delete',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllEventList();
            }, function errorCallback(error) {
                toaster.pop('error', 'Events', error.data.message);
            });
        };
        $scope.editEvent = function(event_id) {
            $state.go('schools.event_edit', {
                event_id: event_id
            });
        };
    }
]);

app.controller('schoolEditEventController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.getEvent = function() {
            $http({
                method: 'GET',
                url: '/event/edit?id=' + $state.params.event_id
            }).then(function successCallback(response) {
                $scope.event = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Events', 'Something went wrong! getting event');
            });
        };
        $scope.cancelEvent = function() {
            $state.go('schools.events_list');
        };
        $scope.saveEditEvent = function() {
            var data = $scope.event;
            console.log(data);
            $http({
                method: 'POST',
                url: '/event/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Events', "saved successfully.");
            }, function errorCallback(error) {
                toaster.pop('error', 'Events', error.data.message);
            });
        };
    }
]);

app.controller('schoolNewEventController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.schools = [];
        $scope.createEvent = function() {
            var data = $scope.event;
            data.school_id = $cookieStore.get('loggedInSchool').id;
            $http({
                method: 'POST',
                url: '/event/register',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'School Event', "added successfully.");
                $state.go('schools.events_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'School Event', error.message);
            });
        };
        $scope.cancelEvent = function() {
            $state.go('schools.events_list');
        };
    }
]);
