// admin user list
app.controller('districtSchoolSuperAdminListController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.users = [];
        $scope.offset = 0;
        $scope.schoolsuperadminusers = function() {
            var data = {
                district_id: $cookieStore.get('loggedInDistrict').id
            };
            $http({
                method: 'POST',
                url: '/user/getAllSchoolAdminsByDistrict',
                data: data
            }).then(function successCallback(response) {
                var users = response.data;
                $scope.users = users;
            }, function errorCallback(error) {
                if (error.data.error_code == 444) {
                    $state.go('signin');
                }
                console.log(error.data.message);
            });
        };

        $scope.inactiveUser = function(user_id) {
            var data = {
                id: user_id
            };
            $http({
                method: 'POST',
                url: '/user/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.schoolsuperadminusers();
            }, function errorCallback(error) {
                console.log(error.data.message);
            });
        };

        $scope.activeUser = function(user_id) {
            var data = {
                id: user_id
            };
            $http({
                method: 'POST',
                url: '/user/active',
                data: data
            }).then(function successCallback(response) {
                $scope.schoolsuperadminusers();
            }, function errorCallback(error) {
                console.log(error.data.message);
            });
        };

        $scope.deleteUser = function(user_id) {
            var data = {
                id: user_id
            };
            $http({
                method: 'POST',
                url: '/user/delete',
                data: data
            }).then(function successCallback(response) {
                $scope.schoolsuperadminusers();
            }, function errorCallback(error) {
                console.log(error.data.message);
            });
        };

        $scope.editUser = function(user_id) {
            $state.go('districts.schoolAdmin_edit', {
                user_id: user_id
            });
        };
        $scope.addSchoolSuparAdminRedirect = function() {
            $state.go('districts.schoolAdmin_create');
        };
    }
]);

app.controller('districtSchoolSuperAdminCreateController', ['$scope', '$http', '$state', 'toaster', '$cookieStore','Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        $scope.user = {};
        $scope.user.profile_image = "";
        $scope.getAllDistrictSchool = function() {
            var data = {
                district_id: $cookieStore.get('loggedInDistrict').id
            };
            $http({
                method: 'POST',
                url: '/school/getDistrictSchools',
                data: data
            }).then(function successCallback(response) {
                $scope.schools = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Schools', error.data.message);
            });
        };

        $scope.getAllSchoolRoles = function(school_id) {
            $scope.getAllInfo(school_id);
            $scope.user.role_id = "";
            var data = {
                school_id: school_id
            };
            $http({
                method: 'POST',
                url: '/school/role/getAllRoleSchool',
                data: data
            }).then(function successCallback(response) {
                $scope.roles = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Schools', error.data.message);
            });
        };

        $scope.getAllInfo = function(school_id) {
            var data = {
                school_id: school_id
            };
            $http({
                method: 'POST',
                url: '/medical_info/getAllMedicalInfos',
                data: data
            }).then(function successCallback(response) {
                $scope.infos = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Medical Info', 'cannot get all the Staff! try reloading page.');
            });
        };

        $scope.uploadFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected !');
            Loading(true);
            Upload.upload({
                url: '/user/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.user.profile_image = success.data;
                },
                function(error) {
                    Loading(false);
                    toaster.pop('error', "Error", error.data.message);
                });
        };
        $scope.saveNewSchoolAdmin = function() {
            var data = $scope.user;
            $http({
                method: 'POST',
                url: '/user/schoolAdmin/register',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success','School Admin','added successfully.');
                $state.go('districts.schoolAdmin_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'Schools', error.data.message);
            });
        };
        $scope.cancel = function() {
            $state.go('districts.schoolAdmin_list');
        };
    }
]);


app.controller('districtSchoolSuperAdminEditController', ['$scope', '$http', '$state', 'toaster', '$cookieStore','Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        $scope.user = {};
        $scope.user.profile_image = "";
        $scope.saveEditSchoolAdmin = function() {
            var data = $scope.user;
            $http({
                method: 'POST',
                url: '/user/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'School Admin', 'updated successfully.');
                $state.go('districts.schoolAdmin_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'School Admin', error.data.message);
            });
        };
        $scope.getAllSchoolRoles = function(school_id) {
            var data = {
                school_id: school_id
            };
            $http({
                method: 'POST',
                url: '/school/role/getAllRoleSchool',
                data: data
            }).then(function successCallback(response) {
                $scope.roles = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Schools', error.data.message);
            });
        };
        $scope.getAllInfo = function(school_id) {
            var data = {
                school_id: school_id
            };
            $http({
                method: 'POST',
                url: '/medical_info/getAllMedicalInfos',
                data: data
            }).then(function successCallback(response) {
                $scope.infos = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Medical Info', 'cannot get all the Staff! try reloading page.');
            });
        };
        $scope.uploadFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected !');
            Loading(true);
            Upload.upload({
                url: '/user/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.user.profile_image = success.data;
                },
                function(error) {
                    Loading(false);
                    toaster.pop('error', "Error", error.data.message);
                });
        };
        $scope.getSchoolAdmin = function() {
            var data = {
                district_id: $cookieStore.get('loggedInDistrict').id
            };
            $http({
                method: 'POST',
                url: '/school/getDistrictSchools',
                data: data
            }).then(function successCallback(response) {
                $scope.schools = response.data;
                $http({
                    method: 'GET',
                    url: '/user/edit?id=' + $state.params.user_id
                }).then(function successCallback(response) {
                    $scope.user = response.data;
                    $scope.user.medical_allergies = (response.data.medical_allergies != null && response.data.medical_allergies != "") ? response.data.medical_allergies.split(',') : [];
                    $scope.user.medical_immunizations = (response.data.medical_immunizations != null && response.data.medical_immunizations != "") ? response.data.medical_immunizations.split(',') : [];
                    $scope.getAllSchoolRoles(response.data.school_id);
                    $scope.getAllInfo(response.data.school_id);
                    Loading(true);
                    setTimeout(function() {
                        $scope.user.role_id = response.data.role_id;
                        Loading(false);
                    }, 1000);
                    
                }, function errorCallback(error) {
                    toaster.pop('error', 'School Admin', error.data.message);
                });
            }, function errorCallback(error) {
                toaster.pop('error', 'School Admin', error.data.message);
            });

        };
        $scope.cancel = function() {
            $state.go('districts.schoolAdmin_list');
        };
    }
]);
