var bCrypt = require('bcrypt-nodejs');
var db = require('orm').db,
    Event = db.models.events;

exports.register = function(req, res) {
    if (typeof req.body.title == 'undefined') {
        return sendError(req, res, 422, 'required parameter not fullfiled for register new event');
    }
    var new_event = new Event();
    new_event.title = req.body.title;
    new_event.school_id = (typeof req.body.school_id != 'undefined') ? req.body.school_id : "1";
    new_event.start_date = req.body.start_date;
    new_event.end_date = req.body.end_date;
    new_event.address = req.body.address;
    new_event.country = req.body.country;
    new_event.state = req.body.state;
    new_event.city = req.body.city;
    new_event.pin_code = req.body.pin_code;
    new_event.content = req.body.content;
    new_event.status = (typeof req.body.status != 'undefined') ? req.body.status : "1";
    new_event.save(function(err, saved_event) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        res.send(saved_event);
    });
};

exports.getEvent = function(req, res) {
    if (typeof req.query.id == 'undefined') return res.send("id required for getting json");
    Event.find({
        id: req.query.id
    }, 1, function(err, events) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!events.length) {
            return sendError(req, res, 404, "event not found with perticular id");
        }
        return res.send(events[0]);
    });
};
exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'event id required');
    }
    Event.find({
        id: req.body.id
    }, 1, function(err, events) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!events.length) {
            return sendError(req, res, 404, "Department not found");
        }
        events[0].title = req.body.title;
        events[0].school_id = (typeof req.body.school_id != 'undefined') ? req.body.school_id : events[0].school_id;
        events[0].start_date = req.body.start_date;
        events[0].end_date = req.body.end_date;
        events[0].address = req.body.address;
        events[0].country = req.body.country;
        events[0].state = req.body.state;
        events[0].city = req.body.city;
        events[0].pin_code = req.body.pin_code;
        events[0].content = req.body.content;
        events[0].status = (typeof req.body.status != 'undefined') ? req.body.status : events[0].status;
        events[0].save(function(err, updated_event) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            return res.send(updated_event);
        });
    });
};
exports.active = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'event id required');
    }
    Event.find({
        id: req.body.id
    }, 1, function(err, events) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        events[0].status = "1";
        events[0].save(function(err, activated_event) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "something went wrong in activating event");
            }
            return res.send(activated_event);
        });
    });
};

exports.inactive = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'event id required');
    }
    Event.find({
        id: req.body.id
    }, 1, function(err, events) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        events[0].status = "0";
        events[0].save(function(err, activated_event) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "something went wrong in inactivating event");
            }
            return res.send(activated_event);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'event id required');
    }
    Event.find({
        id: req.body.id
    }, 1, function(err, events) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!events.length) {
            return sendError(req, res, 404, "Event not found");
        }
        events[0].remove(function(err) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "Something went wrong in deleting event");
            }
            return res.send(events[0]);
        });
    });
};

exports.getAllEvents = function(req, res) {
    var query_str = "SELECT t1.* , t2.school_name FROM `events` as t1 LEFT JOIN schools as t2 ON t1.school_id = t2.id WHERE ?";
    db.driver.execQuery(query_str, ['1'], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getAllDistrictEvents = function(req, res) {
    if (typeof req.body.district_id == 'undefined' || typeof req.body.district_id === null) {
        return sendError(req, res, 422, "district id required");
    }
    var query_str = "SELECT t1.* , t2.school_name FROM `events` as t1 LEFT JOIN schools as t2 ON t1.school_id = t2.id WHERE t2.parent_id = ? LIMIT 7";
    db.driver.execQuery(query_str, [req.body.district_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getAllSchoolEvents = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || typeof req.body.school_id === null) {
        return sendError(req, res, 422, "district id required");
    }
    var query_str = "SELECT t1.* , t2.school_name FROM `events` as t1 LEFT JOIN schools as t2 ON t1.school_id = t2.id WHERE t2.id = ?";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.listAllStudentevents = function(req, res) {
    if (typeof req.body.student_id == 'undefined' || typeof req.body.student_id === null) {
        return sendError(req, res, 422, "district id required");
    }
    var query_str = "SELECT t1.* , t2.school_name FROM `events` as t1 LEFT JOIN schools as t2 ON t1.school_id = t2.id WHERE t2.id = ?";
    db.driver.execQuery(query_str, [req.body.student_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
