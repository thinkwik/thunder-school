var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var StaffRole = db.define('staff_roles', {
    role_name: String,
    school_id: {
        type: 'integer'
    },
    description: String,
    modules:String,
    status: ['0', '1']
}, {
    timestamp: true,
    methods: {

    }
});