var db = require('orm').db,
    Payment = db.models.payments;

exports.getAllPayments = function(req, res) {
    Payment.find({}, function(err, Payments) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(Payments);
    });
};

exports.register = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || typeof req.body.package_id == 'undefined') {
        return sendError(req, res, 422, 'requested parameter are less then we accept');
    }
    newPayment = new Payment();
    newPayment.school_id = req.body.school_id;
    newPayment.package_id = req.body.package_id;
    newPayment.payment_description = req.body.payment_description;
    newPayment.save(function(err, saved_payment) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(saved_payment);
    });
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'requested parameter are less then we accept');
    }
    var findPayment = {
        id: req.body.id
    };
    Payment.find(findPayment, 1, function(err, payments) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!payments.length) {
            return res.sendError(req, res, 404, "requested payment not found");
        }
        payments[0].school_id = req.body.school_id;
        payments[0].package_id = req.body.package_id;
        payments[0].payment_description = req.body.payment_description;
        payments[0].save(function(err, updated_payment) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(updated_payment);
        });
    });
};

exports.getPayment = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'requested parameter are less then we accept');
    }
    var findPayment = {
        id: req.body.id
    };
    Payment.find(findPayment, 1, function(err, payments) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!payments.length) {
            return res.sendError(req, res, 404, "requested payment not found");
        }
        return res.send(payments[0]);
    });
};
