var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Notice = db.define('notices', {
    school_id: {
        type: 'integer'
    },
    school_auth_person_id: {
        type: 'integer'
    },
    message: String,
    send_notice_to: String,
}, {
    timestamp: true,
    methods: {

    }
});
