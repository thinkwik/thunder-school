app.controller('parentsExamListController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.getAllExamList = function() {
            var data = {
                student_id: $cookieStore.get('loggedInParent').student_id
            };
            $http({
                method: 'POST',
                url: '/exam/getStudentsExam',
                data: data
            }).then(function successCallback(response) {
                $scope.exams = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Exams', 'Something went wrong! while getting all exams');
            });
        };
    }
]);

app.controller('parentsResultListController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.getAllExamResultList = function() {
            var data = {
                school_id: $cookieStore.get('loggedInParent').school_id,
                student_id: $cookieStore.get('loggedInParent').student_id
            };
            $http({
                method: 'POST',
                url: '/exam_result/getStudentExamResults',
                data: data
            }).then(function successCallback(response) {
                $scope.results = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Exams', 'Something went wrong! while getting all exams');
            });
        };
        $scope.viewReport = function(exam_id, student_id){
            $state.go('parents.view_exam_report',{
                exam_id:exam_id,
                student_id:student_id
            });
        };
    }
]);
