var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Relation = db.define('relations', {
    school_id: {
        type: 'integer'
    },
    relation_name: String,
}, {
    timestamp: true,
    methods: {

    }
});
