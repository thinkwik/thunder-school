var bCrypt = require('bcrypt-nodejs');
var db = require('orm').db,
    School = db.models.schools,
    Department = db.models.departments,
    Staff = db.models.staff,
    Classes = db.models.classes,
    StudentClasses = db.models.student_classes;

exports.register = function(req, res) {
    if (typeof req.body.school_id != 'undefined') {
        var new_Class = new Classes();
        new_Class.school_id = req.body.school_id;
        new_Class.class_name = req.body.class_name;
        new_Class.staff_id = req.body.staff_id;
        new_Class.location_id = req.body.location_id;
        // new_Class.department_id = req.body.department_id;
        new_Class.grade_id = req.body.grade_id;
        new_Class.subject_id = req.body.subject_id;
        new_Class.day = req.body.day;
        new_Class.start_time = req.body.start_time;
        new_Class.end_time = req.body.end_time;
        new_Class.max_students = req.body.max_students;
        new_Class.status = (typeof req.body.status != 'undefined') ? req.body.status : "1";
        new_Class.save(function(err, saved_class) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            res.send(saved_class);
        });

    } else {
        return sendError(req, res, 422, 'school_id required for register new Classes');
    }
};

exports.getDistrictClass = function(req, res) {
    if (typeof req.query.id == 'undefined') return res.send("id required for getting json");
    Classes.find({
        id: req.query.id
    }, 1, function(err, calsses) {
        if (err) {
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!(calsses.length)) return res.send("Class not found with perticular id");
        return res.send(calsses[0]);
    });
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'Class id required');
    }
    Classes.find({
        id: req.body.id
    }, 1, function(err, classes) {
        if (err) {
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (classes.length === 0) {
            return sendError(req, res, 404, "Class not found");
        }
        classes[0].school_id = req.body.school_id;
        classes[0].class_name = req.body.class_name;
        classes[0].staff_id = req.body.staff_id;
        classes[0].location_id = req.body.location_id;
        // classes[0].department_id = req.body.department_id;
        classes[0].grade_id = req.body.grade_id;
        classes[0].subject_id = req.body.subject_id;
        classes[0].day = req.body.day;
        classes[0].start_time = req.body.start_time;
        classes[0].end_time = req.body.end_time;
        classes[0].max_students = req.body.max_students;
        classes[0].status = (typeof req.body.status != 'undefined') ? req.body.status : "0";
        classes[0].save(function(err, updated_class) {
            if (err) {
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            return res.send(updated_class);
        });
    });
};

exports.active = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'Class id required');
    }
    Classes.find({
        id: req.body.id
    }, 1, function(err, classes) {
        if (err) {
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        classes[0].status = "1";
        classes[0].save(function(err, activated_class) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            return res.send(activated_class);
        });
    });
};

exports.inactive = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'Class id required');
    }
    Classes.find({
        id: req.body.id
    }, 1, function(err, classes) {
        if (err) {
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        classes[0].status = "0";
        classes[0].save(function(err, inactivated_class) {
            if (err) {
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            return res.send(inactivated_class);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'Class id required');
    }
    Classes.find({
        id: req.body.id
    }, 1, function(err, classes) {
        if (err) {
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (classes.length == 0) {
            return sendError(req, res, 404, "Class not found");
        }
        classes[0].remove(function(err) {
            if (err) {
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            return res.send(classes[0]);
        });
    });
};

exports.getAllDistrictClass = function(req, res) {
    if (typeof req.body.district_id == 'undefined' || req.body.district_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    // var query_str = "SELECT t1.*,t2.school_name,t3.department_name, CONCAT(t4.first_name, ' ', t4.last_name) as user_name FROM classes as t1 LEFT JOIN schools as t2 ON t1.school_id = t2.id LEFT JOIN departments as t3 ON t1.department_id = t3.id LEFT JOIN staff as t4 ON t1.staff_id = t4.id WHERE t2.parent_id = ?";

    var query_str = `SELECT t1.*,t2.school_name,CONCAT(t4.first_name, ' ', t4.last_name) as user_name
                     FROM classes as t1
                     LEFT JOIN schools as t2 ON t1.school_id = t2.id
                     LEFT JOIN staff as t4 ON t1.staff_id = t4.id
                     WHERE t2.parent_id = ?`;
    db.driver.execQuery(query_str, [req.body.district_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getAllSchoolClass = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = `SELECT t1.*,t3.subject_name,t4.department_name,
    CONCAT(t5.first_name, ' ', t5.last_name) as staff_name, t2.grade,t6.school_name
      FROM classes as t1
      LEFT JOIN grades as t2 ON t1.grade_id = t2.id
      LEFT JOIN subjects as t3 ON t1.subject_id = t3.id
      LEFT JOIN departments as t4 ON t3.department_id = t4.id
      LEFT JOIN staff as t5 ON t1.staff_id = t5.id
      LEFT JOIN schools as t6 ON t1.school_id = t6.id
      WHERE t1.school_id = ?`;
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getSchoolClassesGradeWise = function(req, res) {
    if (typeof req.body.grade_id == 'undefined' || req.body.grade_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = `SELECT t1.*,t3.subject_name,t4.department_name, CONCAT(t5.first_name, ' ', t5.last_name) as user_name, t2.grade
              FROM classes as t1
              LEFT JOIN grades as t2 ON t1.grade_id = t2.id
              LEFT JOIN subjects as t3 ON t1.subject_id = t3.id
              LEFT JOIN departments as t4 ON t3.department_id = t4.id
              LEFT JOIN staff as t5 ON t1.staff_id = t5.id
              WHERE t1.grade_id = ?`
    db.driver.execQuery(query_str, [req.body.grade_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getAllSubjectClasses = function(req, res) {
    if (typeof req.body.subject_id == 'undefined') {
        return sendError(req, res, 422, "Please check your paramenters");
    }
    var query_str = "SELECT * FROM `classes` WHERE `subject_id` = ?";
    db.driver.execQuery(query_str, [req.body.subject_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getClassGradeWise = function(req, res) {
    if (typeof req.body.grade_id == 'undefined') {
        return sendError(req, res, 422, "Please check your paramenters");
    }
    var query_str = "SELECT * FROM `classes` WHERE grade_id = ?";
    db.driver.execQuery(query_str, [req.body.grade_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getAllStudentClasses = function(req, res) {
    console.log(req.body);
    if (typeof req.body.student_id == 'undefined' || typeof req.body.grade_id == 'undefined') {
        return sendError(req, res, 422, "Please check your paramenters");
    }
    var query_str = "SELECT t1.*,t2.class_name,t3.grade FROM student_classes as t1 LEFT JOIN classes as t2 ON t1.class_id = t2.id LEFT JOIN grades as t3 ON t1.grade_id = t3.id WHERE t1.student_id = ? AND t2.grade_id = ? ORDER BY t2.day,t2.start_time ASC";
    db.driver.execQuery(query_str, [req.body.student_id, req.body.grade_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.deleteStudentClass = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'Student Class id required!!');
    }
    StudentClasses.find({
        id: req.body.id
    }, 1, function(err, studentsClasses) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        studentsClasses[0].remove(function(err) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            return res.send(studentsClasses[0]);
        });
    });
};

exports.createStudentClass = function(req, res) {
    if (typeof req.body.grade_id != 'undefined' && typeof req.body.class_id != 'undefined' && typeof req.body.student_id != 'undefined') {
        var new_student_class = new StudentClasses();
        new_student_class.grade_id = req.body.grade_id;
        new_student_class.class_id = req.body.class_id;
        new_student_class.student_id = req.body.student_id;
        var query_str = "SELECT * FROM student_classes WHERE student_id = ? AND grade_id = ? AND class_id = ? ";
        db.driver.execQuery(query_str, [req.body.student_id, req.body.grade_id, req.body.class_id], function(err, rows) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            if (rows.length > 0) {
                return sendError(req, res, 400, "Student already register in this class.");
            }
            new_student_class.save(function(err, saved_student_class) {
                if (err) {
                    console.log(err);
                    return sendError(req, res, 400, "Bad Request check your parameters");
                }
                res.send(saved_student_class);
            });
        });
    } else {
        return sendError(req, res, 422, 'school_id required for register new Classes');
    }
};

exports.getClassSubjectWise = function(req, res) {
    if (typeof req.body.subject_id == 'undefined') {
        return sendError(req, res, 422, "Please check your paramenters");
    }
    var query_str = "SELECT * FROM `classes` WHERE subject_id = ?";
    db.driver.execQuery(query_str, [req.body.subject_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.addMultipleStudentInOneClass = function(req, res) {
    grade_id = req.body.grade_id;
    class_id = req.body.class_id;
    students = req.body.student_id;
    if (typeof req.body.grade_id === 'null' || undefined || typeof req.body.class_id === 'null' || undefined || req.body.student_id === 'null' || undefined) {
        return sendError(req, res, 422, "Please check your paramenters");
    }
    students.forEach(function(student_id) {
        var query_str = "SELECT * FROM student_classes WHERE student_id = ? AND grade_id = ? AND class_id = ?";
        db.driver.execQuery(query_str, [student_id, grade_id, class_id], function(err, rows) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            if (rows.length > 0) {
                console.log("Student already register in this class.");
                return sendError(req, res, 400, "Student already register in this class.");
            }
            new_students_class = new StudentClasses();
            new_students_class.grade_id = grade_id;
            new_students_class.class_id = class_id;
            new_students_class.student_id = student_id;
            new_students_class.save(function(err, saved_student_class) {
                if (err) {
                    return sendError(req, res, 400, "Bad Request check your parameters");
                }
            });
        });
    });
    res.send("Success!");
};


/*

SELECT t2.id, t3.grade , AVG(t1.achived_marks) as current_grade 

FROM exam_results as t1
LEFT JOIN admissions  as t2 ON t1.student_id = t2.id
LEFT JOIN grades as t3 ON t2.grade_id = t3.id

WHERE 1 GROUP BY t2.id


*/