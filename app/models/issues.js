var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Issues = db.define('issues', {
    student_id: {
        type: 'integer'
    },
    staff_id: {
        type: 'integer'
    },
    parent_id: {
        type: 'integer'
    },
    subject: String,
    priority: String,
    description: String,
    status: ['0', '1' ,'2']
}, {
    timestamp: true,
    methods: {

    }
});
