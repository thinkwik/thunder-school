app.controller('createTemplateController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore',
    function($scope, $http, $state, toaster, Upload, $cookieStore) {
        $scope.createTemplate = function() {
            data = $scope.template;
            $http({
                url: '/template/register',
                method: "POST",
                data: data
            }).then(function(response) {
                $state.go('users.templates_list');
                toaster.pop('success', "Templates", "Template create successfully!!!");
            }, function(response) {
                toaster.pop('error', "Templates", "Something went wrong!!!");
            });
        };

        $scope.getKeyList = function() {
            $http({
                method: 'POST',
                url: '/template/getAllTemplate'
            }).then(function successCallback(response) {
                var keys = response.data;
                $scope.keys = keys;
            }, function errorCallback(error) {
                toaster.pop('error', 'Template', 'cannot get all the templates! try reloading page.');
            });
        };

        $scope.listTemplates = function() {
            $state.go('users.templates_list');
        };
    }
]);

app.controller('listTemplateController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore',
    function($scope, $http, $state, toaster, Upload, $cookieStore) {
        $scope.listTemplates = function() {
            $http({
                method: 'POST',
                url: '/template/getAllTemplate'
            }).then(function successCallback(response) {
                var templates = response.data;
                $scope.templates = templates;
            }, function errorCallback(error) {
                toaster.pop('error', 'Template', 'cannot get all the templates! try reloading page.');
            });
        };

        $scope.deleteTemplate = function(template_id) {
            var data = {
                id: template_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/template/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.listTemplates();
                    toaster.pop('success', 'Template', 'Template removed successfully.');
                }, function errorCallback(error) {
                    toaster.pop('error', 'Template', 'something went wrong!');
                });
            }
        };

        $scope.editTemplate = function(template_id) {
            $state.go('users.templates_edit', {
                template_id: template_id
            });
        };

        $scope.addNewTemplate = function() {
            $state.go('users.templates_create');
        };

    }
]);

app.controller('editTemplateController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore',
    function($scope, $http, $state, toaster, Upload, $cookieStore) {
        $scope.listTemplates = function() {
            $state.go('users.templates_list');
        };

        $scope.saveUpdateTemplate = function() {
            var data = $scope.template;
            $http({
                method: 'POST',
                url: '/template/edit',
                data: data
            }).then(function successCallback(response) {
                $state.go('users.templates_list');
                toaster.pop('success', 'Template', 'Template updated successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Source', error.data.message);
                toaster.pop('error', 'Template', 'Something went wrong!');
            });
        };

        $scope.getKeyList = function() {
            $http({
                method: 'POST',
                url: '/template/getAllTemplate'
            }).then(function successCallback(response) {
                var keys = response.data;
                $scope.keys = keys;
            }, function errorCallback(error) {
                toaster.pop('error', 'Template', 'cannot get all the templates! try reloading page.');
            });
        };

        $scope.editTemplate = function() {
            $http({
                method: 'GET',
                url: '/template/edit?id=' + $state.params.template_id
            }).then(function successCallback(response) {
                var template = response.data;
                $scope.template = template;
                $scope.getKeyList($scope.template.ui_key);
            }, function errorCallback(error) {
                toaster.pop('error', 'Template', error.data.message);
            });
        };
    }
]);
