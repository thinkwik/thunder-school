var encrypt = require('./encrypt_decrypt').encrypt;
var decrypt = require('./encrypt_decrypt').decrypt;
var serialize = require('node-serialize');

module.exports = function(app, passport, multipartyMiddleware) {

    //video conferance routes
    var videoController = require('../app/controllers/video.js');
    app.post('/video/getAllParticipate', videoController.getAllParticipate);
    app.post('/video/myConferanceList', videoController.myConferanceList);


    //Chat room routes
    var chatRoomController = require('../app/controllers/chat_room.js');
    app.post('/chat_room/create', chatRoomController.register);
    app.post('/chat_room/getUserConfigCheck', chatRoomController.getUserConfigCheck);
    app.post('/chat_room/getMessagingHistory', chatRoomController.getMessagingHistory);
    

    //API video conferance + Chat room routes
    var videoController = require('../app/controllers/api/video_api.js');
    app.post('/api/video/apiGetAllParticipate', videoController.apiGetAllParticipate);
    app.post('/api/video/apiMyConferanceList', videoController.apiMyConferanceList);
    app.post('/api/chat_room/create', videoController.register);
    app.post('/api/chat_room/getUserConfigCheck', videoController.getUserConfigCheck);


    var staffApiController = require('../app/controllers/api/staff_api.js');
    app.post('/api/staff/login', staffApiController.tryLogin);
    app.post('/api/staff/me', hasStaffAuthenticateToken, staffApiController.me);
    app.post('/api/staff/profile', hasAuthenticateToken, staffApiController.profile);
    app.post('/api/staff/apiGetSchoolAllStaff', hasAuthenticateToken,staffApiController.apiGetSchoolAllStaff);
    app.post('/api/grade/apiGetAllSchoolGrades', hasAuthenticateToken,staffApiController.apiGetAllSchoolGrades);
    app.post('/api/subject/apiGetAllGradeSubjects', hasAuthenticateToken,staffApiController.apiGetAllGradeSubjects);
    app.post('/api/class/apiGetAllSubjectClasses', hasAuthenticateToken,staffApiController.apiGetAllSubjectClasses);
    // app.post('/api/staff/changePassword', hasAuthenticateToken, staffApiController.changePassword);
    app.post('/api/staff/changePassword', staffApiController.changePassword);

    app.get('/api/staff/edit', staffApiController.getStaffProfile);
    app.post('/api/staff/edit', staffApiController.edit);

    app.post('/api/staff/myschedule', hasAuthenticateToken, staffApiController.apiMyschedule);
    var studentApiController = require('../app/controllers/api/student_api.js');
    app.post('/api/student/login', studentApiController.tryLogin);
    app.post('/api/student/me', hasStudentAuthenticateToken, studentApiController.me);
    app.post('/api/student/profile', hasAuthenticateToken, studentApiController.profile);
    app.post('/api/student/myschedule', hasAuthenticateToken, studentApiController.apiMyschedule);
    // app.post('/api/student/changePassword', hasAuthenticateToken, studentApiController.changePassword);
    app.post('/api/student/changePassword' , studentApiController.changePassword);
    app.get('/api/student/edit', studentApiController.getStudentProfile);
    app.post('/api/student/edit',studentApiController.edit);
    app.post('/api/student/getAllStudentClasses', studentApiController.getAllStudentClasses);

    var parentApiController = require('../app/controllers/api/parent_api.js');
    app.post('/api/parent/login', parentApiController.tryLogin);
    app.post('/api/parent/me', hasParentAuthenticateToken, parentApiController.me);
    app.post('/api/parent/profile', hasAuthenticateToken, parentApiController.profile);
    app.post('/api/parent/changePassword' , parentApiController.changePassword);
    app.get('/api/parent/edit', parentApiController.getParentProfile);
    app.post('/api/parent/edit',parentApiController.edit);
    app.post('/api/parent/getAllChildren',parentApiController.getAllChildren);

    var homeworkApiController = require('../app/controllers/api/homework_api.js');
    app.post('/api/homework/register', hasStaffAuthenticateToken, homeworkApiController.register);
    app.post('/api/homework/apiGetHomeworkListStaff', hasStaffAuthenticateToken, homeworkApiController.apiGetHomeworkListStaff);
    app.post('/api/homework_report/apiGetAllStudentList', hasStaffAuthenticateToken, homeworkApiController.apiGetAllStudentList);
    app.post('/api/homework_report/complete', hasStaffAuthenticateToken, homeworkApiController.complete);
    app.post('/api/homework/apiGetHomeworkStudentWise', hasAuthenticateToken, homeworkApiController.apiGetHomeworkStudentWise);

    // New Routes Added
    app.post('/api/homework/delete', hasStaffAuthenticateToken, homeworkApiController.apiDeleteHomework);
    app.post('/api/homework/edit', hasStaffAuthenticateToken, homeworkApiController.apiEditHomework);
    app.post('/api/homework/uploadStudentHomework',multipartyMiddleware, homeworkApiController.uploadStudentHomeworkFile);
    app.post('/api/homework/apiUploadFile', homeworkApiController.uploadFile);

    var materialApiController = require('../app/controllers/api/material_api.js');
    app.post('/api/material/register', hasStaffAuthenticateToken, materialApiController.register);
    app.post('/api/material/edit', hasStaffAuthenticateToken, materialApiController.edit);
    app.post('/api/material/apiGetAllStaffMaterials', hasStaffAuthenticateToken, materialApiController.apiGetAllStaffMaterials);
    app.post('/api/material/apiGetStudentMaterials', materialApiController.apiGetStudentMaterials);
    app.post('/api/material/delete', materialApiController.delete);

    app.post('/api/material/apiUploadFile',multipartyMiddleware, materialApiController.uploadFile);


    var attendanceApiController = require('../app/controllers/api/attendance_api.js');
    app.post('/api/attendance/register', attendanceApiController.register);
    app.post('/api/attendance/getAttendanceStaffwise', attendanceApiController.apiGetAttendanceStaffwise);
    app.post('/api/attendance/attend', attendanceApiController.apiAttend);
    app.post('/api/attendance/makeAttendAll', attendanceApiController.apiMakeAttendAll);
    app.post('/api/attendance/makeNotAttendAll', attendanceApiController.apiMakeNotAttendAll);
    app.post('/api/attendance/getAllStudentList', attendanceApiController.apiGetAllStudentList);
    app.post('/api/attendance/getAllStudentAttendence', attendanceApiController.apiGetAllStudentAttendence);

    //New Routes added
    app.post('/api/attendance/edit', attendanceApiController.edit);
    app.get('/api/attendance/edit', attendanceApiController.getAttendance);
    app.post('/api/attendance/delete', attendanceApiController.apiDeleteAttendance);

    var examApiController = require('../app/controllers/api/exam_api.js');
    app.post('/api/exam/getStaffExams', examApiController.apiGetStaffExams);
    app.post('/api/exam/getStudentsExam', examApiController.apiGetStudentsExam);
    app.post('/api/exam/apiGetAllExamResults', examApiController.apiGetAllExamResults);
    app.post('/api/exam/apiGetStudentExamResults', examApiController.apiGetStudentExamResults);
    app.post('/api/exam/apiGetAllExamResultsUptoClass', examApiController.apiGetAllExamResultsUptoClass);
    app.post('/api/exam/apiGetAllExamResultsUptoResult', examApiController.apiGetAllExamResultsUptoResult);


    var todoApiController = require('../app/controllers/api/todo_api.js');
    app.post('/api/todo/register', todoApiController.register);
    app.post('/api/todo/delete', todoApiController.delete);
    app.post('/api/todo/getMyTodos', todoApiController.getMyTodos);
    app.post('/api/todo/done', todoApiController.done);

    var assignmentApiController = require('../app/controllers/api/assignment_api.js');
    app.post('/api/assignment/register', assignmentApiController.register);
    app.post('/api/assignment/delete', assignmentApiController.delete);
    app.post('/api/assignment/apiGetAllStaffAssignments', assignmentApiController.apiGetAllStaffAssignments);
    app.post('/api/assignment/apiGetAllAssignmentStudentList', assignmentApiController.apiGetAllAssignmentStudentList);
    app.post('/api/assignment/completed', assignmentApiController.completed);
    app.post('/api/assignment/apiUpdateGrades', assignmentApiController.apiUpdateGrades);
    app.post('/api/assignment/apiGetAllStudentAssignments', assignmentApiController.apiGetAllStudentAssignments);

    app.post('/api/assignment/apiUploadFile',multipartyMiddleware, assignmentApiController.uploadFile);

    //New Assignment Routes added
    app.post('/api/assignment/edit', assignmentApiController.edit);
    app.get('/api/assignment/edit', assignmentApiController.getAssignment);

    //Manage Appointments APIs
    var appointmentApiController = require('../app/controllers/api/appointment_api.js');
    app.post('/api/appointment/register', appointmentApiController.register);
    app.post('/api/appointment/delete', appointmentApiController.delete);
    app.get('/api/appointment/edit', appointmentApiController.getAppointment);
    app.post('/api/appointment/edit', appointmentApiController.edit);
    app.post('/api/appointment/active', appointmentApiController.active);
    app.post('/api/appointment/inactive', appointmentApiController.inactive);
    app.post('/api/appointment/getAllAppointments', appointmentApiController.getAllAppointments);
    app.post('/api/appointment/getAllStudentsAppointment', appointmentApiController.getAllStudentsAppointments);
    app.post('/api/appointment/getAllParentsAppointments', appointmentApiController.getAllParentsAppointments);

    app.post('/api/appointment/getAllStaffAppointments', appointmentApiController.getAllStaffAppointments);
    app.post('/api/appointment/changeStatus', appointmentApiController.changeStatus);
    app.post('/api/appointment/changeStudentAttend', appointmentApiController.changeStudentAttend);

    app.post('/api/parents/getSchoolParents', appointmentApiController.getSchoolParents);
    app.post('/api/student/getSchoolStudents',appointmentApiController.getSchoolStudents);

    //Manage Events APIs
    var eventApiController = require('../app/controllers/api/event_api.js');
    app.post('/api/event/getAllDistrictEvents', eventApiController.getAllDistrictEvents);
    app.post('/api/event/getAllSchoolEvents',  eventApiController.getAllSchoolEvents);
    app.post('/api/event/staff_events',  eventApiController.staff_events);
    app.post('/api/event/student_events',  eventApiController.student_events);
    app.post('/api/event/parent_events',  eventApiController.parent_events);
    app.post('/api/gallery/viewAlbum',  eventApiController.viewAlbum);

    var issueApiController = require('../app/controllers/api/issue_api.js');
    app.post('/api/issue/register', issueApiController.register);
    app.post('/api/issue/delete', issueApiController.delete);
    app.post('/api/issue/apiGetAllStaffIssues', issueApiController.apiGetAllStaffIssues);
    app.post('/api/issue/apiGetAllParentsIssues', issueApiController.apiGetAllParentsIssues);
    app.post('/api/issue/apiGetAllStudentsIssues', issueApiController.apiGetAllStudentsIssues);
    app.post('/api/issue/apiParentSendMessage', issueApiController.apiParentSendMessage);
    app.post('/api/issue/apiStudentSendMessage', issueApiController.apiStudentSendMessage);
    app.post('/api/issue/apiStaffSendMessage', issueApiController.apiStaffSendMessage);

    app.post('/api/issue/apiUpdateStatus', issueApiController.apiUpdateStatus);


    app.post('/api/issue/saveIssuesMessgesResponseStudent', issueApiController.saveIssuesMessgesResponseStudent);
    app.post('/api/issue/saveIssuesMessgesResponseParent', issueApiController.saveIssuesMessgesResponseParent);

    //New Routes Added
    app.post('/api/issue/delete', issueApiController.delete);

    //Manage Forgot Password APIs
    var forgotPasswordApiController = require('../app/controllers/api/forgot_password_api.js');
    app.post('/api/forgot_password', forgotPasswordApiController.apiForgotPasswordAll);
    app.post('/api/reset_password', forgotPasswordApiController.apiResetPasswordAll);


    //Manage Departments
    var departmentApiController = require('../app/controllers/api/department_api.js');
    app.post('/api/department/list', departmentApiController.getAllSchoolDepartment);

    //Manage Departments
    var sessionApiController = require('../app/controllers/api/session_api.js');
    app.post('/api/session/list', sessionApiController.getAllSchoolSessions);


    //Manage Lectures
    var lectureApiController = require('../app/controllers/api/lecture_api.js');
    app.post('/api/lecture/register' , lectureApiController.register);
    app.post('/api/lecture/edit', lectureApiController.edit);
    app.post('/api/lecture/delete', lectureApiController.delete);
    app.post('/api/lecture/getLecture', lectureApiController.getLecture);
    app.post('/api/lecture/getAllLecturesSchoolWise', lectureApiController.getAllLecturesSchoolWise);
    app.post('/api/lecture/getAllLecturesStaffWise', lectureApiController.getAllLecturesStaffWise);

    var followerApiController = require('../app/controllers/api/follower_api.js');
    app.post('/api/follow/apiGetList', followerApiController.apiGetList);
    app.post('/api/follow/followPerson', followerApiController.followPerson);
    app.post('/api/follow/getAllPosts', followerApiController.getAllPosts);
    app.post('/api/follow/apiAddPost', followerApiController.apiAddPost);
    app.post('/api/follow/delete', followerApiController.delete);
    app.post('/api/follow/like', followerApiController.like);
    app.post('/api/follow/comment', followerApiController.comment);
    app.post('/api/follow/apiListLikes', followerApiController.apiListLikes);
    app.post('/api/follow/apiListComments', followerApiController.apiListComments);
    app.post('/api/follow/apiFollowerList', followerApiController.apiFollowerList);
    app.post('/api/follow/apiFollowingList', followerApiController.apiFollowingList);

    app.get('/api/parceToken', hasAuthenticateToken, function(req, res) {
        res.send(req.api_token_data);
    });

    app.post('/api/upload', function(req, res){
        //console.log(req.body);
        if(typeof req.files.homework_file != 'undefined'){
            var file = req.files.homework_file;
            fileName = file.path.split('/');
            type = (typeof file.type != 'undefined' && file.type != "") ? "." + file.type.split('/')[1] : "";
            fs.rename(file.path, __dirname + "/../public/upload/pageImages/" + fileName[fileName.length - 1] + type);
            return res.send(fileName[fileName.length - 1] + type);
        }
        res.send({});
    });

    //file not found
    app.get('/upload/*', function(req, res) {
        res.send(404);
    });
    //angularJS routes
    app.get('*', function(req, res) {
        res.render('index.html');
    });
};

function hasAuthenticateToken(req, res, next) {
    if (typeof req.get('Authorization') != 'undefined') {
        var decrypted_token = decrypt(req.get('Authorization'));
        if (decrypted_token) {
            try {
                req.api_token_data = serialize.unserialize(decrypted_token);
                next();
            } catch (ex) {
                return apiSendError(req, res, 444, "Authorization token is wrong");
            }
        } else {
            return apiSendError(req, res, 444, "Authorization token is invalid");
        }
    } else {
        return apiSendError(req, res, 444, "Authorization token is required");
    }
}

function hasStaffAuthenticateToken(req, res, next) {
    if (typeof req.get('Authorization') != 'undefined') {
        var decrypted_token = decrypt(req.get('Authorization'));
        if (decrypted_token) {
            try {
                req.api_token_data = serialize.unserialize(decrypted_token);
                if(req.api_token_data.type != 'undefined' && req.api_token_data.id != 'undefined' && req.api_token_data.type == "staff"){
                    next();
                } else {
                    return apiSendError(req, res, 444, "Staff Authorization token is required");
                }
            } catch (ex) {
                return apiSendError(req, res, 444, "Authorization token is wrong");
            }
        } else {
            return apiSendError(req, res, 444, "Authorization token is invalid");
        }
    } else {
        return apiSendError(req, res, 444, "Authorization token is required");
    }
}

function hasStudentAuthenticateToken(req, res, next) {
    if (typeof req.get('Authorization') != 'undefined') {
        var decrypted_token = decrypt(req.get('Authorization'));
        if (decrypted_token) {
            try {
                req.api_token_data = serialize.unserialize(decrypted_token);
                console.log(req.api_token_data);
                if(req.api_token_data.type != 'undefined' && req.api_token_data.id != 'undefined' && req.api_token_data.type == "student"){
                    next();
                } else {
                    return apiSendError(req, res, 444, "Student Authorization token is required");
                }
            } catch (ex) {
                return apiSendError(req, res, 444, "Authorization token is wrong");
            }
        } else {
            return apiSendError(req, res, 444, "Authorization token is invalid");
        }
    } else {
        return apiSendError(req, res, 444, "Authorization token is required");
    }
}

function hasParentAuthenticateToken(req, res, next) {
    if (typeof req.get('Authorization') != 'undefined') {
        var decrypted_token = decrypt(req.get('Authorization'));
        if (decrypted_token) {
            try {
                req.api_token_data = serialize.unserialize(decrypted_token);
                if(req.api_token_data.type != 'undefined' && req.api_token_data.id != 'undefined' && req.api_token_data.type == "parent"){
                    next();
                } else {
                    return apiSendError(req, res, 444, "Parent Authorization token is required");
                }
            } catch (ex) {
                return apiSendError(req, res, 444, "Authorization token is wrong");
            }
        } else {
            return apiSendError(req, res, 444, "Authorization token is invalid");
        }
    } else {
        return apiSendError(req, res, 444, "Authorization token is required");
    }
}
