app.controller('createStaffAppointmentController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        var date = new Date();
        date.setDate(date.getDate() - 1);
        $scope.minDate = date.toString();
        $scope.hours = "00";
        $scope.mins = "00";
        
        $scope.cancelAppointmentForm = function() {
            $state.go('schoolstaff.appointment_list');
        };


        $scope.createAppointment = function() {
            $scope.appointment.datetime = $scope.appointment.datetime + " "+ $scope.hours + ":"+ $scope.mins + ":00";
            data = $scope.appointment;
            data.staff_id = $cookieStore.get('loggedInStaff').id;
            $http({
                method: "POST",
                url: "/appointment/register",
                data: data
            }).then(function successCallback(response) {
                $state.go('schoolstaff.appointment_list');
                toaster.pop('success', 'Appointment', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Appointment', error.data.message);
            });
        };

        $scope.getSchoolStudents = function() {
            var data = {
                school_id: $cookieStore.get('loggedInStaff').school_id
            };
            $http({
                method: 'POST',
                url: '/student/getAllAdmissionInquiry',
                data: data
            }).then(function successCallback(response) {
                $scope.students = response.data;
                $scope.getSchoolParents();
            }, function errorCallback(error) {
                toaster.pop('error', 'School', 'Something went wrong!');
            });
        };

        $scope.getSchoolParents = function() {
            var data = {
                school_id: $cookieStore.get('loggedInStaff').school_id
            };
            $http({
                method: 'POST',
                url: '/parents/getSchoolParents',
                data: data
            }).then(function successCallback(response) {
                $scope.parents = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'School', 'Something went wrong!');
            });
        };
    }
]);

app.controller('listStaffAppointmentController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {

        $scope.addNewAppointment = function() {
            $state.go('schoolstaff.appointment_create');
        };

        $scope.listAllAppointments = function() {
            var data = {
                staff_id: $cookieStore.get('loggedInStaff').id
            };
            $http({
                method: 'POST',
                url: '/staff/appointment/listAllAppointments',
                data: data
            }).then(function successCallback(response) {
                $scope.appointments = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Appointment', 'cannot get all the Appointments! try reloading page.');
            });
        };

        $scope.listAllSchoolAppointments = function() {
            var data = {
                school_id: $cookieStore.get('loggedInStaff').school_id
            };
            $http({
                method: 'POST',
                url: '/staff/appointment/listAllSchoolAppointments',
                data: data
            }).then(function successCallback(response) {
                $scope.appointments = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Appointment', 'cannot get all the Appointments! try reloading page.');
            });
        };

        

        $scope.init = function() {
            if($cookieStore.get('loggedInStaff').security_roles.indexOf('receptionist') != -1){
                $scope.listAllSchoolAppointments();
            } else {
                $scope.listAllAppointments();
            }
            
        }

        $scope.deleteAppointment = function(appointment_id) {
            var data = {
                id: appointment_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/appointment/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.init();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Appointment', error.data.message);
                });
            }
        };

        $scope.studentAttend = function(appointment) {
            var data = {
                id: appointment.id,
                student_attend: appointment.student_attend
            };
            $http({
                method: 'POST',
                url: '/staff/appointment/changeStudentAttend',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Appointment', 'Updated successfully!');
            }, function errorCallback(error) {
                toaster.pop('error', 'Appointment', 'something went wrong!');
            });
        };

        $scope.changeStatus = function(appointment) {
            var data = {
                id: appointment.id,
                status: appointment.status
            };
            $http({
                method: 'POST',
                url: '/staff/appointment/changeStatus',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Appointment', 'Status updated successfully!');
            }, function errorCallback(error) {
                toaster.pop('error', 'Appointment', 'something went wrong!');
            });
        };
    }
]);
