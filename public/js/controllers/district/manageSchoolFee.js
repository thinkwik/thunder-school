app.controller('districtFeeListController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
	function($scope, $http, $state, toaster, $cookieStore) {
		$scope.createFeeRedirect = function() {
			$state.go('districts.school_fees_create');
		};
		$scope.getAllDistrictFees = function() {
			var data = {
				district_id: $cookieStore.get('loggedInDistrict').id
			};
			$http({
				method: 'POST',
				url: '/fee/getAllDistrictFees',
				data: data
			}).then(function successCallback(response) {
				$scope.fees = response.data;
			}, function errorCallback(error) {
				toaster.pop('error', 'Fee', error.data.message);
			});
		};
		$scope.deleteFee = function(fee_id) {
			var data = {
				id: fee_id
			};
			$http({
				method: 'POST',
				url: '/fee/delete',
				data: data
			}).then(function successCallback(response) {
				$scope.getAllDistrictFees();
			}, function errorCallback(error) {
				toaster.pop('error', 'Fee', error.data.message);
			});
		};
		$scope.editFee = function(fee_id) {
			$state.go('districts.school_fees_edit', {
				fee_id: fee_id
			});
		};
	}
]);

app.controller('districtFeeCreateController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
	function($scope, $http, $state, toaster, $cookieStore) {
		$scope.gradeSelectDiv = false;
		$scope.getDistrictSchools = function() {
			var data = {
				parent_id: $cookieStore.get('loggedInDistrict').id
			};
			$http({
				method: 'POST',
				url: '/school/getAllSchools',
				data: data
			}).then(function successCallback(response) {
				$scope.schools = response.data;
			}, function errorCallback(error) {
				toaster.pop('error', 'School', error.data.message);
			});
		};
		$scope.schoolChange = function(school_id) {
			if (school_id !== '') {
				$scope.gradeSelectDiv = true;
				var data = {
					school_id: school_id
				};
				$http({
					method: 'POST',
					url: '/grade/getAllSchoolGrades',
					data: data
				}).then(function successCallback(response) {
					$scope.grades = response.data;
				}, function errorCallback(error) {
					toaster.pop('error', 'Grade', error.data.message);
				});
			}
		};
		$scope.createNewFee = function() {
			var data = $scope.fee;
			$http({
				method: 'POST',
				url: '/fee/register',
				data: data
			}).then(function successCallback(response) {
				toaster.pop('success', 'New Fee', 'added successfully.');
				$state.go('districts.school_fees_list');
			}, function errorCallback(error) {
				toaster.pop('error', 'Grade', error.data.message);
			});
		};
		$scope.cancelFee = function() {
			$state.go('districts.school_fees_list');
		};
	}
]);

app.controller('districtFeeEditController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
	function($scope, $http, $state, toaster, $cookieStore) {
		$scope.cancelFee = function() {
			$state.go('districts.school_fees_list');
		};
		$scope.editSaveFee = function() {
			var data = $scope.fee;
			$http({
				method: 'POST',
				url: '/fee/edit',
				data: data
			}).then(function successCallback(response) {
				toaster.pop('success', 'New Fee', 'Updated successfully.');
				$state.go('districts.school_fees_list');
			}, function errorCallback(error) {
				toaster.pop('error', 'Fee', error.data.message);
			});
		};
		$scope.schoolChange = function(school_id) {
			if (school_id !== '') {
				$scope.fee.grade_id = '';
				var data = {
					school_id: school_id
				};
				$http({
					method: 'POST',
					url: '/grade/getAllSchoolGrades',
					data: data
				}).then(function successCallback(response) {
					$scope.grades = response.data;
				}, function errorCallback(error) {
					toaster.pop('error', 'Grade', error.data.message);
				});
			}
		};
		$scope.getFeeForEdit = function() {
			$http({
				method: 'GET',
				url: '/fee/edit?id=' + $state.params.fee_id
			}).then(function successCallback(response1) {
				var data1 = {
					parent_id: $cookieStore.get('loggedInDistrict').id
				};
				$http({
					method: 'POST',
					url: '/school/getAllSchools',
					data: data1
				}).then(function successCallback(response2) {
					$scope.schools = response2.data;
					var data2 = {
						school_id: response1.data.school_id
					};
					$http({
						method: 'POST',
						url: '/grade/getAllSchoolGrades',
						data: data2
					}).then(function successCallback(response3) {
						$scope.grades = response3.data;
						$scope.fee = response1.data;
						Loading(true);
						setTimeout(function(){ $scope.fee = response1.data; }, 2000);
						Loading(false);
					}, function errorCallback(error) {
						toaster.pop('error', 'Grade', error.data.message);
					});
				}, function errorCallback(error) {
					toaster.pop('error', 'School', error.data.message);
				});
			}, function errorCallback(error) {
				toaster.pop('error', 'Fee', error.data.message);
			});
		};
	}
]);
