var cert, users;

  users = require('./user');

  cert = require('../../config/certificate');
  var db = require('orm').db;
  module.exports = function(app) {
    var io, server;
    server = require('https').createServer({
      key: cert.privateKey,
      cert: cert.certificate
    }, app);
    io = require('socket.io')(server,{'pingInterval': 2500, 'pingTimeout': 4000});
    io.use(function(socket, next) {
      if (socket.request._query['username'] && socket.request._query['roomname']) {
        if(users.addUser(socket.request._query['username'], socket.id, io)){
          return next();
        }
        return next(new Error('User exists, try refreshing again!'));
      }
      return next(new Error('Authentication error'));
    });
    io.on('connection', function(socket) {
      var activeRoomClients, client, clients_in_the_room, ref, value;
      clients_in_the_room = io.sockets.adapter.rooms[socket.request._query['roomname']] || [];
      activeRoomClients = [];
      ref = clients_in_the_room.sockets;
      for (client in ref) {
        value = ref[client];
        activeRoomClients.push(io.sockets.connected[client].request._query['username']);
      }
      socket.emit('user-list', {
        client: activeRoomClients
      });
      socket.join(socket.request._query['roomname']);
      socket.on('control', function(data) {
        switch (data.type) {
          case 'join-room':
            if (!(data.roomname && data.self)) {
              return console.log("Wrong room join request format :" + (JSON.stringify(data)));
            }
            return database.getChatRoom(data.roomname, false, function(status, room) {
              if (!status) {
                return console.log("Cant join room for client, roomname not found or internal database error");
              }
              if ((room.participants.indexOf(data.self) !== -1) || (room.initiator === data.self)) {
                return socket.join(data.roomname);
              } else {
                return console.log("Cant join room, user may not be invited");
              }
            });
        }
      });
      socket.on('video-signal', function(data) {
        if (!data.roomname) {
          return console.log("Wrong format of video signal :" + (JSON.stringify(data)));
        }
        return io.to(data.roomname).emit('video-signal', data);
      });
      socket.on('group-message', function(data) {
        if (!(data.from && data.message && data.roomname)) {
          return console.log("Wrong format group message :" + (JSON.stringify(data)));
        }
        //saving messages into table
        var query_str = "INSERT INTO `group_messages`(`from_user`, `chat_room_id`, `message`) VALUES ('"+data.unique_id +"','"+data.roomname+"','"+data.message+"')";
        db.driver.execQuery(query_str, [], function(err, rows) {
            if (err) {
                console.log(err);
            }
        });
        return io.to(data.roomname).emit('group-message', data);
      });
      return socket.on('disconnect', function() {
        var userid;
        userid = users.deleteUser(socket.id);
        if (userid) {
          return io.to(socket.request._query['roomname']).emit('video-signal', {
            signal: 'teardown',
            roomname: socket.request._query['roomname'],
            deserter: socket.request._query['username']
          });
        }
      });
    });
    return server.listen(3001);
  };
