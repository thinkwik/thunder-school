var db = require('orm').db,
    BookStore = db.models.bookstore;

exports.register = function(req, res) {
    var query_str = "SELECT * FROM `bookstore` WHERE isbn_number = ?";
    db.driver.execQuery(query_str, [req.body.isbn_number],function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (rows.length) {
            return sendError(req, res, 422, "Book Already Exists");
        }
        if (!rows.length) {
            newBook = new BookStore();
            newBook.category = req.body.category;
            newBook.publisher_name = req.body.publisher_name;
            newBook.title = req.body.title;
            newBook.image = req.body.image;
            newBook.book = req.body.book;
            newBook.isbn_number = req.body.isbn_number;
            newBook.author_name = req.body.author_name;
            newBook.pages = req.body.pages;
            newBook.description = req.body.description;
            newBook.status = '1';
            newBook.save(function(error, added_book) {
                if (error) {
                    console.log(error);
                    return sendError(req, res, 422, 'Please check your parameters.');
                }
                return res.send(added_book);
            });
        }
    });
};

exports.getAllBooks = function(req, res) {
    var query_str = `SELECT t1.id, t1.publisher_name, t1.title ,t1.image ,t1.book ,t1.isbn_number ,t1.author_name ,t1.pages ,t1.description ,t1.status , t1.created_at, t2.category_name as category FROM bookstore as t1
                     LEFT JOIN bookcategory as t2 on t1.category = t2.id 
                     WHERE 1`;
    db.driver.execQuery(query_str, function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getAllAvailBooks = function(req, res) {
    var query_str = `SELECT t1.id, t1.publisher_name, t1.title ,t1.image ,t1.book ,t1.isbn_number ,t1.author_name ,t1.pages ,t1.description ,t1.status , t1.created_at, t2.category_name as category 
                     FROM bookstore as t1
                     LEFT JOIN bookcategory as t2 on t1.category = t2.id 
                     WHERE 1`;
    db.driver.execQuery(query_str, function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Book id not found!');
    }
    BookStore.find({
        id: req.body.id
    }, 1, function(err, books) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, 'Please check your parameters.');
        }
        books[0].category = req.body.category;
        books[0].publisher_name = req.body.publisher_name;
        books[0].title = req.body.title;
        books[0].image = (typeof req.body.image == 'undefined' || req.body.image === "") ? books[0].image : req.body.image;
        books[0].book = (typeof req.body.book == 'undefined' || req.body.book === "") ? books[0].book : req.body.book;
        books[0].isbn_number = req.body.isbn_number;
        books[0].author_name = req.body.author_name;
        books[0].pages = req.body.pages;
        books[0].description = req.body.description;
        books[0].status = (typeof req.body.status != 'undefined') ? req.body.status : books[0].status;
        books[0].save(function(err, updated_book) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, 'Please check your parameters.');
            }
            return res.send(updated_book);
        });
    });
};

exports.available = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Book id required!!');
    }
    BookStore.find({
        id: req.body.id
    }, 1, function(err, books) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        books[0].status = "1";
        books[0].save(function(err, available_book) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, 'Please check your parameters.');
            }
            return res.send(available_book);
        });
    });
};

exports.notAvailable = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Book id required!!');
    }
    BookStore.find({
        id: req.body.id
    }, 1, function(err, books) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        books[0].status = "0";
        books[0].save(function(err, not_available_book) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(not_available_book);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Book id required!!');
    }
    BookStore.find({
        id: req.body.id
    }, 1, function(err, books) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        if (books[0].image) {
            var bookImage = __dirname + "/../../public/upload/bookImages/" + books[0].image;
            var fs = require('fs');
            fs.exists(bookImage, function(exists) {
                if (exists) {
                    fs.unlinkSync(bookImage);
                }
            });
        }
        books[0].remove(function(err) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(books[0]);
        });
    });
};

exports.getBook = function(req, res) {
    if (typeof req.query.id == 'undefined') return sendError(req, res, 401, "ID required field");
    BookStore.find({
        id: req.query.id
    }, 1, function(err, books) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        if (!books.length) {
            return sendError(req, res, 404, "Book not found");
        }
        return res.send(books[0]);
    });
};

var fs = require('fs');
exports.uploadFile = function(req, res) {
    var fs = require('fs');
    var bookImage = __dirname + "/../../public/upload/bookImages/";
    if (!fs.existsSync(bookImage)) {
        fs.mkdirSync(bookImage);
    }
    var file = req.files.file;
    var fileExtension = file.name.split(".")[1];
    if (fileExtension) {
        fileName = file.path.split('/');
        fs.rename(file.path, bookImage + fileName[fileName.length - 1] + "." + fileExtension);
        return res.send(fileName[fileName.length - 1] + "." + fileExtension);
    } else {
        return sendError(req, res, 400, "Bad Request check your parameters");
    }
};


exports.uploadBook = function(req, res) {
    var fs = require('fs');
    var books = __dirname + "/../../public/upload/books/";
    if (!fs.existsSync(books)) {
        fs.mkdirSync(books);
    }
    var file = req.files.file;
    var EPub = require('epub');
    


    var fileExtension = file.name.split(".")[1];
    if (fileExtension) {
        if(fileExtension != 'epub') {
            return sendError(req, res, 400, "Only Epub file can upload");
        }
        fileName = file.path.split('/');
        fs.rename(file.path, books + fileName[fileName.length - 1] + "." + fileExtension);

        var epub = new EPub(books + fileName[fileName.length - 1] + "." + fileExtension);
           epub.on("end", function () {
                // console.log(epub);
                // epub.getImage("image1", function(error, img, mimeType){
                //     if(error){
                //         console.log(error);
                //         return sendError(req, res, 400, "something went wrong");
                //     }
                //     console.log(img);
                // });

                var response = epub.metadata;
                response.newBookName = fileName[fileName.length - 1] + "." + fileExtension;
                return res.send(response);
           });
           epub.on("error", function (error) {
                return sendError(req, res, 400, error);
           });
           epub.parse();

    } else {
        return sendError(req, res, 400, "Bad Request check your parameters");
    }
};
