var db = require('orm').db,
    Admissions = db.models.admissions,
    Appointments = db.models.appointments,
    Staff = db.models.staff;

exports.register = function(req, res) {
    if (req.body.staff_id && req.body.appointment_type && req.body.datetime && req.body.description) {
        newAppointment = new Appointments();
        newAppointment.staff_id = req.body.staff_id;
        newAppointment.student_id = req.body.student_id;
        newAppointment.parent_id = req.body.parent_id;
        newAppointment.appointment_type = req.body.appointment_type;
        newAppointment.datetime = req.body.datetime;
        newAppointment.description = req.body.description;
        newAppointment.status = (typeof req.body.status != 'undefined') ? req.body.status : '1';
        newAppointment.student_attend = (typeof req.body.student_attend != 'undefined') ? req.body.student_attend : '0';
        newAppointment.phone_number = (typeof req.body.phone_number != 'undefined') ? req.body.phone_number : null;
        newAppointment.save(function(err, created_appointment) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 400, "Bad Request");
            }
            var response = {};
            response['data'] = [];
            response['code'] = 200;
            return res.send(response);
        });
    } else {
        return apiSendError(req, res, 422, 'Please check your parameters.');
    }
};

exports.delete = function(req, res) {
    if (req.body.id !== 'undefined' && req.body.id) {
        Appointments.find({
            id: req.body.id
        }, 1, function(err, appointments) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 400, "Bad Request");
            }
            if (!appointments.length) {
                return apiSendError(req, res, 404, "Not Found.");
            }
            appointments[0].remove(function(err) {
                if (err) {
                    console.log(err);
                    return apiSendError(req, res, 400, "Bad Request");
                }
                var response = {};
                response['data'] = [];
                response['code'] = 200;
                return res.send(response);
            });
        });
    } else {
        return apiSendError(req, res, 401, 'Appointment id required!!');
    }
};

exports.getAppointment = function(req, res) {
    if (req.query.id !== 'undefined' && req.query.id) {
        Appointments.find({
            id: req.query.id
        }, 1, function(err, appointments) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 400, "Bad Request check your parameters.");
            }
            if (!appointments.length) {
                return apiSendError(req, res, 404, "Appointment not found.");
            }
            var response = {};
            response['data'] = appointments;
            response['code'] = 200;
            return res.send(response);
        });
    } else {
        return apiSendError(req, res, 401, "Appointment ID required field");
    }
};

exports.edit = function(req, res) {
    if (req.body.id !== 'undefined' && req.body.id) {
        Appointments.find({
            id: req.body.id
        }, 1, function(err, appointments) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 400, "Bad Request check your parameters");
            }
            if (!appointments.length) {
                return apiSendError(req, res, 404, "Appointment not found.");
            }
            appointments[0].appointment_type = req.body.appointment_type;
            appointments[0].datetime = req.body.datetime;
            appointments[0].description = req.body.description;
            appointments[0].status = (typeof req.body.status != 'undefined') ? req.body.status : appointments[0].status;
            appointments[0].student_attend = (typeof req.body.student_attend != 'undefined') ? req.body.student_attend : appointments[0].student_attend;
            appointments[0].save(function(err, updated_appointment) {
                if (err) {
                    console.log(err);
                    return res.send(err);
                }
                var response = {};
                response['data'] = [];
                response['code'] = 200;
                return res.send(response);
            });
        });
    } else {
        return apiSendError(req, res, 404, 'Appointment id required!!');
    }
};

exports.active = function(req, res) {
    if (req.body.id !== 'undefined' && req.body.id) {
        Appointments.find({
            id: req.body.id
        }, 1, function(err, appointment) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 400, "Bad Request check your parameters");
            }
            if (!appointment.length) {
                return apiSendError(req, res, 404, "Appointment not found.");
            }
            appointment[0].status = "1";
            appointment[0].save(function(err, activated_appointment) {
                if (err) {
                    console.log(err);
                    return apiSendError(req, res, 404, "Appointment not found.");
                }
                var response = {};
                response['data'] = [];
                response['code'] = 200;
                return res.send(response);
            });
        });
    } else {
        return apiSendError(req, res, 404, 'Appointment id required!!');
    }
};

exports.inactive = function(req, res) {
    if (req.body.id !== 'undefined' && req.body.id) {
        Appointments.find({
            id: req.body.id
        }, 1, function(err, appointment) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 400, "Bad Request check your parameters");
            }
            if (!appointment.length) {
                return apiSendError(req, res, 404, "Appointment not found.");
            }
            appointment[0].status = "0";
            appointment[0].save(function(err, inactivated_appointment) {
                if (err) {
                    console.log(err);
                    return apiSendError(req, res, 404, "Appointment not found.");
                }
                var response = {};
                response['data'] = [];
                response['code'] = 200;
                return res.send(response);
            });
        });
    } else {
        return apiSendError(req, res, 404, 'Appointment id required!!');
    }
};

exports.getAllStaffAppointments = function(req, res) {
    if (req.body.staff_id !== 'undefined' && req.body.staff_id) {
        var query_str = `SELECT t1.*,CONCAT(t2.first_name,t2.last_name) as staff_name,CONCAT(t3.first_name,' ',t3.last_name) as student_name,t5.full_name
                           FROM appointments as t1
                           LEFT JOIN staff as t2 on t1.staff_id = t2.id
                           LEFT JOIN admissions as t3 on t1.student_id = t3.id
                           LEFT JOIN parents as t5 on t1.parent_id = t5.id
                           LEFT JOIN schools as t4 on t3.school_id = t4.id
                           WHERE t2.id = ?`;
        db.driver.execQuery(query_str, [req.body.staff_id], function(err, rows) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 400, "Bad Request check your parameters");
            }
            if (!rows.length) {
                return apiSendError(req, res, 404, "Appointment not found.");
            }
            var response = {};
            response['data'] = rows;
            response['code'] = 200;
            return res.send(response);
        });
    } else {
        return apiSendError(req, res, 422, "refer parameters");
    }
};

// exports.getAllAppointments = function(req, res) {
//     Appointments.find({}, function(err, allAppointments) {
//         if (err) {
//             console.log(err);
//             return apiSendError(req, res, 400, "Bad Request check your parameters");
//         }
//         if (!allAppointments.length) {
//             return apiSendError(req, res, 404, "Appointment not found.");
//         }
//         var response = {};
//         response['data'] = allAppointments;
//         response['code'] = 200;
//         return res.send(response);
//     });
// };

exports.getAllStudentsAppointments = function(req, res) {
    if (req.body.student_id !== 'undefined' && req.body.student_id) {
        var query_str = `SELECT t1.*,CONCAT(t2.first_name,' ',t2.last_name) as staff_name,
        CONCAT(t3.first_name,' ',t3.last_name) as student_name FROM appointments as t1
        LEFT JOIN staff as t2 on t1.staff_id = t2.id
        LEFT JOIN admissions as t3 on t1.student_id = t3.id
        LEFT JOIN schools as t4 on t3.school_id = t4.id
        WHERE t1.student_id = ?
        ORDER BY t1.datetime DESC`;
        db.driver.execQuery(query_str, [req.body.student_id], function(err, rows) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 400, "Bad Request check your parameters");
            }
            if (!rows.length) {
                return apiSendError(req, res, 404, "Appointment not found.");
            }
            var response = {};
            response['data'] = rows;
            response['code'] = 200;
            return res.send(response);
        });
    } else {
        return apiSendError(req, res, 422, "refer parameters");
    }
};

exports.getAllParentsAppointments = function(req, res) {
    if (req.body.parent_id !== 'undefined' && req.body.parent_id) {
        var query_str = `SELECT t1.*,CONCAT(t2.first_name,t2.last_name) as staff_name,
        CONCAT(t3.first_name,' ',t3.last_name) as student_name,t5.full_name
        FROM appointments as t1
        LEFT JOIN staff as t2 on t1.staff_id = t2.id
        LEFT JOIN admissions as t3 on t1.student_id = t3.id
        LEFT JOIN parents as t5 on t1.parent_id = t5.id
        LEFT JOIN schools as t4 on t3.school_id = t4.id
        WHERE t1.parent_id = ?
        ORDER BY t1.datetime DESC`;
        db.driver.execQuery(query_str, [req.body.parent_id], function(err, rows) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 400, "Bad Request check your parameters");
            }
            if (!rows.length) {
                return apiSendError(req, res, 404, "Appointment not found.");
            }
            var response = {};
            response['data'] = rows;
            response['code'] = 200;
            return res.send(response);
        });
    } else {
        return apiSendError(req, res, 422, "Bad Request check your parameters");
    }
};

exports.changeStatus = function(req, res) {
    if (req.body.id !== 'undefined' && req.body.id) {
        Appointments.find({
            id: req.body.id
        }, 1, function(err, appointment) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 400, "Bad Request check your parameters");
            }
            if (!appointment.length) {
                return apiSendError(req, res, 404, "Appointment not found.");
            }
            appointment[0].status = req.body.status;
            appointment[0].save(function(err, updated_status) {
                if (err) {
                    console.log(err);
                    return apiSendError(req, res, 404, "Appointment not found.");
                }
                var response = {};
                response['data'] = [];
                response['code'] = 200;
                return res.send(response);
            });
        });
    } else {
        return apiSendError(req, res, 422, "refer parameters");
    }
};

exports.changeStudentAttend = function(req, res) {
    if (req.body.id !== 'undefined' && req.body.id) {
        Appointments.find({
            id: req.body.id
        }, 1, function(err, appointment) {
            if (err) {
                console.log(err);
                return apiSendError(req, res, 400, "Bad Request check your parameters");
            }
            if (!appointment.length) {
                return apiSendError(req, res, 404, "Appointment not found.");
            }
            appointment[0].student_attend = req.body.student_attend;
            appointment[0].save(function(err, updated_student_attend) {
                if (err) {
                    console.log(err);
                    return apiSendError(req, res, 404, "Appointment not found.");
                }
                var response = {};
                response['data'] = [];
                response['code'] = 200;
                return res.send(response);
            });
        });
    } else {
        return apiSendError(req, res, 422, "refer parameters");
    }
};

exports.getSchoolStudents = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id === null) {
        return apiSendError(req, res, 422, "refer parameters");
    }
    var query_str = `SELECT t1.*,t2.school_name,CONCAT(t3.first_name ,' ',t3.last_name) as username,
                     t3.photo,t4.grade,t5.session_name
                     FROM admissions as t1
                     LEFT JOIN schools as t2 ON t1.school_id = t2.id
                     LEFT JOIN staff as t3 ON t1.staff_id = t3.id
                     LEFT JOIN grades as t4 ON t1.grade_id = t4.id
                     LEFT JOIN sessions as t5 ON t1.session_id = t5.id
                     WHERE t1.school_id = ? ORDER BY t1.created_at DESC`;
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!rows.length) {
            return apiSendError(req, res, 404, "Appointment not found.");
        }
        var response = {};
        response['data'] = rows;
        response['code'] = 200;
        return res.send(response);
    });
};

exports.getSchoolParents = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || typeof req.body.school_id === null) {
        return apiSendError(req, res, 422, "id required");
    }
    var query_str = `SELECT t1.* FROM parents as t1 LEFT JOIN admissions as t2 ON t1.student_id = t2.id LEFT JOIN schools as t3 ON t2.school_id = t3.id WHERE t2.school_id = ? ORDER BY t1.created_at DESC`;
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!rows.length) {
            return apiSendError(req, res, 404, "Appointment not found.");
        }
        var response = {};
        response['data'] = rows;
        response['code'] = 200;
        return res.send(response);
    });
};

exports.getAllAppointments = function(req, res) {
    var query_str = `SELECT t1.*, CONCAT(t2.first_name,t2.last_name) as staff_name,
                       CONCAT(t3.first_name,' ',t3.last_name) as student_name,t5.full_name as parent_name
                       FROM appointments as t1
                       LEFT JOIN staff as t2 on t1.staff_id = t2.id
                       LEFT JOIN admissions as t3 on t1.student_id = t3.id
                       LEFT JOIN parents as t5 on t1.parent_id = t5.id
                       ORDER BY t1.datetime DESC`;
    db.driver.execQuery(query_str, [req.body.id], function(err, rows) {
        if (err) {
            console.log(err);
            return apiSendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!rows.length) {
            return apiSendError(req, res, 404, "Appointment not found.");
        }
        var response = {};
        response['data'] = rows;
        response['code'] = 200;
        return res.send(response);
    });
};
