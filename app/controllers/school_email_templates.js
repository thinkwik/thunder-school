var bCrypt = require('bcrypt-nodejs');
var db = require('orm').db,
    School_Email_Templates = db.models.school_email_templates;

exports.register = function(req, res) {
    new_template = new School_Email_Templates();
    new_template.ui_key = req.body.ui_key;
    new_template.school_id = req.body.school_id;
    new_template.ui_data = req.body.ui_data;
    new_template.save(function(err, saved_template) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(saved_template);
    });
};

exports.getTemplate = function(req, res) {
    if (typeof req.query.id == 'undefined') {
        return sendError(req, res, 422, 'Template id required');
    }
    School_Email_Templates.find({
        id: req.query.id
    }, function(err, templates) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!templates.length) {
            return sendError(req, res, 404, "Templates not found");
        }
        return res.send(templates[0]);
    });
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'Template id required');
    }
    School_Email_Templates.find({
        id: req.body.id
    }, 1, function(err, templates) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!templates.length) {
            return sendError(req, res, 404, "Templates not found");
        }
        templates[0].ui_key = req.body.ui_key;
        templates[0].ui_data = req.body.ui_data;
        templates[0].save(function(err, updated_template) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "something went wrong in saving template");
            }
            return res.send(templates);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'Template id required');
    }
    School_Email_Templates.find({
        id: req.body.id
    }, 1, function(err, templates) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!templates.length) {
            return sendError(req, res, 404, "Templates not found");
        }
        templates[0].remove(function(err) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "Something went wrong in deleting template!Please try again");
            }
            return res.send(templates[0]);
        });
    });
};

exports.getAllTemplate = function(req, res) {
    School_Email_Templates.find({}, function(err, allTemplates) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        return res.send(allTemplates);
    });
};

exports.getAllSchoolTemplate = function(req, res) {
    if (typeof req.body.school_id == 'undefined') {
        return sendError(req, res, 422, 'School id required');
    }
    School_Email_Templates.find({school_id:req.body.school_id}, function(err, allTemplates) {
        if (err) {
            console.log(err);
            return sendError(req, res, 500, 'Something went wrong, please try again!');
        }
        if(allTemplates.length == 0){
            register_all_templetes(req.body.school_id,function(err,success){
                if(err){
                    return sendError(req, res, 500, 'Something went wrong, please try again!');
                }
                register_all_scale(req.body.school_id,function(err,success){
                    if(err){
                        return sendError(req, res, 500, 'Something went wrong, please try again!');
                    }
                    School_Email_Templates.find({school_id:req.body.school_id},function(error,data){
                        return res.send(data);
                    });
                });
            });
        } else {
            return res.send(allTemplates);
        }
    });
};

register_all_templetes = function(school_id,cb){
    var temp_str = "Hello <<STUDENT_FIRST_NAME>> Thunder School Exam Email Notification.";
    var query_str = `INSERT INTO school_email_templates(school_id, ui_key, ui_data) 
                        VALUES ('`+ school_id +`' , '4', '`+ temp_str +`'),
                         ('`+ school_id +`' , '3.7', '`+ temp_str +`'),
                         ('`+ school_id +`' , '3.3', '`+ temp_str +`'),
                         ('`+ school_id +`' , '3', '`+ temp_str +`'),
                         ('`+ school_id +`' , '2.7', '`+ temp_str +`'),
                         ('`+ school_id +`' , '2.3', '`+ temp_str +`'),
                         ('`+ school_id +`' , '2', '`+ temp_str +`'),
                         ('`+ school_id +`' , '1.7', '`+ temp_str +`'),
                         ('`+ school_id +`' , '1.3', '`+ temp_str +`'),
                         ('`+ school_id +`' , '1', '`+ temp_str +`'),
                         ('`+ school_id +`' , '0', '`+ temp_str +`');
                `;
    db.driver.execQuery(query_str, [], function(err, results) {
        if (err) {
            console.log(err);
            cb(true,null);
        }
        cb(null,true);
    });
};

register_all_scale = function(school_id,cb){
    var query_str = `INSERT INTO school_grade_scale(school_id, from_per, to_per, scale) 
                    VALUES ('`+ school_id+`','97','100','4'),
                    ('`+ school_id+`','93','96','4'),
                    ('`+ school_id+`','90','92','3.7'),
                    ('`+ school_id+`','87','89','3.3'),
                    ('`+ school_id+`','83','86','3'),
                    ('`+ school_id+`','80','82','2.7'),
                    ('`+ school_id+`','77','79','2.3'),
                    ('`+ school_id+`','73','76','2'),
                    ('`+ school_id+`','70','72','1.7'),
                    ('`+ school_id+`','67','69','1.3'),
                    ('`+ school_id+`','65','66','1'),
                    ('`+ school_id+`','0','65','0');
                    `;
    db.driver.execQuery(query_str, [], function(err, results) {
        if (err) {
            console.log(err);
            cb(true,null);
        }
        cb(null,true);
    });
};



exports.getAllSchoolGradeScale = function(req, res){
    if (typeof req.body.school_id == 'undefined') {
        return sendError(req, res, 422, 'School id required');
    }
    var query_str = `SELECT * FROM  school_grade_scale WHERE school_id = ?`;
    db.driver.execQuery(query_str, [req.body.school_id], function(err, results) {
        if (err) {
            console.log(err);
            return sendError(req, res, 404, "Something Went wrong!");
        }
        return res.send(results);
    });
};

exports.getEditGradeScale = function(req, res){
    if (typeof req.query.id == 'undefined') {
        return sendError(req, res, 422, 'Grade Scale id required');
    }
    var query_str = `SELECT * FROM  school_grade_scale WHERE id = ?`;
    db.driver.execQuery(query_str, [req.query.id], function(err, results) {
        if (err) {
            console.log(err);
            return sendError(req, res, 404, "Something Went wrong!");
        }
        return res.send(results[0]);
    });
};

exports.EditGradeScale = function(req, res){
    if (typeof req.body.id == 'undefined' || typeof req.body.from_per == 'undefined' || typeof req.body.to_per == 'undefined' || typeof req.body.scale == 'undefined') {
        return sendError(req, res, 422, 'Grade Scale id required');
    }
    var query_str = `UPDATE school_grade_scale SET from_per = ?, to_per = ? , scale = ?  WHERE  id = ? `;
    db.driver.execQuery(query_str, [req.body.from_per,req.body.to_per,req.body.scale, req.body.id], function(err, saved_data) {
        if (err) {
            console.log(err);
            return sendError(req, res, 404, "Something Went wrong!");
        }
        return res.send(saved_data);
    });
};














// var query_str = 'SELECT t1.*,t2.grade,CONCAT(t3.first_name,t3.last_name) as staff_name,t6.subject_name FROM attendance as t1 LEFT JOIN grades as t2 ON t1.grade_id = t2.id LEFT JOIN subjects as t6 ON t1.subject_id = t6.id LEFT JOIN staff as t3 ON t1.staff_id = t3.id WHERE t3.id = ?';
// db.driver.execQuery(query_str, [req.body.staff_id], function(err, rows) {
//     if (err) {
//         console.log(err);
//         return sendError(req, res, 400, "Bad Request check your parameters");
//     }
//     return res.send(rows);
// });
