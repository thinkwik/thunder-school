app.controller('createAttendanceController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        $scope.getGrades = function() {
            var data = {
                school_id: $cookieStore.get('loggedInStaff').school_id
            };
            $http({
                method: 'POST',
                url: '/grade/getAllSchoolGrades',
                data: data
            }).then(function successCallback(response) {
                $scope.grades = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Grades', 'Something went wrong!');
            });
        };

        $scope.getSubject = function(grade_id) {
            $scope.attendance.subject_id = "";
            var data = {
                grade_id: grade_id,
                school_id: $cookieStore.get('loggedInStaff').school_id
            };
            $http({
                method: 'POST',
                url: '/subject/getSchoolGradesSubjectWise',
                data: data
            }).then(function successCallback(response) {
                $scope.subjects = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Subjects', 'Something went wrong!');
            });
        };

        $scope.getClass = function(subject_id) {
            $scope.attendance.class_id = "";
            var data = {
                subject_id: subject_id
            };
            $http({
                method: 'POST',
                url: '/class/getAllSubjectClasses',
                data: data
            }).then(function successCallback(response) {
                $scope.classes = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Subjects', 'Something went wrong!');
            });
        };

        $scope.listAttendance = function() {
            $state.go('schoolstaff.list_attendance');
        };

        $scope.createAttendance = function() {
            data = $scope.attendance;
            data.school_id = $cookieStore.get('loggedInStaff').school_id;
            data.staff_id = $cookieStore.get('loggedInStaff').id;
            $http({
                method: "POST",
                url: "/attendance/register",
                data: data
            }).then(function successCallback(response) {
                $state.go('schoolstaff.list_attendance');
                toaster.pop('success', 'Attendance', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Attendance', error.data.message);
            });
        };
    }
]);

app.controller('editAttendanceController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        $scope.getGrades = function() {
            var data = {
                school_id: $cookieStore.get('loggedInStaff').school_id
            };
            $http({
                method: 'POST',
                url: '/grade/getAllSchoolGrades',
                data: data
            }).then(function successCallback(response) {
                $scope.grades = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Grades', 'Something went wrong!');
            });
        };

        $scope.getSubject = function(grade_id) {
            var data = {
                grade_id: grade_id,
                school_id: $cookieStore.get('loggedInStaff').school_id
            };
            $http({
                method: 'POST',
                url: '/subject/getSchoolGradesSubjectWise',
                data: data
            }).then(function successCallback(response) {
                $scope.subjects = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Subjects', 'Something went wrong!');
            });
        };

        $scope.getClass = function(subject_id) {
            var data = {
                subject_id: subject_id
            };
            $http({
                method: 'POST',
                url: '/class/getAllSubjectClasses',
                data: data
            }).then(function successCallback(response) {
                $scope.classes = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Subjects', 'Something went wrong!');
            });
        };

        $scope.listAttendance = function() {
            $state.go('schoolstaff.list_attendance');
        };

        $scope.getEditAttendance = function(attendance_id){
            var data = {
                id: $state.params.attendance_id
            };
            $http({
                method: "POST",
                url: "/attendance/getAttendance",
                data: data
            }).then(function successCallback(response) {
                $scope.getGrades(response.data.grade_id);
                $scope.getSubject(response.data.grade_id);
                $scope.getClass(response.data.subject_id);
                $scope.attendance = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Attendance', error.data.message);
            });
        };

        $scope.saveEditAttendance = function() {
            data = $scope.attendance;
            $http({
                method: "POST",
                url: "/attendance/edit",
                data: data
            }).then(function successCallback(response) {
                $state.go('schoolstaff.list_attendance');
                toaster.pop('success', 'Attendance', 'Updated Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Attendance', error.data.message);
            });
        };
    }
]);

app.controller('listAttendanceController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.addNewAttendane = function() {
            $state.go('schoolstaff.create_attendance');
        };

        $scope.attendanceReport = function(attendance_id) {
            $state.go('schoolstaff.attendance_report_list', {
                attendance_id: attendance_id
            });
        };

        $scope.deleteAttendance = function(attendance_id) {
            var data = {
                id: attendance_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/attendance/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllAttendance();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Attendance', error.data.message);
                });
            }
        };

        $scope.getAllAttendance = function() {
            var data = {
                staff_id: $cookieStore.get('loggedInStaff').id
            };
            $http({
                method: 'POST',
                url: '/attendance/getAttendanceStaffwise',
                data: data
            }).then(function successCallback(response) {
                var attendances = response.data;
                $scope.attendances = attendances;
            }, function errorCallback(error) {
                toaster.pop('error', 'Attendance', 'cannot get all the Attendance! try reloading page.');
            });
        };

        $scope.editAttendance = function(attendance_id) {
            $state.go('schoolstaff.edit_attendance', {
                attendance_id: attendance_id
            });
        };
    }
]);


app.controller('listAttendanceReportController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', '$window',
    function($scope, $http, $state, toaster, $cookieStore, $window) {
        $scope.getAttendaceRequiredData = function() {
            $scope.getAttendance();
            $scope.getAllStudentList();
        };

        $scope.makeAttendAll = function() {
            var data = {
                id: $state.params.attendance_id
            };
            $http({
                method: 'POST',
                url: '/attendance_report/makeAttendAll',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllStudentList();
            }, function errorCallback(error) {
                toaster.pop('error', 'Reports', 'Something went wrong!');
            });
        };

        $scope.makeNotAttendAll = function() {
            var data = {
                id: $state.params.attendance_id
            };
            $http({
                method: 'POST',
                url: '/attendance_report/makeNotAttendAll',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllStudentList();
            }, function errorCallback(error) {
                toaster.pop('error', 'Reports', 'Something went wrong!');
            });
        };

        $scope.getAttendance = function() {
            var data = {
                id: $state.params.attendance_id
            };
            $http({
                method: 'POST',
                url: '/attendance/getAttendance',
                data: data
            }).then(function successCallback(response) {
                $scope.attendance = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Reports', 'Something went wrong!');
            });
        };

        $scope.getAllStudentList = function() {
            var data = {
                attendance_id: $state.params.attendance_id
            };
            $http({
                method: 'POST',
                url: '/attendance_report/getAllStudentList',
                data: data
            }).then(function successCallback(response) {
                $scope.attendance_report_lists = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Reports', 'Something went wrong!');
            });
        };

        $scope.studentAttend = function(report_id) {
            var data = {
                id: report_id
            };
            $http({
                method: 'POST',
                url: '/attendance_report/attend',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllStudentList();
            }, function errorCallback(error) {
                toaster.pop('error', 'Reports', 'Something went wrong!');
            });
        };

        $scope.studentNotAttend = function(report_id) {
            var data = {
                id: report_id
            };
            $http({
                method: 'POST',
                url: '/attendance_report/notAttend',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllStudentList();
            }, function errorCallback(error) {
                toaster.pop('error', 'Reports', 'Something went wrong!');
            });
        };

        $scope.listAttendance = function() {
            $state.go('schoolstaff.list_attendance');
        };

    }
]);
