  app.controller('createStaffBookStoreController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore',
      function($scope, $http, $state, toaster, Upload, $cookieStore) {
          $scope.getAllSchoolBookCategory = function() {
              data = {
                asd: "asd"
              };
              $http({
                  method: 'POST',
                  url: '/book_category/getAllSchoolBookCategory',
                  data: data
              }).then(function successCallback(response) {
                  $scope.categories = response.data;
              }, function errorCallback(error) {
                  toaster.pop('error', "Book Category", "something went wrong");
              });
          };
          $scope.getAllSchoolBookCategory();
          $scope.addBook = function() {
              data = $scope.book;
              if(!$scope.bookData){
                toaster.pop('error', "Book Store", "Please select book!");
                return false;
              }
              if(!$scope.image){
                toaster.pop('error', "Book Store", "Please select book image!");
                return false;
              }
              data.image = $scope.image;
              data.book = $scope.bookData;
              $http({
                  method: 'POST',
                  url: '/bookstore/register',
                  data: data
              }).then(function successCallback(response) {
                  $state.go('schoolstaff.list_book_store');
                  toaster.pop('success', "Book Store", "Book added successfully!");
              }, function errorCallback(error) {
                  toaster.pop('error', "Book Store", "Something went wrong!!!");
              });
          };

          $scope.uploadBook = function(file) {
              if (!file) {
                  return false;
              } else {
                  Loading(true);
                  Upload.upload({
                      url: '/bookstore/uploadBook',
                      data: {
                          file: file
                      }
                  }).then(function successCallback(success) {
                      Loading(false);
                      $scope.bookData = success.data.newBookName;
                      $scope.book.publisher_name = success.data.publisher;
                      $scope.book.title = success.data.title;
                      $scope.book.author_name = success.data.creator;
                      $scope.book.description = success.data.description;
                      $scope.saveDisable = true;
                  }, function errorCallback(error) {
                      Loading(false);
                      toaster.pop('error', 'Book Store', error.data.message);
                  });
              }
          };

          $scope.uploadFile = function(file) {
              if (!file) {
                  return false;
              } else {
                  Loading(true);
                  Upload.upload({
                      url: '/bookstore/upload',
                      data: {
                          file: file
                      }
                  }).then(function successCallback(success) {
                      Loading(false);
                      $scope.image = success.data;
                      $scope.saveDisable = true;
                  }, function errorCallback(error) {
                      Loading(false);
                      toaster.pop('error', 'Book Store', error.data.message);
                  });
              }
          };

          $scope.listBookStore = function() {
              $state.go('schoolstaff.list_book_store');
          };
      }
  ]);

  app.controller('listStaffBookStoreController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore', '$window',
      function($scope, $http, $state, toaster, Upload, $cookieStore, $window) {
          $scope.listBookStore = function() {
              $http({
                  method: 'POST',
                  url: '/bookstore/getAllBooks'
              }).then(function successCallback(response) {
                  var bookstore = response.data;
                  $scope.bookstore = bookstore;
              }, function errorCallback(error) {
                  toaster.pop('error', 'Book Store', 'cannot get all the books! try reloading page.');
              });
          };

          $scope.downloadBook = function(book) {
              if (book.book) {
                  var bookName = book.book;
                  bookName = bookName.split(".");
                  var url = $window.location.origin + '/upload/books/' + book.book;
                  var request = new XMLHttpRequest();
                  request.open('HEAD', url, false);
                  request.send();
                  if (request.status == 200) {
                      var element = angular.element('<a/>');
                      element.attr({
                          href: url,
                          target: '_blank',
                          download: book.title + '.' + bookName[1]
                      })[0].click();
                  } else {
                      toaster.pop('error', 'Homework', 'Homework not found!');
                      return false;
                  }
              } else {
                  toaster.pop('error', 'Homework', 'Homework not found!');
                  return false;
              }
          };


          $scope.notAvailableBook = function(book_id) {
              var data = {
                  id: book_id
              };
              $http({
                  method: 'POST',
                  url: '/bookstore/notAvailable',
                  data: data
              }).then(function successCallback(response) {
                  $scope.listBookStore();
              }, function errorCallback(error) {
                  toaster.pop('error', 'Book Store', 'cannot get all the books! try reloading page.');
              });
          };

          $scope.availableBook = function(book_id) {
              var data = {
                  id: book_id
              };
              $http({
                  method: 'POST',
                  url: '/bookstore/available',
                  data: data
              }).then(function successCallback(response) {
                  $scope.listBookStore();
              }, function errorCallback(error) {
                  toaster.pop('error', 'Book Store', 'cannot get all the books! try reloading page.');
              });
          };


          $scope.deleteBook = function(book_id) {
              var data = {
                  id: book_id
              };
              var result = confirm("Please confirm, Do you really want to delete ?");
              if (result) {
                  $http({
                      method: 'POST',
                      url: '/bookstore/delete',
                      data: data
                  }).then(function successCallback(response) {
                      $scope.listBookStore();
                      toaster.pop('success', 'Book Store', 'Book remved successfully.');
                  }, function errorCallback(error) {
                      toaster.pop('error', 'Book Store', 'Something went wrong.');
                  });
              }
          };

          $scope.editBook = function(book_id) {
              $state.go('schoolstaff.edit_book_store', {
                  book_id: book_id
              });
          };

          $scope.addNewBook = function() {
              $state.go('schoolstaff.create_book_store');
          };

          $scope.viewBook = function(book) {
              var book_id = book.id;
              var string = book.book;
              $window.localStorage.setItem('bookname', string);
              string = string.split('.');
              if (string[1] == 'pdf') {
                  $state.go('schoolstaff.view_book', {
                      book_id: book_id
                  });
              } else {
                  $state.go('schoolstaff.view_epub_book', {
                      book_id: book_id
                  });
              }
          };
      }
  ]);

  app.controller('editStaffBookStoreController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore',
      function($scope, $http, $state, toaster, Upload, $cookieStore) {
          $scope.listBookStore = function() {
              $state.go('schoolstaff.list_book_store');
          };
          $scope.getAllSchoolBookCategory = function() {
              data = {
                asd: "asd"
              };
              $http({
                  method: 'POST',
                  url: '/book_category/getAllSchoolBookCategory',
                  data: data
              }).then(function successCallback(response) {
                  $scope.categories = response.data;
              }, function errorCallback(error) {
                  toaster.pop('error', "Book Category", "something went wrong");
              });
          };
          $scope.saveUpdatedBook = function() {
              var data = $scope.book;
              if ($scope.image) {
                  data.image = $scope.image;
              };
              if ($scope.book) {
                  data.book = $scope.bookData;
              }
              $http({
                  method: 'POST',
                  url: '/bookstore/edit',
                  data: data
              }).then(function successCallback(response) {
                  $state.go('schoolstaff.list_book_store');
                  toaster.pop('success', 'Book Store', 'BookStore updated successfully.');
              }, function errorCallback(error) {
                  toaster.pop('error', 'Book Store', error.data.message);
              });
          };

          $scope.uploadBook = function(file) {
              if (!file) {
                  return toaster.pop('error', 'File', 'No file selected !');
              } else {
                  Loading(true);
                  Upload.upload({
                      url: '/bookstore/uploadBook',
                      data: {
                          file: file
                      }
                  }).then(function successCallback(success) {
                      Loading(false);
                      $scope.bookData = success.data;
                      $scope.saveDisable = true;
                  }, function errorCallback(error) {
                      toaster.pop('error', 'Book Store', 'cannot get all the books! try reloading page.');
                  });
              }
          };

          $scope.uploadFile = function(file) {
              if (!file) {
                  return toaster.pop('error', 'File', 'No file selected !');
              } else {
                  Loading(true);
                  Upload.upload({
                      url: '/bookstore/upload',
                      data: {
                          file: file
                      }
                  }).then(function successCallback(success) {
                      Loading(false);
                      $scope.image = success.data;
                      $scope.saveDisable = true;
                  }, function errorCallback(error) {
                      toaster.pop('error', 'Book Store', 'cannot get all the books! try reloading page.');
                  });
              }
          };

          $scope.getEditBook = function() {
              $scope.getAllSchoolBookCategory();
              $http({
                  method: 'GET',
                  url: '/bookstore/edit?id=' + $state.params.book_id,
              }).then(function successCallback(response) {
                  $scope.book = response.data;
              }, function errorCallback(response) {
                  toaster.pop('error', 'Book Store', error.data.message);
              });
          };

      }
  ]);

  app.controller('listStudentBooksController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore', '$window',
      function($scope, $http, $state, toaster, Upload, $cookieStore, $window) {
          $scope.listBookStore = function() {
              $http({
                  method: 'POST',
                  url: '/bookstore/getAllAvailBooks'
              }).then(function successCallback(response) {
                  var bookstore = response.data;
                  $scope.bookstore = bookstore;
              }, function errorCallback(error) {
                  toaster.pop('error', 'Book Store', 'cannot get all the books! try reloading page.');
              });
          };
          $scope.downloadBook = function(book) {
              if (book.book) {
                  var bookName = book.book;
                  bookName = bookName.split(".");
                  var url = $window.location.origin + '/upload/books/' + book.book;
                  var request = new XMLHttpRequest();
                  request.open('HEAD', url, false);
                  request.send();
                  if (request.status == 200) {
                      var element = angular.element('<a/>');
                      element.attr({
                          href: url,
                          target: '_blank',
                          download: book.title + '.' + bookName[1]
                      })[0].click();
                  } else {
                      toaster.pop('error', 'Homework', 'Homework not found!');
                      return false;
                  }
              } else {
                  toaster.pop('error', 'Homework', 'Homework not found!');
                  return false;
              }
          };

          $scope.viewBook = function(book) {
              var book_id = book.id;
              var string = book.book;
              $window.localStorage.setItem('bookname', string);
              string = string.split('.');
              if (string[1] == 'pdf') {
                  $state.go('students.view_books', {
                      book_id: book_id
                  });
              } else {
                  $state.go('students.view_epub_book', {
                      book_id: book_id
                  });
              }
          };
      }
  ]);

  app.controller('viewStudentBooksController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore', '$window',
      function($scope, $http, $state, toaster, Upload, $cookieStore, $window) {
          $scope.getEditBook = function() {
              $http({
                  method: 'GET',
                  url: '/bookstore/edit?id=' + $state.params.book_id,
              }).then(function successCallback(response) {
                  $scope.book = '/upload/books/' + response.data.book;
              }, function errorCallback(error) {
                  toaster.pop('error', 'Book Store', error.data.message);
              });
          };

          $scope.listBooks = function() {
              $state.go('students.list_books');
          }
      }
  ]);

  app.controller('viewStudentEpubBookStoreController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore', '$window','$timeout',
      function($scope, $http, $state, toaster, Upload, $cookieStore, $window,$timeout) {
          $scope.getEditBook = function() {
              $http({
                  method: 'GET',
                  url: '/bookstore/edit?id=' + $state.params.book_id,
              }).then(function successCallback(response) {
                  $scope.book = '/upload/books/' + response.data.book;
                  $timeout(function() {
                      $scope.Book = ePub('upload/books/' + localStorage.bookname, {
                          width: 400,
                          height: 600,
                          spreads : false
                      });
                      $scope.Book.renderTo("area").then(function() {});
                  }, 500);
              }, function errorCallback(error) {
                  toaster.pop('error', 'Book Store', error.data.message);
              });
          };

          $scope.listBooks = function() {
              $state.go('students.view_epub_book');
          }
      }
  ]);

  app.controller('listParentsBooksController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore', '$window',
      function($scope, $http, $state, toaster, Upload, $cookieStore, $window) {
          $scope.listBookStore = function() {
              $http({
                  method: 'POST',
                  url: '/bookstore/getAllAvailBooks'
              }).then(function successCallback(response) {
                  var bookstore = response.data;
                  $scope.bookstore = bookstore;
              }, function errorCallback(error) {
                  toaster.pop('error', 'Book Store', 'cannot get all the books! try reloading page.');
              });
          };

          $scope.downloadBook = function(book) {
              if (book.book) {
                  var bookName = book.book;
                  bookName = bookName.split(".");
                  var url = $window.location.origin + '/upload/books/' + book.book;
                  var request = new XMLHttpRequest();
                  request.open('HEAD', url, false);
                  request.send();
                  if (request.status == 200) {
                      var element = angular.element('<a/>');
                      element.attr({
                          href: url,
                          target: '_blank',
                          download: book.title + '.' + bookName[1]
                      })[0].click();
                  } else {
                      toaster.pop('error', 'Homework', 'Homework not found!');
                      return false;
                  }
              } else {
                  toaster.pop('error', 'Homework', 'Homework not found!');
                  return false;
              }
          };



          $scope.viewBook = function(book) {
              var book_id = book.id;
              var string = book.book;
              $window.localStorage.setItem('bookname', string);
              string = string.split('.');
              if (string[1] == 'pdf') {
                  $state.go('parents.view_books', {
                      book_id: book_id
                  });
              } else {
                  $state.go('parents.view_epub_book', {
                      book_id: book_id
                  });
              }
          };

      }
  ]);

  app.controller('viewParentsBooksController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore', '$window',
      function($scope, $http, $state, toaster, Upload, $cookieStore, $window) {
          $scope.getEditBook = function() {
              $http({
                  method: 'GET',
                  url: '/bookstore/edit?id=' + $state.params.book_id,
              }).then(function successCallback(response) {
                  $scope.book = '/upload/books/' + response.data.book;
              }, function errorCallback(error) {
                  toaster.pop('error', 'Book Store', error.data.message);
              });
          };

          $scope.listBooks = function() {
              $state.go('parents.list_books');
          }
      }
  ]);


  app.controller('viewParentEpubBookStoreController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore', '$window','$timeout',
      function($scope, $http, $state, toaster, Upload, $cookieStore, $window, $timeout) {
          $scope.getEditBook = function() {
              $http({
                  method: 'GET',
                  url: '/bookstore/edit?id=' + $state.params.book_id,
              }).then(function successCallback(response) {
                  $scope.book = '/upload/books/' + response.data.book;
                  $timeout(function() {
                      $scope.Book = ePub('upload/books/' + localStorage.bookname, {
                          width: 400,
                          height: 600,
                          spreads: false
                      });
                      $scope.Book.renderTo("area").then(function() {});
                  }, 500);
              }, function errorCallback(error) {
                  toaster.pop('error', 'Book Store', error.data.message);
              });
          };

          $scope.listBooks = function() {
              $state.go('parents.list_books');
          }
      }
  ]);



  app.controller('viewStaffBookStoreController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore', '$window',
      function($scope, $http, $state, toaster, Upload, $cookieStore, $window) {
          $scope.getEditBook = function() {
              $http({
                  method: 'GET',
                  url: '/bookstore/edit?id=' + $state.params.book_id,
              }).then(function successCallback(response) {
                  $scope.book = '/upload/books/' + response.data.book;

              }, function errorCallback(error) {
                  toaster.pop('error', 'Book Store', error.data.message);
              });
          };

          $scope.listBooks = function() {
              $state.go('schoolstaff.list_book_store');
          }
      }
  ]);



  app.controller('viewStaffEpubBookStoreController', ['$scope', '$http', '$state', 'toaster', 'Upload', '$cookieStore', '$window','$timeout',
      function($scope, $http, $state, toaster, Upload, $cookieStore, $window,$timeout) {
          $scope.getEditBook = function() {
              $http({
                  method: 'GET',
                  url: '/bookstore/edit?id=' + $state.params.book_id,
              }).then(function successCallback(response) {
                  $scope.book = '/upload/books/' + response.data.book;
                  $timeout(function() {
                      $scope.Book = ePub('upload/books/' + localStorage.bookname, {
                          width: 1000,
                          height: 600,
                          spreads: false
                      });
                      $scope.Book.renderTo("area").then(function() {});
                  }, 500);
              }, function errorCallback(error) {
                  toaster.pop('error', 'Book Store', error.data.message);
              });
          };

          $scope.listBooks = function() {
              $state.go('schoolstaff.list_book_store');
          };
      }
  ]);
