var db = require('orm').db,
    BookCategory = db.models.bookcategory;

exports.register = function(req, res) {
    var query_str = "SELECT * FROM bookcategory WHERE category_name = ?";
    db.driver.execQuery(query_str, [req.body.isbn_number],function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (rows.length) {
            return sendError(req, res, 422, "Category Already Exists.");
        }
        if (!rows.length) {
            newBookC = new BookCategory();
            newBookC.category_name = req.body.category_name;
            newBookC.save(function(error, added_book_category) {
                if (error) {
                    console.log(error);
                    return sendError(req, res, 422, 'Please check your parameters.');
                }
                return res.send(added_book_category);
            });
        }
    });
};

exports.getAllSchoolBookCategory = function(req, res) {
    var query_str = "SELECT * FROM bookcategory WHERE 1";
    db.driver.execQuery(query_str,function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'category id required');
    }
    BookCategory.find({
        id: req.body.id
    }, 1, function(err, categories) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!categories.length) {
            return sendError(req, res, 404, "Event not found");
        }
        categories[0].remove(function(err) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "Something went wrong in deleting event");
            }
            return res.send(categories[0]);
        });
    });
};