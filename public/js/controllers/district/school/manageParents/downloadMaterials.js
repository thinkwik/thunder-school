app.config(['$compileProvider', function($compileProvider) {
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(|blob|):/);
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|file|blob|pdf|doc):/);
}]);

app.controller('downloadParentMaterialController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', '$window',
    function($scope, $http, $state, toaster, $cookieStore, $window) {
        $scope.getAllMaterials = function() {
            var data = {
                grade_id: $cookieStore.get('loggedInParent').grade_id
            };
            $http({
                method: 'POST',
                url: '/material/getStudentMaterials',
                data: data
            }).then(function successCallback(response) {
                var materials = response.data;
                $scope.materials = materials;
            }, function errorCallback(error) {
                toaster.pop('error', 'Materials', 'cannot get all the Materials! try reloading page.');
                console.log(error.data.message);
            });
        };

        $scope.DownloadMaterial = function(material) {
            if(material.file_name) {
                var materialFileName = material.file_name;
                materialFileName = materialFileName.split(".");
                var url =  $window.location.origin + '/upload/materials/' + material.file_name;
                var request = new XMLHttpRequest();
                request.open('HEAD', url, false);
                request.send();
                if(request.status == 200) {
                  var element = angular.element('<a/>');
                  element.attr({
                      href: url,
                      target: '_blank',
                      download: material.title + '.' + materialFileName[1]
                  })[0].click();
                } else {
                  toaster.pop('error', 'Materials', 'Material not found!');
                  return false;
                }
            } else {
              toaster.pop('error', 'Materials', 'Material not found!');
              return false;
            }
        };

    }
]);
