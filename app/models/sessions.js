var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Sessions = db.define('sessions', {
    school_id: {
        type: 'integer'
    },
    created_by: {
        type: 'integer'
    },
    session_name: String,
    start_date: String,
    end_date: String,
    status: ['0', '1']
}, {
    timestamp: true,
    methods: {

    }
});
