var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Gallery = db.define('gallery', {
    gallery_title: String,
    school_id: {
        type: 'integer'
    },
    event_id: {
        type: 'integer'
    },
    images: String,
    status: ['0', '1']
}, {
    timestamp: true,
    methods: {

    }
});
