var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Lectures = db.define('lectures', {
    school_id: {
        type: 'integer'
    },
    session_id: {
        type: 'integer'
    },
    staff_id: {
        type: 'integer'
    },
    grade_id: {
        type: 'integer'
    },
    subject_id: {
        type: 'integer'
    },
    department_id: {
        type: 'integer'
    },
    location_id: {
        type: 'integer'
    },
    date: String,
    start_time: String,
    end_time: String,
}, {
    timestamp: true,
    methods: {

    }
});
