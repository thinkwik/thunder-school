var bCrypt = require('bcrypt-nodejs');
var db = require('orm').db,
    User = db.models.users,
    School = db.models.schools;

exports.sendMail = function(req, res) {
    var from = 'binit@thinkwik.com';
    var to = 'binit@thinkwik.com,parth@thinkwik.com';
    var subject = 'Test Mail from Thunder Mail';
    var html = "Hii This is mail from logged in user " + req.user.email + "thanks";
    sendMail(req, res, from, to, subject, html, function(err, success) {
        if (err) {
            console.log(err);
            console.log("something wnet wrong");
            return res.send('error');
        }
        return res.send("success");
    });

};

exports.register = function(req, res) {
    if (req.body.email !== '' && req.body.password !== '') {
        $findUser = {
            email: req.body.email
        };
        User.find($findUser, function(err, users) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            if (users.length > 0) {
                return sendError(req, res, 401, 'school admin allready exists with same email, please enter different email');
            }
            var newUser = new User();
            var name = req.body.ap_name.split(" ");
            if (name.length >= 2) {
                newUser.first_name = name[0];
                newUser.last_name = name[1];
            } else {
                newUser.first_name = name[0];
                newUser.last_name = '';
            }
            newUser.email = req.body.email;
            newUser.password = createHash(req.body.password);
            newUser.mobile = req.body.ap_mobile;
            newUser.user_type = 'school_admin';
            newUser.school_id = 0;
            newUser.role_id = 1;
            newUser.status = "1";
            newUser.save(function(err, new_user) {
                if (err) {
                    console.log('error in user saving');
                    console.log(err);
                    return sendError(req, res, 400, "Bad Request check your parameters");
                }
                var newSchool = new School();
                newSchool.school_name = req.body.school_name;
                newSchool.school_type = req.body.school_type;
                newSchool.address = req.body.address;
                newSchool.country = req.body.country;
                newSchool.state = req.body.state;
                newSchool.city = req.body.city;
                newSchool.phone_number = req.body.phone_number;
                newSchool.office_number = req.body.office_number;
                newSchool.fax_number = req.body.fax_number;
                newSchool.max_students = (typeof req.body.max_students != 'undefined') ? req.body.max_students : 30;
                newSchool.authorized_user_id = new_user.id;
                newSchool.parent_id = (typeof req.body.parent_id != 'undefined' && req.body.parent_id !== null) ? req.body.parent_id : "0";
                newSchool.status = (typeof req.body.status != 'undefined') ? req.body.status : "1";
                newSchool.save(function(err, new_school) {
                    console.log(new_school);
                    if (err) {
                        console.log(err);
                        return sendError(req, res, 400, "Bad Request check your parameters");
                    }
                    User.find({
                        id: new_user.id
                    }, 1, function(err, last_new_user) {
                        console.log(last_new_user);
                        if (err) {
                            console.log(err);
                            return sendError(req, res, 400, "Bad Request check your parameters");
                        }
                        last_new_user[0].school_id = new_school.id;
                        last_new_user[0].save(function(err, success) {
                            console.log(success);
                            if (err) {
                                console.log(err);
                                return sendError(req, res, 400, "Bad Request check your parameters");
                            }
                            return res.send(new_school);
                        });
                    });
                });
            });
        });
    } else {
        return sendError(req, res, 404, 'requested parameter are less then we accept');
    }
};

exports.getSchool = function(req, res) {
    var query_str = "SELECT t1.*, t2.id as user_id, CONCAT(t2.first_name, ' ', t2.last_name) as ap_name, t2.email as email, t2.mobile as ap_mobile FROM `schools` as t1 LEFT JOIN users as t2 ON t1.`authorized_user_id` = t2.id WHERE t1.id = ? ";
    db.driver.execQuery(query_str, [req.query.id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!rows.length) {
            return sendError(req, res, 404, "school not found");
        }
        return res.send(rows[0]);
    });
};

exports.getAllDestricts = function(req, res) {
    var query_str = "SELECT t1.*, t2.id as user_id, CONCAT(t2.first_name, ' ', t2.last_name) as ap_name,t2.profile_image, t2.email as email, t2.mobile as ap_mobile FROM `schools` as t1 LEFT JOIN users as t2 ON t1.`authorized_user_id` = t2.id WHERE t1.school_type = ?";
    db.driver.execQuery(query_str, ['district'], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getAllSchools = function(req, res) {
    var parent_id = 0;
    if (typeof req.body.parent_id != 'undefined' && req.body.parent_id !== null) {
        parent_id = req.body.parent_id;
    }
    var query_str = "SELECT t1.*, t2.id as user_id, CONCAT(t2.first_name, ' ', t2.last_name) as ap_name, t2.email as email, t2.mobile as ap_mobile FROM `schools` as t1 LEFT JOIN users as t2 ON t1.`authorized_user_id` = t2.id WHERE t1.school_type = ? AND t1.parent_id = ? ";
    db.driver.execQuery(query_str, ['school', parent_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getUniversalSchools = function(req, res) {
    var query_str = "SELECT id, school_name  FROM `schools` WHERE school_type = ? ";
    db.driver.execQuery(query_str, ['school'], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getAllSuperAdminSchools = function(req, res) {
    var query_str = "SELECT t1.*, t2.id as user_id, CONCAT(t2.first_name, ' ', t2.last_name) as ap_name, t2.email as email, t2.mobile as ap_mobile, t2.profile_image FROM `schools` as t1 LEFT JOIN users as t2 ON t1.`authorized_user_id` = t2.id WHERE t1.parent_id <> 0 ORDER BY t1.id DESC LIMIT 20 ";
    db.driver.execQuery(query_str, [], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};



exports.getDistrictSchools = function(req, res) {
    if (typeof req.body.district_id == 'undefined' || req.body.district_id === null) {
        return sendError(req, res, 422, "District ID required");
    }
    var query_str = "SELECT id, school_name  FROM `schools` WHERE school_type = ? AND parent_id = ?";
    db.driver.execQuery(query_str, ['school', req.body.district_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.edit = function(req, res) {
    School.find({
        id: req.body.id
    }, 1, function(err, schools) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (schools.length) {
            User.find({
                id: schools[0].authorized_user_id
            }, 1, function(err, users) {
                if (err) {
                    console.log(err);
                    return sendError(req, res, 400, "Bad Request check your parameters");
                }
                if (users.length) {
                    var name = req.body.ap_name.split(" ");
                    if (name.length >= 2) {
                        users[0].first_name = name[0];
                        users[0].last_name = name[1];
                    } else {
                        users[0].first_name = name[0];
                        users[0].last_name = '';
                    }
                    users[0].email = req.body.email;
                    users[0].mobile = req.body.ap_mobile;
                    users[0].save(function(err, saved_user) {
                        if (err) {
                            console.log(err);
                            return sendError(req, res, 400, "Bad Request check your parameters");
                        }
                        schools[0].school_name = req.body.school_name;
                        schools[0].address = req.body.address;
                        schools[0].country = req.body.country;
                        schools[0].state = req.body.state;
                        schools[0].city = req.body.city;
                        schools[0].phone_number = req.body.phone_number;
                        schools[0].office_number = req.body.office_number;
                        schools[0].fax_number = req.body.fax_number;
                        schools[0].max_students = (typeof req.body.max_students != 'undefined') ? req.body.max_students : schools[0].max_students;
                        schools[0].parent_id = (typeof req.body.parent_id != 'undefined') ? req.body.parent_id : schools[0].parent_id;
                        schools[0].status = req.body.status;
                        schools[0].save(function(err, saved_school) {
                            if (err) {
                                console.log(err);
                                return sendError(req, res, 400, "Bad Request check your parameters");
                            }
                            return res.send(saved_school);
                        });
                    });
                } else {
                    return sendError(req, res, 404, 'Cannot find User Admin related to this school');
                }
            });
        } else {
            return sendError(req, res, 404, 'cannot find School with passed ID');
        }
    });
};

exports.active = function(req, res) {
    if (req.body.id !== '' || req.body.id !== undefined) {
        School.find({
            id: req.body.id
        }, 1, function(err, schools) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            if (schools.length) {
                schools[0].status = "1";
                schools[0].save(function(err, success) {
                    if (err) {
                        console.log(err);
                        return sendError(req, res, 500, 'something went wrong in active school');
                    }
                    return res.send(schools[0]);
                });
            } else {
                return sendError(req, res, 404, 'School not found');
            }
        });
    } else {
        return sendError(req, res, 404, 'require school id which you want to active');
    }
};

exports.inactive = function(req, res) {
    if (req.body.id !== '' || req.body.id !== undefined) {
        School.find({
            id: req.body.id
        }, 1, function(err, schools) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            if (schools.length) {
                schools[0].status = "0";
                schools[0].save(function(err, success) {
                    if (err) {
                        console.log(err);
                        return sendError(req, res, 500, 'something went wrong in inactive school');
                    }
                    return res.send(schools[0]);
                });
            } else {
                return sendError(req, res, 404, 'School not found');
            }
        });
    } else {
        return sendError(req, res, 404, 'require school id which you want to inactive');
    }
};

exports.delete = function(req, res) {
    if (req.body.id !== '' || req.body.id != 'undefined') {
        School.find({
            id: req.body.id
        }, 1, function(err, schools) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            if (schools.length) {
                User.find({
                    id: schools[0].authorized_user_id
                }, 1, function(err, users) {
                    if (err) {
                        console.log(err);
                        return sendError(req, res, 400, "Bad Request check your parameters");
                    }
                    if (users.length) {
                        users[0].remove(function(err, success) {
                            if (err) {
                                console.log(err);
                                return sendError(req, res, 500, 'something went wrong in deleting user');
                            }
                            schools[0].remove(function(err, success) {
                                if (err) {
                                    console.log(err);
                                    return sendError(req, res, 500, 'something went wrong while deleting school');
                                }
                                return res.send(schools[0]);
                            });
                        });
                    } else {
                        schools[0].remove(function(err, success) {
                            if (err) {
                                console.log(err);
                                return sendError(req, res, 500, 'something went wrong while deleting school');
                            }
                            return res.send(schools[0]);
                        });
                    }
                });
            } else {
                return sendError(req, res, 404, 'School not found');
            }
        });
    } else {
        return sendError(req, res, 404, 'require school id which you want to delete');
    }
};

exports.trySchoolLogin = function(req, res) {
    var query_str = `SELECT t1.*, t2.id as user_id, CONCAT(t2.first_name, ' ', t2.last_name) as ap_name, t2.email as email,t2.profile_image, t2.mobile as ap_mobile , t3.modules as security_roles, 'admin' as school_admin_flag
                    FROM schools as t1 
                    LEFT JOIN users as t2 ON t1.authorized_user_id = t2.id 
                    LEFT JOIN school_roles as t3 ON t2.role_id = t3.id
                    WHERE t1.id = ? `;
    db.driver.execQuery(query_str, [req.body.id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        if (!rows.length) {
            return sendError(req, res, 404, "school not found");
        }
        return res.send(rows[0]);
    });
};

var createHash = function(password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
};
