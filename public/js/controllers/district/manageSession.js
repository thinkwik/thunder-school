app.controller('districtCreateSessionController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.Schools = [];
        var district = $cookieStore.get('loggedInDistrict');
        if (typeof district != 'undefined') {
            var data = {
                district_id: district.id
            };
            $http({
                method: 'POST',
                url: '/school/getDistrictSchools',
                data: data
            }).then(function successCallback(response) {
                $scope.Schools = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'District', 'Something went wrong!');
            });
        } else {
            toaster.pop('error', 'Login', 'something went wrong! Please try re-login');
        }

        $scope.addSession = function() {
            $state.go('districts.session_create');
        };

        $scope.cancelSessionCreate = function() {
            $state.go('districts.session_list');
        };

        $scope.createSession = function() {
            data = $scope.session;
            data.created_by = $cookieStore.get('loggedInDistrict').authorized_user_id;
            $http({
                method: "POST",
                url: "/session/register",
                data: data
            }).then(function successCallback(response) {
                $scope.cancelSessionCreate();
                toaster.pop('success', 'Session', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Session', error.data.message);
            });
        };

        $scope.cancelSessionForm = function() {
            $state.go('districts.session_list');
        };
    }
]);

app.controller('districtListSessionController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.addNewSession = function() {
            $state.go('districts.session_create');
        };

        $scope.inactiveSession = function(session_id) {
            var data = {
                created_by : $cookieStore.get('loggedInDistrict').authorized_user_id,
                id: session_id
            };
            $http({
                method: 'POST',
                url: '/session/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllSessions();
            }, function errorCallback(error) {
                toaster.pop('error', 'Session', error.data.message);
            });
        };

        $scope.activeSession = function(session_id) {
            var data = {
                created_by : $cookieStore.get('loggedInDistrict').authorized_user_id,
                id: session_id
            };
            $http({
                method: 'POST',
                url: '/session/active',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllSessions();
            }, function errorCallback(error) {
                toaster.pop('error', 'Session', error.data.message);
            });
        };

        $scope.deleteSession = function(session_id) {
            var data = {
                created_by : $cookieStore.get('loggedInDistrict').authorized_user_id,
                id: session_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/session/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllSessions();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Session', error.data.message);
                });
            }
        };

        $scope.getAllSessions = function() {
            var data = {
                district_id: $cookieStore.get('loggedInDistrict').id
            };
            $http({
                method: 'POST',
                url: '/session/getAllDistSessions',
                data: data
            }).then(function successCallback(response) {
                var sessions = response.data;
                $scope.sessions = sessions;
            }, function errorCallback(error) {
                toaster.pop('error', 'Session', 'cannot get all the Sessions! try reloading page.');
                console.log(error.data.message);
            });
        };

        $scope.editSession = function(session_id) {
            $state.go('districts.session_edit', {
                session_id: session_id
            });
        };
    }
]);

app.controller('districtEditSessionController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.Schools = [];
        var district = $cookieStore.get('loggedInDistrict');
        if (typeof district != 'undefined') {
            var data = {
                district_id: district.id
            };
            $http({
                method: 'POST',
                url: '/school/getDistrictSchools',
                data: data
            }).then(function successCallback(response) {
                $scope.Schools = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'District', 'Something went wrong!');
            });
        } else {
            toaster.pop('error', 'Login', 'something went wrong! Please try re-login');
        }

        $scope.getEditSession = function() {
            $http({
                method: 'GET',
                url: '/session/edit?id=' + $state.params.session_id
            }).then(function successCallback(response) {
                $scope.session = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Session', 'cannot get all the Sessions! try reloading page.');
            });
        };

        $scope.saveEditSession = function() {
            data = $scope.session;
            data.created_by = $cookieStore.get('loggedInDistrict').authorized_user_id;
            $http({
                method: 'POST',
                url: '/session/edit',
                data: data
            }).then(function successCallback(response) {
                $scope.cancelSessionForm();
                toaster.pop('success', 'Session', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Session', error.data.message);
            });
        };

        $scope.cancelSessionForm = function() {
            $state.go('districts.session_list');
        };
    }
]);
