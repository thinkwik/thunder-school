var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Emails = db.define('emails', {
    email_from: String,
    email_to: String,
    subject: String,
    content: String
}, {
    timestamp: true,
    methods: {

    }
});
