app.controller('studentsExamListController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
	function($scope, $http, $state, toaster, $cookieStore) {
		$scope.viewReport = function(exam_id){
            $state.go('students.view_exam_report',{
                exam_id:exam_id,
                student_id:$cookieStore.get('loggedInStudent').id
            });
        };
		$scope.getAllExamList = function() {
			var data = {
				student_id: $cookieStore.get('loggedInStudent').id
			};
			$http({
				method: 'POST',
				url: '/exam/getStudentsExam',
				data: data
			}).then(function successCallback(response) {
				$scope.exams = response.data;
			}, function errorCallback(error) {
				toaster.pop('error', 'Exams', 'Something went wrong! while getting all exams');
			});
		};
		$scope.takeExam = function(exam_id){
			if(true){
				$state.go("students.take_exam",{
					exam_id:exam_id
				});
			}
			//confirm("Once you start your exam you cannot left in between. \n Are You sure to start your Exam ?")
		};
	}
]);

app.controller('studentsResultListStaffController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
	function($scope, $http, $state, toaster, $cookieStore) {
		$scope.getAllExamResultList = function() {
			var data = {
				school_id: $cookieStore.get('loggedInStudent').school_id,
				student_id: $cookieStore.get('loggedInStudent').id,
			};
			$http({
				method: 'POST',
				url: '/exam_result/getStudentExamResults',
				data: data
			}).then(function successCallback(response) {
				$scope.results = response.data;
			}, function errorCallback(error) {
				toaster.pop('error', 'Exams', 'Something went wrong! while getting all exams');
			});
		};
	}
]);
