var bCrypt = require('bcrypt-nodejs');
var shortid = require('shortid');
var db = require('orm').db,
    Admissions = db.models.admissions,
    Grade = db.models.grades,
    Sessions = db.models.sessions,
    StudentClasses = db.models.student_classes;

var createHash = function(password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
};

exports.register = function(req, res) {
    var newAdmission = new Admissions();
    newAdmission.school_id = req.body.school_id;
    newAdmission.unique_id = shortid.generate();
    newAdmission.staff_id = req.body.staff_id;
    newAdmission.grade_id = req.body.grade_id;
    newAdmission.session_id = req.body.session_id;
    newAdmission.first_name = req.body.first_name;
    newAdmission.middle_name = req.body.middle_name;
    newAdmission.last_name = req.body.last_name;
    newAdmission.gender = req.body.gender;
    newAdmission.password = createHash(req.body.password);
    newAdmission.birth_date = req.body.birth_date;
    newAdmission.email = req.body.email;
    newAdmission.phone = req.body.phone;
    newAdmission.inquiry_by = req.body.inquiry_by;
    newAdmission.source_id = req.body.source_id;
    newAdmission.source_detail = req.body.source_detail;
    newAdmission.address = req.body.address;
    newAdmission.city = req.body.city;
    newAdmission.state = req.body.state;
    newAdmission.zipcode = req.body.zipcode;
    newAdmission.country = req.body.country;
    newAdmission.image = req.body.image;
    newAdmission.admission_status = req.body.admission_status;
    newAdmission.inquiry_status = req.body.inquiry_status;

    newAdmission.notes = (typeof req.body.notes != 'undefined') ? req.body.notes : null;
    newAdmission.medicalinfo = (typeof req.body.medicalinfo != 'undefined') ? req.body.medicalinfo : null;
    newAdmission.social_behavioral = (typeof req.body.social_behavioral != 'undefined') ? req.body.social_behavioral : null;
    newAdmission.physical_handicaps = (typeof req.body.physical_handicaps != 'undefined') ? req.body.physical_handicaps : null;
    newAdmission.medical_immunizations = (typeof req.body.medical_immunizations != 'undefined' && Array.isArray(req.body.medical_immunizations) ) ? req.body.medical_immunizations.join(',') : null;
    newAdmission.medical_allergies = (typeof req.body.medical_allergies != 'undefined' && Array.isArray(req.body.medical_allergies) ) ? req.body.medical_allergies.join(',') : null;
   

    newAdmission.status = (typeof req.body.status != 'undefined') ? req.body.status : '1';
    newAdmission.profile_status = (typeof req.body.profile_status != 'undefined') ? req.body.profile_status : '1';
    newAdmission.save(function(err, created_admission) {
        if (err) {
            console.log(err);
            return sendError(req, res, 500, 'Something went wrong!');
        }
        var query_str = ` INSERT INTO student_classes (class_id, student_id, grade_id)
                          SELECT t1.id, ?, ?
                          FROM   classes as t1
                          WHERE  t1.grade_id = ?`;
        db.driver.execQuery(query_str, [created_admission.id, created_admission.grade_id, created_admission.grade_id], function(err, all_students) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
        });
        return res.send(created_admission);
    });
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Admission id required!!');
    }
    Admissions.find({
        id: req.body.id
    }, 1, function(err, admissions) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        admissions[0].school_id = req.body.school_id;
        admissions[0].grade_id = req.body.grade_id;
        admissions[0].staff_id = req.body.staff_id;
        admissions[0].session_id = req.body.session_id;
        admissions[0].first_name = req.body.first_name;
        admissions[0].last_name = req.body.last_name;
        admissions[0].gender = req.body.gender;
        admissions[0].birth_date = req.body.birth_date;
        admissions[0].email = req.body.email;
        admissions[0].phone = req.body.phone;
        admissions[0].inquiry_by = req.body.inquiry_by;
        admissions[0].source_id = req.body.source_id;
        admissions[0].source_detail = req.body.source_detail;
        admissions[0].address = req.body.address;
        admissions[0].city = req.body.city;
        admissions[0].state = req.body.state;
        admissions[0].country = req.body.country;
        admissions[0].zipcode = req.body.zipcode;
        admissions[0].comments = req.body.comments;
        admissions[0].academic_history = req.body.academic_history;
        admissions[0].academics = req.body.academics;
        admissions[0].sports = req.body.sports;
        admissions[0].remarks = req.body.remarks;
        admissions[0].height = req.body.height;
        admissions[0].weight = req.body.weight;
        admissions[0].blood_group = req.body.blood_group;
        admissions[0].immunization = req.body.immunization;
        admissions[0].dieases_information = req.body.dieases_information;
        admissions[0].admission_status = req.body.admission_status;
        admissions[0].inquiry_status = req.body.inquiry_status;
        admissions[0].image = (typeof req.body.image == 'undefined' || req.body.image === "") ? admissions[0].image : req.body.image;
        admissions[0].mobile_number = req.body.mobile_number;
        admissions[0].status = (typeof req.body.status != 'undefined') ? req.body.status : admissions[0].status;


        admissions[0].notes = (typeof req.body.notes != 'undefined') ? req.body.notes : admissions[0].notes;
        admissions[0].medicalinfo = (typeof req.body.medicalinfo != 'undefined') ? req.body.medicalinfo : admissions[0].medicalinfo;
        admissions[0].social_behavioral = (typeof req.body.social_behavioral != 'undefined') ? req.body.social_behavioral : admissions[0].social_behavioral;
        admissions[0].physical_handicaps = (typeof req.body.physical_handicaps != 'undefined') ? req.body.physical_handicaps : admissions[0].physical_handicaps;
        admissions[0].medical_immunizations = (typeof req.body.medical_immunizations != 'undefined' && Array.isArray(req.body.medical_immunizations) ) ? req.body.medical_immunizations.join(',') : admissions[0].medical_immunizations;
        admissions[0].medical_allergies = (typeof req.body.medical_allergies != 'undefined' && Array.isArray(req.body.medical_allergies)) ? req.body.medical_allergies.join(',') : admissions[0].medical_allergies;
       

        admissions[0].save(function(err, updated_admission_inquiry) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(updated_admission_inquiry);
        });
    });
};

exports.active = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Admission id required!!');
    }
    Admissions.find({
        id: req.body.id
    }, 1, function(err, admissions) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        admissions[0].status = "1";
        admissions[0].save(function(err, activated_admissions_inquiry) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(activated_admissions_inquiry);
        });
    });
};

exports.inactive = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Admission id required!!');
    }
    Admissions.find({
        id: req.body.id
    }, 1, function(err, admissions) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        admissions[0].status = "0";
        admissions[0].save(function(err, inactivated_admission) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, 'Admission id required!!');
            }
            return res.send(inactivated_admission);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Admission id required!!');
    }
    Admissions.find({
        id: req.body.id
    }, 1, function(err, admissions) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        admissions[0].remove(function(err) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, 'Admission id required!!');
            }
            return res.send(admissions[0]);
        });
    });
};

exports.getStudentsList = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = "SELECT t1.*,t2.school_name,CONCAT(t3.first_name ,' ',t3.last_name) as username,t3.photo FROM admissions as t1 LEFT JOIN schools as t2 ON t1.school_id = t2.id LEFT JOIN staff as t3 ON t1.staff_id = t3.id WHERE t2.id = ?";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getStudent = function(req, res) {
    if (typeof req.body.student_id == 'undefined' || req.body.student_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    Admissions.find({
        id: req.body.student_id
    }, 1, function(err, students) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        if (!(students.length)) return sendError(req, res, 404, "Students not found");
        return res.send(students[0]);
    });
};

exports.getAdmission = function(req, res) {
    if (typeof req.query.id == 'undefined') return sendError(req, res, 401, "ID required field");
    Admissions.find({
        id: req.query.id
    }, 1, function(err, admissions) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        if (!(admissions.length)) return sendError(req, res, 404, "Admission Inquiry not found");
        return res.send(admissions[0]);
    });
};

exports.getAllAdmissionInquiry = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = "SELECT t1.*,t2.school_name,CONCAT(t3.first_name ,' ',t3.last_name) as username,t3.photo,t4.grade,t5.session_name FROM admissions as t1 LEFT JOIN schools as t2 ON t1.school_id = t2.id LEFT JOIN staff as t3 ON t1.staff_id = t3.id LEFT JOIN grades as t4 ON t1.grade_id = t4.id LEFT JOIN sessions as t5 ON t1.session_id = t5.id WHERE t1.school_id = ?";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getDistrictAdmissionInquiry = function(req, res) {
    if (typeof req.body.district_id == 'undefined' || req.body.district_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = "SELECT t1.*,t2.school_name,CONCAT(t3.first_name ,' ',t3.last_name) as username,t3.photo,t4.grade,t5.session_name FROM admissions as t1 LEFT JOIN schools as t2 ON t1.school_id = t2.id LEFT JOIN staff as t3 ON t1.staff_id = t3.id LEFT JOIN grades as t4 ON t1.grade_id = t4.id LEFT JOIN sessions as t5 ON t1.session_id = t5.id WHERE t2.parent_id = ?";
    db.driver.execQuery(query_str, [req.body.district_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getSchoolStudentsList = function(req, res) {
    if (typeof req.body.district_id == 'undefined' || req.body.district_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = "SELECT t1.*,t2.school_name,CONCAT(t3.first_name ,' ',t3.last_name) as username,t3.photo,t4.grade,t5.session_name FROM admissions as t1 LEFT JOIN schools as t2 ON t1.school_id = t2.id LEFT JOIN staff as t3 ON t1.staff_id = t3.id LEFT JOIN grades as t4 ON t1.grade_id = t4.id LEFT JOIN sessions as t5 ON t1.session_id = t5.id WHERE t1.school_id = ?";
    db.driver.execQuery(query_str, [req.body.district_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getSelectedGradesRecords = function(req, res) {
    if (typeof req.body.grade_id == 'undefined') return sendError(req, res, 401, "ID required field");
    Admissions.find({
        grade_id: req.body.grade_id,
        staff_id: req.body.staff_id
    }, 1, function(err, students) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        if (!students.length) return sendError(req, res, 404, "Records not found");
        return res.send(students[0]);
    });
};

exports.getStudentEmailList = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = "SELECT t1.email  FROM admissions as t1 WHERE t1.school_id = ?";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getStaffEmailList = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = "SELECT t1.email_id FROM staff as t1 WHERE t1.school_id = ?";
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

var fs = require('fs');
exports.uploadFile = function(req, res) {
    var fs = require('fs');
    var studentImages = __dirname + "/../../public/upload/studentImages/";
    if (!fs.existsSync(studentImages)) {
        fs.mkdirSync(studentImages);
    }
    var file = req.files.file;
    var fileExtension = file.name.split(".")[1];
    if (fileExtension) {
      fileName = file.path.split('/');
      fs.rename(file.path, studentImages + fileName[fileName.length - 1] +"."+ fileExtension);
      return res.send(fileName[fileName.length - 1] +"."+ fileExtension);
    }
    else {
      return sendError(req, res, 400, "Bad Request check your parameters");
    }
};

exports.getStudentsListGradeWise = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = "SELECT t1.*,t2.school_name,CONCAT(t3.first_name ,' ',t3.last_name) as username,t3.photo FROM admissions as t1 LEFT JOIN schools as t2 ON t1.school_id = t2.id LEFT JOIN staff as t3 ON t1.staff_id = t3.id WHERE t2.id = ? AND t1.grade_id = ?";
    db.driver.execQuery(query_str, [req.body.school_id, req.body.grade_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getAllStudentGradeWise = function(req, res) {
    if (typeof req.body.grade_id === 'undefined') {
        return sendError(req, res, 422, "Bad Request check your parameters");
    }
    var query_str = "SELECT id, CONCAT(first_name ,' ',last_name) as name FROM admissions WHERE grade_id = ?";
    db.driver.execQuery(query_str, [req.body.grade_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.studentPromotion = function(req, res) {
    if (typeof req.body.student_id == 'undefined' || typeof req.body.select_grade == 'undefined' || typeof req.body.student_promoted_to == 'undefined') {
        return sendError(req, res, 422, 'Please check your parameter');
    }
    if (req.body.select_grade == req.body.student_promoted_to) {
        return sendError(req, res, 422, 'Student is already in perticular grade.');
    }
    var student_id = req.body.student_id;
    for (var i = 0; i < student_id.length; i++) {
        Admissions.find({
            id: student_id[i]
        }, 1, function(err, admissions) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, "Bad Request check your parameters");
            }
            admissions[0].grade_id = req.body.student_promoted_to;
            admissions[0].save(function(err, student_promoted) {
                if (err) {
                    console.log(err);
                    return sendError(req, res, 422, "Bad Request check your parameters");
                }
                var query_str = "DELETE FROM student_classes WHERE student_id = ? AND grade_id = ?";
                db.driver.execQuery(query_str, [student_promoted.id, req.body.student_promoted_to], function(err, rows) {
                    if (err) {
                        console.log(err);
                        return sendError(req, res, 400, "Bad Request check your parameters");
                    }
                    var query_str = ` INSERT INTO student_classes (class_id, student_id, grade_id)
                                      SELECT t1.id, ?, ?
                                      FROM   classes as t1
                                      WHERE  t1.grade_id = ?`;
                    db.driver.execQuery(query_str, [student_promoted.id, req.body.student_promoted_to, req.body.student_promoted_to], function(err, all_students) {
                        if (err) {
                            console.log(err);
                            return sendError(req, res, 400, "Bad Request check your parameters");
                        }
                    });
                });
            });
        });
    }
    res.send("Success!");
};
