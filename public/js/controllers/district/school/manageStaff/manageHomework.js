app.controller('listHomeworkController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', '$window',
    function($scope, $http, $state, toaster, $cookieStore, $window) {
        $scope.getHomeworks = function() {
            var data = {
                school_id: $cookieStore.get('loggedInStaff').school_id,
                staff_id: $cookieStore.get('loggedInStaff').id
            };
            $http({
                method: 'POST',
                url: '/homework/getHomeworkStaffwise',
                data: data
            }).then(function successCallback(response) {
                var homeworks = response.data;
                $scope.homeworks = homeworks;
            }, function errorCallback(error) {
                toaster.pop('error', 'Homework', 'cannot get all the Homework! try reloading page.');
            });
        };
        $scope.homeworkReport = function(homework_id) {
            $state.go('schoolstaff.homework_report_list', {
                homework_id: homework_id
            });
        };
        $scope.addNewHomework = function() {
            $state.go('schoolstaff.homework_create');
        };

        $scope.deleteHomework = function(homework_id) {
            var data = {
                id: homework_id
            };
            var result = confirm("Want to delete?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/homework/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getHomeworks();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Homework', error.data.message);
                });
            }
        };

        $scope.downloadFile = function(homework) {
            if(homework.file) {
                var homeworkFileName = homework.file;
                homeworkFileName = homeworkFileName.split(".");
                var url =  $window.location.origin + '/upload/homeworks/' + homework.file;
                var request = new XMLHttpRequest();
                request.open('HEAD', url, false);
                request.send();
                if(request.status == 200) {
                  var element = angular.element('<a/>');
                  element.attr({
                      href: url,
                      target: '_blank',
                      download: homework.title + '.' + homeworkFileName[1]
                  })[0].click();
                } else {
                  toaster.pop('error', 'Homework', 'Homework not found!');
                  return false;
                }
            } else {
              toaster.pop('error', 'Homework', 'Homework not found!');
              return false;
            }
        };

        $scope.editHomework = function(homework_id) {
            $state.go('schoolstaff.homework_edit', {
                homework_id: homework_id
            });
        };

    }
]);

app.controller('createHomeworkController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {

        // $scope.getGrades = function() {
        //     var data = {
        //         school_id: $cookieStore.get('loggedInStaff').school_id
        //     };
        //     $http({
        //         method: 'POST',
        //         url: '/grade/getAllSchoolGrades',
        //         data: data
        //     }).then(function successCallback(response) {
        //         $scope.grades = response.data;
        //     }, function errorCallback(error) {
        //         toaster.pop('error', 'Grades', 'Something went wrong!');
        //     });
        // };
        //
        // $scope.getSubject = function(grade_id) {
        //     $scope.homework.subject_id = "";
        //     var data = {
        //         grade_id: grade_id,
        //         school_id: $cookieStore.get('loggedInStaff').school_id
        //     };
        //     $http({
        //         method: 'POST',
        //         url: '/subject/getSchoolGradesSubjectWise',
        //         data: data
        //     }).then(function successCallback(response) {
        //         $scope.subjects = response.data;
        //     }, function errorCallback(error) {
        //         toaster.pop('error', 'Subjects', 'Something went wrong!');
        //     });
        // };
        //
        // $scope.getClass = function(subject_id) {
        //     $scope.homework.class_id = "";
        //     var data = {
        //         subject_id: subject_id
        //     };
        //     $http({
        //         method: 'POST',
        //         url: '/class/getAllSubjectClasses',
        //         data: data
        //     }).then(function successCallback(response) {
        //         $scope.classes = response.data;
        //     }, function errorCallback(error) {
        //         toaster.pop('error', 'Subjects', 'Something went wrong!');
        //     });
        // };


        $scope.getSchoolGrades = function() {
            var data = {
                school_id: $cookieStore.get('loggedInStaff').school_id
            };
            $http({
                method: 'POST',
                url: '/grade/getAllSchoolGrades',
                data: data
            }).then(function successCallback(response) {
                $scope.grades = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Grades', 'Something went wrong!');
            });
        };


        $scope.getClassSubjectWise = function(subject_id) {
            var data = {
                subject_id: subject_id
            };
            $http({
                method: 'POST',
                url: '/class/getClassSubjectWise',
                data: data
            }).then(function successCallback(response) {
                $scope.classes = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Student Classes', error.data.message);
            });
        };

        $scope.getSubjectGradeWise = function(grade_id) {
          $scope.classes = [];
          $scope.subjects = [];
          $scope.homework.subject_id = "";
          $scope.homework.class_id = "";
            var data = {
                grade_id: grade_id,
                school_id: $cookieStore.get('loggedInStaff').school_id
            };
            $http({
                method: 'POST',
                url: '/subject/getSchoolGradesSubjectWise',
                data: data
            }).then(function successCallback(response) {
                $scope.subjects = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Subjects', 'Something went wrong!');
            });
        };

        $scope.listHomeworks = function() {
            $state.go('schoolstaff.homework_list');
        };

        $scope.uploadFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected !');
            Loading(true);
            Upload.upload({
                url: '/homewrok/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.file = success.data;
                    $scope.saveDisable = true;
                },
                function(error) {
                    toaster.pop('error', "Error", error.data.message);
                    $scope.saveDisable = false;
                });
        };

        $scope.createHomework = function() {
            data = $scope.homework;
            data.school_id = $cookieStore.get('loggedInStaff').school_id;
            data.staff_id = $cookieStore.get('loggedInStaff').id;
            data.file = $scope.file;
            $http({
                method: "POST",
                url: "/homework/register",
                data: data
            }).then(function successCallback(response) {
                $state.go('schoolstaff.homework_list');
                toaster.pop('success', 'Homework', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Homework', error.data.message);
            });
        };
    }
]);

app.controller('editHomeworkController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        $scope.getGrades = function() {
            var data = {
                school_id: $cookieStore.get('loggedInStaff').school_id
            };
            $http({
                method: 'POST',
                url: '/grade/getAllSchoolGrades',
                data: data
            }).then(function successCallback(response) {
                $scope.grades = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Grades', 'Something went wrong!');
            });
        };

        $scope.getSubject = function(grade_id) {
            var data = {
                grade_id: grade_id,
                school_id: $cookieStore.get('loggedInStaff').school_id
            };
            $http({
                method: 'POST',
                url: '/subject/getSchoolGradesSubjectWise',
                data: data
            }).then(function successCallback(response) {
                $scope.subjects = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Subjects', 'Something went wrong!');
            });
        };

        $scope.getClass = function(subject_id) {
            var data = {
                subject_id: subject_id
            };
            $http({
                method: 'POST',
                url: '/class/getAllSubjectClasses',
                data: data
            }).then(function successCallback(response) {
                $scope.classes = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Subjects', 'Something went wrong!');
            });
        };

        $scope.getEditHomeworks = function() {
            $http({
                method: 'GET',
                url: '/homework/edit?id=' + $state.params.homework_id
            }).then(function successCallback(response) {
                $scope.homework = response.data;
                $scope.getGrades();
                $scope.getSubject(response.data.grade_id);
                $scope.getClass(response.data.subject_id);
            }, function errorCallback(error) {
                toaster.pop('error', 'Homework', 'cannot get all Homework list! try reloading page.');
            });
        };

        $scope.updateHomework = function() {
            data = $scope.homework;
            data.file = $scope.file;
            $http({
                method: 'POST',
                url: '/homework/edit',
                data: data
            }).then(function successCallback(response) {
                $state.go('schoolstaff.homework_list');
                toaster.pop('success', 'Homework', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Homework', error.data.message);
            });
        };

        $scope.listHomeworks = function() {
            $state.go('schoolstaff.homework_list');
        };

        $scope.uploadFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected !');
            Loading(true);
            Upload.upload({
                url: '/homewrok/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.file = success.data;
                    $scope.saveDisable = true;
                },
                function(error) {
                    toaster.pop('error', "Error", error.data.message);
                    $scope.saveDisable = false;
                });
        };

        $scope.listHomework = function() {
            $state.go('schoolstaff.homework_list');
        };
    }
]);
