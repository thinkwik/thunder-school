var bCrypt = require('bcrypt-nodejs');
var shortid = require('shortid');
var db = require('orm').db,
    User = db.models.users;

var createHash = function(password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
};


exports.apiForgotPasswordAll = function(req, res) {
    var type_db = [];
    type_db['staff'] = "staff";
    type_db['student'] = "admissions";
    type_db['parent'] = "parents";
    var email = [];
    email['staff'] = "email_id";
    email['admissions'] = "email";
    email['parents'] = "email";
    email['users'] = "email";
    if (req.body.email && req.body.type) {
        var query_str = `SELECT * FROM ` + type_db[req.body.type] + ` WHERE ` + email[type_db[req.body.type]] + ` = ? LIMIT 1 `;
        db.driver.execQuery(query_str, [req.body.email], function(err, rows) {
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "Something went wrong!");
            }
            if (rows.length <= 0) {
                return sendError(req, res, 404, "Please make sure your email is correct.");
            }
            var token = shortid.generate();
            var query_str_2 = `UPDATE ` + type_db[req.body.type] + ` SET forgot_password_token = ? WHERE id = ?`;
            db.driver.execQuery(query_str_2, [token, rows[0].id], function(err, users) {
                if (err) {
                    console.log(err);
                    return sendError(req, res, 500, "Something went wrong!");
                }
                var to = req.body.email;
                var subject = "Reset Password Link";
                var link = "https://" + req.headers.host + "/reset_password?token=" + token + "&type=" + type_db[req.body.type] + "&email=" + req.body.email;
                var email_html = `
                <body style="font-family:arial; font-size:16px; color:#4c4c4c; padding:0; margin:0;">
                    <table width="100%" style="font-family:arial; font-size:16px; border:0px solid #000; max-width:700px; padding-top:30px;"  align="center" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                            <tr>
                                <td>
                                    <table width="100%" style="font-family:arial; font-size:16px; border:1px solid #d6d6d8; max-width:700px;"  align="center" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td style="padding:50px 40px 40px 40px; text-align:center; font-family:arial;">
                                                    <h1 style="font-family:arial; font-size:40px; color:#30333b; font-weight:normal; padding:0; margin:0;">Hello, <span style="color:rgb(242,53,95); font-weight:bold; text-transform:uppercase;">User</span></h1>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding:0 40px 30px 40px; text-align:center; font-family:arial;">
                                                    <p style="font-family:arial; font-size:24px; color:#737373; font-weight:normal; padding:0; margin:0;">
                                                        Reset Your Password
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding:0 40px 50px 40px; text-align:center;">
                                                    <a href="` + link + `" style="text-decoration:none; outline:none;">
                                                        <img style="display:inline-block;" src="https://socratessms.com:8080/img/reset_password.png" alt="Join Link" />
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding:35px 40px;">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td valign="bottom" style="text-align:right; font-family:arial; font-size:14px; color:#6a6c72;">Kind Regards <br /><b style="text-transform:uppercase;">Thunder System</b></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-family:arial; font-size:10px; padding:20px 0 30px 0; color:#a6a6a6; text-transform:uppercase; text-align:center;">
                                    &#169; 2016 Copyright Thunder System
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </body>
                `;
                sendMail(req, res, null, to, subject, email_html, function(err, success) {
                    if (err) {
                        console.log(err);
                        return sendError(req, res, 500, "Something went wrong!");
                    }
                    var response = {};
                    var temp = {};
                    temp['token'] = token;
                    temp['type'] = req.body.type;
                    temp['email'] = req.body.email;
                    response['data'] = temp;
                    response['code'] = 200;
                    return res.send(response);
                });
            });

        });
    } else {
        return sendError(req, res, 488, "Bad Request check your parameters");
    }
};

exports.apiResetPasswordAll = function(req, res) {
    var type_db = [];
    type_db['staff'] = "staff";
    type_db['student'] = "admissions";
    type_db['parent'] = "parents";
    var email = [];
    email['staff'] = "email_id";
    email['admissions'] = "email";
    email['parents'] = "email";
    email['users'] = "email";
    if (req.body.email && req.body.type && req.body.token && req.body.new_password && req.body.confirm_new_password) {
        var query_str = `SELECT * FROM ` + type_db[req.body.type] + ` WHERE ` + email[type_db[req.body.type]] + ` = ? AND forgot_password_token = ? LIMIT 1 `;
        db.driver.execQuery(query_str, [req.body.email, req.body.token], function(err, rows) {
            if (req.body.new_password != req.body.confirm_new_password) {
                return sendError(req, res, 500, "both password must be same");
            }
            if (err) {
                console.log(err);
                return sendError(req, res, 500, "Something went wrong!");
            }
            if (rows.length <= 0) {
                return sendError(req, res, 404, "Your link has been expired, please try again");
            }
            var password = createHash(req.body.new_password);
            var query_str_2 = `UPDATE ` + type_db[req.body.type] + ` SET forgot_password_token = ? , password = ? WHERE id = ?`;
            db.driver.execQuery(query_str_2, [null, password, rows[0].id], function(err, users) {
                if (err) {
                    console.log(err);
                    return sendError(req, res, 500, "Something went wrong!");
                }
                var response = {};
                response['data'] = [];
                response['code'] = 200;
                return res.send(response);
            });
        });
    } else {
        return sendError(req, res, 400, "Bad Request check your parameters");
    }
};
