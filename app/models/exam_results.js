var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var ExamResult = db.define('exam_results', {
    exam_id: {
        type: 'integer'
    },
    student_id: {
        type: 'integer'
    },
    achived_marks: String,
    result_date: String,
    achived_grade: {
      type: 'integer'  
    }
}, {
    timestamp: true,
    methods: {
    }
});