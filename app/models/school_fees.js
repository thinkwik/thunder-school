var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var SchoolRole = db.define('school_fees', {
    title: String,
    school_id: {
        type: 'integer'
    },
    grade_id: {
        type: 'integer'
    },
    amount: String,
    status: String
}, {
    timestamp: true,
    methods: {

    }
});