    app.controller('createLocationController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload',
    function($scope, $http, $state, toaster, $cookieStore, Upload) {
        $scope.createLocation = function() {
            var data = $scope.location;
            data.school_id = $cookieStore.get('loggedInSchool').id;
            $http({
                method: 'POST',
                url: '/location/register',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', "Locations", "Location added successfully!!!");
                $state.go('schools.list_location');
            }, function errorCallback(error) {
                toaster.pop('error', "Location", error.data.message);
            });
        };

        $scope.listLocation = function() {
            $state.go('schools.list_location');
        };
    }
]);

app.controller('editLocationController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', 'Upload', '$window',
    function($scope, $http, $state, toaster, $cookieStore, Upload, $window) {
        $scope.listLocation = function() {
            $state.go('schools.list_location');
        };

        $scope.saveEditLocation = function(location_id) {
            var data = $scope.location;
            $http({
                method: 'POST',
                url: '/location/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Locations', 'Updated Successfully.');
                $state.go('schools.list_location');
            }, function errorCallback(error) {
                toaster.pop('error', "Location", error.data.message);
            });
        };

        $scope.getEditLocation = function() {
            $http({
                method: 'GET',
                url: '/location/edit?id=' + $state.params.location_id
            }).then(function successCallback(response) {
                $scope.location = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Locations', 'cannot get all the Locations! try reloading page.');
            });
        };
    }
]);

app.controller('listLocationController', ['$scope', '$http', '$state', 'toaster', '$cookieStore', '$window',
    function($scope, $http, $state, toaster, $cookieStore, $window) {

        $scope.addNewLocation = function() {
            $state.go("schools.create_location");
        }

        $scope.getAllLocation = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/location/getAllLocations',
                data: data
            }).then(function successCallback(response) {
                $scope.locations = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Locations', 'cannot get all the Locations! try reloading page.');
            });
        };

        $scope.activeLocations = function(location_id) {
            var data = {
                id: location_id
            };
            $http({
                method: 'POST',
                url: '/location/active',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllLocation();
            }, function errorCallback(error) {
                toaster.pop('error', 'Locations', error.data.message);
            });
        };

        $scope.deleteLocations = function(location_id) {
            var data = {
                id: location_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/location/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllLocation();
                }, function errorCallback(error) {
                    toaster.pop('error', 'Locations', error.data.message);
                });
            }
        };

        $scope.inactiveLocations = function(location_id) {
            var data = {
                id: location_id
            };
            $http({
                method: 'POST',
                url: '/location/inactive',
                data: data
            }).then(function successCallback(response) {
                $scope.getAllLocation();
            }, function errorCallback(error) {
                toaster.pop('error', 'Locations', error.data.message);
            });
        };

        $scope.editLocation = function(location_id) {
            $state.go('schools.edit_location', {
                location_id: location_id
            });
        };
    }
]);
