app.controller('createSchoolAdmissionController', ['$scope', '$http', '$state', 'toaster', '$cookieStore','Upload',
    function($scope, $http, $state, toaster, $cookieStore,Upload) {
        $scope.cancelAdmissionForm = function() {
            $state.go('schools.admission_list');
        };
        $scope.image = "";

        $scope.uploadFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected !');
            Loading(true);
            Upload.upload({
                url: '/student/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.image = success.data;
                    $scope.admission.image = $scope.image;
                },
                function(error) {
                    Loading(false);
                    toaster.pop('error', "Error", error.data.message);
                });
        };

        $scope.getSchoolGrades = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/grade/getAllSchoolGrades',
                data: data
            }).then(function successCallback(response) {
                $scope.grades = response.data;
                $scope.getSchoolSessions();
                $scope.getSchoolSources();
                $scope.getAllInfo();
            }, function errorCallback(error) {
                toaster.pop('error', 'Grades', 'Something went wrong!');
            });
        };

        $scope.getAllInfo = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/medical_info/getAllMedicalInfos',
                data: data
            }).then(function successCallback(response) {
                $scope.infos = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Medical Info', 'cannot get all the Staff! try reloading page.');
            });
        };

        $scope.getSchoolSessions = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/session/getAllSchoolSessions',
                data: data
            }).then(function successCallback(response) {
                $scope.sessions = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Session', 'Something went wrong!');
            });
        };

        $scope.getSchoolSources = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/sources/getAllSchoolSources',
                data: data
            }).then(function successCallback(response) {
                $scope.sources = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Source', 'Something went wrong!');
            });
        };

        $scope.createAdmissionInquiry = function() {
            data = $scope.admission;
            data.school_id = $cookieStore.get('loggedInSchool').id;
            data.admission_status = "false";
            $http({
                method: "POST",
                url: "/student/register",
                data: data
            }).then(function successCallback(response) {
                $scope.cancelAdmissionForm();
                toaster.pop('success', 'Admission Inquiry', 'Saved Successfully.');
            }, function errorCallback(error) {
                toaster.pop('error', 'Admission Inquiry', error.data.message);
            });
        };
    }
]);

app.controller('listSchoolAdmissionController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.createAdmissionInquiry = function() {
            $state.go('schools.admission_create');
        };

        $scope.deleteAdmission = function(admission_id) {
            var data = {
                id: admission_id
            };
            var result = confirm("Please confirm, Do you really want to delete ?");
            if (result) {
                $http({
                    method: 'POST',
                    url: '/student/delete',
                    data: data
                }).then(function successCallback(response) {
                    $scope.getAllSchoolStaff();
                    toaster.pop('success', 'Admission', "Admission removed successfully!");
                }, function errorCallback(error) {
                    toaster.pop('error', 'Admission', error.data.message);
                });
            }
        };

        $scope.getAllSchoolStaff = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/student/getAllAdmissionInquiry',
                data: data
            }).then(function successCallback(response) {
                var admissions = response.data;
                $scope.admissions = admissions;
            }, function errorCallback(error) {
                toaster.pop('error', 'Admission Inquiry', 'cannot get all the Inquiries! try reloading page.');
            });
        };

        $scope.studentProfile = function(student_id) {
            $state.go('schools.student_create', {
                student_id: student_id
            });
        };

        $scope.editProfile = function(student_id) {
            $state.go('schools.student_edit', {
                student_id: student_id
            });
        };

        $scope.viewFollowups = function(student_id) {
            $state.go('schools.view_followUp', {
                student_id: student_id
            });
        };
    }
]);


app.controller('editSchoolAdmissionController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
    function($scope, $http, $state, toaster, $cookieStore) {
        $scope.getSchoolGrades = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/grade/getAllSchoolGrades',
                data: data
            }).then(function successCallback(response) {
                $scope.grades = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Grades', 'Something went wrong!');
            });
        };


        $scope.getAllInfo = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/medical_info/getAllMedicalInfos',
                data: data
            }).then(function successCallback(response) {
                $scope.infos = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Medical Info', 'cannot get all the Staff! try reloading page.');
            });
        };

        $scope.getSchoolSessions = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/session/getAllSchoolSessions',
                data: data
            }).then(function successCallback(response) {
                $scope.sessions = response.data;

            }, function errorCallback(error) {
                toaster.pop('error', 'Session', 'Something went wrong!');
            });
        };

        $scope.getSchoolSources = function() {
            var data = {
                school_id: $cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/sources/getAllSchoolSources',
                data: data
            }).then(function successCallback(response) {
                $scope.sources = response.data;
                $scope.admission.source = response.data.source_id;
            }, function errorCallback(error) {
                toaster.pop('error', 'Source', 'Something went wrong!');
            });
        };

        $scope.getEditAdmission = function() {
            $http({
                method: 'GET',
                url: '/student/edit?id=' + $state.params.student_id
            }).then(function successCallback(response) {
                $scope.getSchoolGrades();
                $scope.getSchoolSessions(response.data.session_id);
                $scope.getSchoolSources(response.data.source_id);
                $scope.getAllInfo();
                $scope.admission = response.data;
                var arr = response.data.sports;
                if (arr) {
                    var array1 = arr.split(',');
                    var data = {};
                    for (var i = 0; i < array1.length; i++) {
                        data[array1[i]] = true;
                    }
                    $scope.sports = data;
                }
            }, function errorCallback(error) {
                toaster.pop('error', 'Admission Inquiry', 'cannot get all the Inquiries! try reloading page.');
            });
        };

        $scope.saveEditAdmission = function() {
            data = $scope.admission;
            data.image = $scope.image;
            $http({
                method: 'POST',
                url: '/student/edit',
                data: data
            }).then(function successCallback(response) {
                toaster.pop('success', 'Admission Inquiry', 'Saved Successfully.');
                $state.go('schools.admission_list');
            }, function errorCallback(error) {
                toaster.pop('error', 'Admission Inquiry', error.data.message);
            });
        };

        $scope.uploadFile = function(file) {
            if (!file) return toaster.pop('error', 'File', 'No file selected !');
            Loading(true);
            Upload.upload({
                url: '/student/upload',
                data: {
                    file: file
                }
            }).then(function(success) {
                    Loading(false);
                    $scope.image = success.data;
                    $scope.saveDisable = true;
                },
                function(error) {
                    Loading(false);
                    toaster.pop('error', "Error", error.data.message);
                    $scope.saveDisable = false;
                });
        };

        $scope.cancelAdmissionForm = function() {
            $state.go('schools.admission_list');
        };
    }
]);
