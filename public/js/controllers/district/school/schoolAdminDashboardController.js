app.controller('schoolAdminDashboardController', ['$scope', '$http', '$state', 'toaster', '$cookieStore','$uibModal','$compile',
    function($scope, $http, $state, toaster, $cookieStore, $uibModal, $compile) {
        $scope.todo_lists = [];
       $scope.getAllSchoolAdminDashboardData = function(){
            $scope.school_total_students();
            $scope.school_events_lists();
            $scope.school_total_departments();
            $scope.school_total_staffs();
            $scope.school_students_lists();
            $scope.school_staffs_lists();
            $scope.school_events_lists_calendar();
            $scope.rander_chart();
        };
        $scope.departmentEnrolledRedirect = function(){
            $state.go('schools.schooldepartment_list');
        };
        $scope.staffEnrolledRedirect = function(){
            $state.go('schools.staff_list');
        };
        $scope.studentEnrolledRedirect = function(){
            $state.go('schools.student_list');
        };
        
        $scope.school_total_students = function(){
        	var data ={
        		school_id:$cookieStore.get('loggedInSchool').id
        	};
            $http({
                method: 'POST',
                url: '/dashboard/school/total_students',
                data:data
            }).then(function successCallback(response) {
              $scope.total_students = (response.data[0].total_students !== undefined) ? response.data[0].total_students : "0";
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong! getting Dashobard data');
            });
        };
        $scope.school_total_staffs = function(){
        	var data ={
        		school_id:$cookieStore.get('loggedInSchool').id
        	};
            $http({
                method: 'POST',
                url: '/dashboard/school/total_staffs',
                data:data
            }).then(function successCallback(response) {
              $scope.total_staffs = (response.data[0].total_staffs !== undefined) ? response.data[0].total_staffs : "0";
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong! getting Dashobard data');
            });
        };
        
        $scope.school_total_departments = function(){
        	var data ={
        		school_id:$cookieStore.get('loggedInSchool').id
        	};
            $http({
                method: 'POST',
                url: '/dashboard/school/total_departments',
                data:data
            }).then(function successCallback(response) {
              $scope.total_departments = (response.data[0].total_departments !== undefined) ? response.data[0].total_departments : "0";
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong! getting Dashobard data');
            });
        };
        $scope.school_events_lists = function(){
        	var data ={
        		school_id:$cookieStore.get('loggedInSchool').id
        	};
            $http({
                method: 'POST',
                url: '/dashboard/school/events_lists',
                data:data
            }).then(function successCallback(response) {
               $scope.events_lists = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong! getting Dashobard data');
            });
        };
        $scope.school_students_lists = function(){
        	var data ={
        		school_id:$cookieStore.get('loggedInSchool').id
        	};
            $http({
                method: 'POST',
                url: '/dashboard/school/students_lists',
                data:data
            }).then(function successCallback(response) {
               $scope.students_lists = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong! getting Dashobard data');
            });
        };
        $scope.school_staffs_lists = function(){
            var data ={
                school_id:$cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/dashboard/school/staffs_lists',
                data:data
            }).then(function successCallback(response) {
               $scope.staffs_lists = response.data;
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong! getting Dashobard data');
            });
        };
        $scope.dashboardEditEvent = function(event_id){
            $state.go('schools.event_edit',{
                event_id:event_id
            });
        };
        $scope.studentCollapsed = [{name: 'one',isCollapsed: true}];
        $scope.staffCollapsed = [{name: 'two',isCollapsed: true}];


        //calendar options and config

        $scope.uiConfig = {
          calendar:{
            height: 450,
            editable: false,
            header:{
              //left: 'month basicWeek basicDay agendaWeek agendaDay',
              left: 'month basicWeek',
              center: 'title',
              right: 'today prev,next'
            },
            eventClick: $scope.alertEventOnClick,
            eventDrop: $scope.alertOnDrop,
            eventResize: $scope.alertOnResize,
            eventRender: $scope.eventRender
          }
        };

        $scope.events = [];
        $scope.eventSources = [$scope.events];
        
        $scope.school_events_lists_calendar = function(){
            var data ={
                school_id:$cookieStore.get('loggedInSchool').id
            };
            $http({
                method: 'POST',
                url: '/dashboard/school/events_lists/calendar',
                data:data
            }).then(function successCallback(response) {
               angular.forEach(response.data,function(value, key){
                $scope.events.push({
                    title: value.title,
                    description: (typeof value.content != 'undefined' ) ? value.content.substring(17, 125).replace("</div>", "")+"..." : "No Description",
                    start: value.start_date,
                    end: value.end_date,
                    stick:true
                });
               });
               $scope.add_my_todos();
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong! getting Dashobard data');
            });
        };

        $scope.eventRender = function( event, element, view ) { 
            element.attr({
                'data-toggle' : 'tooltip',
                'title' : event.description
            });
            element.tooltip();
            $compile(element)($scope);
        };

        $scope.add_my_todos = function(){
            var data = {};
            data.user_id = $cookieStore.get('loggedInSchool').authorized_user_id;
            data.type = "admin";
            $http({
                method: 'POST',
                url: '/todo/getMyTodos',
                data:data
            }).then(function successCallback(response) {
                $scope.todo_lists = response.data;
               angular.forEach(response.data,function(value, key){
                $scope.events.push({
                    title: value.description,
                    description: 'To-Do Task',
                    start: new Date(value.date),
                    allDay: true,
                    color: (value.status == "1") ? "#27c24c" : "#ee3939",
                    stick:true
                });
               });
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong! getting Dashobard data');
            });
        };

        //charts option and config
        $scope.chart_datasetOverride = [{
            label: "Total Enrolled Student",
            borderWidth: 3,
            hoverBackgroundColor: "rgba(255,99,132,0.4)",
            hoverBorderColor: "rgba(255,99,132,1)",
            type: 'bar'
        }];
        var today = new Date();
        $scope.chart_labels = [];
        for (var i = 0; i < 7; i++) {
            $scope.chart_labels.push(today.toISOString().slice(0, 10));
            today.setDate(today.getDate() - 1);
        }
        $scope.chart_data = [];
        $scope.rander_chart = function() {
            var data = {
                school_id:$cookieStore.get('loggedInSchool').id
            }
            $http({
                method: 'POST',
                url: '/dashboard/school/chart_data',
                data:data
            }).then(function successCallback(response) {
                var row = [];

                angular.forEach($scope.chart_labels, function(value, key) {
                    var keepGoing = true;
                    var last_val = 0;
                    angular.forEach(response.data, function(res_value, res_key) {
                        if (keepGoing && value == res_value.date.slice(0, 10)) {
                            last_val = parseInt(res_value.count);
                            keepGoing = false;
                        }
                    });
                    row.push(last_val);
                });
                $scope.chart_data.push(row);
            }, function errorCallback(error) {
                toaster.pop('error', 'Dashobard', 'Something went wrong! getting Dashobard data');
            });
        };
  


    }
]);

//-------binit modal demo-------

  // $scope.open = function(){
    //      $scope.uibModalInstance =   $uibModal.open({
    //           templateUrl: '/partials/modals/testmodal.html',
    //           animation: true,
    //           ariaLabelledBy: 'modal-title',
    //           ariaDescribedBy: 'modal-body',
    //           size: 'lg'
    //         });
    //     };

// app.controller('myModalController', ['$scope', '$http', '$state', 'toaster', '$cookieStore',
//     function($scope, $http, $state, toaster, $cookieStore) {
//         $scope.myVariable = "123";
//         $scope.cancel = function(){
//             $scope.$dismiss();
//         }
//     }
// ]);


// <div ng-controller="myModalController">
//     <div class="modal-header">
//         <h3 class="modal-title" id="modal-title">I'm a modal!</h3>
//     </div>
//      <div class="modal-body" id="modal-body">
//             {{myVariable}}
//       </div>
//     <div class="modal-footer">
//         <button class="btn btn-primary" type="button" ng-click="hii()">OK</button>
//         <button class="btn btn-info" type="button" ng-click="cancel()" type="button" >Cancel</button>
//     </div>  
// </div>

