// getAllHomeworkStaffWise
// getAllHomeworksSchoolWise

var db = require('orm').db,
    Homeworks = db.models.homeworks;
HomeworkReports = db.models.homework_reports;


exports.register = function(req, res) {
    if (typeof req.body.class_id === 'undefined' || typeof req.body.grade_id === 'undefined') {
        return res.sendError(req, res, 500, 'Something went wrong!');
    }
    newHomewroks = new Homeworks();
    newHomewroks.school_id = req.body.school_id;
    newHomewroks.staff_id = req.body.staff_id;
    newHomewroks.grade_id = req.body.grade_id;
    newHomewroks.subject_id = req.body.subject_id;
    newHomewroks.class_id = req.body.class_id;
    newHomewroks.title = req.body.title;
    newHomewroks.description = req.body.description;
    newHomewroks.due_date = req.body.due_date;
    newHomewroks.start_time = req.body.start_time;
    newHomewroks.end_time = req.body.end_time;
    newHomewroks.file = req.body.file;
    //newHomewroks.type = req.body.type;
    //newHomewroks.priority = req.body.priority;
    newHomewroks.save(function(err, created_Homework) {
        if (err) {
            console.log(err);
            return sendError(req, res, 500, 'Something went wrong!');
        }
        var query_str = 'SELECT t1.student_id as id FROM student_classes as t1  WHERE t1.grade_id = ? AND t1.class_id = ?';
        db.driver.execQuery(query_str, [req.body.grade_id, req.body.class_id], function(err, all_students) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            if (all_students.length > 0) {
                all_students.forEach(function(value, key) {
                    var data = {
                        homework_id: created_Homework.id,
                        student_id: value.id,
                        achieved_grade: null,
                        file_path: null,
                        status: "0"
                    };
                    HomeworkReports.create(data, function(err) {
                        if (err) {
                            console.log(err);
                            return sendError(req, res, 500, "Something went wrong!");
                        }
                    });
                });
                return res.send(created_Homework);
            } else {
                return res.send(created_Homework);
            }
        });
    });
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Homework id required!!');
    }
    Homeworks.find({
        id: req.body.id
    }, 1, function(err, homeworks) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "ID required");
        }
        homeworks[0].staff_id = req.body.staff_id;
        homeworks[0].grade_id = req.body.grade_id;
        homeworks[0].subject_id = req.body.subject_id;
        homeworks[0].class_id = req.body.class_id;
        homeworks[0].title = req.body.title;
        homeworks[0].description = req.body.description;
        homeworks[0].due_date = req.body.due_date;
        if (req.body.file) {
            homeworks[0].file = req.body.file;
        }
        homeworks[0].start_time = req.body.start_time;
        homeworks[0].end_time = req.body.end_time;
        // homeworks[0].type = req.body.type;
        // homeworks[0].priority = req.body.priority;
        homeworks[0].save(function(err, updated_homework) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            return res.send(updated_homework);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Homeworks id required!!');
    }
    Homeworks.find({
        id: req.body.id
    }, 1, function(err, homeworks) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        homeworks[0].remove(function(err) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(homeworks[0]);
        });
    });
};

exports.getHomework = function(req, res) {
    if (typeof req.query.id == 'undefined') {
        return sendError(req, res, 401, "ID required field");
    }
    Homeworks.find({
        id: req.query.id
    }, 1, function(err, homeworks) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        if (!homeworks.length) return sendError(req, res, 404, "Homework not found");
        return res.send(homeworks[0]);
    });
};
exports.getHomework_POST = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 401, "ID required field");
    }
    Homeworks.find({
        id: req.body.id
    }, 1, function(err, homeworks) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        if (!homeworks.length) return sendError(req, res, 404, "Homework not found");
        return res.send(homeworks[0]);
    });
};

exports.getHomeworkStaffwise = function(req, res) {
    if (typeof req.body.staff_id == 'undefined' || req.body.staff_id === null) {
        return sendError(req, res, 422, "Staff ID required");
    }
    var query_str = `SELECT t1.*,t2.grade,CONCAT(t3.first_name," ",t3.last_name) as staff_name,t6.subject_name
    FROM homeworks as t1 LEFT JOIN grades as t2 ON t1.grade_id = t2.id
    LEFT JOIN classes as t5 ON t1.class_id = t5.id
    LEFT JOIN subjects as t6 ON t1.subject_id = t6.id
    LEFT JOIN staff as t3 ON t1.staff_id = t3.id
    WHERE t3.id = ? GROUP BY t1.id`;
    db.driver.execQuery(query_str, [req.body.staff_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

var fs = require('fs');
exports.uploadFile = function(req, res) {
    var fs = require('fs');
    var homeworks = __dirname + "/../../public/upload/homeworks/";
    if (!fs.existsSync(homeworks)) {
        fs.mkdirSync(homeworks);
    }
    var file = req.files.file;
    var fileExtension = file.name.split(".")[1];
    if (fileExtension) {
      fileName = file.path.split('/');
      fs.rename(file.path, homeworks + fileName[fileName.length - 1] +"."+ fileExtension);
      return res.send(fileName[fileName.length - 1] +"."+ fileExtension);
    }
    else {
      return sendError(req, res, 400, "Bad Request check your parameters");
    }
};

exports.getHomeworkStudentWise = function(req, res) {console.log(req.body);
    if (typeof req.body.student_id == 'undefined' || req.body.student_id === null) {
        return sendError(req, res, 422, "Staff ID required");
    }
    var query_str = `SELECT t1.*, t2.subject_id,t2.class_id,t2.title as homework_title,t2.description as homework_description,t2.due_date,CONCAT(t3.first_name," ",t3.last_name) as staff_name,t4.subject_name , t5.class_name
                    FROM homework_reports as t1
                    LEFT JOIN homeworks as t2 ON t1.homework_id = t2.id
                    LEFT JOIN staff as t3 ON t2.staff_id = t3.id
                    LEFT JOIN subjects as t4 ON t2.subject_id = t4.id
                    LEFT JOIN classes as t5 ON t2.class_id = t5.id
                    WHERE student_id = ? ORDER BY t1.id DESC`;

    db.driver.execQuery(query_str, [req.body.student_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getStudentsHomework = function(req, res) {
    if (typeof req.body.id == 'undefined' || req.body.id === null) {
        return sendError(req, res, 422, "Homework ID required");
    }
    var query_str = "SELECT t1.* FROM `homework_reports` as t1 WHERE t1.id = ?";
    db.driver.execQuery(query_str, [req.body.id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
