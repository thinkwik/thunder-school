var db = require('orm').db,
    Locations = db.models.locations;

exports.register = function(req, res) {
    newLocation = new Locations();
    newLocation.title = req.body.title;
    newLocation.description = req.body.description;
    newLocation.school_id = req.body.school_id;
    newLocation.building_name = req.body.building_name;
    newLocation.street_name = req.body.street_name;
    newLocation.city = req.body.city;
    newLocation.zip_code = req.body.zip_code;
    newLocation.floor = req.body.floor;
    newLocation.room_number = req.body.room_number;
    newLocation.max_students = req.body.max_students;
    newLocation.status = (typeof req.body.status != 'undefined') ? req.body.status : '1';
    newLocation.save(function(err, created_location) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(created_location);
    });
};



exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Location id required!!');
    }
    Locations.find({
        id: req.body.id
    }, 1, function(err, location) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        location[0].title = req.body.title;
        location[0].description = req.body.description;
        location[0].school_id = req.body.school_id;
        location[0].building_name = req.body.building_name;
        location[0].street_name = req.body.street_name;
        location[0].city = req.body.city;
        location[0].zip_code = req.body.zip_code;
        location[0].floor = req.body.floor;
        location[0].room_number = req.body.room_number;
        location[0].max_students = req.body.max_students;
        location[0].status = (typeof req.body.status != 'undefined') ? req.body.status : location[0].status;
        location[0].save(function(err, updated_location) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, "Bad Request check your parameters");
            }
            return res.send(updated_location);
        });
    });
};

exports.active = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Location id required!!');
    }
    Locations.find({
        id: req.body.id
    }, 1, function(err, location) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        location[0].status = "1";
        location[0].save(function(err, activated_location) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, "Bad Request check your parameters");
            }
            return res.send(activated_location);
        });
    });
};

exports.inactive = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Location id required!!');
    }
    Locations.find({
        id: req.body.id
    }, 1, function(err, location) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        location[0].status = "0";
        location[0].save(function(err, inactivated_location) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, "Bad Request check your parameters");
            }
            return res.send(inactivated_location);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Location id required!!');
    }
    Locations.find({
        id: req.body.id
    }, 1, function(err, locations) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        locations[0].remove(function(err) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, "Bad Request check your parameters");
            }
            return res.send(locations[0]);
        });
    });
};

exports.getLocation = function(req, res) {
    if (typeof req.query.id == 'undefined') return sendError(req, res, 401, "ID required field");
    Locations.find({
        id: req.query.id
    }, 1, function(err, locations) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        if (!locations.length) return sendError(req, res, 404, "Location not found");
        return res.send(locations[0]);
    });
};

exports.getAllLocations = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id == "null") {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = `SELECT t1.*,t2.school_name ,t2.max_students as number_of_seats FROM locations as t1 
    LEFT JOIN schools as t2 on t1.school_id = t2.id
    WHERE t2.id = ?`;
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
