var db = require('orm').db,
    Banners = db.models.banners;

exports.register = function(req, res) {
    newBanner = new Banners();
    newBanner.title = req.body.banner_title;
    newBanner.order = req.body.banner_order;
    newBanner.image_path = req.body.banner_image;
    newBanner.status = (typeof req.body.status != 'undefined') ? req.body.status : '1';
    newBanner.save(function(err, created_banner) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        return res.send(created_banner);
    });
};

exports.getAllBanners = function(req, res) {
    Banners.find({}, function(err, allBanners) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        return res.send(allBanners);
    });
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Banner id required!!');
    }
    Banners.find({
        id: req.body.id
    }, 1, function(err, banner) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        banner[0].title = req.body.banner_title;
        banner[0].order = req.body.blog_order;
        // banner[0].image_path = (req.body.image_path !== null) ? req.body.image_path : banner[0].image_path;
        banner[0].image_path = (typeof req.body.image_path == 'undefined' || req.body.image_path === "") ? banner[0].image_path : req.body.image_path;
        banner[0].status = (typeof req.body.status != 'undefined') ? req.body.status : banner[0].status;
        banner[0].save(function(err, updated_banner) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(updated_banner);
        });
    });
};

exports.active = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Banner id required!!');
    }
    Banners.find({
        id: req.body.id
    }, 1, function(err, banner) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        banner[0].status = "1";
        banner[0].save(function(err, activated_banner) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(activated_banner);
        });
    });
};

exports.inactive = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Banner id required!!');
    }
    Banners.find({
        id: req.body.id
    }, 1, function(err, banner) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        banner[0].status = "0";
        banner[0].save(function(err, inactivated_banner) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(inactivated_banner);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Banner id required!!');
    }
    Banners.find({
        id: req.body.id
    }, 1, function(err, banners) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        if (banners[0].image_path) {
        var bannerImages = __dirname + "/../../public/upload/bannerImages/" + banners[0].image_path;
        var fs = require('fs');
        fs.exists(bannerImages, function(exists) {
            if (exists) {
              fs.unlinkSync(bannerImages);
            }
        });
      }
        banners[0].remove(function(err) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(banners[0]);
        });
    });
};

exports.getBanner = function(req, res) {
    if (typeof req.query.id == 'undefined') return sendError(req, res, 401, "ID required field");
    Banners.find({
        id: req.query.id
    }, 1, function(err, banners) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        if (!banners.length) return sendError(req, res, 404, "Banner not found");
        return res.send(banners[0]);
    });
};

var fs = require('fs');
exports.uploadFile = function(req, res) {
    var fs = require('fs');
    var bannerImages = __dirname + "/../../public/upload/bannerImages/";
    if (!fs.existsSync(bannerImages)) {
        fs.mkdirSync(bannerImages);
    }
    var file = req.files.file;
    var fileExtension = file.name.split(".")[1];
    if (fileExtension) {
      fileName = file.path.split('/');
      fs.rename(file.path, bannerImages + fileName[fileName.length - 1] +"."+ fileExtension);
      return res.send(fileName[fileName.length - 1] +"."+ fileExtension);
    }
    else {
      return sendError(req, res, 400, "Bad Request check your parameters");
    }
};
