var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Banners = db.define('banners', {
    title: String,
    order: Number,
    image_path: String,
    status: ['0', '1']
}, {
    timestamp: true,
    methods: {

    }
});
