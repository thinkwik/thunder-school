var bCrypt = require('bcrypt-nodejs');
var db = require('orm').db,
    School = db.models.schools,
    Sessions = db.models.sessions;

exports.register = function(req, res) {
    if (typeof req.body.school_id != 'undefined') {
        var new_sessions = new Sessions();
        new_sessions.school_id = req.body.school_id;
        new_sessions.created_by = req.body.created_by;
        new_sessions.session_name = req.body.session_name;
        new_sessions.start_date = req.body.start_date;
        new_sessions.end_date = req.body.end_date;
        new_sessions.status = (typeof req.body.status != 'undefined') ? req.body.status : "1";
        new_sessions.save(function(err, saved_Sessions) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, "Bad Request check your parameters");
            }
            res.send(saved_Sessions);
        });

    } else {
        return sendError(req, res, 422, 'school_id required for register new Session');
    }
};

exports.getSession = function(req, res) {
    if (typeof req.query.id == 'undefined') return res.send("id required for getting json");
    Sessions.find({
        id: req.query.id
    }, 1, function(err, session) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        if (!session.length) return res.send("Session not found with perticular id");
        return res.send(session[0]);
    });
};

exports.getAllDistSessions = function(req, res) {
    if (typeof req.body.district_id == 'undefined' || req.body.district_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = "SELECT t1.*,t2.school_name,CONCAT(t3.first_name, ' ', t3.last_name) as user_name FROM `sessions` as t1 LEFT JOIN schools as t2 on t1.school_id = t2.id LEFT JOIN users as t3 on t1.created_by = t3.id  WHERE  t2.parent_id = ?";
    db.driver.execQuery(query_str, [req.body.district_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'Session id required');
    }
    Sessions.find({
        id: req.body.id
    }, 1, function(err, session) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        session[0].school_id = req.body.school_id;
        session[0].created_by = req.body.created_by;
        session[0].session_name = req.body.session_name;
        session[0].start_date = req.body.start_date;
        session[0].end_date = req.body.end_date;
        session[0].status = req.body.status;
        session[0].save(function(err, updated_session) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            return res.send(updated_session);
        });
    });
};

exports.active = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'Session id required');
    }
    Sessions.find({
        id: req.body.id
    }, 1, function(err, session) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        session[0].status = "1";
        session[0].save(function(err, activated_session) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            return res.send(activated_session);
        });
    });
};

exports.inactive = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'session id required');
    }
    Sessions.find({
        id: req.body.id
    }, 1, function(err, session) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        session[0].status = "0";
        session[0].save(function(err, inactivated_session) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            return res.send(inactivated_session);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Session id required!!');
    }
    Sessions.find({
        id: req.body.id
    }, 1, function(err, Sessions) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        Sessions[0].remove(function(err) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, "Bad Request check your parameters");
            }
            return res.send(Sessions[0]);
        });
    });
};

exports.getAllSchoolSessions = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || req.body.school_id === null) {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = `SELECT t1.*,t2.school_name,CONCAT(t3.first_name, ' ', t3.last_name) as user_name 
                    FROM sessions as t1 
                    LEFT JOIN schools as t2 on t1.school_id = t2.id 
                    LEFT JOIN users as t3 on t1.created_by = t3.id  
                    WHERE  t2.id = ? ORDER BY t1.start_date , t1.end_date`;
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};
