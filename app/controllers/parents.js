var bCrypt = require('bcrypt-nodejs');
var shortid = require('shortid');
var db = require('orm').db,
    Admissions = db.models.admissions,
    Parents = db.models.parents;

var createHash = function(password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
};

exports.register = function(req, res) {
    newParent = new Parents();
    newParent.student_id = req.body.student_id;
    newParent.unique_id = shortid.generate();
    newParent.full_name = req.body.full_name;
    newParent.password = createHash(req.body.password);
    newParent.relation = req.body.relation;
    newParent.gender = req.body.gender;
    newParent.email = req.body.email;
    newParent.phone = req.body.phone;
    newParent.mobile = req.body.mobile;
    newParent.work_phone_number = (typeof req.body.work_phone_number != 'undefined') ? req.body.work_phone_number : null;
    newParent.parentImage = req.body.parentImage;
    newParent.address = req.body.address;
    newParent.city = req.body.city;
    newParent.state = req.body.state;
    newParent.zipcode = req.body.zipcode;
    newParent.country = req.body.country;
    newParent.status = (typeof req.body.status != 'undefined') ? req.body.status : '1';
    newParent.profile_status = (typeof req.body.profile_status != 'undefined') ? req.body.profile_status : '1';
    newParent.save(function(err, created_parents) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        return res.send(created_parents);
    });
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Parent id required!!');
    }
    Parents.find({
        id: req.body.id
    }, 1, function(err, parents) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        parents[0].full_name = req.body.full_name;
        parents[0].password = req.body.password;
        parents[0].relation = req.body.relation;
        parents[0].gender = req.body.gender;
        parents[0].email = req.body.email;
        parents[0].phone = req.body.phone;
        parents[0].mobile = req.body.mobile;
        parents[0].work_phone_number = (typeof req.body.work_phone_number != 'undefined') ? req.body.work_phone_number : parents[0].work_phone_number;
        parents[0].parentImage = (typeof req.body.parentImage == 'undefined' || req.body.parentImage === "") ? parents[0].parentImage : req.body.parentImage;
        parents[0].address = req.body.address;
        parents[0].city = req.body.city;
        parents[0].state = req.body.state;
        parents[0].zipcode = req.body.zipcode;
        parents[0].country = req.body.country;
        parents[0].status = (typeof req.body.status != 'undefined') ? req.body.status : parents[0].status;
        parents[0].save(function(err,updated_parents) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(updated_parents);
        });
    });
};

exports.active = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Parent id required!!');
    }
    Parents.find({
        id: req.body.id
    }, 1, function(err, parents) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        parents[0].status = "1";
        parents[0].save(function(err, activated_parents) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(activated_parents);
        });
    });
};

exports.inactive = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Parent id required!!');
    }
    Parents.find({
        id: req.body.id
    }, 1, function(err, parents) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        parents[0].status = "0";
        parents[0].save(function(err, inactivated_parents) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, 'Parent id required!!');
            }
            return res.send(inactivated_parents);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 404, 'Parent id required!!');
    }
    Parents.find({
        id: req.body.id
    }, 1, function(err, parents) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        parents[0].remove(function(err) {
            if (err) {
                console.log(err);
                return sendError(req, res, 422, 'Parent required!!');
            }
            return res.send(parents[0]);
        });
    });
};

exports.getParent = function(req, res) {
    if (typeof req.query.id == 'undefined') return sendError(req, res, 401, "ID required field");
    Parents.find({
        id: req.query.id
    }, 1, function(err, parents) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        if (!parents.length) return sendError(req, res, 404, "Parent not found");
        return res.send(parents[0]);
    });
};

exports.getAllParents = function(req, res) {
    if (typeof req.body.id == 'undefined' || req.body.id === null || req.body.id == "null") {
        return sendError(req, res, 422, "refer parameters");
    }
    var query_str = "SELECT t1.*,CONCAT(t2.first_name ,' ',t2.last_name) as student_name FROM parents as t1 LEFT JOIN admissions as t2 ON t1.student_id = t2.id WHERE t1.id = ?";
    db.driver.execQuery(query_str, [req.body.id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getStudentsParents = function(req, res) {
    if (typeof req.body.student_id == 'undefined' || typeof req.body.student_id === null) {
        return sendError(req, res, 422, "district id required");
    }
    var query_str = `SELECT t1.*,t3.relation_name FROM parents as t1 LEFT JOIN admissions as t2 ON t1.student_id = t2.id LEFT JOIN relations as t3 ON t1.relation = t3.id WHERE t1.student_id = ?`;
    db.driver.execQuery(query_str, [req.body.student_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.parentsLogin = function(req, res) {
    Parents.find({
        email: req.body.email
    }, 1, function(err, result) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        } else if (result.length > 0) {
            if (req.body.password) {
                var hashFromDB = result[0].password;
                var plainPassFromUser = req.body.password;
                if (isValidPassword(result[0], plainPassFromUser)) {
                    var query_str = ' SELECT school_id, grade_id FROM admissions WHERE id = ?';
                    db.driver.execQuery(query_str, [result[0].student_id], function(err, students) {
                        if (err) {
                            console.log(err);
                            return sendError(req, res, 400, "Bad Request check your parameters");
                        }
                        if (students.length === 0) {
                            return sendError(req, res, 422, "parent's student not found");
                        }
                        result[0].school_id = students[0].school_id;
                        result[0].grade_id = students[0].grade_id;
                        return res.send(result[0]);
                    });
                } else {
                    return sendError(req, res, 422, "Password not match!");
                }
            } else {
                return sendError(req, res, 404, "Please enter Password");
            }
        } else {
            return sendError(req, res, 404, "Parents not found");
        }
    });
};

exports.changeChild = function(req, res) {
    if (typeof req.body.student_id == 'undefined' || typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, "please check your parameters");
    }
    Parents.find({
        id: req.body.id
    }, 1, function(err, result) {
        if (err) {
            console.log(err);
            return sendError(req, res, 422, "Bad Request check your parameters");
        } else if (result.length > 0) {
            var query_str = ' SELECT school_id, grade_id FROM admissions WHERE id = ?';
            db.driver.execQuery(query_str, [req.body.student_id], function(err, students) {
                if (err) {
                    console.log(err);
                    return sendError(req, res, 400, "Bad Request check your parameters");
                }
                if (students.length === 0) {
                    return sendError(req, res, 422, "parent's student not found");
                }
                result[0].school_id = students[0].school_id;
                result[0].grade_id = students[0].grade_id;
                result[0].student_id = req.body.student_id;
                return res.send(result[0]);
            });
        } else {
            return sendError(req, res, 404, "Parents not found");
        }
    });
};

var isValidPassword = function(parents, password) {
    return bCrypt.compareSync(password, parents.password);
};

exports.getStudentsParentsEdit = function(req, res) {
    if (typeof req.body.parent_id == 'undefined' || typeof req.body.parent_id === null) {
        return sendError(req, res, 422, "id required");
    }
    var query_str = 'SELECT t1.* FROM `parents` as t1 LEFT JOIN admissions as t2 ON t1.student_id = t2.id WHERE t1.id = ?';
    db.driver.execQuery(query_str, [req.body.parent_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows[0]);
    });

};

exports.children = function(req, res) {
    if (typeof req.body.student_id == 'undefined' || typeof req.body.student_id === null) {
        return sendError(req, res, 422, "id required");
    }
    var query_str = 'SELECT t1.* FROM `admissions` as t1 LEFT JOIN parents as t2 ON t1.id = t2.student_id WHERE t1.id = ?';
    db.driver.execQuery(query_str, [req.body.student_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows[0]);
    });
};

exports.getAllChildren = function(req, res) {
    if (typeof req.body.email == 'undefined' || typeof req.body.email === null) {
        return sendError(req, res, 422, "id required");
    }
    var query_str = 'SELECT t2.* FROM parents as t1 LEFT JOIN admissions as t2 ON t1.student_id = t2.id WHERE t1.email = ?';
    db.driver.execQuery(query_str, [req.body.email], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.getSchoolParents = function(req, res) {
    if (typeof req.body.school_id == 'undefined' || typeof req.body.school_id === null) {
        return sendError(req, res, 422, "id required");
    }
    var query_str = 'SELECT t1.* FROM `parents` as t1 LEFT JOIN admissions as t2 ON t1.student_id = t2.id LEFT JOIN schools as t3 ON t2.school_id = t3.id WHERE t2.school_id = ?';
    db.driver.execQuery(query_str, [req.body.school_id], function(err, rows) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(rows);
    });
};

exports.uploadFile = function(req, res) {
    var fs = require('fs');
    var parents = __dirname + "/../../public/upload/parents/";
    if (!fs.existsSync(parents)) {
        fs.mkdirSync(parents);
    }
    var file = req.files.file;
    var fileExtension = file.name.split(".")[1];
    if (fileExtension) {
        fileName = file.path.split('/');
        fs.rename(file.path, parents + fileName[fileName.length - 1] + "." + fileExtension);
        return res.send(fileName[fileName.length - 1] + "." + fileExtension);
    } else {
        return sendError(req, res, 400, "Bad Request check your parameters");
    }

};
