var db = require('orm').db,
    Package = db.models.packages;

exports.getAllPackages = function(req, res) {
    Package.find({}, function(err, packages) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(packages);
    });
};

exports.register = function(req, res) {
    if (typeof req.body.school_type == 'undefined' || typeof req.body.package_name == 'undefined') {
        return sendError(req, res, 422, 'requested parameter are less then we accept');
    }
    newPackage = new Package();
    newPackage.school_type = req.body.school_type;
    newPackage.package_name = req.body.package_name;
    newPackage.price = req.body.price;
    newPackage.period = req.body.period;
    newPackage.status = (typeof req.body.status != 'undefined') ? req.body.status : '1';
    newPackage.save(function(err, saved_package) {
        if (err) {
            console.log(err);
            return sendError(req, res, 400, "Bad Request check your parameters");
        }
        return res.send(saved_package);
    });
};

exports.getPackage = function(req, res) {
    if (typeof req.query.id == 'undefined') {
        return sendError(req, res, 422, 'requested parameter are less then we accept');
    }
    var findPackage = {
        id: req.query.id
    };
    Package.find(findPackage, 1, function(err, packages) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        if (packages.length) {
            return res.send(packages[0]);
        } else {
            return res.send([]);
        }
    });
};

exports.edit = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'requested parameter are less then we accept');
    }
    var findPackage = {
        id: req.body.id
    };
    Package.find(findPackage, 1, function(err, packages) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        if (!packages.length) {
            return res.sendError(req, res, 404, "requested package not found");
        }
        packages[0].school_type = req.body.school_type;
        packages[0].package_name = req.body.package_name;
        packages[0].price = req.body.price;
        packages[0].period = req.body.period;
        if (typeof req.body.status != 'undefined') {
            packages[0].status = req.body.status;
        }
        packages[0].save(function(err, updated_package) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(updated_package);
        });
    });
};

exports.active = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'requested parameter are less then we accept');
    }
    var findPackage = {
        id: req.body.id
    };
    Package.find(findPackage, 1, function(err, packages) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        if (!packages.length) {
            return res.sendError(req, res, 404, "requested package not found");
        }
        packages[0].status = '1';
        packages[0].save(function(err, active_package) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(active_package);
        });
    });
};

exports.inactive = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'requested parameter are less then we accept');
    }
    var findPackage = {
        id: req.body.id
    };
    Package.find(findPackage, 1, function(err, packages) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        if (!packages.length) {
            return res.sendError(req, res, 404, "requested package not found");
        }
        packages[0].status = '0';
        packages[0].save(function(err, inactive_package) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(inactive_package);
        });
    });
};

exports.delete = function(req, res) {
    if (typeof req.body.id == 'undefined') {
        return sendError(req, res, 422, 'requested parameter are less then we accept');
    }
    var findPackage = {
        id: req.body.id
    };
    Package.find(findPackage, 1, function(err, packages) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        if (!packages.length) {
            return res.sendError(req, res, 404, "requested package not found");
        }
        packages[0].remove(function(err, success) {
            if (err) {
                console.log(err);
                return res.send(err);
            }
            return res.send(packages[0]);
        });
    });
};
