var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Grade = db.define('grades', {
    school_id: {
        type: 'integer'
    },
    grade: String,
    position: {
        type: 'integer'
    },
    // position: Number,
    status: ['0', '1']
}, {
    timestamp: true,
    methods: {

    }
});
