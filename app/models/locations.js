var db = require('orm').db;
var modts = require('orm-timestamps');

db.use(modts, {
    createdProperty: 'created_at',
    modifiedProperty: 'modified_at'
});

var Location = db.define('locations', {
    school_id: {
        type: 'integer'
    },
    title: String,
    description: String,
    building_name: String,
    street_name: String,
    city: String,
    zip_code: String,
    floor: String,
    room_number: String,
    max_students: String,
    status: ['0', '1'],

}, {
    timestamp: true,
    methods: {

    }
});
