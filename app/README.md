ALTER TABLE `appointments` ADD `phone_number` VARCHAR(20) NULL DEFAULT NULL AFTER `description`;
ALTER TABLE `users` ADD `notes` TEXT NULL DEFAULT NULL AFTER `medical_allergies`;
ALTER TABLE `staff` ADD `notes` TEXT NULL DEFAULT NULL AFTER `emergency_contacts`;
ALTER TABLE `admissions` ADD `notes` TEXT NULL DEFAULT NULL AFTER `medical_allergies`;
ALTER TABLE `parents` ADD `notes` TEXT NULL DEFAULT NULL AFTER `zipcode`;