var bCrypt = require('bcrypt-nodejs');
var db = require('orm').db,
    NewsLetter = db.models.news_letters;

exports.register = function(req, res) {
    if (typeof req.body.subject == 'undefined' || typeof req.body.users_to_understand == 'undefined') {
        return sendError(req, res, 400, 'please check your parameters');
    }
    var letter = new NewsLetter();
    letter.subject = req.body.subject;
    letter.users_to_understand = req.body.users_to_understand;
    letter.content = req.body.content;
    letter.time_period = req.body.time_period;
    letter.status = (typeof req.body.status != 'undefined') ? req.body.status : "1";
    letter.save(function(err, saved_letter) {
        if (err) {
            console.log(err);
            return sendError(req, res, 500, 'something went wrong!');
        }
        return res.send(saved_letter);
    });

};
exports.edit = function(req, res) {
    if (req.body.id !== '' || req.body.id !== undefined) {
        NewsLetter.find({
            id: req.body.id
        }, 1, function(err, news_letters) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            if (news_letters.length) {
                news_letters[0].subject = req.body.subject;
                news_letters[0].users_to_understand = req.body.users_to_understand;
                news_letters[0].content = req.body.content;
                news_letters[0].time_period = req.body.time_period;
                news_letters[0].status = (typeof req.body.status != 'undefined') ? req.body.status : news_letters[0].status;
                news_letters[0].save(function(err, success) {
                    if (err) {
                        console.log(err);
                        return sendError(req, res, 500, "something went wrong while updating user");
                    }
                    return res.send(news_letters[0]);
                });
            } else {
                return sendError(req, res, 404, "news_letter not found");
            }
        });
    } else {
        return sendError(req, res, 422, "require news_letter's id in request parameter");
    }
};
exports.getNewsLetter = function(req, res) {
    if (req.query.id !== '' && req.query.id !== undefined) {
        NewsLetter.find({
            id: req.query.id
        }, 1, function(err, news_letters) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            if (news_letters.length) {
                news_letter = news_letters[0];
                return res.send(news_letter);
            } else {
                return sendError(req, res, 404, "News Letter not found");
            }
        });
    } else {
        return sendError(req, res, 422, "require news_letter's id in request parameter");
    }
};

exports.getAllNewsLetters = function(req, res) {
    NewsLetter.find({}, function(err, all_letters) {
        if (err) {
            console.log(err);
            return sendError(req, res, 500, 'something went wrong!');
        }
        return res.send(all_letters);
    });
};
exports.active = function(req, res) {
    if (req.body.id !== '' || req.body.id !== undefined) {
        NewsLetter.find({
            id: req.body.id
        }, 1, function(err, news_letters) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            if (news_letters.length) {
                news_letters[0].status = "1";
                news_letters[0].save(function(err, success) {
                    if (err) {
                        console.log(err);
                        return sendError(req, res, 500, "something went wrong while activating news_letter");
                    }
                    return res.send(news_letters[0]);
                });
            } else {
                return sendError(req, res, 404, "news_letter not found");
            }
        });
    } else {
        return sendError(req, res, 422, "require news_letter's id in request parameter");
    }
};
exports.inactive = function(req, res) {
    if (req.body.id !== '' || req.body.id !== undefined) {
        NewsLetter.find({
            id: req.body.id
        }, 1, function(err, news_letters) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            if (news_letters.length) {
                news_letters[0].status = "0";
                news_letters[0].save(function(err, success) {
                    if (err) {
                        console.log(err);
                        return sendError(req, res, 500, "something went wrong while inactivating news_letter");
                    }
                    return res.send(news_letters[0]);
                });
            } else {
                return sendError(req, res, 404, "news_letter not found");
            }
        });
    } else {
        return sendError(req, res, 422, "require news_letter's id in request parameter");
    }
};
exports.delete = function(req, res) {
    if (req.body.id !== '' || req.body.id != 'undefined') {
        NewsLetter.find({
            id: req.body.id
        }, 1, function(err, news_letters) {
            if (err) {
                console.log(err);
                return sendError(req, res, 400, "Bad Request check your parameters");
            }
            if (news_letters.length) {
                news_letters[0].remove(function(err, success) {
                    if (err) {
                        console.log(err);
                        return sendError(req, res, 500, "something went wrong while deleting user");
                    }
                    return res.send(news_letters[0]);
                });
            } else {
                return sendError(req, res, 404, "News Letter not found");
            }
        });
    } else {
        return sendError(req, res, 422, "require news_letters's id in request parameter");
    }
};
